import { ModuleWithProviders } from '@angular/core';
export declare class SelfComponentsModule {
    static forRoot(config?: any): ModuleWithProviders;
}
