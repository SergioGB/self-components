import { CurrencyPipe } from '@angular/common';
import { SendEmailWidgetComponent } from './components/send-email-widget/send-email-widget';
import { ComponentsModule } from "mapfre-framework/dist";
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';
import { ImageCropperModule } from 'ng2-img-cropper';
import { MbscModule } from './lib/mobiscroll/js/mobiscroll.angular.min';
import { ApiProvider } from './providers/api/api';
import { FlowProvider } from './providers/flow/flow';
import { UserProvider } from './providers/user/user';
import { UserRegistrationProvider } from './providers/user-registration/user-registration';
import { PlatformProvider } from './providers/platform/platform';
import { DateProvider } from './providers/date/date';
import { FormProvider } from './providers/form/form';
import { FormatProvider } from './providers/format/format';
import { PageSettingsProvider } from './providers/page-settings/page-settings';
import { MenuProvider } from './providers/menu/menu';
import { ComponentSettingsProvider } from './providers/component-settings/component-settings';
import { TranslationProvider } from './providers/translation/translation';
import { IncidenceProvider } from './providers/incidence/incidence';
import { IncidencesAndClaimsProvider } from './providers/incidences-and-claims/incidences-and-claims';
import { FileUtils } from './providers/utils/fileUtils';
import { Utils } from './providers/utils/utils';
import { ModalProvider } from './providers/modal/modal';
import { NavigationProvider } from './providers/navigation/navigation';
import { FileInsuranceProvider } from './providers/file-insurance/file-insurance';
import { TransferProvider } from './providers/transfer/transfer';
import { LocationProvider } from './providers/location/location';
import { StepsProvider } from './providers/steps/steps';
import { DocumentsProvider } from './providers/documents/documents';
import { InsuranceProvider } from './providers/insurance/insurance';
import { PersonalInfoProvider } from './providers/personal-info/personal-info';
import { SocialLoginProvider } from './providers/social-login/social-login';
import { InformationProvider } from './providers/information/information';
import { Mocks } from './providers/mocks/mocks';
import { FileIncidencesProvider } from './providers/file-incidences/file-incidences';
import { FileClaimsProvider } from './providers/file-claims/file-claims';
import { AssistanceProvider } from './providers/assistance/assistance';
import { TooltipProvider } from './providers/tooltip/tooltip';
import { InsuranceUpgradeProvider } from './providers/insurance-upgrade/insurance-upgrade';
import { SubmitIncidenceProvider } from './providers/submit-incidence/submit-incidence';
import { SubmitHomeIncidenceProvider } from './providers/submit-incidence/_home/submit-home-incidence';
import { ReceiptPaymentProvider } from './providers/receipt-payment/receipt-payment';
import { OpenReceiptPaymentProvider } from './providers/open-receipt-payment/open-receipt-payment';
import { RequestAssistanceProvider } from './providers/request-assistance/request-assistance';
import { RequestAssistanceHomeProvider } from './providers/request-assistance/_home/request-home-assistance';
import { OfferProvider } from './providers/offer/offer';
import { OpenRequestAssistanceProvider } from './providers/open-request-assistance/open-request-assistance';
import { OpenRequestHomeAssistanceProvider } from './providers/open-request-assistance/open-request-home-assistance';
import { AppLoadService } from './providers/loader/AppLoadService';
import { UserInactiveModalProvider } from './providers/user-inactive-modal/user-inactive-modal';
import { WelcomePackProvider } from './providers/welcome-pack/welcome-pack';
import { IdentificationProvider } from './providers/identification/identification';
import { AnimationsProvider } from './providers/animations/animations';
import { ReceiptFractionamentProvider } from './providers/receipt-fractionament/receipt-fractionament';
import { PolicyCancellationProvider } from './providers/policy-cancellation/policy-cancellation';
import { PolicyChangeProvider } from './providers/policy-change/policy-change';
import { ConfigurationToolProvider } from './providers/configuration-tool/configuration-tool';
import { AppGlobalService } from './providers/utils/app-global.service';
import { Components } from './providers/mocks/components';
import { PotentialClientProvider } from './providers/potential-client/potential-client';
import { SortPipe } from './pipes/sort/sort';
import { KeysPipe } from './pipes/ngfor/keys';
import { DiscardByKeyValuePipe } from './pipes/discard-by-key-value/discard-by-key-value';
import { AutosizeDirective } from './directives/autosize/autosize';
import { DndDirective } from './directives/dnd/dnd';
import { DndClaimsDirective } from './directives/dnd-claims/dnd-claims';
import { DndInsuranceDirective } from './directives/dnd-insurance/dnd-insurance';
import { DynamicCreatorDirective } from './directives/dynamic-component/dynamic-component-creator.directive';
import { HeaderGroupComponent } from './components/header-group/header-group';
import { MapfreFooterComponent } from './components/mapfre-footer/mapfre-footer';
import { MapfreNetworksFooterComponent } from './components/mapfre-networks-footer/mapfre-networks-footer';
import { TypedInputComponent } from './components/typed-input/typed-input';
import { VideoModalComponent } from './components/_modals/video-modal/video-modal';
import { YourPendingTopicsComponent } from './components/your-pending-topics/your-pending-topics';
import { YourManagerComponent } from './components/your-manager/your-manager';
import { DesktopStepsComponent } from './components/desktop-steps/desktop-steps';
import { AppointmentModalComponent } from './components/_modals/appointment-modal/appointment-modal';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner';
import { IconsDisplayComponent } from './components/development/icons-display/icons-display';
import { ModalsDisplayComponent } from './components/modals-display/modals-display';
import { NotLoggedInHeaderComponent } from './components/not-logged-in-header/not-logged-in-header';
import { TimePickerInputComponent } from './components/time-picker-input/time-picker-input';
import { WorkshopDisplayComponent } from './components/workshops/workshop-display/workshop-display';
import { WorkshopSelectionComponent } from './components/workshops/workshop-selection/workshop-selection';
import { AdditionalWorkshopServicesComponent } from './components/workshops/additional-workshop-services/additional-workshop-services';
import { FuzzyDateSelectorComponent } from './components/fuzzy-date-selector/fuzzy-date-selector';
import { TooltipIconComponent } from './components/tooltip-icon/tooltip-icon';
import { PillComponent } from './components/pill/pill';
import { RegistrationFormComponent } from './components/login/registration/registration-form/registration-form';
import { SimpleRegistrationFormComponent } from './components/login/registration/registration-form-simple/registration-form-simple';
import { RegistrationFormBasicComponent } from './components/login/registration/registration-form-basic/registration-form-basic';
import { SocialRegistrationFormComponent } from './components/login/registration/social-registration-form/social-registration-form';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password';
import { ChangedPrivacyForgotPasswordComponent } from './components/personal-info/password-and-security-questions-change/changed-privacy-forgot-password/changed-privacy-forgot-password';
import { ForgotPinComponent } from './components/login/forgot-pin/forgot-pin';
import { LoginFormComponent } from './components/login/login-form/login-form';
import { LoginQuickManagementComponent } from './components/login/login-quick-management/login-quick-management';
import { LoginSuccessStoriesComponent } from './components/login/login-success-stories/login-success-stories';
import { LoginBannerComponent } from './components/login/login-banner/login-banner';
import { LoginVideoComponent } from './components/login/login-video/login-video';
import { LoginAppDownloadComponent } from './components/login/login-app-download/login-app-download';
import { LoginContactComponent } from './components/login/login-contact/login-contact';
import { UserDataFormComponent } from './components/complete-registration-flow/user-data-form/user-data-form';
import { UserPictureDataFormComponent } from './components/complete-registration-flow/user-picture-data-form/user-picture-data-form';
import { UserContactDataFormComponent } from './components/complete-registration-flow/user-contact-data-form/user-contact-data-form';
import { UserSecurityDataFormComponent } from './components/complete-registration-flow/user-security-data-form/user-security-data-form';
import { BlockedUserCheckMailComponent } from './components/blocked-user/blocked-user-check-mail/blocked-user-check-mail';
import { BlockedUserCheckSmsComponent } from './components/blocked-user/blocked-user-check-sms/blocked-user-check-sms';
import { BlockedUserMethodSelectionComponent } from './components/blocked-user/blocked-user-method-selection/blocked-user-method-selection';
import { BlockedUserSecurityQuestionsComponent } from './components/blocked-user/blocked-user-security-questions/blocked-user-security-questions';
import { BlockedUserPasswordChangeComponent } from './components/blocked-user/blocked-user-password-change/blocked-user-password-change';
import { BlockedUserPinChangeComponent } from './components/blocked-user/blocked-user-pin-change/blocked-user-pin-change';
import { ChangeBlockedUserCheckMailComponent } from './components/personal-info/password-and-security-questions-change/change-blocked-user/change-blocked-user-check-mail/change-blocked-user-check-mail';
import { ChangeBlockedUserCheckSmsComponent } from './components/personal-info/password-and-security-questions-change/change-blocked-user/change-blocked-user-check-sms/change-blocked-user-check-sms';
import { ChangeBlockedUserMethodSelectionComponent } from './components/personal-info/password-and-security-questions-change/change-blocked-user/change-blocked-user-method-selection/change-blocked-user-method-selection';
import { ChangeBlockedUserSecurityQuestionsComponent } from './components/personal-info/password-and-security-questions-change/change-blocked-user/change-blocked-user-security-questions/change-blocked-user-security-questions';
import { ChangeBlockedUserPasswordChangeComponent } from './components/personal-info/password-and-security-questions-change/change-blocked-user/change-blocked-user-password-change/change-blocked-user-password-change';
import { ChangeBlockedUserPinChangeComponent } from './components/personal-info/password-and-security-questions-change/change-blocked-user/change-blocked-user-pin-change/change-blocked-user-pin-change';
import { SubheaderComponent } from './components/subheader/subheader';
import { GlobalPositionNewCommunicationsComponent } from './components/global-position/global-position-new-communications/global-position-new-communications';
import { GlobalPositionInsuranceEntryComponent } from './components/global-position/global-position-insurance-entry/global-position-insurance-entry';
import { GlobalPositionHireInsuranceComponent } from './components/global-position/global-position-hire-insurance/global-position-hire-insurance';
import { UserContactsComponent } from './components/user-contacts/user-contacts';
import { UsualInquiriesComponent } from './components/usual-inquiries/usual-inquiries';
import { InsuranceHeaderComponent } from './components/insurance-detail/insurance-header/insurance-header';
import { PolicyListComponent } from './components/insurance-detail/policy-list/policy-list';
import { PolicyDataComponent } from './components/insurance-detail/policy-list/policy-data/policy-data';
import { CarPolicyComponent } from './components/insurance-detail/policy-list/policy-data/car-policy/car-policy';
import { HomePolicyComponent } from './components/insurance-detail/policy-list/policy-data/home-policy/home-policy';
import { CoverageListComponent } from './components/coverage-list/coverage-list';
import { InvoiceListComponent } from './components/insurance-detail/invoice-list/invoice-list';
import { PaymentListComponent } from './components/insurance-detail/payment-list/payment-list';
import { PaymentChooseComponent } from './components/insurance-detail/payment-choose/payment-choose';
import { PolicyCancelModalComponent } from './components/_modals/_policies/policy-cancel-modal/policy-cancel-modal';
import { LossListComponent } from './components/insurance-detail/loss-list/loss-list';
import { LossComponent } from './components/insurance-detail/loss-list/loss/loss';
import { DocumentationListComponent } from './components/insurance-detail/documentation-list/documentation-list';
import { InsuranceDocumentationListComponent } from './components/insurance-detail/insurance-documentation-list/insurance-documentation-list';
import { ClaimDocumentationListComponent } from './components/insurance-detail/documentation-list/claim-documentation-list/claim-documentation-list';
import { ProceedingDocumentationListComponent } from './components/insurance-detail/documentation-list/claim-documentation-list/proceeding-documentation-list/proceeding-documentation-list';
import { CancelPolicyQuestionComponent } from './components/insurance-detail/cancel-policy-question/cancel-policy-question';
import { PolicyUpdateSuccessWithoutPaymentModalComponent } from './components/_modals/_policies/update-feedback/policy-update-success-without-payment-modal/policy-update-success-without-payment-modal';
import { PolicyUpdateSuccessWithPaymentModalComponent } from './components/_modals/_policies/update-feedback/policy-update-success-with-payment-modal/policy-update-success-with-payment-modal';
import { PolicyUpdateFailureModalComponent } from './components/_modals/_policies/update-feedback/policy-update-failure-modal/policy-update-failure-modal';
import { GeneralTermsComponent } from './components/interest-information/general-terms/general-terms';
import { DocumentationComponent } from './components/interest-information/documentation/documentation';
import { InformativeNotesComponent } from './components/interest-information/informative-notes/informative-notes';
import { InsuranceConsortiumComponent } from './components/interest-information/insurance-consortium/insurance-consortium';
import { LossDetailComponent } from './components/loss-detail/loss-detail';
import { FaqsHeaderComponent } from './components/faqs/faqs-header/faqs-header';
import { FaqsContentComponent } from './components/faqs/faqs-content/faqs-content';
import { CardFaqsComponent } from './components/faqs/card-faqs/card-faqs';
import { CommunicationsListComponent } from './components/communications-list/communications-list';
import { CommunicationsEmptyComponent } from './components/communications-list/communications-empty/communications-empty';
import { CommunicationsComponent } from './components/communications-list/communications/communications';
import { CommunicationsModalComponent } from './components/_modals/_communications/communications-modal/communications-modal';
import { TitleComponent } from './components/title/title';
import { IncidenceFormComponent } from './components/incidence-form/incidence-form';
import { IncidenceFormSimpleComponent } from './components/incidence-form-simple/incidence-form-simple';
import { CardButtonComponent } from './components/card-button/card-button';
import { ConfirmIncidenceCardComponent } from './components/confirm-incidence-card/confirm-incidence-card';
import { ClaimFormComponent } from './components/claim-form/claim-form';
import { ClaimFormSimpleComponent } from './components/claim-form-simple/claim-form-simple';
import { IncidencesAndClaimsCardsComponent } from './components/incidences-and-claims/cards/incidences-and-claims-cards';
import { InsuranceSelectionComponent } from './components/submit-incidence/insurance-selection/insurance-selection';
import { IncidentHeaderComponent } from './components/incident-detail/incident-header/incident-header';
import { SubmitIncidenceCallMeBackComponent } from './components/submit-incidence/submit-incidence-call-me-back/submit-incidence-call-me-back';
import { SubmitIncidenceHowOtherVehiclesComponent } from './components/submit-incidence/submit-incidence-when-where-and-how/submit-incidence-how-other-vehicles/submit-incidence-how-other-vehicles';
import { SubmitIncidenceHowOwnVehicleComponent } from './components/submit-incidence/submit-incidence-when-where-and-how/submit-incidence-how-own-vehicle/submit-incidence-how-own-vehicle';
import { HomeIncidenceDateSelectionComponent } from './components/submit-incidence/_home/home-incidence-date-selection/home-incidence-date-selection';
import { HomeIncidenceTypeSelectionComponent } from './components/submit-incidence/_home/home-incidence-type-selection/home-incidence-type-selection';
import { HomeIncidenceLocationCardComponent } from './components/submit-incidence/_home/home-incidence-location-card/home-incidence-location-card';
import { HomeIncidenceDamagedItemsCardComponent } from './components/submit-incidence/_home/home-incidence-damaged-items-card/home-incidence-damaged-items-card';
import { HomeIncidenceThirdPartyDamagesCardComponent } from './components/submit-incidence/_home/home-incidence-third-party-damages-card/home-incidence-third-party-damages-card';
import { HomeIncidenceDamageTypeSelectionComponent } from './components/submit-incidence/_home/home-incidence-damage-type-selection/home-incidence-damage-type-selection';
import { HomeIncidenceDamageReasonSelectionComponent } from './components/submit-incidence/_home/home-incidence-damage-reason-selection/home-incidence-damage-reason-selection';
import { HomeIncidenceDamageDetailComponent } from './components/submit-incidence/_home/home-incidence-damage-detail/home-incidence-damage-detail';
import { HomeIncidenceInjuredDataComponent } from './components/submit-incidence/_home/home-incidence-injured-data/home-incidence-injured-data';
import { FabGroupComponent } from './components/fab-group/fab-group';
import { CallMeResponseModalComponent } from './components/_modals/_contact/call-me-response-modal/call-me-response-modal';
import { FormResponseModalComponent } from './components/_modals/_receipts/form-response-modal/form-response-modal';
import { OwnDamagesFormComponent } from './components/submit-incidence/own-damages-form/own-damages-form';
import { ContraryDamagesFormComponent } from './components/submit-incidence/contrary-damages-form/contrary-damages-form';
import { VehicleDamagedAreasComponent } from './components/submit-incidence/vehicle-damaged-areas/vehicle-damaged-areas';
import { OtherDamagesFormComponent } from './components/submit-incidence/other-damages-form/other-damages-form';
import { OtherParticipantsFormComponent } from './components/submit-incidence/other-participants-form/other-participants-form';
import { OfflineAlertComponent } from './components/offline/offline-alert';
import { PaymentMethodSelectionComponent } from './components/payment-method-selection/payment-method-selection';
import { IncidenceProfessionalSelectionComponent } from './components/submit-incidence/incidence-professional-selection/incidence-professional-selection';
import { ProfessionalMeetingConfirmationModalComponent } from './components/_modals/professional-meeting-confirmation-modal/professional-meeting-confirmation-modal';
import { ProfessionalMeetingModalComponent } from './components/_modals/professional-meeting-modal/professional-meeting-modal';
import { ModifyCardPopupComponent } from './components/modify-card-popup/modify-card-popup';
import { SinisterTracingsComponent } from './components/sinisters/tracings/sinister-tracings';
import { SinisterTracingListComponent } from './components/sinisters/tracings/sinister-tracing-list/sinister-tracing-list';
import { SinisterTracingEntryComponent } from './components/sinisters/tracings/sinister-tracing-entry/sinister-tracing-entry';
import { SinisterDocumentationComponent } from './components/sinisters/documentation/sinister-documentation';
import { ImageSliderModalComponent } from './components/_modals/image-slider-modal/image-slider-modal';
import { ProfessionalMeetingComponent } from './components/submit-incidence/professional-meeting/professional-meeting';
import { ReportConfirmationComponent } from './components/report-incidence/report-confirmation/report-confirmation';
import { RatingComponent } from './components/rating/rating';
import { ProcessConfirmationComponent } from './components/process-confirmation/process-confirmation';
import { ProcessFailureComponent } from './components/process-failure/process-failure';
import { ProcessFailureModalComponent } from './components/_modals/process-failure-modal/process-failure-modal';
import { NextStepsComponent } from './components/submit-incidence/next-steps/next-steps';
import { IncidenceDocumentationComponent } from './components/submit-incidence/incidence-documentation/incidence-documentation';
import { IncidenceSummaryComponent } from './components/submit-incidence/incidence-summary/incidence-summary';
import { CloseSessionModalComponent } from './components/_modals/_user/close-session-modal/close-session-modal';
import { DocumentAdhocModalComponent } from './components/_modals/document-adhoc-modal/document-adhoc-modal';
import { CallMeModalComponent } from './components/_modals/_contact/call-me-modal/call-me-modal';
import { HtmlContentModalComponent } from './components/_modals/html-content-modal/html-content-modal';
import { HappyOrNotModalComponent } from './components/_modals/_claims/happy-or-not-modal/happy-or-not-modal';
import { AssistanceCancellationModalComponent } from './components/_modals/_assistances/assistance-cancellation-modal/assistance-cancellation-modal';
import { AssistanceSuccessComponent } from './components/_assistances/request-assistance/assistance-success/assistance-success';
import { OpenAssistanceSuccessComponent } from './components/_assistances/request-assistance/open-assistance-success/open-assistance-success';
import { AssistanceCardComponent } from './components/_assistances/assistance-card/assistance-card';
import { PopoverTooltipComponent } from './components/popover-tooltip/popover-tooltip';
import { AssistanceIssuesComponent } from './components/_assistances/assistance-issues/assistance-issues';
import { AssistancesComponent } from './components/_assistances/assistances/assistances';
import { CookiesAdviceComponent } from './components/cookies-advice/cookies-advice';
import { MapsLocationSelectorComponent } from './components/maps-location-selector/maps-location-selector';
import { UpgradeInsuranceUpgradeSelectionComponent } from './components/upgrade-insurance/upgrade-insurance-upgrade-selection/upgrade-insurance-upgrade-selection';
import { UpgradeInsuranceInsuranceTypeCardComponent } from './components/upgrade-insurance/upgrade-insurance-insurance-type-card/upgrade-insurance-insurance-type-card';
import { UpgradeInsuranceConfirmationComponent } from './components/upgrade-insurance/upgrade-insurance-confirmation/upgrade-insurance-confirmation';
import { UpgradeInsuranceResultComponent } from './components/upgrade-insurance/upgrade-insurance-result/upgrade-insurance-result';
import { MyIncidencesComponent } from './components/incidence-management/my-incidences/my-incidences';
import { MyClaimsComponent } from './components/incidence-management/my-claims/my-claims';
import { IncidenceDetailComponent } from './components/incidence-management/incidence-detail/incidence-detail';
import { ClaimDetailComponent } from './components/incidence-management/claim-detail/claim-detail';
import { WarningAdvertisementComponent } from './components/warning-footer/warnings-advertisement/warnings-advertisement';
import { InterestInformationHeaderComponent } from './components/interest-information/interest-information-header/interest-information-header';
import { OfflineModalComponent } from './components/_modals/offline-modal/offline-modal';
import { DownloadItemComponent } from './components/download/download-item/download-item';
import { PopoverDownloadItemComponent } from './components/download/download-item/popover-download-item/popover-download-item';
import { VehiclePolicySelectorComponent } from './components/_assistances/request-assistance/vehicle-policy-selector/vehicle-policy-selector';
import { YourOffersComponent } from './components/_offers/your-offers/your-offers';
import { OffersListComponent } from './components/_offers/offers-list/offers-list';
import { OffersBannerComponent } from './components/_offers/offers-banner/offers-banner';
import { OffersDetailDescriptionComponent } from './components/_offers/_detail/offers-detail-description/offers-detail-description';
import { OffersDetailListBenefitsComponent } from './components/_offers/_detail/offers-detail-list-benefits/offers-detail-list-benefits';
import { OffersDetailListTermsComponent } from './components/_offers/_detail/offers-detail-list-terms/offers-detail-list-terms';
import { OfferCardComponent } from './components/_offers/offer-card/offer-card';
import { RenovationModalComponent } from './components/_modals/renovation-modal/renovation-modal';
import { SinisterClosedComponent } from './components/sinisters/tracings/sinister-closed/sinister-closed';
import { AccountDataComponent } from './components/personal-info/account-data/account-data';
import { PersonalDataComponent } from './components/personal-info/personal-data/personal-data';
import { ContactDataComponent } from './components/personal-info/contact-data/contact-data';
import { WelcomePackLinkCardComponent } from './components/personal-info/welcome-pack-link-card/welcome-pack-link-card';
import { PasswordAndSecurityQuestionsChangeComponent } from './components/personal-info/password-and-security-questions-change/password-and-security-questions-change';
import { ChangedPasswordModalComponent } from './components/_modals/_user/changed-password-modal/changed-password-modal';
import { ChangedPinModalComponent } from './components/_modals/_user/changed-pin-modal/changed-pin-modal';
import { ChangedSecurityQuestionModalComponent } from './components/_modals/_user/changed-security-question-modal/changed-security-question-modal';
import { ChangesSuccessModalComponent } from './components/_modals/_user/changes-success-modal/changes-success-modal';
import { LanguageChangedSuccessModalComponent } from './components/_modals/language-changed-success-modal/language-changed-success-modal';
import { HelpComponent } from './components/help/help';
import { TabLinksComponent } from './components/tab-links/tab-links';
import { WelcomePackModalComponent } from './components/_modals/welcome-pack-modal/welcome-pack-modal';
import { MapfreMenu } from './components/mapfre-menu-2/mapfre-menu-2';
import { BudgetsComponent } from './components/budgets/budgets';
import { BudgetsV1Component } from './components/budgets-v1/budgets-v1';
import { FooterModalComponent } from './components/_modals/footer-modal/footer-modal';
import { MailMeModalComponent } from './components/_modals/_contact/mail-me-modal/mail-me-modal';
import { ProfileImageComponent } from './components/personal-info/profile-image/profile-image';
import { ProfileImageModalComponent } from './components/_modals/_user/profile-image-modal/profile-image-modal';
import { GrantedPermissionsComponent } from './components/personal-info/granted-permissions/granted-permissions';
import { UserRelationshipsComponent } from './components/personal-info/user-relationships/user-relationships';
import { PaymentMethodsComponent } from './components/personal-info/payment-methods/payment-methods';
import { PinLoginComponent } from './components/pin-login/pin-login';
import { IonDigitKeyboard } from './components/ion-digit-keyboard/ion-digit-keyboard';
import { DownloadAppNotificationComponent } from './components/download-app-notification/download-app-notification';
import { SignMethodsComponent } from './components/personal-info/sign-methods/sign-methods';
import { NewCommunicationsComponent } from './components/personal-info/new-communications/new-communications';
import { AddReceiptsToPayModalComponent } from './components/_modals/_receipts/add-receipts-to-pay-modal/add-receipts-to-pay-modal';
import { PopoverReceiptPaymentComponent } from './components/insurance-detail/invoice-list/popover-receipt-payment/popover-receipt-payment';
import { ReceiptFractionateQuestionComponent } from './components/insurance-detail/receipt-fractionate-question/receipt-fractionate-question';
import { PaymentGroupingComponent } from './components/payment-grouping/payment-grouping';
import { FileUploaderComponent } from './components/file-uploader/file-uploader';
import { FileUploaderModalComponent } from './components/_modals/file-uploader-modal/file-uploader-modal';
import { FileUploaderCardComponent } from './components/file-uploader/card/file-uploader-card';
import { NewPaymentMethodFormComponent } from './components/new-payment-method-form/new-payment-method-form';
import { FractionamentTypeSelectionComponent } from './components/fractionament-type-selection/fractionament-type-selection';
import { PolicyExtensionComponent } from './components/policy-extension/policy-extension';
import { PaymentPlanComponent } from './components/payment-plan/payment-plan';
import { SummaryComponent } from './components/summary/summary';
import { SplitAmountComponent } from './components/split-amount/split-amount';
import { AmountDivisionsModalComponent } from './components/_modals/_receipts/amount-divisions-modal/amount-divisions-modal';
import { InformationModalComponent } from './components/_modals/information-modal/information-modal';
import { ReasonsCommentsComponent } from './components/reasons-comments/reasons-comments';
import { CancellationDateComponent } from './components/cancellation-date/cancellation-date';
import { PolicyModalitiesComparatorComponent } from './components/policy-modalities-comparator/policy-modalities-comparator';
import { PolicyCardComponent } from './components/policy-card/policy-card';
import { OpenPaymentModalComponent } from './components/_modals/_receipts/open-payment-modal/open-payment-modal';
import { OpenPaymentSmsConfirmationComponent } from './components/open-payment/open-payment-sms-confirmation/open-payment-sms-confirmation';
import { ProcessPolicyChangeFailureModalComponent } from './components/_modals/process-policy-change-failure-modal/process-policy-change-failure-modal';
import { PaymentReceiptsComponent } from './components/insurance-detail/payment-receipts/payment-receipts';
import { EmptySubmitIncidenceComponent } from './components/insurance-detail/empty-submit-incidence/empty-submit-incidence';
import { ConditionsAcceptModalComponent } from './components/_modals/_claims/conditions-accept-modal/conditions-accept-modal';
import { IndemnifyDetailModalComponent } from './components/_modals/_claims/indemnify-detail-modal/indemnify-detail-modal';
import { CancelSinisterModalComponent } from './components/_modals/_claims/cancel-sinister-modal/cancel-sinister-modal';
import { UrgentAssistanceModalComponent } from './components/_modals/_assistances/urgent-assistance-modal/urgent-assistance-modal';
import { NoticeNonCoverageModalComponent } from './components/_modals/_claims/notice-non-coverage-modal/notice-non-coverage-modal';
import { ProfessionalAppointmentComponent } from './components/professional-appointment/professional-appointment';
import { ServicesManagementComponent } from './components/services-management/services-management';
import { CoverageNotIncludedComponent } from './components/coverage-not-included/coverage-not-included';
import { SubmitIncidenceInjuredConfirmationComponent } from './components/submit-incidence/submit-incidence-injured-confirmation/submit-incidence-injured-confirmation';
import { RefuseRenewalModalComponent } from './components/_modals/_policies/refuse-renewal-modal/refuse-renewal-modal';
import { InformationComponent } from './components/information/information';
import { AssistanceTypeHomeComponent } from './components/_assistances/request-assistance/assistance-type-home/assistance-type-home';
import { NotIncludeCoveragesModalComponent } from './components/_modals/_assistances/not-include-coverages-modal/not-include-coverages-modal';
import { RequestAssistanceCancelModalComponent } from './components/_modals/_assistances/request-assistance-cancel-modal/request-assistance-cancel-modal';
import { MyLocationComponent } from './components/my-location/my-location';
import { CalendarMeetingModalComponent } from './components/_modals/calendar-meeting-modal/calendar-meeting-modal';
import { SaveIncidenceModalComponent } from './components/_modals/save-incidence-modal/save-incidence-modal';
import { BackWarningModalComponent } from './components/_modals/back-warning-modal/back-warning-modal';
import { PageHeaderComponent } from './components/page-header/page-header';
import { FractionamentPaymentResultComponent } from './components/fractionament-payment-result/fractionament-payment-result';
import { PolicyExtensionModalComponent } from './components/_modals/_policies/policy-extension-modal/policy-extension-modal';
import { ProcessConfirmationModalComponent } from './components/_modals/process-confirmation-modal/process-confirmation-modal';
import { UserWelcomeComponent } from './components/user-welcome/user-welcome';
import { PageBackgroundComponent } from './components/page-background/page-background';
import { PageTitleComponent } from './components/page-title/page-title';
import { AttendanceTrackingModalComponent } from './components/_modals/_assistances/attendance-tracking-modal/attendance-tracking-modal';
import { AttendanceTrackingComponent } from './components/attendance-tracking/attendance-tracking';
import { AssistanceTypeSelectionComponent } from './components/request-assistance/assistance-type-selection/assistance-type-selection';
import { AssistanceRiskSelectionComponent } from './components/request-assistance/assistance-risk-selection/assistance-risk-selection';
import { AssistanceMotionSelectionComponent } from './components/request-assistance/assistance-motion-selection/assistance-motion-selection';
import { SubmitIncidenceAdjusterComponent } from './components/submit-incidence/submit-incidence-adjuster/submit-incidence-adjuster';
import { SubmitIncidenceSubtypeComponent } from './components/submit-incidence/submit-incidence-subtype/submit-incidence-subtype';
import { SubmitIncidenceTypeComponent } from './components/submit-incidence/submit-incidence-type/submit-incidence-type';
import { SubmitIncidenceInjuriesComponent } from './components/submit-incidence/submit-incidence-injuries/submit-incidence-injuries';
import { SubmitIncidenceInjuriesDetailComponent } from './components/submit-incidence/submit-incidence-injuries-detail/submit-incidence-injuries-detail';
import { SubmitIncidenceWhenComponent } from './components/submit-incidence/submit-incidence-when/submit-incidence-when';
import { SubmitIncidenceHowComponent } from './components/submit-incidence/submit-incidence-when-where-and-how/submit-incidence-how/submit-incidence-how';
import { OffersDetailHeaderComponent } from './components/_offers/_detail/offers-detail-header/offers-detail-header';
import { PillTabsComponent } from './components/pill-tabs/pill-tabs';
import { ReceiptSelectionComponent } from './components/receipt-selection/receipt-selection';
import { ConfirmationCheckComponent } from './components/confirmation-check/confirmation-check';
import { ComponentCreatorService } from './providers/component-creator/component-creator.service';
import { DocumentShareModalComponent } from './components/_modals/document-share-modal/document-share-modal';
import { IconSelectorComponent } from './components/icon-selector/icon-selector';
import { LegalRegulationsDocumentationComponent } from './components/legal-regulations-documentation/legal-regulations-documentation';
import { ServiceManagementComponent } from './components/service-management/service-management';
import { HtmlComponent } from './components/html-component/html-component';
import { RichTextComponent } from './components/rich-text-component/rich-text-component';
import { SafeHtmlPipe } from './pipes/safe-html/safe-html';
import { InsuranceSelectionV2Component } from './components/submit-incidence/insurance-selection-v2/insurance-selection-v2';
import { BenefitsBannerComponent } from './components/benefits-banner/benefits-banner';
import { BudgetSelectionComponent } from './components/potential-client/budget-selection/budget-selection';
import { PotentialBudgetDetailsComponent } from './components/potential-client/potential-budget-details/potential-budget-details';
import { NearestOfficeComponent } from './components/nearest-office/nearest-office';
import { ImportantBudgetInformationComponent } from './components/potential-client/important-budget-information/important-budget-information';
import { PotentialClientConfirmationComponent } from './components/potential-client/potential-client-confirmation/potential-client-confirmation';
import { PotentialClientCallmeComponent } from './components/potential-client/potential-client-callme/potential-client-callme';
import { PotentialClientAdditionalDataComponent } from './components/potential-client/potential-client-additional-data/potential-client-additional-data';
import { PotentialClientPrivateAreaDataComponent } from './components/potential-client/potential-client-private-area-data/potential-client-private-area-data';
import { UpgradeInsuranceInsuranceTypeCardV1Component } from './components/upgrade-insurance/upgrade-insurance-insurance-type-card-v1/upgrade-insurance-insurance-type-card-v1';
import { OpenPaymentModalV2Component } from './components/_modals/_receipts/open-payment-modal-v2/open-payment-modal-v2';
import { LoginQuickManagementV2Component } from './components/login/login-quick-management-v2/login-quick-management-v2';
import { PaymentMethodSelectionV1Component } from './components/payment-methods-selection-v1/payment-methods-selection-v1';
import { NewPaymentMethodFormV1Component } from './components/new-payment-method-form-v1/new-payment-method-form-v1';
import { UpgradeInsuranceUpgradeSelectionV1Component } from './components/upgrade-insurance/upgrade-insurance-upgrade-selection-v1/upgrade-insurance-upgrade-selection-v1';
import { LossNoClientComponent } from './components/insurance-detail/loss-list/loss-no-client/loss-no-client';
import { SummaryV2Component } from './components/summary-v2/summary-v2';
import { OpenPaymentEmailConfirmationComponent } from './components/open-payment/open-payment-email-confirmation/open-payment-email-confirmation';
import { OpenPaymentCallMeComponent } from './components/open-payment/open-payment-call-me/open-payment-call-me';
import { CoverageImprovementSelectorComponent } from './components/coverage-improvement-selector/coverage-improvement-selector';
import { PolicyModalitiesComparatorV1Component } from './components/policy-modalities-comparator-v1/policy-modalities-comparator-v1';
import { PolicyModalitiesComparatorV1Modal } from './components/upgrade-insurance/policy-modalities-comparator-v1-modal/policy-modalities-comparator-v1-modal';
import { ContraryDamagesFormV1Component } from './components/submit-incidence/contrary-damages-form-v1/contrary-damages-form-v1';
import { UserRelationshipsV1Component } from './components/personal-info/user-relationships-v1/user-relationships-v1';
import { PaymentMethodSelectionV2Component } from './components/payment-method-selection-v2/payment-method-selection-v2';
import { ClaimFormSimpleV1Component } from './components/claim-form-simple-v1/claim-form-simple-v1';
import { PaymentMethodsV1Component } from './components/personal-info/payment-methods-v1/payment-methods-v1';
import { PaymentChooseV1Component } from './components/insurance-detail/payment-choose-v1/payment-choose-v1';
import { PaymentReceiptsV1Component } from './components/insurance-detail/payment-receipts-v1/payment-receipts-v1';
import { SubmitIncidenceTypeV1Component } from './components/submit-incidence/submit-incidence-type-v1/submit-incidence-type-v1';
import { SinisterNoClientComponent } from './components/sinister-no-client/sinister-no-client';
import { OpenSubmitIncidenceProvider } from './providers/open-submit-incidence/open-submit-incidence';
import { OpenSubmitIncidenceModalComponent } from './components/_modals/_submit-incidence/open-submit-incidence-modal/open-submit-incidence-modal';
import { PolicyCoveragesComparatorComponent } from './components/policy-coverages-comparator/policy-coverages-comparator';
import { SubheaderV1Component } from './components/subheader-v1/subheader-v1';
import { FabGroupV1Component } from './components/fab-group-v1/fab-group-v1';
import { LossDetailModal } from './components/insurance-detail/loss-list/loss-detail-modal/loss-detail-modal';
import { IncidenceFormSimpleV1Component } from './components/incidence-form-simple-v1/incidence-form-simple-v1';
import { AssistanceInsuranceSelectionComponent } from './components/request-assistance/assistance-insurance-selection/assistance-insurance-selection';
import { TitleGlobalComponent } from './components/title-global/title-global';
import { YourOffersV1Component } from './components/_offers/your-offers-v1/your-offers-v1';
var SelfComponentsModule = (function () {
    function SelfComponentsModule() {
    }
    SelfComponentsModule.forRoot = function (config) {
        return {
            ngModule: SelfComponentsModule,
            providers: [
                AppLoadService,
                ApiProvider,
                AssistanceProvider,
                ComponentCreatorService,
                FlowProvider,
                ComponentSettingsProvider,
                CurrencyPipe,
                DiscardByKeyValuePipe,
                UserProvider,
                PlatformProvider,
                FormProvider,
                FormatProvider,
                TranslationProvider,
                PageSettingsProvider,
                MenuProvider,
                DateProvider,
                FileIncidencesProvider,
                FileClaimsProvider,
                IncidenceProvider,
                FileUtils,
                Utils,
                TooltipProvider,
                ModalProvider,
                FileInsuranceProvider,
                NavigationProvider,
                TransferProvider,
                LocationProvider,
                StepsProvider,
                InsuranceUpgradeProvider,
                ReceiptPaymentProvider,
                OpenReceiptPaymentProvider,
                RequestAssistanceProvider,
                RequestAssistanceHomeProvider,
                OpenRequestAssistanceProvider,
                OpenRequestHomeAssistanceProvider,
                SubmitIncidenceProvider,
                SubmitHomeIncidenceProvider,
                IncidencesAndClaimsProvider,
                DocumentsProvider,
                InsuranceProvider,
                UserRegistrationProvider,
                SocialLoginProvider,
                InformationProvider,
                Mocks,
                UserInactiveModalProvider,
                WelcomePackProvider,
                PersonalInfoProvider,
                IdentificationProvider,
                PolicyCancellationProvider,
                AnimationsProvider,
                ReceiptFractionamentProvider,
                PolicyChangeProvider,
                OfferProvider,
                ConfigurationToolProvider,
                AppGlobalService,
                Components,
                PotentialClientProvider,
                OpenSubmitIncidenceProvider
            ]
        };
    };
    SelfComponentsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        MbscModule,
                        IonicModule,
                        ComponentsModule,
                        ImageCropperModule,
                        Ionic2RatingModule
                    ],
                    exports: [
                        SortPipe,
                        KeysPipe,
                        DiscardByKeyValuePipe,
                        CurrencyPipe,
                        MapfreFooterComponent,
                        MapfreNetworksFooterComponent,
                        ConfirmationCheckComponent,
                        SendEmailWidgetComponent,
                        YourPendingTopicsComponent,
                        YourManagerComponent,
                        DesktopStepsComponent,
                        AppointmentModalComponent,
                        LoadingSpinnerComponent,
                        IconsDisplayComponent,
                        ModalsDisplayComponent,
                        DownloadItemComponent,
                        PopoverDownloadItemComponent,
                        NotLoggedInHeaderComponent,
                        TimePickerInputComponent,
                        WorkshopDisplayComponent,
                        WorkshopSelectionComponent,
                        AdditionalWorkshopServicesComponent,
                        FuzzyDateSelectorComponent,
                        TooltipIconComponent,
                        PillComponent,
                        ForgotPasswordComponent,
                        ChangedPrivacyForgotPasswordComponent,
                        ForgotPinComponent,
                        LoginFormComponent,
                        LoginQuickManagementComponent,
                        LoginSuccessStoriesComponent,
                        LoginBannerComponent,
                        LoginVideoComponent,
                        LoginAppDownloadComponent,
                        LoginContactComponent,
                        RegistrationFormComponent,
                        SimpleRegistrationFormComponent,
                        RegistrationFormBasicComponent,
                        SocialRegistrationFormComponent,
                        UserDataFormComponent,
                        UserPictureDataFormComponent,
                        UserContactDataFormComponent,
                        UserSecurityDataFormComponent,
                        SubheaderComponent,
                        GlobalPositionNewCommunicationsComponent,
                        GlobalPositionInsuranceEntryComponent,
                        GlobalPositionHireInsuranceComponent,
                        UsualInquiriesComponent,
                        UserContactsComponent,
                        InterestInformationHeaderComponent,
                        InsuranceHeaderComponent,
                        PolicyListComponent,
                        PolicyDataComponent,
                        CarPolicyComponent,
                        HomePolicyComponent,
                        CoverageListComponent,
                        InvoiceListComponent,
                        PaymentListComponent,
                        PaymentChooseComponent,
                        PaymentMethodsComponent,
                        PolicyCancelModalComponent,
                        DocumentationListComponent,
                        InsuranceDocumentationListComponent,
                        ClaimDocumentationListComponent,
                        ProceedingDocumentationListComponent,
                        PolicyUpdateSuccessWithoutPaymentModalComponent,
                        PolicyUpdateSuccessWithPaymentModalComponent,
                        PolicyUpdateFailureModalComponent,
                        DocumentationComponent,
                        GeneralTermsComponent,
                        InformativeNotesComponent,
                        InsuranceConsortiumComponent,
                        FaqsHeaderComponent,
                        FaqsContentComponent,
                        CardFaqsComponent,
                        ReceiptSelectionComponent,
                        TitleComponent,
                        IncidenceFormComponent,
                        IncidenceFormSimpleComponent,
                        CardButtonComponent,
                        ConfirmIncidenceCardComponent,
                        ClaimFormComponent,
                        ClaimFormSimpleComponent,
                        IncidencesAndClaimsCardsComponent,
                        AssistanceInsuranceSelectionComponent,
                        InsuranceSelectionComponent,
                        OwnDamagesFormComponent,
                        ContraryDamagesFormComponent,
                        VehicleDamagedAreasComponent,
                        OtherDamagesFormComponent,
                        OtherParticipantsFormComponent,
                        IncidentHeaderComponent,
                        SubmitIncidenceCallMeBackComponent,
                        SubmitIncidenceHowOwnVehicleComponent,
                        HomeIncidenceDateSelectionComponent,
                        HomeIncidenceTypeSelectionComponent,
                        HomeIncidenceLocationCardComponent,
                        HomeIncidenceDamagedItemsCardComponent,
                        HomeIncidenceThirdPartyDamagesCardComponent,
                        HomeIncidenceDamageTypeSelectionComponent,
                        HomeIncidenceDamageReasonSelectionComponent,
                        HomeIncidenceDamageDetailComponent,
                        HomeIncidenceInjuredDataComponent,
                        YourOffersComponent,
                        OffersBannerComponent,
                        OffersDetailDescriptionComponent,
                        OffersDetailListBenefitsComponent,
                        OffersDetailListTermsComponent,
                        OfferCardComponent,
                        AutosizeDirective,
                        DndDirective,
                        HeaderGroupComponent,
                        FabGroupComponent,
                        DndClaimsDirective,
                        BlockedUserCheckMailComponent,
                        BlockedUserCheckSmsComponent,
                        BlockedUserMethodSelectionComponent,
                        BlockedUserSecurityQuestionsComponent,
                        BlockedUserPasswordChangeComponent,
                        BlockedUserPinChangeComponent,
                        ChangeBlockedUserCheckMailComponent,
                        ChangeBlockedUserCheckSmsComponent,
                        ChangeBlockedUserMethodSelectionComponent,
                        ChangeBlockedUserSecurityQuestionsComponent,
                        ChangeBlockedUserPasswordChangeComponent,
                        ChangeBlockedUserPinChangeComponent,
                        PaymentMethodSelectionComponent,
                        TypedInputComponent,
                        CallMeResponseModalComponent,
                        FormResponseModalComponent,
                        ModifyCardPopupComponent,
                        CommunicationsListComponent,
                        CommunicationsEmptyComponent,
                        CommunicationsComponent,
                        CommunicationsModalComponent,
                        IncidentHeaderComponent,
                        LossListComponent,
                        LossComponent,
                        LossDetailComponent,
                        SinisterTracingsComponent,
                        SinisterTracingListComponent,
                        SinisterTracingEntryComponent,
                        SinisterDocumentationComponent,
                        ImageSliderModalComponent,
                        ProfessionalMeetingComponent,
                        NextStepsComponent,
                        IncidenceDocumentationComponent,
                        ReportConfirmationComponent,
                        RatingComponent,
                        ProcessConfirmationComponent,
                        ProcessFailureComponent,
                        ProcessFailureModalComponent,
                        IncidenceSummaryComponent,
                        ReportConfirmationComponent,
                        IncidenceProfessionalSelectionComponent,
                        ProfessionalMeetingModalComponent,
                        ProfessionalMeetingConfirmationModalComponent,
                        CloseSessionModalComponent,
                        CalendarMeetingModalComponent,
                        DocumentAdhocModalComponent,
                        CallMeModalComponent,
                        MailMeModalComponent,
                        UrgentAssistanceModalComponent,
                        NoticeNonCoverageModalComponent,
                        ServicesManagementComponent,
                        CoverageNotIncludedComponent,
                        SubmitIncidenceInjuredConfirmationComponent,
                        WorkshopDisplayComponent,
                        HtmlContentModalComponent,
                        HappyOrNotModalComponent,
                        AssistanceCancellationModalComponent,
                        AssistanceSuccessComponent,
                        OpenAssistanceSuccessComponent,
                        AssistanceCardComponent,
                        OfflineAlertComponent,
                        OfflineModalComponent,
                        PopoverTooltipComponent,
                        PopoverDownloadItemComponent,
                        AssistanceIssuesComponent,
                        DndInsuranceDirective,
                        DynamicCreatorDirective,
                        AssistancesComponent,
                        CancelPolicyQuestionComponent,
                        CookiesAdviceComponent,
                        VideoModalComponent,
                        MapsLocationSelectorComponent,
                        UpgradeInsuranceUpgradeSelectionComponent,
                        UpgradeInsuranceInsuranceTypeCardComponent,
                        UpgradeInsuranceConfirmationComponent,
                        UpgradeInsuranceResultComponent,
                        MyIncidencesComponent,
                        MyClaimsComponent,
                        IncidenceDetailComponent,
                        ClaimDetailComponent,
                        WarningAdvertisementComponent,
                        FileUploaderComponent,
                        FileUploaderModalComponent,
                        FileUploaderCardComponent,
                        ModalsDisplayComponent,
                        RenovationModalComponent,
                        SinisterClosedComponent,
                        AccountDataComponent,
                        PersonalDataComponent,
                        ContactDataComponent,
                        WelcomePackLinkCardComponent,
                        PasswordAndSecurityQuestionsChangeComponent,
                        ChangedPasswordModalComponent,
                        ChangedPinModalComponent,
                        ChangedSecurityQuestionModalComponent,
                        ChangesSuccessModalComponent,
                        MapfreMenu,
                        LanguageChangedSuccessModalComponent,
                        HelpComponent,
                        TabLinksComponent,
                        BudgetsComponent,
                        BudgetsV1Component,
                        ProfessionalAppointmentComponent,
                        WelcomePackModalComponent,
                        FooterModalComponent,
                        ProfileImageComponent,
                        ProfileImageModalComponent,
                        GrantedPermissionsComponent,
                        UserRelationshipsComponent,
                        PinLoginComponent,
                        IonDigitKeyboard,
                        DownloadAppNotificationComponent,
                        SignMethodsComponent,
                        NewCommunicationsComponent,
                        ReasonsCommentsComponent,
                        CancellationDateComponent,
                        PolicyModalitiesComparatorComponent,
                        InformationModalComponent,
                        AddReceiptsToPayModalComponent,
                        PopoverReceiptPaymentComponent,
                        ReceiptFractionateQuestionComponent,
                        NewPaymentMethodFormComponent,
                        FractionamentTypeSelectionComponent,
                        PaymentPlanComponent,
                        PolicyExtensionComponent,
                        SummaryComponent,
                        PaymentGroupingComponent,
                        SplitAmountComponent,
                        AmountDivisionsModalComponent,
                        CancellationDateComponent,
                        PolicyModalitiesComparatorComponent,
                        ProcessPolicyChangeFailureModalComponent,
                        PolicyCardComponent,
                        OpenPaymentModalComponent,
                        OpenPaymentSmsConfirmationComponent,
                        PaymentReceiptsComponent,
                        AssistanceTypeHomeComponent,
                        NotIncludeCoveragesModalComponent,
                        RefuseRenewalModalComponent,
                        RequestAssistanceCancelModalComponent,
                        EmptySubmitIncidenceComponent,
                        ConditionsAcceptModalComponent,
                        IndemnifyDetailModalComponent,
                        CancelSinisterModalComponent,
                        InformationComponent,
                        MyLocationComponent,
                        CalendarMeetingModalComponent,
                        SaveIncidenceModalComponent,
                        BackWarningModalComponent,
                        VehiclePolicySelectorComponent,
                        PageHeaderComponent,
                        FractionamentPaymentResultComponent,
                        AssistanceTypeSelectionComponent,
                        AssistanceRiskSelectionComponent,
                        AssistanceMotionSelectionComponent,
                        PolicyExtensionModalComponent,
                        ProcessConfirmationModalComponent,
                        UserWelcomeComponent,
                        TitleGlobalComponent,
                        PageBackgroundComponent,
                        SubmitIncidenceAdjusterComponent,
                        SubmitIncidenceInjuriesComponent,
                        SubmitIncidenceInjuriesDetailComponent,
                        SubmitIncidenceSubtypeComponent,
                        SubmitIncidenceTypeComponent,
                        SubmitIncidenceWhenComponent,
                        SubmitIncidenceHowComponent,
                        PageTitleComponent,
                        AttendanceTrackingModalComponent,
                        AttendanceTrackingComponent,
                        OffersListComponent,
                        OffersDetailHeaderComponent,
                        PillTabsComponent,
                        SubmitIncidenceHowOtherVehiclesComponent,
                        DocumentShareModalComponent,
                        IconSelectorComponent,
                        LegalRegulationsDocumentationComponent,
                        ServiceManagementComponent,
                        HtmlComponent,
                        RichTextComponent,
                        BenefitsBannerComponent,
                        BudgetSelectionComponent,
                        PotentialBudgetDetailsComponent,
                        NearestOfficeComponent,
                        ImportantBudgetInformationComponent,
                        PotentialClientConfirmationComponent,
                        PotentialClientCallmeComponent,
                        PotentialClientAdditionalDataComponent,
                        PotentialClientPrivateAreaDataComponent,
                        UpgradeInsuranceInsuranceTypeCardV1Component,
                        OpenPaymentModalV2Component,
                        LoginQuickManagementV2Component,
                        PaymentMethodSelectionV1Component,
                        NewPaymentMethodFormV1Component,
                        SinisterNoClientComponent,
                        UpgradeInsuranceUpgradeSelectionV1Component,
                        LossNoClientComponent,
                        SummaryV2Component,
                        OpenPaymentEmailConfirmationComponent,
                        OpenPaymentCallMeComponent,
                        CoverageImprovementSelectorComponent,
                        PolicyModalitiesComparatorV1Component,
                        PolicyModalitiesComparatorV1Modal,
                        PolicyCoveragesComparatorComponent,
                        OpenSubmitIncidenceModalComponent,
                        ContraryDamagesFormV1Component,
                        InsuranceSelectionV2Component,
                        CoverageImprovementSelectorComponent,
                        UserRelationshipsV1Component,
                        SubheaderV1Component,
                        PaymentMethodSelectionV2Component,
                        FabGroupV1Component,
                        ClaimFormSimpleV1Component,
                        LossDetailModal,
                        PaymentMethodsV1Component,
                        PaymentChooseV1Component,
                        PaymentReceiptsV1Component,
                        SubmitIncidenceTypeV1Component,
                        IncidenceFormSimpleV1Component,
                        YourOffersV1Component
                    ],
                    declarations: [
                        SortPipe,
                        KeysPipe,
                        DiscardByKeyValuePipe,
                        SafeHtmlPipe,
                        MapfreFooterComponent,
                        ConfirmationCheckComponent,
                        SendEmailWidgetComponent,
                        MapfreNetworksFooterComponent,
                        YourPendingTopicsComponent,
                        YourManagerComponent,
                        DesktopStepsComponent,
                        AppointmentModalComponent,
                        LoadingSpinnerComponent,
                        IconsDisplayComponent,
                        ModalsDisplayComponent,
                        DownloadItemComponent,
                        PopoverDownloadItemComponent,
                        NotLoggedInHeaderComponent,
                        TimePickerInputComponent,
                        WorkshopDisplayComponent,
                        WorkshopSelectionComponent,
                        AdditionalWorkshopServicesComponent,
                        FuzzyDateSelectorComponent,
                        TooltipIconComponent,
                        PillComponent,
                        ForgotPasswordComponent,
                        ChangedPrivacyForgotPasswordComponent,
                        ForgotPinComponent,
                        LoginFormComponent,
                        LoginQuickManagementComponent,
                        LoginSuccessStoriesComponent,
                        LoginBannerComponent,
                        LoginVideoComponent,
                        LoginAppDownloadComponent,
                        LoginContactComponent,
                        RegistrationFormComponent,
                        SimpleRegistrationFormComponent,
                        RegistrationFormBasicComponent,
                        SocialRegistrationFormComponent,
                        UserDataFormComponent,
                        UserPictureDataFormComponent,
                        UserContactDataFormComponent,
                        UserSecurityDataFormComponent,
                        SubheaderComponent,
                        GlobalPositionNewCommunicationsComponent,
                        GlobalPositionInsuranceEntryComponent,
                        GlobalPositionHireInsuranceComponent,
                        UsualInquiriesComponent,
                        UserContactsComponent,
                        InterestInformationHeaderComponent,
                        InsuranceHeaderComponent,
                        PolicyListComponent,
                        PolicyDataComponent,
                        CarPolicyComponent,
                        HomePolicyComponent,
                        CoverageListComponent,
                        InvoiceListComponent,
                        PaymentListComponent,
                        PaymentChooseComponent,
                        PaymentMethodsComponent,
                        PolicyCancelModalComponent,
                        DocumentationListComponent,
                        InsuranceDocumentationListComponent,
                        ClaimDocumentationListComponent,
                        ProceedingDocumentationListComponent,
                        PolicyUpdateSuccessWithoutPaymentModalComponent,
                        PolicyUpdateSuccessWithPaymentModalComponent,
                        PolicyUpdateFailureModalComponent,
                        DocumentationComponent,
                        GeneralTermsComponent,
                        InformativeNotesComponent,
                        InsuranceConsortiumComponent,
                        FaqsHeaderComponent,
                        FaqsContentComponent,
                        CardFaqsComponent,
                        TitleComponent,
                        IncidenceFormComponent,
                        IncidenceFormSimpleComponent,
                        CardButtonComponent,
                        ConfirmIncidenceCardComponent,
                        ClaimFormComponent,
                        ClaimFormSimpleComponent,
                        IncidencesAndClaimsCardsComponent,
                        AssistanceInsuranceSelectionComponent,
                        InsuranceSelectionComponent,
                        OwnDamagesFormComponent,
                        ContraryDamagesFormComponent,
                        VehicleDamagedAreasComponent,
                        OtherDamagesFormComponent,
                        OtherParticipantsFormComponent,
                        IncidentHeaderComponent,
                        SubmitIncidenceCallMeBackComponent,
                        SubmitIncidenceHowOtherVehiclesComponent,
                        SubmitIncidenceHowOwnVehicleComponent,
                        HomeIncidenceDateSelectionComponent,
                        HomeIncidenceTypeSelectionComponent,
                        HomeIncidenceLocationCardComponent,
                        HomeIncidenceDamagedItemsCardComponent,
                        HomeIncidenceThirdPartyDamagesCardComponent,
                        HomeIncidenceDamageTypeSelectionComponent,
                        HomeIncidenceDamageReasonSelectionComponent,
                        HomeIncidenceDamageDetailComponent,
                        HomeIncidenceInjuredDataComponent,
                        YourOffersComponent,
                        OffersBannerComponent,
                        OffersDetailDescriptionComponent,
                        OffersDetailListBenefitsComponent,
                        OffersDetailListTermsComponent,
                        OfferCardComponent,
                        AutosizeDirective,
                        DndDirective,
                        HeaderGroupComponent,
                        FabGroupComponent,
                        DndClaimsDirective,
                        BlockedUserCheckMailComponent,
                        BlockedUserCheckSmsComponent,
                        BlockedUserMethodSelectionComponent,
                        BlockedUserSecurityQuestionsComponent,
                        BlockedUserPasswordChangeComponent,
                        BlockedUserPinChangeComponent,
                        ChangeBlockedUserCheckMailComponent,
                        ChangeBlockedUserCheckSmsComponent,
                        ChangeBlockedUserMethodSelectionComponent,
                        ChangeBlockedUserSecurityQuestionsComponent,
                        ChangeBlockedUserPasswordChangeComponent,
                        ChangeBlockedUserPinChangeComponent,
                        PaymentMethodSelectionComponent,
                        TypedInputComponent,
                        CallMeResponseModalComponent,
                        FormResponseModalComponent,
                        ModifyCardPopupComponent,
                        CommunicationsListComponent,
                        CommunicationsEmptyComponent,
                        CommunicationsComponent,
                        CommunicationsModalComponent,
                        IncidentHeaderComponent,
                        LossListComponent,
                        LossComponent,
                        LossDetailComponent,
                        SinisterTracingsComponent,
                        SinisterTracingListComponent,
                        SinisterTracingEntryComponent,
                        SinisterDocumentationComponent,
                        ImageSliderModalComponent,
                        ProfessionalMeetingComponent,
                        NextStepsComponent,
                        IncidenceDocumentationComponent,
                        ReportConfirmationComponent,
                        RatingComponent,
                        ProcessConfirmationComponent,
                        ProcessFailureComponent,
                        ReceiptSelectionComponent,
                        ProcessFailureModalComponent,
                        IncidenceSummaryComponent,
                        ReportConfirmationComponent,
                        IncidenceProfessionalSelectionComponent,
                        ProfessionalMeetingModalComponent,
                        ProfessionalMeetingConfirmationModalComponent,
                        CloseSessionModalComponent,
                        DocumentAdhocModalComponent,
                        CallMeModalComponent,
                        MailMeModalComponent,
                        UrgentAssistanceModalComponent,
                        NoticeNonCoverageModalComponent,
                        ServicesManagementComponent,
                        CoverageNotIncludedComponent,
                        SubmitIncidenceInjuredConfirmationComponent,
                        WorkshopDisplayComponent,
                        HtmlContentModalComponent,
                        HappyOrNotModalComponent,
                        AssistanceCancellationModalComponent,
                        AssistanceSuccessComponent,
                        OpenAssistanceSuccessComponent,
                        AssistanceCardComponent,
                        OfflineAlertComponent,
                        OfflineModalComponent,
                        PopoverTooltipComponent,
                        PopoverDownloadItemComponent,
                        AssistanceIssuesComponent,
                        DndInsuranceDirective,
                        DynamicCreatorDirective,
                        AssistancesComponent,
                        CancelPolicyQuestionComponent,
                        CookiesAdviceComponent,
                        VideoModalComponent,
                        MapsLocationSelectorComponent,
                        UpgradeInsuranceUpgradeSelectionComponent,
                        UpgradeInsuranceInsuranceTypeCardComponent,
                        UpgradeInsuranceConfirmationComponent,
                        UpgradeInsuranceResultComponent,
                        MyIncidencesComponent,
                        MyClaimsComponent,
                        IncidenceDetailComponent,
                        ClaimDetailComponent,
                        WarningAdvertisementComponent,
                        FileUploaderComponent,
                        FileUploaderModalComponent,
                        FileUploaderCardComponent,
                        ModalsDisplayComponent,
                        RenovationModalComponent,
                        SinisterClosedComponent,
                        AccountDataComponent,
                        PersonalDataComponent,
                        ContactDataComponent,
                        WelcomePackLinkCardComponent,
                        PasswordAndSecurityQuestionsChangeComponent,
                        ChangedPasswordModalComponent,
                        ChangedPinModalComponent,
                        ChangedSecurityQuestionModalComponent,
                        ChangesSuccessModalComponent,
                        MapfreMenu,
                        LanguageChangedSuccessModalComponent,
                        HelpComponent,
                        TabLinksComponent,
                        BudgetsComponent,
                        BudgetsV1Component,
                        ProfessionalAppointmentComponent,
                        WelcomePackModalComponent,
                        FooterModalComponent,
                        ProfileImageComponent,
                        ProfileImageModalComponent,
                        GrantedPermissionsComponent,
                        UserRelationshipsComponent,
                        PinLoginComponent,
                        IonDigitKeyboard,
                        DownloadAppNotificationComponent,
                        SignMethodsComponent,
                        NewCommunicationsComponent,
                        ReasonsCommentsComponent,
                        CancellationDateComponent,
                        PolicyModalitiesComparatorComponent,
                        InformationModalComponent,
                        AddReceiptsToPayModalComponent,
                        PopoverReceiptPaymentComponent,
                        ReceiptFractionateQuestionComponent,
                        NewPaymentMethodFormComponent,
                        FractionamentTypeSelectionComponent,
                        PaymentPlanComponent,
                        PolicyExtensionComponent,
                        SummaryComponent,
                        PaymentGroupingComponent,
                        SplitAmountComponent,
                        AmountDivisionsModalComponent,
                        CancellationDateComponent,
                        PolicyModalitiesComparatorComponent,
                        ProcessPolicyChangeFailureModalComponent,
                        PolicyCardComponent,
                        OpenPaymentModalComponent,
                        OpenPaymentSmsConfirmationComponent,
                        PaymentReceiptsComponent,
                        AssistanceTypeHomeComponent,
                        NotIncludeCoveragesModalComponent,
                        RefuseRenewalModalComponent,
                        InformationComponent,
                        RequestAssistanceCancelModalComponent,
                        EmptySubmitIncidenceComponent,
                        ConditionsAcceptModalComponent,
                        IndemnifyDetailModalComponent,
                        CancelSinisterModalComponent,
                        InformationComponent,
                        MyLocationComponent,
                        CalendarMeetingModalComponent,
                        SaveIncidenceModalComponent,
                        BackWarningModalComponent,
                        VehiclePolicySelectorComponent,
                        PageHeaderComponent,
                        FractionamentPaymentResultComponent,
                        AssistanceTypeSelectionComponent,
                        AssistanceRiskSelectionComponent,
                        AssistanceMotionSelectionComponent,
                        PolicyExtensionModalComponent,
                        ProcessConfirmationModalComponent,
                        UserWelcomeComponent,
                        TitleGlobalComponent,
                        PageBackgroundComponent,
                        SubmitIncidenceAdjusterComponent,
                        SubmitIncidenceInjuriesComponent,
                        SubmitIncidenceInjuriesDetailComponent,
                        SubmitIncidenceSubtypeComponent,
                        SubmitIncidenceTypeComponent,
                        SubmitIncidenceWhenComponent,
                        SubmitIncidenceHowComponent,
                        PageTitleComponent,
                        AttendanceTrackingModalComponent,
                        AttendanceTrackingComponent,
                        OffersListComponent,
                        OffersDetailHeaderComponent,
                        PillTabsComponent,
                        DocumentShareModalComponent,
                        IconSelectorComponent,
                        LegalRegulationsDocumentationComponent,
                        ServiceManagementComponent,
                        HtmlComponent,
                        RichTextComponent,
                        BenefitsBannerComponent,
                        BudgetSelectionComponent,
                        NearestOfficeComponent,
                        ImportantBudgetInformationComponent,
                        PotentialBudgetDetailsComponent,
                        PotentialClientConfirmationComponent,
                        PotentialClientCallmeComponent,
                        PotentialClientAdditionalDataComponent,
                        PotentialClientPrivateAreaDataComponent,
                        UpgradeInsuranceInsuranceTypeCardV1Component,
                        OpenPaymentModalV2Component,
                        LoginQuickManagementV2Component,
                        PaymentMethodSelectionV1Component,
                        NewPaymentMethodFormV1Component,
                        SinisterNoClientComponent,
                        UpgradeInsuranceUpgradeSelectionV1Component,
                        LossNoClientComponent,
                        SummaryV2Component,
                        OpenPaymentEmailConfirmationComponent,
                        OpenPaymentCallMeComponent,
                        CoverageImprovementSelectorComponent,
                        PolicyModalitiesComparatorV1Component,
                        PolicyModalitiesComparatorV1Modal,
                        PolicyCoveragesComparatorComponent,
                        OpenSubmitIncidenceModalComponent,
                        ContraryDamagesFormV1Component,
                        InsuranceSelectionV2Component,
                        CoverageImprovementSelectorComponent,
                        UserRelationshipsV1Component,
                        SubheaderV1Component,
                        PaymentMethodSelectionV2Component,
                        FabGroupV1Component,
                        ClaimFormSimpleV1Component,
                        LossDetailModal,
                        PaymentMethodsV1Component,
                        PaymentChooseV1Component,
                        SubmitIncidenceTypeV1Component,
                        PaymentReceiptsV1Component,
                        IncidenceFormSimpleV1Component,
                        YourOffersV1Component
                    ],
                    entryComponents: [
                        MapfreFooterComponent,
                        ConfirmationCheckComponent,
                        SendEmailWidgetComponent,
                        MapfreNetworksFooterComponent,
                        YourPendingTopicsComponent,
                        YourManagerComponent,
                        DesktopStepsComponent,
                        AppointmentModalComponent,
                        LoadingSpinnerComponent,
                        IconsDisplayComponent,
                        ModalsDisplayComponent,
                        DownloadItemComponent,
                        PopoverDownloadItemComponent,
                        NotLoggedInHeaderComponent,
                        TimePickerInputComponent,
                        WorkshopDisplayComponent,
                        WorkshopSelectionComponent,
                        AdditionalWorkshopServicesComponent,
                        FuzzyDateSelectorComponent,
                        TooltipIconComponent,
                        PillComponent,
                        ForgotPasswordComponent,
                        ChangedPrivacyForgotPasswordComponent,
                        ForgotPinComponent,
                        LoginFormComponent,
                        LoginQuickManagementComponent,
                        LoginSuccessStoriesComponent,
                        LoginBannerComponent,
                        LoginVideoComponent,
                        LoginAppDownloadComponent,
                        LoginContactComponent,
                        RegistrationFormComponent,
                        SimpleRegistrationFormComponent,
                        RegistrationFormBasicComponent,
                        SocialRegistrationFormComponent,
                        UserDataFormComponent,
                        UserPictureDataFormComponent,
                        UserContactDataFormComponent,
                        UserSecurityDataFormComponent,
                        SubheaderComponent,
                        GlobalPositionNewCommunicationsComponent,
                        GlobalPositionInsuranceEntryComponent,
                        GlobalPositionHireInsuranceComponent,
                        UsualInquiriesComponent,
                        UserContactsComponent,
                        InterestInformationHeaderComponent,
                        InsuranceHeaderComponent,
                        PolicyListComponent,
                        PolicyDataComponent,
                        CarPolicyComponent,
                        HomePolicyComponent,
                        CoverageListComponent,
                        InvoiceListComponent,
                        PaymentListComponent,
                        PaymentChooseComponent,
                        PaymentMethodsComponent,
                        PolicyCancelModalComponent,
                        DocumentationListComponent,
                        InsuranceDocumentationListComponent,
                        ClaimDocumentationListComponent,
                        ProceedingDocumentationListComponent,
                        PolicyUpdateSuccessWithoutPaymentModalComponent,
                        PolicyUpdateSuccessWithPaymentModalComponent,
                        PolicyUpdateFailureModalComponent,
                        DocumentationComponent,
                        GeneralTermsComponent,
                        InformativeNotesComponent,
                        InsuranceConsortiumComponent,
                        FaqsHeaderComponent,
                        FaqsContentComponent,
                        CardFaqsComponent,
                        TitleComponent,
                        IncidenceFormComponent,
                        IncidenceFormSimpleComponent,
                        CardButtonComponent,
                        ConfirmIncidenceCardComponent,
                        ClaimFormComponent,
                        ClaimFormSimpleComponent,
                        IncidencesAndClaimsCardsComponent,
                        AssistanceInsuranceSelectionComponent,
                        InsuranceSelectionComponent,
                        OwnDamagesFormComponent,
                        ContraryDamagesFormComponent,
                        VehicleDamagedAreasComponent,
                        OtherDamagesFormComponent,
                        OtherParticipantsFormComponent,
                        IncidentHeaderComponent,
                        SubmitIncidenceCallMeBackComponent,
                        SubmitIncidenceHowOwnVehicleComponent,
                        HomeIncidenceDateSelectionComponent,
                        HomeIncidenceTypeSelectionComponent,
                        HomeIncidenceLocationCardComponent,
                        HomeIncidenceDamagedItemsCardComponent,
                        HomeIncidenceThirdPartyDamagesCardComponent,
                        HomeIncidenceDamageTypeSelectionComponent,
                        HomeIncidenceDamageReasonSelectionComponent,
                        HomeIncidenceDamageDetailComponent,
                        HomeIncidenceInjuredDataComponent,
                        YourOffersComponent,
                        OffersBannerComponent,
                        OffersDetailDescriptionComponent,
                        OffersDetailListBenefitsComponent,
                        OffersDetailListTermsComponent,
                        OfferCardComponent,
                        HeaderGroupComponent,
                        FabGroupComponent,
                        BlockedUserCheckMailComponent,
                        BlockedUserCheckSmsComponent,
                        BlockedUserMethodSelectionComponent,
                        BlockedUserSecurityQuestionsComponent,
                        BlockedUserPasswordChangeComponent,
                        BlockedUserPinChangeComponent,
                        ChangeBlockedUserCheckMailComponent,
                        ChangeBlockedUserCheckSmsComponent,
                        ChangeBlockedUserMethodSelectionComponent,
                        ChangeBlockedUserSecurityQuestionsComponent,
                        ChangeBlockedUserPasswordChangeComponent,
                        ChangeBlockedUserPinChangeComponent,
                        PaymentMethodSelectionComponent,
                        TypedInputComponent,
                        CallMeResponseModalComponent,
                        FormResponseModalComponent,
                        ModifyCardPopupComponent,
                        CommunicationsListComponent,
                        CommunicationsEmptyComponent,
                        CommunicationsComponent,
                        CommunicationsModalComponent,
                        IncidentHeaderComponent,
                        LossListComponent,
                        LossComponent,
                        LossDetailComponent,
                        SinisterTracingsComponent,
                        SinisterTracingListComponent,
                        SinisterTracingEntryComponent,
                        SinisterDocumentationComponent,
                        ImageSliderModalComponent,
                        ProfessionalMeetingComponent,
                        NextStepsComponent,
                        IncidenceDocumentationComponent,
                        ReportConfirmationComponent,
                        RatingComponent,
                        ProcessConfirmationComponent,
                        ProcessFailureComponent,
                        ProcessFailureModalComponent,
                        IncidenceSummaryComponent,
                        ReportConfirmationComponent,
                        IncidenceProfessionalSelectionComponent,
                        ProfessionalMeetingModalComponent,
                        ProfessionalMeetingConfirmationModalComponent,
                        CloseSessionModalComponent,
                        CalendarMeetingModalComponent,
                        DocumentAdhocModalComponent,
                        CallMeModalComponent,
                        MailMeModalComponent,
                        UrgentAssistanceModalComponent,
                        NoticeNonCoverageModalComponent,
                        ServicesManagementComponent,
                        CoverageNotIncludedComponent,
                        SubmitIncidenceInjuredConfirmationComponent,
                        WorkshopDisplayComponent,
                        HtmlContentModalComponent,
                        HappyOrNotModalComponent,
                        AssistanceCancellationModalComponent,
                        AssistanceSuccessComponent,
                        OpenAssistanceSuccessComponent,
                        AssistanceCardComponent,
                        OfflineAlertComponent,
                        OfflineModalComponent,
                        PopoverTooltipComponent,
                        PopoverDownloadItemComponent,
                        AssistanceIssuesComponent,
                        AssistancesComponent,
                        CancelPolicyQuestionComponent,
                        CookiesAdviceComponent,
                        VideoModalComponent,
                        MapsLocationSelectorComponent,
                        UpgradeInsuranceUpgradeSelectionComponent,
                        UpgradeInsuranceInsuranceTypeCardComponent,
                        UpgradeInsuranceConfirmationComponent,
                        UpgradeInsuranceResultComponent,
                        MyIncidencesComponent,
                        MyClaimsComponent,
                        IncidenceDetailComponent,
                        ClaimDetailComponent,
                        WarningAdvertisementComponent,
                        FileUploaderComponent,
                        FileUploaderModalComponent,
                        FileUploaderCardComponent,
                        ModalsDisplayComponent,
                        RenovationModalComponent,
                        SinisterClosedComponent,
                        AccountDataComponent,
                        PersonalDataComponent,
                        ContactDataComponent,
                        WelcomePackLinkCardComponent,
                        PasswordAndSecurityQuestionsChangeComponent,
                        ChangedPasswordModalComponent,
                        ChangedPinModalComponent,
                        ChangedSecurityQuestionModalComponent,
                        ChangesSuccessModalComponent,
                        MapfreMenu,
                        LanguageChangedSuccessModalComponent,
                        HelpComponent,
                        TabLinksComponent,
                        BudgetsComponent,
                        BudgetsV1Component,
                        ProfessionalAppointmentComponent,
                        WelcomePackModalComponent,
                        FooterModalComponent,
                        ProfileImageComponent,
                        ProfileImageModalComponent,
                        GrantedPermissionsComponent,
                        UserRelationshipsComponent,
                        PinLoginComponent,
                        IonDigitKeyboard,
                        DownloadAppNotificationComponent,
                        SignMethodsComponent,
                        NewCommunicationsComponent,
                        ReasonsCommentsComponent,
                        CancellationDateComponent,
                        PolicyModalitiesComparatorComponent,
                        InformationModalComponent,
                        AddReceiptsToPayModalComponent,
                        PopoverReceiptPaymentComponent,
                        ReceiptFractionateQuestionComponent,
                        NewPaymentMethodFormComponent,
                        FractionamentTypeSelectionComponent,
                        PaymentPlanComponent,
                        PolicyExtensionComponent,
                        SummaryComponent,
                        PaymentGroupingComponent,
                        SplitAmountComponent,
                        AmountDivisionsModalComponent,
                        CancellationDateComponent,
                        PolicyModalitiesComparatorComponent,
                        ProcessPolicyChangeFailureModalComponent,
                        PolicyCardComponent,
                        DocumentShareModalComponent,
                        OpenPaymentModalComponent,
                        OpenPaymentSmsConfirmationComponent,
                        PaymentReceiptsComponent,
                        ReceiptSelectionComponent,
                        AssistanceTypeHomeComponent,
                        NotIncludeCoveragesModalComponent,
                        RefuseRenewalModalComponent,
                        InformationComponent,
                        RequestAssistanceCancelModalComponent,
                        EmptySubmitIncidenceComponent,
                        ConditionsAcceptModalComponent,
                        IndemnifyDetailModalComponent,
                        CancelSinisterModalComponent,
                        InformationComponent,
                        MyLocationComponent,
                        SaveIncidenceModalComponent,
                        BackWarningModalComponent,
                        VehiclePolicySelectorComponent,
                        PageHeaderComponent,
                        FractionamentPaymentResultComponent,
                        AssistanceTypeSelectionComponent,
                        AssistanceRiskSelectionComponent,
                        AssistanceMotionSelectionComponent,
                        PolicyExtensionModalComponent,
                        ProcessConfirmationModalComponent,
                        UserWelcomeComponent,
                        TitleGlobalComponent,
                        PageBackgroundComponent,
                        SubmitIncidenceAdjusterComponent,
                        SubmitIncidenceInjuriesComponent,
                        SubmitIncidenceInjuriesDetailComponent,
                        SubmitIncidenceSubtypeComponent,
                        SubmitIncidenceTypeComponent,
                        SubmitIncidenceWhenComponent,
                        SubmitIncidenceHowComponent,
                        HomeIncidenceDateSelectionComponent,
                        HomeIncidenceDamageDetailComponent,
                        HomeIncidenceDamagedItemsCardComponent,
                        HomeIncidenceDamageReasonSelectionComponent,
                        HomeIncidenceInjuredDataComponent,
                        HomeIncidenceDamageTypeSelectionComponent,
                        HomeIncidenceLocationCardComponent,
                        HomeIncidenceThirdPartyDamagesCardComponent,
                        HomeIncidenceTypeSelectionComponent,
                        PageTitleComponent,
                        AttendanceTrackingModalComponent,
                        AttendanceTrackingComponent,
                        OffersListComponent,
                        OffersDetailHeaderComponent,
                        PillTabsComponent,
                        SubmitIncidenceHowOtherVehiclesComponent,
                        IconSelectorComponent,
                        LegalRegulationsDocumentationComponent,
                        ServiceManagementComponent,
                        HtmlComponent,
                        RichTextComponent,
                        BenefitsBannerComponent,
                        BudgetSelectionComponent,
                        NearestOfficeComponent,
                        ImportantBudgetInformationComponent,
                        PotentialBudgetDetailsComponent,
                        PotentialClientConfirmationComponent,
                        PotentialClientCallmeComponent,
                        PotentialClientAdditionalDataComponent,
                        PotentialClientPrivateAreaDataComponent,
                        UpgradeInsuranceInsuranceTypeCardV1Component,
                        OpenPaymentModalV2Component,
                        LoginQuickManagementV2Component,
                        PaymentMethodSelectionV1Component,
                        NewPaymentMethodFormV1Component,
                        SinisterNoClientComponent,
                        UpgradeInsuranceUpgradeSelectionV1Component,
                        LossNoClientComponent,
                        SummaryV2Component,
                        OpenPaymentEmailConfirmationComponent,
                        OpenPaymentCallMeComponent,
                        CoverageImprovementSelectorComponent,
                        PolicyModalitiesComparatorV1Component,
                        PolicyModalitiesComparatorV1Modal,
                        PolicyCoveragesComparatorComponent,
                        OpenSubmitIncidenceModalComponent,
                        ContraryDamagesFormV1Component,
                        InsuranceSelectionV2Component,
                        CoverageImprovementSelectorComponent,
                        UserRelationshipsV1Component,
                        SubheaderV1Component,
                        PaymentMethodSelectionV2Component,
                        FabGroupV1Component,
                        ClaimFormSimpleV1Component,
                        LossDetailModal,
                        PaymentMethodsV1Component,
                        PaymentChooseV1Component,
                        PaymentReceiptsV1Component,
                        SubmitIncidenceTypeV1Component,
                        IncidenceFormSimpleV1Component,
                        YourOffersV1Component
                    ]
                },] },
    ];
    SelfComponentsModule.ctorParameters = function () { return []; };
    return SelfComponentsModule;
}());
export { SelfComponentsModule };
//# sourceMappingURL=components.module.js.map