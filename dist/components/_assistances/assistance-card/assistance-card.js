import { ViewEncapsulation } from '@angular/core';
import { Component, Input, ElementRef } from '@angular/core';
import { Events, ModalController } from 'ionic-angular';
import * as jQuery from 'jquery';
import { Mocks } from '../../../providers/mocks/mocks';
import { TranslationProvider } from '../../../providers/translation/translation';
import { FormatProvider } from '../../../providers/format/format';
import { AttendanceTrackingModalComponent } from '../../_modals/_assistances/attendance-tracking-modal/attendance-tracking-modal';
import { AssistanceCancellationModalComponent } from '../../_modals/_assistances/assistance-cancellation-modal/assistance-cancellation-modal';
var AssistanceCardComponent = (function () {
    function AssistanceCardComponent(translationProvider, formatProvider, modalCtrl, elementRef, events) {
        this.translationProvider = translationProvider;
        this.formatProvider = formatProvider;
        this.modalCtrl = modalCtrl;
        this.elementRef = elementRef;
        this.events = events;
        this.cardHeaderPadding = 18;
        this.cardHeaderBorderThickness = 1;
        this.defaultIcons = ["mapfre-arrow-up"];
        this.translationProvider.bind(this);
        this.formatDate = this.formatProvider.formatDate;
    }
    AssistanceCardComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.mocked) {
            this.assistance = Mocks.getAssistance();
            this.modals = [{
                    type: 1,
                    modalId: "attendance-tracking-modal",
                    componentId: "assistance-card",
                    reference: 'AttendanceTrackingModalComponent',
                    reflabel: 'Modal de seguimiento de asistencia',
                    labels: {
                        title: "modal.assistance-card.attendance-tracking-modal.attendance_tracking.title",
                        subtitle: "modal.assistance-card.attendance-tracking-modal.attendance_tracking.subtitle",
                        generic_cancel: "modal.assistance-card-tracking-modal.generic.cancel",
                        generic_call: "modal.assistance-card.attendance-tracking-modal.generic.call",
                        cancellation_title: "modal.assistance-card.attendance-tracking-modal.request_assistance.assistance_cancelation.title",
                        cancelation_subtitle: "modal.assistance-card.attendance-tracking-modal.request_assistance.assistance_cancelation.subtitle",
                        no_button: "modal.assistance-card.attendance-tracking-modal.request_assistance.assistance_cancelation.no_button",
                        yes_button: "modal.assistance-card.attendance-tracking-modal.request_assistance.assistance_cancelation.yes_button",
                        confirmation_title: "modal.assistance-card.attendance-tracking-modal.request_assistance.assistance_cancelation.confirmation_title",
                        confirmation_subtitle: "modal.assistance-card.attendance-tracking-modal.request_assistance.assistance_cancelation.confirmation_description"
                    }
                }, {
                    type: 2,
                    modalId: "assistance-cancellation-modal",
                    componentId: "assistance-card",
                    reference: 'AssistanceCancellationModalComponent',
                    reflabel: 'Modal de cancelación de asistencia',
                    labels: {
                        title: "modal.assistance-card.assistance-cancellation-modal.request_assistance.assistance_cancelation.title",
                        subtitle: "modal.assistance-card.assistance-cancellation-modal.request_assistance.assistance_cancelation.subtitle",
                        no_button: "modal.assistance-card.assistance-cancellation-modal.request_assistance.assistance_cancelation.no_button",
                        yes_button: "modal.assistance-card.assistance-cancellation-modal.request_assistance.assistance_cancelation.yes_button",
                        confirmation_title: "modal.assistance-card.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_title",
                        confirmation_subtitle: "modal.assistance-card.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_description"
                    }
                }];
            this.componentReady = true;
        }
        else {
            this.componentReady = true;
        }
        this.events.unsubscribe(AssistanceCancellationModalComponent.cancellationEvent);
        this.events.subscribe(AssistanceCancellationModalComponent.cancellationEvent, function (assistance) {
            if (_this.assistance.assistance_number === assistance.assistance_number) {
                _this.onCancellation();
            }
        });
    };
    AssistanceCardComponent.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(AssistanceCancellationModalComponent.cancellationEvent);
    };
    AssistanceCardComponent.prototype.cancel = function () {
        this.modalCtrl.create(AssistanceCancellationModalComponent, {
            assistance: this.assistance,
            labels: {
                title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.title",
                subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.subtitle",
                no_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.no_button",
                yes_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.yes_button",
                confirmation_title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_title",
                confirmation_subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_description"
            }
        }).present();
    };
    AssistanceCardComponent.prototype.onCancellation = function () {
        this.assistance.assistance_state = 'Cancelada';
        this.assistance.cancel_enabled = false;
    };
    AssistanceCardComponent.prototype.toggle = function () {
        var _this = this;
        if (!this.fullHeight) {
            this.storeHeights();
        }
        setTimeout(function () {
            _this.collapsed = !_this.collapsed;
        });
    };
    AssistanceCardComponent.prototype.locate = function () {
        this.modalCtrl.create(AttendanceTrackingModalComponent, {
            labels: {
                title: "modal.your-pending-topics.attendance-tracking-modal.attendance_tracking.title",
                subtitle: "modal.your-pending-topics.attendance-tracking-modal.attendance_tracking.subtitle",
                generic_cancel: "modal.your-pending-topics-tracking-modal.generic.cancel",
                generic_call: "modal.your-pending-topics.attendance-tracking-modal.generic.call",
                cancellation_title: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.title",
                cancelation_subtitle: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.subtitle",
                no_button: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.no_button",
                yes_button: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.yes_button",
                confirmation_title: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.confirmation_title",
                confirmation_subtitle: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.confirmation_description"
            },
            assistance: this.assistance,
            mocked: this.mocked
        }).present();
    };
    AssistanceCardComponent.prototype.storeHeights = function () {
        var wrapperElement = jQuery(this.elementRef.nativeElement).find('ion-card');
        this.fullHeight = wrapperElement.outerHeight();
        this.collapsedHeight = wrapperElement.find('ion-card-header').outerHeight() - this.cardHeaderPadding - this.cardHeaderBorderThickness;
        wrapperElement.css('height', this.fullHeight + "px");
    };
    AssistanceCardComponent.prototype.getProperHeight = function () {
        return this.fullHeight ? ((this.collapsed ? this.collapsedHeight : this.fullHeight) + "px") : 'auto';
    };
    AssistanceCardComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    AssistanceCardComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    AssistanceCardComponent.inputs = ['assistance'];
    AssistanceCardComponent.configInputs = ['mocked', 'icons', 'styles', 'modals'];
    AssistanceCardComponent.decorators = [
        { type: Component, args: [{
                    selector: 'assistance-card',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["assistance-card .values-list ion-col {   padding: 0 !important; }"],
                    template: "<ion-card *ngIf=\"componentReady\" class=\"animated medium\" [style.height]=\"getProperHeight()\" [ngStyle]=\"styles\">    <ion-card-header>     <ion-item (click)=\"toggle()\" tabindex=\"0\" (keyup.enter)=\"toggle()\" class=\"clickable\">       <h3>         {{ assistance.type }}       </h3>       <h4 class=\"fmm-body1 grey1 vertical-margin-xs\">         {{ assistance.description }}       </h4>       <ion-icon         [name]=\"getIcon(0)\"         item-end class=\"animated medium no-horizontal-margin\"         [class.rotate-180]=\"collapsed\">       </ion-icon>     </ion-item>   </ion-card-header>    <ion-card-content class=\"assistance-content\">     <ion-grid class=\"values-list font-m no-padding-top no-horizontal-padding padding-bottom-s bottom-border\">        <ion-row>         <ion-col col-12 col-sm-4>           <strong class=\"primary-text\">             {{ translate('assistances.assistance-card.my_assistances.assistance_number') }}           </strong>         </ion-col>         <ion-col col-12 col-sm-8>           <div class=\"secondary-text\">             {{ assistance.number }}           </div>         </ion-col>       </ion-row>        <ion-row>         <ion-col col-12 col-sm-4>           <strong class=\"primary-text\">             {{ translate('assistances.assistance-card.my_assistances.assistance_date') }}           </strong>         </ion-col>         <ion-col col-12 col-sm-8>           <div class=\"secondary-text\">             {{ formatDate(assistance.date) }}           </div>         </ion-col>       </ion-row>        <ion-row>         <ion-col col-12 col-sm-4>           <strong class=\"primary-text\">             {{ translate('assistances.assistance-card.my_assistances.assistance_location') }}           </strong>         </ion-col>         <ion-col col-12 col-sm-8>           <div class=\"secondary-text\">             {{ assistance.location }}           </div>         </ion-col>       </ion-row>        <ion-row *ngIf=\"assistance.estimated_time\">         <ion-col col-12 col-sm-4>           <strong class=\"primary-text\">             {{ translate('assistances.assistance-card.my_assistances.assistance_estimated_time') }}           </strong>         </ion-col>         <ion-col col-12 col-sm-8>           <div class=\"secondary-text\">             {{ assistance.estimated_time }}           </div>         </ion-col>       </ion-row>        <ion-row>         <ion-col col-12 col-sm-4>           <strong class=\"primary-text\">             {{ translate('assistances.assistance-card.my_assistances.assistance_state') }}           </strong>         </ion-col>         <ion-col col-12 col-sm-8>           <div class=\"secondary-text\">             {{ assistance.state }}           </div>         </ion-col>       </ion-row>      </ion-grid>      <ion-grid class=\"actions-wrapper no-padding generic-buttons margin-top-m\">        <ion-row>         <ion-col col-6 col-sm-5 col-md-3>           <button class=\"secondary no-margin\" ion-button full text-uppercase tabindex=\"0\" (keyup.enter)=\"cancel()\"                   (click)=\"cancel()\" [disabled]=\"!assistance.cancel_enabled\">             {{ translate('assistances.assistance-card.my_assistances.cancel_button') }}           </button>         </ion-col>          <ion-col class=\"margin-left-auto\" col-6 col-sm-5 col-md-3>           <button class=\"secondary no-margin\" (click)=\"locate()\" ion-button full text-uppercase [disabled]=\"!assistance.cancel_enabled\">             {{ translate('assistances.assistance-card.generic.locate') }}           </button>         </ion-col>       </ion-row>      </ion-grid>    </ion-card-content>  </ion-card>"
                },] },
    ];
    AssistanceCardComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: FormatProvider, },
        { type: ModalController, },
        { type: ElementRef, },
        { type: Events, },
    ]; };
    AssistanceCardComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'assistance': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'modals': [{ type: Input },],
    };
    return AssistanceCardComponent;
}());
export { AssistanceCardComponent };
//# sourceMappingURL=assistance-card.js.map