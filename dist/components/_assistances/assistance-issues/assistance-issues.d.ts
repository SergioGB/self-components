import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class AssistanceIssuesComponent implements OnInit {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    assistancePhone: string;
    componentReady: boolean;
    defaultIcons: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
