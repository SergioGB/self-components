import { EventEmitter } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Assistance } from '../../../models/assistance';
export declare class AssistancesComponent {
    private componentSettingsProvider;
    private translationProvider;
    private insuranceProvider;
    private events;
    mocked?: boolean;
    styles?: any;
    riskId: any;
    onEmptyAssistance: EventEmitter<boolean>;
    componentReady: boolean;
    assistances: Assistance[];
    static componentName: string;
    static loadEvent: string;
    static inputs: string[];
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, insuranceProvider: InsuranceProvider, events: Events);
    ngOnInit(): void;
    private getPolicyAssistances;
    private emitLoad;
}
