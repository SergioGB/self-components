import { ViewEncapsulation } from '@angular/core';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Utils } from '../../../providers/utils/utils';
var AssistancesComponent = (function () {
    function AssistancesComponent(componentSettingsProvider, translationProvider, insuranceProvider, events) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.translationProvider = translationProvider;
        this.insuranceProvider = insuranceProvider;
        this.events = events;
        this.onEmptyAssistance = new EventEmitter();
        this.translationProvider.bind(this);
    }
    AssistancesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.assistances = [];
        if (this.mocked) {
            this.riskId = "1";
        }
        if (this.riskId) {
            this.insuranceProvider.getInsuranceDetailSettings(this.mocked, this.riskId).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    if (mapfreResponse.data && mapfreResponse.data.associated_policies && mapfreResponse.data.associated_policies.length) {
                        _this.getPolicyAssistances(mapfreResponse.data.associated_policies);
                    }
                    else {
                        _this.componentReady = true;
                        _this.emitLoad();
                    }
                }
            });
        }
        else {
            this.insuranceProvider.getPoliciesSettings(this.mocked, this.insuranceProvider.getClientId()).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    if (mapfreResponse.data && mapfreResponse.data.length > 0) {
                        _this.getPolicyAssistances(mapfreResponse.data);
                    }
                    else {
                        _this.componentReady = true;
                        _this.emitLoad();
                    }
                }
            });
        }
    };
    AssistancesComponent.prototype.getPolicyAssistances = function (policies) {
        var _this = this;
        policies.forEach(function (policy, policyIndex) {
            _this.componentSettingsProvider.getClaimsSettings(_this.mocked, policy.id, _this.riskId).subscribe(function (response) {
                var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    if (mapfreResponse.data && mapfreResponse.data.length) {
                        var claims_1 = mapfreResponse.data;
                        claims_1.forEach(function (claim, claimIndex) {
                            _this.componentSettingsProvider.getAssistancesSettings(_this.mocked, claim.id, true).subscribe(function (response) {
                                var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                                if (mapfreResponse) {
                                    _this.assistances.push.apply(_this.assistances, mapfreResponse.data);
                                    if (Utils.isLast(policyIndex, policies) && Utils.isLast(claimIndex, claims_1)) {
                                        _this.componentReady = true;
                                        _this.emitLoad();
                                    }
                                }
                            });
                        });
                    }
                    else {
                        _this.componentReady = true;
                        _this.emitLoad();
                    }
                }
            });
        });
    };
    AssistancesComponent.prototype.emitLoad = function () {
        this.events.publish(AssistancesComponent.loadEvent, {
            hasAssistances: this.assistances && this.assistances.length > 0
        });
    };
    AssistancesComponent.componentName = 'assistances';
    AssistancesComponent.loadEvent = AssistancesComponent.componentName + ":load";
    AssistancesComponent.inputs = ['riskId'];
    AssistancesComponent.configInputs = ['mocked', 'styles'];
    AssistancesComponent.decorators = [
        { type: Component, args: [{
                    selector: 'assistances',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["assistances > assistance-card:first-child > ion-card {   margin-top: 0 !important; }  assistances > assistance-card:last-child > ion-card {   margin-bottom: 0 !important; }"],
                    template: "<ion-card *ngIf=\"!(assistances && assistances.length) && !componentReady\" [ngStyle]=\"styles\">   <loading-spinner></loading-spinner> </ion-card>  <div *ngIf=\"assistances && assistances.length\">   <assistance-card     *ngFor=\"let assistance of assistances\"     [assistance]=\"assistance\">   </assistance-card> </div>"
                },] },
    ];
    AssistancesComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: TranslationProvider, },
        { type: InsuranceProvider, },
        { type: Events, },
    ]; };
    AssistancesComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'onEmptyAssistance': [{ type: Output },],
    };
    return AssistancesComponent;
}());
export { AssistancesComponent };
//# sourceMappingURL=assistances.js.map