import { ViewEncapsulation } from '@angular/core';
import { ApiProvider } from './../../../../providers/api/api';
import { Component, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Mocks } from '../../../../providers/mocks/mocks';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { Events } from 'ionic-angular';
import { OpenRequestAssistanceProvider } from '../../../../providers/open-request-assistance/open-request-assistance';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { RequestAssistanceProvider } from '../../../../providers/request-assistance/request-assistance';
var AssistanceTypeHomeComponent = (function () {
    function AssistanceTypeHomeComponent(translateProvider, openRequestAssistanceProvider, requestAssistanceprovider, formBuilder, apiProvider, componentSettingsProvider, events) {
        this.translateProvider = translateProvider;
        this.openRequestAssistanceProvider = openRequestAssistanceProvider;
        this.requestAssistanceprovider = requestAssistanceprovider;
        this.formBuilder = formBuilder;
        this.apiProvider = apiProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.events = events;
        this.assistanceTypes = [];
        this.defaultIcons = ["mapfre-check", "mapfre-no-check"];
        this.translateProvider.bind(this);
        this.assistanceTypes = [];
    }
    AssistanceTypeHomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.failureDetailForm = this.formBuilder.group({
            failureDetail: [this.failureDetail]
        });
        this.failureDetailForm.valueChanges.subscribe(function () {
            _this.emitSelection({
                assistanceType: _this.selectedAssistanceType,
                assistanceFailure: _this.failureDetailForm.value.failureDetail
            });
        });
        if (this.mocked) {
            this.assistanceTypes = Mocks.getHomeAssistaceTypes();
            this.componentReady = true;
        }
        else {
            this.open = this.requestAssistanceprovider.getOpenFlag();
            if (this.open) {
                this.openRequestAssistanceProvider.getAssistanceTypes().subscribe(function (response) {
                    var mapfreResponse = _this.apiProvider.getResponseJSON(response);
                    if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                        _this.assistanceTypes = mapfreResponse.data;
                        _this.componentReady = true;
                    }
                });
            }
            else {
                this.requestAssistanceprovider.getAssistanceTypes().subscribe(function (response) {
                    var mapfreResponse = _this.apiProvider.getResponseJSON(response);
                    if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                        _this.assistanceTypes = mapfreResponse.data;
                        _this.componentReady = true;
                    }
                });
            }
        }
        this.events.publish(AssistanceTypeHomeComponent.completedEvent, !!this.selectedAssistanceType);
    };
    AssistanceTypeHomeComponent.prototype.isSelected = function (assistanceType) {
        return this.selectedAssistanceType && this.selectedAssistanceType.id === assistanceType.id;
    };
    AssistanceTypeHomeComponent.prototype.selectAssistanceType = function (assistanceType) {
        this.selectedAssistanceType = assistanceType;
        this.emitSelection({
            assistanceType: assistanceType,
            assistanceFailure: this.failureDetailForm.value.failureDetail
        });
    };
    AssistanceTypeHomeComponent.prototype.emitSelection = function (selection) {
        this.events.publish(AssistanceTypeHomeComponent.selectionEvent, selection.assistanceType);
        this.events.publish(AssistanceTypeHomeComponent.selectedFailureDetailEvent, selection.assistanceFailure);
        this.events.publish(AssistanceTypeHomeComponent.completedEvent, true);
    };
    AssistanceTypeHomeComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    AssistanceTypeHomeComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    AssistanceTypeHomeComponent.componentName = "AssistanceTypeHomeComponent";
    AssistanceTypeHomeComponent.selectionEvent = AssistanceTypeHomeComponent.componentName + ":selection";
    AssistanceTypeHomeComponent.selectedFailureDetailEvent = AssistanceTypeHomeComponent.componentName + ":selectedFailureDetail";
    AssistanceTypeHomeComponent.completedEvent = AssistanceTypeHomeComponent.componentName + ":completed";
    AssistanceTypeHomeComponent.inputs = ['assistanceTypes', 'selectedAssistanceType', 'failureDetail', 'open'];
    AssistanceTypeHomeComponent.configInputs = ['mocked', 'icons', 'styles'];
    AssistanceTypeHomeComponent.decorators = [
        { type: Component, args: [{
                    selector: 'assistance-type-home',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<loading-spinner *ngIf=\"!componentReady\"></loading-spinner> <ion-card *ngIf=\"componentReady\" [ngStyle]=\"styles\">   <ion-card-header>     <ion-item>       <h2>{{ translate('assistance-type-home.process_steps.assistances.type') }}</h2>     </ion-item>   </ion-card-header>    <ion-card-content class=\"no-vertical-padding bottom-border border-only\">     <ion-item *ngFor=\"let assistanceType of assistanceTypes\" tabindex=\"0\" no-padding       class=\"checkbox-item clickable margin-bottom-s\" (tap)=\"selectAssistanceType(assistanceType)\">        <ion-icon item-start [name]=\"isSelected(assistanceType) ? getIcon(0) : getIcon(1)\"></ion-icon>        <h3 class=\"primary-text\">         {{ assistanceType.title }}       </h3>     </ion-item>      <form [formGroup]=\"failureDetailForm\">       <ion-item class=\"text-area margin-bottom-m\">          <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\" formControlName=\"failureDetail\"           [placeholder]=\"translate('assistance-type-home.request_assistance.assistance_request_type.detail')\">         </ion-textarea>        </ion-item>     </form>   </ion-card-content> </ion-card>"
                },] },
    ];
    AssistanceTypeHomeComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: OpenRequestAssistanceProvider, },
        { type: RequestAssistanceProvider, },
        { type: FormBuilder, },
        { type: ApiProvider, },
        { type: ComponentSettingsProvider, },
        { type: Events, },
    ]; };
    AssistanceTypeHomeComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'open': [{ type: Input },],
        'assistanceTypes': [{ type: Input },],
        'selectedAssistanceType': [{ type: Input },],
        'failureDetail': [{ type: Input },],
    };
    return AssistanceTypeHomeComponent;
}());
export { AssistanceTypeHomeComponent };
//# sourceMappingURL=assistance-type-home.js.map