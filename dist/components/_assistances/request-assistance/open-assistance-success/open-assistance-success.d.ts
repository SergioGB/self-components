import { OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Events, ModalController, NavController } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { UserProvider } from '../../../../providers/user/user';
export declare class OpenAssistanceSuccessComponent implements OnInit, OnDestroy {
    private navCtrl;
    private modalCtrl;
    private formBuilder;
    private events;
    private userProvider;
    private translationProvider;
    mocked?: boolean;
    styles?: any;
    assistanceTypeId: any;
    componentReady: boolean;
    assistanceResultForm: FormGroup;
    contactPhone: string;
    icon: any;
    description: any;
    static inputs: string[];
    static configInputs: string[];
    constructor(navCtrl: NavController, modalCtrl: ModalController, formBuilder: FormBuilder, events: Events, userProvider: UserProvider, translationProvider: TranslationProvider);
    ngOnInit(): void;
    ngOnDestroy(): void;
    private getUserData;
    cancelAssistance(): void;
    finishAssistance(): void;
    private initializeForm;
}
