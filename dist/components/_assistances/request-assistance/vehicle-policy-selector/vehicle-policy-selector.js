import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events, ModalController } from 'ionic-angular';
import { InsuranceProvider } from '../../../../providers/insurance/insurance';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { PlatformProvider } from '../../../../providers/platform/platform';
import { Observable } from 'rxjs/Observable';
import { Utils } from '../../../../providers/utils/utils';
var VehiclePolicySelectorComponent = (function () {
    function VehiclePolicySelectorComponent(insuranceProvider, translationProvider, modalCtrl, platformProvider, events) {
        this.insuranceProvider = insuranceProvider;
        this.translationProvider = translationProvider;
        this.modalCtrl = modalCtrl;
        this.platformProvider = platformProvider;
        this.events = events;
        this.defaultIcons = ["mapfre-check", "mapfre-no-check"];
        this.translationProvider.bind(this);
        this.platformProvider.bindTo(this);
        this.userRoles = ['HOLDER', 'OWNER', 'INSURED'];
    }
    VehiclePolicySelectorComponent.getSelectionEventName = function (eventTarget) {
        return Utils.getPrefixedString(VehiclePolicySelectorComponent.componentName + ":selection", eventTarget);
    };
    VehiclePolicySelectorComponent.prototype.ngOnInit = function () {
        var _this = this;
        Observable.forkJoin([
            this.insuranceProvider.getPoliciesOfType(this.mocked, 'C', this.userRoles),
            this.insuranceProvider.getPoliciesOfType(this.mocked, 'H', this.userRoles)
        ]).subscribe(function (responses) {
            _this.policies = responses[0].concat(responses[1]);
            _this.componentReady = true;
        });
        this.events.publish(VehiclePolicySelectorComponent.completedEvent, !!this.selectedPolicy);
    };
    VehiclePolicySelectorComponent.prototype.isSelected = function (policy) {
        return this.selectedPolicy && this.selectedPolicy.id === policy.id;
    };
    VehiclePolicySelectorComponent.prototype.selectPolicy = function (policy) {
        this.selectedPolicy = policy;
        this.events.publish(VehiclePolicySelectorComponent.getSelectionEventName(this.eventTarget), this.selectedPolicy);
        this.events.publish(VehiclePolicySelectorComponent.completedEvent, true);
    };
    VehiclePolicySelectorComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    VehiclePolicySelectorComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    VehiclePolicySelectorComponent.componentName = "VehiclePolicySelectorComponent";
    VehiclePolicySelectorComponent.selectionEvent = VehiclePolicySelectorComponent.componentName + ":selection";
    VehiclePolicySelectorComponent.completedEvent = VehiclePolicySelectorComponent.componentName + ":completed";
    VehiclePolicySelectorComponent.inputs = ['eventTarget', 'selectedPolicy'];
    VehiclePolicySelectorComponent.configInputs = ['mocked', 'icons', 'styles'];
    VehiclePolicySelectorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'vehicle-policy-selector',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div class=\"cards-container\" [ngStyle]=\"styles\">   <ion-card *ngIf=\"!(policies && policies.length) || !componentReady\">     <loading-spinner></loading-spinner>   </ion-card>    <ion-card *ngFor=\"let policy of policies\" class=\"clickable\"    (tap)=\"selectPolicy(policy)\">     <ion-card-content class=\"no-vertical-padding bottom-border border-only\">       <ion-item tabindex=\"0\" (keyup.enter)=\"selectPolicy(policy)\" class=\"checkbox-item\" no-padding>         <ion-icon *ngIf=\"!onMobile\" item-start extra-large [name]=\"isSelected(policy) ? getIcon(0) : getIcon(1)\"></ion-icon>         <ion-icon *ngIf=\"onMobile\" item-start [name]=\"isSelected(policy) ? getIcon(0) : getIcon(1)\"></ion-icon>          <h3 class=\"primary-text\">           {{ policy.risk_name }}         </h3>          <p class=\"secondary-text font-m\">           {{ policy.type }}         </p>       </ion-item>     </ion-card-content>   </ion-card>  </div>"
                },] },
    ];
    VehiclePolicySelectorComponent.ctorParameters = function () { return [
        { type: InsuranceProvider, },
        { type: TranslationProvider, },
        { type: ModalController, },
        { type: PlatformProvider, },
        { type: Events, },
    ]; };
    VehiclePolicySelectorComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'selectedPolicy': [{ type: Input },],
        'eventTarget': [{ type: Input },],
    };
    return VehiclePolicySelectorComponent;
}());
export { VehiclePolicySelectorComponent };
//# sourceMappingURL=vehicle-policy-selector.js.map