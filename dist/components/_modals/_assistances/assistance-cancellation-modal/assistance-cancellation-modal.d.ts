import { ElementRef } from '@angular/core';
import { Events, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Assistance } from '../../../../models/assistance';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
import { AssistanceProvider } from '../../../../providers/assistance/assistance';
export declare class AssistanceCancellationModalComponent {
    private viewCtrl;
    private formBuilder;
    private navParams;
    private events;
    private assistanceProvider;
    private translationProvider;
    private elementRef;
    private modalProvider;
    private mocked;
    formCancel: boolean;
    finishCancel: boolean;
    assistance: Assistance;
    cancelForm: FormGroup;
    labels: any;
    static componentName: string;
    static cancellationEvent: string;
    constructor(viewCtrl: ViewController, formBuilder: FormBuilder, navParams: NavParams, events: Events, assistanceProvider: AssistanceProvider, translationProvider: TranslationProvider, elementRef: ElementRef, modalProvider: ModalProvider);
    dismiss(): void;
    confirmCancelAssistance(): void;
    private onCancellationSuccess;
}
