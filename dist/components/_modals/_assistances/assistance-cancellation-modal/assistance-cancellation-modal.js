import { Component, ElementRef } from '@angular/core';
import { Events, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Mocks } from '../../../../providers/mocks/mocks';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
import { AssistanceProvider } from '../../../../providers/assistance/assistance';
var AssistanceCancellationModalComponent = (function () {
    function AssistanceCancellationModalComponent(viewCtrl, formBuilder, navParams, events, assistanceProvider, translationProvider, elementRef, modalProvider) {
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.navParams = navParams;
        this.events = events;
        this.assistanceProvider = assistanceProvider;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider.bind(this);
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.labels = {};
            this.assistance = Mocks.getAssistance();
            this.labels.title = this.translationProvider.getValueForKey('assistance-cancellation-modal.request_assistance.assistance_cancelation.title');
            this.labels.subtitle = this.translationProvider.getValueForKey('assistance-cancellation-modal.request_assistance.assistance_cancelation.subtitle');
            this.labels.no_button = this.translationProvider.getValueForKey('assistance-cancellation-modal.request_assistance.assistance_cancelation.no_button');
            this.labels.yes_button = this.translationProvider.getValueForKey('assistance-cancellation-modal.request_assistance.assistance_cancelation.yes_button');
            this.labels.confirmation_title = this.translationProvider.getValueForKey('assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_title');
            this.labels.confirmation_description = this.translationProvider.getValueForKey('assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_description');
        }
        else {
            this.assistance = this.navParams.get('assistance');
            this.labels = this.navParams.get('labels');
        }
        this.formCancel = true;
        this.finishCancel = false;
        this.cancelForm = this.formBuilder.group({
            reason: ['', Validators.required]
        });
        this.modalProvider.setupCentering(this.elementRef);
    }
    AssistanceCancellationModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AssistanceCancellationModalComponent.prototype.confirmCancelAssistance = function () {
        var _this = this;
        this.assistanceProvider.cancelAssistance(this.mocked, this.assistance).subscribe(function (data) {
            var mapfreResponse = _this.assistanceProvider.getResponseJSON(data);
            if (_this.assistanceProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.onCancellationSuccess();
            }
        });
    };
    AssistanceCancellationModalComponent.prototype.onCancellationSuccess = function () {
        this.formCancel = false;
        this.finishCancel = true;
        this.modalProvider.center(this.elementRef);
        this.events.publish(AssistanceCancellationModalComponent.cancellationEvent, this.assistance);
    };
    AssistanceCancellationModalComponent.componentName = 'assistance-cancellation';
    AssistanceCancellationModalComponent.cancellationEvent = AssistanceCancellationModalComponent.componentName + ":cancellation";
    AssistanceCancellationModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'assistance-cancellation',
                    template: "<ion-card no-margin *ngIf=\"formCancel\">   <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>      <div text-center class=\"padding-bottom-m bottom-border border-only\">       <ion-icon name=\"mapfre-buoy\" huge class=\"margin-bottom-l margin-top-m\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate(labels.title) }}       </h3>        <ion-grid no-padding>         <ion-row>           <ion-col col-12 col-md-8 push-md-2>             <div class=\"modal-description\">               {{ translate(labels.subtitle) }}             </div>           </ion-col>         </ion-row>       </ion-grid>      </div>      <ion-grid class=\"no-padding\">       <ion-row class=\"no-padding buttons-wrapper margin-top-l\">          <ion-col col-12 col-sm-4>           <button class=\"secondary\" ion-button full text-uppercase tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\">             {{ translate(labels.no_button) }}           </button>         </ion-col>          <ion-col col-12 col-sm-4 class=\"margin-left-auto\">           <button ion-button full text-uppercase tabindex=\"0\" (keyup.enter)=\"confirmCancelAssistance()\" (click)=\"confirmCancelAssistance()\">             {{ translate(labels.yes_button) }}           </button>         </ion-col>        </ion-row>     </ion-grid>    </ion-card-content>  </ion-card>  <ion-card no-margin *ngIf=\"finishCancel\">   <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>     <ion-item text-center>       <ion-icon name=\"mapfre-check\" huge class=\"green margin-bottom-l\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate(confirmation_title) }}       </h3>        <ion-grid no-padding>         <ion-row class=\"margin-top-m\">           <ion-col col-12 col-md-8 push-md-2>             <div class=\"modal-description\">               {{ translate(confirmation_description) }}             </div>           </ion-col>         </ion-row>       </ion-grid>      </ion-item>   </ion-card-content>  </ion-card>"
                },] },
    ];
    AssistanceCancellationModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: FormBuilder, },
        { type: NavParams, },
        { type: Events, },
        { type: AssistanceProvider, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: ModalProvider, },
    ]; };
    return AssistanceCancellationModalComponent;
}());
export { AssistanceCancellationModalComponent };
//# sourceMappingURL=assistance-cancellation-modal.js.map