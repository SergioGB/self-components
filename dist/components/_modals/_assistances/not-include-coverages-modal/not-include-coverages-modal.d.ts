import { ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ViewController, NavParams, Events } from 'ionic-angular';
import { DocumentsProvider } from '../../../../providers/documents/documents';
import { RequestAssistanceProvider } from '../../../../providers/request-assistance/request-assistance';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
import { PlatformProvider } from '../../../../providers/platform/platform';
export declare class NotIncludeCoveragesModalComponent implements OnInit, OnDestroy {
    private translationProvider;
    private modalProvider;
    private viewCtrl;
    private documentProvider;
    private navParams;
    private platformProvider;
    private requestAssistanceProvider;
    private elementRef;
    private events;
    mocked?: boolean;
    title: string;
    constructor(translationProvider: TranslationProvider, modalProvider: ModalProvider, viewCtrl: ViewController, documentProvider: DocumentsProvider, navParams: NavParams, platformProvider: PlatformProvider, requestAssistanceProvider: RequestAssistanceProvider, elementRef: ElementRef, events: Events);
    ngOnInit(): void;
    ngOnDestroy(): void;
    dismiss(): void;
    continue(): void;
    downloadDocument(): void;
    callMeCoverages(): void;
    exit(): void;
    goToOffer(): void;
}
