import { ElementRef } from '@angular/core';
import { ViewController, ModalController } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class RequestAssistanceCancelModalComponent {
    private elementRef;
    private viewCtrl;
    private modalController;
    private translationProvider;
    private modalProvider;
    constructor(elementRef: ElementRef, viewCtrl: ViewController, modalController: ModalController, translationProvider: TranslationProvider, modalProvider: ModalProvider);
    dismiss(): void;
    callMeOpened(): void;
}
