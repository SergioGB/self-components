import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { ViewController, ModalController } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var RequestAssistanceCancelModalComponent = (function () {
    function RequestAssistanceCancelModalComponent(elementRef, viewCtrl, modalController, translationProvider, modalProvider) {
        var _this = this;
        this.elementRef = elementRef;
        this.viewCtrl = viewCtrl;
        this.modalController = modalController;
        this.translationProvider = translationProvider;
        this.modalProvider = modalProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
        setTimeout(function () {
            _this.modalProvider.center(_this.elementRef);
        }, 1000);
    }
    RequestAssistanceCancelModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    RequestAssistanceCancelModalComponent.prototype.callMeOpened = function () {
        this.viewCtrl.dismiss(true);
    };
    RequestAssistanceCancelModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'request-assistance-cancel-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card>   <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>      <process-failure       [icon]=\"'mapfre-cross-error'\"       [title]=\"translate('request-assistance-cancel-modal.process.cancelation')\">     </process-failure>   </ion-card-content> </ion-card>"
                },] },
    ];
    RequestAssistanceCancelModalComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: ViewController, },
        { type: ModalController, },
        { type: TranslationProvider, },
        { type: ModalProvider, },
    ]; };
    return RequestAssistanceCancelModalComponent;
}());
export { RequestAssistanceCancelModalComponent };
//# sourceMappingURL=request-assistance-cancel-modal.js.map