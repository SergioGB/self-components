import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class CancelSinisterModalComponent {
    private viewCtrl;
    private navParams;
    private translationProvider;
    private elementRef;
    private modalProvider;
    private mocked;
    title: string;
    subtitle: string;
    icon: any;
    type: number;
    confirmFlowReference: boolean;
    labels: any;
    textRightButton: string;
    textLeftButton: string;
    textCenterButton: string;
    constructor(viewCtrl: ViewController, navParams: NavParams, translationProvider: TranslationProvider, elementRef: ElementRef, modalProvider: ModalProvider);
    dismiss(accepted: any): void;
    confirmAction(): void;
}
