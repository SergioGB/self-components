import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var CancelSinisterModalComponent = (function () {
    function CancelSinisterModalComponent(viewCtrl, navParams, translationProvider, elementRef, modalProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.labels = {};
        this.translationProvider.bind(this);
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.icon = 'mapfre-tools';
            this.labels.title = this.translationProvider.getValueForKey('cancel-sinister-modal.sinisters.tracing.cancel_modal.title');
            this.labels.textLeftButton = this.translationProvider.getValueForKey('cancel-sinister-modal.generic.no');
            this.labels.textRightButton = this.translationProvider.getValueForKey('cancel-sinister-modal.generic.yes');
        }
        else {
            this.labels = this.navParams.get('labels');
            this.icon = this.navParams.get('icon');
        }
        this.modalProvider.setupCentering(this.elementRef);
    }
    CancelSinisterModalComponent.prototype.dismiss = function (accepted) {
        this.viewCtrl.dismiss(false);
    };
    CancelSinisterModalComponent.prototype.confirmAction = function () {
        this.viewCtrl.dismiss(true);
    };
    CancelSinisterModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'cancel-sinister-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>    <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>     <div class=\"modal-content bottom-border border-only\" text-center>       <ion-icon name=\"{{ icon }}\" class=\"modal-icon\" [hidden]=\"!icon\"></ion-icon>            <h3 class=\"modal-title\">         {{ labels.title }}       </h3>            <ion-grid no-padding class=\"margin-top-m\">         <ion-row>           <ion-col col-12 col-md-8 push-md-2>             <div class=\"modal-description\">               {{ labels.subtitle }}             </div>           </ion-col>         </ion-row>       </ion-grid>     </div>       <ion-grid class=\"actions-wrapper no-padding\">       <ion-row class=\"padding-top-l\">          <ion-col col-6 col-sm-5 col-md-3 *ngIf=\"textLeftButton\">           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"dismiss()\">             {{ labels.textLeftButton }}           </button>         </ion-col>          <ion-col col-6 col-sm-5 col-md-3 *ngIf=\"textRightButton\">           <button ion-button full text-uppercase class=\"margin-left-auto\" (click)=\"confirmAction()\">             {{ labels.textRightButton }}           </button>         </ion-col>          <ion-col col-sm-4 col-12 offset-sm-4 text-center *ngIf=\"labels.textCenterButton\">           <button full ion-button text-uppercase (keyup.enter)=\"dismiss()\" (tap)=\"confirmAction()\">             {{ labels.textCenterButton }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content> </ion-card>"
                },] },
    ];
    CancelSinisterModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: NavParams, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: ModalProvider, },
    ]; };
    return CancelSinisterModalComponent;
}());
export { CancelSinisterModalComponent };
//# sourceMappingURL=cancel-sinister-modal.js.map