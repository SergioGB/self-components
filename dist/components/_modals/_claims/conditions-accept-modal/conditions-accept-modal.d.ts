import { ElementRef } from '@angular/core';
import { ViewController, ModalController, NavParams } from 'ionic-angular';
import { InformationProvider } from '../../../../providers/information/information';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class ConditionsAcceptModalComponent {
    private viewCtrl;
    private modalCtrl;
    private translationProvider;
    private navParams;
    private informationProvider;
    private elementRef;
    private modalProvider;
    private mocked;
    labels: any;
    accepted: boolean;
    indemnityProposal: any;
    constructor(viewCtrl: ViewController, modalCtrl: ModalController, translationProvider: TranslationProvider, navParams: NavParams, informationProvider: InformationProvider, elementRef: ElementRef, modalProvider: ModalProvider);
    dismiss(): void;
    openPrivacyModal(): void;
    openConditionsModal(): void;
    acceptConditions(): void;
}
