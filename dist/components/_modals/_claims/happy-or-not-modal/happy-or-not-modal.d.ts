import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class HappyOrNotModalComponent {
    private viewCtrl;
    private navParams;
    private elementRef;
    private navCtrl;
    private modalProvider;
    private translationProvider;
    constructor(viewCtrl: ViewController, navParams: NavParams, elementRef: ElementRef, navCtrl: NavController, modalProvider: ModalProvider, translationProvider: TranslationProvider);
    dismiss(): void;
    redirect(num: number): void;
}
