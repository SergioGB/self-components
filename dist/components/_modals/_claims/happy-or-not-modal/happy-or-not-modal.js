import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
var HappyOrNotModalComponent = (function () {
    function HappyOrNotModalComponent(viewCtrl, navParams, elementRef, navCtrl, modalProvider, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.elementRef = elementRef;
        this.navCtrl = navCtrl;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
    }
    HappyOrNotModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    HappyOrNotModalComponent.prototype.redirect = function (num) {
        if (num < 5) {
            this.navCtrl.push('ClaimPageFormPage');
        }
        else {
            this.navCtrl.push('IncidencePageFormPage');
        }
        this.dismiss();
    };
    HappyOrNotModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'happy-or-not-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["happy-or-not-modal ion-icon {   margin-left: 50%;   text-align: center !important;   width: 32px !important;   height: 32px !important; }  happy-or-not-modal p {   text-align: center; }"],
                    template: "<ion-card no-margin>    <ion-card-header class=\"no-padding-bottom\">     <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>   </ion-card-header>    <ion-card-content>     <hr>     <ion-grid class=\"actions-wrapper no-padding\">       <ion-row>         <ion-col>           <ion-item>             <h3>               {{ '\u00BFCu\u00E1l es la gravedad de tu situaci\u00F3n?' }}             </h3>           </ion-item>         </ion-col>       </ion-row>       <ion-row>         <ion-col>           <ion-item>             <div class=\"modal-description\">               <!--h3>1. Objeto del contrato.</h3-->               <div class=\"section-wrapper\">                 <p>{{'Para poder atenderte correctamente, por favor ind\u00EDcanos primero tu grado de satisfacci\u00F3n con                   nuestros servicios'}}</p>               </div>             </div>           </ion-item>         </ion-col>       </ion-row>       <ion-row>         <ion-col class=\"clickable padding\" (click)=\"redirect(1)\" col-12 col-sm-2 col-md-2>           <ion-icon name=\"mapfre-sad-face\" large></ion-icon>           <p>             {{ 'Descontento' }}           </p>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(2)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(3)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(4)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(5)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(6)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(7)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(8)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(9)\" col-12 col-sm-1 col-md-1>           <ion-icon name=\"mapfre-full-circle\"></ion-icon>         </ion-col>          <ion-col class=\"clickable padding\" (click)=\"redirect(10)\" col-12 col-sm-2 col-md-2>           <ion-icon name=\"mapfre-happy-face\" large></ion-icon>           <p>             {{ 'Muy Satisfecho' }}           </p>         </ion-col>       </ion-row>     </ion-grid>     <hr>     <ion-grid class=\"actions-wrapper no-padding\">       <ion-row>          <ion-col col-12 col-sm-5 col-md-3>           <button class=\"secondary\" ion-button full text-uppercase (click)=\"dismiss()\">             {{ translate('happy-or-not-modal.generic.cancel') }}           </button>         </ion-col>          <ion-col col-12 col-sm-5 col-md-3>           <button ion-button full text-uppercase (click)=\"redirect()\">             {{ translate('happy-or-not-modal.generic.accept') }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    HappyOrNotModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: NavParams, },
        { type: ElementRef, },
        { type: NavController, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
    ]; };
    return HappyOrNotModalComponent;
}());
export { HappyOrNotModalComponent };
//# sourceMappingURL=happy-or-not-modal.js.map