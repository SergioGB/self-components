import { ElementRef } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { FormatProvider } from '../../../../providers/format/format';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class IndemnifyDetailModalComponent {
    private viewCtrl;
    private translationProvider;
    private elementRef;
    private navParams;
    private formatProvider;
    private modalProvider;
    private mocked;
    loss: any;
    lossDetail: any;
    labels: any;
    formatDate: Function;
    paymentDetail: any;
    amount: any;
    constructor(viewCtrl: ViewController, translationProvider: TranslationProvider, elementRef: ElementRef, navParams: NavParams, formatProvider: FormatProvider, modalProvider: ModalProvider);
    dismiss(): void;
    accept(): void;
}
