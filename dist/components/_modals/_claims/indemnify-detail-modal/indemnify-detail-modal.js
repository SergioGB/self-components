import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Mocks } from '../../../../providers/mocks/mocks';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { FormatProvider } from '../../../../providers/format/format';
import { ModalProvider } from '../../../../providers/modal/modal';
var IndemnifyDetailModalComponent = (function () {
    function IndemnifyDetailModalComponent(viewCtrl, translationProvider, elementRef, navParams, formatProvider, modalProvider) {
        this.viewCtrl = viewCtrl;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.navParams = navParams;
        this.formatProvider = formatProvider;
        this.modalProvider = modalProvider;
        this.translationProvider.bind(this);
        this.formatDate = this.formatProvider.formatDate;
        this.labels = this.navParams.get('labels');
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.loss = Mocks.getPolicyClaims()[0];
            this.lossDetail = Mocks.getPolicyClaims()[0].complete_detail.car_detail;
        }
        else {
            this.loss = this.navParams.get('loss');
            this.lossDetail = this.navParams.get('lossDetail');
        }
        if (this.loss.indemnity_proposal && this.loss.indemnity_proposal.payment_method &&
            this.loss.indemnity_proposal.payment_method.credit_card_info) {
            this.paymentDetail = {
                description: 'Tarjeta de crédito',
                number: this.loss.indemnity_proposal.payment_method.credit_card_info.cardholder_name
            };
        }
        else if (this.loss.indemnity_proposal && this.loss.indemnity_proposal.payment_method
            && this.loss.indemnity_proposal.payment_method.bank_account_info) {
            this.paymentDetail = {
                description: 'Transferencia bancaria',
                number: this.loss.indemnity_proposal.payment_method.bank_account_info.account_number
            };
        }
        if (this.loss.indemnity_proposal) {
            this.amount = this.loss.indemnity_proposal.amount;
        }
        this.modalProvider.setupCentering(this.elementRef);
    }
    IndemnifyDetailModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    IndemnifyDetailModalComponent.prototype.accept = function () {
        this.viewCtrl.dismiss(true);
    };
    IndemnifyDetailModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'indemnify-detail-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>   <ion-card-header class=\"no-border\">     <ion-item>       <h3>         {{ translate(labels.title) }}       </h3>       <h4 class=\"fmm-body1 grey1 vertical-margin-xs\">         {{ translate(labels.title) }}       </h4>     </ion-item>   </ion-card-header>   <ion-card-content>     <ion-row>       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.insured) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ 'Fernando Espinel bermejo' }}         </span>       </ion-col>     </ion-row>     <ion-row>       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.advertiser) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ 'Fernando Espinel Bermejo' }}         </span>       </ion-col>     </ion-row>     <ion-row *ngIf=\"lossDetail\">       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.risk) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ lossDetail.associated_risk.risk }}         </span>       </ion-col>     </ion-row>     <ion-row>       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.policy_number) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ '57021345' }}         </span>       </ion-col>     </ion-row>     <ion-row>       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.sinister_number) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ loss.number }}         </span>       </ion-col>     </ion-row>     <ion-row>       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.sinister_date) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ formatDate(loss.opening_date) }}         </span>       </ion-col>     </ion-row>     <ion-row *ngIf=\"amount\">       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.amount) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ amount.amount | currency:amount.iso_code:true:'1.2-2' }}         </span>       </ion-col>     </ion-row>     <ion-row *ngIf=\"paymentDetail\">       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.payment_method) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ paymentDetail.description }}         </span>       </ion-col>     </ion-row>     <ion-row *ngIf=\"paymentDetail\">       <ion-col col-12 col-sm-4 class=\"no-padding-left\">         <strong class=\"primary-text\">           {{ translate(labels.account_number) }}         </strong>       </ion-col>       <ion-col col-12 col-sm-8 class=\"no-padding-left\">         <span class=\"secondary-text\">           {{ paymentDetail.number }}         </span>       </ion-col>     </ion-row>     <hr class=\"no-margin-bottom\">     <ion-grid class=\"no-padding\">       <ion-row class=\"no-padding buttons-wrapper margin-top-l\">          <ion-col col-5 col-sm-4 class=\"no-vertical-padding\">           <button class=\"secondary no-margin\" ion-button full text-uppercase tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\">             {{ translate(labels.cancel) }}           </button>         </ion-col>          <ion-col col-5 col-sm-4 class=\"margin-left-auto no-vertical-padding\">           <button class=\"no-margin\" ion-button full text-uppercase tabindex=\"0\" (keyup.enter)=\"accept()\" (click)=\"accept()\">             {{ translate(labels.accept) }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content> </ion-card>"
                },] },
    ];
    IndemnifyDetailModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: NavParams, },
        { type: FormatProvider, },
        { type: ModalProvider, },
    ]; };
    return IndemnifyDetailModalComponent;
}());
export { IndemnifyDetailModalComponent };
//# sourceMappingURL=indemnify-detail-modal.js.map