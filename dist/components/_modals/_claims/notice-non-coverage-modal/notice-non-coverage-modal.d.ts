import { ElementRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Events, ViewController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../../providers/user/user';
import { ApiProvider } from '../../../../providers/api/api';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class NoticeNonCoverageModalComponent {
    private userProvider;
    private viewCtrl;
    private apiProvider;
    private translationProvider;
    private elementRef;
    private formBuilder;
    private navParams;
    private componentSettingsProvider;
    private modalProvider;
    private events;
    private form;
    labels: any;
    constructor(userProvider: UserProvider, viewCtrl: ViewController, apiProvider: ApiProvider, translationProvider: TranslationProvider, elementRef: ElementRef, formBuilder: FormBuilder, navParams: NavParams, componentSettingsProvider: ComponentSettingsProvider, modalProvider: ModalProvider, events: Events);
    initializeForm(): void;
    dismiss(): void;
    cancel(): void;
    confirm(): void;
}
