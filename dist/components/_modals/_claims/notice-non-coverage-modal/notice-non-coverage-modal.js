import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Events, ViewController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../../providers/user/user';
import { ApiProvider } from '../../../../providers/api/api';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var NoticeNonCoverageModalComponent = (function () {
    function NoticeNonCoverageModalComponent(userProvider, viewCtrl, apiProvider, translationProvider, elementRef, formBuilder, navParams, componentSettingsProvider, modalProvider, events) {
        this.userProvider = userProvider;
        this.viewCtrl = viewCtrl;
        this.apiProvider = apiProvider;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.formBuilder = formBuilder;
        this.navParams = navParams;
        this.componentSettingsProvider = componentSettingsProvider;
        this.modalProvider = modalProvider;
        this.events = events;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
        this.labels = this.navParams.get('labels');
        this.initializeForm();
    }
    NoticeNonCoverageModalComponent.prototype.initializeForm = function () {
        this.form = null;
    };
    NoticeNonCoverageModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    NoticeNonCoverageModalComponent.prototype.cancel = function () {
        this.events.publish('notice_non_coverage_modal:cancel');
        this.dismiss();
    };
    NoticeNonCoverageModalComponent.prototype.confirm = function () {
        this.events.publish('notice_non_coverage_modal:confirm');
        this.dismiss();
    };
    NoticeNonCoverageModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'notice-non-coverage-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>    <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>      <div class=\"modal-content bottom-border border-only\" text-center>       <ion-icon name=\"mapfre-warning\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate(labels.title) }}       </h3>        <ion-grid no-padding class=\"margin-top-m\">         <ion-row>           <ion-col col-12 col-md-8 push-md-2>             <div class=\"modal-description\">               {{ translate(labels.subtitle) }}             </div>           </ion-col>         </ion-row>       </ion-grid>     </div>      <ion-grid class=\"actions-wrapper generic-buttons margin-top-l no-padding\">       <ion-row>         <ion-col col-6 col-sm-4 col-xl-3>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"cancel()\">             {{ translate(labels.cancel) }}           </button>         </ion-col>         <ion-col col-6 col-sm-4 col-xl-3 class=\"margin-left-auto\">           <button ion-button full text-uppercase (click)=\"confirm()\">             {{ translate(labels.continue) }}           </button>         </ion-col>       </ion-row>     </ion-grid>   </ion-card-content> </ion-card>"
                },] },
    ];
    NoticeNonCoverageModalComponent.ctorParameters = function () { return [
        { type: UserProvider, },
        { type: ViewController, },
        { type: ApiProvider, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: FormBuilder, },
        { type: NavParams, },
        { type: ComponentSettingsProvider, },
        { type: ModalProvider, },
        { type: Events, },
    ]; };
    return NoticeNonCoverageModalComponent;
}());
export { NoticeNonCoverageModalComponent };
//# sourceMappingURL=notice-non-coverage-modal.js.map