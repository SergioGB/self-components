import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class CommunicationsModalComponent {
    private viewCtrl;
    private navParams;
    private elementRef;
    private modalProvider;
    private mocked;
    private title;
    private description;
    constructor(viewCtrl: ViewController, navParams: NavParams, elementRef: ElementRef, modalProvider: ModalProvider);
    dismiss(): void;
}
