import { ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';
import { UserProvider } from '../../../../providers/user/user';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class CallMeResponseModalComponent {
    private viewCtrl;
    private navParams;
    private modalProvider;
    private elementRef;
    private userProvider;
    private formBuilder;
    private translationProvider;
    private mocked;
    wrongPhone: boolean;
    callMeForm: FormGroup;
    mobilePhone: String;
    constructor(viewCtrl: ViewController, navParams: NavParams, modalProvider: ModalProvider, elementRef: ElementRef, userProvider: UserProvider, formBuilder: FormBuilder, translationProvider: TranslationProvider);
    dismiss(): void;
    change(): void;
    private initializeForm;
    onBlurPhone(): void;
}
