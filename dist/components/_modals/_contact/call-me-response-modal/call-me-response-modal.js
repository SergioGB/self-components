import { Component, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavParams, ViewController } from 'ionic-angular';
import { Mocks } from '../../../../providers/mocks/mocks';
import { UserProvider } from '../../../../providers/user/user';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var CallMeResponseModalComponent = (function () {
    function CallMeResponseModalComponent(viewCtrl, navParams, modalProvider, elementRef, userProvider, formBuilder, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.modalProvider = modalProvider;
        this.elementRef = elementRef;
        this.userProvider = userProvider;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.wrongPhone = false;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
        this.initializeForm();
    }
    CallMeResponseModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CallMeResponseModalComponent.prototype.change = function () {
        this.dismiss();
    };
    CallMeResponseModalComponent.prototype.initializeForm = function () {
        var _this = this;
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.mobilePhone = Mocks.getPhone();
        }
        else {
            this.userProvider.getUserData().subscribe(function (userData) {
                if (userData) {
                    _this.mobilePhone = userData.mobile_phone;
                }
            });
        }
        this.callMeForm = this.formBuilder.group({
            phoneNumber: [this.mobilePhone, Validators.required]
        });
    };
    CallMeResponseModalComponent.prototype.onBlurPhone = function () {
        this.wrongPhone =
            this.callMeForm.value.phoneNumber &&
                !this.callMeForm.controls['phoneNumber'].valid && this.callMeForm.controls['phoneNumber'].dirty;
    };
    CallMeResponseModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'call-me-response',
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>      <div text-center>       <ion-icon name=\"mapfre-rocket\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate('call-me-response-modal.change_payment.call_me.title') }}       </h3>        <div class=\"modal-description\">         {{ translate('call-me-response-modal.change_payment.call_me.subtitle') }}       </div>      </div>      <form [formGroup]=\"callMeForm\" (ngSubmit)=\"change()\">       <ion-grid no-padding class=\"margin-top-m\">         <ion-row>           <ion-col offset-lg-1 col-lg-10 col-12>             <ion-row>               <ion-col class=\"phone\" col-md-8 col-12>                 <ion-item no-padding>                   <ion-input                     formControlName=\"phoneNumber\" type=\"tel\" maxlength=\"20\"                     pattern=\"(\\\\+[0-9]*)?[0-9]*\" (ionBlur)=\"onBlurPhone()\" placeholder=\"649 87 54 87\">                   </ion-input>                 </ion-item>                  <div *ngIf=\"wrongPhone\" class=\"input-value-error\">                   <span>                     {{ translate('call-me-response-modal.change_payment.call_me.phone_format_error') }}                   </span>                 </div>               </ion-col>                <ion-col col-md-4 col-12>                 <button ion-button full text-uppercase [disabled]=\"!callMeForm.valid\">                   {{ translate('call-me-response-modal.change_payment.call_me.change_button') }}                 </button>               </ion-col>             </ion-row>           </ion-col>         </ion-row>       </ion-grid>     </form>   </ion-card-content>  </ion-card>"
                },] },
    ];
    CallMeResponseModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: NavParams, },
        { type: ModalProvider, },
        { type: ElementRef, },
        { type: UserProvider, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
    ]; };
    return CallMeResponseModalComponent;
}());
export { CallMeResponseModalComponent };
//# sourceMappingURL=call-me-response-modal.js.map