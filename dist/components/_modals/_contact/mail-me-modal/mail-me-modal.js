import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalController, NavParams, ViewController } from 'ionic-angular';
import { Mocks } from '../../../../providers/mocks/mocks';
import { InformationProvider } from '../../../../providers/information/information';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { UserProvider } from '../../../../providers/user/user';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { Utils } from '../../../../providers/utils/utils';
var MailMeModalComponent = (function () {
    function MailMeModalComponent(elementRef, viewCtrl, navParams, formBuilder, modalCtrl, modalProvider, translationProvider, componentSettingsProvider, informationProvider, userProvider) {
        this.elementRef = elementRef;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.modalCtrl = modalCtrl;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.informationProvider = informationProvider;
        this.userProvider = userProvider;
        this.translationProvider.bind(this);
        this.labels = this.navParams.get('labels');
        this.mocked = this.navParams.get('mocked');
        this.initializeForm();
    }
    MailMeModalComponent.prototype.ngOnInit = function () {
        this.modalProvider.setupCentering(this.elementRef);
    };
    MailMeModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    MailMeModalComponent.prototype.openPrivacyModal = function () {
        this.informationProvider.showPrivacyModal(this.modalCtrl, this.mocked);
    };
    MailMeModalComponent.prototype.openConditionsModal = function () {
        this.informationProvider.showConditionsModal(this.modalCtrl, this.mocked);
    };
    MailMeModalComponent.prototype.onSubmit = function () {
        this.isMailNew = this.currentMail !== this.mailMeForm.value.mail;
        if (!this.isMailNew) {
            this.postMailMeRequest();
        }
        this.centerSelf();
    };
    MailMeModalComponent.prototype.cancelNewMailDirection = function () {
        this.isMailNew = false;
        this.centerSelf();
    };
    MailMeModalComponent.prototype.postNewMailDirection = function () {
        var _this = this;
        if (this.mocked) {
            this.onMailSuccess();
        }
        else {
            this.userProvider.sendUpdateUser({
                contact_data: {
                    mail: this.mailMeForm.value.mail
                }
            }, this.userProvider.getClientId()).subscribe(function (response) {
                _this.postMailMeRequest();
            });
        }
    };
    MailMeModalComponent.prototype.initializeForm = function () {
        var user;
        if (this.mocked) {
            user = Mocks.getClients()[0];
        }
        else {
            user = this.componentSettingsProvider.getLoggedUserData();
        }
        this.currentMail = user.contact_data.mail;
        this.mailMeForm = this.formBuilder.group({
            mail: [this.currentMail, Validators.required],
            policyCheckbox: [false, Utils.isControlChecked]
        });
    };
    MailMeModalComponent.prototype.postMailMeRequest = function () {
        this.onMailSuccess();
    };
    MailMeModalComponent.prototype.onMailSuccess = function () {
        this.isMailNew = false;
        this.isConfirmation = true;
        this.centerSelf();
    };
    MailMeModalComponent.prototype.centerSelf = function () {
        this.modalProvider.center(this.elementRef);
    };
    MailMeModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'mail-me-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>   <form [formGroup]=\"mailMeForm\" (ngSubmit)=\"onSubmit()\">     <ion-card-content>       <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>        <div class=\"modal-content\" text-center *ngIf=\"!isMailNew && !isConfirmation\">         <ion-icon name=\"mapfre-envelope\" class=\"modal-icon\"></ion-icon>          <h3 class=\"modal-title\">           {{ translate(labels.title) }}         </h3>          <ion-grid no-padding class=\"margin-top-m\">           <ion-row>             <ion-col col-12 col-md-8 push-md-2>                <div class=\"modal-description\">                 {{ translate(labels.description) }}               </div>              </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-md-8 push-md-2>                <div class=\"margin-bottom-m\">                 <ion-item no-padding>                   <ion-label floating>                     {{ translate(labels.mail) }}                   </ion-label>                    <ion-input formControlName=\"mail\" type=\"mail\" maxlength=\"20\"></ion-input>                 </ion-item>               </div>              </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-md-8 push-md-2>               <ion-checkbox formControlName=\"policyCheckbox\" type=\"checkbox\"></ion-checkbox>                <span class=\"conditions-label\">                 {{ translate(labels.privacy_check1) }}                 <a (click)=\"openPrivacyModal()\" class=\"clickable\" [title]=\"translate(labels.privacy_check2)\">                   {{ translate(labels.privacy_check2) }}                 </a>                 {{ translate(labels.privacy_check3) }}                 <a (click)=\"openConditionsModal()\" onClick=\"return false;\" class=\"clickable\" [title]=\"translate(labels.privacy_check4)\">                   {{ translate(labels.privacy_check4) }}                 </a>                 {{ translate(labels.privacy_check5) }}               </span>                <hr class=\"margin-top-m\">             </ion-col>           </ion-row>         </ion-grid>       </div>        <div class=\"modal-content\" text-center *ngIf=\"isMailNew && !isConfirmation\">         <ion-icon name=\"mapfre-rocket\" class=\"modal-icon\"></ion-icon>          <h3 class=\"modal-title\">           {{ translate(labels.want_update_your_data_title) }}         </h3>          <ion-grid no-padding class=\"margin-top-m\">           <ion-row>             <ion-col col-12 col-md-8 push-md-2>                <div class=\"modal-description\">                 {{ translate(labels.want_update_your_data_description) }}               </div>              </ion-col>           </ion-row>         </ion-grid>       </div>        <div class=\"modal-content\" *ngIf=\"!isMailNew && isConfirmation\">         <process-confirmation           [icon]=\"'mapfre-check'\"           [title]=\"translate(labels.confirmation)\">         </process-confirmation>       </div>        <ion-grid *ngIf=\"!isConfirmation\" no-padding class=\"actions-wrapper\">         <ion-row *ngIf=\"!isMailNew\">           <ion-col col-12 col-md-8 push-md-2>              <button ion-button full text-uppercase [disabled]=\"!mailMeForm.valid\">               {{ translate(labels.mail_me_button) }}             </button>            </ion-col>         </ion-row>         <ion-row *ngIf=\"isMailNew\">           <ion-col col-12 col-md-3>              <button class=\"secondary\" ion-button full text-uppercase (click)=\"cancelNewMailDirection()\">               {{ translate(labels.cancel) }}             </button>            </ion-col>           <ion-col col-12 col-md-3 class=\"margin-left-auto\">              <button ion-button full text-uppercase (click)=\"postNewMailDirection()\">               {{ translate(labels.save) }}             </button>            </ion-col>         </ion-row>       </ion-grid>     </ion-card-content>   </form> </ion-card>"
                },] },
    ];
    MailMeModalComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: ViewController, },
        { type: NavParams, },
        { type: FormBuilder, },
        { type: ModalController, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
        { type: InformationProvider, },
        { type: UserProvider, },
    ]; };
    return MailMeModalComponent;
}());
export { MailMeModalComponent };
//# sourceMappingURL=mail-me-modal.js.map