import { ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { InsuranceProvider } from '../../../../providers/insurance/insurance';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class PolicyCancelModalComponent {
    private navParams;
    private navController;
    private viewCtrl;
    private formBuilder;
    private elementRef;
    private modalProvider;
    private translationProvider;
    private insuranceProvider;
    private mocked;
    private labels;
    riskId: number;
    policyId: string;
    cancelForm: FormGroup;
    formCancel: boolean;
    finishCancel: boolean;
    constructor(navParams: NavParams, navController: NavController, viewCtrl: ViewController, formBuilder: FormBuilder, elementRef: ElementRef, modalProvider: ModalProvider, translationProvider: TranslationProvider, insuranceProvider: InsuranceProvider);
    submit(): void;
    dismiss(): void;
    private initializeForm;
    private attemptPolicyCancellation;
    private onSuccess;
}
