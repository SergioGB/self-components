import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { InsuranceProvider } from '../../../../providers/insurance/insurance';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
var PolicyCancelModalComponent = (function () {
    function PolicyCancelModalComponent(navParams, navController, viewCtrl, formBuilder, elementRef, modalProvider, translationProvider, insuranceProvider) {
        this.navParams = navParams;
        this.navController = navController;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.insuranceProvider = insuranceProvider;
        this.formCancel = true;
        this.translationProvider.bind(this);
        this.policyId = navParams.get('mocked');
        this.labels = navParams.get('labels');
        this.riskId = navParams.get('riskId');
        this.policyId = navParams.get('policyId');
        this.modalProvider.setupCentering(this.elementRef);
        this.initializeForm();
    }
    PolicyCancelModalComponent.prototype.submit = function () {
        this.navController.push('PolicyCancellationStepsPage', { stepId: 1, riskId: this.riskId, policyId: this.policyId }, { animate: false });
    };
    PolicyCancelModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PolicyCancelModalComponent.prototype.initializeForm = function () {
        this.cancelForm = this.formBuilder.group({
            reason: ['', [Validators.required]]
        });
    };
    PolicyCancelModalComponent.prototype.attemptPolicyCancellation = function () {
        var _this = this;
        this.insuranceProvider.cancelPolicy(this.mocked, this.policyId).subscribe(function (response) {
            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
            if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.onSuccess();
            }
        });
    };
    PolicyCancelModalComponent.prototype.onSuccess = function () {
        this.formCancel = false;
        this.finishCancel = true;
        this.modalProvider.center(this.elementRef);
    };
    PolicyCancelModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'policy-cancel',
                    template: "<ion-card no-margin *ngIf=\"formCancel\">   <ion-card-header class=\"no-padding-bottom\">     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>      <ion-item>       <ion-grid no-padding>         <ion-row>            <ion-col col-10 col-sm-10 col-xl-11>             <h3>               {{ translate(labels.title) }}             </h3>             <h4 class=\"fmm-body1 grey1\">               {{ translate(labels.policy) }} {{ policyId }}             </h4>           </ion-col>          </ion-row>       </ion-grid>     </ion-item>   </ion-card-header>    <form [formGroup]=\"cancelForm\">     <ion-card-content class=\"no-padding-top\">        <ion-item class=\"text-area required margin-bottom-xs\">          <ion-label floating>           {{ translate(labels.reason) }}         </ion-label>          <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\" formControlName=\"reason\">         </ion-textarea>        </ion-item>        <hr class=\"vertical-margin-m horizontal-margin-s\">        <ion-grid no-padding class=\"padding-top-xxs\">         <ion-row>           <ion-col offset-sm-9 col-sm-3 col-12 offset-md-9 col-md-3>             <button [disabled]=\"!cancelForm.valid\" (click)=\"submit()\" ion-button full text-uppercase>               {{ translate(labels.request_button) }}             </button>           </ion-col>         </ion-row>       </ion-grid>      </ion-card-content>   </form> </ion-card>  <ion-card no-margin *ngIf=\"finishCancel\">   <ion-card-content class=\"content-finish\">     <ion-icon name=\"mapfre-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>      <ion-grid no-padding>       <ion-row>         <ion-col col-xl-8 offset-xl-2 offset-xs-8 col-xs-2 text-center class=\"modal-content\">           <confirmation-check></confirmation-check>            <h3 class=\"modal-title\">             {{ translate(labels.procedure_title) }}           </h3>            <div class=\"modal-description\">             {{ translate(labels.procedure_subtitle) }}           </div>          </ion-col>       </ion-row>     </ion-grid>   </ion-card-content> </ion-card>"
                },] },
    ];
    PolicyCancelModalComponent.ctorParameters = function () { return [
        { type: NavParams, },
        { type: NavController, },
        { type: ViewController, },
        { type: FormBuilder, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
        { type: InsuranceProvider, },
    ]; };
    return PolicyCancelModalComponent;
}());
export { PolicyCancelModalComponent };
//# sourceMappingURL=policy-cancel-modal.js.map