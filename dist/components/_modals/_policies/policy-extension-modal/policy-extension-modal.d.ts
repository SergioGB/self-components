import { ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Events, ModalController, NavParams, ViewController } from 'ionic-angular';
import { InsuranceProvider } from '../../../../providers/insurance/insurance';
import { AssociatedRisk } from '../../../../models/associatedRisk';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class PolicyExtensionModalComponent implements OnInit, OnDestroy {
    private viewCtrl;
    private modalProvider;
    private modalCtrl;
    private navParams;
    private insuranceProvider;
    private events;
    private translationProvider;
    private elementRef;
    private mocked;
    private labels;
    private policyId;
    private policiesExtension;
    constructor(viewCtrl: ViewController, modalProvider: ModalProvider, modalCtrl: ModalController, navParams: NavParams, insuranceProvider: InsuranceProvider, events: Events, translationProvider: TranslationProvider, elementRef: ElementRef);
    ngOnInit(): void;
    ngOnDestroy(): void;
    dismiss(): void;
    confirmAction(): void;
    policiesExtensionSelected(policies: AssociatedRisk[]): void;
}
