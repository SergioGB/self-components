import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { Events, ModalController, NavParams, ViewController } from 'ionic-angular';
import { InsuranceProvider } from '../../../../providers/insurance/insurance';
import { PolicyExtensionComponent } from '../../../policy-extension/policy-extension';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
var PolicyExtensionModalComponent = (function () {
    function PolicyExtensionModalComponent(viewCtrl, modalProvider, modalCtrl, navParams, insuranceProvider, events, translationProvider, elementRef) {
        this.viewCtrl = viewCtrl;
        this.modalProvider = modalProvider;
        this.modalCtrl = modalCtrl;
        this.navParams = navParams;
        this.insuranceProvider = insuranceProvider;
        this.events = events;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.translationProvider.bind(this);
        this.labels = this.navParams.get('labels');
        this.mocked = this.navParams.get('mocked');
        this.policyId = this.navParams.get('policyId');
        this.modalProvider.setupCentering(this.elementRef);
    }
    PolicyExtensionModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.events.unsubscribe(PolicyExtensionComponent.readyEvent);
        this.events.subscribe(PolicyExtensionComponent.readyEvent, function () {
            _this.modalProvider.setupCentering(_this.elementRef);
        });
        this.events.unsubscribe(PolicyExtensionComponent.extensionEvent);
        this.events.subscribe(PolicyExtensionComponent.extensionEvent, function (policies) {
            _this.policiesExtensionSelected(policies);
        });
    };
    PolicyExtensionModalComponent.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(PolicyExtensionComponent.readyEvent);
        this.events.unsubscribe(PolicyExtensionComponent.extensionEvent);
    };
    PolicyExtensionModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    PolicyExtensionModalComponent.prototype.confirmAction = function () {
        this.viewCtrl.dismiss(true);
    };
    PolicyExtensionModalComponent.prototype.policiesExtensionSelected = function (policies) {
        this.policiesExtension = policies;
    };
    PolicyExtensionModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'policy-extension-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>   <policy-extension [mocked]=\"true\" [policyId]=\"policyId\"></policy-extension>   <ion-card-content class=\"no-padding-top\">     <ion-grid class=\"generic-buttons\">       <ion-row>          <ion-col col-6 col-sm-5 col-md-3>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"dismiss()\">             {{ translate(labels.cancel) }}           </button>         </ion-col>          <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">           <button ion-button full text-uppercase (click)=\"confirmAction()\">             {{ translate(labels.apply) }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content> </ion-card>"
                },] },
    ];
    PolicyExtensionModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ModalProvider, },
        { type: ModalController, },
        { type: NavParams, },
        { type: InsuranceProvider, },
        { type: Events, },
        { type: TranslationProvider, },
        { type: ElementRef, },
    ]; };
    return PolicyExtensionModalComponent;
}());
export { PolicyExtensionModalComponent };
//# sourceMappingURL=policy-extension-modal.js.map