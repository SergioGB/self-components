import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { NavParams, ViewController, Events } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { CancellationReason } from '../../../../models/cancellation_reason';
import { ReasonsCommentsComponent } from '../../../reasons-comments/reasons-comments';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var RefuseRenewalModalComponent = (function () {
    function RefuseRenewalModalComponent(componentSettingsProvider, viewCtrl, modalProvider, events, navParams, translationProvider, elementRef) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.viewCtrl = viewCtrl;
        this.modalProvider = modalProvider;
        this.events = events;
        this.navParams = navParams;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.componentReady = false;
        this.labels = navParams.get('labels');
        this.step = navParams.get('page') || 1;
        if (this.step === 3) {
            this.invokeDeletePolicy();
            this.componentReady = true;
        }
        else {
            this.componentReady = false;
        }
        this.reason = new CancellationReason();
        this.isSelectedReason = false;
        this.translationProvider.bind(this);
    }
    RefuseRenewalModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.modalProvider.setupCentering(this.elementRef);
        this.events.unsubscribe(ReasonsCommentsComponent.readyEvent);
        this.events.subscribe(ReasonsCommentsComponent.readyEvent, function () {
            _this.modalProvider.setupCentering(_this.elementRef);
            _this.componentReady = true;
        });
        this.events.unsubscribe(ReasonsCommentsComponent.selectedEvent);
        this.events.subscribe(ReasonsCommentsComponent.selectedEvent, function (selection) {
            _this.setReasonsComments(selection);
        });
    };
    RefuseRenewalModalComponent.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(ReasonsCommentsComponent.readyEvent);
        this.events.unsubscribe(ReasonsCommentsComponent.selectedEvent);
    };
    RefuseRenewalModalComponent.prototype.dismiss = function (params) {
        this.viewCtrl.dismiss(params);
        this.events.unsubscribe('reasons-comments:ready');
    };
    RefuseRenewalModalComponent.prototype.ngAfterViewInit = function () {
        this.modalProvider.setupCentering(this.elementRef);
    };
    RefuseRenewalModalComponent.prototype.confirmAction = function () {
        switch (this.step) {
            case 1:
                if (this.reason.navigation_info && this.reason.navigation_info.page_ref) {
                    this.step = 2;
                }
                else {
                    this.step = 4;
                }
                this.modalProvider.setupCentering(this.elementRef);
                break;
            case 2:
                if (this.reason.navigation_info && this.reason.navigation_info.page_ref) {
                    this.dismiss(this.reason.navigation_info.page_ref);
                }
                break;
            case 3:
                break;
            case 4:
                break;
            default:
                this.dismiss();
        }
    };
    RefuseRenewalModalComponent.prototype.cancelAction = function () {
        switch (this.step) {
            case 1:
                this.dismiss();
                break;
            case 2:
                this.invokeDeletePolicy();
                this.step = 3;
                this.modalProvider.setupCentering(this.elementRef);
                break;
            default:
                this.dismiss();
        }
    };
    RefuseRenewalModalComponent.prototype.callmeAction = function () {
        this.dismiss(this.reason.navigation_info.page_ref);
    };
    RefuseRenewalModalComponent.prototype.invokeDeletePolicy = function () {
    };
    RefuseRenewalModalComponent.prototype.showButton = function (buttonRef) {
        switch (this.step) {
            case 1:
                switch (buttonRef) {
                    case 'continue':
                        return true;
                    case 'cancel':
                        return true;
                    case 'callme':
                        return false;
                    default:
                        return false;
                }
            case 2:
                switch (buttonRef) {
                    case 'continue':
                        return (this.reason.navigation_info && this.reason.navigation_info.page_ref
                            && !this.reason.navigation_info.page_ref.view_name.includes('UserInactiveModalComponent'));
                    case 'cancel':
                        return true;
                    case 'callme':
                        return (this.reason.navigation_info && this.reason.navigation_info.page_ref
                            && this.reason.navigation_info.page_ref.view_name.includes('UserInactiveModalComponent'));
                    default:
                        return false;
                }
            case 3:
                switch (buttonRef) {
                    case 'continue':
                        return false;
                    case 'cancel':
                        return false;
                    case 'callme':
                        return false;
                    default:
                        return false;
                }
            default:
                return false;
        }
    };
    RefuseRenewalModalComponent.prototype.canContinue = function () {
        return this.isSelectedReason;
    };
    RefuseRenewalModalComponent.prototype.setReasonsComments = function (response) {
        if (response.reason) {
            this.reason.business_parameter = response.reason;
        }
        if (response.navigation_info) {
            this.reason.navigation_info = response.navigation_info;
        }
        this.isSelectedReason = true;
    };
    RefuseRenewalModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'refuse-renewal-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin [hidden]=\"!componentReady\">   <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\"    (tap)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>   <ion-card-content class=\"check-green\">     <div class=\"modal-description\">       <div class=\"section-wrapper\" *ngIf=\"step === 1\">         <reasons-comments           [title]=\"translate(labels.title)\"           [subtitle]=\"translate(labels.subtitle)\"           [noVerticalPadding]=\"true\"           [showInputReasons]=\"false\"           [noPaddingContent]=\"true\"           [noPaddingTop]=\"true\">         </reasons-comments>       </div>       <div class=\"section-wrapper\" *ngIf=\"step === 2\">         <information [icon]=\"'reason.navigation_info.icon'\" [title]=\"reason.navigation_info.title\" [subtitle]=\"reason.navigation_info.description\"></information>       </div>       <div class=\"section-wrapper\" *ngIf=\"step === 3\">         <information [icon]=\"'mapfre-sad-face'\" [title]=\"translate(labels.lastview_title)\" [subtitle]=\"translate(labels.lastview_subtitle)\"></information>       </div>       <div class=\"section-wrapper\" *ngIf=\"step === 4\">         <information [icon]=\"'mapfre-heart-hands'\" [title]=\"translate(labels.thanks_title)\" [subtitle]=\"translate(labels.thanks_subtitle)\"></information>       </div>     </div>      <hr *ngIf=\"step === 1 ||\u00A0step == 2\" class=\"no-margin-top margin-bottom-l\">      <ion-grid class=\"generic-buttons no-padding\" *ngIf=\"step !== 3\">       <ion-row>         <ion-col col-6 col-md-3>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"cancelAction()\" [hidden]=\"!showButton('cancel')\">             {{ translate(labels.cancel_button) }}           </button>         </ion-col>         <ion-col col-6 col-md-3 class=\"margin-left-auto\">           <button ion-button full text-uppercase (click)=\"confirmAction()\" [disabled]=\"!canContinue()\" [hidden]=\"!showButton('continue')\">             {{ translate(labels.continue_button) }}           </button>           <button ion-button full text-uppercase (click)=\"callmeAction()\" [disabled]=\"!canContinue()\" [hidden]=\"!showButton('callme')\">             {{ translate(labels.callme_button) }}           </button>         </ion-col>       </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    RefuseRenewalModalComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: ViewController, },
        { type: ModalProvider, },
        { type: Events, },
        { type: NavParams, },
        { type: TranslationProvider, },
        { type: ElementRef, },
    ]; };
    return RefuseRenewalModalComponent;
}());
export { RefuseRenewalModalComponent };
//# sourceMappingURL=refuse-renewal-modal.js.map