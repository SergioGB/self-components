import { ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../../providers/translation/translation';
export declare class PolicyUpdateFailureModalComponent {
    private viewCtrl;
    private elementRef;
    private modalProvider;
    private translationProvider;
    constructor(viewCtrl: ViewController, elementRef: ElementRef, modalProvider: ModalProvider, translationProvider: TranslationProvider);
    dismiss(): void;
}
