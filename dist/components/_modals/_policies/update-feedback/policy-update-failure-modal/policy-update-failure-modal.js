import { Component, ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../../providers/translation/translation';
var PolicyUpdateFailureModalComponent = (function () {
    function PolicyUpdateFailureModalComponent(viewCtrl, elementRef, modalProvider, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
    }
    PolicyUpdateFailureModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PolicyUpdateFailureModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'policy-update-failure',
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>      <div text-center>       <ion-icon name=\"mapfre-close\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate('policy-update-failure-modal.policy_update_feedback.failure.title') }}       </h3>        <div class=\"modal-description\">         {{ translate('policy-update-failure-modal.policy_update_feedback.failure.subtitle') }}       </div>     </div>    </ion-card-content>  </ion-card>"
                },] },
    ];
    PolicyUpdateFailureModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
    ]; };
    return PolicyUpdateFailureModalComponent;
}());
export { PolicyUpdateFailureModalComponent };
//# sourceMappingURL=policy-update-failure-modal.js.map