import { ElementRef } from '@angular/core';
import { Events, ViewController, NavParams } from 'ionic-angular';
import { ModalProvider } from '../../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../../providers/translation/translation';
export declare class PolicyUpdateSuccessWithPaymentModalComponent {
    private viewCtrl;
    private events;
    private elementRef;
    private modalProvider;
    private navParams;
    private translationProvider;
    labels: any;
    static componentName: string;
    static cancellationEvent: string;
    static updateEvent: string;
    constructor(viewCtrl: ViewController, events: Events, elementRef: ElementRef, modalProvider: ModalProvider, navParams: NavParams, translationProvider: TranslationProvider);
    dismiss(): void;
    cancel(): void;
    agree(): void;
}
