import { Component, ElementRef } from '@angular/core';
import { Events, ViewController, NavParams } from 'ionic-angular';
import { ModalProvider } from '../../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../../providers/translation/translation';
var PolicyUpdateSuccessWithPaymentModalComponent = (function () {
    function PolicyUpdateSuccessWithPaymentModalComponent(viewCtrl, events, elementRef, modalProvider, navParams, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.events = events;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.navParams = navParams;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.labels = this.navParams.get('labels');
        this.modalProvider.setupCentering(this.elementRef);
    }
    PolicyUpdateSuccessWithPaymentModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PolicyUpdateSuccessWithPaymentModalComponent.prototype.cancel = function () {
        this.events.publish(PolicyUpdateSuccessWithPaymentModalComponent.cancellationEvent);
        this.viewCtrl.dismiss(false);
    };
    PolicyUpdateSuccessWithPaymentModalComponent.prototype.agree = function () {
        this.events.publish(PolicyUpdateSuccessWithPaymentModalComponent.updateEvent);
        this.viewCtrl.dismiss(true);
    };
    PolicyUpdateSuccessWithPaymentModalComponent.componentName = 'policy-update';
    PolicyUpdateSuccessWithPaymentModalComponent.cancellationEvent = PolicyUpdateSuccessWithPaymentModalComponent.componentName + ":cancellation";
    PolicyUpdateSuccessWithPaymentModalComponent.updateEvent = PolicyUpdateSuccessWithPaymentModalComponent.componentName + ":update";
    PolicyUpdateSuccessWithPaymentModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'policy-update-success-with-payment',
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content class=\"bottom-border border-only\">      <div text-center>       <ion-icon name=\"mapfre-warning\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate(labels.title) }}       </h3>        <div class=\"modal-description\">         {{ translate(labels.subtitle) }}       </div>     </div>    </ion-card-content>    <ion-card-content>      <ion-grid class=\"no-padding generic-buttons\">       <ion-row class=\"no-padding\">          <ion-col col-6 col-sm-4 col-md-3>           <button ion-button full text-uppercase class=\"no-vertical-margin secondary\" (click)=\"cancel()\">             {{ translate(labels.cancel_button) }}           </button>         </ion-col>          <ion-col col-6 col-sm-4 col-md-3 class=\"margin-left-auto\">           <button ion-button full text-uppercase class=\"no-vertical-margin\" (click)=\"agree()\">             {{ translate(labels.continue_button) }}           </button>         </ion-col>        </ion-row>     </ion-grid>    </ion-card-content>  </ion-card>"
                },] },
    ];
    PolicyUpdateSuccessWithPaymentModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: Events, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: NavParams, },
        { type: TranslationProvider, },
    ]; };
    return PolicyUpdateSuccessWithPaymentModalComponent;
}());
export { PolicyUpdateSuccessWithPaymentModalComponent };
//# sourceMappingURL=policy-update-success-with-payment-modal.js.map