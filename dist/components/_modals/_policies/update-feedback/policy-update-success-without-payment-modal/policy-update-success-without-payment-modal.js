import { Component, ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../../providers/translation/translation';
var PolicyUpdateSuccessWithoutPaymentModalComponent = (function () {
    function PolicyUpdateSuccessWithoutPaymentModalComponent(viewCtrl, elementRef, modalProvider, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
    }
    PolicyUpdateSuccessWithoutPaymentModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PolicyUpdateSuccessWithoutPaymentModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'policy-update-success-without-payment',
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>      <div text-center>       <confirmation-check></confirmation-check>        <h3 class=\"modal-title\">         {{ translate('policy-update-success-without-payment-modal.policy_update_feedback.success.without_payment.title') }}       </h3>        <div class=\"modal-description\" *ngIf=\"false\">         {{ translate('policy-update-success-without-payment-modal.policy_update_feedback.success.without_payment.subtitle') }}       </div>     </div>    </ion-card-content>  </ion-card>"
                },] },
    ];
    PolicyUpdateSuccessWithoutPaymentModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
    ]; };
    return PolicyUpdateSuccessWithoutPaymentModalComponent;
}());
export { PolicyUpdateSuccessWithoutPaymentModalComponent };
//# sourceMappingURL=policy-update-success-without-payment-modal.js.map