import { ElementRef, EventEmitter, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { ReceiptPaymentProvider } from '../../../../providers/receipt-payment/receipt-payment';
import { AssociatedReceipt } from '../../../../models/associatedReceipt';
import { AssociatedRisk } from '../../../../models/associatedRisk';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class AddReceiptsToPayModalComponent implements OnInit {
    private componentSettingsProvider;
    private viewCtrl;
    private navCtrl;
    private navParams;
    private translationProvider;
    private receiptPaymentProvider;
    private elementRef;
    private modalProvider;
    onPoliciesExtension: EventEmitter<AssociatedRisk[]>;
    private mocked;
    riskId: string;
    labels: any;
    originalReceipt: any;
    receiptsExtension: AssociatedReceipt[];
    amount: number;
    risks: any;
    receipts: any;
    receiptSelected: any;
    ready: boolean;
    constructor(componentSettingsProvider: ComponentSettingsProvider, viewCtrl: ViewController, navCtrl: NavController, navParams: NavParams, translationProvider: TranslationProvider, receiptPaymentProvider: ReceiptPaymentProvider, elementRef: ElementRef, modalProvider: ModalProvider);
    ngOnInit(): void;
    changeCheck(receipt: any, policy: any, value: boolean): void;
    dismiss(): void;
    onReceiptSelected(): void;
}
