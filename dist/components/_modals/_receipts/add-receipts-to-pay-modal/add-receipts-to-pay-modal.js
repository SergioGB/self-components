import { Component, ElementRef, EventEmitter, Output } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { ReceiptPaymentProvider } from '../../../../providers/receipt-payment/receipt-payment';
import { AssociatedReceipt } from '../../../../models/associatedReceipt';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var AddReceiptsToPayModalComponent = (function () {
    function AddReceiptsToPayModalComponent(componentSettingsProvider, viewCtrl, navCtrl, navParams, translationProvider, receiptPaymentProvider, elementRef, modalProvider) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translationProvider = translationProvider;
        this.receiptPaymentProvider = receiptPaymentProvider;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.onPoliciesExtension = new EventEmitter();
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
    }
    AddReceiptsToPayModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.receiptsExtension = [];
        this.labels = this.navParams.get('labels');
        this.mocked = this.navParams.get('mocked');
        this.riskId = this.navParams.get('riskId');
        this.originalReceipt = this.navParams.get('receipt');
        Observable.forkJoin([
            this.componentSettingsProvider.getInsuranceSettings(this.mocked),
            this.componentSettingsProvider.getUnpaidClientReceipts(this.mocked)
        ]).subscribe(function (responses) {
            if (_this.componentSettingsProvider.areResponsesValid(responses)) {
                _this.risks = _this.componentSettingsProvider.getResponseJSON(responses[0]).data;
                _this.receipts = _this.componentSettingsProvider.getResponseJSON(responses[1]).data;
                for (var _i = 0, _a = _this.receipts; _i < _a.length; _i++) {
                    var receipt = _a[_i];
                    receipt.risk_name = 'Audi A1';
                }
            }
            _this.ready = true;
            _this.modalProvider.setupCentering(_this.elementRef);
        });
    };
    AddReceiptsToPayModalComponent.prototype.changeCheck = function (receipt, policy, value) {
        receipt.isOn = value;
        var receiptExtension = new AssociatedReceipt(receipt.id, policy.id);
        if (value) {
            this.receiptSelected = receipt;
            this.receiptsExtension.push(receipt);
            this.onPoliciesExtension.emit(this.receiptsExtension);
            this.amount = policy.amount;
        }
        else {
            var index = -1;
            for (var _i = 0, _a = this.receiptsExtension; _i < _a.length; _i++) {
                var entry = _a[_i];
                if (entry.policy_id === receiptExtension.policy_id && entry.receipt_id === receiptExtension.receipt_id) {
                    index = this.receiptsExtension.indexOf(entry);
                    break;
                }
            }
            if (index !== -1) {
                this.receiptsExtension.splice(index, 1);
            }
            this.onPoliciesExtension.emit(this.receiptsExtension);
        }
    };
    AddReceiptsToPayModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    AddReceiptsToPayModalComponent.prototype.onReceiptSelected = function () {
        this.receiptPaymentProvider.setReceiptSelectedData(this.originalReceipt);
        this.receiptPaymentProvider.setExtraReceipts(this.receiptsExtension);
        this.viewCtrl.dismiss(true);
    };
    AddReceiptsToPayModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'add-receipts-to-pay',
                    template: "<ion-card *ngIf=\"ready\" no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>   <ion-card-header class=\"no-border\">     <ion-item class=\"no-padding margin-top-s\">       <h3>         {{ translate(labels.title) }}       </h3>     </ion-item>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>   </ion-card-header>   <hr class=\"margin-left-xl margin-right-xl margin-top-s\">   <ion-card-content *ngIf=\"receipts\" class=\"no-padding-top check-green\">     <ion-grid class=\"actions-wrapper no-padding\">       <div>         <p class=\"fmm-body1 fmm-regular\">           {{ translate(labels.warning) }}         </p>       </div>     </ion-grid>      <ion-grid class=\"actions-wrapper no-padding\">       <div *ngFor=\"let receipt of receipts\" class=\" padding-right-m\">         <ion-item no-padding class=\"flex align-start no-label-margin vertical-margin-s\">           <ion-icon             item-start class=\"clickable\"             [name]=\"receipt.isOn ? 'mapfre-checkbox-on' : 'mapfre-checkbox-off'\"             (click)=\"changeCheck(receipt, receipt, !receipt.isOn)\">           </ion-icon>            <div class=\"margin-left-s\">             <h3 class=\"no-margin-top margin-bottom-s\">{{ receipt.price.amount | currency:receipt.price.iso_code:true:'1.2-2'}}</h3>             <p class=\"fmm-body1 fmm-regular\">{{ receipt.risk_name }}</p>           </div>         </ion-item>       </div>     </ion-grid>     <hr>     <ion-grid class=\"actions-wrapper generic-buttons no-padding\">         <ion-row class=\"padding-top-s padding-bottom-s\">            <ion-col col-6 col-sm-5 col-md-3>             <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"dismiss()\">               {{ translate(labels.cancel) }}             </button>           </ion-col>            <ion-col col-6 col-sm-5 col-md-3>             <button ion-button full text-uppercase (click)=\"onReceiptSelected()\">               {{ translate(labels.add) }}             </button>           </ion-col>          </ion-row>     </ion-grid>   </ion-card-content> </ion-card>"
                },] },
    ];
    AddReceiptsToPayModalComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: ViewController, },
        { type: NavController, },
        { type: NavParams, },
        { type: TranslationProvider, },
        { type: ReceiptPaymentProvider, },
        { type: ElementRef, },
        { type: ModalProvider, },
    ]; };
    AddReceiptsToPayModalComponent.propDecorators = {
        'onPoliciesExtension': [{ type: Output },],
    };
    return AddReceiptsToPayModalComponent;
}());
export { AddReceiptsToPayModalComponent };
//# sourceMappingURL=add-receipts-to-pay-modal.js.map