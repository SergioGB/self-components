import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { Events, ModalController, NavParams, ViewController } from 'ionic-angular';
import { ProcessFailureComponent } from '../../../process-failure/process-failure';
import { SplitAmountComponent } from '../../../split-amount/split-amount';
import { PlatformProvider } from '../../../../providers/platform/platform';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var AmountDivisionsModalComponent = (function () {
    function AmountDivisionsModalComponent(elementRef, navParams, viewCtrl, modalController, platformProvider, translationProvider, modalProvider, events) {
        var _this = this;
        this.elementRef = elementRef;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.modalController = modalController;
        this.platformProvider = platformProvider;
        this.translationProvider = translationProvider;
        this.modalProvider = modalProvider;
        this.events = events;
        this.translationProvider.bind(this);
        this.platformProvider.bindTo(this);
        this.modalProvider.center(this.elementRef);
        setTimeout(function () {
            _this.modalProvider.center(_this.elementRef);
        }, 1000);
    }
    AmountDivisionsModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.labels = this.navParams.get('labels');
        this.mocked = this.navParams.get('mocked');
        this.cancelComp = false;
        this.confirmComp = false;
        this.icon = 'mapfre-cross-error';
        this.title = this.translationProvider.getValueForKey('amount-divisions-modal.my_receipts.action.split.cancel');
        this.events.unsubscribe(ProcessFailureComponent.callMeEvent);
        this.events.subscribe(ProcessFailureComponent.callMeEvent, function (data) {
            _this.callMeOpened();
        });
        this.events.unsubscribe(SplitAmountComponent.loadEvent);
        this.events.subscribe(SplitAmountComponent.loadEvent, function () {
            _this.readyComponent();
        });
    };
    AmountDivisionsModalComponent.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(ProcessFailureComponent.callMeEvent);
        this.events.unsubscribe(SplitAmountComponent.loadEvent);
    };
    AmountDivisionsModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    AmountDivisionsModalComponent.prototype.accept = function () {
        this.modalProvider.setupCentering(this.elementRef);
        this.confirmComp = true;
    };
    AmountDivisionsModalComponent.prototype.cancel = function () {
        this.modalProvider.setupCentering(this.elementRef);
        this.cancelComp = true;
    };
    AmountDivisionsModalComponent.prototype.readyComponent = function () {
        this.modalProvider.setupCentering(this.elementRef);
        this.ready = true;
    };
    AmountDivisionsModalComponent.prototype.callMeOpened = function () {
        this.viewCtrl.dismiss(true);
    };
    AmountDivisionsModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'amount-divisions-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["amount-divisions-modal .button-center {   align-items: center; }"],
                    template: "<ion-card no-margin [hidden]=\"!ready\">   <ion-card-content *ngIf=\"!cancelComp && !confirmComp\" class=\"padding-bottom-s\">     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (click)=\"dismiss()\"></ion-icon>     <split-amount [mocked]=\"mocked\" [receiptId]=\"1\"></split-amount>      <hr class=\"margin-top-m margin-bottom-s\">      <ion-grid class=\"generic-buttons actions-wrapper no-padding\">       <ion-row class=\"padding-top-s padding-bottom-s\">          <ion-col col-6 col-sm-5 col-md-3>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"cancel()\">             {{ translate(labels.cancel) }}           </button>         </ion-col>          <ion-col col-6 col-sm-5 col-md-3>           <button ion-button full text-uppercase class=\"margin-left-auto\" (click)=\"accept()\">             {{ translate(labels.confirm) }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content>   <ion-card-content *ngIf=\"cancelComp && icon && title\">     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (click)=\"dismiss()\"></ion-icon>      <process-failure       [icon]=\"icon\"       [title]=\"title\"       [showCallMe]=\"false\">     </process-failure>   </ion-card-content>    <ion-card-content *ngIf=\"confirmComp && icon && title\">     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (click)=\"dismiss()\"></ion-icon>      <process-confirmation       [icon]=\"'mapfre-check'\"       [ratingText]=\"' '\"       [flowReference]=\"true\"       [title]=\"translate(labels.title)\">     </process-confirmation>      <ion-grid class=\"actions-wrapper no-padding\">       <ion-row class=\"padding-top-s padding-bottom-s\">         <ion-col col-12 col-sm-4 push-sm-4 text-center>           <button ion-button full text-uppercase (click)=\"dismiss()\">             {{ translate(labels.finalize) }}           </button>         </ion-col>       </ion-row>     </ion-grid>   </ion-card-content> </ion-card>"
                },] },
    ];
    AmountDivisionsModalComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: NavParams, },
        { type: ViewController, },
        { type: ModalController, },
        { type: PlatformProvider, },
        { type: TranslationProvider, },
        { type: ModalProvider, },
        { type: Events, },
    ]; };
    return AmountDivisionsModalComponent;
}());
export { AmountDivisionsModalComponent };
//# sourceMappingURL=amount-divisions-modal.js.map