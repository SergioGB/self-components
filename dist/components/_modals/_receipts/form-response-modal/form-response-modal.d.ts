import { ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class FormResponseModalComponent {
    private viewCtrl;
    private modalProvider;
    private elementRef;
    private translationProvider;
    constructor(viewCtrl: ViewController, modalProvider: ModalProvider, elementRef: ElementRef, translationProvider: TranslationProvider);
    dismiss(): void;
}
