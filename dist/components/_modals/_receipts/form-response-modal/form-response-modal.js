import { Component, ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
var FormResponseModalComponent = (function () {
    function FormResponseModalComponent(viewCtrl, modalProvider, elementRef, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.modalProvider = modalProvider;
        this.elementRef = elementRef;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
    }
    FormResponseModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    FormResponseModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'form-response',
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>      <div text-center>       <ion-icon name=\"mapfre-rocket\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate('form-response-modal.change_payment.form.title') }}       </h3>        <div class=\"modal-description\">         {{ translate('form-response-modal.change_payment.form.subtitle') }}       </div>      </div>   </ion-card-content>  </ion-card>"
                },] },
    ];
    FormResponseModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ModalProvider, },
        { type: ElementRef, },
        { type: TranslationProvider, },
    ]; };
    return FormResponseModalComponent;
}());
export { FormResponseModalComponent };
//# sourceMappingURL=form-response-modal.js.map