import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events } from 'ionic-angular/util/events';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { NavParams } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { UserProvider } from '../../../../providers/user/user';
import { PasswordChangeRequest } from '../../../../models/login/passwordChangeRequest';
import { ChangeBlockedUserPasswordChangeComponent } from '../../../personal-info/password-and-security-questions-change/change-blocked-user/change-blocked-user-password-change/change-blocked-user-password-change';
import { ChangedPrivacyForgotPasswordComponent } from '../../../personal-info/password-and-security-questions-change/changed-privacy-forgot-password/changed-privacy-forgot-password';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var ChangedPasswordModalComponent = (function () {
    function ChangedPasswordModalComponent(viewCtrl, navParams, events, elementRef, formBuilder, translationProvider, modalProvider, componentSettingsProvider, userProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.events = events;
        this.elementRef = elementRef;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.modalProvider = modalProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.userProvider = userProvider;
        this.errorGotten = false;
        this.passwordsMismatched = false;
        this.changePasswordNeeded = false;
        this.translationProvider.bind(this);
        this.labels = this.navParams.get('labels');
        this.mocked = this.navParams.get('mocked');
        this.changePasswordForm = this.formBuilder.group({
            oldPassword: ['', Validators.required],
            password: ['', Validators.required],
            validationPassword: ['', [Validators.required, this.passwordValidator]]
        }, {
            validator: this.passwordsValidator('password', 'validationPassword')
        });
        this.modalProvider.setupCentering(this.elementRef);
    }
    ChangedPasswordModalComponent.prototype.ngOnDestroy = function () {
        this._unsubscribeMethods();
    };
    ChangedPasswordModalComponent.prototype.ngOnInit = function () {
        this._unsubscribeMethods();
        this._subscribeMethods();
    };
    ChangedPasswordModalComponent.prototype._subscribeMethods = function () {
        var _this = this;
        this.events.subscribe(ChangeBlockedUserPasswordChangeComponent.onCloseEvent, function () { return _this.dismiss(); });
        this.events.subscribe(ChangedPrivacyForgotPasswordComponent.passwordChangeEvent, function () { return _this.dismiss(); });
        this.events.subscribe(ChangedPrivacyForgotPasswordComponent.created, function () { return _this.dismiss(); });
        this.events.subscribe('closeModal', function () {
            _this.dismiss();
        });
    };
    ChangedPasswordModalComponent.prototype._unsubscribeMethods = function () {
        this.events.subscribe(ChangeBlockedUserPasswordChangeComponent.onCloseEvent);
        this.events.subscribe(ChangedPrivacyForgotPasswordComponent.passwordChangeEvent);
        this.events.subscribe(ChangedPrivacyForgotPasswordComponent.created);
        this.events.subscribe('closeModal');
    };
    ChangedPasswordModalComponent.prototype.onCurrentPasswordBlur = function () {
        var _this = this;
        var loginRequest = {
            user_pass_login: {
                user: this.userProvider.getUsername(),
                password: this.changePasswordForm.value.oldPassword
            },
            rememberMe: false
        };
        this.userProvider.sendLoginRequest(this.mocked, loginRequest).subscribe(function (response) {
            _this.errorGotten = !response.data.user_token;
        });
    };
    ChangedPasswordModalComponent.prototype.onPasswordBlur = function () {
        var password = this.changePasswordForm.value.password;
        var confirmPassword = this.changePasswordForm.value.validationPassword;
        this.passwordsMismatched = password && confirmPassword && password !== confirmPassword;
    };
    ChangedPasswordModalComponent.prototype.passwordsValidator = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value && confirmPassword.value && password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    ChangedPasswordModalComponent.prototype.passwordValidator = function (input) {
        if (input.value === input.root.value['password']) {
            return null;
        }
        else {
            return {
                isValid: true
            };
        }
    };
    ChangedPasswordModalComponent.prototype.forgotPassword = function () {
    };
    ChangedPasswordModalComponent.prototype.dismiss = function (event) {
        if (event) {
            event.preventDefault();
        }
        this.viewCtrl.dismiss();
    };
    ChangedPasswordModalComponent.prototype.toggleChangePassword = function () {
        this.changePasswordNeeded = !this.changePasswordNeeded;
    };
    ChangedPasswordModalComponent.prototype.attemptPasswordChange = function () {
        var _this = this;
        var user = this.userProvider.getUserData();
        this.componentSettingsProvider.getClientsSettings(this.mocked, user.name).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                var userData = mapfreResponse.data[0];
                var userPut = new PasswordChangeRequest(userData.username, userData.name, userData.surname1, userData.surname2, userData.identification_number, userData.mobile_phone, userData.mail, userData.address, _this.changePasswordForm.value.password, userData.security_questions);
                _this.userProvider.sendPasswordChange(userPut, userData.id).subscribe(function (response) {
                    var mapfreResponse = _this.userProvider.getResponseJSON(response);
                    if (_this.userProvider.isMapfreResponseValid(mapfreResponse)) {
                        _this.dismiss();
                    }
                });
            }
        });
    };
    ChangedPasswordModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'changed-password-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>   <form [formGroup]=\"changePasswordForm\" (ngSubmit)=\"dismiss($event)\">     <ion-card-content>       <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss($event)\"></ion-icon>        <div class=\"modal-content\" text-center *ngIf=\"!changePasswordNeeded\">         <h3 class=\"modal-title\">           {{ translate(labels.title) }}         </h3>          <ion-grid no-padding class=\"margin-top-m\">           <ion-row>             <ion-col col-12 col-md-8 push-md-2>               <div class=\"modal-description\">                 {{ translate(labels.subtitle) }}               </div>             </ion-col>           </ion-row>         </ion-grid>       </div>       <div *ngIf=\"!changePasswordNeeded\">         <ion-row>           <ion-col col-12 col-sm-6 offset-sm-3>             <ion-item no-padding class=\"modal-form-content required\">               <ion-label floating [class.red-text]=\"errorGotten\">                 {{ translate(labels.current_password) }}               </ion-label>               <span>                   {{ translate(labels.old_wrong) }}               </span>               <ion-input type=\"password\" formControlName=\"oldPassword\" (ionBlur)=\"onCurrentPasswordBlur()\" [class.invalid]=\"errorGotten\"></ion-input>             </ion-item>           </ion-col>         </ion-row>         <ion-row>             <ion-col col-12 col-sm-6 offset-sm-3 *ngIf=\"errorGotten\" class=\"input-value-error\">               <span>                   {{ translate(labels.old_wrong) }}               </span>             </ion-col>         </ion-row>         <ion-row>           <ion-col col-12 col-sm-6 offset-sm-3>             <div margin-top text-right class=\"forgot-password fmm-body1 font-s clickable\" tabindex=\"0\" (keyup.enter)=\"toggleChangePassword()\"                  (click)=\"toggleChangePassword()\">               <span>                 {{ translate(labels.forgot_password) }}               </span>               <ion-icon name=\"mapfre-arrow-right\" class=\"custom-size font-s primary\"></ion-icon>             </div>           </ion-col>         </ion-row>         <ion-row>           <ion-col col-12 col-sm-6 offset-sm-3>             <ion-item no-padding class=\"modal-form-content required\">               <ion-label floating [class.red-text]=\"errorGotten\">                 {{ translate(labels.new_password) }}               </ion-label>               <ion-input type=\"password\" formControlName=\"password\" [class.invalid]=\"errorGotten\"></ion-input>             </ion-item>           </ion-col>         </ion-row>         <ion-row>           <ion-col col-12 col-sm-6 offset-sm-3>             <ion-item no-padding class=\"modal-form-content required\">               <ion-label floating [class.red-text]=\"errorGotten\">                 {{ translate(labels.repeat_password) }}               </ion-label>               <ion-input type=\"password\" formControlName=\"validationPassword\" (ionBlur)=\"onPasswordBlur()\" [class.invalid]=\"errorGotten\"></ion-input>             </ion-item>           </ion-col>         </ion-row>          <ion-row>           <ion-col col-12 col-sm-6 offset-sm-3 *ngIf=\"passwordsMismatched\" class=\"input-value-error\">             <span>               {{ translate(labels.passwords_mismatch) }}             </span>           </ion-col>         </ion-row>                </div>       <ion-grid class=\"actions-wrapper padding-top\" *ngIf=\"!changePasswordNeeded\">         <ion-row>            <ion-col col-12 col-sm-5 col-md-3>             <button class=\"secondary\" ion-button full text-uppercase (click)=\"dismiss($event)\">               {{ translate(labels.cancel) }}             </button>           </ion-col>            <ion-col col-12 col-sm-5 col-md-3>             <button ion-button full text-uppercase (click)=\"attemptPasswordChange()\" [disabled]=\"!changePasswordForm.valid\">               {{ translate(labels.accept) }}             </button>           </ion-col>          </ion-row>       </ion-grid>              <div *ngIf=\"changePasswordNeeded\">         <changed-privacy-forgot-password >         </changed-privacy-forgot-password>       </div>     </ion-card-content>   </form> </ion-card>"
                },] },
    ];
    ChangedPasswordModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: NavParams, },
        { type: Events, },
        { type: ElementRef, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: ModalProvider, },
        { type: ComponentSettingsProvider, },
        { type: UserProvider, },
    ]; };
    return ChangedPasswordModalComponent;
}());
export { ChangedPasswordModalComponent };
//# sourceMappingURL=changed-password-modal.js.map