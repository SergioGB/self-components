import { ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { NavParams } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { UserProvider } from '../../../../providers/user/user';
import { ModalProvider } from '../../../../providers/modal/modal';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class ChangedSecurityQuestionModalComponent {
    private viewCtrl;
    private navParams;
    private elementRef;
    private formBuilder;
    private translationProvider;
    private modalProvider;
    private componentSettingsProvider;
    private userProvider;
    private mocked;
    private labels;
    securityQuestionsForm: FormGroup;
    securityQuestions: any[];
    ready: boolean;
    user: any;
    selectOptions: any;
    constructor(viewCtrl: ViewController, navParams: NavParams, elementRef: ElementRef, formBuilder: FormBuilder, translationProvider: TranslationProvider, modalProvider: ModalProvider, componentSettingsProvider: ComponentSettingsProvider, userProvider: UserProvider);
    dismiss(event?: Event): void;
    attemptChange(): void;
    private onChangeSuccess;
    private getSecurityQuestions;
    private initializeForm;
}
