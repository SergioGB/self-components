import { ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { NavParams } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class ChangesSuccessModalComponent {
    private viewCtrl;
    private navParams;
    private elementRef;
    private modalProvider;
    private translationProvider;
    private mocked;
    ready: boolean;
    title: string;
    constructor(viewCtrl: ViewController, navParams: NavParams, elementRef: ElementRef, modalProvider: ModalProvider, translationProvider: TranslationProvider);
    dismiss(): void;
}
