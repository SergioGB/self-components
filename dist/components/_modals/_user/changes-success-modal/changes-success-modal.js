import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { NavParams } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var ChangesSuccessModalComponent = (function () {
    function ChangesSuccessModalComponent(viewCtrl, navParams, elementRef, modalProvider, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.title = 'Cambios realizados con éxito';
        }
        else {
            this.title = this.navParams.get('title');
        }
        this.ready = true;
        this.modalProvider.setupCentering(this.elementRef);
    }
    ChangesSuccessModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ChangesSuccessModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'changes-success-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"ready\" no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>      <div text-center>       <confirmation-check></confirmation-check>        <h3 class=\"modal-title\">         {{ 'Cambios realizados con \u00E9xito' }}       </h3>     </div>    </ion-card-content>  </ion-card>"
                },] },
    ];
    ChangesSuccessModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: NavParams, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
    ]; };
    return ChangesSuccessModalComponent;
}());
export { ChangesSuccessModalComponent };
//# sourceMappingURL=changes-success-modal.js.map