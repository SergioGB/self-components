import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { UserProvider } from '../../../../providers/user/user';
import { ApiProvider } from '../../../../providers/api/api';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class CloseSessionModalComponent {
    private userProvider;
    private navParams;
    private viewCtrl;
    private apiProvider;
    private translationProvider;
    private elementRef;
    private modalProvider;
    private mocked;
    private labels;
    constructor(userProvider: UserProvider, navParams: NavParams, viewCtrl: ViewController, apiProvider: ApiProvider, translationProvider: TranslationProvider, elementRef: ElementRef, modalProvider: ModalProvider);
    dismiss(): void;
    logout(): void;
}
