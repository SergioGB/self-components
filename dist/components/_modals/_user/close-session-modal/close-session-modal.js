import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { UserProvider } from '../../../../providers/user/user';
import { ApiProvider } from '../../../../providers/api/api';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ModalProvider } from '../../../../providers/modal/modal';
var CloseSessionModalComponent = (function () {
    function CloseSessionModalComponent(userProvider, navParams, viewCtrl, apiProvider, translationProvider, elementRef, modalProvider) {
        this.userProvider = userProvider;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiProvider = apiProvider;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider.bind(this);
        this.mocked = this.navParams.get('mocked');
        this.labels = this.navParams.get('labels');
        this.modalProvider.setupCentering(this.elementRef);
    }
    CloseSessionModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    CloseSessionModalComponent.prototype.logout = function () {
        var _this = this;
        if (this.mocked) {
            this.viewCtrl.dismiss(true);
        }
        else {
            this.userProvider.getLogout().subscribe(function (response) {
                var mapfreResponse = _this.apiProvider.getResponseJSON(response);
            });
            this.viewCtrl.dismiss(true);
        }
    };
    CloseSessionModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'close-session-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["close-session-modal .actions-wrapper button {   padding-top: 10px;   padding-bottom: 10px;   height: 3.4rem;   contain: initial;   position: static; }   close-session-modal .actions-wrapper button:hover {     padding-top: 10px;     padding-bottom: 10px;     height: 3.4rem;     contain: initial;     position: static; }"],
                    template: "<ion-card no-margin>    <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>      <div class=\"modal-content bottom-border border-only\" text-center>       <ion-icon name=\"mapfre-door\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate(labels.title) }}       </h3>        <ion-grid no-padding class=\"margin-top-m\">         <ion-row>           <ion-col col-12 col-md-8 push-md-2>             <div class=\"modal-description\">               {{ translate(labels.subtitle) }}             </div>           </ion-col>         </ion-row>       </ion-grid>     </div>      <ion-grid class=\"actions-wrapper no-padding\">       <ion-row class=\"padding-top-s padding-bottom-s\">          <ion-col col-12 col-sm-5 col-md-3>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"dismiss()\">             {{ translate(labels.cancel_button) }}           </button>         </ion-col>          <ion-col col-12 col-sm-5 col-md-3>           <button ion-button full text-uppercase (click)=\"logout()\">             {{ translate(labels.accept_button) }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    CloseSessionModalComponent.ctorParameters = function () { return [
        { type: UserProvider, },
        { type: NavParams, },
        { type: ViewController, },
        { type: ApiProvider, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: ModalProvider, },
    ]; };
    return CloseSessionModalComponent;
}());
export { CloseSessionModalComponent };
//# sourceMappingURL=close-session-modal.js.map