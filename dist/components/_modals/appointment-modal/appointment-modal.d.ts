import { ElementRef } from '@angular/core';
import { App, NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class AppointmentModalComponent {
    private app;
    private viewCtrl;
    private navParams;
    private translationProvider;
    private elementRef;
    private modalProvider;
    private navCtrl;
    private mocked;
    private labels;
    appointment: any;
    constructor(app: App, viewCtrl: ViewController, navParams: NavParams, translationProvider: TranslationProvider, elementRef: ElementRef, modalProvider: ModalProvider, navCtrl: NavController);
    navigateToHome(): void;
    dismiss(): void;
}
