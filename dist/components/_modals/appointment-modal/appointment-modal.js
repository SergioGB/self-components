import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { App, NavController, NavParams, ViewController } from 'ionic-angular';
import { Mocks } from '../../../providers/mocks/mocks';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
var AppointmentModalComponent = (function () {
    function AppointmentModalComponent(app, viewCtrl, navParams, translationProvider, elementRef, modalProvider, navCtrl) {
        this.app = app;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.navCtrl = navCtrl;
        this.translationProvider.bind(this);
        this.labels = navParams.get('labels');
        this.mocked = navParams.get('mocked');
        if (this.mocked) {
            this.appointment = {
                phone: Mocks.getPhone()
            };
        }
        else {
            this.appointment = navParams.get('appointment');
        }
        this.modalProvider.setupCentering(this.elementRef);
    }
    AppointmentModalComponent.prototype.navigateToHome = function () {
        this.navCtrl.setRoot('GlobalPositionPage', {}, { animate: true, direction: 'forward' });
        this.dismiss();
    };
    AppointmentModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AppointmentModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'appointment-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin *ngIf=\"appointment\">    <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>      <div class=\"modal-content\" text-center>       <ion-icon name=\"mapfre-rocket\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate(labels.title) }}       </h3>        <div class=\"modal-description\">         {{ translate(labels.less_than) }}         <strong>{{ translate(labels.hours_48) }}</strong>         {{ translate(labels.will_call) }}         <strong>{{ appointment.phone }}</strong>         {{ translate(labels.appointment_time) }}       </div>     </div>      <ion-grid no-padding>       <ion-row>         <ion-col col-12 col-sm-5 col-md-4 no-padding class=\"margin-left-auto\">           <button             ion-button full text-uppercase no-margin class=\"bold\"             (click)=\"navigateToHome()\">             {{ translate(labels.home_button) }}           </button>         </ion-col>       </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    AppointmentModalComponent.ctorParameters = function () { return [
        { type: App, },
        { type: ViewController, },
        { type: NavParams, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: NavController, },
    ]; };
    return AppointmentModalComponent;
}());
export { AppointmentModalComponent };
//# sourceMappingURL=appointment-modal.js.map