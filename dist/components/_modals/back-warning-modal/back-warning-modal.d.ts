import { ElementRef } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { ApiProvider } from '../../../providers/api/api';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class BackWarningModalComponent {
    private userProvider;
    private viewCtrl;
    private apiProvider;
    private translationProvider;
    private elementRef;
    private navParams;
    private modalProvider;
    text: string;
    method: any;
    dataA: any;
    dataB: any;
    subtitle: any;
    constructor(userProvider: UserProvider, viewCtrl: ViewController, apiProvider: ApiProvider, translationProvider: TranslationProvider, elementRef: ElementRef, navParams: NavParams, modalProvider: ModalProvider);
    dismiss(): void;
}
