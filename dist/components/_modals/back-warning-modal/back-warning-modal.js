import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { ApiProvider } from '../../../providers/api/api';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
var BackWarningModalComponent = (function () {
    function BackWarningModalComponent(userProvider, viewCtrl, apiProvider, translationProvider, elementRef, navParams, modalProvider) {
        this.userProvider = userProvider;
        this.viewCtrl = viewCtrl;
        this.apiProvider = apiProvider;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.navParams = navParams;
        this.modalProvider = modalProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
        this.method = this.navParams.get('method');
        this.dataA = this.navParams.get('dataA');
        this.dataB = this.navParams.get('dataB');
        if (this.method === "1") {
            this.subtitle = this.translationProvider.getValueForKey('back-warning-modal.back_warning_modal.desc1')
                + this.translationProvider.getValueForKey('back-warning-modal.back_warning_modal.bank_account_l')
                + ' ' + this.dataA + ' '
                + this.translationProvider.getValueForKey('back-warning-modal.back_warning_modal.desc2');
        }
        else if (this.method === "2") {
            this.subtitle = this.translationProvider.getValueForKey('back-warning-modal.back_warning_modal.desc1')
                + this.translationProvider.getValueForKey('back-warning-modal.back_warning_modal.credit_card_l')
                + ' ' + this.dataA + ' '
                + this.translationProvider.getValueForKey('back-warning-modal.back_warning_modal.desc2');
        }
    }
    BackWarningModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    BackWarningModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'back-warning-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["back-warning-modal .text_entry {   text-align: left !important; }  back-warning-modal strong {   text-align: left !important; }"],
                    template: "<ion-card no-margin>    <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>     <div class=\"modal-content bottom-border border-only\" text-center>       <ion-icon name=\"mapfre-warning\" class=\"modal-icon\"></ion-icon>            <h3 class=\"modal-title\">         {{ translate('back-warning-modal.back_warning_modal.title') }}       </h3>            <ion-grid no-padding class=\"margin-top-m border-bottom\">         <ion-row>           <ion-col col-12 col-md-8 push-md-2>             <div class=\"modal-description\">               {{ subtitle }}             </div>           </ion-col>         </ion-row>       </ion-grid>     </div>      <ion-grid *ngIf=\"method === '1'\" class=\"border-top\">       <ion-row class=\"text_entry\">         <ion-col col-12 col-md-3>           <strong>             {{ translate('back-warning-modal.back_warning_modal.method') }}           </strong>         </ion-col>         <ion-col col-12 col-md-3 offset-md-1>           <div>             {{ translate('back-warning-modal.back_warning_modal.bank_account') }}           </div>         </ion-col>       </ion-row>       <ion-row class=\"text_entry\">         <ion-col col-12 col-md-3>           <strong>             {{ translate('back-warning-modal.back_warning_modal.account_number') }}           </strong>         </ion-col>         <ion-col col-12 col-md-3 offset-md-1>           <div>             {{ dataA }}           </div>         </ion-col>       </ion-row>       <ion-row class=\"text_entry\">         <ion-col col-12 col-md-3>           <strong>             {{ translate('back-warning-modal.back_warning_modal.amount') }}           </strong>         </ion-col>         <ion-col col-12 col-md-3 offset-md-1>           <div>             {{ dataB.amount | currency:dataB.iso_code:true:'1.2-2' }}           </div>         </ion-col>       </ion-row>     </ion-grid>     <ion-grid *ngIf=\"method === '2'\" class=\"border-top\">       <ion-row class=\"text_entry\">         <ion-col col-12 col-md-3>           <strong>             {{ translate('back-warning-modal.back_warning_modal.method') }}           </strong>         </ion-col>         <ion-col col-12 col-md-3 offset-md-1>           <div>             {{ translate('back-warning-modal.back_warning_modal.credit_card') }}           </div>         </ion-col>       </ion-row>       <ion-row class=\"text_entry\">         <ion-col col-12 col-md-3>           <strong>             {{ translate('back-warning-modal.back_warning_modal.card_owner') }}           </strong>         </ion-col>         <ion-col col-12 col-md-3 offset-md-1>           <div class=\"modal-description\">             {{ dataA }}           </div>         </ion-col>       </ion-row>       <ion-row class=\"text_entry\">         <ion-col col-12 col-md-3>           <strong>             {{ translate('back-warning-modal.back_warning_modal.card_number') }}           </strong>         </ion-col>         <ion-col col-12 col-md-3 offset-md-1>           <div class=\"modal-description\">             {{ dataB.amount | currency:dataB.iso_code:true:'1.2-2'  }}           </div>         </ion-col>       </ion-row>     </ion-grid>     <ion-grid class=\"generic-buttons\">       <ion-row>          <ion-col col-6 col-sm-5 col-md-3>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"dismiss()\">             {{ translate('back-warning-modal.generic.cancel') }}           </button>         </ion-col>          <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">           <button ion-button full text-uppercase (click)=\"dismiss()\">             {{ translate('back-warning-modal.generic.continue') }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    BackWarningModalComponent.ctorParameters = function () { return [
        { type: UserProvider, },
        { type: ViewController, },
        { type: ApiProvider, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: NavParams, },
        { type: ModalProvider, },
    ]; };
    return BackWarningModalComponent;
}());
export { BackWarningModalComponent };
//# sourceMappingURL=back-warning-modal.js.map