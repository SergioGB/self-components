import { ElementRef } from '@angular/core';
import { Events, NavParams, ViewController } from 'ionic-angular';
import { MapfreFile } from '../../../models/mapfreFile';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class FileUploaderModalComponent {
    private navParams;
    private viewCtrl;
    private modalProvider;
    private elementRef;
    private translationProvider;
    private events;
    private mocked;
    settings: any;
    static componentName: string;
    static uploadEvent: string;
    static downloadEvent: string;
    static childRemovalEvent: string;
    static confirmationEvent: string;
    constructor(navParams: NavParams, viewCtrl: ViewController, modalProvider: ModalProvider, elementRef: ElementRef, translationProvider: TranslationProvider, events: Events);
    dismiss(): void;
    confirm(): void;
    onChildUpload(mapfreFile: MapfreFile): void;
    onChildDownload(mapfreFile: MapfreFile): void;
    onChildRemoval(mapfreFile: MapfreFile): void;
    onConfirmation(): void;
    private center;
}
