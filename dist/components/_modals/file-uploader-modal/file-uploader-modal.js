import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { Events, NavParams, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
var FileUploaderModalComponent = (function () {
    function FileUploaderModalComponent(navParams, viewCtrl, modalProvider, elementRef, translationProvider, events) {
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.modalProvider = modalProvider;
        this.elementRef = elementRef;
        this.translationProvider = translationProvider;
        this.events = events;
        this.translationProvider.bind(this);
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.settings = {
                title: 'Sube tus documentos',
                description: 'Necesitamos que nos adjuntes los siguientes documentos:',
                requiredFiles: ['DNI', 'Libro de familia']
            };
        }
        else {
            this.settings = this.navParams.data;
        }
        this.modalProvider.setupCentering(this.elementRef);
    }
    FileUploaderModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    FileUploaderModalComponent.prototype.confirm = function () {
        this.onConfirmation();
        this.dismiss();
    };
    FileUploaderModalComponent.prototype.onChildUpload = function (mapfreFile) {
        this.events.publish(FileUploaderModalComponent.uploadEvent, mapfreFile);
        this.center();
    };
    FileUploaderModalComponent.prototype.onChildDownload = function (mapfreFile) {
        this.events.publish(FileUploaderModalComponent.downloadEvent, mapfreFile);
        this.center();
    };
    FileUploaderModalComponent.prototype.onChildRemoval = function (mapfreFile) {
        this.events.publish(FileUploaderModalComponent.childRemovalEvent, mapfreFile);
        this.center();
    };
    FileUploaderModalComponent.prototype.onConfirmation = function () {
        this.events.publish(FileUploaderModalComponent.confirmationEvent, this.settings.files);
    };
    FileUploaderModalComponent.prototype.center = function () {
        this.modalProvider.center(this.elementRef);
    };
    FileUploaderModalComponent.componentName = 'file-uploader-modal';
    FileUploaderModalComponent.uploadEvent = FileUploaderModalComponent.componentName + ":upload";
    FileUploaderModalComponent.downloadEvent = FileUploaderModalComponent.componentName + ":download";
    FileUploaderModalComponent.childRemovalEvent = FileUploaderModalComponent.componentName + ":childRemoval";
    FileUploaderModalComponent.confirmationEvent = FileUploaderModalComponent.componentName + ":confirmation";
    FileUploaderModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'file-uploader-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>    <ion-card-header class=\"no-padding-bottom\">      <ion-item>       <h3>         {{ settings.title }}       </h3>     </ion-item>    </ion-card-header>   <ion-card-content>      <div class=\"modal-description\">        <file-uploader         *ngIf=\"settings\"         [description]=\"settings.description\"         [requiredFiles]=\"settings.requiredFiles\"         [allowedTypes]=\"settings.allowedTypes\"         [files]=\"settings.files\"         [multiple]=\"settings.multiple\"          (onUpload)=\"onChildUpload($event)\"         (onDownload)=\"onChildDownload($event)\"         (onRemoval)=\"onChildRemoval($event)\">       </file-uploader>      </div>      <hr class=\"padding-top-m padding-bottom-xxs\">      <ion-grid class=\"generic-pagination\">       <ion-row>          <ion-col col-5 col-sm-3>           <button ion-button full text-uppercase class=\"fmm-white\" (tap)=\"dismiss()\">             {{ translate('file-uploader-modal.generic.cancel') }}           </button>         </ion-col>          <ion-col col-5 col-sm-3 class=\"margin-left-auto\">           <button ion-button full text-uppercase (tap)=\"confirm()\">             {{ translate('file-uploader-modal.generic.send') }}           </button>         </ion-col>        </ion-row>     </ion-grid>    </ion-card-content>  </ion-card>"
                },] },
    ];
    FileUploaderModalComponent.ctorParameters = function () { return [
        { type: NavParams, },
        { type: ViewController, },
        { type: ModalProvider, },
        { type: ElementRef, },
        { type: TranslationProvider, },
        { type: Events, },
    ]; };
    return FileUploaderModalComponent;
}());
export { FileUploaderModalComponent };
//# sourceMappingURL=file-uploader-modal.js.map