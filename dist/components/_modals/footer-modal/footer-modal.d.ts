import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../providers/modal/modal';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class FooterModalComponent {
    private viewCtrl;
    private navParams;
    private elementRef;
    private modalProvider;
    private translationProvider;
    private mocked;
    title: string;
    description: any;
    constructor(viewCtrl: ViewController, navParams: NavParams, elementRef: ElementRef, modalProvider: ModalProvider, translationProvider: TranslationProvider);
    dismiss(): void;
}
