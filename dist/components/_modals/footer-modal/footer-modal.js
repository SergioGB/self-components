import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Mocks } from '../../../providers/mocks/mocks';
import { ModalProvider } from '../../../providers/modal/modal';
import { TranslationProvider } from '../../../providers/translation/translation';
var FooterModalComponent = (function () {
    function FooterModalComponent(viewCtrl, navParams, elementRef, modalProvider, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        var dataSource;
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            dataSource = Mocks.getFooterSettings().access[0].modal_data;
        }
        else {
            dataSource = this.navParams.data;
        }
        this.title = dataSource.title;
        this.description = dataSource.description;
        this.modalProvider.setupCentering(this.elementRef);
    }
    FooterModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    FooterModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'footer-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" (tap)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>          <ion-card-header class=\"no-padding-bottom\">          <ion-item>         <h3>           {{ title }}         </h3>       </ion-item>        </ion-card-header>        <ion-card-content>       <div class=\"modal-description\">         <!--h3>1. Objeto del contrato.</h3-->         <div class=\"section-wrapper\" [innerHTML]=\"description\">                    </div>       </div>        </ion-card-content>      </ion-card>   "
                },] },
    ];
    FooterModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: NavParams, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
    ]; };
    return FooterModalComponent;
}());
export { FooterModalComponent };
//# sourceMappingURL=footer-modal.js.map