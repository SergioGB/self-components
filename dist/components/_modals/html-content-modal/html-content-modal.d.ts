import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class HtmlContentModalComponent {
    private viewCtrl;
    private navParams;
    private apiProvider;
    private elementRef;
    private modalProvider;
    private translationProvider;
    private mocked;
    private title;
    private description;
    componentReady: boolean;
    key: any;
    constructor(viewCtrl: ViewController, navParams: NavParams, apiProvider: ApiProvider, elementRef: ElementRef, modalProvider: ModalProvider, translationProvider: TranslationProvider);
    dismiss(): void;
}
