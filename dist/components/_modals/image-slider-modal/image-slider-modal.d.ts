import { ElementRef } from '@angular/core';
import { NavParams, Slides, ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class ImageSliderModalComponent {
    private navParams;
    private viewCtrl;
    private modalProvider;
    private elementRef;
    slides: Slides;
    private mocked;
    images: any;
    constructor(navParams: NavParams, viewCtrl: ViewController, modalProvider: ModalProvider, elementRef: ElementRef);
    dismiss(): void;
    next(): void;
    prev(): void;
    isFirstImage(): boolean;
    isLastImage(): boolean;
}
