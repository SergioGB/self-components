import { ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Events } from 'ionic-angular/util/events';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ModalProvider } from '../../../providers/modal/modal';
import { TranslationProvider } from '../../../providers/translation/translation';
import { NavParams } from 'ionic-angular';
export declare class LanguageChangedSuccessModalComponent {
    private viewCtrl;
    private events;
    private navCtrl;
    private navParams;
    private elementRef;
    private modalProvider;
    private translationProvider;
    labels: any;
    constructor(viewCtrl: ViewController, events: Events, navCtrl: NavController, navParams: NavParams, elementRef: ElementRef, modalProvider: ModalProvider, translationProvider: TranslationProvider);
    dismiss(): void;
}
