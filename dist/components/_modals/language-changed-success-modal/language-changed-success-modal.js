import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Events } from 'ionic-angular/util/events';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ModalProvider } from '../../../providers/modal/modal';
import { TranslationProvider } from '../../../providers/translation/translation';
import { NavParams } from 'ionic-angular';
var LanguageChangedSuccessModalComponent = (function () {
    function LanguageChangedSuccessModalComponent(viewCtrl, events, navCtrl, navParams, elementRef, modalProvider, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.labels = this.navParams.get('labels');
        this.modalProvider.setupCentering(this.elementRef);
    }
    LanguageChangedSuccessModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    LanguageChangedSuccessModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'language-changed-success-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>   <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>    <ion-card-content>      <div text-center>       <confirmation-check></confirmation-check>        <h3 class=\"modal-title\">         {{ translate(labels.title) }}       </h3>        <div class=\"modal-description\">         {{ translate(labels.subtitle) }}       </div>     </div>    </ion-card-content>  </ion-card>"
                },] },
    ];
    LanguageChangedSuccessModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: Events, },
        { type: NavController, },
        { type: NavParams, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
    ]; };
    return LanguageChangedSuccessModalComponent;
}());
export { LanguageChangedSuccessModalComponent };
//# sourceMappingURL=language-changed-success-modal.js.map