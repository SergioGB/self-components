import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { ViewController } from "ionic-angular";
import { ModalProvider } from '../../../providers/modal/modal';
import { TranslationProvider } from '../../../providers/translation/translation';
var OfflineModalComponent = (function () {
    function OfflineModalComponent(viewCtrl, elementRef, modalProvider, translationProvider) {
        this.viewCtrl = viewCtrl;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
    }
    OfflineModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    OfflineModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'offline-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>   <ion-card-content>      <div text-center>       <ion-icon name=\"mapfre-warning\" class=\"modal-icon\"></ion-icon>        <h3 class=\"modal-title\">         {{ translate('mapfre-header.offline-modal.offline_alert.title') }}       </h3>        <div class=\"modal-description\">         {{ translate('mapfre-header.offline-modal.offline_alert.subtitle') }}       </div>     </div>    </ion-card-content> </ion-card>"
                },] },
    ];
    OfflineModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: TranslationProvider, },
    ]; };
    return OfflineModalComponent;
}());
export { OfflineModalComponent };
//# sourceMappingURL=offline-modal.js.map