import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class ProcessConfirmationModalComponent {
    private viewCtrl;
    private navParams;
    private elementRef;
    private modalProvider;
    private mocked;
    title: string;
    description: string;
    icon: any;
    ratingText: any;
    flowReference: any;
    constructor(viewCtrl: ViewController, navParams: NavParams, elementRef: ElementRef, modalProvider: ModalProvider);
    ngAfterViewInit(): void;
    dismiss(): void;
}
