import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../providers/modal/modal';
var ProcessConfirmationModalComponent = (function () {
    function ProcessConfirmationModalComponent(viewCtrl, navParams, elementRef, modalProvider) {
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.icon = 'mapfre-check';
            this.title = 'Póliza cancelada';
            this.description = 'La cancelación se ha realizado correctamente.';
            this.flowReference = 'AssistanceCancellation';
        }
        else {
            this.icon = this.navParams.get('icon');
            this.title = this.navParams.get('title');
            this.description = this.navParams.get('description');
            this.ratingText = this.navParams.get('ratingText');
            this.flowReference = this.navParams.get('flowReference');
        }
    }
    ProcessConfirmationModalComponent.prototype.ngAfterViewInit = function () {
        this.modalProvider.setupCentering(this.elementRef);
    };
    ProcessConfirmationModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    ProcessConfirmationModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'process-confirmation-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>    <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>     <div class=\"modal-content bottom-border border-only\" text-center>       <process-confirmation         [mocked]=\"mocked\"         [flowReference]=\"flowReference\"         [title]=\"title\"         [description]=\"description\"         [icon]=\"icon\"         [ratingText]=\"ratingText\">       </process-confirmation>     </div>   </ion-card-content>    </ion-card>"
                },] },
    ];
    ProcessConfirmationModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: NavParams, },
        { type: ElementRef, },
        { type: ModalProvider, },
    ]; };
    return ProcessConfirmationModalComponent;
}());
export { ProcessConfirmationModalComponent };
//# sourceMappingURL=process-confirmation-modal.js.map