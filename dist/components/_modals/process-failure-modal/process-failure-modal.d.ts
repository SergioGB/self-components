import { ElementRef, OnInit } from '@angular/core';
import { Events, NavParams, ViewController } from 'ionic-angular';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class ProcessFailureModalComponent implements OnInit {
    private viewCtrl;
    private elementRef;
    private navParams;
    private events;
    private modalProvider;
    private mocked;
    ready: boolean;
    settings: any;
    constructor(viewCtrl: ViewController, elementRef: ElementRef, navParams: NavParams, events: Events, modalProvider: ModalProvider);
    ngOnInit(): void;
    ngOnDestroy(): void;
    dismiss(): void;
    private onCallMe;
}
