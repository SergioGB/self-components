import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { Events, NavParams, ViewController } from 'ionic-angular';
import { ProcessFailureComponent } from '../../process-failure/process-failure';
import { ModalProvider } from '../../../providers/modal/modal';
var ProcessFailureModalComponent = (function () {
    function ProcessFailureModalComponent(viewCtrl, elementRef, navParams, events, modalProvider) {
        this.viewCtrl = viewCtrl;
        this.elementRef = elementRef;
        this.navParams = navParams;
        this.events = events;
        this.modalProvider = modalProvider;
    }
    ProcessFailureModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.mocked = this.navParams.get('mocked');
        if (this.mocked) {
            this.settings = {
                icon: 'mapfre-cross-error',
                title: 'Proceso cancelado',
                description: 'No se ha podido llevar a cabo tu modificación ni se ha cobrado el importe en el método de pago seleccionado.'
            };
        }
        else {
            this.settings = this.navParams.get('settings');
        }
        this.ready = true;
        this.modalProvider.setupCentering(this.elementRef);
        this.events.unsubscribe(ProcessFailureComponent.finishChangeEvent);
        this.events.subscribe(ProcessFailureComponent.finishChangeEvent, function () {
            _this.dismiss();
        });
        this.events.unsubscribe(ProcessFailureComponent.callMeEvent);
        this.events.subscribe(ProcessFailureComponent.callMeEvent, function (data) {
            _this.onCallMe();
        });
    };
    ProcessFailureModalComponent.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(ProcessFailureComponent.finishChangeEvent);
        this.events.unsubscribe(ProcessFailureComponent.callMeEvent);
    };
    ProcessFailureModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ProcessFailureModalComponent.prototype.onCallMe = function () {
        this.events.publish('process-failure-modal:call-me');
    };
    ProcessFailureModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'process-failure-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin>    <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" (click)=\"dismiss()\"></ion-icon>      <div class=\"modal-content no-padding\" text-center>       <process-failure         *ngIf=\"ready\"         [icon]=\"settings.icon\"         [title]=\"settings.title\"         [description]=\"settings.description\"         [showCallMe]=\"settings.showCallMe\"         [showFinish]=\"settings.showFinish\"         [isModal]=\"settings.isModal\">       </process-failure>     </div>   </ion-card-content>  </ion-card>"
                },] },
    ];
    ProcessFailureModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ElementRef, },
        { type: NavParams, },
        { type: Events, },
        { type: ModalProvider, },
    ]; };
    return ProcessFailureModalComponent;
}());
export { ProcessFailureModalComponent };
//# sourceMappingURL=process-failure-modal.js.map