import { ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Events, ModalController, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class ProcessPolicyChangeFailureModalComponent implements OnInit, OnDestroy {
    private elementRef;
    private viewCtrl;
    private modalController;
    private translationProvider;
    private modalProvider;
    private events;
    text: string;
    constructor(elementRef: ElementRef, viewCtrl: ViewController, modalController: ModalController, translationProvider: TranslationProvider, modalProvider: ModalProvider, events: Events);
    ngOnInit(): void;
    ngOnDestroy(): void;
    dismiss(): void;
    callMeOpened(): void;
}
