import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { Events, ModalController, ViewController } from 'ionic-angular';
import { ProcessFailureComponent } from '../../process-failure/process-failure';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
var ProcessPolicyChangeFailureModalComponent = (function () {
    function ProcessPolicyChangeFailureModalComponent(elementRef, viewCtrl, modalController, translationProvider, modalProvider, events) {
        var _this = this;
        this.elementRef = elementRef;
        this.viewCtrl = viewCtrl;
        this.modalController = modalController;
        this.translationProvider = translationProvider;
        this.modalProvider = modalProvider;
        this.events = events;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
        setTimeout(function () {
            _this.modalProvider.center(_this.elementRef);
        }, 1000);
    }
    ProcessPolicyChangeFailureModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.events.unsubscribe(ProcessFailureComponent.callMeEvent);
        this.events.subscribe(ProcessFailureComponent.callMeEvent, function (data) {
            _this.callMeOpened();
        });
    };
    ProcessPolicyChangeFailureModalComponent.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(ProcessFailureComponent.callMeEvent);
    };
    ProcessPolicyChangeFailureModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    ProcessPolicyChangeFailureModalComponent.prototype.callMeOpened = function () {
        this.viewCtrl.dismiss(true);
    };
    ProcessPolicyChangeFailureModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'process-policy-change-failure-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card>   <ion-card-content>     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>     <process-failure       [icon]=\"'mapfre-cross-error'\"       [title]=\"translate('process-policy-change-failure-modal.policy_change.cancel.title')\"       [description]=\"translate('process-policy-change-failure-modal.policy_change.cancel.subtitle')\"       [showCallMe]=\"true\"       [isModal]=\"true\">     </process-failure>   </ion-card-content> </ion-card>"
                },] },
    ];
    ProcessPolicyChangeFailureModalComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: ViewController, },
        { type: ModalController, },
        { type: TranslationProvider, },
        { type: ModalProvider, },
        { type: Events, },
    ]; };
    return ProcessPolicyChangeFailureModalComponent;
}());
export { ProcessPolicyChangeFailureModalComponent };
//# sourceMappingURL=process-policy-change-failure-modal.js.map