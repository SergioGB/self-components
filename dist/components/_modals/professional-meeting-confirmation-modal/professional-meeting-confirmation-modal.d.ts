import { ElementRef } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class ProfessionalMeetingConfirmationModalComponent {
    private viewCtrl;
    private modalProvider;
    private elementRef;
    private navParams;
    private translationProvider;
    mocked: boolean;
    appointment: any;
    professional: any;
    workshop: any;
    constructor(viewCtrl: ViewController, modalProvider: ModalProvider, elementRef: ElementRef, navParams: NavParams, translationProvider: TranslationProvider);
    dismiss(): void;
}
