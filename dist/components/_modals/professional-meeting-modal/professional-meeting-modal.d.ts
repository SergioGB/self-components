import { ElementRef, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Events, ViewController, NavParams, ModalController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';
import { SubmitIncidenceProvider } from '../../../providers/submit-incidence/submit-incidence';
import { SubmitHomeIncidenceProvider } from '../../../providers/submit-incidence/_home/submit-home-incidence';
import { Workshop } from '../../../models/workshop';
import { ModalProvider } from '../../../providers/modal/modal';
import { TranslationProvider } from '../../../providers/translation/translation';
import { FormatProvider } from '../../../providers/format/format';
export declare class ProfessionalMeetingModalComponent implements OnInit, OnDestroy {
    private viewCtrl;
    private formBuilder;
    private modalProvider;
    private elementRef;
    private events;
    private translationProvider;
    private navParams;
    private formatProvider;
    private submitIncidenceProvider;
    private submitHomeIncidenceProvider;
    private apiProvider;
    private modalCtrl;
    static componentName: string;
    private mocked;
    formUnlocked: boolean;
    currentMoment: string;
    appointment: any;
    professional: any;
    workshop: Workshop;
    editForm: FormGroup;
    formatDate: Function;
    formatTime: Function;
    selectedDate: Date;
    selectedDateTemp: Date;
    modal: any;
    labels: any;
    selectedTime: string;
    selectedTimeTemp: string;
    static confirmedEvent: string;
    constructor(viewCtrl: ViewController, formBuilder: FormBuilder, modalProvider: ModalProvider, elementRef: ElementRef, events: Events, translationProvider: TranslationProvider, navParams: NavParams, formatProvider: FormatProvider, submitIncidenceProvider: SubmitIncidenceProvider, submitHomeIncidenceProvider: SubmitHomeIncidenceProvider, apiProvider: ApiProvider, modalCtrl: ModalController);
    ngOnInit(): void;
    ngOnDestroy(): void;
    modify(): void;
    onTimeSelection(selectedTime: string): void;
    cancel(): void;
    confirm(): void;
    onSuccess(): void;
    dismiss(): void;
    validData(): boolean;
    private initializeForm;
    openCalendarModal(): void;
}
