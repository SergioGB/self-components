import { ElementRef, OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class RenovationModalComponent implements OnInit {
    private viewCtrl;
    private elementRef;
    private modalProvider;
    private translationProvider;
    private navParams;
    private mocked;
    private title;
    private description;
    private labels;
    constructor(viewCtrl: ViewController, elementRef: ElementRef, modalProvider: ModalProvider, translationProvider: TranslationProvider, navParams: NavParams);
    ngOnInit(): void;
    dismiss(): void;
}
