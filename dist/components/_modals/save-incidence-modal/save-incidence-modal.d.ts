import { ElementRef } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { ApiProvider } from '../../../providers/api/api';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class SaveIncidenceModalComponent {
    private userProvider;
    private viewCtrl;
    private apiProvider;
    private navParams;
    private formBuilder;
    private translationProvider;
    private elementRef;
    private modalProvider;
    data: boolean;
    saveForm: FormGroup;
    emailSave: string;
    constructor(userProvider: UserProvider, viewCtrl: ViewController, apiProvider: ApiProvider, navParams: NavParams, formBuilder: FormBuilder, translationProvider: TranslationProvider, elementRef: ElementRef, modalProvider: ModalProvider);
    dismiss(): void;
    isDisabled(): boolean;
    confirmAction(): void;
}
