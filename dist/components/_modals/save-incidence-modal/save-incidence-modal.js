import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { ApiProvider } from '../../../providers/api/api';
import { FormBuilder, Validators } from '@angular/forms';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
var SaveIncidenceModalComponent = (function () {
    function SaveIncidenceModalComponent(userProvider, viewCtrl, apiProvider, navParams, formBuilder, translationProvider, elementRef, modalProvider) {
        this.userProvider = userProvider;
        this.viewCtrl = viewCtrl;
        this.apiProvider = apiProvider;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
        this.emailSave = 'fernando123@gmail.com';
        this.saveForm = this.formBuilder.group({
            email: [this.emailSave, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
        });
    }
    SaveIncidenceModalComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss(false);
    };
    SaveIncidenceModalComponent.prototype.isDisabled = function () {
        return this.saveForm.invalid;
    };
    SaveIncidenceModalComponent.prototype.confirmAction = function () {
        this.viewCtrl.dismiss(true);
    };
    SaveIncidenceModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'save-incidence-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["save-incidence-modal .modal-icon {   color: #8db602; }  save-incidence-modal .modal-form-content {   text-align: left; }   save-incidence-modal .modal-form-content input {     margin-left: 0 !important; }"],
                    template: "<ion-card no-margin>   <ion-card-content class=\"no-margin-bottom\">     <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (click)=\"dismiss()\"></ion-icon>      <div class=\"modal-content\" text-center>       <confirmation-check></confirmation-check>        <h3 class=\"modal-title\">         {{ translate('save-incidence-modal.save_incidence_modal.title') }}       </h3>        <ion-grid no-padding class=\"margin-top-m\">         <ion-row>           <ion-col col-12 col-md-8 push-md-2>             <div class=\"modal-description\">               {{ translate('save-incidence-modal.save_incidence_modal.description') }}             </div>           </ion-col>         </ion-row>         <form [formGroup]=\"saveForm\">           <ion-row>             <ion-col col-12 col-md-8 push-md-2>               <ion-item class=\"modal-form-content\">                 <ion-input type=\"text\" formControlName=\"email\"></ion-input>               </ion-item>             </ion-col>           </ion-row>         </form>       </ion-grid>     </div>      <hr class=\"no-margin-top margin-bottom-l\">      <ion-grid class=\"generic-buttons no-padding actions-wrapper\">       <ion-row>          <ion-col col-6 col-sm-5 col-md-3>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"dismiss()\">             {{ translate('save-incidence-modal.save_incidence_modal.cancel') }}           </button>         </ion-col>          <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">           <button             ion-button full text-uppercase             [disabled]=\"isDisabled()\"             (click)=\"confirmAction()\">              {{ translate('save-incidence-modal.save_incidence_modal.send') }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    SaveIncidenceModalComponent.ctorParameters = function () { return [
        { type: UserProvider, },
        { type: ViewController, },
        { type: ApiProvider, },
        { type: NavParams, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: ModalProvider, },
    ]; };
    return SaveIncidenceModalComponent;
}());
export { SaveIncidenceModalComponent };
//# sourceMappingURL=save-incidence-modal.js.map