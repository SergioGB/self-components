import { OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
export declare class VideoModalComponent implements OnInit {
    viewCtrl: ViewController;
    private navParams;
    player: any;
    mocked: any;
    src: any;
    playing: boolean;
    autoplay: boolean;
    constructor(viewCtrl: ViewController, navParams: NavParams);
    ngOnInit(): void;
    togglePlayVideo(): void;
    dismiss(): void;
}
