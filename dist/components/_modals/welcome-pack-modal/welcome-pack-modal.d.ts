import { ElementRef, OnInit } from '@angular/core';
import { NavParams, Slides, ViewController } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { WelcomePackProvider } from '../../../providers/welcome-pack/welcome-pack';
import { PlatformProvider } from '../../../providers/platform/platform';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class WelcomePackModalComponent implements OnInit {
    private viewCtrl;
    private elementRef;
    private navParams;
    private componentSettingsProvider;
    private platformProvider;
    private translationProvider;
    private modalProvider;
    private welcomePackProvider;
    slides: Slides;
    private mocked;
    componentReady: boolean;
    slidesWelcome: any;
    labels: any;
    constructor(viewCtrl: ViewController, elementRef: ElementRef, navParams: NavParams, componentSettingsProvider: ComponentSettingsProvider, platformProvider: PlatformProvider, translationProvider: TranslationProvider, modalProvider: ModalProvider, welcomePackProvider: WelcomePackProvider);
    ngOnInit(): void;
    dismiss(): void;
    next(): void;
    prev(): void;
    isFirstImage(): boolean;
    isLastImage(): boolean;
}
