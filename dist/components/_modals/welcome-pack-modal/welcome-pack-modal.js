import { ViewEncapsulation } from '@angular/core';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavParams, Slides, ViewController } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { WelcomePackProvider } from '../../../providers/welcome-pack/welcome-pack';
import { PlatformProvider } from '../../../providers/platform/platform';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalProvider } from '../../../providers/modal/modal';
var WelcomePackModalComponent = (function () {
    function WelcomePackModalComponent(viewCtrl, elementRef, navParams, componentSettingsProvider, platformProvider, translationProvider, modalProvider, welcomePackProvider) {
        this.viewCtrl = viewCtrl;
        this.elementRef = elementRef;
        this.navParams = navParams;
        this.componentSettingsProvider = componentSettingsProvider;
        this.platformProvider = platformProvider;
        this.translationProvider = translationProvider;
        this.modalProvider = modalProvider;
        this.welcomePackProvider = welcomePackProvider;
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
    }
    WelcomePackModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.mocked = this.navParams.get('mocked');
        var platform;
        if (this.platformProvider.onDesktop()) {
            platform = 'DESKTOP';
        }
        else if (this.platformProvider.onTablet()) {
            platform = 'TABLET';
        }
        else if (this.platformProvider.onMobile()) {
            platform = 'MOBILE';
        }
        this.componentSettingsProvider.getWelcomePack(this.mocked, platform).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.labels = _this.navParams.get('labels');
                _this.slidesWelcome = mapfreResponse.data;
                _this.componentReady = true;
                _this.modalProvider.setupCentering(_this.elementRef);
            }
        });
    };
    WelcomePackModalComponent.prototype.dismiss = function () {
        this.welcomePackProvider.setWelcomePackAvailable(true);
        this.viewCtrl.dismiss();
    };
    WelcomePackModalComponent.prototype.next = function () {
        this.slides.slideNext();
    };
    WelcomePackModalComponent.prototype.prev = function () {
        this.slides.slidePrev();
    };
    WelcomePackModalComponent.prototype.isFirstImage = function () {
        return this.slides.isBeginning();
    };
    WelcomePackModalComponent.prototype.isLastImage = function () {
        return this.slides.isEnd();
    };
    WelcomePackModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'welcome-pack-modal',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["welcome-pack-modal {   padding-top: 5px;   padding-bottom: 5px; }   welcome-pack-modal .swiper-pagination-bullet {     width: 12px;     height: 12px; }  /* tablet */ @media (min-width: 768px) and (max-width: 1199px) {   welcome-pack-modal .welcome_pack_button {     text-align: center; } }"],
                    template: "<ion-card [hidden]=\"!componentReady\" no-margin>    <ion-card-content>          <ion-icon name=\"mapfre-close\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>     <ion-grid no-padding>       <ion-row class=\"center-vertically slides-wrapper\">          <ion-col col-0 col-sm-1 text-left (click)=\"prev()\"                  [ngClass]=\"{ 'disabled': isFirstImage(), 'clickable': !isFirstImage() }\">           <ion-icon name=\"mapfre-arrow-left\" class=\"arrow-col\"></ion-icon>         </ion-col>          <ion-col col-12 col-sm-10 text-center>           <ion-slides pager>             <ion-slide *ngFor=\"let slideWelcome of slidesWelcome\">               <div padding-bottom padding-top>                 <h3 class=\"padding-bottom-xs\" text-center>{{ slideWelcome.title }}</h3>                 <p class=\"font-m\" text-center>                   {{ slideWelcome.description }}                 </p>               </div>               <img class=\"image margin-left-auto margin-right-auto\" src=\"{{slideWelcome.image}}\">             </ion-slide>           </ion-slides>         </ion-col>          <ion-col col-0 col-sm-1 text-right (click)=\"next()\"                  [ngClass]=\"{ 'disabled': isLastImage(), 'clickable': !isLastImage() }\">           <ion-icon name=\"mapfre-arrow-right\" class=\"arrow-col\"></ion-icon>         </ion-col>        </ion-row>       <ion-row padding-top *ngIf=\"componentReady\">         <ion-col text-center col-12>           <button class=\"fmm-white\" ion-button full text-uppercase (click)=\"dismiss()\">{{ translate(labels.button_text) }}</button>         </ion-col>       </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    WelcomePackModalComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ElementRef, },
        { type: NavParams, },
        { type: ComponentSettingsProvider, },
        { type: PlatformProvider, },
        { type: TranslationProvider, },
        { type: ModalProvider, },
        { type: WelcomePackProvider, },
    ]; };
    WelcomePackModalComponent.propDecorators = {
        'slides': [{ type: ViewChild, args: [Slides,] },],
    };
    return WelcomePackModalComponent;
}());
export { WelcomePackModalComponent };
//# sourceMappingURL=welcome-pack-modal.js.map