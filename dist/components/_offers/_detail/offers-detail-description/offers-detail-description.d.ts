import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { OfferProvider } from '../../../../providers/offer/offer';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { Offer } from '../../../../models/offer';
export declare class OffersDetailDescriptionComponent implements OnInit {
    private translationProvider;
    private offerProvider;
    private componentSettingsProvider;
    mocked?: boolean;
    styles?: any;
    offerId: string;
    componentReady: boolean;
    offerDetail: Offer;
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, offerProvider: OfferProvider, componentSettingsProvider: ComponentSettingsProvider);
    ngOnInit(): void;
    private queryData;
}
