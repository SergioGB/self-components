import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { OfferProvider } from '../../../../providers/offer/offer';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
var OffersDetailDescriptionComponent = (function () {
    function OffersDetailDescriptionComponent(translationProvider, offerProvider, componentSettingsProvider) {
        this.translationProvider = translationProvider;
        this.offerProvider = offerProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.componentReady = false;
        this.translationProvider.bind(this);
    }
    OffersDetailDescriptionComponent.prototype.ngOnInit = function () {
        this.queryData();
    };
    OffersDetailDescriptionComponent.prototype.queryData = function () {
        var _this = this;
        if (this.mocked) {
            this.offerId = "1";
        }
        this.offerProvider.getOfferDetail(this.mocked, this.offerId).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.offerDetail = mapfreResponse.data;
                _this.componentReady = true;
            }
        });
    };
    OffersDetailDescriptionComponent.inputs = ['offerId'];
    OffersDetailDescriptionComponent.configInputs = ['mocked', 'styles'];
    OffersDetailDescriptionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'offers-detail-description',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["offers-detail-description .card-margin-top {   margin-top: 0px !important; }"],
                    template: "<div *ngIf=\"componentReady\" [ngStyle]=\"styles\">   <ion-card class=\"card-margin-top\">     <ion-card-header>       <ion-item>         <h3>           {{ translate('offers-detail-description.offers.detail.description.title') }}         </h3>       </ion-item>     </ion-card-header>     <ion-card-content>       <div [innerHTML]=\"offerDetail.description\"></div>     </ion-card-content>   </ion-card> </div>"
                },] },
    ];
    OffersDetailDescriptionComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: OfferProvider, },
        { type: ComponentSettingsProvider, },
    ]; };
    OffersDetailDescriptionComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'offerId': [{ type: Input },],
    };
    return OffersDetailDescriptionComponent;
}());
export { OffersDetailDescriptionComponent };
//# sourceMappingURL=offers-detail-description.js.map