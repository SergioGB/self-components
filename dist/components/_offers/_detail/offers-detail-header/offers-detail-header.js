import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { OfferProvider } from '../../../../providers/offer/offer';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
var OffersDetailHeaderComponent = (function () {
    function OffersDetailHeaderComponent(translationProvider, offerProvider, componentSettingsProvider) {
        this.translationProvider = translationProvider;
        this.offerProvider = offerProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.componentReady = false;
        this.translationProvider.bind(this);
    }
    OffersDetailHeaderComponent.prototype.ngOnInit = function () {
        this.queryData();
    };
    OffersDetailHeaderComponent.prototype.queryData = function () {
        var _this = this;
        this.offerProvider.getOfferDetail(this.mocked, this.offerId).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.offerDetail = mapfreResponse.data;
                if (_this.offerDetail && _this.offerDetail.insurance_line === 'C') {
                    _this.offerDetail.subtitle = _this.translationProvider.getValueForKey('offers-detail-header.offers.subtitle_car');
                }
                else {
                    _this.offerDetail.subtitle = _this.translationProvider.getValueForKey('offers-detail-header.offers.subtitle_home');
                }
                _this.componentReady = true;
            }
        });
    };
    OffersDetailHeaderComponent.prototype.accessService = function () {
        window.location.href = this.offerDetail.link;
    };
    OffersDetailHeaderComponent.inputs = ['offerId'];
    OffersDetailHeaderComponent.configInputs = ['mocked', 'light', 'styles'];
    OffersDetailHeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'offers-detail-header',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div *ngIf=\"componentReady && offerDetail\" class=\"upper-header\" [class.light]=\"light\" [ngStyle]=\"styles\">   <h1 *ngIf=\"offerDetail.title\" class=\"margin-bottom-s\">     {{ offerDetail.title }}   </h1>    <p *ngIf=\"offerDetail.subtitle\" class=\"font-l\">     {{ offerDetail.subtitle }}   </p>    <ion-row>     <ion-col class=\"no-padding\" col-xl-2 col-sm-4 col-12>       <button *ngIf=\"offerDetail.link\" ion-button full text-uppercase class=\"margin-top-l margin-bottom-s\" (click)=\"accessService()\">         {{ translate('offers-detail-header.offer.detail.access_service_button') }}       </button>     </ion-col>   </ion-row> </div>"
                },] },
    ];
    OffersDetailHeaderComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: OfferProvider, },
        { type: ComponentSettingsProvider, },
    ]; };
    OffersDetailHeaderComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'offerId': [{ type: Input },],
        'light': [{ type: Input },],
    };
    return OffersDetailHeaderComponent;
}());
export { OffersDetailHeaderComponent };
//# sourceMappingURL=offers-detail-header.js.map