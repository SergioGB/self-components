import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { OfferProvider } from '../../../../providers/offer/offer';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { Offer } from '../../../../models/offer';
export declare class OffersDetailListTermsComponent implements OnInit {
    private translationProvider;
    private offerProvider;
    private componentSettingsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    offerId: string;
    componentReady: boolean;
    offerDetail: Offer;
    defaultIcons: string[];
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, offerProvider: OfferProvider, componentSettingsProvider: ComponentSettingsProvider);
    ngOnInit(): void;
    private queryData;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
