import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { Offer } from '../../../models/offer';
import { PlatformProvider } from '../../../providers/platform/platform';
import { NavController } from 'ionic-angular';
export declare class OfferCardComponent implements OnInit {
    private translationProvider;
    private navController;
    private platformProvider;
    mocked?: boolean;
    styles?: any;
    offer: Offer;
    isMobile: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, navController: NavController, platformProvider: PlatformProvider);
    ngOnInit(): void;
    accessService(event: Event): void;
    goToDetail(offer: any): void;
}
