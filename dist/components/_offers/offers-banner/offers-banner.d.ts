import { OnInit } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class OffersBannerComponent implements OnInit {
    private navController;
    private translationProvider;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    title: string;
    percentage: any;
    offerId: any;
    defaultIcons: string[];
    static componentName: string;
    static offerTriggeredEvent: string;
    static inputs: string[];
    static configInputs: string[];
    constructor(navController: NavController, translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    goToOffer(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
