import { Events, NavController, NavParams } from 'ionic-angular';
import { OnDestroy, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslationProvider } from '../../../providers/translation/translation';
import { OfferProvider } from '../../../providers/offer/offer';
import { Offer } from '../../../models/offer';
export declare class OffersListComponent implements OnInit, OnDestroy {
    private navController;
    private navParams;
    private formBuilder;
    private translationProvider;
    private elementRef;
    private offerProvider;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    private debounceTime;
    private itemsPerPage;
    private marginPill;
    static configInputs: string[];
    searchForm: FormGroup;
    selectedOfferGroups: any[];
    currentPage: number;
    offerGroups: any[];
    offers: Offer[];
    ready: boolean;
    error: boolean;
    searching: boolean;
    hasMorePages: boolean;
    isMobile: boolean;
    constructor(navController: NavController, navParams: NavParams, formBuilder: FormBuilder, translationProvider: TranslationProvider, elementRef: ElementRef, offerProvider: OfferProvider, events: Events);
    ngOnInit(): void;
    ngOnDestroy(): void;
    getOfferGroups(): void;
    searchOffers(resetPagination?: boolean): void;
    selectOfferGroup(offerGroup: any): void;
    deselectOfferGroup(offerGroup: any): void;
    showMore(event: Event): void;
    private initialize;
    private initializeForm;
    private queryOfferGroups;
    private queryOffers;
    private queryOffersFilter;
    private getSelectedOfferGroupIds;
    private getSelectedInsuranceLines;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
