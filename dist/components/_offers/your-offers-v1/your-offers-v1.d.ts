import { OnInit } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Offer } from '../../../models/offer';
import { ApiProvider } from '../../../providers/api/api';
export declare class YourOffersV1Component implements OnInit {
    private apiProvider;
    private navController;
    private translationProvider;
    private componentSettingsProvider;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    related?: any;
    offersImages?: any;
    defaultIcons: string[];
    ready: boolean;
    offers: Offer[];
    static configInputs: string[];
    constructor(apiProvider: ApiProvider, navController: NavController, translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider, events: Events);
    ngOnInit(): void;
    goToOffers(): void;
    goToDetail(offer: any): void;
    ellipsisNotWorking(): any;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
