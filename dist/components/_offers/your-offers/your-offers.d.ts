import { OnInit } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Offer } from '../../../models/offer';
export declare class YourOffersComponent implements OnInit {
    private navController;
    private translationProvider;
    private componentSettingsProvider;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    related?: any;
    defaultIcons: string[];
    ready: boolean;
    offers: Offer[];
    static configInputs: string[];
    constructor(navController: NavController, translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider, events: Events);
    ngOnInit(): void;
    goToOffers(): void;
    goToDetail(offer: any): void;
    ellipsisNotWorking(): any;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
