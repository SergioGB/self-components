export declare class BenefitsBannerMocks {
    private static benefitImagesMock;
    static getBenefitImagesMocks(): any;
    private static benefitCategoriesMock;
    static getBenefitCategoriesMock(): string[];
    private static mapfreRelationshipResponse;
    static getMapfreRelationshipResponse(): any;
}
