import { Injectable } from '@angular/core';
var BenefitsBannerMocks = (function () {
    function BenefitsBannerMocks() {
    }
    BenefitsBannerMocks.getBenefitImagesMocks = function () {
        return this.benefitImagesMock;
    };
    BenefitsBannerMocks.getBenefitCategoriesMock = function () {
        return this.benefitCategoriesMock;
    };
    BenefitsBannerMocks.getMapfreRelationshipResponse = function () {
        return this.mapfreRelationshipResponse;
    };
    BenefitsBannerMocks.benefitImagesMock = {
        es: {
            images: [
                {
                    image: 'https://www.mapfre.es/seguros/images/924x386-carrusel-salud-cold_tcm744-544106.jpg',
                    category: 'PLATINIUM'
                },
                {
                    image: 'https://www.mapfre.es/seguros/images/carrusel-particulares-coche924x386-carrusel-particulares-coche_tcm744-540224.jpg',
                    category: 'GOLD'
                },
                {
                    image: 'https://www.mapfre.es/seguros/images/924x386-carrusel-home-warm_tcm744-543840.jpg',
                    category: 'SILVER'
                }
            ],
            title: "¡Beneficios por ser cliente MAPFRE!",
            subtitle: "Te ayudamos a ahorrar en tu seguro",
            becauseOfYouImage: 'https://www.mapfre.es/seguros/images/924x386-carrusel-home-warm_tcm744-543840.jpg'
        }
    };
    BenefitsBannerMocks.benefitCategoriesMock = [
        'PLATINUM',
        'GOLD',
        'SILVER'
    ];
    BenefitsBannerMocks.mapfreRelationshipResponse = {
        "response": {
            "code": 0,
            "message": "string"
        },
        "data": {
            "segment": {
                "id": "PLATINUM",
                "avatar": "string",
                "title": "string",
                "benefits": {
                    "link": "string",
                    "content": "string"
                }
            },
            "loyalty_points": {
                "accumulated": 0,
                "redeemed": 0
            },
            "agreements": [
                "string"
            ]
        }
    };
    BenefitsBannerMocks.decorators = [
        { type: Injectable },
    ];
    BenefitsBannerMocks.ctorParameters = function () { return []; };
    return BenefitsBannerMocks;
}());
export { BenefitsBannerMocks };
//# sourceMappingURL=mocks.js.map