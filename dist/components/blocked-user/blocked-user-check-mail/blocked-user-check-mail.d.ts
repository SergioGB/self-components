import { TranslationProvider } from '../../../providers/translation/translation';
import { Events } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
export declare class BlockedUserCheckMailComponent {
    private translationProvider;
    events: Events;
    private userProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    source: string;
    defaultIcons: string[];
    componentReady: boolean;
    reSent: boolean;
    static componentName: string;
    static onCloseEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events, userProvider: UserProvider);
    resendValidation(): void;
    scrollToTopPage(): void;
    onClose(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
