import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { Events } from 'ionic-angular';
import { Message } from '../../../models/message';
import { MessageEmailContent } from '../../../models/messageEmailContent';
import { UserProvider } from '../../../providers/user/user';
var BlockedUserCheckMailComponent = (function () {
    function BlockedUserCheckMailComponent(translationProvider, events, userProvider) {
        this.translationProvider = translationProvider;
        this.events = events;
        this.userProvider = userProvider;
        this.defaultIcons = ["mapfre-close", "mapfre-check"];
        this.translationProvider.bind(this);
        this.componentReady = true;
    }
    BlockedUserCheckMailComponent.prototype.resendValidation = function () {
        var _this = this;
        if (!this.mocked) {
            var validationContent = new Message('EMAIL', new MessageEmailContent('', '', '', null));
            this.userProvider.sendMailSMSRequest(validationContent).subscribe(function (response) {
                var mapfreResponse = _this.userProvider.getResponseJSON(response);
                var success = _this.userProvider.isMapfreResponseValid(mapfreResponse);
                if (success) {
                    _this.scrollToTopPage();
                    _this.reSent = true;
                }
            });
        }
    };
    BlockedUserCheckMailComponent.prototype.scrollToTopPage = function () {
        this.events.publish('scrollToTop');
    };
    BlockedUserCheckMailComponent.prototype.onClose = function () {
        this.events.publish(BlockedUserCheckMailComponent.onCloseEvent, this.source);
    };
    BlockedUserCheckMailComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    BlockedUserCheckMailComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    BlockedUserCheckMailComponent.componentName = 'blocked-user-check-mail';
    BlockedUserCheckMailComponent.onCloseEvent = BlockedUserCheckMailComponent.componentName + ":close";
    BlockedUserCheckMailComponent.inputs = ['source'];
    BlockedUserCheckMailComponent.configInputs = ['mocked', 'icons', 'styles'];
    BlockedUserCheckMailComponent.decorators = [
        { type: Component, args: [{
                    selector: 'blocked-user-check-mail',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin *ngIf=\"componentReady\" [ngStyle]=\"styles\">    <ion-item>     <ion-icon class=\"clickable\" small item-end [name]=\"getIcon(0)\" tabindex=\"0\" (keyup.enter)=\"onClose($event)\" (click)=\"onClose($event)\"></ion-icon>   </ion-item>    <ion-card-content class=\"bottom-border border-only no-padding-top\">      <div text-center>       <ion-icon *ngIf=\"reSent\" [name]=\"getIcon(1)\" extra-huge class=\"green\"></ion-icon>     </div>      <h3 class=\"margin-top-m\" text-center>       {{ translate(!reSent ? 'login.new_account.validation_by_mail.title' : 'login.new_account.validation_by_mail.resended_title') }}     </h3>      <h5 class=\"fmm-primary-text fmm-regular vertical-margin-s\" text-center>       {{ translate(!reSent ? 'login.new_account.validation_by_mail.description' : 'login.new_account.validation_by_mail.resended_description') }}     </h5>    </ion-card-content>    <ion-card-content *ngIf=\"!reSent\" class=\"padding-m\">     <div text-center>       <a [title]=\"translate('blocked-user-check-mail.new_account.validation_by_mail.resend')\" class=\"font-m clickable\" tabindex=\"0\" (keyup.enter)=\"resendValidation()\" (click)=\"resendValidation()\">         {{ translate('login.blocked-user-check-mail.new_account.validation_by_mail.resend') }}       </a>     </div>   </ion-card-content>  </ion-card>"
                },] },
    ];
    BlockedUserCheckMailComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: Events, },
        { type: UserProvider, },
    ]; };
    BlockedUserCheckMailComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'source': [{ type: Input },],
    };
    return BlockedUserCheckMailComponent;
}());
export { BlockedUserCheckMailComponent };
//# sourceMappingURL=blocked-user-check-mail.js.map