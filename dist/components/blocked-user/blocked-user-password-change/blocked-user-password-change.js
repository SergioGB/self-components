import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { ComponentSettingsProvider } from "../../../providers/component-settings/component-settings";
import { TranslationProvider } from "../../../providers/translation/translation";
import { UserProvider } from "../../../providers/user/user";
import { Events } from 'ionic-angular';
import { PasswordChangeRequest } from '../../../models/login/passwordChangeRequest';
import { Mocks } from '../../../providers/mocks/mocks';
var BlockedUserPasswordChangeComponent = (function () {
    function BlockedUserPasswordChangeComponent(componentSettingsProvider, translationProvider, formBuilder, userProvider, events) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.translationProvider = translationProvider;
        this.formBuilder = formBuilder;
        this.userProvider = userProvider;
        this.events = events;
        this.defaultIcons = ["mapfre-warning", "close-circle", "mapfre-close"];
        this.errorGotten = false;
        this.passwordsMismatched = false;
        this.changePasswordForm = this.formBuilder.group({
            password: ['', Validators.required],
            validationPassword: ['', [Validators.required, this.passwordValidator]]
        }, {
            validator: this.passwordsValidator('password', 'validationPassword')
        });
        this.componentSettings = {
            title: this.translationProvider.getValueForKey('login.blocked-user-password-change.blocked_user.password_change.title'),
            subtitle: this.translationProvider.getValueForKey('login.blocked-user-password-change.blocked_user.password_change.subtitle'),
            new_password_password_tag: this.translationProvider.getValueForKey('login.blocked-user-password-change.blocked_user.password_change.password'),
            new_password_repeat_password_tag: this.translationProvider.getValueForKey('login.blocked-user-password-change.blocked_user.password_change.password_repeat'),
            new_password_passwords_mismatch_tag: this.translationProvider.getValueForKey('login.blocked-user-password-change.blocked_user.password_change.mismatch'),
            confirm_button_tag: this.translationProvider.getValueForKey('login.blocked-user-password-change.blocked_user.password_change.confirm')
        };
        this.componentReady = true;
    }
    BlockedUserPasswordChangeComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.userName = Mocks.getAccountData().username;
        }
    };
    BlockedUserPasswordChangeComponent.prototype.onPasswordBlur = function () {
        var password = this.changePasswordForm.value.password;
        var confirmPassword = this.changePasswordForm.value.validationPassword;
        this.passwordsMismatched = password && confirmPassword && password !== confirmPassword;
        if (this.passwordsMismatched) {
            this.scrollToTopComponent();
        }
    };
    BlockedUserPasswordChangeComponent.prototype.onClose = function (event) {
        this.events.publish(BlockedUserPasswordChangeComponent.onCloseEvent);
    };
    BlockedUserPasswordChangeComponent.prototype.scrollToTopPage = function () {
        this.events.publish('scrollToTop');
    };
    BlockedUserPasswordChangeComponent.prototype.scrollToTopComponent = function () {
        this.events.publish('scrollToElement', 'passwordsMismatched');
    };
    BlockedUserPasswordChangeComponent.prototype.attemptPasswordChange = function () {
        var _this = this;
        this.componentSettingsProvider.getClientsSettings(this.mocked, this.userName)
            .subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            var userValidated = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
            if (userValidated) {
                mapfreResponse.data[0].security_questions = null;
                var userData = mapfreResponse.data[0];
                var userPut = new PasswordChangeRequest(userData.username, userData.name, userData.surname1, userData.surname2, userData.identification_number, userData.mobile_phone, userData.mail, userData.address, _this.changePasswordForm.value.password, userData.security_questions);
                _this.userProvider.sendPasswordChange(userPut, userData.id).subscribe(function (response) {
                    var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                    var success = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
                    if (success) {
                        _this.events.publish(BlockedUserPasswordChangeComponent.onPasswordChangeEvent, mapfreResponse);
                        _this.events.publish(BlockedUserPasswordChangeComponent.onForgotPasswordEvent, mapfreResponse);
                    }
                    _this.scrollToTopPage();
                });
            }
        });
    };
    BlockedUserPasswordChangeComponent.prototype.passwordsValidator = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value && confirmPassword.value && password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    BlockedUserPasswordChangeComponent.prototype.passwordValidator = function (input) {
        if (input.value === input.root.value['password']) {
            return null;
        }
        else {
            return {
                isValid: true
            };
        }
    };
    BlockedUserPasswordChangeComponent.prototype.closeResponse = function () {
        this.passwordsMismatched = false;
    };
    BlockedUserPasswordChangeComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    BlockedUserPasswordChangeComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    BlockedUserPasswordChangeComponent.componentName = 'blocked-user-password-change';
    BlockedUserPasswordChangeComponent.onCloseEvent = BlockedUserPasswordChangeComponent.componentName + ":close";
    BlockedUserPasswordChangeComponent.onPasswordChangeEvent = BlockedUserPasswordChangeComponent.componentName + ":password-change";
    BlockedUserPasswordChangeComponent.onForgotPasswordEvent = BlockedUserPasswordChangeComponent.componentName + ":forgot-password";
    BlockedUserPasswordChangeComponent.inputs = ['userName'];
    BlockedUserPasswordChangeComponent.configInputs = ['mocked', 'icons', 'styles'];
    BlockedUserPasswordChangeComponent.decorators = [
        { type: Component, args: [{
                    selector: 'blocked-user-password-change',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin *ngIf=\"componentReady\" id=\"passwordsMismatched\" [ngStyle]=\"styles\">    <form [formGroup]=\"changePasswordForm\" (ngSubmit)=\"attemptPasswordChange()\">      <ion-item class=\"alert warning drop-shadow\" *ngIf=\"passwordsMismatched\">       <ion-icon [name]=\"getIcon(0)\" class=\"login-response-warning-icon\" small item-start></ion-icon>       <p class=\"horizontal-margin-m login-response-message\">         {{ componentSettings.new_password_passwords_mismatch_tag }}       </p>       <ion-icon [name]=\"getIcon(1)\" class=\"clickable login-response-close-icon\" small item-end tabindex=\"0\"                 (keyup.enter)=\"closeResponse()\" (click)=\"closeResponse()\">       </ion-icon>     </ion-item>      <ion-item>       <ion-icon [name]=\"getIcon(2)\" class=\"clickable\" small item-end tabindex=\"0\"                 (keyup.enter)=\"onClose($event)\" (click)=\"onClose($event)\">       </ion-icon>     </ion-item>      <ion-card-content class=\"no-padding-top\">        <h3 class=\"margin-top-m\" text-center>         {{ componentSettings.title }}       </h3>        <h5 class=\"fmm-primary-text fmm-regular vertical-margin-s\" text-center>         {{ componentSettings.subtitle }}       </h5>        <ion-item no-padding class=\"required\">         <ion-label floating>           {{ componentSettings.new_password_password_tag }}         </ion-label>         <ion-input type=\"password\" formControlName=\"password\" (ionBlur)=\"onPasswordBlur()\"></ion-input>       </ion-item>        <ion-item no-padding class=\"required\">         <ion-label floating>           {{ componentSettings.new_password_repeat_password_tag }}         </ion-label>         <ion-input type=\"password\" formControlName=\"validationPassword\" (ionBlur)=\"onPasswordBlur()\"></ion-input>       </ion-item>        <div class=\"margin-top-xl\">         <button ion-button full text-uppercase [disabled]=\"!changePasswordForm.valid\">           {{ componentSettings.confirm_button_tag }}         </button>       </div>      </ion-card-content>    </form> </ion-card>"
                },] },
    ];
    BlockedUserPasswordChangeComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: TranslationProvider, },
        { type: FormBuilder, },
        { type: UserProvider, },
        { type: Events, },
    ]; };
    BlockedUserPasswordChangeComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'userName': [{ type: Input },],
    };
    return BlockedUserPasswordChangeComponent;
}());
export { BlockedUserPasswordChangeComponent };
//# sourceMappingURL=blocked-user-password-change.js.map