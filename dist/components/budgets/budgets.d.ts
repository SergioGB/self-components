import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { FormatProvider } from '../../providers/format/format';
import { Budget } from '../../models/budget';
export declare class BudgetsComponent implements OnInit {
    private translationProvider;
    private componentSettingsProvider;
    private formatProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    ready: boolean;
    budgets: Budget[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider, formatProvider: FormatProvider);
    ngOnInit(): void;
    formatDate(date: Date): string;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
