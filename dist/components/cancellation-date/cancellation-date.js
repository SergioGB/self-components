import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import * as moment from 'moment';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
var CancellationDateComponent = (function () {
    function CancellationDateComponent(navCtrl, translationProvider, platformProvider, events) {
        this.navCtrl = navCtrl;
        this.translationProvider = translationProvider;
        this.platformProvider = platformProvider;
        this.events = events;
        this.defaultIcons = ["mapfre-check", "mapfre-no-check"];
        this.dateOptions = [];
        this.translationProvider.bind(this);
        this.platformProvider.bindTo(this);
        this.componentReady = true;
    }
    CancellationDateComponent.prototype.ngOnInit = function () {
        this.currentMoment = moment().toISOString();
        this.otherDate = new Date();
        if (this.cancellationDateSelection) {
            this.selectedDateOption = this.cancellationDateSelection.dateOption;
            this.otherDate = this.cancellationDateSelection.date || this.otherDate;
        }
        this.dateOptions = [{
                id: 1,
                title: this.translationProvider.getValueForKey('cancellation-date.cancelation_date.option.today')
            }, {
                id: 2,
                title: this.translationProvider.getValueForKey('cancellation-date.cancelation_date.option.other_date'),
                date: true
            }, {
                id: 3,
                title: this.translationProvider.getValueForKey('cancellation-date.cancelation_date.option.due_date')
            }];
        this.publishSelection();
    };
    CancellationDateComponent.prototype.setDateOption = function (dateOption) {
        this.selectedDateOption = dateOption;
        this.publishSelection();
    };
    CancellationDateComponent.prototype.publishSelection = function () {
        if (this.selectedDateOption) {
            this.cancellationDateSelection = {
                dateOption: this.selectedDateOption
            };
            if (this.selectedDateOption.date) {
                this.cancellationDateSelection.date = this.otherDate;
            }
            this.events.publish(CancellationDateComponent.selectedEvent, this.cancellationDateSelection);
            this.events.publish(CancellationDateComponent.completedEvent, !!this.cancellationDateSelection);
        }
    };
    CancellationDateComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    CancellationDateComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    CancellationDateComponent.componentName = "CancellationDateComponent";
    CancellationDateComponent.selectedEvent = CancellationDateComponent.componentName + ":selected";
    CancellationDateComponent.completedEvent = CancellationDateComponent.componentName + ":completed";
    CancellationDateComponent.inputs = ['cancellationDateSelection'];
    CancellationDateComponent.configInputs = ['mocked', 'icons', 'styles'];
    CancellationDateComponent.decorators = [
        { type: Component, args: [{
                    selector: 'cancellation-date',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["cancellation-date mat-datepicker-toggle {   margin-right: 80px !important; }"],
                    template: "<ion-card>   <loading-spinner *ngIf=\"!componentReady\"></loading-spinner>   <div *ngIf=\"componentReady\" [ngStyle]=\"styles\" [ngStyle]=\"styles\">      <ion-card-header>       <ion-item>         <h3>           {{ translate('cancellation-date.cancelation_date.title') }}         </h3>       </ion-item>     </ion-card-header>      <ion-card-content>       <div *ngFor=\"let dateOption of dateOptions\">         <ion-item         no-padding tabindex=\"0\" class=\"clickable\"         [class.no-label-margin]=\"dateOption.date\"         (click)=\"setDateOption(dateOption)\">            <ion-icon             [name]=\"(selectedDateOption && selectedDateOption.id === dateOption.id) ? getIcon(0) : getIcon(1)\" item-start>           </ion-icon>            <ion-grid no-padding>             <ion-row *ngIf=\"dateOption.date\">                <ion-col col-12 col-sm-8 col-xl-9 class=\"flex center-contents\">                 <h4>                   {{ dateOption.title }}                 </h4>               </ion-col>              </ion-row>             <ion-row *ngIf=\"!dateOption.date\">                <ion-col col-12>                 <h4>                   {{ dateOption.title }}                 </h4>               </ion-col>              </ion-row>           </ion-grid>          </ion-item>         <ion-row *ngIf=\"dateOption.date\">           <ion-col col-12 col-sm-4 col-xl-3>             <mapfre-datetime               item-content               [min]=\"currentMoment\"               [(mModel)]=\"otherDate\"               [calendar]=\"true\"               [displayFormat]=\"'DD/MM/YYYY'\"               [pickerFormat]=\"'DD MMMM YYYY'\"               [placeHolder]=\"'Elige tu fecha'\">             </mapfre-datetime>           </ion-col>         </ion-row>       </div>     </ion-card-content>   </div>  </ion-card>"
                },] },
    ];
    CancellationDateComponent.ctorParameters = function () { return [
        { type: NavController, },
        { type: TranslationProvider, },
        { type: PlatformProvider, },
        { type: Events, },
    ]; };
    CancellationDateComponent.propDecorators = {
        'cancellationDateSelection': [{ type: Input },],
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return CancellationDateComponent;
}());
export { CancellationDateComponent };
//# sourceMappingURL=cancellation-date.js.map