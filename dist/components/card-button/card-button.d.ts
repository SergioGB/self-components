import { OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavigationProvider } from '../../providers/navigation/navigation';
export declare class CardButtonComponent implements OnInit {
    private navCtrl;
    private navigationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    button: any;
    isModeSmall: boolean;
    defaultIcons: string[];
    componentReady: boolean;
    static configInputs: string[];
    constructor(navCtrl: NavController, navigationProvider: NavigationProvider);
    ngOnInit(): void;
    goToDetail(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
