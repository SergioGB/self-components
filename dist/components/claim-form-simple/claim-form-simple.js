import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { DocumentsProvider } from '../../providers/documents/documents';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { IncidencesAndClaimsProvider } from '../../providers/incidences-and-claims/incidences-and-claims';
import { Product } from '../../models/product';
import { UserProvider } from '../../providers/user/user';
import { Observable } from 'rxjs/Observable';
var ClaimFormSimpleComponent = (function () {
    function ClaimFormSimpleComponent(navCtrl, formBuilder, translationProvider, documentsProvider, userProvider, componentSettingsProvider, incidencesAndClaimsProvider, events) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.documentsProvider = documentsProvider;
        this.userProvider = userProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.incidencesAndClaimsProvider = incidencesAndClaimsProvider;
        this.events = events;
        this.selectOptions = {
            cssClass: 'popover-content-cm'
        };
        this.translationProvider.bind(this);
        this.initializeData();
        this.initializeForm();
    }
    ClaimFormSimpleComponent.prototype.ngOnInit = function () {
        this.queryFormData();
    };
    ClaimFormSimpleComponent.prototype.onBlurEmail = function () {
        this.wrongEmail =
            this.claimForm.value.email &&
                !this.claimForm.controls['email'].valid && this.claimForm.controls['email'].dirty;
    };
    ClaimFormSimpleComponent.prototype.onBlurPhone = function () {
        this.wrongPhone =
            this.claimForm.value.phone_number &&
                !this.claimForm.controls['phone_number'].valid && this.claimForm.controls['phone_number'].dirty;
    };
    ClaimFormSimpleComponent.prototype.confirmForm = function () {
        if (this.mocked) {
            this.redirectToConfirmation();
        }
        else {
            this.attemptSubmission();
        }
    };
    ClaimFormSimpleComponent.prototype.queryFormData = function () {
        var _this = this;
        Observable.forkJoin([
            this.componentSettingsProvider.getInsuranceSettings(this.mocked),
            this.incidencesAndClaimsProvider.getClaimReasons(this.mocked),
            this.incidencesAndClaimsProvider.getClaimDocuments(this.mocked)
        ]).subscribe(function (responses) {
            if (_this.componentSettingsProvider.areResponsesValid(responses)) {
                if (_this.componentSettingsProvider.getResponseJSON(responses[0])) {
                    _this.loadInsuranceProducts(_this.componentSettingsProvider.getResponseJSON(responses[0]).data);
                }
                _this.claimReasons = _this.componentSettingsProvider.getResponseJSON(responses[1]).data;
                _this.associated_documents = _this.componentSettingsProvider.getResponseJSON(responses[2]).data || [];
                _this.componentReady = true;
            }
        });
    };
    ClaimFormSimpleComponent.prototype.loadInsuranceProducts = function (insurances) {
        var _this = this;
        insurances.forEach(function (insurance) {
            insurance.associated_policies.forEach(function (policy) {
                _this.products.push(new Product(policy.id, insurance.name + " - " + policy.type));
            });
        });
    };
    ClaimFormSimpleComponent.prototype.attemptSubmission = function () {
        var _this = this;
        if (this.claimForm.valid) {
            this.userProvider.getUserData().subscribe(function (userData) {
                if (userData) {
                    var complaintFormData = {
                        client_personal_data: userData.personal_data,
                        complaint_data: _this.claimForm.value,
                        associated_documents: _this.associated_documents
                    };
                    _this.incidencesAndClaimsProvider.postClaimForm(_this.mocked, complaintFormData).subscribe(function (response) {
                        var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                        var success = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
                        if (success) {
                            _this.redirectToConfirmation();
                        }
                    });
                }
            });
        }
    };
    ClaimFormSimpleComponent.prototype.redirectToConfirmation = function () {
        this.events.publish('claim-form-simple::goConfirmIncidence');
    };
    ClaimFormSimpleComponent.prototype.initializeData = function () {
        this.associated_documents = [];
        this.products = [];
    };
    ClaimFormSimpleComponent.prototype.initializeForm = function () {
        this.claimForm = this.formBuilder.group({
            phone_number: ['', [Validators.required, Validators.pattern('[0-9]{9}')]],
            email: ['', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
            product_id: [''],
            contract_number: ['', Validators.required],
            reason_id: [''],
            reason_description: [''],
            need: ['']
        });
    };
    ClaimFormSimpleComponent.configInputs = ['mocked', 'styles'];
    ClaimFormSimpleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'claim-form-simple',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["claim-form-simple .minimum-width {   min-width: 200px; }"],
                    template: "<loading-spinner *ngIf=\"!componentReady\"></loading-spinner> <form *ngIf=\"componentReady\" [formGroup]=\"claimForm\" class=\"incidence-and-claims-form\" [ngStyle]=\"styles\">   <ion-card>     <ion-card-header>       <ion-item>         <h3>           {{ translate('claim-form-simple.claim_form_simple.fill_data') }}         </h3>       </ion-item>     </ion-card-header>      <ion-card-content class=\"contentCard\">       <ion-row class=\"content-form\">         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item class=\"required\">             <ion-label floating>               {{translate('claim-form-simple.claim_form_simple.phone_number')}}             </ion-label>             <ion-input type=\"tel\" formControlName=\"phone_number\" pattern=\"(\\\\+[0-9]*)?[0-9]*\" (ionBlur)=\"onBlurPhone()\">             </ion-input>           </ion-item>           <div *ngIf=\"wrongPhone\" class=\"format-error\">             <span>               {{ translate('claim-form-simple.claim_form_simple.phone_format_error') }}.             </span>           </div>         </ion-col>         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{translate('claim-form-simple.claim_form_simple.mail')}}             </ion-label>             <ion-input type=\"text\" formControlName=\"email\" (ionBlur)=\"onBlurEmail()\"></ion-input>           </ion-item>           <div *ngIf=\"wrongEmail\" class=\"format-error\">             <span>               {{ translate('claim-form-simple.claim_form_simple.email_format_error') }}.             </span>           </div>         </ion-col>         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{translate('claim-form-simple.claim_form_simple.select_product')}}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" formControlName=\"product_id\">               <ion-option *ngFor=\"let product of products\" value=\"{{product.id}}\">                 {{product.name}}               </ion-option>             </ion-select>           </ion-item>         </ion-col>         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item class=\"required\">             <ion-label floating>               {{translate('claim-form-simple.claim_form_simple.contract_number')}}             </ion-label>             <ion-input type=\"text\" formControlName=\"contract_number\"></ion-input>           </ion-item>         </ion-col>       </ion-row>       <ion-row class=\"text-area\">         <ion-col col-12>           <ion-item class=\"text-area margin-bottom-xs\">              <ion-label floating>               {{translate('claim-form-simple.claim_form_simple.reason_description')}}             </ion-label>              <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\" formControlName=\"reason_description\">             </ion-textarea>            </ion-item>          </ion-col>       </ion-row>       <ion-row class=\"text-area\">         <ion-col col-12>           <ion-item class=\"text-area margin-bottom-xs\">              <ion-label floating>               {{translate('claim-form-simple.claim_form_simple.what_you_need')}}             </ion-label>              <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\" formControlName=\"need\">             </ion-textarea>            </ion-item>         </ion-col>       </ion-row>     </ion-card-content>   </ion-card>    <file-uploader-card [title]=\"translate('claim-form-simple.claim_form_simple.upload_documents.title')\"     [allowedTypes]=\"['pdf', 'image', 'video']\" [files]=\"associated_documents\">   </file-uploader-card>    <ion-grid no-padding>     <ion-row>       <ion-col col-12 col-sm-3 col-lg-2 class=\"margin-left-auto minimum-width\">         <button [disabled]=\"!claimForm.valid\" full text-uppercase (click)=\"confirmForm()\" ion-button>           {{ translate('claim-form-simple.claim_form_simple.confirm_button') }}         </button>       </ion-col>     </ion-row>   </ion-grid>  </form>"
                },] },
    ];
    ClaimFormSimpleComponent.ctorParameters = function () { return [
        { type: NavController, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: DocumentsProvider, },
        { type: UserProvider, },
        { type: ComponentSettingsProvider, },
        { type: IncidencesAndClaimsProvider, },
        { type: Events, },
    ]; };
    ClaimFormSimpleComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return ClaimFormSimpleComponent;
}());
export { ClaimFormSimpleComponent };
//# sourceMappingURL=claim-form-simple.js.map