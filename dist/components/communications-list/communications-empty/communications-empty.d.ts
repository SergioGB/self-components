import { TranslationProvider } from '../../../providers/translation/translation';
export declare class CommunicationsEmptyComponent {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    getIcon(position: number): string;
    existIcon(position: number): string;
}
