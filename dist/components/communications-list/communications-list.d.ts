import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
export declare class CommunicationsListComponent implements OnInit {
    private componentSettingsProvider;
    private translationProvider;
    mocked?: boolean;
    styles?: any;
    icons?: any[];
    communicationsReady: boolean;
    communications: any;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider);
    ngOnInit(): void;
    groupByDate(communications: any): any;
}
