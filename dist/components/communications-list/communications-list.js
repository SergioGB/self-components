import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
var CommunicationsListComponent = (function () {
    function CommunicationsListComponent(componentSettingsProvider, translationProvider) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
    }
    CommunicationsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.componentSettingsProvider.getCommunicationsSettingsByState(this.mocked, 'ALL').subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.communications = _this.groupByDate(mapfreResponse.data);
                _this.communicationsReady = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
            }
        });
    };
    CommunicationsListComponent.prototype.groupByDate = function (communications) {
        var communicationsMap = new Map();
        if (communications && communications.length) {
            for (var _i = 0, communications_1 = communications; _i < communications_1.length; _i++) {
                var communication = communications_1[_i];
                var communicationDate = this.translationProvider.getMonthTagByDate(new Date(communication.date)) + "\n           " + new Date(communication.date).getFullYear();
                if (communicationsMap.has(communicationDate)) {
                    var existingCommunications = communicationsMap.get(communicationDate);
                    existingCommunications.push(communication);
                    communicationsMap.set(communicationDate, existingCommunications);
                }
                else {
                    communicationsMap.set(communicationDate, [communication]);
                }
            }
        }
        return communicationsMap;
    };
    CommunicationsListComponent.configInputs = ['mocked', 'styles', 'icons', 'modals'];
    CommunicationsListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'communications-list',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["communications-list > div {   margin-top: 10px; }"],
                    template: "<loading-spinner *ngIf=\"!communicationsReady\"></loading-spinner>  <div *ngIf=\"communicationsReady\" [ngStyle]=\"styles\">    <div *ngIf=\"communications && communications.size\">     <communications       *ngFor=\"let group of communications.keys(); let i = index\"       [order]=\"i\"       [group]=\"group\"       [communications]=\"communications\"       [icons]=\"icons\">     </communications>   </div>    <communications-empty     *ngIf=\"!(communications && communications.size)\">   </communications-empty>  </div>"
                },] },
    ];
    CommunicationsListComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: TranslationProvider, },
    ]; };
    CommunicationsListComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'icons': [{ type: Input },],
    };
    return CommunicationsListComponent;
}());
export { CommunicationsListComponent };
//# sourceMappingURL=communications-list.js.map