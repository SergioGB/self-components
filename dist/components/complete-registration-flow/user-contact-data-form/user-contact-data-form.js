import { ViewEncapsulation } from '@angular/core';
import { ConfigProvider } from '../../../models/self-component.config';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Events } from 'ionic-angular';
import * as _ from 'lodash';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { FormUtil } from '../../../providers/utils/form';
var UserContactDataFormComponent = (function () {
    function UserContactDataFormComponent(events, formBuilder, translationProvider, componentSettingsProvider) {
        this.events = events;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.invalidFields = {};
        this.selectOptions = {
            cssClass: 'popover-content-cm'
        };
        this.translationProvider.bind(this);
    }
    UserContactDataFormComponent.prototype.ngOnInit = function () {
        this.queryFormData();
    };
    UserContactDataFormComponent.prototype.ngOnDestroy = function () {
        if (this.valueChangesSubscription) {
            this.valueChangesSubscription.unsubscribe();
        }
    };
    UserContactDataFormComponent.prototype.invalidateOnBlur = function (fieldName) {
        this.invalidFields[fieldName] = this.registrationForm.value[fieldName] &&
            (!this.registrationForm.controls[fieldName].valid && this.registrationForm.controls[fieldName].dirty);
    };
    UserContactDataFormComponent.prototype.validateIfPhonesMatch = function () {
        this.phonesMismatch = !this.doPhonesMatch();
    };
    UserContactDataFormComponent.prototype.validateIfEmailsMatch = function () {
        this.emailsMismatch = !this.doEmailsMatch();
    };
    UserContactDataFormComponent.prototype.doPhonesMatch = function () {
        var phoneNumber = this.registrationForm.value.phone_number;
        var repeatPhoneNumber = this.registrationForm.value.repeat_phone_number;
        return !(_.size(phoneNumber) && _.size(repeatPhoneNumber)) || phoneNumber === repeatPhoneNumber;
    };
    UserContactDataFormComponent.prototype.doEmailsMatch = function () {
        var email = this.registrationForm.value.email;
        var repeatEmail = this.registrationForm.value.repeat_email;
        return !(_.size(email) && _.size(repeatEmail)) || email === repeatEmail;
    };
    UserContactDataFormComponent.prototype.queryFormData = function () {
        var _this = this;
        Observable.forkJoin([
            this.componentSettingsProvider.getContactPreferences(this.mocked),
            this.componentSettingsProvider.getTimeRanges(this.mocked)
        ]).subscribe(function (responses) {
            if (_this.componentSettingsProvider.areResponsesValid(responses)) {
                _this.contact_platforms = _this.componentSettingsProvider.getResponseJSON(responses[0]).data;
                _this.time_ranges = _this.componentSettingsProvider.getResponseJSON(responses[1]).data;
                _this.initializeForm();
            }
        });
    };
    UserContactDataFormComponent.prototype.initializeForm = function () {
        var _this = this;
        this.registrationForm = this.formBuilder.group({
            phone_number: [undefined, [Validators.required, Validators.pattern(ConfigProvider.config.phonePattern)]],
            repeat_phone_number: [undefined, [Validators.required, Validators.pattern(ConfigProvider.config.phonePattern)]],
            email: ['', [Validators.required, Validators.email]],
            repeat_email: ['', [Validators.required, Validators.email]],
            contact_platform: [undefined],
            time_range: [undefined]
        });
        if (this.userContactData) {
            FormUtil.mapFormData(this.userContactData, this.registrationForm);
            this.publishFormData();
        }
        FormUtil.mapListItem(this.contact_platforms, 'contact_platform', this.registrationForm);
        FormUtil.mapListItem(this.time_ranges, 'time_range', this.registrationForm);
        this.valueChangesSubscription = this.registrationForm.valueChanges.subscribe(function () {
            _this.publishFormData();
        });
    };
    UserContactDataFormComponent.prototype.publishFormData = function () {
        this.events.publish(UserContactDataFormComponent.updatedEvent, this.registrationForm.value);
        this.updateCompleteness();
    };
    UserContactDataFormComponent.prototype.updateCompleteness = function () {
        this.events.publish(UserContactDataFormComponent.completedEvent, this.registrationForm.valid &&
            this.doPhonesMatch() &&
            this.doEmailsMatch());
    };
    UserContactDataFormComponent.componentName = "UserContactDataFormComponent";
    UserContactDataFormComponent.updatedEvent = UserContactDataFormComponent.componentName + ":updated";
    UserContactDataFormComponent.completedEvent = UserContactDataFormComponent.componentName + ":completed";
    UserContactDataFormComponent.inputs = ['userContactData'];
    UserContactDataFormComponent.configInputs = ['mocked', 'styles'];
    UserContactDataFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'user-contact-data-form',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card>   <loading-spinner *ngIf=\"!registrationForm\"></loading-spinner>    <div *ngIf=\"registrationForm\" [ngStyle]=\"styles\">     <ion-card-header class=\"no-padding-bottom\">       <ion-item>         <h2 [class.margin-bottom-xs]=\"onMobile\">           {{ translate('user-contact-data-form.new_account.user-contact-data') }}         </h2>          <span class=\"secondary-text\" [attr.item-end]=\"onMobile ? null : ''\">           {{ translate('user-contact-data-form.generic.required_fields') }}         </span>       </ion-item>     </ion-card-header>      <ion-card-content>       <form [formGroup]=\"registrationForm\">         <ion-grid class=\"no-padding\">           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-contact-data-form.new_account.mobile_phone') }}                 </ion-label>                  <ion-input                   type=\"tel\" formControlName=\"phone_number\"                   (ionBlur)=\"invalidateOnBlur('phone_number');validateIfPhonesMatch()\" maxlength=\"20\">                 </ion-input>               </ion-item>                <div *ngIf=\"invalidFields.phone_number\" class=\"input-value-error\">                 <span>                   {{ translate('user-contact-data-form.new_account.phone_format_error') }}.                 </span>               </div>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-contact-data-form.new_account.repeat_mobile_phone') }}                 </ion-label>                  <ion-input                   type=\"tel\" formControlName=\"repeat_phone_number\"                   (ionBlur)=\"invalidateOnBlur('repeat_phone_number');validateIfPhonesMatch()\" maxlength=\"20\">                 </ion-input>               </ion-item>                <div *ngIf=\"invalidFields.repeat_phone_number\" class=\"input-value-error\">                 <span>                   {{ translate('user-contact-data-form.new_account.phone_format_error') }}.                 </span>               </div>              </ion-col>             <ion-col col-12 *ngIf=\"phonesMismatch\" class=\"input-value-error\">                <span>                 {{ translate('user-contact-data-form.new_account.phones_mismatch') }}               </span>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-contact-data-form.new_account.mail') }}                 </ion-label>                  <ion-input                   type=\"email\" formControlName=\"email\"                   (ionBlur)=\"invalidateOnBlur('email');validateIfEmailsMatch()\" maxlength=\"50\">                 </ion-input>               </ion-item>                <div *ngIf=\"invalidFields.email\" class=\"input-value-error\">                 <span>                   {{ translate('user-contact-data-form.new_account.email_format_error') }}                 </span>               </div>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-contact-data-form.new_account.repeat_mail') }}                 </ion-label>                  <ion-input                   type=\"email\" formControlName=\"repeat_email\"                   (ionBlur)=\"invalidateOnBlur('repeat_email');validateIfEmailsMatch()\" maxlength=\"50\">                 </ion-input>               </ion-item>                <div *ngIf=\"invalidFields.repeat_email\" class=\"input-value-error\">                 <span>                   {{ translate('user-contact-data-form.new_account.email_format_error') }}                 </span>               </div>              </ion-col>             <ion-col col-12 *ngIf=\"emailsMismatch\" class=\"input-value-error\">                <span>                 {{ translate('user-contact-data-form.new_account.mails_mismatch') }}               </span>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding>                 <ion-label floating>                   {{ translate('user-contact-data-form.new_account.contact_platform') }}                 </ion-label>                  <ion-select class=\"form-select\" [selectOptions]=\"selectOptions\" formControlName=\"contact_platform\" full>                   <ion-option *ngFor=\"let contact_platform of contact_platforms\" [value]=\"contact_platform\">                     {{ contact_platform.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding>                 <ion-label floating>                   {{ translate('user-contact-data-form.new_account.time_range') }}                 </ion-label>                  <ion-select class=\"form-select\" [selectOptions]=\"selectOptions\" formControlName=\"time_range\" full>                   <ion-option *ngFor=\"let time_range of time_ranges\" [value]=\"time_range\">                     {{ time_range.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>           </ion-row>         </ion-grid>        </form>     </ion-card-content>   </div> </ion-card>"
                },] },
    ];
    UserContactDataFormComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
    ]; };
    UserContactDataFormComponent.propDecorators = {
        'userContactData': [{ type: Input },],
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return UserContactDataFormComponent;
}());
export { UserContactDataFormComponent };
//# sourceMappingURL=user-contact-data-form.js.map