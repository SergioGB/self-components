import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Utils } from '../../../providers/utils/utils';
import { FormUtil } from '../../../providers/utils/form';
var UserDataFormComponent = (function () {
    function UserDataFormComponent(events, formBuilder, translationProvider, componentSettingsProvider) {
        this.events = events;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.defaultIcons = ["warning", "close-circle"];
        this.selectOptions = {
            cssClass: 'popover-content-cm'
        };
        this.translationProvider.bind(this);
    }
    UserDataFormComponent.prototype.ngOnInit = function () {
        this.queryFormData();
    };
    UserDataFormComponent.prototype.ngOnDestroy = function () {
        if (this.valueChangesSubscription) {
            this.valueChangesSubscription.unsubscribe();
        }
    };
    UserDataFormComponent.prototype.checkIfUserAlreadyExists = function () {
        var userAlreadyExists = false;
        var enteredNumber = this.registrationForm.value.identification_number;
        if (enteredNumber) {
            this.existingUsers.forEach(function (user) {
                if (Utils.equalsIgnoreCase(enteredNumber, user.personal_data.identification_document_number)) {
                    userAlreadyExists = true;
                }
            });
        }
        this.userAlreadyExists = userAlreadyExists;
        this.updateCompleteness();
    };
    UserDataFormComponent.prototype.closeResponse = function () {
        this.errorGotten = !this.errorGotten;
    };
    UserDataFormComponent.prototype.queryFormData = function () {
        var _this = this;
        Observable.forkJoin([
            this.componentSettingsProvider.getClientsSettings(this.mocked, 'ALL'),
            this.componentSettingsProvider.getIdentificationDocuments(this.mocked),
            this.componentSettingsProvider.getRoadTypesSettings(this.mocked),
            this.componentSettingsProvider.getCitiesSettings(this.mocked),
            this.componentSettingsProvider.getProvincesSettings(this.mocked),
            this.componentSettingsProvider.getCountriesSettings(this.mocked)
        ]).subscribe(function (responses) {
            if (_this.componentSettingsProvider.areResponsesValid(responses)) {
                _this.existingUsers = _this.componentSettingsProvider.getResponseJSON(responses[0]).data;
                _this.identificationDocuments = _this.componentSettingsProvider.getResponseJSON(responses[1]).data;
                _this.roadTypes = _this.componentSettingsProvider.getResponseJSON(responses[2]).data;
                _this.cities = _this.componentSettingsProvider.getResponseJSON(responses[3]).data;
                _this.provinces = _this.componentSettingsProvider.getResponseJSON(responses[4]).data;
                _this.countries = _this.componentSettingsProvider.getResponseJSON(responses[5]).data;
                _this.initializeForm();
            }
        });
    };
    UserDataFormComponent.prototype.initializeForm = function () {
        var _this = this;
        this.registrationForm = this.formBuilder.group({
            name: ['', Validators.required],
            surname1: ['', Validators.required],
            surname2: ['', Validators.required],
            identification_document: [undefined, Validators.required],
            identification_number: ['', [Validators.required]],
            road_type: [undefined, Validators.required],
            road_name: ['', Validators.required],
            road_number: ['', Validators.required],
            postal_code: ['', Validators.required],
            city: [undefined, Validators.required],
            province: [undefined, Validators.required],
            country: [undefined, Validators.required]
        });
        if (this.userData) {
            FormUtil.mapFormData(this.userData, this.registrationForm);
            this.publishFormData();
        }
        FormUtil.mapListItem(this.identificationDocuments, 'identification_document', this.registrationForm);
        FormUtil.mapListItem(this.roadTypes, 'road_type', this.registrationForm);
        FormUtil.mapListItem(this.cities, 'city', this.registrationForm);
        FormUtil.mapListItem(this.provinces, 'province', this.registrationForm);
        FormUtil.mapListItem(this.countries, 'country', this.registrationForm);
        this.valueChangesSubscription = this.registrationForm.valueChanges.subscribe(function () {
            _this.publishFormData();
        });
    };
    UserDataFormComponent.prototype.publishFormData = function () {
        this.events.publish(UserDataFormComponent.updatedEvent, this.registrationForm.value);
        this.updateCompleteness();
    };
    UserDataFormComponent.prototype.updateCompleteness = function () {
        this.events.publish(UserDataFormComponent.completedEvent, this.registrationForm.valid && !this.errorGotten && !this.userAlreadyExists);
    };
    UserDataFormComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    UserDataFormComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    UserDataFormComponent.componentName = "UserDataFormComponent";
    UserDataFormComponent.updatedEvent = UserDataFormComponent.componentName + ":updated";
    UserDataFormComponent.completedEvent = UserDataFormComponent.componentName + ":completed";
    UserDataFormComponent.inputs = ['userData'];
    UserDataFormComponent.configInputs = ['mocked', 'icons', 'styles'];
    UserDataFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'user-data-form',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card>   <loading-spinner *ngIf=\"!registrationForm\"></loading-spinner>    <div *ngIf=\"registrationForm\" [ngStyle]=\"styles\">     <ion-item *ngIf=\"userAlreadyExists\" class=\"alert warning\">       <ion-icon [name]=\"getIcon(0)\" class=\"login-response-warning-icon\" small item-start></ion-icon>        <p class=\"horizontal-margin-m\">         {{ translate('user-data-form.new_account.identification_warning') }}       </p>        <ion-icon         [name]=\"getIcon(1)\" tabindex=\"0\" item-end small class=\"clickable login-response-close-icon\"         (click)=\"closeResponse()\">       </ion-icon>        <div class=\"padding\">         <ion-item class=\"warning alert no-padding margin-left-s\">           <a             class=\"uppercase clickable warning\"             [title]=\"translate('user-data-form.new_account.recover_password')\"             (click)=\"goToForgottenPassword()\">              {{ translate('user-data-form.new_account.recover_password') }}           </a>         </ion-item>       </div>     </ion-item>      <ion-card-header class=\"no-padding-bottom\">       <ion-item>         <h2 [class.margin-bottom-xs]=\"onMobile\">           {{ translate('user-data-form.new_account.user-contact-data') }}         </h2>          <span class=\"secondary-text\" [attr.item-end]=\"onMobile ? null : ''\">           {{ translate('user-data-form.generic.required_fields') }}         </span>       </ion-item>     </ion-card-header>      <ion-card-content>       <form [formGroup]=\"registrationForm\">         <ion-grid class=\"no-padding\">           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.name') }}                 </ion-label>                  <ion-input                   type=\"text\"                   formControlName=\"name\"                   maxlength=\"50\">                 </ion-input>               </ion-item>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.surname1') }}                 </ion-label>                  <ion-input                   type=\"text\"                   formControlName=\"surname1\"                   maxlength=\"20\">                 </ion-input>               </ion-item>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.surname2') }}                 </ion-label>                  <ion-input                   type=\"text\"                   formControlName=\"surname2\"                   maxlength=\"20\">                 </ion-input>               </ion-item>              </ion-col>             <ion-col col-6 col-md-3>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.document') }}                 </ion-label>                  <ion-select class=\"form-select\" [selectOptions]=\"selectOptions\" formControlName=\"identification_document\" full>                   <ion-option *ngFor=\"let identification_document of identificationDocuments\" [value]=\"identification_document\">                     {{ identification_document.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>             <ion-col col-6 col-md-3>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.identity_document') }}                 </ion-label>                  <ion-input                   type=\"text\"                   formControlName=\"identification_number\"                   maxlength=\"20\"                   (keyup)=\"checkIfUserAlreadyExists()\">                 </ion-input>               </ion-item>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12 col-md-3>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.address') }}                 </ion-label>                  <ion-select class=\"form-select\" [selectOptions]=\"selectOptions\" formControlName=\"road_type\" full>                   <ion-option *ngFor=\"let road_type of roadTypes\" [value]=\"road_type\">                     {{ road_type.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>             <ion-col col-12 col-md-9>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.road_name') }}                 </ion-label>                  <ion-input                   type=\"text\"                   maxlength=\"50\"                   formControlName=\"road_name\">                 </ion-input>               </ion-item>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-6 col-md-3>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.road_number') }}                 </ion-label>                  <ion-input                   type=\"text\"                   maxlength=\"50\"                   formControlName=\"road_number\">                 </ion-input>               </ion-item>              </ion-col>             <ion-col col-6 col-md-3>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.cp') }}                 </ion-label>                  <ion-input                   type=\"text\"                   maxlength=\"50\"                   formControlName=\"postal_code\">                 </ion-input>               </ion-item>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.city') }}                 </ion-label>                  <ion-select class=\"form-select\" [selectOptions]=\"selectOptions\" formControlName=\"city\" full>                   <ion-option *ngFor=\"let city of cities\" [value]=\"city\">                     {{ city.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.province') }}                 </ion-label>                  <ion-select class=\"form-select\" [selectOptions]=\"selectOptions\" formControlName=\"province\" full>                   <ion-option *ngFor=\"let province of provinces\" [value]=\"province\">                     {{ province.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-data-form.new_account.country') }}                 </ion-label>                  <ion-select class=\"form-select\" [selectOptions]=\"selectOptions\" formControlName=\"country\" full>                   <ion-option *ngFor=\"let country of countries\" [value]=\"country\">                     {{ country.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>           </ion-row>         </ion-grid>        </form>      </ion-card-content>   </div> </ion-card>"
                },] },
    ];
    UserDataFormComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
    ]; };
    UserDataFormComponent.propDecorators = {
        'userData': [{ type: Input },],
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'icons': [{ type: Input },],
    };
    return UserDataFormComponent;
}());
export { UserDataFormComponent };
//# sourceMappingURL=user-data-form.js.map