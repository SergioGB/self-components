import { ViewEncapsulation } from '@angular/core';
import { Events } from 'ionic-angular';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ModalController } from 'ionic-angular';
import * as _ from 'lodash';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { TranslationProvider } from '../../../providers/translation/translation';
import { HtmlContentModalComponent } from '../../_modals/html-content-modal/html-content-modal';
import { FormUtil } from '../../../providers/utils/form';
var UserSecurityDataFormComponent = (function () {
    function UserSecurityDataFormComponent(events, formBuilder, translationProvider, componentSettingsProvider, modalCtrl) {
        this.events = events;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.modalCtrl = modalCtrl;
        this.translationProvider.bind(this);
    }
    UserSecurityDataFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.componentSettingsProvider.getSecurityQuestions(this.mocked).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.securityQuestions = mapfreResponse.data;
                _this.initializeForm();
            }
        });
    };
    UserSecurityDataFormComponent.prototype.ngOnDestroy = function () {
        if (this.valueChangesSubscription) {
            this.valueChangesSubscription.unsubscribe();
        }
    };
    UserSecurityDataFormComponent.prototype.openPrivacyModal = function () {
        var _this = this;
        this.componentSettingsProvider.getInformativeMessage(this.mocked, 'PRIVACITY').subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                var data = mapfreResponse.data[0];
                _this.modalCtrl.create(HtmlContentModalComponent, { title: data.title, description: data.description }).present();
            }
        });
    };
    UserSecurityDataFormComponent.prototype.openConditionsModal = function () {
        var _this = this;
        this.componentSettingsProvider.getInformativeMessage(this.mocked, 'TERMS_OF_USE').subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                var data = mapfreResponse.data[0];
                _this.modalCtrl.create(HtmlContentModalComponent, { title: data.title, description: data.description }).present();
            }
        });
    };
    UserSecurityDataFormComponent.prototype.validateIfPasswordsMatch = function () {
        this.passwordsMismatch = !this.doPasswordsMatch();
    };
    UserSecurityDataFormComponent.prototype.doPasswordsMatch = function () {
        var password = this.registrationForm.value.password;
        var repeatPassword = this.registrationForm.value.repeat_password;
        return !(_.size(password) && _.size(repeatPassword)) || password === repeatPassword;
    };
    UserSecurityDataFormComponent.prototype.initializeForm = function () {
        var _this = this;
        this.registrationForm = this.formBuilder.group({
            password: [undefined, Validators.required],
            repeat_password: [undefined, Validators.required],
            first_security_question: ['', Validators.required],
            first_security_question_answer: ['', Validators.required],
            second_security_question: ['', Validators.required],
            second_security_question_answer: ['', Validators.required],
            conditionsAndPoliciesAccepted: [false, Validators.required]
        });
        if (this.userSecurityData) {
            FormUtil.mapFormData(this.userSecurityData, this.registrationForm);
            this.publishFormData();
        }
        FormUtil.mapListItem(this.securityQuestions, 'first_security_question', this.registrationForm);
        FormUtil.mapListItem(this.securityQuestions, 'second_security_question', this.registrationForm);
        this.valueChangesSubscription = this.registrationForm.valueChanges.subscribe(function () {
            _this.publishFormData();
        });
    };
    UserSecurityDataFormComponent.prototype.publishFormData = function () {
        this.events.publish(UserSecurityDataFormComponent.updatedEvent, this.registrationForm.value);
        this.updateCompleteness();
    };
    UserSecurityDataFormComponent.prototype.updateCompleteness = function () {
        this.events.publish(UserSecurityDataFormComponent.completedEvent, this.registrationForm.valid &&
            this.registrationForm.value.conditionsAndPoliciesAccepted === true &&
            this.doPasswordsMatch());
    };
    UserSecurityDataFormComponent.componentName = "UserSecurityDataFormComponent";
    UserSecurityDataFormComponent.updatedEvent = UserSecurityDataFormComponent.componentName + ":updated";
    UserSecurityDataFormComponent.completedEvent = UserSecurityDataFormComponent.componentName + ":completed";
    UserSecurityDataFormComponent.inputs = ['userSecurityData'];
    UserSecurityDataFormComponent.configInputs = ['mocked', 'styles'];
    UserSecurityDataFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'user-security-data-form',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">   <loading-spinner *ngIf=\"!registrationForm\"></loading-spinner>    <div *ngIf=\"registrationForm\">     <ion-card-header class=\"no-padding-bottom\">       <ion-item>         <h2 [class.margin-bottom-xs]=\"onMobile\">           {{ translate('user-security-data-form.new_account.user-security') }}         </h2>          <span class=\"secondary-text\" [attr.item-end]=\"onMobile ? null : ''\">           {{ translate('user-security-data-form.generic.required_fields') }}         </span>       </ion-item>     </ion-card-header>      <ion-card-content>       <form [formGroup]=\"registrationForm\">         <ion-grid class=\"no-padding\">           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-security-data-form.new_account.password') }}                 </ion-label>                  <ion-input                   type=\"password\" formControlName=\"password\"                   (ionBlur)=\"validateIfPasswordsMatch()\" maxlength=\"50\">                 </ion-input>               </ion-item>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-security-data-form.new_account.repeat_password') }}                 </ion-label>                  <ion-input                   type=\"password\" formControlName=\"repeat_password\"                   (ionBlur)=\"validateIfPasswordsMatch()\" maxlength=\"50\">                 </ion-input>               </ion-item>              </ion-col>             <ion-col col-12 *ngIf=\"passwordsMismatch\" class=\"input-value-error\">                <span>                 {{ translate('user-security-data-form.new_account.passwords_mismatch') }}               </span>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-security-data-form.new_account.first_security_question') }}                 </ion-label>                  <ion-select formControlName=\"first_security_question\" class=\"form-select\" full>                   <ion-option *ngFor=\"let securityQuestion of securityQuestions\" [value]=\"securityQuestion\">                     {{ securityQuestion.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-security-data-form.new_account.first_security_question_answer') }}                 </ion-label>                  <ion-input                   formControlName=\"first_security_question_answer\"                   maxlength=\"50\">                 </ion-input>               </ion-item>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-security-data-form.new_account.second_security_question') }}                 </ion-label>                  <ion-select formControlName=\"second_security_question\" class=\"form-select\" full>                   <ion-option *ngFor=\"let securityQuestion of securityQuestions\" [value]=\"securityQuestion\">                     {{ securityQuestion.title }}                   </ion-option>                 </ion-select>               </ion-item>              </ion-col>             <ion-col col-12 col-md-6>                <ion-item no-padding class=\"required\">                 <ion-label floating>                   {{ translate('user-security-data-form.new_account.second_security_question_answer') }}                 </ion-label>                  <ion-input                   formControlName=\"second_security_question_answer\"                   maxlength=\"50\">                 </ion-input>               </ion-item>              </ion-col>           </ion-row>           <ion-row>             <ion-col col-12>                <div class=\"flex margin-top-m\">                 <ion-checkbox type=\"checkbox\" formControlName=\"conditionsAndPoliciesAccepted\"></ion-checkbox>                  <span class=\"margin-left-s\">                   {{ translate('user-security-data-form.new_account.privacy_check1') }}                   <a (click)=\"openPrivacyModal()\" class=\"clickable\" [title]=\"translate('user-security-data-form.new_account.privacy_check2')\">                     {{ translate('user-security-data-form.new_account.privacy_check2') }}                   </a>                   {{ translate('user-security-data-form.new_account.privacy_check3') }}                   <a (click)=\"openConditionsModal()\" onClick=\"return false;\" class=\"clickable\" [title]=\"translate('user-security-data-form.new_account.privacy_check4')\">                     {{ translate('user-security-data-form.new_account.privacy_check4') }}                   </a>                   {{ translate('user-security-data-form.new_account.privacy_check5') }}                 </span>               </div>              </ion-col>           </ion-row>         </ion-grid>        </form>     </ion-card-content>   </div> </ion-card>"
                },] },
    ];
    UserSecurityDataFormComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
        { type: ModalController, },
    ]; };
    UserSecurityDataFormComponent.propDecorators = {
        'userSecurityData': [{ type: Input },],
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return UserSecurityDataFormComponent;
}());
export { UserSecurityDataFormComponent };
//# sourceMappingURL=user-security-data-form.js.map