import { NavController, Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
export declare class ConfirmIncidenceCardComponent {
    private navCtrl;
    private translationProvider;
    private events;
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    constructor(navCtrl: NavController, translationProvider: TranslationProvider, events: Events);
    goToGlobalPosition(): any;
}
