import { OnInit } from '@angular/core';
import { Modal, ModalController } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { ApiProvider } from '../../providers/api/api';
export declare class CookiesAdviceComponent implements OnInit {
    private translationProvider;
    private apiProvider;
    private componentSettingsProvider;
    private modalCtrl;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    showCookiesAdvice: boolean;
    componentReady: boolean;
    componentSettings: Object;
    modal: Modal;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, apiProvider: ApiProvider, componentSettingsProvider: ComponentSettingsProvider, modalCtrl: ModalController);
    ngOnInit(): void;
    close(): void;
    openPrivacyModal(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
