import { EventEmitter, OnInit, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
export interface ImprovementStep {
    amount?: string;
    status?: string;
}
export declare class CoverageImprovementSelectorComponent implements OnInit, OnChanges {
    hideScrollButtons: Boolean;
    selectedStep: ImprovementStep;
    selectedStepIndex: number;
    steps: ImprovementStep[];
    private stepsContainer;
    orientation: string;
    mClick: EventEmitter<Event>;
    amounts: any[];
    height: string;
    constructor(overflowContainer: ElementRef, stepsContainer: ElementRef);
    ngOnChanges(changes: SimpleChanges): void;
    protected doBulletClick(event: Event, index: number): void;
    nextStep(): void;
    previousStep(): void;
    setCompleted(index: number): void;
    ngOnInit(): void;
}
