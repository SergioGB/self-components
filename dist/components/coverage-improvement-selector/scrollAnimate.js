export function scrollAnimate(_a) {
    var timing = _a.timing, draw = _a.draw, duration = _a.duration;
    var start = performance.now();
    requestAnimationFrame(function scrollAnimate(time) {
        var timeFraction = (time - start) / duration;
        if (timeFraction > 1) {
            timeFraction = 1;
        }
        var progress = timing(timeFraction);
        draw(progress);
        if (timeFraction < 1) {
            requestAnimationFrame(scrollAnimate);
        }
    });
}
//# sourceMappingURL=scrollAnimate.js.map