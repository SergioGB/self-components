import { OnInit } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { InsuranceProvider } from '../../providers/insurance/insurance';
export declare class CoverageListComponent implements OnInit {
    private navCtrl;
    private insuranceProvider;
    private translationProvider;
    private events;
    mocked?: boolean;
    styles?: any;
    riskId?: any;
    policyId: string;
    coveragesReady: boolean;
    improvementAvailable: boolean;
    coverageList: any;
    static inputs: string[];
    static configInputs: string[];
    constructor(navCtrl: NavController, insuranceProvider: InsuranceProvider, translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    goToPolicyUpgrade(): void;
    private queryCoverages;
}
