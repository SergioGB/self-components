import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { InsuranceProvider } from '../../providers/insurance/insurance';
var CoverageListComponent = (function () {
    function CoverageListComponent(navCtrl, insuranceProvider, translationProvider, events) {
        this.navCtrl = navCtrl;
        this.insuranceProvider = insuranceProvider;
        this.translationProvider = translationProvider;
        this.events = events;
        this.coverageList = [];
        this.translationProvider.bind(this);
    }
    CoverageListComponent.prototype.ngOnInit = function () {
        if (this.policyId) {
            this.queryCoverages();
        }
    };
    CoverageListComponent.prototype.goToPolicyUpgrade = function () {
        this.events.publish('coverage-list::goUpgradeInsurance', { stepId: 1, policyId: this.policyId });
    };
    CoverageListComponent.prototype.queryCoverages = function () {
        var _this = this;
        this.insuranceProvider.getCoverageSettings(this.mocked, this.policyId, this.riskId).subscribe(function (response) {
            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
            if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.coverageList = mapfreResponse.data;
                _this.coveragesReady = true;
            }
        });
        this.insuranceProvider.getPolicyDetailSettings(this.mocked, this.policyId).subscribe(function (response) {
            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
            if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.improvementAvailable = mapfreResponse.data.improvement_allowed;
            }
        });
    };
    CoverageListComponent.inputs = ['riskId', 'policyId'];
    CoverageListComponent.configInputs = ['mocked', 'styles'];
    CoverageListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'coverage-list',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["coverage-list .coverages-detail {   display: flex; }   coverage-list .coverages-detail ion-icon {     padding-top: 12px; }  coverage-list .minimum-width {   min-width: 200px;   margin: auto; }"],
                    template: "<ion-card *ngIf=\"!coveragesReady || coverageList && coverageList.length\" [ngStyle]=\"styles\">   <loading-spinner *ngIf=\"!coveragesReady\"></loading-spinner>    <div *ngIf=\"coveragesReady\">     <ion-card-header class=\"no-padding-bottom\">       <ion-item>         <h3>           {{ translate('policy.coverage-list.coverage_list.title') }}         </h3>       </ion-item>     </ion-card-header>      <ion-card-content class=\"no-padding-bottom\">       <ion-item no-padding *ngFor=\"let coverage of coverageList\" class=\"bottom-border border-only last-border-too\">         <ion-row class=\"coverages-detail\">           <ion-icon [name]=\"coverage.icon?.name\" item-start large [style.color]=\"coverage.icon?.hex_color\"></ion-icon>            <ion-col>             <h3 class=\"fmm-primary-text\">               {{ coverage.title }}             </h3>                <p class=\"fmm-primary-text fmm-regular\">               {{ coverage.description }}             </p>                <a *ngFor=\"let entry of coverage.associated_actions\" [href]=\"entry.action_link\"               class=\"inline-block margin-top-s\" text-uppercase title=\"{{ entry.action_name }}\">               {{ entry.action_name }}             </a>           </ion-col>         </ion-row>       </ion-item>     </ion-card-content>      <ion-card-content *ngIf=\"improvementAvailable\" class=\"actions-wrapper\">       <ion-grid class=\"values-list no-padding\">         <ion-row>           <ion-col col-12 col-sm-4 col-xl-2 no-padding >              <button class=\"secondary\" ion-button full text-uppercase (click)=\"goToPolicyUpgrade()\">               {{ translate('policy.coverage-list.coverage_list.upgrade_insurance_button') }}             </button>            </ion-col>         </ion-row>       </ion-grid>     </ion-card-content>   </div>  </ion-card>"
                },] },
    ];
    CoverageListComponent.ctorParameters = function () { return [
        { type: NavController, },
        { type: InsuranceProvider, },
        { type: TranslationProvider, },
        { type: Events, },
    ]; };
    CoverageListComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'policyId': [{ type: Input },],
    };
    return CoverageListComponent;
}());
export { CoverageListComponent };
//# sourceMappingURL=coverage-list.js.map