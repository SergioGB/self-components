import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { Coverage } from '../../models/upgrade-insurance/coverage';
export declare class CoverageNotIncludedComponent implements OnInit {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    notIncludedCoverages: Coverage[];
    defaultIcons: string[];
    componentReady: boolean;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
