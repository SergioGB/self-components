import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events, ModalController } from 'ionic-angular';
import { Mocks } from '../../providers/mocks/mocks';
import { StepsProvider } from '../../providers/steps/steps';
import { InformationModalComponent } from '../_modals/information-modal/information-modal';
import { TranslationProvider } from '../../providers/translation/translation';
import { Utils } from '../../providers/utils/utils';
var DesktopStepsComponent = (function () {
    function DesktopStepsComponent(events, modalController, translationProvider, stepsProvider) {
        this.events = events;
        this.modalController = modalController;
        this.translationProvider = translationProvider;
        this.stepsProvider = stepsProvider;
        this.defaultIcons = ["mapfre-no-check", "mapfre-full-circle"];
    }
    DesktopStepsComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.steps = Mocks.getSteps();
            this.currentStep = this.steps[0];
        }
        this.updateSteps();
    };
    DesktopStepsComponent.prototype.ngOnChanges = function (changes) {
        if (changes.steps || changes.currentStepId) {
            this.updateSteps();
        }
    };
    DesktopStepsComponent.prototype.updateSteps = function () {
        this.stepsProvider.updateSteps(this.steps, this.currentStepId);
    };
    DesktopStepsComponent.prototype.onStepSelected = function (newStep) {
        if (newStep && newStep.completed && !newStep.active) {
            this.events.publish(DesktopStepsComponent.selectionEvent, newStep);
        }
    };
    DesktopStepsComponent.prototype.presentConfirmationModal = function () {
        return this.modalController.create(InformationModalComponent, {
            type: 4,
            icon: 'mapfre-warning',
            title: this.translationProvider.getValueForKey('modal.desktop-steps.information-modal.generic.unsaved_data_will_be_lost'),
            subtitle: this.translationProvider.getValueForKey('modal.desktop-steps.information-modal.generic.continue_question'),
            rightButton: this.translationProvider.getValueForKey('modal.desktop-steps.information-modal.generic.continue'),
            labels: {
                cancel: "modal.desktop-steps.information-modal.generic.cancel",
                finish: "modal.desktop-steps.information-modal.generic.finish",
                accept: "modal.desktop-steps.information-modal.generic.accept"
            }
        });
    };
    DesktopStepsComponent.prototype.hasMultipleValues = function (object) {
        return Utils.isArray(object);
    };
    DesktopStepsComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    DesktopStepsComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    DesktopStepsComponent.componentName = "DesktopStepsComponent";
    DesktopStepsComponent.selectionEvent = DesktopStepsComponent.componentName + ":selection";
    DesktopStepsComponent.configInputs = ['mocked', 'icons', 'styles'];
    DesktopStepsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'desktop-steps',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card class=\"desktop-steps no-margin-bottom\" [ngStyle]=\"styles\">        <loading-spinner *ngIf=\"!(steps && steps.length)\"></loading-spinner>        <ion-item *ngFor=\"let step of steps | discardByKeyValue: 'hidden':true; let index = index;\"               [class.visitable]=\"step.visitable\"               [class.clickable]=\"step.clickable\"               [class.completed]=\"step.completed\"               [class.active]=\"step.active\"               tabindex=\"0\"               (click)=\"onStepSelected(step)\">          <ion-icon [name]=\"step.active ? getIcon(0) : getIcon(1)\" item-start extra-large>         <span class=\"font-s\">           {{ index + 1 }}         </span>       </ion-icon>         <h4 *ngIf=\"step.title\" class=\"title\">         {{ step.title }}       </h4>          <div *ngIf=\"step.subtitle\" class=\"subtitle margin-top-xs\">         <div *ngIf=\"hasMultipleValues(step.subtitle)\">           <p *ngFor=\"let subtitleLine of step.subtitle\">             {{ subtitleLine }}           </p>         </div>            <div *ngIf=\"!hasMultipleValues(step.subtitle)\">           <p>             {{ step.subtitle }}           </p>       </div>       </div>  </ion-item>    </ion-card>   "
                },] },
    ];
    DesktopStepsComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: ModalController, },
        { type: TranslationProvider, },
        { type: StepsProvider, },
    ]; };
    DesktopStepsComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'steps': [{ type: Input },],
        'currentStep': [{ type: Input },],
        'currentStepId': [{ type: Input },],
    };
    return DesktopStepsComponent;
}());
export { DesktopStepsComponent };
//# sourceMappingURL=desktop-steps.js.map