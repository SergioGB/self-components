import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
export declare class IconsDisplayComponent {
    private translationProvider;
    private componentSettingsProvider;
    iconNames: string[];
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider);
}
