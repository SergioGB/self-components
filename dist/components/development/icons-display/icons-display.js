import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
var IconsDisplayComponent = (function () {
    function IconsDisplayComponent(translationProvider, componentSettingsProvider) {
        var _this = this;
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.translationProvider.bind(this);
        this.componentSettingsProvider.getIconsNames().subscribe(function (iconNames) {
            _this.iconNames = iconNames;
        });
    }
    IconsDisplayComponent.configInputs = ['mocked', 'styles'];
    IconsDisplayComponent.decorators = [
        { type: Component, args: [{
                    selector: 'icons-display',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">    <ion-card-header>     <ion-item>       <h3>         {{ translate('icons-display.icon_display.title') }}       </h3>     </ion-item>   </ion-card-header>    <ion-card-content>     <ion-grid>       <ion-row>         <ion-col *ngFor=\"let iconName of iconNames\" col-12 col-sm-4 col-md-3 col-lg-2 col-xl-3 text-center>           <ion-icon [name]=\"iconName\"></ion-icon>           <div>             {{ iconName }}           </div>         </ion-col>       </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    IconsDisplayComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
    ]; };
    IconsDisplayComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return IconsDisplayComponent;
}());
export { IconsDisplayComponent };
//# sourceMappingURL=icons-display.js.map