import { AppAvailability } from '@ionic-native/app-availability';
import { Platform } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
export declare class DownloadAppNotificationComponent {
    private appAvailability;
    private platform;
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    showDownloadApp: boolean;
    appAvailable: boolean;
    constructor(appAvailability: AppAvailability, platform: Platform, translationProvider: TranslationProvider);
    close(): void;
    openApp(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
