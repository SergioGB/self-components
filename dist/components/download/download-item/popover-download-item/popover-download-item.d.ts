import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { NavParams, ViewController } from 'ionic-angular';
import { DocumentsProvider } from '../../../../providers/documents/documents';
import { MapfreFile } from '../../../../models/mapfreFile';
export declare class PopoverDownloadItemComponent implements OnInit {
    private translationProvider;
    private documentsProvider;
    private navParams;
    private viewCtrl;
    file: MapfreFile;
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, documentsProvider: DocumentsProvider, navParams: NavParams, viewCtrl: ViewController);
    ngOnInit(): void;
    download(): void;
}
