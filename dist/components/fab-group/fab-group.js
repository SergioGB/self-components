import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import * as _ from 'lodash';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { SubmitIncidenceProvider } from '../../providers/submit-incidence/submit-incidence';
import { NavigationProvider } from '../../providers/navigation/navigation';
import { TranslationProvider } from '../../providers/translation/translation';
import { ModalProvider } from '../../providers/modal/modal';
import { PlatformProvider } from '../../providers/platform/platform';
import { Fab } from '../../models/fab';
import { CallMeModalComponent } from '../_modals/_contact/call-me-modal/call-me-modal';
var FabGroupComponent = (function () {
    function FabGroupComponent(componentSettingsProvider, navContrl, translationProvider, modalController, platformProvider, modalProvider, submitIncidenceProvider, navigationProvider) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.navContrl = navContrl;
        this.translationProvider = translationProvider;
        this.modalController = modalController;
        this.platformProvider = platformProvider;
        this.modalProvider = modalProvider;
        this.submitIncidenceProvider = submitIncidenceProvider;
        this.navigationProvider = navigationProvider;
        this.translationProvider.bind(this);
        this.platformProvider.bindTo(this);
    }
    FabGroupComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.componentSettingsProvider.getHelpMethods(this.mocked).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.setupLinks(mapfreResponse.data.help_methods);
            }
            _this.ready = true;
        });
    };
    FabGroupComponent.prototype.setupLinks = function (links) {
        if (links) {
            var fabLinks = [];
            for (var i = 0; i < links.length; i++) {
                var fab_1 = new Fab();
                fab_1.link = "access_" + i;
                fab_1.name = fab_1.link;
                fab_1.text = links[i].title;
                fab_1.icon = links[i].icon.name;
                fab_1.page_ref = links[i].page_ref;
                fab_1.link_data = links[i].link_data;
                fabLinks.push(fab_1);
            }
            var fab = new Fab();
            fab.link = "access_" + fabLinks.length;
            fab.name = fab.link;
            fab.text = this.translationProvider.getValueForKey('fab-group.help_method.call_me');
            fab.icon = 'mapfre-incoming-call';
            fab.page_ref = 'modalCallMe';
            fab.link_data = undefined;
            fabLinks.push(fab);
            if (!this.platformProvider.onDesktop()) {
                fabLinks = fabLinks.reverse();
            }
            this.fabLinks = fabLinks;
        }
    };
    FabGroupComponent.prototype.goToFab = function (event) {
        var _this = this;
        if (event['mProperty']) {
            var selectedFab = event['mProperty'];
            for (var _i = 0, _a = this.fabLinks; _i < _a.length; _i++) {
                var fab = _a[_i];
                if (_.isEqual(fab.link, selectedFab)) {
                    if (fab.page_ref === 'modalCallMe') {
                        this.modal = this.modalController.create(CallMeModalComponent, { mocked: this.mocked,
                            labels: {
                                title: "modal.fab-group.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.fab-group.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.fab-group.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.fab-group.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.fab-group.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.fab-group.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.fab-group.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.fab-group.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.fab-group.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.fab-group.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.fab-group.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.fab-group.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.fab-group.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.fab-group.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.fab-group.call-me-modal.call_me_innactivity_modal.save_button",
                            } }, {});
                        this.modalProvider.setHelpModalProvider(true);
                        this.modal.onDidDismiss(function () {
                            _this.modalProvider.setHelpModalProvider(false);
                        });
                        this.modal.present();
                    }
                    else {
                        this.navigationProvider.navigateTo(this.navContrl, fab.page_ref, fab.link_data);
                    }
                }
            }
        }
    };
    FabGroupComponent.configInputs = ['mocked', 'styles', 'modals'];
    FabGroupComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fab-group',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["fab-group mapfre-fab ion-fab {   bottom: 60px !important; }  fab-group button {   padding: 0 !important; }   fab-group button > span {     border-radius: 50%;     display: flex !important; }"],
                    template: "<div showWhen=\"mobile,tablet\" [ngStyle]=\"styles\">   <mapfre-fab     [subFabs]=\"fabLinks\"     [name]=\"'fabPrincipal'\"     [icon]=\"'mapfre-earphones'\"     [side]=\"'top'\"     [positionLabel]=\"true\"     [right]=\"true\"     [top]=\"false\"     [disabled]=\"false\"     (mClick)=\"goToFab($event)\">   </mapfre-fab> </div>"
                },] },
    ];
    FabGroupComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: NavController, },
        { type: TranslationProvider, },
        { type: ModalController, },
        { type: PlatformProvider, },
        { type: ModalProvider, },
        { type: SubmitIncidenceProvider, },
        { type: NavigationProvider, },
    ]; };
    FabGroupComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'modals': [{ type: Input },],
    };
    return FabGroupComponent;
}());
export { FabGroupComponent };
//# sourceMappingURL=fab-group.js.map