import { AfterViewInit, ElementRef, OnInit } from '@angular/core';
import { AnimationsProvider } from '../../../providers/animations/animations';
export declare class CardFaqsComponent implements OnInit, AfterViewInit {
    private elementRef;
    private animationsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    topic_name: string;
    faqs: any;
    defaultIcons: string[];
    private uniqueId;
    private type;
    private ready;
    static configInputs: string[];
    constructor(elementRef: ElementRef, animationsProvider: AnimationsProvider);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    verticalSlideToggle(faq: any, transitionDuration?: number): void;
    storeHeights(faq: any): void;
    getProperHeight(faq: any): string;
    getFaqContentId(faq: any): string;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
