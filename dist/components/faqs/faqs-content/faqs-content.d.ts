import { OnDestroy, OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
export declare class FaqsContentComponent implements OnInit, OnDestroy {
    private translationProvider;
    private componentSettingsProvider;
    private events;
    mocked?: boolean;
    styles?: any;
    ready: boolean;
    topics: any[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider, events: Events);
    ngOnInit(): void;
    private loadTopics;
    ngOnDestroy(): void;
    selectTopic(selectedTopic: any): void;
}
