import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { FormatProvider } from '../../../providers/format/format';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { ContentTab } from '../../../models/content-tab';
export declare class FaqsHeaderComponent implements OnInit {
    private translationProvider;
    private formatProvider;
    private componentSettingsProvider;
    mocked?: boolean;
    styles?: any;
    ready: boolean;
    headerTabs: ContentTab[];
    headerData: any;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, formatProvider: FormatProvider, componentSettingsProvider: ComponentSettingsProvider);
    ngOnInit(): void;
    private generateTabsFromTopics;
}
