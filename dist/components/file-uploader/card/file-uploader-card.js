import { ViewEncapsulation } from '@angular/core';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Events } from 'ionic-angular';
import { FileUploaderComponent } from '../file-uploader';
import { ApiProvider } from '../../../providers/api/api';
var FileUploaderCardComponent = (function () {
    function FileUploaderCardComponent(events, apiProvider) {
        this.events = events;
        this.apiProvider = apiProvider;
        this.onUpload = new EventEmitter();
        this.onDownload = new EventEmitter();
        this.onRemove = new EventEmitter();
    }
    FileUploaderCardComponent.prototype.ngOnInit = function () {
        this.unsubscribeEvents();
        this.subscribeEvents();
        if (this.mocked) {
            this.title = { es: 'Sube tus documentos' };
            this.description = { es: 'Necesitamos que nos adjuntes los siguientes documentos:' };
            this.requiredFiles = { es: 'DNI, Libro de familia' };
        }
        this.ready = true;
    };
    FileUploaderCardComponent.prototype.ngOnDestroy = function () {
        this.unsubscribeEvents();
    };
    FileUploaderCardComponent.prototype.subscribeEvents = function () {
        var _this = this;
        this.events.subscribe(FileUploaderComponent.completedEvent, function (completed) {
            _this.events.publish(FileUploaderCardComponent.completedEvent, completed);
        });
    };
    FileUploaderCardComponent.prototype.unsubscribeEvents = function () {
        this.events.unsubscribe(FileUploaderComponent.completedEvent);
    };
    FileUploaderCardComponent.prototype.onChildUpload = function (mapfreFile) {
        this.onUpload.emit(mapfreFile);
    };
    FileUploaderCardComponent.prototype.onChildDownload = function (mapfreFile) {
        this.onDownload.emit(mapfreFile);
    };
    FileUploaderCardComponent.prototype.onChildRemoval = function (mapfreFile) {
        this.onRemove.emit(mapfreFile);
    };
    FileUploaderCardComponent.prototype.getTitle = function () {
        var language = this.apiProvider.getLanguage();
        return this.title[language] ? this.title[language] : undefined;
    };
    FileUploaderCardComponent.componentName = "FileUploaderCardComponent";
    FileUploaderCardComponent.inputs = ['files'];
    FileUploaderCardComponent.configInputs = ['mocked', 'styles', 'title', 'description', 'requiredFiles', 'allowedTypes', 'multiple'];
    FileUploaderCardComponent.completedEvent = FileUploaderCardComponent.componentName + ":completed";
    FileUploaderCardComponent.decorators = [
        { type: Component, args: [{
                    selector: 'file-uploader-card',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"ready\" [ngStyle]=\"styles\">   <ion-card-header *ngIf=\"getTitle()\">      <ion-item>       <h3>         {{ getTitle() }}       </h3>     </ion-item>    </ion-card-header>   <ion-card-content>      <file-uploader       [description]=\"description\"       [requiredFiles]=\"requiredFiles\"       [allowedTypes]=\"allowedTypes\"       [files]=\"files\"       [multiple]=\"multiple\"       [mocked]=\"mocked\"       (onUpload)=\"onChildUpload($event)\"       (onDownload)=\"onChildDownload($event)\"       (onRemoval)=\"onChildRemoval($event)\">     </file-uploader>    </ion-card-content> </ion-card>"
                },] },
    ];
    FileUploaderCardComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: ApiProvider, },
    ]; };
    FileUploaderCardComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'title': [{ type: Input },],
        'description': [{ type: Input },],
        'requiredFiles': [{ type: Input },],
        'allowedTypes': [{ type: Input },],
        'files': [{ type: Input },],
        'multiple': [{ type: Input },],
        'onUpload': [{ type: Output },],
        'onDownload': [{ type: Output },],
        'onRemove': [{ type: Output },],
    };
    return FileUploaderCardComponent;
}());
export { FileUploaderCardComponent };
//# sourceMappingURL=file-uploader-card.js.map