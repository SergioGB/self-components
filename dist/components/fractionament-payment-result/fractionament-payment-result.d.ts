import { ReceiptFractionamentProvider } from '../../providers/receipt-fractionament/receipt-fractionament';
import { TranslationProvider } from '../../providers/translation/translation';
import { PotentialClientProvider } from '../../providers/potential-client/potential-client';
export declare class FractionamentPaymentResultComponent {
    private receiptFractionamentProvider;
    private translationProvider;
    private potentialClientProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    paymentMethod: any;
    static configInputs: string[];
    result: any;
    constructor(receiptFractionamentProvider: ReceiptFractionamentProvider, translationProvider: TranslationProvider, potentialClientProvider: PotentialClientProvider);
    ngOnInit(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
