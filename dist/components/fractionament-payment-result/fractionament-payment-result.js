import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ReceiptFractionamentProvider } from '../../providers/receipt-fractionament/receipt-fractionament';
import { TranslationProvider } from '../../providers/translation/translation';
import { PotentialClientProvider } from '../../providers/potential-client/potential-client';
var FractionamentPaymentResultComponent = (function () {
    function FractionamentPaymentResultComponent(receiptFractionamentProvider, translationProvider, potentialClientProvider) {
        this.receiptFractionamentProvider = receiptFractionamentProvider;
        this.translationProvider = translationProvider;
        this.potentialClientProvider = potentialClientProvider;
        this.defaultIcons = ["mapfre-money-bag"];
        this.translationProvider.bind(this);
    }
    FractionamentPaymentResultComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.result = {
                paymentDate: '12/06/2018',
                price: {
                    amount: '500',
                    iso_code: 'EUR'
                },
                paymentMethod: {
                    type: 1,
                    credit_card_info: {
                        card_number: "12341234123412341234"
                    }
                }
            };
        }
        else {
            if (this.potentialClientProvider.getPaymentMethodSelectedData()) {
                this.paymentMethod = this.potentialClientProvider.getPaymentMethodSelectedData();
                if (this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.type === 1) {
                    this.result = {
                        paymentDate: "12/01/2019",
                        price: {
                            iso_code: "EUR",
                            amount: 100
                        },
                        paymentMethod: {
                            type: this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.type,
                            credit_card_info: {
                                card_number: this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.credit_card_info.card_number
                            }
                        }
                    };
                }
                else if (this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.type === 2 && this.potentialClientProvider.getPaymentMethodSelectedData().bankFormInfo) {
                    this.result = {
                        paymentDate: "12/01/2019",
                        price: {
                            iso_code: "EUR",
                            amount: 100
                        },
                        paymentMethod: {
                            type: this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.type,
                            bank_account_info: {
                                account_number: this.potentialClientProvider.getPaymentMethodSelectedData().bankFormInfo.iban ? this.potentialClientProvider.getPaymentMethodSelectedData().bankFormInfo.iban : this.potentialClientProvider.getPaymentMethodSelectedData().bankFormInfo.account_number
                            }
                        }
                    };
                }
                else if (this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.type === 2 && this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.bank_account_info) {
                    this.result = {
                        paymentDate: "12/01/2019",
                        price: {
                            iso_code: "EUR",
                            amount: 100
                        },
                        paymentMethod: {
                            type: this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.type,
                            bank_account_info: {
                                account_number: this.potentialClientProvider.getPaymentMethodSelectedData().selectedPaymentMethod.bank_account_info.account_number
                            }
                        }
                    };
                }
            }
        }
    };
    FractionamentPaymentResultComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    FractionamentPaymentResultComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    FractionamentPaymentResultComponent.configInputs = ['mocked', 'icons', 'styles'];
    FractionamentPaymentResultComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fractionament-payment-result',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"result\" class=\"no-horizontal-margin\" [ngStyle]=\"styles\">   <ion-card-content>      <ion-item tabindex=\"0\" no-padding class=\"pay-item\">        <ion-icon [name]=\"getIcon(0)\" item-start extra-large></ion-icon>        <h4>         {{ translate('fractionament-payment-result.fractionament.success.payment.title') }}       </h4>        <p class=\"fmm-body1 fmm-regular\">         <span>           {{ result.paymentDate }}         </span>       </p>        <div class=\"padding-top-s\">         <ion-row>           <ion-col col-sm-3 class=\"no-padding-left no-padding-right\">             <strong class=\"primary-text\">               {{ translate('fractionament-payment-result.fractionament.success.payment.amount') }}             </strong>           </ion-col>           <ion-col col-sm-9>             <span class=\"secondary-text\">               {{ result.price.amount | currency:result.price.iso_code:true:'1.2-2' }}             </span>           </ion-col>         </ion-row>         <ion-row>           <ion-col col-sm-3 class=\"no-padding-left no-padding-right\">             <strong class=\"primary-text\">               {{ translate('fractionament-payment-result.fractionament.success.payment.payment_method') }}             </strong>           </ion-col>           <ion-col col-sm-9>             <span class=\"secondary-text\" *ngIf=\"result.paymentMethod.type === 1\">               {{translate('fractionament-payment-result.receipt_payment_page.method_selection.credit_card')}}             </span>             <span class=\"secondary-text\" *ngIf=\"result.paymentMethod.type === 2\">                {{translate('fractionament-payment-result.receipt_payment_page.method_selection.bank_dom') }}             </span>             <span class=\"secondary-text\" *ngIf=\"result.paymentMethod.type === 3\">               {{translate('fractionament-payment-result.receipt_payment_page.method_selection.bar_code') }}             </span>           </ion-col>         </ion-row>         <ion-row>           <ion-col col-sm-3 class=\"no-padding-left no-padding-right\">             <strong class=\"primary-text\" *ngIf=\"result.paymentMethod.type === 1\">               {{translate('fractionament-payment-result.fractionament.success.payment.card_number')}}             </strong>             <strong class=\"primary-text\" *ngIf=\"result.paymentMethod.type === 2\">               {{translate('fractionament-payment-result.fractionament.success.payment.bank_account')}}             </strong>            </ion-col>           <ion-col col-sm-9>             <span class=\"secondary-text\" *ngIf=\"result.paymentMethod.type === 1\">               {{result.paymentMethod.credit_card_info.card_number}}             </span>             <span class=\"secondary-text\" *ngIf=\"result.paymentMethod.type === 2\">               {{result.paymentMethod.bank_account_info.account_number}}             </span>           </ion-col>         </ion-row>       </div>      </ion-item>    </ion-card-content> </ion-card>"
                },] },
    ];
    FractionamentPaymentResultComponent.ctorParameters = function () { return [
        { type: ReceiptFractionamentProvider, },
        { type: TranslationProvider, },
        { type: PotentialClientProvider, },
    ]; };
    FractionamentPaymentResultComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'result': [{ type: Input },],
    };
    return FractionamentPaymentResultComponent;
}());
export { FractionamentPaymentResultComponent };
//# sourceMappingURL=fractionament-payment-result.js.map