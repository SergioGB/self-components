import { EventEmitter, OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
export declare class FuzzyDateSelectorComponent implements OnInit {
    private translationProvider;
    private platformProvider;
    private events;
    static componentName: string;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    selectedDate?: any;
    selectedMode?: string;
    exactDate?: Date;
    startDate?: Date;
    endDate?: Date;
    onSelection: EventEmitter<any>;
    defaultIcons: string[];
    exactInputMode: string;
    fuzzyInputMode: string;
    componentReady: boolean;
    static selectedEvent: string;
    static completedEvent: string;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, platformProvider: PlatformProvider, events: Events);
    ngOnInit(): void;
    publishSelection(): void;
    selectInputMode(inputType: string): void;
    private setDefaults;
    private getSelection;
    private updateCompleteness;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
