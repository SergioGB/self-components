import { ViewEncapsulation } from '@angular/core';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
var FuzzyDateSelectorComponent = (function () {
    function FuzzyDateSelectorComponent(translationProvider, platformProvider, events) {
        this.translationProvider = translationProvider;
        this.platformProvider = platformProvider;
        this.events = events;
        this.onSelection = new EventEmitter();
        this.defaultIcons = ["mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check"];
        this.exactInputMode = 'exact';
        this.fuzzyInputMode = 'fuzzy';
        this.translationProvider.bind(this);
        this.platformProvider.bindTo(this);
    }
    FuzzyDateSelectorComponent.prototype.ngOnInit = function () {
        if (this.selectedDate) {
            this.selectedMode = this.selectedDate.selectedMode;
            this.exactDate = this.selectedDate.exactDate;
            this.startDate = this.selectedDate.startDate;
            this.endDate = this.selectedDate.endDate;
        }
        this.setDefaults();
        this.publishSelection();
        this.componentReady = true;
    };
    FuzzyDateSelectorComponent.prototype.publishSelection = function () {
        this.events.publish(FuzzyDateSelectorComponent.selectedEvent, this.getSelection());
        this.updateCompleteness();
    };
    FuzzyDateSelectorComponent.prototype.selectInputMode = function (inputType) {
        this.selectedMode = inputType;
        this.publishSelection();
    };
    FuzzyDateSelectorComponent.prototype.setDefaults = function () {
        this.exactDate = this.exactDate || new Date();
        this.startDate = this.startDate || new Date();
        this.endDate = this.endDate || new Date();
        this.selectedMode = this.selectedMode || this.exactInputMode;
    };
    FuzzyDateSelectorComponent.prototype.getSelection = function () {
        var selection;
        if (this.selectedMode === this.exactInputMode) {
            selection = {
                selectedMode: this.selectedMode,
                exactDate: this.exactDate
            };
        }
        else if (this.startDate && this.endDate) {
            selection = {
                selectedMode: this.selectedMode,
                startDate: this.startDate,
                endDate: this.endDate
            };
        }
        return selection;
    };
    FuzzyDateSelectorComponent.prototype.updateCompleteness = function () {
        var completed = (this.selectedMode === this.exactInputMode && !!this.exactDate) ||
            (this.selectedMode === this.fuzzyInputMode && !!this.startDate && !!this.endDate);
        this.events.publish(FuzzyDateSelectorComponent.completedEvent, completed);
    };
    FuzzyDateSelectorComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    FuzzyDateSelectorComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    FuzzyDateSelectorComponent.componentName = "FuzzyDateSelectorComponent";
    FuzzyDateSelectorComponent.selectedEvent = FuzzyDateSelectorComponent.componentName + ":selection";
    FuzzyDateSelectorComponent.completedEvent = FuzzyDateSelectorComponent.componentName + ":completed";
    FuzzyDateSelectorComponent.configInputs = ['mocked', 'icons', 'styles'];
    FuzzyDateSelectorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fuzzy-date-selector',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["fuzzy-date-selector ion-grid {   padding-left: 30px !important; }  fuzzy-date-selector .mobileDate input {   width: 100% !important; }"],
                    template: "<!-- exact input--> <ion-item   no-padding class=\"clickable\"   [class.clickable]=\"selectedMode !== exactInputMode\"   (click)=\"selectInputMode(exactInputMode)\"   [ngStyle]=\"styles\">    <ion-icon     item-start class=\"selection\"     [name]=\"selectedMode === exactInputMode ? getIcon(0) : getIcon(1)\">   </ion-icon>    <h4>     {{ translate('home-incidence-date-selection.fuzzy_date_selector.exact_date') }}   </h4>  </ion-item>  <ion-grid [hidden]=\"selectedMode !== exactInputMode\" class=\"no-padding-right no-vertical-col-padding col-padding-s\">   <ion-row>      <ion-col col-12 col-sm-4>        <mapfre-datetime         [placeHolder]=\"translate('home-incidence-date-selection.fuzzy_date_selector.date')\"         [pickerFormat]=\"datePickerPattern\"         [displayFormat]=\"datePattern\"         [calendar]=\"true\"         [(mModel)]=\"exactDate\"         (mModelChange)=\"publishSelection()\">       </mapfre-datetime>        <!--<strong class=\"primary-text\">-->         <!--{{ exactDate ? exactDate : exactDate.getTime() }}-->       <!--</strong>-->      </ion-col>    </ion-row> </ion-grid>  <!-- fuzzy input --> <ion-item   no-padding class=\"clickable\"   [class.clickable]=\"selectedMode !== fuzzyInputMode\"   (click)=\"selectInputMode(fuzzyInputMode)\">    <ion-icon     item-start class=\"selection\"     [name]=\"selectedMode === fuzzyInputMode ? getIcon(2) : getIcon(3)\">   </ion-icon>    <h4>     {{ translate('home-incidence-date-selection.fuzzy_date_selector.fuzzy_date') }}   </h4>  </ion-item>  <ion-grid [hidden]=\"selectedMode !== fuzzyInputMode\" *ngIf=\"!onMobile\" class=\"no-padding-right no-vertical-col-padding col-padding-s\">   <ion-row>      <ion-col col-6 col-sm-4>        <mapfre-datetime         [placeHolder]=\"translate('home-incidence-date-selection.fuzzy_date_selector.start_date')\"         [pickerFormat]=\"datePickerPattern\"         [displayFormat]=\"datePattern\"         [calendar]=\"true\"         [(mModel)]=\"startDate\"         (mModelChange)=\"publishSelection()\">       </mapfre-datetime>      </ion-col>      <ion-col col-6 col-sm-4>        <mapfre-datetime         [placeHolder]=\"translate('home-incidence-date-selection.fuzzy_date_selector.end_date')\"         [pickerFormat]=\"datePickerPattern\"         [displayFormat]=\"datePattern\"         [calendar]=\"true\"         [(mModel)]=\"endDate\"         (mModelChange)=\"publishSelection()\">       </mapfre-datetime>      </ion-col>    </ion-row> </ion-grid>  <ion-grid [hidden]=\"selectedMode !== fuzzyInputMode\" *ngIf=\"onMobile\" class=\"no-padding-right no-vertical-col-padding col-padding-s\">   <ion-row>      <ion-col col-6 col-sm-3>        <mapfre-datetime class=\"mobileDate\"         [placeHolder]=\"translate('home-incidence-date-selection.fuzzy_date_selector.start_date')\"         [pickerFormat]=\"datePickerPattern\"         [displayFormat]=\"datePattern\"         [calendar]=\"true\"         [(mModel)]=\"startDate\"         (mModelChange)=\"publishSelection()\">       </mapfre-datetime>      </ion-col>      <ion-col col-6 col-sm-3>        <mapfre-datetime class=\"mobileDate\"         [placeHolder]=\"translate('home-incidence-date-selection.fuzzy_date_selector.end_date')\"         [pickerFormat]=\"datePickerPattern\"         [displayFormat]=\"datePattern\"         [calendar]=\"true\"         [(mModel)]=\"endDate\"         (mModelChange)=\"publishSelection()\">       </mapfre-datetime>      </ion-col>    </ion-row> </ion-grid>"
                },] },
    ];
    FuzzyDateSelectorComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: PlatformProvider, },
        { type: Events, },
    ]; };
    FuzzyDateSelectorComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'selectedDate': [{ type: Input },],
        'selectedMode': [{ type: Input },],
        'exactDate': [{ type: Input },],
        'startDate': [{ type: Input },],
        'endDate': [{ type: Input },],
        'onSelection': [{ type: Output },],
    };
    return FuzzyDateSelectorComponent;
}());
export { FuzzyDateSelectorComponent };
//# sourceMappingURL=fuzzy-date-selector.js.map