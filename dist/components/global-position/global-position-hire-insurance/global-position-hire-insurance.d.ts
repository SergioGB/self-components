import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class GlobalPositionHireInsuranceComponent implements OnInit {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    ready: boolean;
    defaultIcons: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
    openLink(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
