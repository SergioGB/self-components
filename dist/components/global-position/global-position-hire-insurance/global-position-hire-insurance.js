import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
var GlobalPositionHireInsuranceComponent = (function () {
    function GlobalPositionHireInsuranceComponent(translationProvider) {
        this.translationProvider = translationProvider;
        this.defaultIcons = ["mapfre-arrow-right"];
        this.translationProvider.bind(this);
    }
    GlobalPositionHireInsuranceComponent.prototype.ngOnInit = function () {
        this.ready = true;
    };
    GlobalPositionHireInsuranceComponent.prototype.openLink = function () {
        window.open('https://www.mapfre.es');
    };
    GlobalPositionHireInsuranceComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    GlobalPositionHireInsuranceComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    GlobalPositionHireInsuranceComponent.configInputs = ['mocked', 'icons', 'styles'];
    GlobalPositionHireInsuranceComponent.decorators = [
        { type: Component, args: [{
                    selector: 'global-position-hire-insurance',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["global-position-hire-insurance span.related-product {   padding-left: 20px; }"],
                    template: "<ion-card *ngIf=\"ready\" tabindex=\"0\" class=\"clickable\" (click)=\"openLink()\" [ngStyle]=\"styles\">    <ion-card-header class=\"no-border\">     <ion-item>       <h3>         {{ translate('global-position-hire-insurance.hire_insurance.title') }}       </h3>       <h4 class=\"fmm-body1 grey1\">         {{ translate('global-position-hire-insurance.hire_insurance.description')}}       </h4>        <ion-icon item-end no-margin [name]=\"getIcon(0)\" class=\"primary\"></ion-icon>     </ion-item>   </ion-card-header>  </ion-card>"
                },] },
    ];
    GlobalPositionHireInsuranceComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
    ]; };
    GlobalPositionHireInsuranceComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return GlobalPositionHireInsuranceComponent;
}());
export { GlobalPositionHireInsuranceComponent };
//# sourceMappingURL=global-position-hire-insurance.js.map