import { OnInit } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { TranslationProvider } from '../../../providers/translation/translation';
import { Communication } from '../../../models/communication';
export declare class GlobalPositionNewCommunicationsComponent implements OnInit {
    private componentSettingsProvider;
    private translationProvider;
    private navCtrl;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    ready: boolean;
    defaultIcons: string[];
    communications: Communication[];
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, navCtrl: NavController, events: Events);
    ngOnInit(): void;
    goToCommunications(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
