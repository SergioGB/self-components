import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { TranslationProvider } from '../../../providers/translation/translation';
var GlobalPositionNewCommunicationsComponent = (function () {
    function GlobalPositionNewCommunicationsComponent(componentSettingsProvider, translationProvider, navCtrl, events) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.translationProvider = translationProvider;
        this.navCtrl = navCtrl;
        this.events = events;
        this.defaultIcons = ["mapfre-arrow-right"];
        this.translationProvider.bind(this);
    }
    GlobalPositionNewCommunicationsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.componentSettingsProvider.getCommunicationsSettingsByState(this.mocked, 'UNREAD').subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.communications = mapfreResponse.data;
            }
            _this.ready = true;
        });
    };
    GlobalPositionNewCommunicationsComponent.prototype.goToCommunications = function () {
        this.events.publish('global-position-new-communications::goCommunicationsCenter');
    };
    GlobalPositionNewCommunicationsComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    GlobalPositionNewCommunicationsComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    GlobalPositionNewCommunicationsComponent.configInputs = ['mocked', 'icons', 'styles'];
    GlobalPositionNewCommunicationsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'global-position-new-communications',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["global-position-new-communications span.related-product {   padding-left: 20px; }"],
                    template: "<ion-card *ngIf=\"!ready || communications && communications.length\" [ngStyle]=\"styles\">   <loading-spinner *ngIf=\"!ready\"></loading-spinner>    <div *ngIf=\"ready\">     <ion-card-header class=\"no-padding-bottom\">       <ion-item>         <h3>           {{ translate('global-position-new-communications.new_communications.title') }}         </h3>       </ion-item>     </ion-card-header>      <ion-card-content class=\"no-vertical-padding\">        <ion-item *ngFor=\"let communication of communications\" tabindex=\"0\" no-padding class=\"bottom-border border-only clickable\" (click)=\"goToCommunications()\">         <ion-icon           *ngIf=\"communication.icon\" [name]=\"communication.icon.name\" item-start large [style.color]=\"communication.icon.hex_color\">         </ion-icon>          <h4>           {{ communication.title }}         </h4>          <p class=\"fmm-body1 fmm-regular\">           <span>{{ communication.code }}</span><span *ngIf=\"communication.related_risk\" class=\"related-product\">{{ translate('global-position-new-communications.new_communications.related_product') }} - {{ communication.related_risk }}</span>         </p>          <ion-icon           item-end small [name]=\"getIcon(0)\" class=\"primary no-margin-right\">         </ion-icon>       </ion-item>     </ion-card-content>   </div>  </ion-card>"
                },] },
    ];
    GlobalPositionNewCommunicationsComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: TranslationProvider, },
        { type: NavController, },
        { type: Events, },
    ]; };
    GlobalPositionNewCommunicationsComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return GlobalPositionNewCommunicationsComponent;
}());
export { GlobalPositionNewCommunicationsComponent };
//# sourceMappingURL=global-position-new-communications.js.map