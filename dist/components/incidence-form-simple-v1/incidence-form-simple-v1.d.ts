import { OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { DocumentsProvider } from '../../providers/documents/documents';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { IncidencesAndClaimsProvider } from '../../providers/incidences-and-claims/incidences-and-claims';
import { Product } from '../../models/product';
import { UserProvider } from '../../providers/user/user';
export declare class IncidenceFormSimpleV1Component implements OnInit {
    private events;
    private formBuilder;
    private translationProvider;
    private documentsProvider;
    private userProvider;
    private componentSettingsProvider;
    private incidencesAndClaimsProvider;
    mocked?: boolean;
    styles?: any;
    associated_documents: any;
    wrongEmail: boolean;
    wrongPhone: boolean;
    componentReady: boolean;
    isPotential: boolean;
    incidenceForm: FormGroup;
    incidencePotencialForm: FormGroup;
    products: Product[];
    selectOptions: any;
    static configInputs: string[];
    constructor(events: Events, formBuilder: FormBuilder, translationProvider: TranslationProvider, documentsProvider: DocumentsProvider, userProvider: UserProvider, componentSettingsProvider: ComponentSettingsProvider, incidencesAndClaimsProvider: IncidencesAndClaimsProvider);
    ngOnInit(): void;
    onBlurEmail(): void;
    onBlurPhone(): void;
    confirmForm(): any;
    private queryFormData;
    private loadInsuranceProducts;
    private attemptSubmission;
    private redirectToConfirmation;
    private initializeData;
    private initializeForm;
}
