import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { DocumentsProvider } from '../../providers/documents/documents';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { IncidencesAndClaimsProvider } from '../../providers/incidences-and-claims/incidences-and-claims';
import { Product } from '../../models/product';
import { UserProvider } from '../../providers/user/user';
import { Observable } from 'rxjs/Observable';
var IncidenceFormSimpleComponent = (function () {
    function IncidenceFormSimpleComponent(events, formBuilder, translationProvider, documentsProvider, userProvider, componentSettingsProvider, incidencesAndClaimsProvider) {
        this.events = events;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.documentsProvider = documentsProvider;
        this.userProvider = userProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.incidencesAndClaimsProvider = incidencesAndClaimsProvider;
        this.selectOptions = {
            cssClass: 'popover-content-cm'
        };
        this.translationProvider.bind(this);
        this.initializeData();
        this.initializeForm();
    }
    IncidenceFormSimpleComponent.prototype.ngOnInit = function () {
        this.queryFormData();
    };
    IncidenceFormSimpleComponent.prototype.onBlurEmail = function () {
        this.wrongEmail =
            this.incidenceForm.value.email &&
                !this.incidenceForm.controls['email'].valid && this.incidenceForm.controls['email'].dirty;
    };
    IncidenceFormSimpleComponent.prototype.onBlurPhone = function () {
        this.wrongPhone =
            this.incidenceForm.value.phone_number &&
                !this.incidenceForm.controls['phone_number'].valid && this.incidenceForm.controls['phone_number'].dirty;
    };
    IncidenceFormSimpleComponent.prototype.confirmForm = function () {
        if (this.mocked) {
            this.redirectToConfirmation();
        }
        else {
            this.attemptSubmission();
        }
    };
    IncidenceFormSimpleComponent.prototype.queryFormData = function () {
        var _this = this;
        Observable.forkJoin([
            this.componentSettingsProvider.getInsuranceSettings(this.mocked)
        ]).subscribe(function (responses) {
            if (_this.componentSettingsProvider.areResponsesValid(responses)) {
                if (_this.componentSettingsProvider.getResponseJSON(responses[0])) {
                    _this.loadInsuranceProducts(_this.componentSettingsProvider.getResponseJSON(responses[0]).data);
                }
                _this.componentReady = true;
            }
        });
    };
    IncidenceFormSimpleComponent.prototype.loadInsuranceProducts = function (insurances) {
        for (var i = 0; i < insurances.length; i++) {
            for (var j = 0; j < insurances[i].associated_policies.length; j++) {
                if (insurances[i].associated_policies[j]) {
                    this.products.push(new Product(insurances[i].associated_policies[j].id, insurances[i].name + " - " + insurances[i].associated_policies[j].type));
                }
            }
        }
    };
    IncidenceFormSimpleComponent.prototype.attemptSubmission = function () {
        var _this = this;
        if (this.incidenceForm.valid) {
            this.userProvider.getUserData().subscribe(function (userData) {
                if (userData) {
                    var incidenceFormData = {
                        client_personal_data: userData.personal_data,
                        loss_data: _this.incidenceForm.value,
                        associated_documents: _this.associated_documents
                    };
                    _this.incidencesAndClaimsProvider.postIncidenceForm(_this.mocked, incidenceFormData).subscribe(function (response) {
                        var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                        var success = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
                        if (success) {
                            _this.redirectToConfirmation();
                        }
                    });
                }
            });
        }
    };
    IncidenceFormSimpleComponent.prototype.redirectToConfirmation = function () {
        this.events.publish('incidence-form-simple::goConfirmIncidence');
    };
    IncidenceFormSimpleComponent.prototype.initializeData = function () {
        this.associated_documents = [];
        this.products = [];
    };
    IncidenceFormSimpleComponent.prototype.initializeForm = function () {
        this.incidenceForm = this.formBuilder.group({
            phone_number: ['', [Validators.pattern('[0-9]{9}')]],
            email: ['', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
            product_id: [''],
            contract_number: [''],
            reason_id: [''],
            reason_description: ['']
        });
    };
    IncidenceFormSimpleComponent.configInputs = ['mocked', 'styles'];
    IncidenceFormSimpleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'incidence-form-simple',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["incidence-form-simple .minimum-width {   min-width: 200px; }"],
                    template: "<loading-spinner *ngIf=\"!componentReady\"></loading-spinner> <form *ngIf=\"componentReady\" [formGroup]=\"incidenceForm\" class=\"incidence-and-claims-form\" [ngStyle]=\"styles\">   <ion-card>     <ion-card-header>       <ion-item>         <h3>           {{ translate('incidence-form-simple.incidence_form_simple.fill_data') }}         </h3>       </ion-item>     </ion-card-header>      <ion-card-content class=\"contentCard\">       <ion-row class=\"content-form\">         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{translate('incidence-form-simple.incidence_form_simple.phone_number')}}             </ion-label>             <ion-input type=\"tel\" formControlName=\"phone_number\" pattern=\"(\\\\+[0-9]*)?[0-9]*\" (ionBlur)=\"onBlurPhone()\">             </ion-input>           </ion-item>           <div *ngIf=\"wrongPhone\" class=\"format-error\">             <span>               {{ translate('incidence-form-simple.incidence_form_simple.phone_format_error') }}.             </span>           </div>         </ion-col>         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{translate('incidence-form-simple.incidence_form_simple.mail')}}             </ion-label>             <ion-input type=\"text\" formControlName=\"email\" (ionBlur)=\"onBlurEmail()\"></ion-input>           </ion-item>           <div *ngIf=\"wrongEmail\" class=\"format-error\">             <span>               {{ translate('incidence-form-simple.incidence_form_simple.email_format_error') }}.             </span>           </div>         </ion-col>         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{translate('incidence-form-simple.incidence_form_simple.select_product')}}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" fromControlName=\"product_id\">               <ion-option *ngFor=\"let product of products\" value=\"{{product.id}}\">                 {{product.name}}               </ion-option>             </ion-select>           </ion-item>         </ion-col>         <ion-col col-xl-6 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{translate('incidence-form-simple.incidence_form_simple.contract_number')}}             </ion-label>             <ion-input type=\"text\" formControlName=\"contract_number\"></ion-input>           </ion-item>         </ion-col>       </ion-row>       <ion-row class=\"text-area\">         <ion-col col-12>           <ion-item class=\"text-area margin-bottom-xs\">              <ion-label floating>               {{translate('incidence-form-simple.incidence_form_simple.reason_description')}}             </ion-label>              <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\" formControlName=\"reason_description\">             </ion-textarea>            </ion-item>         </ion-col>       </ion-row>     </ion-card-content>   </ion-card>    <file-uploader-card [title]=\"translate('incidence-form-simple.incidence_form_simple.upload_documents.title')\"     [allowedTypes]=\"['pdf', 'image', 'video']\" [files]=\"associated_documents\">   </file-uploader-card>    <ion-grid no-padding>     <ion-row>       <ion-col col-12 col-sm-3 col-lg-2 class=\"margin-left-auto minimum-width\">         <button [disabled]=\"!incidenceForm.valid\" full text-uppercase (click)=\"confirmForm()\" ion-button>           {{ translate('incidence-form-simple.incidence_form_simple.confirm_button') }}         </button>       </ion-col>     </ion-row>   </ion-grid>  </form>"
                },] },
    ];
    IncidenceFormSimpleComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: DocumentsProvider, },
        { type: UserProvider, },
        { type: ComponentSettingsProvider, },
        { type: IncidencesAndClaimsProvider, },
    ]; };
    IncidenceFormSimpleComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return IncidenceFormSimpleComponent;
}());
export { IncidenceFormSimpleComponent };
//# sourceMappingURL=incidence-form-simple.js.map