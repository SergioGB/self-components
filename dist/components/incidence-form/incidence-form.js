import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Events } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { TranslationProvider } from '../../providers/translation/translation';
import { DocumentsProvider } from '../../providers/documents/documents';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { IncidencesAndClaimsProvider } from '../../providers/incidences-and-claims/incidences-and-claims';
import { Product } from '../../models/product';
var IncidenceFormComponent = (function () {
    function IncidenceFormComponent(navCtrl, formBuilder, translationProvider, documentsProvider, componentSettingsProvider, incidencesAndClaimsProvider, events) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.documentsProvider = documentsProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.incidencesAndClaimsProvider = incidencesAndClaimsProvider;
        this.events = events;
        this.defaultIcons = ["mapfre-upload-doc"];
        this.selectOptions = {
            cssClass: 'popover-content-cm'
        };
        this.translationProvider.bind(this);
        this.initializeData();
        this.initializeForm();
    }
    IncidenceFormComponent.prototype.ngOnInit = function () {
        this.associated_documents = [];
        this.queryFormData();
    };
    IncidenceFormComponent.prototype.fileEvent = function (fileInput) {
        this.documentsProvider.uploadFiles(fileInput, this.associated_documents, true, false, false);
    };
    IncidenceFormComponent.prototype.downloadFile = function (mapfreFile) {
        this.documentsProvider.downloadFile(mapfreFile);
    };
    IncidenceFormComponent.prototype.removeFile = function (file) {
        var index = this.associated_documents.indexOf(file);
        if (index > -1) {
            this.associated_documents.splice(index, 1);
        }
    };
    IncidenceFormComponent.prototype.onBlurEmail = function () {
        this.wrongEmail =
            this.incidenceForm.value.email &&
                !this.incidenceForm.controls['email'].valid && this.incidenceForm.controls['email'].dirty;
    };
    IncidenceFormComponent.prototype.onBlurPhone = function () {
        this.wrongPhone =
            this.incidenceForm.value.phone_number &&
                !this.incidenceForm.controls['phone_number'].valid && this.incidenceForm.controls['phone_number'].dirty;
    };
    IncidenceFormComponent.prototype.confirmForm = function () {
        if (this.mocked) {
            this.redirectToConfirmation();
        }
        else {
            this.attemptSubmission();
        }
    };
    IncidenceFormComponent.prototype.queryFormData = function () {
        var _this = this;
        Observable.forkJoin([
            this.componentSettingsProvider.getInsuranceSettings(this.mocked),
            this.incidencesAndClaimsProvider.getIncidenceReasons(this.mocked),
            this.componentSettingsProvider.getCountriesSettings(this.mocked),
            this.componentSettingsProvider.getStatesSettings(this.mocked),
            this.componentSettingsProvider.getProvincesSettings(this.mocked),
            this.componentSettingsProvider.getCitiesSettings(this.mocked),
            this.componentSettingsProvider.getRoadTypesSettings(this.mocked)
        ]).subscribe(function (responses) {
            if (_this.componentSettingsProvider.areResponsesValid(responses)) {
                if (_this.componentSettingsProvider.getResponseJSON(responses[0])) {
                    _this.loadInsuranceProducts(_this.componentSettingsProvider.getResponseJSON(responses[0]).data);
                }
                _this.incidenceReasons = _this.componentSettingsProvider.getResponseJSON(responses[1]).data;
                _this.countries = _this.componentSettingsProvider.getResponseJSON(responses[2]).data;
                _this.states = _this.componentSettingsProvider.getResponseJSON(responses[3]).data;
                _this.provinces = _this.componentSettingsProvider.getResponseJSON(responses[4]).data;
                _this.cities = _this.componentSettingsProvider.getResponseJSON(responses[5]).data;
                _this.road_types = _this.componentSettingsProvider.getResponseJSON(responses[6]).data;
                _this.componentReady = true;
            }
        });
    };
    IncidenceFormComponent.prototype.loadInsuranceProducts = function (insurances) {
        var _this = this;
        insurances.forEach(function (insurance) {
            insurance.associated_policies.forEach(function (policy) {
                _this.products.push(new Product(policy.id, insurance.name + " - " + policy.type));
            });
        });
    };
    IncidenceFormComponent.prototype.attemptSubmission = function () {
        var _this = this;
        if (this.incidenceForm.valid) {
            var incidenceFormData = void 0;
            incidenceFormData = {
                client_personal_data: {
                    name: this.incidenceForm.value.name,
                    surname1: this.incidenceForm.value.surname1,
                    surname2: this.incidenceForm.value.surname2,
                    identification_document_number: this.incidenceForm.value.identity_doc,
                    address: {
                        road_type_id: this.incidenceForm.value.road_type.id,
                        road_name: this.incidenceForm.value.road_name,
                        road_number: this.incidenceForm.value.road_number,
                        zip_code: this.incidenceForm.value.postal_code,
                        city_id: this.incidenceForm.value.city.id,
                        province_id: this.incidenceForm.value.province.id,
                        country_id: this.incidenceForm.value.country.id
                    }
                },
                loss_data: {
                    phone: this.incidenceForm.value.phone_number,
                    mail: this.incidenceForm.value.email,
                    product_id: this.incidenceForm.value.product_id,
                    contract_number: this.incidenceForm.value.contract_number,
                    reason_id: this.incidenceForm.value.reason_id,
                    reason_detail: this.incidenceForm.value.reason_detail
                }
            };
            this.incidencesAndClaimsProvider.postIncidenceForm(this.mocked, incidenceFormData).subscribe(function (response) {
                var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                var success = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
                if (success) {
                    _this.responseForm = mapfreResponse.data.id;
                }
            });
            var documentForm = new FormData();
            documentForm.append('upfile', this.associated_documents[0]);
            documentForm.append('object', 'LOSS');
            documentForm.append('object_id', this.responseForm);
            this.incidencesAndClaimsProvider.postDocuments(this.mocked, documentForm).subscribe(function (response) {
                var mapfreResponseDoc = _this.componentSettingsProvider.getResponseJSON(response);
                if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponseDoc)) {
                    _this.redirectToConfirmation();
                }
            });
        }
    };
    IncidenceFormComponent.prototype.redirectToConfirmation = function () {
        this.events.publish('incidence-form::goConfirmIncidence');
    };
    IncidenceFormComponent.prototype.initializeData = function () {
        this.associated_documents = [];
        this.products = [];
    };
    IncidenceFormComponent.prototype.initializeForm = function () {
        this.incidenceForm = this.formBuilder.group({
            identity_doc: ['', Validators.required],
            name: ['', [Validators.required, Validators.pattern('[A-Z a-z]*')]],
            surname1: ['', [Validators.required, Validators.pattern('[A-Z a-z]*')]],
            surname2: ['', [Validators.pattern('[A-Z a-z]*')]],
            phone_number: ['', [Validators.required]],
            email: ['', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
            product_id: [''],
            contract_number: ['', Validators.required],
            reason_id: [''],
            reason_description: [''],
            road_type: ['', [Validators.required]],
            road_name: ['', [Validators.required]],
            road_number: ['', [Validators.required]],
            postal_code: ['', [Validators.required]],
            city: ['', [Validators.required]],
            province: ['', [Validators.required]],
            country: ['', [Validators.required]]
        });
    };
    IncidenceFormComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    IncidenceFormComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    IncidenceFormComponent.configInputs = ['mocked', 'icons', 'styles'];
    IncidenceFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'incidence-form',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<loading-spinner *ngIf=\"!componentReady\"></loading-spinner> <form *ngIf=\"componentReady\" [formGroup]=\"incidenceForm\" class=\"incidence-and-claims-form\" [ngStyle]=\"styles\">   <ion-card>     <ion-card-header>       <ion-item>         <h2>           {{ translate('incidence-form.incidence_form.fill_data') }}         </h2>       </ion-item>     </ion-card-header>      <ion-card-content class=\"contentCard\">       <ion-row class=\"content-form\">         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.incidence_form.name') }}             </ion-label>             <ion-input type=\"text\" formControlName=\"name\"></ion-input>           </ion-item>         </ion-col>         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.incidence_form.surname1') }}             </ion-label>             <ion-input type=\"text\" formControlName=\"surname1\"></ion-input>           </ion-item>         </ion-col>         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{ translate('incidence-form.incidence_form.surname2') }}             </ion-label>             <ion-input type=\"text\" formControlName=\"surname2\"></ion-input>           </ion-item>         </ion-col>         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.incidence_form.identification_number') }}             </ion-label>             <ion-input type=\"text\" formControlName=\"identity_doc\"></ion-input>           </ion-item>         </ion-col>         <ion-col col-xl-4 col-12 col-sm-6>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.loss_report.where.road_type') }}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" formControlName=\"road_type\" full>               <ion-option *ngFor=\"let road_type of road_types\" [value]=\"road_type\">                 {{ road_type.title }}               </ion-option>             </ion-select>           </ion-item>         </ion-col>          <ion-col col-xl-4 col-12 col-sm-6>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.loss_report.where.road_name') }}             </ion-label>             <ion-input               type=\"text\"               maxlength=\"50\"               formControlName=\"road_name\">             </ion-input>           </ion-item>         </ion-col>          <ion-col col-xl-4 col-12 col-sm-6>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.loss_report.where.road_number') }}             </ion-label>             <ion-input               type=\"text\"               maxlength=\"50\"               formControlName=\"road_number\">             </ion-input>           </ion-item>         </ion-col>          <ion-col col-xl-4 col-12 col-sm-6>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.loss_report.where.cp') }}             </ion-label>             <ion-input               type=\"text\"               maxlength=\"50\"               formControlName=\"postal_code\">             </ion-input>           </ion-item>         </ion-col>          <ion-col col-xl-4 col-12 col-sm-6>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.loss_report.where.city') }}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" formControlName=\"city\" full>               <ion-option *ngFor=\"let city of cities\" [value]=\"city\">                 {{ city.title }}               </ion-option>             </ion-select>           </ion-item>         </ion-col>          <ion-col col-xl-4 col-12 col-sm-6>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.loss_report.where.province') }}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" formControlName=\"province\" full>               <ion-option *ngFor=\"let province of provinces\" [value]=\"province\">                 {{ province.title }}               </ion-option>             </ion-select>           </ion-item>         </ion-col>          <ion-col col-xl-4 col-12 col-sm-6>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.loss_report.where.country') }}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" formControlName=\"country\" full>               <ion-option *ngFor=\"let country of countries\" [value]=\"country\">                 {{ country.title }}               </ion-option>             </ion-select>           </ion-item>         </ion-col>         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.incidence_form.phone_number') }}             </ion-label>             <ion-input type=\"tel\" formControlName=\"phone_number\" pattern=\"(\\\\+[0-9]*)?[0-9]*\" (ionBlur)=\"onBlurPhone()\"></ion-input>           </ion-item>           <div *ngIf=\"wrongPhone\" class=\"format-error\">             <span>               {{ translate('incidence-form.incidence_form.phone_format_error') }}.             </span>           </div>         </ion-col>         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{ translate('incidence-form.incidence_form.mail') }}             </ion-label>             <ion-input type=\"text\" formControlName=\"email\" (ionBlur)=\"onBlurEmail()\"></ion-input>           </ion-item>           <div *ngIf=\"wrongEmail\" class=\"format-error\">             <span>               {{ translate('incidence-form.incidence_form.email_format_error') }}.             </span>           </div>         </ion-col>         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{ translate('incidence-form.incidence_form.select_product') }}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" formControlName=\"product_id\">               <ion-option *ngFor=\"let product of products\" value=\"{{ product.id }}\">                 {{ product.name }}               </ion-option>             </ion-select>           </ion-item>         </ion-col>         <ion-col col-xl-4 col-sm-6 col-12>           <ion-item class=\"required\">             <ion-label floating>               {{ translate('incidence-form.incidence_form.contract_number') }}             </ion-label>             <ion-input type=\"text\" formControlName=\"contract_number\"></ion-input>           </ion-item>         </ion-col>         <ion-col col-xl-8 col-sm-6 col-12>           <ion-item>             <ion-label floating>               {{ translate('incidence-form.incidence_form.reason') }}             </ion-label>             <ion-select [selectOptions]=\"selectOptions\" formControlName=\"reason_id\">               <ion-option *ngFor=\"let reason of incidenceReasons\" value=\"{{ reason.id }}\">                 {{ reason.title }}               </ion-option>             </ion-select>           </ion-item>         </ion-col>       </ion-row>       <ion-row class=\"text-area\">         <ion-col col-12>           <ion-item no-border>             <ion-label floating>               {{ translate('incidence-form.incidence_form.reason_description') }}             </ion-label>             <ion-textarea id=\"textArea\" rows=5 maxLength=\"150\" autosize formControlName=\"reason_description\"></ion-textarea>           </ion-item>         </ion-col>       </ion-row>     </ion-card-content>   </ion-card>    <ion-card>     <ion-card-header>       <ion-item>         <h2>           {{ translate('incidence-form.incidence_form.upload_documents.title') }}         </h2>       </ion-item>     </ion-card-header>      <ion-card-content class=\"file-drop\">        <div *ngIf=\"associated_documents\" class=\"uploaded-files\">         <download-item           *ngFor=\"let file of associated_documents\" [file]=\"file\" [noDownload]=\"true\"           (onDownload)=\"downloadFile($event)\" (onRemove)=\"removeFile($event)\">         </download-item>       </div>        <div dnd class=\"drop-area\">         <ion-icon [name]=\"getIcon(0)\"></ion-icon>         <a class=\"font-m upload-link clickable\" (click)=\"fileUpload.click()\" tabindex=\"0\" title=\"{{ translate('incidence-form.incidence_form.upload_documents.upload11') }}\">           {{ translate('incidence-form.incidence_form.upload_documents.upload1') }}         </a>         <span>           {{ translate('incidence-form.incidence_form.upload_documents.your_files') }}         </span>         <input #fileUpload type=\"file\" (change)=\"fileEvent($event)\">       </div>      </ion-card-content>   </ion-card>    <ion-grid no-padding>     <ion-row>       <ion-col col-12 col-sm-3 col-lg-2 class=\"margin-left-auto\">         <button [disabled]=\"!incidenceForm.valid\" full text-uppercase (click)=\"confirmForm()\" ion-button>           {{ translate('incidence-form.incidence_form.confirm_button') }}         </button>       </ion-col>     </ion-row>   </ion-grid>  </form>"
                },] },
    ];
    IncidenceFormComponent.ctorParameters = function () { return [
        { type: NavController, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: DocumentsProvider, },
        { type: ComponentSettingsProvider, },
        { type: IncidencesAndClaimsProvider, },
        { type: Events, },
    ]; };
    IncidenceFormComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return IncidenceFormComponent;
}());
export { IncidenceFormComponent };
//# sourceMappingURL=incidence-form.js.map