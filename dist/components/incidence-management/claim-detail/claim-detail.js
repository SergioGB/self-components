import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events } from 'ionic-angular';
import { FormatProvider } from '../../../providers/format/format';
import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { IncidencesAndClaimsProvider } from '../../../providers/incidences-and-claims/incidences-and-claims';
import { Mocks } from '../../../providers/mocks/mocks';
var ClaimDetailComponent = (function () {
    function ClaimDetailComponent(events, translationProvider, documentsProvider, formatProvider, componentSettingsProvider, formBuilder, incidencesAndClaimsProvider) {
        this.events = events;
        this.translationProvider = translationProvider;
        this.documentsProvider = documentsProvider;
        this.formatProvider = formatProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.formBuilder = formBuilder;
        this.incidencesAndClaimsProvider = incidencesAndClaimsProvider;
        this.translationProvider.bind(this);
        this.formatDate = this.formatProvider.formatDate;
        this.formatCost = this.formatProvider.formatCost;
    }
    ClaimDetailComponent.prototype.ngOnInit = function () {
        this.additionalDataForm = this.formBuilder.group({
            additionalData: ['', Validators.required]
        });
        if (this.mocked) {
            this.claimData = Mocks.getClaim();
            this.componentReady = true;
        }
        else {
        }
    };
    ClaimDetailComponent.prototype.sendAdditionalData = function () {
        this.additionalDataForm.reset();
    };
    ClaimDetailComponent.prototype.attemptSubmission = function () {
        if (this.mocked) {
            this.redirectToConfirmation();
        }
        else {
            this.postDocuments();
        }
    };
    ClaimDetailComponent.prototype.postDocuments = function () {
        var _this = this;
        var documentForm = new FormData();
        documentForm.append('upfile', this.claimData.associated_documents[0]);
        documentForm.append('object', 'COMPLAINT');
        documentForm.append('object_id', this.responseForm);
        this.incidencesAndClaimsProvider.postDocuments(this.mocked, documentForm).subscribe(function (response) {
            var mapfreResponseDoc = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponseDoc)) {
                _this.redirectToConfirmation();
            }
        });
    };
    ClaimDetailComponent.prototype.redirectToConfirmation = function () {
        this.events.publish('claim-detail::goConfirmIncidence');
    };
    ClaimDetailComponent.inputs = ['insurance', 'size'];
    ClaimDetailComponent.configInputs = ['mocked', 'styles'];
    ClaimDetailComponent.decorators = [
        { type: Component, args: [{
                    selector: 'claim-detail',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">   <form [formGroup]=\"additionalDataForm\" (ngSubmit)=\"sendAdditionalData()\">     <loading-spinner *ngIf=\"!componentReady\"></loading-spinner>     <div *ngIf=\"componentReady\">       <ion-card-header class=\"no-border\">         <ion-item>           <h3 class=\"primary\">             {{ translate('claim-detail.my_claims.claim_number') }} {{ claimData.id }}           </h3>          </ion-item>       </ion-card-header>        <div>         <ion-item class=\"sub-title\">           <h4>             {{ translate('claim-detail.my_claims.data.title') }}           </h4>         </ion-item>          <ion-card-content class=\"no-padding-bottom\">           <ion-item no-padding>             <ion-grid no-padding class=\"values-list\">               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.name') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.name }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.surname1') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.surname1 }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.surname2') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.surname2 }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.identity_document') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.identification_number }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.address') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.address }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.phone_number') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.phone_number }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.email') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.email }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.product') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.product }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.contract_number') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.contract_number }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.reason') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.reason }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.reason_detail') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.reason_detail }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('claim-detail.my_claims.data.claim_detail') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claimData.claim_detail }}                   </span>                 </ion-col>               </ion-row>               <hr>             </ion-grid>             <hr>           </ion-item>           <div>             <ion-item class=\"text-area margin-bottom-m\">                <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\" formControlName=\"additionalData\"                 [placeholder]=\"translate('claim-detail.loss_detail.more_data')\">               </ion-textarea>              </ion-item>           </div>         </ion-card-content>       </div>        <ion-item class=\"sub-title\">         <h4>           {{ translate('claim-detail.my_claims.docs.title') }}         </h4>       </ion-item>        <ion-card-content class=\"padding-top-m\">          <file-uploader [allowedTypes]=\"{ pdf: true, image: false, video: false }\"           [files]=\"claimData.associated_documents\">         </file-uploader>          <ion-grid no-padding class=\"margin-top-m\">           <ion-row>             <ion-col col-12 col-sm-3 col-lg-2 class=\"margin-left-auto\">               <button full text-uppercase (click)=\"attemptSubmission()\" ion-button>                 {{ translate('claim-detail.claim_form.confirm_button') }}               </button>             </ion-col>           </ion-row>         </ion-grid>        </ion-card-content>      </div>   </form> </ion-card>"
                },] },
    ];
    ClaimDetailComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: TranslationProvider, },
        { type: DocumentsProvider, },
        { type: FormatProvider, },
        { type: ComponentSettingsProvider, },
        { type: FormBuilder, },
        { type: IncidencesAndClaimsProvider, },
    ]; };
    ClaimDetailComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'insurance': [{ type: Input },],
        'size': [{ type: Input },],
    };
    return ClaimDetailComponent;
}());
export { ClaimDetailComponent };
//# sourceMappingURL=claim-detail.js.map