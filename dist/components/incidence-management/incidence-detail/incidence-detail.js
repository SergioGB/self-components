import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { Events } from 'ionic-angular';
import { FormatProvider } from '../../../providers/format/format';
import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { IncidencesAndClaimsProvider } from '../../../providers/incidences-and-claims/incidences-and-claims';
import { Mocks } from '../../../providers/mocks/mocks';
var IncidenceDetailComponent = (function () {
    function IncidenceDetailComponent(events, translationProvider, documentsProvider, formatProvider, componentSettingsProvider, formBuilder, incidencesAndClaimsProvider) {
        this.events = events;
        this.translationProvider = translationProvider;
        this.documentsProvider = documentsProvider;
        this.formatProvider = formatProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.formBuilder = formBuilder;
        this.incidencesAndClaimsProvider = incidencesAndClaimsProvider;
        this.translationProvider.bind(this);
        this.formatDate = this.formatProvider.formatDate;
        this.formatCost = this.formatProvider.formatCost;
    }
    IncidenceDetailComponent.prototype.ngOnInit = function () {
        this.additionalDataForm = this.formBuilder.group({
            additionalData: ['', Validators.required]
        });
        if (this.mocked) {
            this.incidenceData = Mocks.getIncidence();
            this.componentReady = true;
        }
        else {
        }
    };
    IncidenceDetailComponent.prototype.sendAdditionalData = function () {
        this.additionalDataForm.reset();
    };
    IncidenceDetailComponent.prototype.attemptSubmission = function () {
        if (this.mocked) {
            this.redirectToConfirmation();
        }
        else {
            this.postDocuments();
        }
    };
    IncidenceDetailComponent.prototype.postDocuments = function () {
        var _this = this;
        var documentForm = new FormData();
        documentForm.append('upfile', this.incidenceData.associated_documents[0]);
        documentForm.append('object', 'COMPLAINT');
        documentForm.append('object_id', this.responseForm);
        this.incidencesAndClaimsProvider.postDocuments(this.mocked, documentForm).subscribe(function (response) {
            var mapfreResponseDoc = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponseDoc)) {
                _this.redirectToConfirmation();
            }
        });
    };
    IncidenceDetailComponent.prototype.redirectToConfirmation = function () {
        this.events.publish('incidence-detail::goConfirmIncidence');
    };
    IncidenceDetailComponent.inputs = ['insurance', 'size'];
    IncidenceDetailComponent.configInputs = ['mocked', 'styles'];
    IncidenceDetailComponent.decorators = [
        { type: Component, args: [{
                    selector: 'incidence-detail',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">   <form [formGroup]=\"additionalDataForm\" (ngSubmit)=\"sendAdditionalData()\">     <loading-spinner *ngIf=\"!componentReady\"></loading-spinner>     <div *ngIf=\"componentReady\">       <ion-card-header class=\"no-border\">         <ion-item>           <h3 class=\"primary\">             {{ translate('incidence-detail.my_incidences.incidence_number') }} {{ incidenceData.id }}           </h3>          </ion-item>       </ion-card-header>        <div>         <ion-item class=\"sub-title\">           <h4>             {{ translate('incidence-detail.my_incidences.data.title') }}           </h4>         </ion-item>          <ion-card-content class=\"no-padding-bottom\">           <ion-item no-padding>             <ion-grid no-padding class=\"values-list\">               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.name') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.name }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.surname1') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.surname1 }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.surname2') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.surname2 }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.identity_document') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.identification_number }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.address') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.address }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.phone_number') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.phone_number }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.email') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.email }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.product') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.product }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.contract_number') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.contract_number }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.reason') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.reason }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ translate('incidence-detail.my_incidences.data.reason_detail') }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ incidenceData.reason_detail }}                   </span>                 </ion-col>               </ion-row>               <hr>             </ion-grid>             <hr>           </ion-item>           <div>             <ion-item class=\"text-area margin-bottom-m\">                <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\" formControlName=\"additionalData\"                 [placeholder]=\"translate('incidence-detail.loss_detail.more_data')\">               </ion-textarea>              </ion-item>           </div>         </ion-card-content>       </div>        <ion-item class=\"sub-title\">         <h4>           {{ translate('incidence-detail.my_incidences.docs.title') }}         </h4>       </ion-item>        <ion-card-content class=\"padding-top-m\">          <file-uploader [allowedTypes]=\"{ pdf: true, image: false, video: false }\"           [files]=\"incidenceData.associated_documents\">         </file-uploader>          <ion-grid no-padding class=\"margin-top-m\">           <ion-row>             <ion-col col-12 col-sm-3 col-lg-2 class=\"margin-left-auto\">               <button full text-uppercase (click)=\"attemptSubmission()\" ion-button>                 {{ translate('incidence-detail.claim_form.confirm_button') }}               </button>             </ion-col>           </ion-row>         </ion-grid>        </ion-card-content>      </div>   </form> </ion-card>"
                },] },
    ];
    IncidenceDetailComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: TranslationProvider, },
        { type: DocumentsProvider, },
        { type: FormatProvider, },
        { type: ComponentSettingsProvider, },
        { type: FormBuilder, },
        { type: IncidencesAndClaimsProvider, },
    ]; };
    IncidenceDetailComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'insurance': [{ type: Input },],
        'size': [{ type: Input },],
    };
    return IncidenceDetailComponent;
}());
export { IncidenceDetailComponent };
//# sourceMappingURL=incidence-detail.js.map