import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { Mocks } from '../../../providers/mocks/mocks';
import { FormatProvider } from '../../../providers/format/format';
import { TranslationProvider } from '../../../providers/translation/translation';
var MyClaimsComponent = (function () {
    function MyClaimsComponent(events, translationProvider, formatProvider) {
        this.events = events;
        this.translationProvider = translationProvider;
        this.formatProvider = formatProvider;
        this.translationProvider.bind(this);
    }
    MyClaimsComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.mockData();
            this.componentReady = true;
        }
        else {
        }
    };
    MyClaimsComponent.prototype.formatDate = function (date) {
        return this.formatProvider.formatDate(date);
    };
    MyClaimsComponent.prototype.redirectToClaimDetail = function () {
        this.events.publish('my-claims::goClaimDetail');
    };
    MyClaimsComponent.prototype.mockData = function () {
        this.claims = Mocks.getClaims();
        this.sortableProperties = [{
                id: '1',
                field: 'Fecha de apertura'
            }, {
                id: '2',
                field: 'Estado'
            }];
        this.selectedSort = this.sortableProperties[0];
    };
    MyClaimsComponent.inputs = ['insurance', 'size'];
    MyClaimsComponent.configInputs = ['mocked', 'styles'];
    MyClaimsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'my-claims',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["my-claims ion-col {   padding: none !important; }"],
                    template: "<ion-card [ngStyle]=\"styles\">   <loading-spinner *ngIf=\"!componentReady\"></loading-spinner>    <div *ngIf=\"componentReady\">     <ion-card-header class=\"no-border no-padding-bottom\">       <ion-grid no-padding>         <ion-row>           <ion-col col-12 col-sm-8 col-md-9>             <ion-item no-padding>               <h3 class=\"primary\">                 {{ 'Mis quejas' }}               </h3>             </ion-item>           </ion-col>            <ion-col col-12 col-sm-4 col-md-3>             <div class=\"card-header-select\">               <h4 class=\"select-label\">                 <strong>                   {{ 'Ordenar por:' }}                 </strong>               </h4>                <ion-select [(ngModel)]=\"selectedSort\" full>                 <ion-option *ngFor=\"let sort of sortableProperties\" [value]=\"sort\">                   {{ sort.field }}                 </ion-option>               </ion-select>             </div>           </ion-col>         </ion-row>       </ion-grid>     </ion-card-header>      <div>       <ion-card-content class=\"no-padding-top\">         <ion-item no-padding>           <ion-grid no-padding class=\"values-list\">             <div *ngFor=\"let claim of claims\">               <hr>               <ion-row>                 <ion-col no-padding col-12 col-sm-10>                   <strong class=\"primary-text\">                     {{ 'N\u00BA Queja' }} {{ claim.id }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-2>                   <a href=\"#\" (click)=\"redirectToClaimDetail()\" class=\"primary-text\">                     {{ 'VER DETALLE' }}                   </a>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claim.type }}                   </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ 'Estado' }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                     <span class=\"secondary-text\">                       {{ claim.status }}                     </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ 'Fecha de apertura' }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                   <span class=\"secondary-text\">                     {{ claim.open_date }}                   </span>                 </ion-col>               </ion-row>             </div>           </ion-grid>         </ion-item>       </ion-card-content>     </div>    </div>  </ion-card>"
                },] },
    ];
    MyClaimsComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: TranslationProvider, },
        { type: FormatProvider, },
    ]; };
    MyClaimsComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'insurance': [{ type: Input },],
        'size': [{ type: Input },],
    };
    return MyClaimsComponent;
}());
export { MyClaimsComponent };
//# sourceMappingURL=my-claims.js.map