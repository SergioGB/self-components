import { OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { FormatProvider } from '../../../providers/format/format';
import { TranslationProvider } from '../../../providers/translation/translation';
import { Insurance } from '../../../models/insurance';
export declare class MyIncidencesComponent implements OnInit {
    private events;
    private translationProvider;
    private formatProvider;
    mocked?: boolean;
    styles?: any;
    insurance: Insurance;
    size: number;
    componentReady: boolean;
    incidences: any[];
    sortableProperties: any[];
    selectedSort: any[];
    static inputs: string[];
    static configInputs: string[];
    constructor(events: Events, translationProvider: TranslationProvider, formatProvider: FormatProvider);
    ngOnInit(): void;
    formatDate(date: Date): string;
    redirectToIncidentDetail(): void;
    private mockData;
}
