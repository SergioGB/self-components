import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { Mocks } from '../../../providers/mocks/mocks';
import { FormatProvider } from '../../../providers/format/format';
import { TranslationProvider } from '../../../providers/translation/translation';
var MyIncidencesComponent = (function () {
    function MyIncidencesComponent(events, translationProvider, formatProvider) {
        this.events = events;
        this.translationProvider = translationProvider;
        this.formatProvider = formatProvider;
        this.translationProvider.bind(this);
    }
    MyIncidencesComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.mockData();
            this.componentReady = true;
        }
        else {
        }
    };
    MyIncidencesComponent.prototype.formatDate = function (date) {
        return this.formatProvider.formatDate(date);
    };
    MyIncidencesComponent.prototype.redirectToIncidentDetail = function () {
        this.events.publish('my-incidences::goInsuranceDetail');
    };
    MyIncidencesComponent.prototype.mockData = function () {
        this.incidences = Mocks.getIncidences();
        this.sortableProperties = [{
                id: '1',
                field: 'Fecha de apertura'
            }, {
                id: '2',
                field: 'Estado'
            }];
        this.selectedSort = this.sortableProperties[0];
    };
    MyIncidencesComponent.inputs = ['insurance', 'size'];
    MyIncidencesComponent.configInputs = ['mocked', 'styles'];
    MyIncidencesComponent.decorators = [
        { type: Component, args: [{
                    selector: 'my-incidences',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["my-incidences ion-col {   padding: none !important; }  my-incidences ion-select {   width: 100%; }"],
                    template: "<ion-card [ngStyle]=\"styles\">   <loading-spinner *ngIf=\"!componentReady\"></loading-spinner>    <div *ngIf=\"componentReady\">     <ion-card-header class=\"no-border no-padding-bottom\">       <ion-grid no-padding>         <ion-row>           <ion-col col-12 col-sm-8 col-md-9>             <ion-item no-padding>               <h3 class=\"primary\">                 {{ 'Mis incidencias' }}               </h3>             </ion-item>           </ion-col>            <ion-col col-12 col-sm-4 col-md-3>             <div class=\"card-header-select\">               <h4 class=\"select-label\">                 <strong>                   {{ 'Ordenar por:' }}                 </strong>               </h4>                <ion-select [(ngModel)]=\"selectedSort\" full>                 <ion-option *ngFor=\"let sort of sortableProperties\" [value]=\"sort\">                   {{ sort.field }}                 </ion-option>               </ion-select>             </div>           </ion-col>         </ion-row>       </ion-grid>     </ion-card-header>      <div>       <ion-card-content class=\"no-padding-top\">         <ion-item no-padding>           <ion-grid no-padding class=\"values-list\">             <div *ngFor=\"let incidence of incidences\">               <hr>               <ion-row>                 <ion-col no-padding col-12 col-sm-10>                   <strong class=\"primary-text\">                     {{ 'N\u00BA Incidencia' }} {{ incidence.id }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-2>                   <a href=\"#\" (click)=\"redirectToIncidentDetail()\" class=\"primary-text\">                     {{ 'VER DETALLE' }}                   </a>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-9>                     <span class=\"secondary-text\">                       {{ incidence.type }}                     </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ 'Estado' }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                       <span class=\"secondary-text\">                         {{ incidence.status }}                       </span>                 </ion-col>               </ion-row>               <ion-row>                 <ion-col col-12 col-sm-3>                   <strong class=\"primary-text\">                     {{ 'Fecha de apertura' }}                   </strong>                 </ion-col>                 <ion-col col-12 col-sm-9>                       <span class=\"secondary-text\">                         {{ incidence.open_date }}                       </span>                 </ion-col>               </ion-row>             </div>           </ion-grid>         </ion-item>       </ion-card-content>     </div>    </div>  </ion-card>"
                },] },
    ];
    MyIncidencesComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: TranslationProvider, },
        { type: FormatProvider, },
    ]; };
    MyIncidencesComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'insurance': [{ type: Input },],
        'size': [{ type: Input },],
    };
    return MyIncidencesComponent;
}());
export { MyIncidencesComponent };
//# sourceMappingURL=my-incidences.js.map