import { OnInit } from '@angular/core';
import { IncidencesAndClaimsProvider } from '../../../providers/incidences-and-claims/incidences-and-claims';
export declare class IncidencesAndClaimsCardsComponent implements OnInit {
    private incidencesAndClaimsProvider;
    mocked?: boolean;
    styles?: any;
    isModeSmall?: boolean;
    buttons: any[];
    componentReady: boolean;
    index: number;
    static configInputs: string[];
    constructor(incidencesAndClaimsProvider: IncidencesAndClaimsProvider);
    ngOnInit(): void;
    private queryData;
}
