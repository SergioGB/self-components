import { OnInit } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { FormatProvider } from '../../../providers/format/format';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { ContentTab } from '../../../models/content-tab';
export declare class IncidentHeaderComponent implements OnInit {
    private translationProvider;
    private formatProvider;
    private insuranceProvider;
    private navCtrl;
    private events;
    mocked?: boolean;
    styles?: any;
    riskId: string;
    incidentId: string;
    headerTabs: ContentTab[];
    headerData: any;
    ready: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, formatProvider: FormatProvider, insuranceProvider: InsuranceProvider, navCtrl: NavController, events: Events);
    ngOnInit(): void;
    private queryData;
    private loadHeaderData;
    private goToInsuranceDetail;
}
