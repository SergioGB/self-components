import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { TranslationProvider } from '../../../providers/translation/translation';
import { FormatProvider } from '../../../providers/format/format';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { PageHeaderComponent } from '../../page-header/page-header';
var IncidentHeaderComponent = (function () {
    function IncidentHeaderComponent(translationProvider, formatProvider, insuranceProvider, navCtrl, events) {
        this.translationProvider = translationProvider;
        this.formatProvider = formatProvider;
        this.insuranceProvider = insuranceProvider;
        this.navCtrl = navCtrl;
        this.events = events;
        this.translationProvider.bind(this);
    }
    IncidentHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.mocked) {
            this.riskId = "1";
            this.incidentId = "1";
        }
        this.queryData();
        this.events.subscribe(PageHeaderComponent.backClickedEvent, function () {
            _this.goToInsuranceDetail();
        });
    };
    IncidentHeaderComponent.prototype.queryData = function () {
        var _this = this;
        Observable.forkJoin([
            this.insuranceProvider.getInsuranceDetailSettings(this.mocked, this.riskId),
            this.insuranceProvider.getClaimDetailSettings(this.mocked, this.incidentId)
        ]).subscribe(function (responses) {
            if (_this.insuranceProvider.areResponsesValid(responses)) {
                var risk = _this.insuranceProvider.getResponseJSON(responses[0]).data;
                var incident = _this.insuranceProvider.getResponseJSON(responses[1]).data;
                _this.loadHeaderData(risk, incident);
            }
        });
    };
    IncidentHeaderComponent.prototype.loadHeaderData = function (risk, incident) {
        this.headerData = {
            title: this.translationProvider.getValueForKey('incident-header.incident_header.title') + " " + incident.number,
            description: incident.detail,
            rows: [{
                    key: 'incident_header.risk',
                    value: risk.name
                }, {
                    key: 'incident_header.state',
                    value: incident.state
                }, {
                    key: 'incident_header.date',
                    value: this.formatProvider.formatDate(incident.opening_date)
                }]
        };
        this.ready = true;
    };
    IncidentHeaderComponent.prototype.goToInsuranceDetail = function () {
        this.navCtrl.push('InsuranceDetailPage', { insuranceNumber: this.riskId, incidents: true });
    };
    IncidentHeaderComponent.inputs = ['riskId', 'incidentId'];
    IncidentHeaderComponent.configInputs = ['mocked', 'styles', 'headerTabs'];
    IncidentHeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'incident-header',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<page-header   *ngIf=\"ready\"   [data]=\"headerData\"   [tabs]=\"headerTabs\"   [backButtonAvailable]=\"true\"   [backgroundUrl]=\"'../assets/images/non-scalable/background/claims/img_seguimiento_siniestro.jpg'\"   [ngStyle]=\"styles\"> </page-header>"
                },] },
    ];
    IncidentHeaderComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: FormatProvider, },
        { type: InsuranceProvider, },
        { type: NavController, },
        { type: Events, },
    ]; };
    IncidentHeaderComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'incidentId': [{ type: Input },],
        'headerTabs': [{ type: Input },],
    };
    return IncidentHeaderComponent;
}());
export { IncidentHeaderComponent };
//# sourceMappingURL=incident-header.js.map