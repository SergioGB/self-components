import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events, NavController } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { NavigationProvider } from '../../providers/navigation/navigation';
var InformationComponent = (function () {
    function InformationComponent(formBuilder, events, navigationProvider, navCtrl, translationProvider) {
        this.formBuilder = formBuilder;
        this.events = events;
        this.navigationProvider = navigationProvider;
        this.navCtrl = navCtrl;
        this.translationProvider = translationProvider;
        this.defaultIcons = ["mapfre-earphones"];
        this.contactPhone = '';
        this.translationProvider.bind(this);
    }
    InformationComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.mocked) {
            this.icons.push('mapfre-check');
            this.title = 'Éxito';
            this.subtitle = 'La operación se ha realizado correctamente.';
            this.showContactWithUs = false;
        }
        if (this.formCallMe) {
            this.callMeForm = this.formBuilder.group({
                phoneNumber: [this.contactPhone, Validators.required]
            });
            this.callMeForm.valueChanges.subscribe(function (val) {
                _this.events.publish(InformationComponent.phoneChangeEvent, { valid: _this.callMeForm.valid, phoneNumber: val.phoneNumber });
            });
            this.events.publish(InformationComponent.phoneChangeEvent, { valid: this.callMeForm.valid, phoneNumber: this.contactPhone });
        }
    };
    InformationComponent.prototype.outgoingCall = function () {
        var number = {
            href: 'tel:',
            target: '_blank'
        };
        this.navigationProvider.navigateTo(this.navCtrl, null, number);
    };
    InformationComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    InformationComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    InformationComponent.componentName = 'information';
    InformationComponent.phoneChangeEvent = InformationComponent.componentName + ":phoneChange";
    InformationComponent.inputs = ['icon', 'title', 'subtitle', 'subtitle2', 'newLineSubtitle', 'varSubtitle', 'amount'];
    InformationComponent.configInputs = ['mocked', 'icons', 'styles', 'formCallMe', 'showContactWithUs'];
    InformationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'information',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["information .circular-button {   border-radius: 50%;   color: white;   background: #d81e05;   padding: 10px; }  information ion-item {   background: transparent !important; }   information ion-item > .item-inner {     border: none !important; }"],
                    template: "<div class=\"modal-content bottom-border border-only\" text-center [ngStyle]=\"styles\">   <ion-icon [name]=\"icon ? icon : getIcon(0)\" class=\"modal-icon\" [hidden]=\"!icon\"></ion-icon>    <h3 class=\"modal-title\">     {{ title }}   </h3>    <ion-grid no-padding class=\"margin-top-m\">     <ion-row>       <ion-col col-12 col-md-8 push-md-2>         <div class=\"modal-description\">           {{ subtitle }} <strong *ngIf=\"varSubtitle\">{{ varSubtitle }}</strong>           <strong *ngIf=\"amount\">{{ amount.amount | currency:amount.iso_code:true:'1.2-2' }}</strong>           <span *ngIf=\"subtitle2\">{{ subtitle2 }}</span>           <p *ngIf=\"newLineSubtitle\">{{ newLineSubtitle }}</p>         </div>       </ion-col>     </ion-row>   </ion-grid>    <form *ngIf=\"formCallMe\" [formGroup]=\"callMeForm\">     <ion-row margin-bottom class=\"margin-top-m\">       <ion-col col-12 col-sm-4 push-sm-4>         <ion-item no-padding class=\"modal-form-content\">           <ion-label floating>             {{ translate('information.call_me_innactivity_modal.mobile_phone') }}           </ion-label>           <ion-input             formControlName=\"phoneNumber\"             type=\"tel\"             maxlength=\"20\"             pattern=\"(\\\\+[0-9]*)?[0-9]*\">           </ion-input>         </ion-item>       </ion-col>     </ion-row>   </form>    <div class=\"inline-block margin-auto margin-top-xl\" *ngIf=\"showContactWithUs\">     <ion-item class=\"no-padding-left bottom-border border-only clickable\" (click)=\"outgoingCall()\" tabindex=\"0\">       <ion-icon item-start large class=\"fmm-primary-text circular-button\" [name]=\"getIcon(1)\"></ion-icon>       <p class=\"fmm-body1\">         {{ translate('information.pending_task.assistance.cancel.call_to') }}       </p>     </ion-item>   </div> </div>"
                },] },
    ];
    InformationComponent.ctorParameters = function () { return [
        { type: FormBuilder, },
        { type: Events, },
        { type: NavigationProvider, },
        { type: NavController, },
        { type: TranslationProvider, },
    ]; };
    InformationComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icon': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'title': [{ type: Input },],
        'subtitle': [{ type: Input },],
        'subtitle2': [{ type: Input },],
        'newLineSubtitle': [{ type: Input },],
        'varSubtitle': [{ type: Input },],
        'amount': [{ type: Input },],
        'formCallMe': [{ type: Input },],
        'showContactWithUs': [{ type: Input },],
    };
    return InformationComponent;
}());
export { InformationComponent };
//# sourceMappingURL=information.js.map