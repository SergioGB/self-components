import { ModalController, NavController } from 'ionic-angular';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { TranslationProvider } from '../../../providers/translation/translation';
import { PolicyCancellationProvider } from '../../../providers/policy-cancellation/policy-cancellation';
export declare class CancelPolicyQuestionComponent {
    private modalCtrl;
    private insuranceProvider;
    private translationProvider;
    private policyCancellationProvider;
    private navController;
    mocked?: boolean;
    modals?: any;
    icons?: string[];
    styles?: any;
    riskId: string;
    policyId: string;
    cancellationAllowed: boolean;
    defaultIcons: string[];
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(modalCtrl: ModalController, insuranceProvider: InsuranceProvider, translationProvider: TranslationProvider, policyCancellationProvider: PolicyCancellationProvider, navController: NavController);
    ngOnInit(): void;
    presentCancelPolicyModal(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
