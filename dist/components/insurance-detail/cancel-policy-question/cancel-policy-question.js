import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InformationModalComponent } from '../../_modals/information-modal/information-modal';
import { ProcessFailureModalComponent } from '../../_modals/process-failure-modal/process-failure-modal';
import { PolicyCancellationProvider } from '../../../providers/policy-cancellation/policy-cancellation';
var CancelPolicyQuestionComponent = (function () {
    function CancelPolicyQuestionComponent(modalCtrl, insuranceProvider, translationProvider, policyCancellationProvider, navController) {
        this.modalCtrl = modalCtrl;
        this.insuranceProvider = insuranceProvider;
        this.translationProvider = translationProvider;
        this.policyCancellationProvider = policyCancellationProvider;
        this.navController = navController;
        this.defaultIcons = ["arbmapfre-arrow-rightol"];
        this.translationProvider.bind(this);
    }
    CancelPolicyQuestionComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.policyId) {
            this.insuranceProvider.getPolicyDetailSettings(this.mocked, this.policyId).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                    _this.cancellationAllowed = mapfreResponse.data.cancellation_allowed;
                }
            });
        }
    };
    CancelPolicyQuestionComponent.prototype.presentCancelPolicyModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(InformationModalComponent, {
            mocked: this.mocked,
            icon: 'mapfre-warning',
            labels: {
                cancel: "modal.cancel-policy-question.information-modal.generic.cancel",
                finish: "modal.cancel-policy-question.information-modal.generic.finish",
                accept: "modal.cancel-policy-question.information-modal.generic.accept",
                title: 'modal.cancel-policy-question.information-modal.cancelation_process.modal_cancelation.title',
                subtitle: 'modal.cancel-policy-question.information-modal.cancelation_process.modal_cancelation.description',
            }
        });
        modal.onDidDismiss(function (value) {
            if (value) {
                _this.policyCancellationProvider.resetFlowData();
                _this.navController.push('PolicyCancellationStepsPage', { stepId: 1, riskId: _this.riskId, policyId: _this.policyId }, { animate: false });
            }
            else {
                _this.modalCtrl.create(ProcessFailureModalComponent, {
                    mocked: _this.mocked,
                    settings: {
                        icon: 'mapfre-cross-error',
                        title: 'modal.cancel-policy-question.cancel_policy.result.failure.title',
                        description: 'modal.cancel-policy-question.cancel_policy.result.failure.description',
                        isModal: true
                    }
                }).present();
            }
        });
        modal.present();
    };
    CancelPolicyQuestionComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    CancelPolicyQuestionComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    CancelPolicyQuestionComponent.inputs = ['riskId', 'policyId'];
    CancelPolicyQuestionComponent.configInputs = ['mocked', 'icons', 'styles', 'modals'];
    CancelPolicyQuestionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'cancel-policy-question',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"cancellationAllowed\" tabindex=\"0\" class=\"clickable\" (click)=\"presentCancelPolicyModal()\" [ngStyle]=\"styles\">    <ion-card-header class=\"no-border\">     <ion-item>       <h3>         {{ translate('policy.cancel-policy-question.cancel_policy.title') }}       </h3>        <h4 class=\"fmm-body1 grey1\">         {{ translate('policy.cancel-policy-question.cancel_policy.subtitle') }}       </h4>        <ion-icon item-end no-margin [name]=\"getIcon(0)\" class=\"primary\"></ion-icon>     </ion-item>   </ion-card-header>  </ion-card>"
                },] },
    ];
    CancelPolicyQuestionComponent.ctorParameters = function () { return [
        { type: ModalController, },
        { type: InsuranceProvider, },
        { type: TranslationProvider, },
        { type: PolicyCancellationProvider, },
        { type: NavController, },
    ]; };
    CancelPolicyQuestionComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'modals': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'policyId': [{ type: Input },],
    };
    return CancelPolicyQuestionComponent;
}());
export { CancelPolicyQuestionComponent };
//# sourceMappingURL=cancel-policy-question.js.map