import { ElementRef } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { TransferProvider } from '../../../../../providers/transfer/transfer';
import { FileInsuranceProvider } from '../../../../../providers/file-insurance/file-insurance';
import { InsuranceProvider } from '../../../../../providers/insurance/insurance';
import { DocumentsProvider } from '../../../../../providers/documents/documents';
import { TranslationProvider } from '../../../../../providers/translation/translation';
export declare class ProceedingDocumentationListComponent {
    private transferProvider;
    private fileInsurance;
    private modalCtrl;
    private elementRef;
    private insuranceProvider;
    private documentsProvider;
    private translationProvider;
    myFile: any;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    riskId: string;
    proceeding: any;
    modals: any;
    defaultIcons: string[];
    componentReady: boolean;
    fullHeight: number;
    collapsedHeight: number;
    collapsed: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(transferProvider: TransferProvider, fileInsurance: FileInsuranceProvider, modalCtrl: ModalController, elementRef: ElementRef, insuranceProvider: InsuranceProvider, documentsProvider: DocumentsProvider, translationProvider: TranslationProvider);
    ngOnInit(): void;
    openAdhocForm(file: any): void;
    download(file: any): void;
    generate(file: any): void;
    toggle(): void;
    storeHeights(): void;
    getProperHeight(): string;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
