import { OnDestroy, OnInit } from '@angular/core';
import { Events, ModalController, NavController } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class EmptySubmitIncidenceComponent implements OnInit, OnDestroy {
    private navCtrl;
    private translation;
    private modalController;
    private events;
    emptyLosses: boolean;
    emptyAssistances: boolean;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    modals?: any[];
    defaultIcons: string[];
    static configInputs: string[];
    constructor(navCtrl: NavController, translation: TranslationProvider, modalController: ModalController, events: Events);
    ngOnInit(): void;
    ngOnDestroy(): void;
    submitIncidence(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
