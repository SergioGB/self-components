import { ElementRef } from '@angular/core';
import { FileInsuranceProvider } from '../../../providers/file-insurance/file-insurance';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalController } from 'ionic-angular';
export declare class InsuranceDocumentationListComponent {
    private fileInsurance;
    modalCtrl: ModalController;
    private elementRef;
    private insuranceProvider;
    private documentsProvider;
    private translationProvider;
    mocked?: boolean;
    modals?: any;
    styles?: any;
    riskId: string;
    icons: string[];
    policyIds: string[];
    static inputs: string[];
    static configInputs: string[];
    constructor(fileInsurance: FileInsuranceProvider, modalCtrl: ModalController, elementRef: ElementRef, insuranceProvider: InsuranceProvider, documentsProvider: DocumentsProvider, translationProvider: TranslationProvider);
    ngOnInit(): void;
}
