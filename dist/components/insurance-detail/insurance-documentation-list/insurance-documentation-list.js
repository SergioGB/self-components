import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef, Input } from '@angular/core';
import { FileInsuranceProvider } from '../../../providers/file-insurance/file-insurance';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ModalController } from 'ionic-angular';
var InsuranceDocumentationListComponent = (function () {
    function InsuranceDocumentationListComponent(fileInsurance, modalCtrl, elementRef, insuranceProvider, documentsProvider, translationProvider) {
        this.fileInsurance = fileInsurance;
        this.modalCtrl = modalCtrl;
        this.elementRef = elementRef;
        this.insuranceProvider = insuranceProvider;
        this.documentsProvider = documentsProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
    }
    InsuranceDocumentationListComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.mocked) {
            this.riskId = "1";
        }
        if (this.riskId) {
            this.insuranceProvider.getInsuranceDetailSettings(this.mocked, this.riskId).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    var insurance = mapfreResponse.data;
                    if (insurance.associated_policies && insurance.associated_policies.length > 0) {
                        _this.policyIds = [];
                        for (var _i = 0, _a = insurance.associated_policies; _i < _a.length; _i++) {
                            var policy = _a[_i];
                            _this.policyIds.push(policy.id);
                        }
                    }
                }
            });
        }
    };
    InsuranceDocumentationListComponent.inputs = ['riskId'];
    InsuranceDocumentationListComponent.configInputs = ['mocked', 'styles', 'modals', 'icons'];
    InsuranceDocumentationListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'insurance-documentation-list',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div *ngFor=\"let policyId of policyIds\" [ngStyle]=\"styles\">   <documentation-list [mocked]=\"mocked\" [icons]=\"icons\" [riskId]=\"riskId\" [policyId]=\"policyId\" [owner]=\"'MAPFRE'\"></documentation-list>   <documentation-list [mocked]=\"mocked\" [icons]=\"icons\" [riskId]=\"riskId\" [policyId]=\"policyId\" [owner]=\"'CLIENT'\"></documentation-list> </div>"
                },] },
    ];
    InsuranceDocumentationListComponent.ctorParameters = function () { return [
        { type: FileInsuranceProvider, },
        { type: ModalController, },
        { type: ElementRef, },
        { type: InsuranceProvider, },
        { type: DocumentsProvider, },
        { type: TranslationProvider, },
    ]; };
    InsuranceDocumentationListComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'modals': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'icons': [{ type: Input },],
    };
    return InsuranceDocumentationListComponent;
}());
export { InsuranceDocumentationListComponent };
//# sourceMappingURL=insurance-documentation-list.js.map