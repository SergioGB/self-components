import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { ContentTab } from '../../../models/content-tab';
export declare class InsuranceHeaderComponent implements OnInit {
    private translationProvider;
    private insuranceProvider;
    mocked?: boolean;
    styles?: any;
    riskId: string;
    headerTabs: ContentTab[];
    defaultTab: ContentTab;
    private carInsuranceType;
    private homeInsuranceType;
    headerData: any;
    ready: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, insuranceProvider: InsuranceProvider);
    ngOnInit(): void;
    private queryData;
    private generateTabs;
    private generateCarTabs;
    private generateHomeTabs;
}
