import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
var InsuranceHeaderComponent = (function () {
    function InsuranceHeaderComponent(translationProvider, insuranceProvider) {
        this.translationProvider = translationProvider;
        this.insuranceProvider = insuranceProvider;
        this.carInsuranceType = 'C';
        this.homeInsuranceType = 'H';
        this.translationProvider.bind(this);
    }
    InsuranceHeaderComponent.prototype.ngOnInit = function () {
        this.queryData();
    };
    InsuranceHeaderComponent.prototype.queryData = function () {
        var _this = this;
        if (this.riskId) {
            this.insuranceProvider.getInsuranceDetailSettings(this.mocked, this.riskId).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    var insurance_1 = mapfreResponse.data;
                    if (insurance_1.associated_policies && insurance_1.associated_policies.length > 0) {
                        _this.insuranceProvider.getPolicyDetailSettings(_this.mocked, insurance_1.associated_policies[0].id).subscribe(function (response) {
                            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                            if (mapfreResponse) {
                                var policy = mapfreResponse.data;
                                _this.generateTabs(insurance_1, policy);
                            }
                            _this.ready = true;
                        });
                    }
                    else {
                        _this.ready = true;
                    }
                }
            });
        }
    };
    InsuranceHeaderComponent.prototype.generateTabs = function (insurance, policy) {
        this.headerData = {
            title: insurance.name,
            rows: []
        };
        if (insurance.type === this.carInsuranceType) {
            this.generateCarTabs(policy.car_policy_detail.vehicle_info);
        }
        else if (insurance.type === this.homeInsuranceType) {
            this.generateHomeTabs(policy.home_policy_detail.home_info);
        }
    };
    InsuranceHeaderComponent.prototype.generateCarTabs = function (policyData) {
        this.headerData.rows.push({
            key: 'insurance_detail.insurance_header.risk',
            value: this.translationProvider.getValueForKey('insurance-header.insurance_detail.insurance_header.car')
        });
        this.headerData.rows.push({
            key: 'risk_detail.car.vehicle_registration',
            value: policyData.registration.field_content
        });
        this.headerData.rows.push({
            key: 'risk_detail.car.vehicle_type',
            value: policyData.type.field_content
        });
    };
    InsuranceHeaderComponent.prototype.generateHomeTabs = function (policyData) {
        this.headerData.rows.push({
            key: 'insurance_detail.insurance_header.risk',
            value: this.translationProvider.getValueForKey('insurance-header.insurance_detail.insurance_header.home')
        });
        this.headerData.rows.push({
            key: 'risk_detail.home.regime',
            value: policyData.regime.field_content
        });
        this.headerData.rows.push({
            key: 'risk_detail.home.use',
            value: policyData.use.field_content
        });
    };
    InsuranceHeaderComponent.inputs = ['riskId', 'defaultTab'];
    InsuranceHeaderComponent.configInputs = ['mocked', 'styles', 'headerTabs'];
    InsuranceHeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'insurance-header',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<page-header   *ngIf=\"ready\"   [data]=\"headerData\"   [tabs]=\"headerTabs\"   [backgroundUrl]=\"'../assets/images/non-scalable/background/policy-detail/img_poliza.jpg'\"   [ngStyle]=\"styles\"> </page-header>"
                },] },
    ];
    InsuranceHeaderComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: InsuranceProvider, },
    ]; };
    InsuranceHeaderComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'headerTabs': [{ type: Input },],
        'defaultTab': [{ type: Input },],
    };
    return InsuranceHeaderComponent;
}());
export { InsuranceHeaderComponent };
//# sourceMappingURL=insurance-header.js.map