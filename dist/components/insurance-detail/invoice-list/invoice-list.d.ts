import { ElementRef } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { TranslationProvider } from '../../../providers/translation/translation';
import { TransferProvider } from '../../../providers/transfer/transfer';
import { FormatProvider } from '../../../providers/format/format';
import { ReceiptPaymentProvider } from '../../../providers/receipt-payment/receipt-payment';
import { TooltipProvider } from '../../../providers/tooltip/tooltip';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { PlatformProvider } from '../../../providers/platform/platform';
export declare class InvoiceListComponent {
    private navCtrl;
    private elementRef;
    private popoverCtrl;
    private insuranceProvider;
    private documentsProvider;
    private translationProvider;
    private platformProvider;
    private transferProvider;
    private tooltipProvider;
    private formatProvider;
    private receiptPaymentProvider;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    riskId: string;
    policyId: string;
    defaultIcons: string[];
    invoices: any[];
    collapsable: boolean;
    invoicesGroupsVisible: boolean[];
    popover: any;
    isMobile: boolean;
    ready: boolean;
    types: any[];
    formatDate: Function;
    selectedReceipt: any;
    page: number;
    pageSize: number;
    totalReceipts: number;
    moreInvoicesAvailable: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(navCtrl: NavController, elementRef: ElementRef, popoverCtrl: PopoverController, insuranceProvider: InsuranceProvider, documentsProvider: DocumentsProvider, translationProvider: TranslationProvider, platformProvider: PlatformProvider, transferProvider: TransferProvider, tooltipProvider: TooltipProvider, formatProvider: FormatProvider, receiptPaymentProvider: ReceiptPaymentProvider, events: Events);
    ngOnInit(): void;
    ngOnDestroy(): void;
    showMore(): void;
    toggleOptions(index: number): void;
    toggleInvoice(invoice: any): void;
    downloadFile(documentId: number): void;
    presentPopover(myEvent: Event, receipt: any): void;
    dismissPopoverTooltip(myEvent: Event): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
