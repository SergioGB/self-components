import { ElementRef, OnInit } from '@angular/core';
import { ViewController, Events, NavParams } from 'ionic-angular';
import { ModalProvider } from '../../../../providers/modal/modal';
export declare class LossDetailModal implements OnInit {
    private viewCtrl;
    private elementRef;
    private navParams;
    private modalProvider;
    private events;
    lossId?: any;
    constructor(viewCtrl: ViewController, elementRef: ElementRef, navParams: NavParams, modalProvider: ModalProvider, events: Events);
    ngAfterViewInit(): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
    dismiss(): void;
}
