import { OnInit, EventEmitter } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
export declare class LossListComponent implements OnInit {
    private insuranceProvider;
    private translationProvider;
    private events;
    mocked?: boolean;
    styles?: any;
    riskId: string;
    modals: any;
    icons: string[];
    onEmptyLoss: EventEmitter<boolean>;
    ready: boolean;
    losses: any;
    insuranceType: any;
    static componentName: string;
    static loadEvent: string;
    static inputs: string[];
    static configInputs: string[];
    constructor(insuranceProvider: InsuranceProvider, translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    private emitLoad;
}
