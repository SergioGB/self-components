import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
var PaymentReceiptsV1Component = (function () {
    function PaymentReceiptsV1Component(insuranceProvider) {
        this.insuranceProvider = insuranceProvider;
        this.policyIds = [];
    }
    PaymentReceiptsV1Component.prototype.ngOnInit = function () {
        this.queryData();
    };
    PaymentReceiptsV1Component.prototype.queryData = function () {
        var _this = this;
        if (this.mocked) {
            this.riskId = "1";
        }
        if (this.riskId) {
            this.insuranceProvider.getInsuranceDetailSettings(this.mocked, this.riskId).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    _this.processInsurance(mapfreResponse.data);
                }
            });
        }
    };
    PaymentReceiptsV1Component.prototype.processInsurance = function (insurance) {
        for (var _i = 0, _a = insurance.associated_policies; _i < _a.length; _i++) {
            var policy = _a[_i];
            if (this.policyIds.indexOf(policy.id) < 0) {
                this.policyIds.push(policy.id);
            }
        }
    };
    PaymentReceiptsV1Component.inputs = ['riskId'];
    PaymentReceiptsV1Component.configInputs = ['mocked', 'styles', 'modals'];
    PaymentReceiptsV1Component.decorators = [
        { type: Component, args: [{
                    selector: 'payment-receipts-v1',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div *ngIf=\"policyIds && policyIds.length\" [ngStyle]=\"styles\">     <div *ngFor=\"let policyId of policyIds\" class=\"main-payment\">       <invoice-list [mocked]=\"mocked\" [riskId]=\"riskId\" [policyId]=\"policyId\"></invoice-list>          <payment-choose-v1 [mocked]=\"mocked\" [policyId]=\"policyId\"></payment-choose-v1>          <receipt-fractionate-question [mocked]=\"mocked\" [riskId]=\"riskId\" [policyId]=\"policyId\"></receipt-fractionate-question>     </div>   </div>   "
                },] },
    ];
    PaymentReceiptsV1Component.ctorParameters = function () { return [
        { type: InsuranceProvider, },
    ]; };
    PaymentReceiptsV1Component.propDecorators = {
        'mocked': [{ type: Input },],
        'modals': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
    };
    return PaymentReceiptsV1Component;
}());
export { PaymentReceiptsV1Component };
//# sourceMappingURL=payment-receipts-v1.js.map