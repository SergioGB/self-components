import { OnInit } from '@angular/core';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
export declare class PaymentReceiptsComponent implements OnInit {
    private insuranceProvider;
    mocked?: boolean;
    modals?: boolean;
    styles?: any;
    riskId: string;
    policyIds: string[];
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(insuranceProvider: InsuranceProvider);
    ngOnInit(): void;
    private queryData;
    private processInsurance;
}
