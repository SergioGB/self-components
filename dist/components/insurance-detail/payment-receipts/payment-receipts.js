import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
var PaymentReceiptsComponent = (function () {
    function PaymentReceiptsComponent(insuranceProvider) {
        this.insuranceProvider = insuranceProvider;
        this.policyIds = [];
    }
    PaymentReceiptsComponent.prototype.ngOnInit = function () {
        this.queryData();
    };
    PaymentReceiptsComponent.prototype.queryData = function () {
        var _this = this;
        if (this.mocked) {
            this.riskId = "1";
        }
        if (this.riskId) {
            this.insuranceProvider.getInsuranceDetailSettings(this.mocked, this.riskId).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    _this.processInsurance(mapfreResponse.data);
                }
            });
        }
    };
    PaymentReceiptsComponent.prototype.processInsurance = function (insurance) {
        for (var _i = 0, _a = insurance.associated_policies; _i < _a.length; _i++) {
            var policy = _a[_i];
            if (this.policyIds.indexOf(policy.id) < 0) {
                this.policyIds.push(policy.id);
            }
        }
    };
    PaymentReceiptsComponent.inputs = ['riskId'];
    PaymentReceiptsComponent.configInputs = ['mocked', 'styles', 'modals'];
    PaymentReceiptsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'payment-receipts',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div *ngIf=\"policyIds && policyIds.length\" [ngStyle]=\"styles\">   <div *ngFor=\"let policyId of policyIds\" class=\"main-payment\">     <invoice-list [mocked]=\"mocked\" [riskId]=\"riskId\" [policyId]=\"policyId\"></invoice-list>      <payment-choose [mocked]=\"mocked\" [policyId]=\"policyId\"></payment-choose>      <receipt-fractionate-question [mocked]=\"mocked\" [riskId]=\"riskId\" [policyId]=\"policyId\"></receipt-fractionate-question>   </div> </div>"
                },] },
    ];
    PaymentReceiptsComponent.ctorParameters = function () { return [
        { type: InsuranceProvider, },
    ]; };
    PaymentReceiptsComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'modals': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
    };
    return PaymentReceiptsComponent;
}());
export { PaymentReceiptsComponent };
//# sourceMappingURL=payment-receipts.js.map