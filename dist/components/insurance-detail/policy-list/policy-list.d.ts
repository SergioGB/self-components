import { OnInit } from '@angular/core';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { Policy } from '../../../models/policy';
export declare class PolicyListComponent implements OnInit {
    private insuranceProvider;
    mocked?: boolean;
    modals?: any;
    styles?: any;
    riskId?: string;
    policies?: Policy[];
    fromCancellation?: boolean;
    componentReady: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(insuranceProvider: InsuranceProvider);
    ngOnInit(): void;
    private getPolicies;
}
