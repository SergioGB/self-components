import { OnInit } from '@angular/core';
import { ModalController, Events } from 'ionic-angular';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { TranslationProvider } from '../../../providers/translation/translation';
import { UserProvider } from '../../../providers/user/user';
export declare class ReceiptFractionateQuestionComponent implements OnInit {
    private events;
    private modalCtrl;
    private insuranceProvider;
    private userProvider;
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    riskId?: string;
    policyId?: string;
    showComponent: boolean;
    defaultIcons: string[];
    static inputs: string[];
    static configInputs: string[];
    constructor(events: Events, modalCtrl: ModalController, insuranceProvider: InsuranceProvider, userProvider: UserProvider, translationProvider: TranslationProvider);
    ngOnInit(): void;
    private queryData;
    presentFractionateModal(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
