import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ModalController, Events } from 'ionic-angular';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InformationModalComponent } from '../../_modals/information-modal/information-modal';
import { UserProvider } from '../../../providers/user/user';
var ReceiptFractionateQuestionComponent = (function () {
    function ReceiptFractionateQuestionComponent(events, modalCtrl, insuranceProvider, userProvider, translationProvider) {
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.insuranceProvider = insuranceProvider;
        this.userProvider = userProvider;
        this.translationProvider = translationProvider;
        this.defaultIcons = ["mapfre-arrow-right"];
        this.translationProvider.bind(this);
    }
    ReceiptFractionateQuestionComponent.prototype.ngOnInit = function () {
        this.queryData();
    };
    ReceiptFractionateQuestionComponent.prototype.queryData = function () {
        var _this = this;
        if (this.riskId && this.policyId) {
            this.insuranceProvider.getPolicyDetailSettings(this.mocked, this.policyId).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                    if (mapfreResponse.data) {
                        var policy_1 = mapfreResponse.data;
                        _this.insuranceProvider.getPolicyFractionamentTypes(_this.mocked, _this.policyId).subscribe(function (response) {
                            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                            if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                                var fractionamentTypes = mapfreResponse.data;
                                if (policy_1.car_policy_detail) {
                                    _this.showComponent = policy_1.car_policy_detail.basic_info.payment_modality.editable && fractionamentTypes;
                                }
                                else if (policy_1.home_policy_detail) {
                                    _this.showComponent = policy_1.home_policy_detail.basic_info.payment_modality.editable && fractionamentTypes;
                                }
                            }
                        });
                    }
                }
            });
        }
    };
    ReceiptFractionateQuestionComponent.prototype.presentFractionateModal = function () {
        var _this = this;
        var cancelContent = {
            icon: 'mapfre-cross-error',
            title: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.receipt_fractionate.process_cancel')
        };
        var modal = this.modalCtrl.create(InformationModalComponent, {
            cancelContent: cancelContent,
            icon: 'mapfre-information',
            labels: {
                cancel: "modal.receipt-fractionate-question.information-modal.generic.cancel",
                finish: "modal.receipt-fractionate-question.information-modal.generic.finish",
                accept: "modal.receipt-fractionate-question.information-modal.generic.accept"
            },
            rightButton: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.generic.continue'),
            subtitle: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.fractionament.alert.subtitle'),
            title: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.fractionament.alert.title'),
            type: 2,
            varSubtitle: ''
        });
        modal.onDidDismiss(function (accepted) {
            if (accepted) {
                _this.events.publish('receipt-fractionate-question::goFractionamentTypeSelection', { riskId: _this.riskId, policyId: _this.policyId, stepId: 1 });
            }
        });
        modal.present();
    };
    ReceiptFractionateQuestionComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    ReceiptFractionateQuestionComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    ReceiptFractionateQuestionComponent.inputs = ['riskId', 'policyId'];
    ReceiptFractionateQuestionComponent.configInputs = ['mocked', 'icons', 'styles'];
    ReceiptFractionateQuestionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'receipt-fractionate-question',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"showComponent\" tabindex=\"0\" class=\"clickable\" (click)=\"presentFractionateModal()\" [ngStyle]=\"styles\">    <ion-card-header class=\"no-border\">     <ion-item>       <h3>         {{ translate('payment-receipts.receipt-fractionate-question.receipt_fractionate.title') }}       </h3>       <h4 class=\"fmm-body1 grey1\">         {{ translate('payment-receipts.receipt-fractionate-question.receipt_fractionate.subtitle') }}       </h4>        <ion-icon item-end no-margin [name]=\"getIcon(0)\" class=\"primary\"></ion-icon>     </ion-item>   </ion-card-header>  </ion-card>"
                },] },
    ];
    ReceiptFractionateQuestionComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: ModalController, },
        { type: InsuranceProvider, },
        { type: UserProvider, },
        { type: TranslationProvider, },
    ]; };
    ReceiptFractionateQuestionComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'policyId': [{ type: Input },],
    };
    return ReceiptFractionateQuestionComponent;
}());
export { ReceiptFractionateQuestionComponent };
//# sourceMappingURL=receipt-fractionate-question.js.map