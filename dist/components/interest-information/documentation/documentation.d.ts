import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { MapfreFile } from '../../../models/mapfreFile';
export declare class DocumentationComponent implements OnInit {
    private translationProvider;
    private documentsProvider;
    mocked?: boolean;
    styles?: any;
    icons?: string[];
    documentation: any[];
    defaultIcons: string[];
    private documentationFullHeight;
    private documentationCollapsedHeight;
    private documentationCollapsed;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, documentsProvider: DocumentsProvider);
    ngOnInit(): void;
    toggleDocumentation(): void;
    storeDocumentationHeight(): void;
    getProperDocumentationHeight(): string;
    downloadFile(mapfreFile: MapfreFile): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
