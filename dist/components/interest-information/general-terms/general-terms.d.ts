import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { MapfreFile } from '../../../models/mapfreFile';
export declare class GeneralTermsComponent {
    private translationProvider;
    private documentsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    carsFullHeight: number;
    carsCollapsedHeight: number;
    carsCollapsed: boolean;
    motoFullHeight: number;
    motoCollapsedHeight: number;
    motoCollapsed: boolean;
    homeFullHeight: number;
    homeCollapsedHeight: number;
    homeCollapsed: boolean;
    cars: any[];
    motorbikes: any[];
    home: any[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, documentsProvider: DocumentsProvider);
    ngOnInit(): void;
    toggleCars(): void;
    storeCarsHeight(): void;
    getProperCarsHeight(): string;
    toggleMoto(): void;
    storeMotoHeight(): void;
    getProperMotoHeight(): string;
    toggleHome(): void;
    storeHomeHeight(): void;
    getProperHomeHeight(): string;
    downloadFile(mapfreFile: MapfreFile): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
