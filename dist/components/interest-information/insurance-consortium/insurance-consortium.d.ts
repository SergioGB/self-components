import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { MapfreFile } from '../../../models/mapfreFile';
export declare class InsuranceConsortiumComponent implements OnInit {
    private translationProvider;
    private documentsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    insuranceConsortiumFullHeight: number;
    insuranceConsortiumCollapsedHeight: number;
    insuranceConsortiumCollapsed: boolean;
    insurances: any[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, documentsProvider: DocumentsProvider);
    ngOnInit(): void;
    toggleInsuranceConsortium(): void;
    storeInsuranceConsortiumHeight(): void;
    getProperInsuranceConsortiumHeight(): string;
    downloadFile(mapfreFile: MapfreFile): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
