import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ContentTab } from '../../../models/content-tab';
export declare class InterestInformationHeaderComponent implements OnInit {
    private translationProvider;
    headerTabs: ContentTab[];
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    headerData: any;
    ready: boolean;
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
}
