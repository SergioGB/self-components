import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
var InterestInformationHeaderComponent = (function () {
    function InterestInformationHeaderComponent(translationProvider) {
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
    }
    InterestInformationHeaderComponent.prototype.ngOnInit = function () {
        this.translationProvider.bind(this);
        this.headerData = {
            title: this.translationProvider.getValueForKey('interest-information-header.interest_information.interest_information_header.title')
        };
        this.ready = true;
    };
    InterestInformationHeaderComponent.configInputs = ['mocked', 'styles', 'headerTabs'];
    InterestInformationHeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'interest-information-header',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<page-header   *ngIf=\"ready\"   [data]=\"headerData\"   [tabs]=\"headerTabs\"   [ngStyle]=\"styles\"> </page-header>"
                },] },
    ];
    InterestInformationHeaderComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
    ]; };
    InterestInformationHeaderComponent.propDecorators = {
        'headerTabs': [{ type: Input },],
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return InterestInformationHeaderComponent;
}());
export { InterestInformationHeaderComponent };
//# sourceMappingURL=interest-information-header.js.map