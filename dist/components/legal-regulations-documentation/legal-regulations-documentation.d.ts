import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { DocumentsProvider } from '../../providers/documents/documents';
export declare class LegalRegulationsDocumentationComponent implements OnInit {
    private documentsProvider;
    private translationProvider;
    mocked?: boolean;
    styles?: any;
    ready: boolean;
    documents: any[];
    static configInputs: string[];
    constructor(documentsProvider: DocumentsProvider, translationProvider: TranslationProvider);
    ngOnInit(): void;
    downloadDocument(document: any): void;
}
