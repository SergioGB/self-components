import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Mocks } from '../../providers/mocks/mocks';
import { TranslationProvider } from '../../providers/translation/translation';
import { DocumentsProvider } from '../../providers/documents/documents';
var LegalRegulationsDocumentationComponent = (function () {
    function LegalRegulationsDocumentationComponent(documentsProvider, translationProvider) {
        this.documentsProvider = documentsProvider;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
    }
    LegalRegulationsDocumentationComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.documents = Mocks.getLegalRegulationsDocuments();
            this.ready = true;
        }
        else {
        }
    };
    LegalRegulationsDocumentationComponent.prototype.downloadDocument = function (document) {
        this.documentsProvider.downloadFile(document);
    };
    LegalRegulationsDocumentationComponent.configInputs = ['mocked', 'styles'];
    LegalRegulationsDocumentationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'legal-regulations-documentation',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">   <loading-spinner *ngIf=\"!ready\"></loading-spinner>   <div *ngIf=\"ready\">      <ion-card-header class=\"no-padding-bottom\">       <ion-item>         <h3>           {{ translate('legal-regulations-documentation.legal_regulation.available_documentation') }}         </h3>       </ion-item>     </ion-card-header>      <ion-card-content class=\"padding-top-s\">       <download-item         *ngFor=\"let document of documents\"         no-padding         [file]=\"document\"         [noTrash]=\"true\"         (onDownload)=\"downloadDocument($event)\">       </download-item>      </ion-card-content>    </div> </ion-card>"
                },] },
    ];
    LegalRegulationsDocumentationComponent.ctorParameters = function () { return [
        { type: DocumentsProvider, },
        { type: TranslationProvider, },
    ]; };
    LegalRegulationsDocumentationComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return LegalRegulationsDocumentationComponent;
}());
export { LegalRegulationsDocumentationComponent };
//# sourceMappingURL=legal-regulations-documentation.js.map