import { OnInit } from '@angular/core';
import { ComponentSettingsProvider } from "../../../providers/component-settings/component-settings";
import { ApiProvider } from '../../../providers/api/api';
export declare class LoginBannerComponent implements OnInit {
    private componentSettingsProvider;
    private apiProvider;
    mocked?: boolean;
    styles?: any;
    clear?: boolean;
    background_image?: string;
    title?: any;
    description?: any;
    language: string;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, apiProvider: ApiProvider);
    ngOnInit(): void;
}
