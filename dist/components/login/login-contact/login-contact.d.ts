import { OnInit } from '@angular/core';
import { ComponentSettingsProvider } from "../../../providers/component-settings/component-settings";
import { ApiProvider } from '../../../providers/api/api';
export declare class LoginContactComponent implements OnInit {
    private componentSettingsProvider;
    private apiProvider;
    mocked?: boolean;
    styles?: any;
    title?: any;
    subtitle?: any;
    background_image?: any;
    componentReady: boolean;
    componentSettings: Object;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, apiProvider: ApiProvider);
    ngOnInit(): void;
    getTitle(): any;
    getSubtitle(): any;
}
