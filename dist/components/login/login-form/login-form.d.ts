import { AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Events, ModalController, NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { SocialUser } from 'angularx-social-login';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { UserProvider } from '../../../providers/user/user';
import { SocialLoginProvider } from '../../../providers/social-login/social-login';
import { IdentificationProvider } from "../../../providers/identification/identification";
import { WelcomePackProvider } from '../../../providers/welcome-pack/welcome-pack';
import { BlockedUserRecoveryMethod } from '../../../enums/blocked-user-recovery-method';
import { User } from '../../../models/user';
import { SocialLoginRequest } from '../../../models/login/socialLoginRequest';
import { MapfreResponseEntity } from '../../../models/mapfreResponseEntity';
import { ApiProvider } from '../../../providers/api/api';
import { PlatformProvider } from '../../../providers/platform/platform';
import { TranslationProvider } from '../../../providers/translation/translation';
import { MapfreResponse } from '../../../models/mapfreResponse';
export declare class LoginFormComponent implements OnInit, AfterViewInit, OnDestroy {
    private platformProvider;
    private formBuilder;
    private componentSettingsProvider;
    private translationProvider;
    private userProvider;
    private socialLoginProvider;
    private navCtrl;
    private apiProvider;
    private navParams;
    private events;
    private modalController;
    private identificationProvider;
    private welcomePackProvider;
    static componentName: string;
    userValidated: boolean;
    securityQuestionsIn: any[];
    icons?: string[];
    modals?: string[];
    styles?: any;
    email: string;
    simplified: string;
    registrationResponse?: MapfreResponseEntity;
    defaultIcons: string[];
    componentReady: boolean;
    componentSettings: any;
    socialLogins: any;
    loginForm: FormGroup;
    loginResponse: MapfreResponseEntity;
    passwordChangeResponse: MapfreResponseEntity;
    pinChangeResponse: MapfreResponseEntity;
    selectedBlockedUserRecoveryMethod: BlockedUserRecoveryMethod;
    errorGotten: boolean;
    registrationType: string;
    registrationActive: boolean;
    registrationSuccessful: boolean;
    forgotPasswordActive: boolean;
    forgotPinActive: boolean;
    loginActive: boolean;
    userBlocked: boolean;
    changeForgotPassword: boolean;
    changeRegistration: boolean;
    changeBlockedUser: boolean;
    user: User;
    socialUser: SocialUser;
    recoveryPasswordTitle: string;
    recoveryPasswordSubtitle: string;
    welcome_title: string;
    welcome_subtitle: string;
    accessWithPin: boolean;
    isFirst: boolean;
    transitionTime: number;
    userName: string;
    loggedIn: boolean;
    fingerprintAvailable: boolean;
    fingerprintType: string;
    automaticFingerprint: boolean;
    fingerprintEnabled: boolean;
    logged: boolean;
    userData: any;
    dayTimeSlots: any;
    componentName: string;
    rememberedUser: string;
    loggedUserName: string;
    static redirectedEvent: string;
    static inputs: string[];
    static configInputs: string[];
    constructor(platformProvider: PlatformProvider, formBuilder: FormBuilder, componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, userProvider: UserProvider, socialLoginProvider: SocialLoginProvider, navCtrl: NavController, apiProvider: ApiProvider, navParams: NavParams, events: Events, modalController: ModalController, identificationProvider: IdentificationProvider, welcomePackProvider: WelcomePackProvider);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    private _unsubscribeEvents;
    private _subscribeEvents;
    toggleRegistration($event?: Event): void;
    toggleForgotPassword($event?: Event): void;
    toggleForgotPin($event?: Event): void;
    toggleBlocked($event?: Event): void;
    loginWith(socialNetwork: any): void;
    socialLogin(socialLoginRequest: SocialLoginRequest): void;
    showFingerprintDialog(): void;
    scrollToTopPage(): void;
    scrollToTopComponent(): void;
    attemptLogin(): void;
    onRegistration(mapfreResponse: MapfreResponse): void;
    onSocialLogin(socialUser: SocialUser): void;
    onPasswordChange(mapfreResponse: MapfreResponse): void;
    onPinChange(mapfreResponse: MapfreResponse): void;
    onBlockedUserMethodSelection(selectedBlockedUserRecoveryMethod: BlockedUserRecoveryMethod): void;
    closeResponse(mapfreResponseEntity: MapfreResponseEntity): void;
    forgotPassword(): void;
    forgotPin(): void;
    register(registrationType?: string): void;
    redirectToRegistrationFlow(): void;
    loginAnimationEnd(needsCss?: boolean): void;
    forgotPasswordAnimationEnd(): void;
    forgotPinAnimationEnd(): void;
    registrationAnimationEnd(): void;
    blockedUserAnimationEnd(): void;
    needsCssTransition(): any;
    loginCanBeHidden(): boolean;
    private onLoginSuccess;
    private showWelcomePack;
    private removeCssClasses;
    private hideResponsesCss;
    resizeLoginIE(): void;
    forgetMe(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
