import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { SuccessStory } from '../../../models/login/successStory';
export declare class LoginSuccessStoriesComponent implements OnInit {
    private componentSettingsProvider;
    private translationProvider;
    mocked?: boolean;
    styles?: any;
    clear?: boolean;
    background_image?: string;
    autoplayDelay: number;
    componentReady: boolean;
    componentSettings: Object;
    stories: SuccessStory[];
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider);
    ngOnInit(): void;
    private readyComponent;
}
