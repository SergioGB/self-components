import { OnInit } from '@angular/core';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { ModalController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';
export declare class LoginVideoComponent implements OnInit {
    private componentSettingsProvider;
    private modalCtrl;
    private apiProvider;
    static componentName: string;
    player: any;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    clear?: boolean;
    video_link?: string;
    title?: any;
    description?: any;
    defaultIcons: string[];
    playing: boolean;
    language: string;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, modalCtrl: ModalController, apiProvider: ApiProvider);
    ngOnInit(): void;
    togglePlayVideo(): void;
    openVideoModal(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
