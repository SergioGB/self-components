import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events, ModalController, NavController } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { UserProvider } from '../../../../providers/user/user';
import { Message } from '../../../../models/message';
import { SMSMessageContent } from '../../../../models/smsMessageContent';
import { MessageEmailContent } from '../../../../models/messageEmailContent';
import { HtmlContentModalComponent } from '../../../_modals/html-content-modal/html-content-modal';
import { BlockedUserCheckSmsComponent } from '../../../blocked-user/blocked-user-check-sms/blocked-user-check-sms';
var SimpleRegistrationFormComponent = (function () {
    function SimpleRegistrationFormComponent(translationProvider, componentSettingsProvider, userProvider, formBuilder, modalCtrl, navCtrl, events) {
        var _this = this;
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.userProvider = userProvider;
        this.formBuilder = formBuilder;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.events = events;
        this.defaultIcons = ["warning", "checkmark", "close-circle", "warning", "checkmark", "mapfre-thumbs-up", "mapfre-close", "mapfre-rocket"];
        this.errorGotten = false;
        this.wrongEmail = false;
        this.wrongPhone = false;
        this.mismatchedPasswords = false;
        this.registrationSuccessMessage = false;
        this.registrationSuccessMessageManager = false;
        this.smsSelected = false;
        this.emailSelected = false;
        this.componentReady = false;
        this.registrationForm = this.formBuilder.group({
            name: ['', Validators.required],
            surname1: ['', Validators.required],
            surname2: ['', Validators.required],
            identification_document: [undefined, Validators.required],
            identification_number: ['', Validators.required],
            mail: ['', [Validators.required, Validators.email]],
            mobile_phone: ['', Validators.required],
            password: ['', Validators.required],
            validationPassword: ['', [Validators.required, this.passwordValidation]],
            acceptedConditionsAndPoliciesCheck: [false, this.isChecked]
        }, {
            validator: this.passwordsValidator
        });
        this.popOverOptions = {
            cssClass: 'popover-content-cm'
        };
        this.translationProvider.bind(this);
        this.componentSettingsProvider.getClientsSettings(this.mocked, "ALL").subscribe(function (response) {
            var mapfreResponse = _this.userProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.clients = mapfreResponse.data;
            }
        });
    }
    SimpleRegistrationFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.registrationResponse) {
            if (this.registrationResponse.code === 0) {
                this.events.publish(SimpleRegistrationFormComponent.registrationEvent, this.registrationMapfreResponse);
            }
            else if (this.registrationResponse.code === 1) {
                this.registrationSuccessMessageManager = true;
            }
            else if (this.registrationResponse.code === 2) {
                this.registrationSuccessMessage = true;
            }
            this.scrollToTopPage();
        }
        if (this.mocked) {
            return;
        }
        this.componentSettingsProvider.getIdentificationDocuments(this.mocked).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.componentReady = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
                _this.identificationDocuments = mapfreResponse.data;
            }
        });
        this._unsubscribeMethods();
        this._subscribeMethods();
    };
    SimpleRegistrationFormComponent.prototype.ngOnDestroy = function () {
        this._unsubscribeMethods();
    };
    SimpleRegistrationFormComponent.prototype._subscribeMethods = function () {
        this.events.subscribe(BlockedUserCheckSmsComponent.onCloseEvent);
    };
    SimpleRegistrationFormComponent.prototype._unsubscribeMethods = function () {
        this.events.unsubscribe(BlockedUserCheckSmsComponent.onCloseEvent);
    };
    SimpleRegistrationFormComponent.prototype.isChecked = function (control) {
        if (control.value !== true) {
            return {
                "notChecked": true
            };
        }
        return null;
    };
    Object.defineProperty(SimpleRegistrationFormComponent.prototype, "componentName", {
        get: function () {
            return SimpleRegistrationFormComponent.componentName;
        },
        enumerable: true,
        configurable: true
    });
    SimpleRegistrationFormComponent.prototype.passwordValidation = function (input) {
        if (input.value === input.root.value['password']) {
            return null;
        }
        else {
            return {
                isValid: true
            };
        }
    };
    SimpleRegistrationFormComponent.prototype.passwordsValidator = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value && confirmPassword.value && password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    SimpleRegistrationFormComponent.prototype.scrollToTopPage = function () {
        this.events.publish('scrollToTop');
    };
    SimpleRegistrationFormComponent.prototype.scrollToMessage = function () {
        this.events.publish('scrollToElement', 'alertMessage');
    };
    SimpleRegistrationFormComponent.prototype.attemptRegistration = function () {
        var _this = this;
        if (this.registrationForm.valid) {
            var registrationRequest = this.registrationForm.value;
            this.userProvider.sendRegistrationRequest(this.registrationForm.value).subscribe(function (response) {
                var mapfreResponse = _this.userProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    _this.registrationMapfreResponse = mapfreResponse;
                    _this.accountValidation = mapfreResponse.response.code;
                    _this.responseValidation = mapfreResponse.data.new_account_response;
                    if (_this.accountValidation !== undefined) {
                        if (_this.accountValidation === 0) {
                            _this.events.publish(SimpleRegistrationFormComponent.registrationEvent, _this.registrationMapfreResponse);
                        }
                        else if (_this.accountValidation === 1) {
                            _this.scrollToTopPage();
                            _this.registrationSuccessMessageManager = true;
                        }
                        else if (_this.accountValidation === 2) {
                            _this.scrollToTopPage();
                            _this.registrationSuccessMessage = true;
                        }
                    }
                    else {
                        _this.events.publish(SimpleRegistrationFormComponent.registrationEvent, _this.registrationMapfreResponse);
                    }
                }
            });
        }
    };
    SimpleRegistrationFormComponent.prototype.onBlurEmail = function () {
        this.wrongEmail =
            this.registrationForm.value.mail &&
                !this.registrationForm.controls['mail'].valid && this.registrationForm.controls['mail'].dirty;
    };
    SimpleRegistrationFormComponent.prototype.onBlurPhone = function () {
        this.wrongPhone =
            this.registrationForm.value.mobile_phone &&
                !this.registrationForm.controls['mobile_phone'].valid && this.registrationForm.controls['mobile_phone'].dirty;
    };
    SimpleRegistrationFormComponent.prototype.onBlurPasswords = function () {
        if (this.registrationForm.value.password !== "" && this.registrationForm.value.validationPassword !== "") {
            var password = this.registrationForm.value.password;
            var confirmPassword = this.registrationForm.value.validationPassword;
            this.mismatchedPasswords = password !== confirmPassword;
        }
    };
    SimpleRegistrationFormComponent.prototype.onBlurIdentification = function () {
        if (this.registrationForm.value.identification_number !== "") {
            for (var _i = 0, _a = this.clients; _i < _a.length; _i++) {
                var user = _a[_i];
                if (this.registrationForm.value.identification_number === user.personal_data.identification_document_number) {
                    this.errorGotten = true;
                }
            }
        }
    };
    SimpleRegistrationFormComponent.prototype.closeResponse = function () {
        this.errorGotten = !this.errorGotten;
    };
    SimpleRegistrationFormComponent.prototype.goToForgottenPassword = function () {
        this.closeResponse();
        this.events.publish('registration-form-simple::goLogin');
    };
    SimpleRegistrationFormComponent.prototype.sendSMS = function () {
        var _this = this;
        this.validationContent = new Message('SMS', null, new SMSMessageContent('', '', null));
        this.userProvider.sendMailSMSRequest(this.validationContent).subscribe(function (response) {
            var mapfreResponse = _this.userProvider.getResponseJSON(response);
            var success = _this.userProvider.isMapfreResponseValid(mapfreResponse);
            if (success) {
                _this.scrollToTopPage();
                _this.smsSelected = true;
            }
        });
    };
    SimpleRegistrationFormComponent.prototype.sendEmail = function () {
        var _this = this;
        this.validationContent = new Message('EMAIL', new MessageEmailContent('', '', '', null));
        this.userProvider.sendMailSMSRequest(this.validationContent).subscribe(function (response) {
            var mapfreResponse = _this.userProvider.getResponseJSON(response);
            var success = _this.userProvider.isMapfreResponseValid(mapfreResponse);
            if (success) {
                _this.scrollToTopPage();
                _this.emailSelected = true;
            }
        });
    };
    SimpleRegistrationFormComponent.prototype.onChildPasswordChange = function () {
        this.events.publish(SimpleRegistrationFormComponent.registrationEvent, this.registrationMapfreResponse);
        this.events.publish(SimpleRegistrationFormComponent.successEvent);
    };
    SimpleRegistrationFormComponent.prototype.onSuccess = function () {
        this.events.publish(SimpleRegistrationFormComponent.successEvent);
    };
    SimpleRegistrationFormComponent.prototype.onCancellation = function () {
        this.events.publish(SimpleRegistrationFormComponent.cancelEvent);
    };
    SimpleRegistrationFormComponent.prototype.closeResponseEmail = function () {
        this.wrongEmail = false;
    };
    SimpleRegistrationFormComponent.prototype.closeResponsePhone = function () {
        this.wrongPhone = false;
    };
    SimpleRegistrationFormComponent.prototype.closeResponseMismatchedPasswords = function () {
        this.mismatchedPasswords = false;
    };
    SimpleRegistrationFormComponent.prototype.openPrivacyModal = function () {
        var _this = this;
        this.componentSettingsProvider.getInformativeMessage(this.mocked, 'PRIVACITY').subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                var data = mapfreResponse.data[0];
                _this.modalCtrl.create(HtmlContentModalComponent, { title: data.title, description: data.description }).present();
            }
        });
    };
    SimpleRegistrationFormComponent.prototype.openConditionsModal = function () {
        var _this = this;
        this.componentSettingsProvider.getInformativeMessage(this.mocked, 'TERMS_OF_USE').subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                var data = mapfreResponse.data[0];
                _this.modalCtrl.create(HtmlContentModalComponent, { title: data.title, description: data.description }).present();
            }
        });
    };
    SimpleRegistrationFormComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    SimpleRegistrationFormComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    SimpleRegistrationFormComponent.componentName = 'registration-form-simple';
    SimpleRegistrationFormComponent.successEvent = SimpleRegistrationFormComponent.componentName + ":success";
    SimpleRegistrationFormComponent.cancelEvent = SimpleRegistrationFormComponent.componentName + ":cancel";
    SimpleRegistrationFormComponent.registrationEvent = SimpleRegistrationFormComponent.componentName + ":registration";
    SimpleRegistrationFormComponent.inputs = ['registrationResponse'];
    SimpleRegistrationFormComponent.configInputs = ['mocked', 'icons'];
    SimpleRegistrationFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'registration-form-simple',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["registration-form-simple .warning-link {   width: 100%; }  registration-form-simple .continue-col {   margin-left: 7px !important;   width: 133px !important;   height: 42px !important;   text-align: right !important; }  registration-form-simple .cancel-col {   margin-right: 7px !important;   max-width: 133px !important;   width: 133px !important;   height: 42px !important; }  registration-form-simple .continue-button {   width: 133px !important;   height: 42px !important; }  registration-form-simple .cancel-button {   width: 133px !important;   height: 42px !important;   text-align: right !important; }  /* mobile */ @media (max-width: 767px) {   .continue-col {     max-width: 133px !important;     right: 18px !important;     position: absolute; } }"],
                    template: "<ion-row [ngStyle]=\"styles\">   <ion-col class=\"no-padding\" col-12 col-xl-8 offset-xl-2>     <ion-card *ngIf=\"!registrationSuccessMessage && !registrationSuccessMessageManager\">         <ion-card *ngIf=\"errorGotten\" class=\"no-horizontal-margin\">             <ion-item class=\"alert\" [ngClass]=\"{'warning': errorGotten, 'success': !errorGotten}\">               <ion-icon [name]=\"errorGotten ? getIcon(0) : getIcon(1)\" class=\"login-response-warning-icon\" small item-start></ion-icon>               <p class=\"horizontal-margin-m\">                 <!-- {{ registrationResponse.message }} -->                 {{ translate('login.registration-form-simple.new_account.identification_warning') }}               </p>               <ion-icon [name]=\"getIcon(2)\" item-end class=\"clickable login-response-close-icon\" small (click)=\"closeResponse()\"                 tabindex=\"0\" (keyup.enter)=\"closeResponse()\"></ion-icon>                 <div class=\"padding\">                   <ion-item class=\"warning alert no-padding margin-left-s\">                     <a class=\"uppercase clickable warning\"                      class=\"uppercase clickable warning\"                      [title]=\"translate('login.registration-form-simple.new_account.recover_password')\"                     (click)=\"goToForgottenPassword()\">                       {{ translate('login.registration-form-simple.new_account.recover_password') }}                     </a>                   </ion-item>                 </div>           </ion-item>         </ion-card>       <ion-card *ngIf=\"registrationResponse\">          <ion-item class=\"alert\" [ngClass]=\"{'warning': errorGotten, 'success': !errorGotten}\">           <ion-icon [name]=\"errorGotten ? getIcon(3) : getIcon(4)\" class=\"login-response-warning-icon\" small item-start></ion-icon>           <p class=\"horizontal-margin-m\">             {{ registrationResponse.message }}           </p>         </ion-item>        </ion-card>              <form [formGroup]=\"registrationForm\" (ngSubmit)=\"attemptRegistration()\">          <ion-card-content>           <!--<div class=\"mobile-only\">-->             <h1 class=\"margin-top-s margin-bottom-xs\">               {{ translate('login.registration-form-simple.new_account.title') }}             </h1>             <h4 showWhen=\"core,tablet\" class=\"fmm-secondary-text fmm-regular vertical-margin-s\">               {{ translate('login.registration-form-simple.new_account.subtitle') }}             </h4>             <h5 class=\"margin-top-xs fmm-regular\">               {{ translate('login.registration-form-simple.generic.required_fields') }}             </h5>           <!--</div>-->            <ion-grid class=\"buttons-wrapper no-padding\">             <ion-row>                <ion-col col-12 col-md-6>                  <ion-item no-padding class=\"required\">                   <ion-label floating>                     {{ translate('login.registration-form-simple.new_account.name') }}                   </ion-label>                   <ion-input type=\"text\" formControlName=\"name\" maxlength=\"50\"></ion-input>                 </ion-item>                </ion-col>                <ion-col col-12 col-md-6>                  <ion-item no-padding class=\"required\">                   <ion-label floating>                     {{ translate('login.registration-form-simple.new_account.surname1') }}                   </ion-label>                   <ion-input type=\"text\" formControlName=\"surname1\" maxlength=\"20\"></ion-input>                 </ion-item>                </ion-col>              </ion-row>             <ion-row>                <ion-col col-12 col-md-6>                  <ion-item no-padding class=\"required\">                   <ion-label floating>                     {{ translate('login.registration-form-simple.new_account.surname2') }}                   </ion-label>                   <ion-input type=\"text\" formControlName=\"surname2\" maxlength=\"20\"></ion-input>                 </ion-item>                </ion-col>                <ion-col col-5 col-md-2>                  <ion-item no-padding class=\"required\">                   <ion-label floating>                     {{ translate('login.registration-form-simple.new_account.identification_document') }}                   </ion-label>                   <ion-select interface=\"popover\" *ngIf=\"componentReady\" [selectOptions]=\"popOverOptions\" formControlName=\"identification_document\">                     <ion-option *ngFor=\"let identification_document of identificationDocuments\" value=\"{{identification_document.id}}\">                       {{identification_document.title}}                     </ion-option>                   </ion-select>                 </ion-item>                </ion-col>                <ion-col col-7 col-md-4>                  <ion-item no-padding class=\"required\">                   <ion-label floating>                     {{ translate('login.registration-form-simple.new_account.identification_number') }}                   </ion-label>                   <ion-input type=\"text\" formControlName=\"identification_number\" (ionBlur)=\"onBlurIdentification()\" maxlength=\"20\"></ion-input>                 </ion-item>                </ion-col>              </ion-row>             <ion-row>                <ion-col col-12 col-md-6>                  <ion-item no-padding class=\"required\">                   <ion-label floating>                     {{ translate('login.registration-form-simple.new_account.mail') }}                   </ion-label>                   <ion-input type=\"email\" formControlName=\"mail\" (ionBlur)=\"onBlurEmail()\" maxlength=\"50\"></ion-input>                 </ion-item>                 <div *ngIf=\"wrongEmail\" class=\"input-value-error\">                   <span>                     {{ translate('login.registration-form-simple.new_account.email_format_error') }}                   </span>                 </div>               </ion-col>                <ion-col col-12 col-md-6>                  <ion-item no-padding class=\"required\">                   <ion-label floating>                     {{ translate('login.registration-form-simple.new_account.mobile_phone') }}                   </ion-label>                   <ion-input type=\"tel\" maxlength=\"20\" formControlName=\"mobile_phone\" pattern=\"(\\\\+[0-9]*)?[0-9]*\" (ionBlur)=\"onBlurPhone()\"></ion-input>                 </ion-item>                 <div *ngIf=\"wrongPhone\" class=\"input-value-error\">                   <span>                     {{ translate('login.registration-form-simple.new_account.phone_format_error') }}.                   </span>                 </div>               </ion-col>              </ion-row>             <ion-row>                <ion-col col-12 col-md-6>                  <ion-item no-padding class=\"required\">                   <ion-label floating *ngIf=\"!mismatchedPasswords\">                     {{ translate('login.registration-form-simple.new_account.password') }}                   </ion-label>                   <ion-label floating *ngIf=\"mismatchedPasswords\">                     {{ translate('login.registration-form-simple.new_account.password') }}                   </ion-label>                   <ion-input type=\"password\" formControlName=\"password\" (ionBlur)=\"onBlurPasswords()\"></ion-input>                 </ion-item>                </ion-col>                <ion-col col-12 col-md-6>                  <ion-item no-padding class=\"required\">                   <ion-label floating *ngIf=\"!mismatchedPasswords\">                     {{ translate('login.registration-form-simple.new_account.repeat_password') }}                   </ion-label>                   <ion-label floating class=\"red-text\" *ngIf=\"mismatchedPasswords\">                     {{ translate('login.registration-form-simple.new_account.repeat_password') }}                   </ion-label>                   <ion-input type=\"password\" formControlName=\"validationPassword\" (ionBlur)=\"onBlurPasswords()\"></ion-input>                 </ion-item>                </ion-col>              </ion-row>             <ion-col col-12 *ngIf=\"!mismatchedPasswords\">               <h5 class=\"margin-top-xs\">                 {{ translate('login.registration-form-simple.new_account.passwords_pattern') }}               </h5>             </ion-col>             <ion-col col-12 *ngIf=\"mismatchedPasswords\" class=\"input-value-error\">               <span>                 {{ translate('login.registration-form-simple.new_account.passwords_mismatch') }}               </span>             </ion-col>              <ion-row class=\"margin-top-l\">               <ion-col no-padding col-12 col-md-12>                 <ion-checkbox type=\"checkbox\" formControlName=\"acceptedConditionsAndPoliciesCheck\"></ion-checkbox>                 <span class=\"conditions-label\">                   {{ translate('login.registration-form-simple.new_account.privacy_check1') }}                   <a tabindex=\"0\" (keyup.enter)=\"openPrivacyModal()\" (click)=\"openPrivacyModal()\"                    class=\"clickable\" [title]=\"translate('login.registration-form-simple.new_account.privacy_check2')\">                     {{ translate('login.registration-form-simple.new_account.privacy_check2') }}                   </a>                   {{ translate('login.registration-form-simple.new_account.privacy_check3') }}                   <a (click)=\"openConditionsModal()\" onClick=\"return false;\" class=\"clickable\" [title]=\"translate('login.registration-form-simple.new_account.privacy_check4')\">                     {{ translate('login.registration-form-simple.new_account.privacy_check4') }}                   </a>                   {{ translate('login.registration-form-simple.new_account.privacy_check5') }}                 </span>                 <hr class=\"margin-top-m no-margin-bottom\">               </ion-col>             </ion-row>            </ion-grid>            <br>            <ion-grid no-padding class=\"actions-wrapper\">              <ion-row [hidden]=\"!errorGotten && registrationResponse\">               <ion-col no-padding class=\"cancel-col\">                 <button class=\"fmm-white cancel-button\" ion-button text-uppercase (click)=\"onCancellation($event)\">                   {{ translate('login.registration-form-simple.new_account.cancel_button') }}                 </button>               </ion-col>                <ion-col no-padding class=\"continue-col\" float-right>                 <button ion-button class=\"continue-button\" text-uppercase [disabled]=\"!registrationForm.valid || errorGotten\">                   {{ translate('login.registration-form-simple.generic.continue') }}                 </button>               </ion-col>             </ion-row>              <ion-row *ngIf=\"!errorGotten && registrationResponse\">               <ion-col col-sm-5 no-padding>                 <button class=\"secondary\" ion-button text-uppercase full (click)=\"onSuccess($event)\">                   {{ translate('login.registration-form-simple.new_account.back_button') }}                 </button>               </ion-col>             </ion-row>            </ion-grid>          </ion-card-content>       </form>     </ion-card>   </ion-col>    <ion-col class=\"no-padding\" col-12 col-xl-4 offset-xl-4>     <ion-card *ngIf=\"registrationSuccessMessage && !(emailSelected || smsSelected)\">       <ion-card-content text-center>         <ion-row>           <ion-col col-xl-8 offset-xl-2 offset-xs-8 col-xs-2 class=\"titleCard\">             <div class=\"icon-cancel-finish\">               <ion-icon [name]=\"getIcon(5)\"></ion-icon>             </div>             <div class=\"title-cancel-finish\">               <h4>{{ translate('login.registration-form-simple.new_account.validation_exit.title') }}</h4>             </div>             <span class=\"fmm-body2 grey1\">{{ translate('login.registration-form-simple.new_account.validation_exit.description') }}</span>           </ion-col>         </ion-row>       </ion-card-content>        <hr>       <br>        <ion-grid class=\"actions-wrapper no-padding\">          <ion-row>           <ion-col col-sm-5>             <ion-item text-center>               <a class=\"create-account-link clickable\" tabindex=\"0\" (keyup.enter)=\"sendSMS()\"                (click)=\"sendSMS()\" title=\"{{ translate('login.registration-form-simple.remember_password.send_sms') }}\">{{ translate('login.registration-form-simple.remember_password.send_sms') }}</a>             </ion-item>           </ion-col>           <ion-col col-sm-5>             <ion-item text-center>               <a class=\"create-account-link clickable\" tabindex=\"0\" (keyup.enter)=\"sendEmail()\"                (click)=\"sendEmail()\" title=\"{{ translate('login.registration-form-simple.remember_password.send_email') }}\">{{ translate('login.registration-form-simple.remember_password.send_email') }}</a>             </ion-item>           </ion-col>         </ion-row>        </ion-grid>        <br>      </ion-card>      <!-- <ion-row > -->     <ion-col col-12 col-xl-4 offset-xl-4>       <ion-card *ngIf=\"registrationSuccessMessageManager\">         <ion-item>           <ion-icon class=\"clickable\" small item-end [name]=\"getIcon(6)\" tabindex=\"0\" (keyup.enter)=\"onCancellation($event)\"            (click)=\"onCancellation($event)\"></ion-icon>         </ion-item>         <ion-card-content text-center>           <ion-row>             <ion-col col-xl-8 offset-xl-2 offset-xs-8 col-xs-2 class=\"titleCard\">               <div class=\"icon-cancel-finish\">                 <ion-icon [name]=\"getIcon(7)\"></ion-icon>               </div>               <div class=\"title-cancel-finish\">                 <h4>{{ translate('login.registration-form-simple.new_account.agent_exit.title') }}</h4>               </div>               <span class=\"fmm-body2 grey1\">{{ translate('login.registration-form-simple.new_account.agent_exit.description') }}</span>             </ion-col>           </ion-row>         </ion-card-content>          <br>        </ion-card>     </ion-col>     <!-- </ion-row> -->      <blocked-user-check-mail        *ngIf=\"emailSelected\"        [source]=\"componentName\">     </blocked-user-check-mail>      <blocked-user-check-sms *ngIf=\"smsSelected\" [source]=\"componentName\" [needsPasswordChange]=\"false\">     </blocked-user-check-sms>    </ion-col> </ion-row>"
                },] },
    ];
    SimpleRegistrationFormComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
        { type: UserProvider, },
        { type: FormBuilder, },
        { type: ModalController, },
        { type: NavController, },
        { type: Events, },
    ]; };
    SimpleRegistrationFormComponent.propDecorators = {
        'registrationResponse': [{ type: Input },],
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
    };
    return SimpleRegistrationFormComponent;
}());
export { SimpleRegistrationFormComponent };
//# sourceMappingURL=registration-form-simple.js.map