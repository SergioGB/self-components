import { ComponentSettingsProvider } from "../../providers/component-settings/component-settings";
export declare class MapfreNetworksFooterComponent {
    private componentSettingsProvider;
    mocked?: boolean;
    styles?: any;
    componentReady: boolean;
    componentSettings: Object;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider);
    ngOnInit(): void;
}
