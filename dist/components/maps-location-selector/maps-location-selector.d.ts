import { OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Events, LoadingController, Loading, ViewController } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
import { FormatProvider } from '../../providers/format/format';
export declare class MapsLocationSelectorComponent implements OnInit {
    private viewCtrl;
    private platformProvider;
    private formatProvider;
    private geolocation;
    private events;
    private loadingCtrl;
    private translationProvider;
    map: any;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    componentReady: boolean;
    firstTime: boolean;
    geocoder: any;
    loadingComponent: Loading;
    centerMarkerElement: any;
    locationLabelElement: any;
    constructor(viewCtrl: ViewController, platformProvider: PlatformProvider, formatProvider: FormatProvider, geolocation: Geolocation, events: Events, loadingCtrl: LoadingController, translationProvider: TranslationProvider);
    ngOnInit(): void;
    addMapsScript(): void;
    selectCurrentPosition(): void;
    selectAddress(): void;
    dismiss(): void;
    private initializeMap;
    private triggerResize;
    private injectMapOverlays;
    private getCurrentPosition;
    private getSelectedLocation;
    private displaySelectedLocation;
    private getAddressFromComponents;
    private getLocationProperty;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
