import { ModalController } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
export declare class ModalsDisplayComponent {
    private translationProvider;
    private platformProvider;
    private modalController;
    mocked?: boolean;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, platformProvider: PlatformProvider, modalController: ModalController);
    getModals(): any[];
    displayModal(modal: any): void;
}
