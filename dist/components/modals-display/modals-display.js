import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { modals } from './modals-list';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
var ModalsDisplayComponent = (function () {
    function ModalsDisplayComponent(translationProvider, platformProvider, modalController) {
        this.translationProvider = translationProvider;
        this.platformProvider = platformProvider;
        this.modalController = modalController;
        this.translationProvider.bind(this);
        this.platformProvider.bindTo(this);
    }
    ModalsDisplayComponent.prototype.getModals = function () {
        var _this = this;
        return modals.filter(function (modal) {
            return !modal.appOnly || _this.platformProvider.onApp();
        });
    };
    ModalsDisplayComponent.prototype.displayModal = function (modal) {
        this.modalController.create(modal.view, { mocked: this.mocked }, { cssClass: modal.class }).present();
    };
    ModalsDisplayComponent.configInputs = ['mocked'];
    ModalsDisplayComponent.decorators = [
        { type: Component, args: [{
                    selector: 'modals-display',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">    <ion-card-header>     <ion-item>       <h3>         {{ translate('modals-display.modals_display.title') }}       </h3>     </ion-item>   </ion-card-header>    <ion-card-content>     <ion-grid class=\"no-padding\">       <ion-row class=\"no-padding\">         <ion-col *ngFor=\"let modal of getModals()\">           <button ion-button full (click)=\"displayModal(modal)\" [disabled]=\"modal.disabled\">             {{ modal.label }}           </button>         </ion-col>       </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    ModalsDisplayComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: PlatformProvider, },
        { type: ModalController, },
    ]; };
    ModalsDisplayComponent.propDecorators = {
        'mocked': [{ type: Input },],
    };
    return ModalsDisplayComponent;
}());
export { ModalsDisplayComponent };
//# sourceMappingURL=modals-display.js.map