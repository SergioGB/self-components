import { AddReceiptsToPayModalComponent } from '../_modals/_receipts/add-receipts-to-pay-modal/add-receipts-to-pay-modal';
import { AmountDivisionsModalComponent } from '../_modals/_receipts/amount-divisions-modal/amount-divisions-modal';
import { AppointmentModalComponent } from '../_modals/appointment-modal/appointment-modal';
import { AssistanceCancellationModalComponent } from '../_modals/_assistances/assistance-cancellation-modal/assistance-cancellation-modal';
import { BackWarningModalComponent } from '../_modals/back-warning-modal/back-warning-modal';
import { CalendarMeetingModalComponent } from '../_modals/calendar-meeting-modal/calendar-meeting-modal';
import { CallMeModalComponent } from '../_modals/_contact/call-me-modal/call-me-modal';
import { CallMeResponseModalComponent } from '../_modals/_contact/call-me-response-modal/call-me-response-modal';
import { CancelSinisterModalComponent } from '../_modals/_claims/cancel-sinister-modal/cancel-sinister-modal';
import { ChangedPasswordModalComponent } from '../_modals/_user/changed-password-modal/changed-password-modal';
import { ChangedPinModalComponent } from '../_modals/_user/changed-pin-modal/changed-pin-modal';
import { ChangedSecurityQuestionModalComponent } from '../_modals/_user/changed-security-question-modal/changed-security-question-modal';
import { ChangesSuccessModalComponent } from '../_modals/_user/changes-success-modal/changes-success-modal';
import { CloseSessionModalComponent } from '../_modals/_user/close-session-modal/close-session-modal';
import { CommunicationsModalComponent } from '../_modals/_communications/communications-modal/communications-modal';
import { ConditionsAcceptModalComponent } from '../_modals/_claims/conditions-accept-modal/conditions-accept-modal';
import { DocumentAdhocModalComponent } from '../_modals/document-adhoc-modal/document-adhoc-modal';
import { FileUploaderModalComponent } from '../_modals/file-uploader-modal/file-uploader-modal';
import { FooterModalComponent } from '../_modals/footer-modal/footer-modal';
import { FormResponseModalComponent } from '../_modals/_receipts/form-response-modal/form-response-modal';
import { HappyOrNotModalComponent } from '../_modals/_claims/happy-or-not-modal/happy-or-not-modal';
import { HtmlContentModalComponent } from '../_modals/html-content-modal/html-content-modal';
import { ImageSliderModalComponent } from '../_modals/image-slider-modal/image-slider-modal';
import { IndemnifyDetailModalComponent } from '../_modals/_claims/indemnify-detail-modal/indemnify-detail-modal';
import { InformationModalComponent } from '../_modals/information-modal/information-modal';
import { LanguageChangedSuccessModalComponent } from '../_modals/language-changed-success-modal/language-changed-success-modal';
import { MailMeModalComponent } from '../_modals/_contact/mail-me-modal/mail-me-modal';
import { ModifyCardPopupComponent } from '../modify-card-popup/modify-card-popup';
import { NotIncludeCoveragesModalComponent } from '../_modals/_assistances/not-include-coverages-modal/not-include-coverages-modal';
import { NoticeNonCoverageModalComponent } from '../_modals/_claims/notice-non-coverage-modal/notice-non-coverage-modal';
import { OfflineModalComponent } from '../_modals/offline-modal/offline-modal';
import { OpenPaymentModalComponent } from '../_modals/_receipts/open-payment-modal/open-payment-modal';
import { PaymentGroupingComponent } from '../payment-grouping/payment-grouping';
import { PolicyCancelModalComponent } from '../_modals/_policies/policy-cancel-modal/policy-cancel-modal';
import { PolicyExtensionModalComponent } from '../_modals/_policies/policy-extension-modal/policy-extension-modal';
import { PolicyUpdateFailureModalComponent } from '../_modals/_policies/update-feedback/policy-update-failure-modal/policy-update-failure-modal';
import { PolicyUpdateSuccessWithPaymentModalComponent } from '../_modals/_policies/update-feedback/policy-update-success-with-payment-modal/policy-update-success-with-payment-modal';
import { PolicyUpdateSuccessWithoutPaymentModalComponent } from '../_modals/_policies/update-feedback/policy-update-success-without-payment-modal/policy-update-success-without-payment-modal';
import { ProcessConfirmationModalComponent } from '../_modals/process-confirmation-modal/process-confirmation-modal';
import { ProcessFailureModalComponent } from '../_modals/process-failure-modal/process-failure-modal';
import { ProfessionalMeetingConfirmationModalComponent } from '../_modals/professional-meeting-confirmation-modal/professional-meeting-confirmation-modal';
import { ProfessionalMeetingModalComponent } from '../_modals/professional-meeting-modal/professional-meeting-modal';
import { ProfileImageModalComponent } from '../_modals/_user/profile-image-modal/profile-image-modal';
import { RefuseRenewalModalComponent } from '../_modals/_policies/refuse-renewal-modal/refuse-renewal-modal';
import { RenovationModalComponent } from '../_modals/renovation-modal/renovation-modal';
import { RequestAssistanceCancelModalComponent } from '../_modals/_assistances/request-assistance-cancel-modal/request-assistance-cancel-modal';
import { SaveIncidenceModalComponent } from '../_modals/save-incidence-modal/save-incidence-modal';
import { UrgentAssistanceModalComponent } from '../_modals/_assistances/urgent-assistance-modal/urgent-assistance-modal';
import { VideoModalComponent } from '../_modals/video-modal/video-modal';
import { WelcomePackModalComponent } from '../_modals/welcome-pack-modal/welcome-pack-modal';
import { AttendanceTrackingModalComponent } from '../_modals/_assistances/attendance-tracking-modal/attendance-tracking-modal';
export var modals = [{
        label: 'Add Receipts To Payment',
        view: AddReceiptsToPayModalComponent
    }, {
        label: 'Appointment',
        view: AppointmentModalComponent
    }, {
        label: 'Assistance Cancellation',
        view: AssistanceCancellationModalComponent
    }, {
        label: 'Attendance Tracking',
        view: AttendanceTrackingModalComponent
    }, {
        label: 'Back Warning',
        view: BackWarningModalComponent
    }, {
        label: 'Calendar Meeting',
        view: CalendarMeetingModalComponent,
        disabled: true
    }, {
        label: 'Call Me Response',
        view: CallMeResponseModalComponent
    }, {
        label: 'Call Me',
        view: CallMeModalComponent
    }, {
        label: 'Cancel Sinister',
        view: CancelSinisterModalComponent
    }, {
        label: 'Changed Password',
        view: ChangedPasswordModalComponent
    }, {
        label: 'Changed Pin',
        view: ChangedPinModalComponent,
        appOnly: true
    }, {
        label: 'Changed Security Question',
        view: ChangedSecurityQuestionModalComponent
    }, {
        label: 'Changes Success',
        view: ChangesSuccessModalComponent
    }, {
        label: 'Close Session',
        view: CloseSessionModalComponent
    }, {
        label: 'Communications',
        view: CommunicationsModalComponent
    }, {
        label: 'Conditions Accept',
        view: ConditionsAcceptModalComponent
    }, {
        label: 'Document Ad hoc',
        view: DocumentAdhocModalComponent
    }, {
        label: 'File Upload',
        view: FileUploaderModalComponent
    }, {
        label: 'Footer Modal',
        view: FooterModalComponent
    }, {
        label: 'Form Response',
        view: FormResponseModalComponent
    }, {
        label: 'HTML Content',
        view: HtmlContentModalComponent
    }, {
        label: 'Happy Or Not',
        view: HappyOrNotModalComponent
    }, {
        label: 'Image Slider',
        view: ImageSliderModalComponent,
        class: 'no-mobile-fullscreen'
    }, {
        label: 'Incidence Professional Meeting Confirmation',
        view: ProfessionalMeetingConfirmationModalComponent
    }, {
        label: 'Indemnify Detail',
        view: IndemnifyDetailModalComponent
    }, {
        label: 'Information Modal',
        view: InformationModalComponent
    }, {
        label: 'Language Change Success',
        view: LanguageChangedSuccessModalComponent
    }, {
        label: 'Mail Me',
        view: MailMeModalComponent
    }, {
        label: 'Not Include Coverages',
        view: NotIncludeCoveragesModalComponent
    }, {
        label: 'Notice Non Coverage',
        view: NoticeNonCoverageModalComponent
    }, {
        label: 'Offline',
        view: OfflineModalComponent
    }, {
        label: 'Open Payment',
        view: OpenPaymentModalComponent
    }, {
        label: 'Payment Amount Divisions',
        view: AmountDivisionsModalComponent
    }, {
        label: 'Payment Card Modification',
        view: ModifyCardPopupComponent
    }, {
        label: 'Payment Grouping',
        view: PaymentGroupingComponent
    }, {
        label: 'Policy Cancellation',
        view: PolicyCancelModalComponent
    }, {
        label: 'Policy Extension',
        view: PolicyExtensionModalComponent
    }, {
        label: 'Policy Update - Failure',
        view: PolicyUpdateFailureModalComponent
    }, {
        label: 'Policy Update - Success w/ payment',
        view: PolicyUpdateSuccessWithPaymentModalComponent
    }, {
        label: 'Policy Update - Success w/o payment',
        view: PolicyUpdateSuccessWithoutPaymentModalComponent
    }, {
        label: 'Process Confirmation',
        view: ProcessConfirmationModalComponent
    }, {
        label: 'Process Failure',
        view: ProcessFailureModalComponent
    }, {
        label: 'Professional Meeting',
        view: ProfessionalMeetingModalComponent
    }, {
        label: 'Profile Image',
        view: ProfileImageModalComponent
    }, {
        label: 'Refuse Renewal (policy)',
        view: RefuseRenewalModalComponent
    }, {
        label: 'Renovation Modal',
        view: RenovationModalComponent
    }, {
        label: 'Request Assistance Cancel',
        view: RequestAssistanceCancelModalComponent
    }, {
        label: 'Save Incidence',
        view: SaveIncidenceModalComponent
    }, {
        label: 'Urgent Assistance',
        view: UrgentAssistanceModalComponent
    }, {
        label: 'Video',
        view: VideoModalComponent,
        class: 'video-modal-wrapper fullscreen'
    }, {
        label: 'Welcome Pack',
        view: WelcomePackModalComponent
    }];
//# sourceMappingURL=modals-list.js.map