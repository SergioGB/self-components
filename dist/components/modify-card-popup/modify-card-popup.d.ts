import { ElementRef } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { ModalProvider } from '../../providers/modal/modal';
import { TranslationProvider } from '../../providers/translation/translation';
export declare class ModifyCardPopupComponent {
    private viewCtrl;
    private modalProvider;
    private navParams;
    private elementRef;
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    labels: any;
    defaultIcons: string[];
    static configInputs: string[];
    constructor(viewCtrl: ViewController, modalProvider: ModalProvider, navParams: NavParams, elementRef: ElementRef, translationProvider: TranslationProvider);
    dismiss(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
