export declare class NearestOfficeMocks {
    private static nearestOfficeResponse;
    static getNearestOfficeResponse(): any;
    private static defaultIcons;
    static getDefaultIcons(): any;
}
