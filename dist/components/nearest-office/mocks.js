import { Injectable } from '@angular/core';
var NearestOfficeMocks = (function () {
    function NearestOfficeMocks() {
    }
    NearestOfficeMocks.getNearestOfficeResponse = function () {
        return this.nearestOfficeResponse;
    };
    NearestOfficeMocks.getDefaultIcons = function () {
        return this.defaultIcons;
    };
    NearestOfficeMocks.nearestOfficeResponse = {
        "response": {
            "code": 0,
            "message": "string"
        },
        "data": {
            'name': 'MAPFRE ABRAXAS',
            'address': '1 Victoria Street, Bristol Bridge, Bristol, BS1 6AA',
            'phone': '01454 616 300',
            'agent': 'Luis Alberto Elvira Velázquez'
        }
    };
    NearestOfficeMocks.defaultIcons = ["mapfre-location", "mapfre-incoming-call", "mapfre-incoming-call"];
    NearestOfficeMocks.decorators = [
        { type: Injectable },
    ];
    NearestOfficeMocks.ctorParameters = function () { return []; };
    return NearestOfficeMocks;
}());
export { NearestOfficeMocks };
//# sourceMappingURL=mocks.js.map