import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
export declare class NearestOfficeComponent implements OnInit {
    private translationProvider;
    private componentSettingsProvider;
    mocked?: boolean;
    styles?: string;
    icons?: string[];
    static component_name: string;
    static configInputs: string[];
    private componentReady;
    private office;
    constructor(translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider);
    ngOnInit(): void;
    call(phoneNumber: string): void;
    openAddress(url: string): void;
    getIcon(position: number): any;
    existIcon(position: number): string;
}
