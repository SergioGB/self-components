import { ViewEncapsulation } from '@angular/core';
import { Component, Input, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { UserProvider } from '../../providers/user/user';
import * as moment from 'moment';
import { ConfigProvider } from '../../models/self-component.config';
var NewPaymentMethodFormV1Component = (function () {
    function NewPaymentMethodFormV1Component(translationProvider, componentSettingsProvider, formBuilder, events, userProvider, elementRef) {
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.formBuilder = formBuilder;
        this.events = events;
        this.userProvider = userProvider;
        this.elementRef = elementRef;
        this.defaultIcons = ["mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check"];
        this.translationProvider.bind(this);
        this.numberPattern = ConfigProvider.config.numberPattern;
        this.spacedNumberPattern = ConfigProvider.config.spacedNumberPattern;
        this.initializeForms();
    }
    NewPaymentMethodFormV1Component.prototype.initializeForms = function () {
        var _this = this;
        this.isTaker = true;
        this.newCardForm = this.formBuilder.group({
            card_holder: ['', [Validators.required]],
            card_number: ['', [Validators.required]],
            expiration_month: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
            expiration_year: ['', [Validators.required, Validators.min(1)]],
            card_verification: ['', [Validators.required, Validators.min(100)]]
        });
        this.userProvider.getAppUserData().then(function (userData) {
            _this.BankAccountTakerForm = _this.formBuilder.group({
                iban1: ['', [Validators.required, Validators.pattern('^[0-9]{2}?$')]],
                iban2: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
                iban3: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
                iban4: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
                iban5: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
                iban6: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
                iban: [''],
                name: [userData && userData.personal_data ? userData.personal_data.name : '', [Validators.required]],
                lastname1: [userData && userData.personal_data ? userData.personal_data.surname1 : '', [Validators.required]],
                lastname2: [userData && userData.personal_data ? userData.personal_data.surname2 : '', [Validators.required]]
            });
            _this.BankAccountTakerForm.get('iban1').valueChanges.subscribe(function () {
                setTimeout(function () {
                    _this.nextIban(_this.BankAccountTakerForm.value['iban1'], 2, _this.iban2);
                }, 0);
            });
            _this.BankAccountTakerForm.get('iban2').valueChanges.subscribe(function () {
                setTimeout(function () {
                    _this.nextIban(_this.BankAccountTakerForm.value['iban2'], 4, _this.iban3);
                }, 0);
            });
            _this.BankAccountTakerForm.get('iban3').valueChanges.subscribe(function () {
                setTimeout(function () {
                    _this.nextIban(_this.BankAccountTakerForm.value['iban3'], 4, _this.iban4);
                }, 0);
            });
            _this.BankAccountTakerForm.get('iban4').valueChanges.subscribe(function () {
                setTimeout(function () {
                    _this.nextIban(_this.BankAccountTakerForm.value['iban4'], 4, _this.iban5);
                }, 0);
            });
            _this.BankAccountTakerForm.get('iban5').valueChanges.subscribe(function () {
                setTimeout(function () {
                    _this.nextIban(_this.BankAccountTakerForm.value['iban5'], 4, _this.iban6);
                }, 0);
            });
        });
        this.BankAccountForm = this.formBuilder.group({
            iban1: ['', [Validators.required, Validators.pattern('^[0-9]{2}?$')]],
            iban2: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
            iban3: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
            iban4: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
            iban5: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
            iban6: ['', [Validators.required, Validators.pattern('^[0-9]{4}?$')]],
            iban: [''],
            name: ['', [Validators.required]],
            lastname1: ['', [Validators.required]],
            lastname2: ['', [Validators.required]]
        });
        this.BankAccountForm.get('iban1').valueChanges.subscribe(function () {
            setTimeout(function () {
                _this.nextIban(_this.BankAccountForm.value['iban1'], 2, _this.iban2);
            }, 0);
        });
        this.BankAccountForm.get('iban2').valueChanges.subscribe(function () {
            setTimeout(function () {
                _this.nextIban(_this.BankAccountForm.value['iban2'], 4, _this.iban3);
            }, 0);
        });
        this.BankAccountForm.get('iban3').valueChanges.subscribe(function () {
            setTimeout(function () {
                _this.nextIban(_this.BankAccountForm.value['iban3'], 4, _this.iban4);
            }, 0);
        });
        this.BankAccountForm.get('iban4').valueChanges.subscribe(function () {
            setTimeout(function () {
                _this.nextIban(_this.BankAccountForm.value['iban4'], 4, _this.iban5);
            }, 0);
        });
        this.BankAccountForm.get('iban5').valueChanges.subscribe(function () {
            setTimeout(function () {
                _this.nextIban(_this.BankAccountForm.value['iban5'], 4, _this.iban6);
            }, 0);
        });
    };
    NewPaymentMethodFormV1Component.prototype.selectMethodType = function (methodType) {
        this.selectedMethodType = methodType;
    };
    NewPaymentMethodFormV1Component.prototype.changeTaker = function (value) {
        this.isTaker = value;
    };
    NewPaymentMethodFormV1Component.prototype.cancel = function (event) {
        this.preventDefault(event);
        delete this.selectedMethodType;
        this.emitCreation();
    };
    NewPaymentMethodFormV1Component.prototype.submit = function (event) {
        this.preventDefault(event);
        this.attemptSubmission();
    };
    NewPaymentMethodFormV1Component.prototype.preventDefault = function (event) {
        if (event) {
            event.preventDefault();
        }
    };
    NewPaymentMethodFormV1Component.prototype.attemptSubmission = function () {
        var _this = this;
        var paymentMethodRequest;
        if (this.selectedMethodType === 1) {
            var formData = this.newCardForm.value;
            paymentMethodRequest = {
                type: this.selectedMethodType,
                credit_card_info: {
                    cardholder_name: formData.card_holder,
                    card_number: formData.card_number,
                    expiration_date: moment(formData.expiration_month + "/" + formData.expiration_year, 'MM/YY').toDate(),
                    cvv: formData.card_verification
                }
            };
        }
        else {
            if (this.isTaker) {
                var formData = this.BankAccountTakerForm.value;
                this.BankAccountTakerForm['iban'] = 'ES' + formData['iban1'] + ' ' + formData['iban2'] + ' ' + formData['iban3'] + ' ' + formData['iban4'] + ' ' + formData['iban5'] + ' ' + formData['iban6'];
                paymentMethodRequest = {
                    type: this.selectedMethodType,
                    bank_account_info: {
                        account_number: this.BankAccountTakerForm['iban'],
                        entity: "EVO BANCO"
                    }
                };
            }
            else {
                var formData = this.BankAccountForm.value;
                this.BankAccountForm['iban'] = 'ES' + formData['iban1'] + ' ' + formData['iban2'] + ' ' + formData['iban3'] + ' ' + formData['iban4'] + ' ' + formData['iban5'] + ' ' + formData['iban6'];
                paymentMethodRequest = {
                    type: this.selectedMethodType,
                    bank_account_info: {
                        account_number: this.BankAccountForm['iban'],
                        entity: "EVO BANCO"
                    }
                };
            }
        }
        this.componentSettingsProvider.postPaymentMethod(this.mocked, paymentMethodRequest).subscribe(function (response) {
            if (_this.componentSettingsProvider.isResponseValid(response)) {
                _this.emitCreation(paymentMethodRequest);
                _this.reset();
            }
        });
    };
    NewPaymentMethodFormV1Component.prototype.emitCreation = function (paymentMethod) {
        this.events.publish(NewPaymentMethodFormV1Component.submissionEvent, paymentMethod);
    };
    NewPaymentMethodFormV1Component.prototype.reset = function () {
        delete this.selectedMethodType;
        this.initializeForms();
    };
    NewPaymentMethodFormV1Component.prototype.onBlur = function () {
        var wrongIban = this.BankAccountForm.value.iban &&
            !this.BankAccountForm.controls['iban'].valid && this.BankAccountForm.controls['iban'].dirty;
        var wrongName = this.BankAccountForm.value.entity &&
            !this.BankAccountForm.controls['name'].valid && this.BankAccountForm.controls['name'].dirty;
        var wrongLastName1 = this.BankAccountForm.value.office &&
            !this.BankAccountForm.controls['lastname1'].valid && this.BankAccountForm.controls['lastname1'].dirty;
        var wrongLastName2 = this.BankAccountForm.value.control_digit &&
            !this.BankAccountForm.controls['lastname2'].valid && this.BankAccountForm.controls['lastname2'].dirty;
        this.wrongAccountNumber = (wrongIban || wrongName || wrongLastName1 || wrongLastName2);
    };
    NewPaymentMethodFormV1Component.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    NewPaymentMethodFormV1Component.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    NewPaymentMethodFormV1Component.prototype.nextIban = function (value, length, nextElement) {
        if (value.length === length) {
            nextElement.nativeElement.firstElementChild.focus();
        }
    };
    NewPaymentMethodFormV1Component.componentName = 'NewPaymentMethodFormV1Component';
    NewPaymentMethodFormV1Component.submissionEvent = NewPaymentMethodFormV1Component.componentName + ":submit";
    NewPaymentMethodFormV1Component.configInputs = ['mocked', 'icons', 'styles'];
    NewPaymentMethodFormV1Component.decorators = [
        { type: Component, args: [{
                    selector: 'new-payment-method-form-v1',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["new-payment-method-form-v1 input.text-input {   width: 100%; }  new-payment-method-form-v1 .iban-cols ion-col {   justify-self: flex-end; }  new-payment-method-form-v1 .span-align-down span {   display: flex;   align-self: center; }"],
                    template: "<ion-item tabindex=\"0\" (tap)=\"selectMethodType(1)\"   class=\"flex no-horizontal-padding align-start clickable no-label-margin\" [ngStyle]=\"styles\">   <ion-icon item-start large [name]=\"selectedMethodType === 1 ? getIcon(0) : getIcon(1)\"     [class.green]=\"selectedMethodType === 1\">   </ion-icon>    <h3 class=\"primary-text no-margin\">     {{ translate('payment-method-selection.new-payment-method-form-v1.payment-method-creation.card.title') }}   </h3> </ion-item>    <!-- card form --> <form *ngIf=\"selectedMethodType === 1\" [formGroup]=\"newCardForm\" class=\"padding-top-m\">   <ion-grid class=\"no-padding values-list unlabeled-form\">      <ion-row no-padding>        <!-- card_holder -->       <ion-col col-12 col-sm-6 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input type=\"text\" maxlength=\"50\" formControlName=\"card_holder\"             [placeholder]=\"translate('payment-method-selection.new-payment-method-form-v1.payment-method-creation.card.card_holder') + '*'\">           </ion-input>         </ion-item>       </ion-col>        <!-- card_number -->        <ion-col col-12 col-sm-6 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input type=\"text\" maxlength=\"25\" formControlName=\"card_number\" [pattern]=\"spacedNumberPattern\"             [placeholder]=\"translate('payment-method-selection.new-payment-method-form-v1.payment-method-creation.card.card_number') + '*'\">           </ion-input>         </ion-item>       </ion-col>      </ion-row>      <ion-row no-padding>        <!-- expiration_month -->       <ion-col col-4 col-sm-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input type=\"text\" maxlength=\"2\" formControlName=\"expiration_month\" [pattern]=\"numberPattern\"             [placeholder]=\"translate('payment-method-selection.new-payment-method-form-v1.payment-method-creation.card.expiration_month') + '*'\">           </ion-input>         </ion-item>       </ion-col>        <!-- expiration_year -->       <ion-col col-4 col-sm-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input type=\"text\" maxlength=\"2\" formControlName=\"expiration_year\" [pattern]=\"numberPattern\"             [placeholder]=\"translate('payment-method-selection.new-payment-method-form-v1.payment-method-creation.card.expiration_year') + '*'\">           </ion-input>         </ion-item>       </ion-col>        <!-- card_verification -->       <ion-col col-4 col-sm-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input type=\"text\" maxlength=\"3\" formControlName=\"card_verification\" [pattern]=\"numberPattern\"             [placeholder]=\"translate('payment-method-selection.new-payment-method-form-v1.payment-method-creation.card.card_verification') + '*'\">           </ion-input>         </ion-item>       </ion-col>      </ion-row>        </ion-grid> </form> <ion-item tabindex=\"0\" (tap)=\"selectMethodType(2)\"   class=\"flex no-horizontal-padding align-start clickable no-label-margin margin-top-m\">   <ion-icon large item-start [name]=\"selectedMethodType === 2 ? getIcon(2) : getIcon(3)\"     [class.green]=\"selectedMethodType === 2\">   </ion-icon>    <h3 class=\"primary-text no-margin\">     {{ translate('payment-method-selection.new-payment-method-form-v1.payment-method-creation.bank_account.title') }}   </h3> </ion-item> <!-- bank account form --> <div *ngIf=\"selectedMethodType === 2\">   <ion-grid  no-padding>     <p>{{ translate('payment-method-selection-v1.bank_account.question') }}</p>     <ion-item (click)=\"changeTaker(true)\" no-padding tabindex=\"0\"       class=\"margin-bottom-s margin-top-s flex align-start no-label-margin clickable\">       <ion-icon item-start large [name]=\"isTaker ? getIcon(2) : getIcon(3)\" [class.green]=\"isTaker\">       </ion-icon>       <h4 class=\"primary-text vertical-margin-xs\">         {{ translate('payment-method-selection-v1.form.yes') }}       </h4>     </ion-item>     <form *ngIf=\"isTaker\" [formGroup]=\"BankAccountTakerForm\">       <ion-grid class=\"no-padding\">          <ion-row no-padding>            <!-- Name -->           <ion-col col-12 col-md-4 class=\"input-col vertical-margin-s\">             <ion-item no-padding class=\"required\">               <ion-label floating> {{ translate('payment-method-selection-v1.form.name') }}</ion-label>               <ion-input type=\"text\" maxlength=\"50\" formControlName=\"name\" disabled>               </ion-input>             </ion-item>           </ion-col>            <!-- LastName1 -->           <ion-col col-12 col-md-8 class=\"input-col vertical-margin-s\">             <ion-item no-padding class=\"required\">               <ion-label floating>{{ translate('payment-method-selection-v1.form.surname1') }}</ion-label>               <ion-input type=\"text\" formControlName=\"lastname1\" disabled>               </ion-input>             </ion-item>           </ion-col>            <!-- LastName2 -->           <ion-col col-12 col-md-4 class=\"input-col vertical-margin-s\">             <ion-item no-padding class=\"required\">               <ion-label floating> {{ translate('payment-method-selection-v1.form.surname2') }}</ion-label>               <ion-input type=\"text\" formControlName=\"lastname2\" disabled>               </ion-input>             </ion-item>           </ion-col>           <!-- iban -->           <ion-col class=\"flex\" col-12 col-md-8>               <ion-row no-padding class=\"iban-cols span-align-down\">                 <span>ES</span>                 <ion-col col-12 col-md-1 class=\"input-col padding-bottom-xxs\">                   <ion-item no-padding>                     <div>ES</div>                     <ion-input #IBAN1 type=\"text\" formControlName=\"iban1\" maxlength=\"2\" ></ion-input>                   </ion-item>                 </ion-col>                 <span>/</span>                 <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                   <ion-item no-padding>                     <ion-input #IBAN2 type=\"text\" formControlName=\"iban2\" maxlength=\"4\" ></ion-input>                   </ion-item>                 </ion-col>                 <span>/</span>                 <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                   <ion-item no-padding>                     <ion-input #IBAN3 type=\"text\" formControlName=\"iban3\" maxlength=\"4\" ></ion-input>                   </ion-item>                 </ion-col>                 <span>/</span>                 <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                   <ion-item no-padding>                     <ion-input #IBAN4 type=\"text\" formControlName=\"iban4\" maxlength=\"4\" ></ion-input>                   </ion-item>                 </ion-col>                 <span>/</span>                 <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                   <ion-item no-padding>                     <ion-input #IBAN5 type=\"text\" formControlName=\"iban5\" maxlength=\"4\" ></ion-input>                   </ion-item>                 </ion-col>                 <span>/</span>                 <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                   <ion-item no-padding>                     <ion-input #IBAN6 type=\"text\" formControlName=\"iban6\" maxlength=\"4\" ></ion-input>                   </ion-item>                 </ion-col>               </ion-row>             </ion-col>         </ion-row>         <ion-row no-padding>           <div *ngIf=\"wrongAccountNumber\" class=\"input-value-error\">             <span>               {{ translate('payment-method-selection.new-payment-method-form.payment-method-creation.bank_account.format_error') }}             </span>           </div>         </ion-row>         <ion-row no-padding class=\"margin-top-m\">                </ion-row>       </ion-grid>     </form>     <ion-item (click)=\"changeTaker(false)\" no-padding tabindex=\"0\"       class=\"flex align-start no-label-margin clickable\">       <ion-icon item-start large [name]=\"!isTaker ? getIcon(2) : getIcon(3)\" [class.green]=\"!isTaker\">       </ion-icon>       <h4 class=\"primary-text vertical-margin-xs\">         {{ translate('payment-method-selection-v1.form.no') }}       </h4>     </ion-item>     <form *ngIf=\"!isTaker\" [formGroup]=\"BankAccountForm\">       <ion-grid class=\"no-padding\">          <ion-row no-padding>            <!-- Name -->           <ion-col col-12 col-md-4 class=\"input-col vertical-margin-s\">             <ion-item no-padding class=\"required\">               <ion-label floating> {{ translate('payment-method-selection-v1.form.name') }}</ion-label>               <ion-input type=\"text\" maxlength=\"50\" formControlName=\"name\">               </ion-input>             </ion-item>           </ion-col>            <!-- LastName1 -->           <ion-col col-12 col-md-8 class=\"input-col vertical-margin-s\">             <ion-item no-padding class=\"required\">               <ion-label floating> {{ translate('payment-method-selection-v1.form.surname1') }}</ion-label>               <ion-input type=\"text\" formControlName=\"lastname1\">               </ion-input>             </ion-item>           </ion-col>            <!-- LastName2 -->           <ion-col col-12 col-md-4 class=\"input-col vertical-margin-s\">             <ion-item no-padding class=\"required\">               <ion-label floating> {{ translate('payment-method-selection-v1.form.surname2') }}</ion-label>               <ion-input type=\"text\" formControlName=\"lastname2\">               </ion-input>             </ion-item>           </ion-col>           <!-- iban -->           <ion-col class=\"flex\" col-12 col-md-8>             <ion-row no-padding class=\"iban-cols span-align-down\">               <span>ES</span>               <ion-col col-12 col-md-1 class=\"input-col padding-bottom-xxs\">                 <ion-item no-padding>                   <div>ES</div>                   <ion-input #IBAN1 type=\"text\" formControlName=\"iban1\" maxlength=\"2\" ></ion-input>                 </ion-item>               </ion-col>               <span>/</span>               <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                 <ion-item no-padding>                   <ion-input #IBAN2 type=\"text\" formControlName=\"iban2\" maxlength=\"4\" ></ion-input>                 </ion-item>               </ion-col>               <span>/</span>               <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                 <ion-item no-padding>                   <ion-input #IBAN3 type=\"text\" formControlName=\"iban3\" maxlength=\"4\" ></ion-input>                 </ion-item>               </ion-col>               <span>/</span>               <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                 <ion-item no-padding>                   <ion-input #IBAN4 type=\"text\" formControlName=\"iban4\" maxlength=\"4\" ></ion-input>                 </ion-item>               </ion-col>               <span>/</span>               <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                 <ion-item no-padding>                   <ion-input #IBAN5 type=\"text\" formControlName=\"iban5\" maxlength=\"4\" ></ion-input>                 </ion-item>               </ion-col>               <span>/</span>               <ion-col col-12 col-md-2 class=\"input-col padding-bottom-xxs\">                 <ion-item no-padding>                   <ion-input #IBAN6 type=\"text\" formControlName=\"iban6\" maxlength=\"4\" ></ion-input>                 </ion-item>               </ion-col>             </ion-row>           </ion-col>         </ion-row>        </ion-grid>     </form>    </ion-grid>  </div>  <ion-row no-padding class=\"margin-top-m\" *ngIf=\"selectedMethodType === 2 && isTaker\">    <ion-col col-6 col-sm-5 col-md-3>     <button ion-button full text-uppercase class=\"secondary\" (click)=\"cancel($event)\">       {{ translate('payment-method-selection.new-payment-method-form-v1.generic.cancel') }}     </button>   </ion-col>    <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">     <button ion-button full text-uppercase (click)=\"submit($event)\" [disabled]=\"!BankAccountTakerForm.valid\">       {{ translate('payment-method-selection.new-payment-method-form-v1.generic.save') }}     </button>   </ion-col>  </ion-row> <ion-row no-padding class=\"margin-top-m\" *ngIf=\"selectedMethodType === 2 && !isTaker\">    <ion-col col-6 col-sm-5 col-md-3>     <button ion-button full text-uppercase class=\"secondary\" (click)=\"cancel($event)\">       {{ translate('payment-method-selection.new-payment-method-form-v1.generic.cancel') }}     </button>   </ion-col>    <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">     <button ion-button full text-uppercase (click)=\"submit($event)\" [disabled]=\"!BankAccountForm.valid\">       {{ translate('payment-method-selection.new-payment-method-form-v1.generic.save') }}     </button>   </ion-col>  </ion-row>   <ion-row no-padding class=\"margin-top-m\" *ngIf=\"selectedMethodType === 1\">    <ion-col col-6 col-sm-5 col-md-3>     <button ion-button full text-uppercase class=\"secondary\" (click)=\"cancel($event)\">       {{ translate('payment-method-selection.new-payment-method-form-v1.generic.cancel') }}     </button>   </ion-col>    <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">     <button ion-button full text-uppercase (click)=\"submit($event)\" [disabled]=\"!newCardForm.valid\">       {{ translate('payment-method-selection.new-payment-method-form-v1.generic.save') }}     </button>   </ion-col>  </ion-row>"
                },] },
    ];
    NewPaymentMethodFormV1Component.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
        { type: FormBuilder, },
        { type: Events, },
        { type: UserProvider, },
        { type: ElementRef, },
    ]; };
    NewPaymentMethodFormV1Component.propDecorators = {
        'selectedMethodType': [{ type: Input },],
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'iban1': [{ type: ViewChild, args: ['IBAN1', { read: ElementRef },] },],
        'iban2': [{ type: ViewChild, args: ['IBAN2', { read: ElementRef },] },],
        'iban3': [{ type: ViewChild, args: ['IBAN3', { read: ElementRef },] },],
        'iban4': [{ type: ViewChild, args: ['IBAN4', { read: ElementRef },] },],
        'iban5': [{ type: ViewChild, args: ['IBAN5', { read: ElementRef },] },],
        'iban6': [{ type: ViewChild, args: ['IBAN6', { read: ElementRef },] },],
    };
    return NewPaymentMethodFormV1Component;
}());
export { NewPaymentMethodFormV1Component };
//# sourceMappingURL=new-payment-method-form-v1.js.map