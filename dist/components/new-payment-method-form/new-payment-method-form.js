import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import * as moment from 'moment';
import { ConfigProvider } from '../../models/self-component.config';
var NewPaymentMethodFormComponent = (function () {
    function NewPaymentMethodFormComponent(translationProvider, componentSettingsProvider, formBuilder, events) {
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.formBuilder = formBuilder;
        this.events = events;
        this.defaultIcons = ["mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check"];
        this.translationProvider.bind(this);
        this.numberPattern = ConfigProvider.config.numberPattern;
        this.spacedNumberPattern = ConfigProvider.config.spacedNumberPattern;
        this.initializeForms();
    }
    NewPaymentMethodFormComponent.prototype.initializeForms = function () {
        this.newCardForm = this.formBuilder.group({
            card_holder: ['', [Validators.required]],
            card_number: ['', [Validators.required]],
            expiration_month: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
            expiration_year: ['', [Validators.required, Validators.min(1)]],
            card_verification: ['', [Validators.required, Validators.min(100)]]
        });
        this.newBankAccountForm = this.formBuilder.group({
            iban: ['', [Validators.required, Validators.pattern('ES[0-9][0-9]')]],
            entity: ['', [Validators.required]],
            office: ['', [Validators.required]],
            control_digit: ['', [Validators.required]],
            account_number: ['', [Validators.required]]
        });
    };
    NewPaymentMethodFormComponent.prototype.selectMethodType = function (methodType) {
        this.selectedMethodType = methodType;
    };
    NewPaymentMethodFormComponent.prototype.cancel = function (event) {
        this.preventDefault(event);
        this.emitCreation();
    };
    NewPaymentMethodFormComponent.prototype.submit = function (event) {
        this.preventDefault(event);
        this.attemptSubmission();
    };
    NewPaymentMethodFormComponent.prototype.preventDefault = function (event) {
        if (event) {
            event.preventDefault();
        }
    };
    NewPaymentMethodFormComponent.prototype.attemptSubmission = function () {
        var paymentMethodRequest;
        if (this.selectedMethodType === 1) {
            var formData = this.newCardForm.value;
            paymentMethodRequest = {
                payment_method_type: this.selectedMethodType,
                credit_card_info: {
                    cardholder_name: formData.card_holder,
                    card_number: formData.card_number,
                    expiration_date: moment(formData.expiration_month + "/" + formData.expiration_year, 'MM/YY').toDate(),
                    cvv: formData.card_verification
                }
            };
        }
        else {
            var formData = this.newBankAccountForm.value;
            paymentMethodRequest = {
                payment_method_type: this.selectedMethodType,
                bank_account_info: {
                    iban: formData.iban,
                    entity: formData.entity,
                    office: formData.office,
                    control_digit: formData.control_digit,
                    account_number: formData.account_number
                }
            };
        }
        paymentMethodRequest.id = new Date().getTime();
        paymentMethodRequest.type = paymentMethodRequest.payment_method_type;
        this.emitCreation(paymentMethodRequest);
        this.reset();
    };
    NewPaymentMethodFormComponent.prototype.emitCreation = function (paymentMethod) {
        this.events.publish(NewPaymentMethodFormComponent.submissionEvent, paymentMethod);
    };
    NewPaymentMethodFormComponent.prototype.reset = function () {
        delete this.selectedMethodType;
        this.initializeForms();
    };
    NewPaymentMethodFormComponent.prototype.onBlur = function () {
        var wrongIban = this.newBankAccountForm.value.iban &&
            !this.newBankAccountForm.controls['iban'].valid && this.newBankAccountForm.controls['iban'].dirty;
        var wrongEntity = this.newBankAccountForm.value.entity &&
            !this.newBankAccountForm.controls['entity'].valid && this.newBankAccountForm.controls['entity'].dirty;
        var wrongOffice = this.newBankAccountForm.value.office &&
            !this.newBankAccountForm.controls['office'].valid && this.newBankAccountForm.controls['office'].dirty;
        var wrongControlDigit = this.newBankAccountForm.value.control_digit &&
            !this.newBankAccountForm.controls['control_digit'].valid && this.newBankAccountForm.controls['control_digit'].dirty;
        var wrongAccountNumber = this.newBankAccountForm.value.account_number &&
            !this.newBankAccountForm.controls['account_number'].valid && this.newBankAccountForm.controls['account_number'].dirty;
        this.wrongAccountNumber = (wrongIban || wrongEntity || wrongOffice || wrongControlDigit || wrongAccountNumber);
    };
    NewPaymentMethodFormComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    NewPaymentMethodFormComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    NewPaymentMethodFormComponent.componentName = 'new-payment-method-form';
    NewPaymentMethodFormComponent.submissionEvent = NewPaymentMethodFormComponent.componentName + ":submit";
    NewPaymentMethodFormComponent.configInputs = ['mocked', 'icons', 'styles'];
    NewPaymentMethodFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'new-payment-method-form',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["new-payment-method-form input.text-input {   width: 100%; }"],
                    template: "<ion-item tabindex=\"0\" (tap)=\"selectMethodType(1)\" class=\"flex no-horizontal-padding align-start clickable no-label-margin\" [ngStyle]=\"styles\">   <ion-icon     item-start large     [name]=\"selectedMethodType === 1 ? getIcon(0) : getIcon(1)\"     [class.green]=\"selectedMethodType === 1\">   </ion-icon>    <h3 class=\"primary-text no-margin\">     {{ translate('payment-method-selection.new-payment-method-form.payment-method-creation.card.title') }}   </h3> </ion-item>  <ion-item tabindex=\"0\" (tap)=\"selectMethodType(2)\" class=\"flex no-horizontal-padding align-start clickable no-label-margin margin-top-m\">   <ion-icon large     item-start     [name]=\"selectedMethodType === 2 ? getIcon(2) : getIcon(3)\"     [class.green]=\"selectedMethodType === 2\">   </ion-icon>    <h3 class=\"primary-text no-margin\">     {{ translate('payment-method-selection.new-payment-method-form.payment-method-creation.bank_account.title') }}   </h3> </ion-item>  <!-- card form --> <form *ngIf=\"selectedMethodType === 1\" [formGroup]=\"newCardForm\" class=\"padding-top-m\">   <ion-grid class=\"no-padding values-list unlabeled-form\">      <ion-row no-padding>        <!-- card_holder -->       <ion-col col-12 col-sm-6 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"50\"             formControlName=\"card_holder\"             [placeholder]=\"translate('new-payment-method-form.payment-method-creation.card.card_holder') + '*'\">           </ion-input>         </ion-item>       </ion-col>        <!-- card_number -->        <ion-col col-12 col-sm-6 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"25\"             formControlName=\"card_number\"             [pattern]=\"spacedNumberPattern\"             [placeholder]=\"translate('new-payment-method-form.payment-method-creation.card.card_number') + '*'\">           </ion-input>         </ion-item>       </ion-col>      </ion-row>      <ion-row no-padding>        <!-- expiration_month -->       <ion-col col-4 col-sm-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"2\"             formControlName=\"expiration_month\"             [pattern]=\"numberPattern\"             [placeholder]=\"translate('new-payment-method-form.payment-method-creation.card.expiration_month') + '*'\">           </ion-input>         </ion-item>       </ion-col>        <!-- expiration_year -->       <ion-col col-4 col-sm-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"2\"             formControlName=\"expiration_year\"             [pattern]=\"numberPattern\"             [placeholder]=\"translate('new-payment-method-form.payment-method-creation.card.expiration_year') + '*'\">           </ion-input>         </ion-item>       </ion-col>        <!-- card_verification -->       <ion-col col-4 col-sm-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"3\"             formControlName=\"card_verification\"             [pattern]=\"numberPattern\"             [placeholder]=\"translate('new-payment-method-form.payment-method-creation.card.card_verification') + '*'\">           </ion-input>         </ion-item>       </ion-col>      </ion-row>      <!-- buttons -->     <ion-row no-padding class=\"margin-top-m\">        <ion-col col-6 col-sm-5 col-md-3>         <button ion-button full text-uppercase class=\"secondary\" (click)=\"cancel($event)\">           {{ translate('payment-method-selection.new-payment-method-form.generic.cancel') }}         </button>       </ion-col>        <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">         <button ion-button full text-uppercase (click)=\"submit($event)\" [disabled]=\"!newCardForm.valid\">           {{ translate('payment-method-selection.new-payment-method-form.generic.save') }}         </button>       </ion-col>      </ion-row>    </ion-grid> </form>  <!-- bank account form --> <form *ngIf=\"selectedMethodType === 2\" [formGroup]=\"newBankAccountForm\" class=\"padding-top-m\">   <ion-grid class=\"no-padding values-list unlabeled-form\">      <ion-row no-padding>        <!-- iban -->       <ion-col col-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"4\"             formControlName=\"iban\"             (ionBlur)=\"onBlur()\"             [placeholder]=\"translate('new-payment-method-form.payment-method-creation.bank_account.iban') + '*'\">           </ion-input>         </ion-item>       </ion-col>        <!-- entity -->       <ion-col col-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"4\"             formControlName=\"entity\"             placeholder=\"*\"             (ionBlur)=\"onBlur()\"             [pattern]=\"numberPattern\">           </ion-input>         </ion-item>       </ion-col>        <!-- office -->       <ion-col col-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"4\"             formControlName=\"office\"             placeholder=\"*\"             (ionBlur)=\"onBlur()\"             [pattern]=\"numberPattern\">           </ion-input>         </ion-item>       </ion-col>        <!-- control_digit -->       <ion-col col-2 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"2\"             formControlName=\"control_digit\"             placeholder=\"*\"             (ionBlur)=\"onBlur()\"             [pattern]=\"numberPattern\">           </ion-input>         </ion-item>       </ion-col>        <!-- account_number -->       <ion-col col-4 class=\"input-col vertical-margin-s\">         <ion-item no-padding>           <ion-input             type=\"text\"             maxlength=\"10\"             formControlName=\"account_number\"             placeholder=\"*\"             (ionBlur)=\"onBlur()\"             [pattern]=\"spacedNumberPattern\">           </ion-input>         </ion-item>       </ion-col>      </ion-row>     <ion-row no-padding>       <div *ngIf=\"wrongAccountNumber\" class=\"input-value-error\">         <span>           {{ translate('payment-method-selection.new-payment-method-form.payment-method-creation.bank_account.format_error') }}         </span>       </div>     </ion-row>      <!-- buttons -->     <ion-row no-padding class=\"margin-top-m\">        <ion-col col-6 col-sm-5 col-md-3>         <button ion-button full text-uppercase class=\"secondary\" (click)=\"cancel($event)\">           {{ translate('payment-method-selection.new-payment-method-form.generic.cancel') }}         </button>       </ion-col>        <ion-col col-6 col-sm-5 col-md-3 class=\"margin-left-auto\">         <button ion-button full text-uppercase (click)=\"submit($event)\" [disabled]=\"!newBankAccountForm.valid\">           {{ translate('payment-method-selection.new-payment-method-form.generic.save') }}         </button>       </ion-col>      </ion-row>    </ion-grid> </form>"
                },] },
    ];
    NewPaymentMethodFormComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
        { type: FormBuilder, },
        { type: Events, },
    ]; };
    NewPaymentMethodFormComponent.propDecorators = {
        'selectedMethodType': [{ type: Input },],
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return NewPaymentMethodFormComponent;
}());
export { NewPaymentMethodFormComponent };
//# sourceMappingURL=new-payment-method-form.js.map