import { OnInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import { MapfreNetworkService } from 'mapfre-framework/dist';
import { TranslationProvider } from '../../providers/translation/translation';
export declare class OfflineAlertComponent implements OnInit {
    private net;
    private platform;
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    offline: boolean;
    static configInputs: string[];
    constructor(net: MapfreNetworkService, platform: Platform, translationProvider: TranslationProvider);
    ngOnInit(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
