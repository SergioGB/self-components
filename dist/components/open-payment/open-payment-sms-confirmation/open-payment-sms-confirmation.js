import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events, ViewController } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { TranslationProvider } from '../../../providers/translation/translation';
import { Message } from '../../../models/message';
import { SMSMessageContent } from '../../../models/smsMessageContent';
import { PinLoginV2 } from '../../../models/pinLoginV2';
var OpenPaymentSmsConfirmationComponent = (function () {
    function OpenPaymentSmsConfirmationComponent(viewController, userProvider, formBuilder, events, translationProvider) {
        this.viewController = viewController;
        this.userProvider = userProvider;
        this.formBuilder = formBuilder;
        this.events = events;
        this.translationProvider = translationProvider;
        this.resendSMS = false;
        this.messageSuccess = false;
        this.wrongCode = false;
        this.translationProvider.bind(this);
        this.validationForm = this.formBuilder.group({
            validationCode: ['', Validators.required]
        });
        this.validation = this.validationForm.controls['validationCode'];
        this.componentSettings = {
            title: "C\u00F3digo de validaci\u00F3n SMS",
            subtitle: "Te hemos enviado un SMS con un c\u00F3digo de validaci\u00F3n a tu M\u00F3vil.\n                Por favor, introd\u00FAcelo para verificar tu usuario y completar el proceso.",
            enter_code: "Introduce el c\u00F3digo",
            send: "Enviar",
            resend: "Volver a enviar SMS"
        };
        this.componentReady = true;
    }
    OpenPaymentSmsConfirmationComponent.prototype.resendValidation = function () {
        var _this = this;
        this.validationContent = new Message('SMS', null, new SMSMessageContent('', '', null));
        if (this.mocked) {
            this.onResendSuccess();
        }
        else {
            this.userProvider.sendMailSMSRequest(this.validationContent).subscribe(function (response) {
                var mapfreResponse = _this.userProvider.getResponseJSON(response);
                if (_this.userProvider.isMapfreResponseValid(mapfreResponse)) {
                    _this.onResendSuccess();
                }
            });
        }
    };
    OpenPaymentSmsConfirmationComponent.prototype.scrollToTopPage = function () {
        this.events.publish('scrollToTop');
    };
    OpenPaymentSmsConfirmationComponent.prototype.scrollToTopComponent = function () {
        this.events.publish('scrollToElement', 'wrongCode');
    };
    OpenPaymentSmsConfirmationComponent.prototype.attemptValidation = function () {
        var _this = this;
        if (this.mocked) {
            this.onSubmissionSuccess();
        }
        else if (this.validationForm.valid) {
            var validation = new PinLoginV2(this.requestInfo.by_identification_number, this.requestInfo.by_mail, this.requestInfo.by_budget, this.validationForm.value.validationCode);
            var validationRequest = {
                pin_login: validation
            };
            this.userProvider.sendOpenLoginRequest(this.mocked, validationRequest).subscribe(function (mapfreResponse) {
                if (_this.userProvider.isMapfreResponseValidV1(mapfreResponse)) {
                    _this.onSubmissionSuccess();
                }
                else {
                    _this.scrollToTopComponent();
                    _this.messageSuccess = true;
                    _this.emitWrongCode(true);
                }
            });
        }
    };
    OpenPaymentSmsConfirmationComponent.prototype.closeResponse = function () {
        this.wrongCode = false;
    };
    OpenPaymentSmsConfirmationComponent.prototype.goToOpenPayment = function () {
        this.events.publish(OpenPaymentSmsConfirmationComponent.goToOpenPaymentEvent);
        this.dismiss();
    };
    OpenPaymentSmsConfirmationComponent.prototype.goToOpenAssistance = function () {
        this.events.publish(OpenPaymentSmsConfirmationComponent.goToOpenAssistanceEvent);
        this.dismiss();
    };
    OpenPaymentSmsConfirmationComponent.prototype.goToOpenSubmitIncidence = function () {
        this.events.publish(OpenPaymentSmsConfirmationComponent.goToOpenSubmitIncidenceEvent);
        this.dismiss();
    };
    OpenPaymentSmsConfirmationComponent.prototype.goToOpenContractBudget = function () {
        this.events.publish(OpenPaymentSmsConfirmationComponent.goToOpenContractBudgetEvent);
        this.dismiss();
    };
    OpenPaymentSmsConfirmationComponent.prototype.dismiss = function () {
        this.viewController.dismiss();
    };
    OpenPaymentSmsConfirmationComponent.prototype.onResendSuccess = function () {
        this.scrollToTopPage();
        this.resendSMS = true;
    };
    OpenPaymentSmsConfirmationComponent.prototype.onSubmissionSuccess = function () {
        this.scrollToTopPage();
        this.messageSuccess = false;
        this.emitWrongCode(false);
        if (this.process === 'open-receipt-payment') {
            this.goToOpenPayment();
        }
        else if (this.process === 'open-request-assistance') {
            this.goToOpenAssistance();
        }
        else if (this.process === 'open-potential-client') {
            this.goToOpenContractBudget();
        }
        else if (this.process === 'open-submit-incidence') {
            this.goToOpenSubmitIncidence();
        }
    };
    OpenPaymentSmsConfirmationComponent.prototype.emitWrongCode = function (isWrong) {
        this.events.publish(OpenPaymentSmsConfirmationComponent.wrongCodeEvent, isWrong);
    };
    OpenPaymentSmsConfirmationComponent.componentName = 'open-payment-sms-confirmation';
    OpenPaymentSmsConfirmationComponent.wrongCodeEvent = OpenPaymentSmsConfirmationComponent.componentName + ":wrongCode";
    OpenPaymentSmsConfirmationComponent.goToOpenPaymentEvent = OpenPaymentSmsConfirmationComponent.componentName + ":goToOpenPayment";
    OpenPaymentSmsConfirmationComponent.goToOpenAssistanceEvent = OpenPaymentSmsConfirmationComponent.componentName + ":goToOpenAssistance";
    OpenPaymentSmsConfirmationComponent.goToOpenContractBudgetEvent = OpenPaymentSmsConfirmationComponent.componentName + ":goToOpenContractBudget";
    OpenPaymentSmsConfirmationComponent.goToOpenSubmitIncidenceEvent = OpenPaymentSmsConfirmationComponent.componentName + ":goToOpenSubmitIncidence";
    OpenPaymentSmsConfirmationComponent.inputs = ['onClose', 'userName', 'process', 'requestInfo'];
    OpenPaymentSmsConfirmationComponent.configInputs = ['mocked', 'styles'];
    OpenPaymentSmsConfirmationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'open-payment-sms-confirmation',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card-content *ngIf=\"componentSettings\" class=\"bottom-border border-only no-padding-top\" [ngStyle]=\"styles\">    <div *ngIf=\"!resendSMS\">     <h3 class=\"margin-top-m\" text-center>       {{ translate('open-payment-sms-confirmation.new_account.validation_by_sms.title') }}     </h3>      <h5 class=\"fmm-primary-text fmm-regular vertical-margin-s\" text-center>       {{ translate('open-payment-sms-confirmation.new_account.validation_by_sms.description') }}     </h5>   </div>    <div *ngIf=\"resendSMS\">     <h3 class=\"margin-top-m\" text-center>       {{ translate('open-payment-sms-confirmation.new_account.validation_by_resend_sms.title') }}     </h3>      <h5 class=\"fmm-primary-text fmm-regular vertical-margin-s\" text-center>       {{ translate('open-payment-sms-confirmation.new_account.validation_by_resend_sms.description') }}     </h5>   </div>    <form [formGroup]=\"validationForm\" (ngSubmit)=\"attemptValidation()\">      <ion-item no-padding class=\"required\" [class.invalid]=\"wrongCode\">       <ion-label floating *ngIf=\"!wrongCode\">         {{ translate('open-payment-sms-confirmation.new_account.validation_by_sms.enter_code') }}       </ion-label>       <ion-label floating class=\"red-text\" *ngIf=\"wrongCode\">         {{ translate('open-payment-sms-confirmation.new_account.validation_by_sms.enter_code') }}       </ion-label>       <ion-input type=\"text\" [formControl]=\"validation\" [class.invalid]=\"wrongCode\"></ion-input>     </ion-item>      <div class=\"margin-top-m\">       <button margin-top ion-button full text-uppercase [disabled]=\"!validationForm.valid\">         {{ translate('open-payment-sms-confirmation.new_account.validation_by_sms.send') }}       </button>     </div>      <div *ngIf=\"!resendSMS\" margin-top text-right class=\"forgot-password fmm-body1 font-s clickable\" tabindex=\"0\"          (keyup.enter)=\"resendValidation()\" (click)=\"resendValidation()\">       {{ translate('open-payment-sms-confirmation.new_account.validation_by_sms.resend') }}     </div>    </form>  </ion-card-content>  <ion-card-content class=\"padding-m\" *ngIf=\"messageSuccess || resendSMS\">   <ion-row *ngIf=\"messageSuccess || resendSMS\">     <ion-col class=\"gestor\" col-12 text-center>       <a title=\"{{ translate('open-payment-sms-confirmation.blocked_user.check_sms.contact_us') }}\" class=\"font-m clickable\" href=\"https://www.mapfre.es/seguros/contacto/\" target=\"_blank\">         {{ translate('open-payment-sms-confirmation.blocked_user.check_sms.contact_us') }}       </a>     </ion-col>   </ion-row> </ion-card-content>"
                },] },
    ];
    OpenPaymentSmsConfirmationComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: UserProvider, },
        { type: FormBuilder, },
        { type: Events, },
        { type: TranslationProvider, },
    ]; };
    OpenPaymentSmsConfirmationComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'onClose': [{ type: Input },],
        'userName': [{ type: Input },],
        'process': [{ type: Input },],
        'requestInfo': [{ type: Input },],
    };
    return OpenPaymentSmsConfirmationComponent;
}());
export { OpenPaymentSmsConfirmationComponent };
//# sourceMappingURL=open-payment-sms-confirmation.js.map