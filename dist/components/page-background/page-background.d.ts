import { OnInit } from '@angular/core';
export declare class PageBackgroundComponent implements OnInit {
    backgroundUrl?: string;
    mocked?: boolean;
    styles?: any;
    hidden?: boolean;
    ready: boolean;
    static configInputs: string[];
    constructor();
    ngOnInit(): void;
    private getBackground;
}
