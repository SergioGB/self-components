import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import * as jQuery from "jquery";
var PageBackgroundComponent = (function () {
    function PageBackgroundComponent() {
    }
    PageBackgroundComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ready = true;
        setTimeout(function () {
            if (_this.backgroundUrl && _this.backgroundUrl.length) {
                jQuery('.header-background').css({
                    background: _this.getBackground()
                });
            }
        });
    };
    PageBackgroundComponent.prototype.getBackground = function () {
        return (this.backgroundUrl && this.backgroundUrl.length) ?
            "linear-gradient(to right,rgba(0,0,0,.7),transparent),url(" + this.backgroundUrl + ") center" : undefined;
    };
    PageBackgroundComponent.configInputs = ['mocked', 'backgroundUrl', 'styles', 'hidden'];
    PageBackgroundComponent.decorators = [
        { type: Component, args: [{
                    selector: 'page-background',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div *ngIf=\"!hidden && ready\" [ngStyle]=\"styles\">   <div class=\"header-background\"></div> </div>"
                },] },
    ];
    PageBackgroundComponent.ctorParameters = function () { return []; };
    PageBackgroundComponent.propDecorators = {
        'backgroundUrl': [{ type: Input },],
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'hidden': [{ type: Input },],
    };
    return PageBackgroundComponent;
}());
export { PageBackgroundComponent };
//# sourceMappingURL=page-background.js.map