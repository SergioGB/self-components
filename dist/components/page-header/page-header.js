import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events, NavController } from 'ionic-angular';
import * as jQuery from 'jquery';
import { Mocks } from '../../providers/mocks/mocks';
import { TranslationProvider } from '../../providers/translation/translation';
import { Utils } from '../../providers/utils/utils';
var PageHeaderComponent = (function () {
    function PageHeaderComponent(navController, translationProvider, events) {
        this.navController = navController;
        this.translationProvider = translationProvider;
        this.events = events;
        this.defaultIcons = ["ios-arrow-round-back"];
        this.translationProvider.bind(this);
    }
    PageHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.mocked) {
            this.data = Mocks.getPageHeaderData();
            this.ready = true;
        }
        else {
            this.selectTab(this.defaultTab || (this.tabs ? this.tabs[0] : undefined));
            this.ready = true;
            setTimeout(function () {
                if (_this.backgroundUrl && _this.backgroundUrl.length) {
                    jQuery('.tabbed-header').css({
                        background: _this.getBackground()
                    });
                }
            });
        }
    };
    PageHeaderComponent.prototype.emitBackClicked = function () {
        this.events.publish(PageHeaderComponent.backClickedEvent);
    };
    PageHeaderComponent.prototype.selectTab = function (selectedTab) {
        if (selectedTab && this.tabs && this.tabs.length) {
            this.tabs.forEach(function (tab) {
                tab.active = tab.key === selectedTab.key;
            });
            Utils.scrollToId(selectedTab.key);
            this.events.publish(PageHeaderComponent.tabSelectionEvent, selectedTab);
        }
    };
    PageHeaderComponent.prototype.getBackground = function () {
        return (this.backgroundUrl && this.backgroundUrl.length) ?
            "linear-gradient(to right,rgba(0,0,0,.7),transparent),url(" + this.backgroundUrl + ") center" : undefined;
    };
    PageHeaderComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    PageHeaderComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    PageHeaderComponent.componentName = 'page-header';
    PageHeaderComponent.tabSelectionEvent = PageHeaderComponent.componentName + ":tabSelection";
    PageHeaderComponent.backClickedEvent = PageHeaderComponent.componentName + ":backClicked";
    PageHeaderComponent.inputs = ['data', 'tabs', 'defaultTab', 'requestInfo'];
    PageHeaderComponent.configInputs = ['mocked', 'icons', 'styles', 'backButtonAvailable', 'backgroundUrl'];
    PageHeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'page-header',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<loading-spinner *ngIf=\"!ready\"></loading-spinner>  <div   *ngIf=\"ready\"   class=\"tabbed-header\"   [ngStyle]=\"styles\">    <ion-grid *ngIf=\"data\" no-padding class=\"header-content\">      <ion-row>       <ion-col col-sm-8>          <div class=\"title-wrapper flex center-contents\">           <ion-icon             *ngIf=\"backButtonAvailable\"             [name]=\"getIcon(0)\"             class=\"back-icon clickable\"             (click)=\"emitBackClicked()\">           </ion-icon>            <div class=\"header-title\">             <h2 *ngIf=\"data.title\" class=\"primary\">               {{ data.title }}             </h2>              <h4 *ngIf=\"data.description\" class=\"header-subtitle fmm-body1\">               {{ data.description }}             </h4>           </div>         </div>          <ion-grid *ngIf=\"data.rows\" no-padding class=\"values-list\">           <ion-row *ngFor=\"let row of data.rows | sort: 'order'\">             <ion-col col-sm-3>                <strong class=\"primary-text\">                 {{ translate(row.key) }}               </strong>              </ion-col>             <ion-col col-sm-9>              <span class=\"secondary-text\">               {{ row.value }}             </span>              </ion-col>           </ion-row>         </ion-grid>        </ion-col>     </ion-row>   </ion-grid>    <div *ngIf=\"tabs && tabs.length\" class=\"tabs-wrapper\">     <ion-scroll scrollX>       <a *ngFor=\"let tab of tabs\"          text-uppercase class=\"scroll-item clickable\"          [id]=\"tab.key\"          [class.selected]=\"tab.active\"          [title]=\"translate(tab.label)\"          (click)=\"selectTab(tab)\">         {{ translate(tab.label) }}       </a>     </ion-scroll>   </div>  </div>"
                },] },
    ];
    PageHeaderComponent.ctorParameters = function () { return [
        { type: NavController, },
        { type: TranslationProvider, },
        { type: Events, },
    ]; };
    PageHeaderComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'data': [{ type: Input },],
        'tabs': [{ type: Input },],
        'defaultTab': [{ type: Input },],
        'backButtonAvailable': [{ type: Input },],
        'backgroundUrl': [{ type: Input },],
    };
    return PageHeaderComponent;
}());
export { PageHeaderComponent };
//# sourceMappingURL=page-header.js.map