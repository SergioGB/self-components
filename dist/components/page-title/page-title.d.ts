import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
export declare class PageTitleComponent implements OnInit {
    private translationProvider;
    private platformProvider;
    mocked?: boolean;
    styles?: any;
    title?: string;
    subtitle?: string;
    subtitleOnMobile?: boolean;
    light?: boolean;
    ready: boolean;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, platformProvider: PlatformProvider);
    ngOnInit(): void;
}
