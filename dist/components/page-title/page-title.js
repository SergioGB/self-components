import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { PlatformProvider } from '../../providers/platform/platform';
var PageTitleComponent = (function () {
    function PageTitleComponent(translationProvider, platformProvider) {
        this.translationProvider = translationProvider;
        this.platformProvider = platformProvider;
        this.translationProvider.bind(this);
        this.platformProvider.bindTo(this);
    }
    PageTitleComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.title = 'offers.list.title';
            this.subtitle = 'offers.list.subtitle';
            this.ready = true;
        }
        else {
            this.subtitleOnMobile = this.subtitleOnMobile !== false;
            this.ready = true;
        }
    };
    PageTitleComponent.configInputs = ['mocked', 'styles', 'title', 'subtitle', 'subtitleOnMobile', 'light'];
    PageTitleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'page-title',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div *ngIf=\"ready\" class=\"upper-header\" [class.light]=\"light\" [ngStyle]=\"styles\">   <h2 [hidden]=\"!title\">     {{ translate(title) }}   </h2>   <p [hidden]=\"!subtitle || (onMobile && !subtitleOnMobile)\" class=\"font-m\">     {{ translate(subtitle) }}   </p> </div>"
                },] },
    ];
    PageTitleComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: PlatformProvider, },
    ]; };
    PageTitleComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'title': [{ type: Input },],
        'subtitle': [{ type: Input },],
        'subtitleOnMobile': [{ type: Input },],
        'light': [{ type: Input },],
    };
    return PageTitleComponent;
}());
export { PageTitleComponent };
//# sourceMappingURL=page-title.js.map