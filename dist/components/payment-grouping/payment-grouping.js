import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ViewController, NavParams } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { ModalProvider } from '../../providers/modal/modal';
var PaymentGroupingComponent = (function () {
    function PaymentGroupingComponent(viewCtrl, elementRef, modalProvider, navParams, translationProvider, formBuilder) {
        this.viewCtrl = viewCtrl;
        this.elementRef = elementRef;
        this.modalProvider = modalProvider;
        this.navParams = navParams;
        this.translationProvider = translationProvider;
        this.formBuilder = formBuilder;
        this.defaultIcons = ["mapfre-money-bag", "mapfre-close", "mapfre-close"];
        this.confirmationSuccess = false;
        this.selectOptions = {
            cssClass: 'popover-content-cm'
        };
        this.translationProvider.bind(this);
        this.modalProvider.setupCentering(this.elementRef);
        this.labels = this.navParams.get('labels');
        this.initializePaymentDays();
        this.initializeForm();
    }
    PaymentGroupingComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PaymentGroupingComponent.prototype.confirm = function () {
        this.confirmationSuccess = true;
        this.modalProvider.setupCentering(this.elementRef);
    };
    PaymentGroupingComponent.prototype.cancel = function () {
        this.confirmationCancel = true;
        this.modalProvider.setupCentering(this.elementRef);
    };
    PaymentGroupingComponent.prototype.initializePaymentDays = function () {
        this.paymentDays = [];
        for (var i = 0; i < 31; i++) {
            this.paymentDays.push({
                value: i,
                label: i + 1
            });
        }
    };
    PaymentGroupingComponent.prototype.initializeForm = function () {
        this.paymentForm = this.formBuilder.group({
            paymentDay: [undefined, Validators.required]
        });
    };
    PaymentGroupingComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    PaymentGroupingComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    PaymentGroupingComponent.configInputs = ['mocked', 'icons', 'styles'];
    PaymentGroupingComponent.decorators = [
        { type: Component, args: [{
                    selector: 'payment-grouping',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin [ngStyle]=\"styles\">   <ion-card-content *ngIf=\"!confirmationSuccess && !confirmationCancel\">      <ion-grid no-padding>        <!-- form -->       <ion-row no-padding class=\"margin-top-m bottom-border padding-bottom-l\">         <ion-col col-12 col-sm-8 offset-sm-2 text-center>            <ion-icon [name]=\"getIcon(0)\" class=\"modal-icon\"></ion-icon>            <h3 class=\"modal-title\">             {{ translate(labels.title) }}           </h3>            <div class=\"modal-description\">             <p>               {{ translate(labels.description_line1) }}             </p>             <p>               {{ translate(labels.description_line2) }}             </p>           </div>          </ion-col>         <ion-col col-12 col-sm-6 offset-sm-3 text-center>            <form [formGroup]=\"paymentForm\" class=\"bordered-selects\">             <ion-item class=\"required\">               <ion-select [selectOptions]=\"selectOptions\" [placeholder]=\"translate(labels.day_selection_placeholder)\" formControlName=\"paymentDay\" full>                 <ion-option *ngFor=\"let paymentDay of paymentDays\" [value]=\"paymentDay.value\">                   {{ paymentDay.label }}                 </ion-option>               </ion-select>             </ion-item>           </form>          </ion-col>       </ion-row>        <!-- buttons -->       <ion-row no-padding class=\"margin-top-l\">          <ion-col col-12 col-sm-5 col-md-3>           <button ion-button full text-uppercase class=\"secondary\" (click)=\"cancel()\">             {{ translate(labels.cancel) }}           </button>         </ion-col>          <ion-col col-12 col-sm-5 col-md-3 class=\"margin-left-auto\">           <button ion-button full text-uppercase (click)=\"confirm($event)\" [disabled]=\"!paymentForm.valid\">             {{ translate(labels.confirm) }}           </button>         </ion-col>        </ion-row>      </ion-grid>    </ion-card-content>    <ion-card-content *ngIf=\"confirmationSuccess\">       <ion-icon [name]=\"getIcon(1)\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>       <div class=\"modal-content bottom-border border-only\" text-center>         <process-confirmation           [flowReference]=\"true\"           [title]=\"translate(labels.confirmation)\"           [description]=\"\"           [icon]=\"'mapfre-check'\"           [ratingText]=\"' '\">         </process-confirmation>       </div>    </ion-card-content>    <ion-card-content *ngIf=\"confirmationCancel\">     <ion-icon [name]=\"getIcon(2)\" class=\"modal-close\" tabindex=\"0\" (keyup.enter)=\"dismiss()\" (click)=\"dismiss()\"></ion-icon>     <div class=\"modal-content bottom-border border-only\" text-center>       <process-failure         [title]=\"translate(labels.action_selectday_cancel)\"         [icon]=\"'mapfre-cross-error'\">       </process-failure>     </div>   </ion-card-content> </ion-card>"
                },] },
    ];
    PaymentGroupingComponent.ctorParameters = function () { return [
        { type: ViewController, },
        { type: ElementRef, },
        { type: ModalProvider, },
        { type: NavParams, },
        { type: TranslationProvider, },
        { type: FormBuilder, },
    ]; };
    PaymentGroupingComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return PaymentGroupingComponent;
}());
export { PaymentGroupingComponent };
//# sourceMappingURL=payment-grouping.js.map