import { ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ModalController, Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { FormatProvider } from '../../providers/format/format';
import { AnimationsProvider } from '../../providers/animations/animations';
import { UserProvider } from '../../providers/user/user';
import { FormGroup, FormBuilder } from '@angular/forms';
export declare class PaymentMethodSelectionV2Component implements OnInit, OnDestroy {
    private elementRef;
    private modalCtrl;
    private translationProvider;
    private componentSettingsProvider;
    private formatProvider;
    private animationsProvider;
    private events;
    private formBuilder;
    private userProvider;
    static componentName: string;
    mocked?: boolean;
    modals?: boolean;
    icons?: string[];
    styles?: any;
    eventTarget?: string;
    selectedPaymentMethod?: any;
    oneCard?: boolean;
    policyId: any;
    isOpenFlow: boolean;
    ShowNoClient: boolean;
    defaultIcons: string[];
    paymentMethods: any[];
    componentReady: boolean;
    creationFormActive: boolean;
    client: string;
    isTaker: boolean;
    BankAccountTakerForm: FormGroup;
    BankAccountForm: FormGroup;
    componentSelector: string;
    collapsibleCreationSelector: string;
    static completedEvent: string;
    static getSelectionEventName(eventTarget?: string): string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(elementRef: ElementRef, modalCtrl: ModalController, translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider, formatProvider: FormatProvider, animationsProvider: AnimationsProvider, events: Events, formBuilder: FormBuilder, userProvider: UserProvider);
    ngOnInit(): void;
    initializeForms(): void;
    ngOnDestroy(): void;
    formatPaymentMethodNumber(paymentMethod: any): string;
    isMethodSelected(paymentMethod: any): boolean;
    isBankAccount(paymentMethod: any): boolean;
    changeTaker(value: boolean): void;
    getFormattedExpiration(date: Date): string;
    onMethodSelected(paymentMethod: any): void;
    onUpdateSubmission(): void;
    onCancel(): void;
    toggleCreation(): void;
    onCreation(paymentMethod: any): void;
    private mockData;
    private queryData;
    private readyComponent;
    private onUpdateSuccess;
    private onCreationSuccess;
    private publishSelection;
    private updateCompleteness;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
