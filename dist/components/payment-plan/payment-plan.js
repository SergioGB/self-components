import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Mocks } from '../../providers/mocks/mocks';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { FormatProvider } from '../../providers/format/format';
import { PlatformProvider } from '../../providers/platform/platform';
import { TranslationProvider } from '../../providers/translation/translation';
var PaymentPlanComponent = (function () {
    function PaymentPlanComponent(componentSettingsProvider, translationProvider, formatProvider, platformProvider, events) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.translationProvider = translationProvider;
        this.formatProvider = formatProvider;
        this.platformProvider = platformProvider;
        this.events = events;
        this.translationProvider.bind(this);
    }
    PaymentPlanComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.price = Mocks.getCost();
            this.periodicity = 'quarter';
        }
        this.loadData();
    };
    PaymentPlanComponent.prototype.formatCost = function (cost) {
        return this.formatProvider.formatCost(cost);
    };
    PaymentPlanComponent.prototype.getPeriodCost = function () {
        return {
            amount: this.price.amount / this.numberOfPayments,
            iso_code: this.price.iso_code
        };
    };
    PaymentPlanComponent.prototype.loadData = function () {
        this.calculateNumberOfPayments();
        this.generatePaymentList();
        this.componentReady = true;
    };
    PaymentPlanComponent.prototype.calculateNumberOfPayments = function () {
        switch (this.periodicity) {
            case 'year':
                this.numberOfPayments = 1;
                break;
            case 'semester':
                this.numberOfPayments = 2;
                break;
            case 'quarter':
                this.numberOfPayments = 4;
                break;
            case 'month':
                this.numberOfPayments = 12;
                break;
        }
    };
    PaymentPlanComponent.prototype.generatePaymentList = function () {
        this.paymentPlanList = [];
        var initialDate = moment(new Date('01/01/2019'));
        var paymentDate = initialDate.format('DD/MM/YYYY');
        var monthsDiff;
        if (this.numberOfPayments > 1) {
            monthsDiff = 12 / this.numberOfPayments;
        }
        for (var i = 0; i < this.numberOfPayments; i++) {
            if (i > 0 && monthsDiff !== undefined) {
                initialDate = initialDate.add(monthsDiff, 'M');
                paymentDate = initialDate.format('DD/MM/YYYY');
            }
            this.paymentPlanList.push({
                icon: {
                    name: 'mapfre-solid-radio-on',
                    hex_color: '#999999'
                },
                title: this.translationProvider.getValueForKey("receipt_fractionament.flow.summary.payment" + (i + 1)),
                date: paymentDate,
                status: this.translationProvider.getValueForKey('payment-plan.receipt_fractionament.flow.summary.not_issued')
            });
        }
        this.publishGeneration();
    };
    PaymentPlanComponent.prototype.publishGeneration = function () {
        this.events.publish(PaymentPlanComponent.generatedEvent, { paymentPlanList: this.paymentPlanList });
        this.updateCompleteness();
    };
    PaymentPlanComponent.prototype.updateCompleteness = function () {
        this.events.publish(PaymentPlanComponent.completedEvent, !_.isNil(this.paymentPlanList));
    };
    PaymentPlanComponent.componentName = "PaymentPlanComponent";
    PaymentPlanComponent.generatedEvent = PaymentPlanComponent.componentName + ":generated";
    PaymentPlanComponent.completedEvent = PaymentPlanComponent.componentName + ":completed";
    PaymentPlanComponent.inputs = ['price', 'periodicity'];
    PaymentPlanComponent.configInputs = ['mocked', 'styles'];
    PaymentPlanComponent.decorators = [
        { type: Component, args: [{
                    selector: 'payment-plan',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["payment-plan span.related-product {   padding-left: 20px; }  payment-plan .padding-icon {   padding-top: 11px; }"],
                    template: "<ion-card *ngIf=\"componentReady && paymentPlanList\" [ngStyle]=\"styles\">    <ion-card-header class=\"no-padding-bottom\">     <ion-item>       <h3>         {{ translate('payment-plan.receipt_fractionament.flow.summary.payment_plan') }}       </h3>     </ion-item>   </ion-card-header>    <ion-card-content class=\"no-vertical-padding\">      <ion-grid *ngFor=\"let paymentPlanItem of paymentPlanList; let i = index\" tabindex=\"0\" no-padding class=\"bottom-border border-only\">       <ion-row class=\"vertical-padding-m\">         <ion-col col-12 col-md-4>           <p class=\"fmm-body1 primary-text\">             {{ paymentPlanItem.title }}           </p>         </ion-col>         <ion-col col-12 col-md-2 [class.col-right]=\"!platformProvider.onMobile()\">           <p *ngIf=\"price\" class=\"fmm-body1 primary-text\">             {{ formatCost(getPeriodCost()) }}           </p>         </ion-col>         <ion-col col-12 col-md-4 [class.col-right]=\"!platformProvider.onMobile()\">           <p class=\"fmm-body1 primary-text\">             {{ translate('payment-plan.receipt_fractionament.flow.summary.overcome') }} {{ paymentPlanItem.date }}           </p>         </ion-col>         <ion-col col-12 col-md-2 class=\"no-padding-right\" [class.col-right]=\"!platformProvider.onMobile()\">           <p class=\"fmm-body1 fmm-regular\">             {{ paymentPlanItem.status }}           </p>         </ion-col>       </ion-row>     </ion-grid>    </ion-card-content>  </ion-card>"
                },] },
    ];
    PaymentPlanComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: TranslationProvider, },
        { type: FormatProvider, },
        { type: PlatformProvider, },
        { type: Events, },
    ]; };
    PaymentPlanComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'price': [{ type: Input },],
        'periodicity': [{ type: Input },],
    };
    return PaymentPlanComponent;
}());
export { PaymentPlanComponent };
//# sourceMappingURL=payment-plan.js.map