import { AppGlobalService } from './../../../providers/utils/app-global.service';
import { ElementRef, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { FormBuilder } from '@angular/forms';
import { App, Content, ModalController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { SocialUser } from 'angularx-social-login/src/entities/user';
import { TranslationProvider } from '../../../providers/translation/translation';
import { PersonalInfoProvider } from '../../../providers/personal-info/personal-info';
import { UserProvider } from '../../../providers/user/user';
import { SocialLoginProvider } from '../../../providers/social-login/social-login';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { PlatformProvider } from '../../../providers/platform/platform';
import { IdentificationProvider } from '../../../providers/identification/identification';
import { AnimationsProvider } from '../../../providers/animations/animations';
import { AppLoadService } from '../../../providers/loader/AppLoadService';
import { User } from '../../../models/user';
export declare class AccountDataComponent implements OnInit {
    private elementRef;
    private formBuilder;
    private appGlobalService;
    private app;
    private http;
    private modalCtrl;
    private events;
    private content;
    private platform;
    private animationsProvider;
    private translationProvider;
    private userProvider;
    private componentSettingsProvider;
    private identificationProvider;
    private personalInfoProvider;
    private socialLoginProvider;
    private appLoadService;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    account: User;
    modals?: any;
    defaultIcons: string[];
    ismyApp: boolean;
    isAndroid: boolean;
    isIos: boolean;
    fingerprintEnabled: boolean;
    isPinActivated: boolean;
    componentReady: boolean;
    userData: any;
    notificationMethods: any;
    preferredLanguage: any;
    languages: any;
    cardHeaderPadding: number;
    cardHeaderBorderThickness: number;
    fullHeight: number;
    collapsedHeight: number;
    collapsed: boolean;
    fingerprintAvailable: boolean;
    fingerprintType: string;
    availableSocialNetworks: any;
    facebookUser: SocialUser;
    twitterUser: SocialUser;
    googleUser: SocialUser;
    static inputs: string[];
    static configInputs: string[];
    constructor(elementRef: ElementRef, formBuilder: FormBuilder, appGlobalService: AppGlobalService, app: App, http: Http, modalCtrl: ModalController, events: Events, content: Content, platform: PlatformProvider, animationsProvider: AnimationsProvider, translationProvider: TranslationProvider, userProvider: UserProvider, componentSettingsProvider: ComponentSettingsProvider, identificationProvider: IdentificationProvider, personalInfoProvider: PersonalInfoProvider, socialLoginProvider: SocialLoginProvider, appLoadService: AppLoadService);
    ngOnInit(): void;
    signInWithFacebook(): void;
    signInWithTwitter(): void;
    signInWithGoogle(): void;
    signOutWithFacebook(): void;
    signOutWithTwitter(): void;
    signOutWithGoogle(): void;
    hasSocialUsers(): boolean;
    hasUnconnectedUsers(): boolean;
    displayLanguageModal(): void;
    toggleChild(selector: string): void;
    isChildCollapsed(selector: string): boolean;
    submitEdition(): void;
    submitLanguageEdition(input: string): void;
    private loadAccountInfo;
    openChangePasswordModal(): void;
    toggleFingerprint(): void;
    togglePin(): void;
    openChangePinModal(): void;
    openSecurityQuestionsModal(): void;
    toggle(): void;
    private storeHeights;
    getProperHeight(): string;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
