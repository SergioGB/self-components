import { ElementRef } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { Events } from 'ionic-angular';
import { AnimationsProvider } from '../../../providers/animations/animations';
export declare class NewCommunicationsComponent {
    private elementRef;
    private events;
    private animationsProvider;
    private translationsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    componentReady: boolean;
    cardHeaderPadding: number;
    cardHeaderBorderThickness: number;
    fullHeight: number;
    collapsedHeight: number;
    collapsed: boolean;
    static configInputs: string[];
    constructor(elementRef: ElementRef, events: Events, animationsProvider: AnimationsProvider, translationsProvider: TranslationProvider);
    toggleChild(selector: string): void;
    isChildCollapsed(selector: string): boolean;
    goToCommunications(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
