import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef, Input } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { Events } from 'ionic-angular';
import { AnimationsProvider } from '../../../providers/animations/animations';
var NewCommunicationsComponent = (function () {
    function NewCommunicationsComponent(elementRef, events, animationsProvider, translationsProvider) {
        this.elementRef = elementRef;
        this.events = events;
        this.animationsProvider = animationsProvider;
        this.translationsProvider = translationsProvider;
        this.defaultIcons = ["mapfre-arrow-up", "mapfre-information"];
        this.componentReady = false;
        this.cardHeaderPadding = 1;
        this.cardHeaderBorderThickness = 1;
        this.translationsProvider.bind(this);
        this.componentReady = true;
    }
    NewCommunicationsComponent.prototype.toggleChild = function (selector) {
        this.animationsProvider.verticalSlideToggle(selector);
    };
    NewCommunicationsComponent.prototype.isChildCollapsed = function (selector) {
        return this.animationsProvider.isCollapsed(selector);
    };
    NewCommunicationsComponent.prototype.goToCommunications = function () {
        this.events.publish('new-communications::goCommunicationsCenter');
    };
    NewCommunicationsComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    NewCommunicationsComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    NewCommunicationsComponent.configInputs = ['mocked', 'icons', 'styles'];
    NewCommunicationsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'new-communications',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["new-communications #newCommunications ion-icon {   margin-top: 14px !important; }"],
                    template: "<div *ngIf=\"componentReady\" [ngStyle]=\"styles\">   <ion-card class=\"animated medium\">       <ion-card-header tabindex=\"0\" (keyup.enter)=\"toggleChild('#newCommunications')\" (click)=\"toggleChild('#newCommunications')\" class=\"clickable no-padding-bottom\">         <ion-item>           <h3>             {{ translate('new-communications.personal_data_page.new_communications.title') }}           </h3>           <ion-icon class=\"animated medium no-margin-right\" item-end                 [name]=\"getIcon(0)\"                 [class.rotate-180]=\"isChildCollapsed('#newCommunications')\">           </ion-icon>         </ion-item>       </ion-card-header>       <ion-card-content class=\"no-vertical-padding\" id=\"newCommunications\">          <ion-item no-padding class=\"bottom-border flex align-start border-only clickable\">           <ion-icon [name]=\"getIcon(1)\" item-start large></ion-icon>            <h4>             {{ translate('new-communications.personal_data_page.new_communications.reserve') }}           </h4>           <p class=\"fmm-body1 fmm-regular\">             <span>{{ translate('new-communications.personal_data_page.new_communications.description') }}</span>           </p>            <a tabindex=\"0\" (keyup.enter)=\"goToCommunications()\"           no-padding (click)=\"goToCommunications()\"> {{ translate('new-communications.personal_data_page.new_communications.view') }} </a>         </ion-item>        </ion-card-content>   </ion-card> </div>"
                },] },
    ];
    NewCommunicationsComponent.ctorParameters = function () { return [
        { type: ElementRef, },
        { type: Events, },
        { type: AnimationsProvider, },
        { type: TranslationProvider, },
    ]; };
    NewCommunicationsComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return NewCommunicationsComponent;
}());
export { NewCommunicationsComponent };
//# sourceMappingURL=new-communications.js.map