import { TranslationProvider } from '../../../../../providers/translation/translation';
import { Events } from 'ionic-angular';
import { UserProvider } from '../../../../../providers/user/user';
export declare class ChangeBlockedUserCheckMailComponent {
    private translationProvider;
    events: Events;
    private userProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    componentSettings: Object;
    componentReady: boolean;
    reSent: boolean;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events, userProvider: UserProvider);
    resendValidation(): void;
    scrollToTopPage(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
