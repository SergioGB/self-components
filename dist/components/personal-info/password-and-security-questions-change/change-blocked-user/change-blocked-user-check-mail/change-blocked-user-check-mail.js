import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../../../providers/translation/translation';
import { Events } from 'ionic-angular';
import { Message } from '../../../../../models/message';
import { MessageEmailContent } from '../../../../../models/messageEmailContent';
import { UserProvider } from '../../../../../providers/user/user';
var ChangeBlockedUserCheckMailComponent = (function () {
    function ChangeBlockedUserCheckMailComponent(translationProvider, events, userProvider) {
        this.translationProvider = translationProvider;
        this.events = events;
        this.userProvider = userProvider;
        this.defaultIcons = ["mapfre-check"];
        this.translationProvider.bind(this);
        this.componentSettings = {
            title: "Comprueba tu e-Mail",
            subtitle: "Te hemos mandado un correo solicitando el reseteo de tu contrase\u00F1a.\n                Por favor, verifica tu bandeja de entrada y pincha en el enlace (URL) para activar tu cuenta.\n                No olvide comprobar tu bandeja de spam/correo no deseado.",
            resend: "Volver a enviar e-Mail"
        };
        this.componentReady = true;
    }
    ChangeBlockedUserCheckMailComponent.prototype.resendValidation = function () {
        var _this = this;
        var validationContent = new Message('EMAIL', new MessageEmailContent('', '', '', null));
        this.userProvider.sendMailSMSRequest(validationContent).subscribe(function (response) {
            var mapfreResponse = _this.userProvider.getResponseJSON(response);
            var success = _this.userProvider.isMapfreResponseValid(mapfreResponse);
            if (success) {
                _this.scrollToTopPage();
                _this.reSent = true;
            }
        });
        this.componentSettings = {
            icon: {
                name: 'mapfre-check',
                hex_color: '#8db602'
            },
            title: "e-Mail reenviado",
            subtitle: "Te hemos reenviado un correo solicitando el alta de usuario. Por favor, verifica tu bandeja de entrada\n                y pincha en el enlace (URL) para activar tu cuenta. No olvides comprobar tu bandeja de spam/correo no deseado."
        };
    };
    ChangeBlockedUserCheckMailComponent.prototype.scrollToTopPage = function () {
        this.events.publish('scrollToTop');
    };
    ChangeBlockedUserCheckMailComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    ChangeBlockedUserCheckMailComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    ChangeBlockedUserCheckMailComponent.configInputs = ['mocked', 'icons', 'styles'];
    ChangeBlockedUserCheckMailComponent.decorators = [
        { type: Component, args: [{
                    selector: 'change-blocked-user-check-mail',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin *ngIf=\"componentReady\" [ngStyle]=\"styles\">    <ion-card-content *ngIf=\"componentSettings\" class=\"bottom-border border-only no-padding-top\">      <div text-center>       <ion-icon *ngIf=\"reSent\" [name]=\"getIcon(0)\" extra-huge class=\"green\"></ion-icon>     </div>      <h3 class=\"margin-top-m\" text-center>       {{ translate(!reSent ? 'new_account.validation_by_mail.title' : 'new_account.validation_by_mail.resended_title') }}     </h3>      <h5 class=\"fmm-primary-text fmm-regular vertical-margin-s\" text-center>       {{ translate(!reSent ? 'new_account.validation_by_mail.description' : 'new_account.validation_by_mail.resended_description') }}     </h5>    </ion-card-content>    <ion-card-content *ngIf=\"!reSent\" class=\"padding-m\">     <div text-center>       <a title=\"{{ componentSettings.resend }}\" class=\"font-m clickable\" tabindex=\"0\" (keyup.enter)=\"resendValidation()\" (click)=\"resendValidation()\">         {{ translate('change-blocked-user-check-mail.new_account.validation_by_mail.resend') }}       </a>     </div>   </ion-card-content>  </ion-card>"
                },] },
    ];
    ChangeBlockedUserCheckMailComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: Events, },
        { type: UserProvider, },
    ]; };
    ChangeBlockedUserCheckMailComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return ChangeBlockedUserCheckMailComponent;
}());
export { ChangeBlockedUserCheckMailComponent };
//# sourceMappingURL=change-blocked-user-check-mail.js.map