import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { ComponentSettingsProvider } from "../../../../../providers/component-settings/component-settings";
import { TranslationProvider } from "../../../../../providers/translation/translation";
import { UserProvider } from "../../../../../providers/user/user";
import { Events } from 'ionic-angular';
import { PasswordChangeRequest } from '../../../../../models/login/passwordChangeRequest';
var ChangeBlockedUserPasswordChangeComponent = (function () {
    function ChangeBlockedUserPasswordChangeComponent(componentSettingsProvider, translationProvider, formBuilder, userProvider, events) {
        this.componentSettingsProvider = componentSettingsProvider;
        this.translationProvider = translationProvider;
        this.formBuilder = formBuilder;
        this.userProvider = userProvider;
        this.events = events;
        this.errorGotten = false;
        this.passwordsMismatched = false;
        this.translationProvider.bind(this);
        this.changePasswordForm = this.formBuilder.group({
            password: ['', Validators.required],
            validationPassword: ['', [Validators.required, this.passwordValidator]]
        }, {
            validator: this.passwordsValidator('password', 'validationPassword')
        });
        this.componentSettings = {
            title: this.translationProvider.getValueForKey('change-blocked-user-password-change.blocked_user.password_change.title'),
            subtitle: this.translationProvider.getValueForKey('change-blocked-user-password-change.blocked_user.password_change.subtitle'),
            new_password_password_tag: this.translationProvider.getValueForKey('change-blocked-user-password-change.blocked_user.password_change.password'),
            new_password_repeat_password_tag: this.translationProvider.getValueForKey('change-blocked-user-password-change.blocked_user.password_change.password_repeat'),
            new_password_passwords_mismatch_tag: this.translationProvider.getValueForKey('change-blocked-user-password-change.blocked_user.password_change.mismatch'),
            confirm_button_tag: this.translationProvider.getValueForKey('change-blocked-user-password-change.blocked_user.password_change.confirm')
        };
        this.componentReady = true;
    }
    ChangeBlockedUserPasswordChangeComponent.prototype.onPasswordBlur = function () {
        var password = this.changePasswordForm.value.password;
        var confirmPassword = this.changePasswordForm.value.validationPassword;
        this.passwordsMismatched = password && confirmPassword && password !== confirmPassword;
        if (this.passwordsMismatched) {
            this.scrollToTopComponent();
        }
    };
    ChangeBlockedUserPasswordChangeComponent.prototype.onClose = function () {
        this.events.publish(ChangeBlockedUserPasswordChangeComponent.onCloseEvent);
    };
    ChangeBlockedUserPasswordChangeComponent.prototype.scrollToTopPage = function () {
        this.events.publish('scrollToTop');
    };
    ChangeBlockedUserPasswordChangeComponent.prototype.scrollToTopComponent = function () {
        this.events.publish('scrollToElement', 'passwordsMismatched');
    };
    ChangeBlockedUserPasswordChangeComponent.prototype.attemptPasswordChange = function () {
        var _this = this;
        this.componentSettingsProvider.getClientsSettings(this.mocked, this.userName)
            .subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            var userValidated = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
            if (userValidated) {
                mapfreResponse.data[0].security_questions = null;
                var userData = mapfreResponse.data[0];
                var userPut = new PasswordChangeRequest(userData.username, userData.name, userData.surname1, userData.surname2, userData.identification_number, userData.mobile_phone, userData.mail, userData.address, _this.changePasswordForm.value.password, userData.security_questions);
                _this.userProvider.sendPasswordChange(userPut, userData.id).subscribe(function (response) {
                    var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                    var success = _this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse);
                    if (success) {
                        _this.events.publish(ChangeBlockedUserPasswordChangeComponent.onPasswordChangeEvent);
                    }
                    _this.scrollToTopPage();
                });
            }
        });
    };
    ChangeBlockedUserPasswordChangeComponent.prototype.passwordsValidator = function (passwordKey, confirmPasswordKey) {
        return function (group) {
            var password = group.controls[passwordKey];
            var confirmPassword = group.controls[confirmPasswordKey];
            if (password.value && confirmPassword.value && password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        };
    };
    ChangeBlockedUserPasswordChangeComponent.prototype.passwordValidator = function (input) {
        if (input.value === input.root.value['password']) {
            return null;
        }
        else {
            return {
                isValid: true
            };
        }
    };
    ChangeBlockedUserPasswordChangeComponent.prototype.closeResponse = function () {
        this.passwordsMismatched = false;
    };
    ChangeBlockedUserPasswordChangeComponent.componentName = 'change-blocked-user-check-mail';
    ChangeBlockedUserPasswordChangeComponent.onCloseEvent = ChangeBlockedUserPasswordChangeComponent.componentName + ":close";
    ChangeBlockedUserPasswordChangeComponent.onPasswordChangeEvent = ChangeBlockedUserPasswordChangeComponent.componentName + ":password-change";
    ChangeBlockedUserPasswordChangeComponent.inputs = ['userName'];
    ChangeBlockedUserPasswordChangeComponent.configInputs = ['mocked', 'styles'];
    ChangeBlockedUserPasswordChangeComponent.decorators = [
        { type: Component, args: [{
                    selector: 'change-blocked-user-password-change',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin *ngIf=\"componentReady\" id=\"passwordsMismatched\" [ngStyle]=\"styles\">    <form [formGroup]=\"changePasswordForm\" (ngSubmit)=\"attemptPasswordChange()\">      <ion-card-content class=\"no-padding-top\">        <h3 class=\"margin-top-m\" text-center>         {{ componentSettings.title }}       </h3>        <h5 class=\"fmm-primary-text fmm-regular vertical-margin-s\" text-center>         {{ componentSettings.subtitle }}       </h5>        <ion-item no-padding class=\"required\">         <ion-label floating>           {{ componentSettings.new_password_password_tag }}         </ion-label>         <ion-input type=\"password\" formControlName=\"password\" (ionBlur)=\"onPasswordBlur()\"></ion-input>       </ion-item>        <ion-item no-padding class=\"required\">         <ion-label floating>           {{ componentSettings.new_password_repeat_password_tag }}         </ion-label>         <ion-input type=\"password\" formControlName=\"validationPassword\" (ionBlur)=\"onPasswordBlur()\"></ion-input>       </ion-item>       <ion-item col-12 *ngIf=\"passwordsMismatched\" class=\"input-value-error\">         <span>           {{ translate('change-blocked-user-password-change.new_account.passwords_mismatch') }}         </span>       </ion-item>       <div class=\"margin-top-xl\">         <button ion-button full text-uppercase [disabled]=\"!changePasswordForm.valid\">           {{ componentSettings.confirm_button_tag }}         </button>       </div>      </ion-card-content>    </form> </ion-card>"
                },] },
    ];
    ChangeBlockedUserPasswordChangeComponent.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: TranslationProvider, },
        { type: FormBuilder, },
        { type: UserProvider, },
        { type: Events, },
    ]; };
    ChangeBlockedUserPasswordChangeComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'userName': [{ type: Input },],
    };
    return ChangeBlockedUserPasswordChangeComponent;
}());
export { ChangeBlockedUserPasswordChangeComponent };
//# sourceMappingURL=change-blocked-user-password-change.js.map