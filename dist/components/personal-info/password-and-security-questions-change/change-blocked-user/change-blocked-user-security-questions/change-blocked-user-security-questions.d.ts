import { OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { Validation } from "../../../../../models/validation";
import { SecurityQuestion } from "../../../../../models/securityQuestion";
import { UserProvider } from "../../../../../providers/user/user";
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../../../providers/translation/translation';
export declare class ChangeBlockedUserSecurityQuestionsComponent implements OnInit {
    private userProvider;
    private formBuilder;
    events: Events;
    private translationProvider;
    componentReady: boolean;
    componentSettings: Object;
    mocked?: boolean;
    styles?: any;
    needsPinChange: boolean;
    securityQuestionsIn: any[];
    userName: any;
    securityQuestionsForm: FormGroup;
    questionsValidated: boolean;
    validationContent: Validation;
    messageSuccess: boolean;
    securityQuestions: SecurityQuestion[];
    static inputs: string[];
    static configInputs: string[];
    constructor(userProvider: UserProvider, formBuilder: FormBuilder, events: Events, translationProvider: TranslationProvider);
    ngOnInit(): void;
    scrollToTopPage(): void;
    scrollToTopComponent(): void;
    sendSecurityQuestionsAnswers(): void;
    closeResponse(): void;
}
