import { OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { NavigationProvider } from '../../../providers/navigation/navigation';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class PasswordAndSecurityQuestionsChangeComponent implements OnInit {
    private navCtrl;
    private navigationProvider;
    private modalCtrl;
    private translationProvider;
    componentReady: boolean;
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    constructor(navCtrl: NavController, navigationProvider: NavigationProvider, modalCtrl: ModalController, translationProvider: TranslationProvider);
    ngOnInit(): void;
    openChangePasswordModal(): void;
    openSecurityQuestionsModal(): void;
}
