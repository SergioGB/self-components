import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { NavigationProvider } from '../../../providers/navigation/navigation';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ChangedPasswordModalComponent } from '../../_modals/_user/changed-password-modal/changed-password-modal';
import { ChangedSecurityQuestionModalComponent } from '../../_modals/_user/changed-security-question-modal/changed-security-question-modal';
var PasswordAndSecurityQuestionsChangeComponent = (function () {
    function PasswordAndSecurityQuestionsChangeComponent(navCtrl, navigationProvider, modalCtrl, translationProvider) {
        this.navCtrl = navCtrl;
        this.navigationProvider = navigationProvider;
        this.modalCtrl = modalCtrl;
        this.translationProvider = translationProvider;
        this.translationProvider.bind(this);
    }
    PasswordAndSecurityQuestionsChangeComponent.prototype.ngOnInit = function () {
        this.componentReady = true;
    };
    PasswordAndSecurityQuestionsChangeComponent.prototype.openChangePasswordModal = function () {
        this.modalCtrl.create(ChangedPasswordModalComponent, {
            labels: {
                current_password: "modal.account-data.changed-password-modal.change_password.current_password",
                new_password: "modal.account-data.changed-password-modal.change_password.new_password",
                old_wrong: "modal.account-data.changed-password-modal.change_password.old_wrong",
                passwords_mismatch: "modal.account-data.changed-password-modal.change_password.passwords_mismatch",
                repeat_password: "modal.account-data.changed-password-modal.change_password.repeat_password",
                subtitle: "modal.account-data.changed-password-modal.change_password.subtitle",
                title: "modal.account-data.changed-password-modal.change_password.title",
                accept: "modal.account-data.changed-password-modal.generic.accept",
                cancel: "modal.account-data.changed-password-modal.generic.cancel",
                forgot_password: "modal.account-data.changed-password-modal.login.forgot_password"
            }
        }).present();
    };
    PasswordAndSecurityQuestionsChangeComponent.prototype.openSecurityQuestionsModal = function () {
        this.modalCtrl.create(ChangedSecurityQuestionModalComponent, {
            labels: {
                first_answer: "modal.account-data.changed-security-question-modal.change_security_questions.first_answer",
                first_question: "modal.account-data.changed-security-question-modal.change_security_questions.first_question",
                second_answer: "modal.account-data.changed-security-question-modal.change_security_questions.second_answer",
                second_question: "modal.account-data.changed-security-question-modal.change_security_questions.second_question",
                subtitle: "modal.account-data.changed-security-question-modal.change_security_questions.subtitle",
                title: "modal.account-data.changed-security-question-modal.change_security_questions.title",
                accept: "modal.account-data.changed-security-question-modal.generic.accept",
                cancel: "modal.account-data.changed-security-question-modal.generic.cancel"
            }
        }).present();
    };
    PasswordAndSecurityQuestionsChangeComponent.configInputs = ['mocked', 'styles'];
    PasswordAndSecurityQuestionsChangeComponent.decorators = [
        { type: Component, args: [{
                    selector: 'password-and-security-questions-change',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">   <ion-card-content *ngIf=\"!componentReady\">     <loading-spinner></loading-spinner>   </ion-card-content>    <ion-card-content *ngIf=\"componentReady\" class=\"vertical-padding-s\">     <ion-item no-padding class=\"clickable\" (click)=\"openChangePasswordModal()\">       <h3>         {{ translate('password-and-security-questions-change.personal_data_page.security_password') }}       </h3>     </ion-item>      <hr class=\"vertical-margin-s\">      <ion-item no-padding class=\"clickable\" (click)=\"openSecurityQuestionsModal()\">       <h3>         {{ translate('password-and-security-questions-change.personal_data_page.security_questions') }}       </h3>     </ion-item>   </ion-card-content> </ion-card>"
                },] },
    ];
    PasswordAndSecurityQuestionsChangeComponent.ctorParameters = function () { return [
        { type: NavController, },
        { type: NavigationProvider, },
        { type: ModalController, },
        { type: TranslationProvider, },
    ]; };
    PasswordAndSecurityQuestionsChangeComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return PasswordAndSecurityQuestionsChangeComponent;
}());
export { PasswordAndSecurityQuestionsChangeComponent };
//# sourceMappingURL=password-and-security-questions-change.js.map