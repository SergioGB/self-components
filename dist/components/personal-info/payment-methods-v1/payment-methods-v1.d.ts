import { ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Events, ModalController } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { TranslationProvider } from '../../../providers/translation/translation';
import { UserProvider } from '../../../providers/user/user';
import { FormatProvider } from '../../../providers/format/format';
export declare class PaymentMethodsV1Component {
    private formBuilder;
    private componentSettingsProvider;
    private translationProvider;
    private elementRef;
    private userProvider;
    private formatProvider;
    private modalController;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    payments: any[];
    addPaymentForm: FormGroup;
    addPaymentFormVisible: boolean;
    selectedPaymentMethodType: string;
    paymentsReady: boolean;
    formUnlocked: boolean;
    cardHeaderPadding: number;
    cardHeaderBorderThickness: number;
    fullHeight: number;
    collapsedHeight: number;
    collapsed: boolean;
    ibanEditable: boolean;
    BankAccountForm: FormGroup;
    static componentName: string;
    static modifyCardEvent: string;
    static openFormEvent: string;
    static openCallMeEvent: string;
    static configInputs: string[];
    private iban1;
    private iban2;
    private iban3;
    private iban4;
    private iban5;
    private iban6;
    constructor(formBuilder: FormBuilder, componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, elementRef: ElementRef, userProvider: UserProvider, formatProvider: FormatProvider, modalController: ModalController, events: Events);
    ngOnInit(): void;
    formatDate(date: Date): string;
    showExpirationInfo(date: any): boolean;
    toggle(): void;
    storeHeights(): void;
    getProperHeight(): string;
    toggleAddPaymentForm(): void;
    toggleEditPaymentForm(): void;
    attemptPaymentMethodSubmission(): void;
    attemptModifyPaymentMethod(): void;
    selectPaymentMethodType(paymentMethodType: string): void;
    modifyDisabled(): boolean;
    modifyCardOpen(): void;
    deletePaymentMethod(payment: any): void;
    private mockData;
    private queryData;
    formatPaymentMethodNumber(paymentMethod: any): string;
    private processPayments;
    private initializeForm;
    nextIban(value: any, length: number, nextElement: any): void;
    private initializeModifyForm;
    private onUpdateSuccess;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
