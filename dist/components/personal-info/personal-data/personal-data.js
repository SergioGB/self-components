import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import * as jQuery from 'jquery';
import { Mocks } from '../../../providers/mocks/mocks';
import { TranslationProvider } from '../../../providers/translation/translation';
import { UserProvider } from '../../../providers/user/user';
import { PersonalInfoProvider } from '../../../providers/personal-info/personal-info';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { FormatProvider } from '../../../providers/format/format';
import { RenovationModalComponent } from '../../_modals/renovation-modal/renovation-modal';
var PersonalDataComponent = (function () {
    function PersonalDataComponent(translationProvider, componentSettingsProvider, userProvider, formatProvider, elementRef, modalCtrl, personalInfoProvider) {
        this.translationProvider = translationProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.userProvider = userProvider;
        this.formatProvider = formatProvider;
        this.elementRef = elementRef;
        this.modalCtrl = modalCtrl;
        this.personalInfoProvider = personalInfoProvider;
        this.defaultIcons = ["mapfre-arrow-up"];
        this.cardHeaderPadding = 1;
        this.cardHeaderBorderThickness = 1;
        this.translationProvider.bind(this);
    }
    PersonalDataComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    PersonalDataComponent.prototype.formatDate = function (date) {
        return this.formatProvider.formatDate(date);
    };
    PersonalDataComponent.prototype.presentModalRenovation = function () {
        this.modalCtrl.create(RenovationModalComponent, { mocked: true, labels: {
                title: this.translationProvider.getValueForKey('modal.personal-data.renovation-modal.accept_renovation.done.title'),
                description: this.translationProvider.getValueForKey('modal.personal-data.renovation-modal.accept_renovation.done.subtitle')
            } }).present();
    };
    PersonalDataComponent.prototype.toggle = function () {
        var _this = this;
        if (!this.fullHeight) {
            this.storeHeights();
        }
        setTimeout(function () {
            _this.collapsed = !_this.collapsed;
        });
    };
    PersonalDataComponent.prototype.storeHeights = function () {
        var wrapperElement = jQuery(this.elementRef.nativeElement).find('ion-card');
        this.fullHeight = wrapperElement.outerHeight();
        this.collapsedHeight = wrapperElement.find('ion-card-header').outerHeight() - this.cardHeaderPadding - this.cardHeaderBorderThickness;
        wrapperElement.css('height', this.fullHeight + "px");
    };
    PersonalDataComponent.prototype.getProperHeight = function () {
        return this.fullHeight ? ((this.collapsed ? this.collapsedHeight : this.fullHeight) + "px") : 'auto';
    };
    PersonalDataComponent.prototype.mockData = function () {
        this.accountData = Mocks.getAccountData();
        this.componentReady = true;
    };
    PersonalDataComponent.prototype.loadData = function () {
        var _this = this;
        var identification;
        this.userProvider.getUserData().subscribe(function (response) {
            if (response) {
                _this.personalInfoProvider.setUserData(response.personal_data);
                _this.userData = response.personal_data;
                _this.componentSettingsProvider.getIdentificationDocuments(_this.mocked).subscribe(function (res) {
                    var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(res);
                    if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                        mapfreResponse.data.forEach(function (element) {
                            if (element.id === _this.userData.identification_document_id) {
                                identification = element.title;
                            }
                        });
                        _this.accountData = {
                            username: _this.userData.name,
                            surname1: _this.userData.surname1,
                            surname2: _this.userData.surname2,
                            birthdate: _this.userData.birthdate,
                            birthplace: _this.userData.birthplace,
                            identification_type: identification,
                            identification_number: _this.userData.identification_document_number,
                            gender: _this.userData.gender,
                            civil_state: _this.userData.civil_state,
                            nacionality: _this.userData.nationality,
                            job: _this.userData.profession,
                            driving_license: _this.userData.license_type,
                            expedition_date: _this.userData.license_date,
                            health_card: _this.userData.health_card
                        };
                        _this.componentReady = true;
                    }
                });
            }
        });
    };
    PersonalDataComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    PersonalDataComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    PersonalDataComponent.inputs = ['account'];
    PersonalDataComponent.configInputs = ['mocked', 'icons', 'styles', 'modals'];
    PersonalDataComponent.decorators = [
        { type: Component, args: [{
                    selector: 'personal-data',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["personal-data ion-grid.padded-cols ion-row ion-col {   padding: 0 4px !important; }"],
                    template: "<div *ngIf=\"componentReady && accountData\" [ngStyle]=\"styles\">   <ion-card [style.height]=\"getProperHeight()\" class=\"animated medium\">     <ion-card-header tabindex=\"0\" (click)=\"toggle()\" class=\"clickable no-padding-bottom\">       <ion-item>         <h3>           {{ translate('personal-data.personal_data_page.personal_info.title') }}         </h3>         <ion-icon           class=\"animated medium no-margin-right\" item-end           [name]=\"getIcon(0)\"           [class.rotate-180]=\"collapsed\">         </ion-icon>       </ion-item>     </ion-card-header>      <!-- Basic Info -->     <ion-card-content class=\"no-padding-bottom\">       <ion-item no-padding>         <ion-grid no-padding class=\"values-list padded-cols\">            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.name') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{ accountData.username }} {{ accountData.surname1 }} {{ accountData.surname2 }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\">                 {{ translate('personal-data.personal_data_page.personal_info.identity_number') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{ accountData.identification_type }} - {{ accountData.identification_number }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.birthdate') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{ formatDate(accountData.birthdate) }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.birthplace') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{ accountData.birthplace }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.nacionality') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{accountData.nacionality }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.civil_state') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{accountData.civil_state }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.gender') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{ accountData.gender }} </span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.job') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{accountData.job }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.driving_license') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{accountData.driving_license }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.expedition_date') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{formatDate(accountData.expedition_date) }}</span>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-5 col-xl-4>               <strong class=\"fmm-body1 primary-text\" [class.required]=\"formUnlocked\">                 {{ translate('personal-data.personal_data_page.personal_info.health_card') }}               </strong>             </ion-col>              <ion-col col-12 col-sm-7 col-xl-8>               <span class=\"secondary-text\">{{accountData.health_card }}</span>             </ion-col>           </ion-row>          </ion-grid>       </ion-item>     </ion-card-content>   </ion-card> </div>"
                },] },
    ];
    PersonalDataComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: ComponentSettingsProvider, },
        { type: UserProvider, },
        { type: FormatProvider, },
        { type: ElementRef, },
        { type: ModalController, },
        { type: PersonalInfoProvider, },
    ]; };
    PersonalDataComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'modals': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'account': [{ type: Input },],
    };
    return PersonalDataComponent;
}());
export { PersonalDataComponent };
//# sourceMappingURL=personal-data.js.map