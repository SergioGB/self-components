import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { UserProvider } from '../../../providers/user/user';
export declare class SignMethodsComponent implements OnInit {
    private translationsProvider;
    private userProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    componentReady: boolean;
    visualize: boolean;
    constructor(translationsProvider: TranslationProvider, userProvider: UserProvider);
    ngOnInit(): void;
    isCollapsed(): boolean;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
