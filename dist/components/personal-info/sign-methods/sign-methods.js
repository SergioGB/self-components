import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { UserProvider } from '../../../providers/user/user';
var SignMethodsComponent = (function () {
    function SignMethodsComponent(translationsProvider, userProvider) {
        this.translationsProvider = translationsProvider;
        this.userProvider = userProvider;
        this.defaultIcons = ["mapfre-arrow-up"];
        this.translationsProvider.bind(this);
    }
    SignMethodsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.componentReady = true;
        this.userProvider.getUserData().subscribe(function (userData) {
            if (userData.id !== '997' && userData.id !== '996') {
                _this.visualize = true;
            }
        });
    };
    SignMethodsComponent.prototype.isCollapsed = function () {
        return true;
    };
    SignMethodsComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    SignMethodsComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    SignMethodsComponent.configInputs = ['mocked', 'icons', 'styles'];
    SignMethodsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'sign-methods',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"visualize\" class=\"animated medium\" [ngStyle]=\"styles\">   <ion-card-header tabindex=\"0\" class=\"clickable no-padding-bottom\">     <ion-item>       <h3>         {{ translate('sign-methods.personal_data_page.sign_methods.title') }}       </h3>        <ion-icon         [name]=\"getIcon(0)\"         item-end class=\"animated medium no-margin-right\"         [class.rotate-180]=\"isCollapsed()\">       </ion-icon>     </ion-item>   </ion-card-header> </ion-card>"
                },] },
    ];
    SignMethodsComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: UserProvider, },
    ]; };
    SignMethodsComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return SignMethodsComponent;
}());
export { SignMethodsComponent };
//# sourceMappingURL=sign-methods.js.map