import { OnInit } from '@angular/core';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { TranslationProvider } from '../../../providers/translation/translation';
import { UserProvider } from '../../../providers/user/user';
export declare class WelcomePackLinkCardComponent implements OnInit {
    private modalCtrl;
    private translationProvider;
    private user;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    modals?: any[];
    defaultIcons: string[];
    static configInputs: string[];
    componentReady: boolean;
    enableShow: boolean;
    constructor(modalCtrl: ModalController, translationProvider: TranslationProvider, user: UserProvider);
    ngOnInit(): void;
    displayWelcomePack(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
