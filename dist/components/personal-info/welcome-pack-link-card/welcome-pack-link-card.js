import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { WelcomePackModalComponent } from '../../_modals/welcome-pack-modal/welcome-pack-modal';
import { TranslationProvider } from '../../../providers/translation/translation';
import { UserProvider } from '../../../providers/user/user';
var WelcomePackLinkCardComponent = (function () {
    function WelcomePackLinkCardComponent(modalCtrl, translationProvider, user) {
        this.modalCtrl = modalCtrl;
        this.translationProvider = translationProvider;
        this.user = user;
        this.modals = [];
        this.defaultIcons = ["mapfre-present", "mapfre-arrow-right"];
        this.translationProvider.bind(this);
    }
    WelcomePackLinkCardComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.modals = [{
                    type: 23,
                    modalId: "welcome-pack-modal",
                    componentId: "welcome-pack-link-card",
                    reference: 'WelcomePackModalComponent',
                    reflabel: 'Modal de Welcome Pack',
                    labels: {
                        button_text: "modal.welcome-pack-link-card.welcome-pack-modal.welcome_pack.button"
                    }
                }];
        }
        this.componentReady = true;
        this.enableShow = !this.user.isPotentialClient();
    };
    WelcomePackLinkCardComponent.prototype.displayWelcomePack = function () {
        this.modalCtrl.create(WelcomePackModalComponent, { labels: this.modals[0].labels }).present();
    };
    WelcomePackLinkCardComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    WelcomePackLinkCardComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    WelcomePackLinkCardComponent.configInputs = ['mocked', 'icons', 'styles', 'modals'];
    WelcomePackLinkCardComponent.decorators = [
        { type: Component, args: [{
                    selector: 'welcome-pack-link-card',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"enableShow\" [ngStyle]=\"styles\">   <ion-card-content *ngIf=\"!componentReady\">     <loading-spinner></loading-spinner>   </ion-card-content>    <ion-card-content class=\"clickable vertical-padding-s\" (tap)=\"displayWelcomePack()\">     <ion-item *ngIf=\"componentReady\" no-padding>       <ion-icon [name]=\"getIcon(0)\" item-start large></ion-icon>       <h3>         {{ translate('welcome-pack-link-card.welcome_pack.display') }}       </h3>       <ion-icon item-end large [name]=\"getIcon(1)\" class=\"primary\"></ion-icon>     </ion-item>   </ion-card-content> </ion-card>"
                },] },
    ];
    WelcomePackLinkCardComponent.ctorParameters = function () { return [
        { type: ModalController, },
        { type: TranslationProvider, },
        { type: UserProvider, },
    ]; };
    WelcomePackLinkCardComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'modals': [{ type: Input },],
    };
    return WelcomePackLinkCardComponent;
}());
export { WelcomePackLinkCardComponent };
//# sourceMappingURL=welcome-pack-link-card.js.map