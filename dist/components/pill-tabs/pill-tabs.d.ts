import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { Pill } from '../../models/pill';
export declare class PillTabsComponent {
    private translationProvider;
    private events;
    mocked?: boolean;
    styles?: any;
    pills: Pill[];
    target?: string;
    chained?: boolean;
    static componentName: string;
    static activationEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events);
    activate(activePill: Pill): void;
    static getActivationEvent(target: string): string;
}
