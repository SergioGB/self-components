import { OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
export declare class PillComponent implements OnInit {
    private events;
    mocked?: any;
    icons?: string[];
    styles?: any;
    label: string;
    data?: any;
    active?: boolean;
    removable?: boolean;
    defaultIcons: string[];
    static componentName: string;
    static removalEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(events: Events);
    ngOnInit(): void;
    remove(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
