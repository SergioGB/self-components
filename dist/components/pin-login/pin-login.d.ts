import { Events, ModalController, NavController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { UserProvider } from '../../providers/user/user';
import { TranslationProvider } from '../../providers/translation/translation';
import { IdentificationProvider } from '../../providers/identification/identification';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { MapfreResponseEntity } from '../../models/mapfreResponseEntity';
import { WelcomePackProvider } from '../../providers/welcome-pack/welcome-pack';
export declare class PinLoginComponent {
    navCtrl: NavController;
    toastCtrl: ToastController;
    private events;
    private componentSettingsProvider;
    private userProvider;
    private translationProvider;
    private identificationProvider;
    private welcomePackProvider;
    private modalController;
    private statusBar;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    fingerprintAvailable: boolean;
    pinChangeResponse?: MapfreResponseEntity;
    defaultIcons: string[];
    fabric: any;
    keyboardSettings: any;
    text: string;
    pinValue: string;
    welcome_title: string;
    welcome_subtitle: string;
    hiddenForgottenPinButton: boolean;
    retryTimes: number;
    maxPinRetryTimes: number;
    errorGotten: boolean;
    userData: any;
    dayTimeSlots: any;
    automaticFingerprint: boolean;
    static componentName: string;
    static forgotPasswordPressed: string;
    static forgetMePressed: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(navCtrl: NavController, toastCtrl: ToastController, events: Events, componentSettingsProvider: ComponentSettingsProvider, userProvider: UserProvider, translationProvider: TranslationProvider, identificationProvider: IdentificationProvider, welcomePackProvider: WelcomePackProvider, modalController: ModalController, statusBar: StatusBar);
    ngOnInit(): void;
    closeMismatchAlert(): void;
    ngOnDestroy(): void;
    ionViewDidEnter(): void;
    showFingerprintDialog(): void;
    performLogin(): void;
    onPressButton(value: string): void;
    onPressDeleteButton(): void;
    onPressForgottenButton(): void;
    closeResponse(mapfreResponseEntity: MapfreResponseEntity): void;
    private validateAndNavigate;
    private onInvalidPin;
    private showWelcomePack;
    forgetMe(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
