import { OnInit } from '@angular/core';
import { InsuranceProvider } from '../../providers/insurance/insurance';
import { FormatProvider } from '../../providers/format/format';
import { Insurance } from '../../models/insurance';
import { Policy } from '../../models/policy';
import { SummaryData } from '../../models/summary';
import { Events } from 'ionic-angular';
export declare class PolicyCardComponent implements OnInit {
    private events;
    private insuranceProvider;
    private formatProvider;
    static componentName: string;
    mocked?: boolean;
    styles?: any;
    riskId: string;
    policyId: string;
    insurance: Insurance;
    policy: Policy;
    policyData: SummaryData;
    static completedEvent: string;
    static inputs: string[];
    static configInputs: string[];
    constructor(events: Events, insuranceProvider: InsuranceProvider, formatProvider: FormatProvider);
    ngOnInit(): void;
    private queryData;
    private getPolicyData;
    private updateCompleteness;
}
