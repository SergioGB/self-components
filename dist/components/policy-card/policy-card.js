import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import { InsuranceProvider } from '../../providers/insurance/insurance';
import { FormatProvider } from '../../providers/format/format';
import { Events } from 'ionic-angular';
var PolicyCardComponent = (function () {
    function PolicyCardComponent(events, insuranceProvider, formatProvider) {
        this.events = events;
        this.insuranceProvider = insuranceProvider;
        this.formatProvider = formatProvider;
    }
    PolicyCardComponent.prototype.ngOnInit = function () {
        this.queryData();
    };
    PolicyCardComponent.prototype.queryData = function () {
        var _this = this;
        if (this.riskId && this.policyId) {
            Observable.forkJoin([
                this.insuranceProvider.getInsuranceDetailSettings(this.mocked, this.riskId),
                this.insuranceProvider.getPolicyDetailSettings(this.mocked, this.policyId)
            ]).subscribe(function (responses) {
                if (_this.insuranceProvider.areResponsesValid(responses)) {
                    _this.insurance = _this.insuranceProvider.getResponseJSON(responses[0]).data;
                    _this.policy = _this.insuranceProvider.getResponseJSON(responses[1]).data;
                    _this.policyData = _this.getPolicyData();
                    _this.updateCompleteness();
                }
            });
        }
    };
    PolicyCardComponent.prototype.getPolicyData = function () {
        var policyData = this.policy.car_policy_detail ? this.policy.car_policy_detail.basic_info : this.policy.home_policy_detail.basic_info;
        return {
            title: 'policy_card.title',
            sections: [{
                    rows: [{
                            key: 'cancel_policy.summary.risk',
                            value: this.insurance.name
                        }, {
                            key: 'cancel_policy.summary.policy_type',
                            value: policyData.description.field_content
                        }, {
                            key: 'cancel_policy.summary.policy_number',
                            value: policyData.number.field_content
                        }, {
                            key: 'cancel_policy.summary.due_date',
                            value: this.formatProvider.formatDate(policyData.due_date.field_content)
                        }, {
                            key: 'cancel_policy.summary.amount',
                            value: this.formatProvider.formatCost(policyData.price.field_content)
                        }, {
                            key: 'cancel_policy.summary.payment_periodicity',
                            value: policyData.payment_modality.field_content
                        }]
                }]
        };
    };
    PolicyCardComponent.prototype.updateCompleteness = function () {
        this.events.publish(PolicyCardComponent.completedEvent, !_.isNil(this.policyData));
    };
    PolicyCardComponent.componentName = "PolicyCardComponent";
    PolicyCardComponent.completedEvent = PolicyCardComponent.componentName + ":completed";
    PolicyCardComponent.inputs = ['riskId', 'policyId'];
    PolicyCardComponent.configInputs = ['mocked', 'styles'];
    PolicyCardComponent.decorators = [
        { type: Component, args: [{
                    selector: 'policy-card',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["policy-card ion-card-content {   padding-top: 8px !important; }"],
                    template: "<summary   *ngIf=\"policyData\"   [summaryData]=\"policyData\"   [ngStyle]=\"styles\"> </summary>"
                },] },
    ];
    PolicyCardComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: InsuranceProvider, },
        { type: FormatProvider, },
    ]; };
    PolicyCardComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'riskId': [{ type: Input },],
        'policyId': [{ type: Input },],
    };
    return PolicyCardComponent;
}());
export { PolicyCardComponent };
//# sourceMappingURL=policy-card.js.map