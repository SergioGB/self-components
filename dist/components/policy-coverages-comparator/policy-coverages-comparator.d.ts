import { PlatformProvider } from '../../providers/platform/platform';
import { TranslationProvider } from '../../providers/translation/translation';
import { FormatProvider } from '../../providers/format/format';
export declare class PolicyCoveragesComparatorComponent {
    private platformProvider;
    private translationProvider;
    private formatProvider;
    icon?: string;
    defaultIcon: string;
    modalities: any[];
    coverages: any[];
    static componentName: string;
    constructor(platformProvider: PlatformProvider, translationProvider: TranslationProvider, formatProvider: FormatProvider);
    isCoverageOptional(modality: any, coverage: any): boolean;
    getIcon(): string;
    existIcon(): string;
}
