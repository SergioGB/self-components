import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { InsuranceProvider } from '../../providers/insurance/insurance';
var PolicyExtensionComponent = (function () {
    function PolicyExtensionComponent(events, translationProvider, insuranceProvider) {
        this.events = events;
        this.translationProvider = translationProvider;
        this.insuranceProvider = insuranceProvider;
        this.defaultIcons = ["mapfre-checkbox-on", "mapfre-checkbox-off"];
        this.translationProvider.bind(this);
        this.policiesExtension = [];
    }
    PolicyExtensionComponent.prototype.ngOnInit = function () {
        this.getInsurances();
        this.updateCompleteness();
    };
    PolicyExtensionComponent.prototype.toggleInsurancePolicy = function (insurance, policy) {
        var _this = this;
        if (this.isPolicySelected(policy)) {
            this.policiesExtension.forEach(function (associatedRisk, i) {
                if (_this.isAssociatedRiskEqualToPolicy(associatedRisk, policy)) {
                    _this.policiesExtension.splice(i, 1);
                }
            });
        }
        else {
            this.policiesExtension.push({
                risk_id: insurance.id,
                policy_id: policy.id,
                risk_description: insurance.name,
                policy_description: policy.type
            });
        }
        this.publishExtension();
    };
    PolicyExtensionComponent.prototype.isPolicySelected = function (policy) {
        var _this = this;
        var selected = false;
        this.policiesExtension.forEach(function (policyExtension) {
            selected = selected || _this.isAssociatedRiskEqualToPolicy(policyExtension, policy);
        });
        return selected;
    };
    PolicyExtensionComponent.prototype.isAssociatedRiskEqualToPolicy = function (associatedRisk, policy) {
        return associatedRisk.policy_id === policy.id && associatedRisk.risk_id === policy.risk_id;
    };
    PolicyExtensionComponent.prototype.getInsurances = function () {
        var _this = this;
        if (this.riskType) {
            this.insuranceProvider.getInsurancesOfType(this.mocked, this.riskType).then(function (insurances) {
                _this.loadInsurances(insurances);
            });
        }
        else {
            this.insuranceProvider.getInsurances(this.mocked).then(function (insurances) {
                _this.loadInsurances(insurances);
            });
        }
    };
    PolicyExtensionComponent.prototype.loadInsurances = function (insurances) {
        this.insurances = insurances;
        this.insurances.forEach(function (insurance) {
            insurance.associated_policies.forEach(function (policy) {
                policy.risk_id = insurance.id;
            });
        });
        this.events.publish(PolicyExtensionComponent.readyEvent);
    };
    PolicyExtensionComponent.prototype.publishExtension = function () {
        this.events.publish(PolicyExtensionComponent.extensionEvent, this.policiesExtension);
    };
    PolicyExtensionComponent.prototype.updateCompleteness = function () {
        this.events.publish(PolicyExtensionComponent.completedEvent, true);
    };
    PolicyExtensionComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    PolicyExtensionComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    PolicyExtensionComponent.componentName = "PolicyExtensionComponent";
    PolicyExtensionComponent.readyEvent = PolicyExtensionComponent.componentName + ":ready";
    PolicyExtensionComponent.extensionEvent = PolicyExtensionComponent.componentName + ":extension";
    PolicyExtensionComponent.completedEvent = PolicyExtensionComponent.componentName + ":completed";
    PolicyExtensionComponent.inputs = ['policyId', 'riskType', 'policiesExtension'];
    PolicyExtensionComponent.configInputs = ['mocked', 'icons', 'styles'];
    PolicyExtensionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'policy-extension',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card [ngStyle]=\"styles\">    <ion-card-header class=\"no-border no-padding-bottom\">     <h3>       {{ translate('policy-extension.policy_extension.title') }}     </h3>     <p class=\"fmm-body1 fmm-regular\">       {{ translate('policy-extension.policy_extension.subtitle') }}     </p>   </ion-card-header>    <ion-card-content *ngIf=\"insurances\" class=\"check-green\">     <div *ngFor=\"let insurance of insurances\">       <div *ngFor=\"let associated_policy of insurance.associated_policies\">         <ion-item *ngIf=\"associated_policy.id !== policyId\" no-padding class=\"no-border clickable\" (tap)=\"toggleInsurancePolicy(insurance, associated_policy)\" >           <ion-icon             [name]=\"isPolicySelected(associated_policy) ? getIcon(0) : getIcon(1)\" item-start>           </ion-icon>            <div item-start class=\"margin-left-s\">             <h3 class=\"margin-bottom-s\">               {{ insurance.name }}             </h3>             <p class=\"fmm-body1 fmm-regular\">               {{ associated_policy.type }}             </p>           </div>         </ion-item>       </div>     </div>   </ion-card-content>  </ion-card>"
                },] },
    ];
    PolicyExtensionComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: TranslationProvider, },
        { type: InsuranceProvider, },
    ]; };
    PolicyExtensionComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'policyId': [{ type: Input },],
        'riskType': [{ type: Input },],
        'policiesExtension': [{ type: Input },],
    };
    return PolicyExtensionComponent;
}());
export { PolicyExtensionComponent };
//# sourceMappingURL=policy-extension.js.map