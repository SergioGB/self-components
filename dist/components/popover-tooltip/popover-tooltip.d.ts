import { OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { PlatformProvider } from '../../providers/platform/platform';
import { TooltipData } from '../../models/tooltipData';
export declare class PopoverTooltipComponent implements OnInit {
    private platformProvider;
    private navParams;
    viewCtrl: ViewController;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    tooltipData: TooltipData;
    static configInputs: string[];
    constructor(platformProvider: PlatformProvider, navParams: NavParams, viewCtrl: ViewController);
    ngOnInit(): void;
    dismiss(): void;
    onMouseLeave(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
