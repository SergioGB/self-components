export declare class BudgetSelectionMocks {
    private static apiResponse;
    static getAPIResponse(): any;
    private static defaultIcons;
    static getDefaultIcons(): string[];
}
