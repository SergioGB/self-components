import { Injectable } from '@angular/core';
var BudgetSelectionMocks = (function () {
    function BudgetSelectionMocks() {
    }
    BudgetSelectionMocks.getAPIResponse = function () {
        return this.apiResponse;
    };
    BudgetSelectionMocks.getDefaultIcons = function () {
        return this.defaultIcons;
    };
    BudgetSelectionMocks.apiResponse = {
        "response": {
            "code": 0,
            "message": "string"
        },
        "data": [
            {
                "id": "1",
                "associated_risk_type": "C",
                "risk_name": "Ford Focus",
                "policy_modality": null,
                "number": "string",
                "state": "PENDING",
                "opening_date": "2019-04-23T09:17:03.242Z",
                "due_date": "2019-04-23T09:17:03.242Z",
                "price": {
                    "amount": 0,
                    "iso_code": "EUR"
                },
                "car_budget_detail": {
                    "vehicle_info": {
                        "brand": "string",
                        "model": "string",
                        "version": "string",
                        "registration": "string",
                        "doors_count": "string",
                        "release_date": "string",
                        "engine_power": "string",
                        "engine_capacity": "string",
                        "gearbox": "MANUAL",
                        "registration_date": "string",
                        "garage_type": "string"
                    },
                    "taker_info": {
                        "name": "string",
                        "gender": {
                            "id": "string",
                            "title": "string"
                        },
                        "bithdate": "string",
                        "zip_code": "string",
                        "identification_document_id": "string",
                        "identification_document_number": "string",
                        "license_date": "2019-04-23T09:17:03.242Z"
                    }
                },
                "home_budget_detail": {
                    "home_info": {
                        "address": "string",
                        "housing_type": {
                            "id": "string",
                            "title": "string"
                        },
                        "size": 0,
                        "construction_date": "2019-04-23T09:17:03.242Z",
                        "type": "string",
                        "use": "string",
                        "rooms_number": "string",
                        "bathrooms": "string",
                        "safety_guards": [
                            "string"
                        ],
                        "floor": "string",
                        "regime": {
                            "id": "string",
                            "title": "string"
                        },
                        "construction_quality": "string"
                    },
                    "taker_info": {
                        "name": "string",
                        "gender": {
                            "id": "string",
                            "title": "string"
                        },
                        "bithdate": "string",
                        "zip_code": "string",
                        "identification_document_id": "string",
                        "identification_document_number": "string"
                    }
                },
                "actions": [
                    {
                        "name": "string",
                        "allowed": true
                    }
                ]
            }
        ]
    };
    BudgetSelectionMocks.defaultIcons = ["mapfre-check", "mapfre-no-check"];
    BudgetSelectionMocks.decorators = [
        { type: Injectable },
    ];
    BudgetSelectionMocks.ctorParameters = function () { return []; };
    return BudgetSelectionMocks;
}());
export { BudgetSelectionMocks };
//# sourceMappingURL=mocks.js.map