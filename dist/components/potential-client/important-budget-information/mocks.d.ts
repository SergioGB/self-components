export declare class ImportantBudgetInformationMocks {
    private static apiResponse;
    static getAPIResponse(): any;
    private static defaultIcons;
    static getDefaultIcons(): string[];
}
