import { Injectable } from '@angular/core';
var ImportantBudgetInformationMocks = (function () {
    function ImportantBudgetInformationMocks() {
    }
    ImportantBudgetInformationMocks.getAPIResponse = function () {
        return this.apiResponse;
    };
    ImportantBudgetInformationMocks.getDefaultIcons = function () {
        return this.defaultIcons;
    };
    ImportantBudgetInformationMocks.apiResponse = {
        "response": {
            "code": 0,
            "message": "string"
        },
        "data": [
            {
                "id": "string",
                "order": 0,
                "done": false,
                "name": "Peritación de la vivienda",
                "description": "Es indispensable la valoración del estado de la vivienda por un perito profesional, iniciada en el plazo máximo de cinco días hábiles a partir de la fecha actual. Por favor, ponte en contacto con el <b>Taller Jiménez</b> en el teléfono <a href='tel:639382940'>639 38 29 40</a> para gestionar tu cita.",
                "date": "2019-04-25T15:40:10.325Z",
                "estimate": 0,
                "actions": [
                    {
                        "manage_appointment_allowed": true,
                        "cancel_appointment_allowed": false,
                        "phone": "678678678"
                    }
                ],
                "attachments": {
                    "documents": {
                        "count": 0,
                        "title": "string"
                    },
                    "images": {
                        "count": 0,
                        "title": "string"
                    }
                }
            },
            {
                "id": "string",
                "order": 1,
                "done": false,
                "name": "Peritación de la vivienda",
                "description": "Es indispensable la valoración del estado de la vivienda por un perito profesional, iniciada en el plazo máximo de cinco días hábiles a partir de la fecha actual. Por favor, llámanos para encontrar el taller que mejor se adapte a tus necesidades",
                "date": "2019-04-25T15:40:10.325Z",
                "estimate": 0,
                "actions": [
                    {
                        "manage_appointment_allowed": false,
                        "cancel_appointment_allowed": false,
                        "phone": "678678678"
                    }
                ],
                "attachments": {
                    "documents": {
                        "count": 0,
                        "title": "string"
                    },
                    "images": {
                        "count": 0,
                        "title": "string"
                    }
                }
            },
            {
                "id": "string",
                "order": 2,
                "done": false,
                "name": "Firma de las condiciones",
                "description": "Es indispensable la firma de las condiciones de la póliza para que la misma se haga efectiva y presentarlas en la oficina correspondiente.",
                "date": "2019-04-25T15:40:10.325Z",
                "estimate": 0,
                "actions": [
                    {
                        "manage_appointment_allowed": false,
                        "cancel_appointment_allowed": false
                    }
                ],
                "attachments": {
                    "documents": {
                        "count": 1,
                        "title": "string"
                    },
                    "images": {
                        "count": 0,
                        "title": "string"
                    }
                }
            }
        ]
    };
    ImportantBudgetInformationMocks.defaultIcons = ["mapfre-information"];
    ImportantBudgetInformationMocks.decorators = [
        { type: Injectable },
    ];
    ImportantBudgetInformationMocks.ctorParameters = function () { return []; };
    return ImportantBudgetInformationMocks;
}());
export { ImportantBudgetInformationMocks };
//# sourceMappingURL=mocks.js.map