import { Injectable } from '@angular/core';
var BudgetDetailsMocks = (function () {
    function BudgetDetailsMocks() {
    }
    BudgetDetailsMocks.getAPIResponse = function () {
        return this.apiResponse;
    };
    BudgetDetailsMocks.apiResponse = {
        "response": {
            "code": 0,
            "message": "string"
        },
        "data": {
            "id": "1",
            "associated_risk_type": "C",
            "risk_name": "Ford Focus",
            "policy_modality": null,
            "number": "7987545",
            "state": "PENDING",
            "opening_date": "2019-04-23T09:17:03.242Z",
            "due_date": "2019-04-23T09:17:03.242Z",
            "price": {
                "amount": 0,
                "iso_code": "EUR"
            },
            "car_budget_detail": {
                "vehicle_info": {
                    "brand": "Ford",
                    "model": "Focus",
                    "version": "FORD FOCUS 1.5TDCI AUTO-S SPORT 120",
                    "registration": "8562-BHS",
                    "doors_count": "5",
                    "release_date": "01/05/2008",
                    "engine_power": "120 CV",
                    "engine_capacity": "1498 cc",
                    "gearbox": "MANUAL",
                    "registration_date": "07/2008",
                    "garage_type": "Privado sin vigilancia"
                },
                "taker_info": {
                    "name": "Lucía Pérez López",
                    "gender": {
                        "id": "1",
                        "title": "Mujer"
                    },
                    "bithdate": "18/04/1986",
                    "zip_code": "28320",
                    "identification_document_id": "1",
                    "identification_document_number": "71265825G",
                    "license_date": "2019-04-23T09:17:03.242Z"
                }
            },
            "home_budget_detail": {
                "home_info": {
                    "address": "C/Álamo Blanco 23, 04620, Vera, Almería",
                    "housing_type": {
                        "id": "1",
                        "title": "Piso"
                    },
                    "size": 65,
                    "construction_date": "2019-04-23T09:17:03.242Z",
                    "type": "Estándar",
                    "use": "Estándar",
                    "rooms_number": "4",
                    "bathrooms": "1",
                    "safety_guards": [
                        "alarma conectada a central"
                    ],
                    "floor": "Intermedia",
                    "regime": {
                        "id": "1",
                        "title": "En propiedad"
                    },
                    "construction_quality": "En propiedad"
                },
                "taker_info": {
                    "name": "Lucía Pérez López",
                    "gender": {
                        "id": "1",
                        "title": "Mujer"
                    },
                    "bithdate": "18/04/1986",
                    "zip_code": "28320",
                    "identification_document_id": "1",
                    "identification_document_number": "71265825G",
                }
            },
            "actions": [
                {
                    "name": "string",
                    "allowed": true
                }
            ]
        }
    };
    BudgetDetailsMocks.decorators = [
        { type: Injectable },
    ];
    BudgetDetailsMocks.ctorParameters = function () { return []; };
    return BudgetDetailsMocks;
}());
export { BudgetDetailsMocks };
//# sourceMappingURL=mocks.js.map