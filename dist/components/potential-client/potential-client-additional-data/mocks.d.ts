export declare class PotentialClientAdditionalDataMock {
    private static contractBudget;
    static getContractBudget(): any;
    private static defaultIcons;
    static getDefaultIcons(): string[];
}
