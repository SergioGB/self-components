import { Injectable } from '@angular/core';
var PotentialClientAdditionalDataMock = (function () {
    function PotentialClientAdditionalDataMock() {
    }
    PotentialClientAdditionalDataMock.getContractBudget = function () {
        return this.contractBudget;
    };
    PotentialClientAdditionalDataMock.getDefaultIcons = function () {
        return this.defaultIcons;
    };
    PotentialClientAdditionalDataMock.contractBudget = {
        "budget_id": "string",
        "policy_type": {
            "id": "string",
            "payment_modality_id": "string",
            "additional_coverages": [
                {
                    "id": "string",
                    "sum_insured": {
                        "amount": 0,
                        "iso_code": "AED"
                    }
                }
            ]
        },
        "additional_information": {
            "init_date": "string",
            "taker_info": {
                "phone_number": "string",
                "address": {
                    "road_type_id": "string",
                    "road_name": "string",
                    "road_number": "string",
                    "zip_code": "string",
                    "city_id": "string",
                    "province_id": "string",
                    "country_id": "string"
                }
            },
            "owner_info": {
                "name": "string",
                "surname1": "string",
                "surname2": "string",
                "identification_document_id": "string",
                "identification_document_number": "string",
                "birthdate": "string"
            },
            "self_password": "string"
        },
        "documentation": [
            {
                "title": "string",
                "mime_type": "audio/aac",
                "content": "string",
                "owner": "MAPFRE"
            }
        ],
        "payment_method": {
            "id": "string",
            "type": 1,
            "bank_account_info": {
                "account_owner": {
                    "name": "string",
                    "surname1": "string",
                    "surname2": "string"
                },
                "account_iban": "string"
            }
        }
    };
    PotentialClientAdditionalDataMock.defaultIcons = ["mapfre-info-full", "mapfre-check", "mapfre-no-check"];
    PotentialClientAdditionalDataMock.decorators = [
        { type: Injectable },
    ];
    PotentialClientAdditionalDataMock.ctorParameters = function () { return []; };
    return PotentialClientAdditionalDataMock;
}());
export { PotentialClientAdditionalDataMock };
//# sourceMappingURL=mocks.js.map