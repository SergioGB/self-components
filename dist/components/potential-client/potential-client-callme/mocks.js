import { Injectable } from '@angular/core';
var PotentialClientCallmeMocks = (function () {
    function PotentialClientCallmeMocks() {
    }
    PotentialClientCallmeMocks.getDefaultIcons = function () {
        return this.defaultIcons;
    };
    PotentialClientCallmeMocks.defaultIcons = ["mapfre-rocket"];
    PotentialClientCallmeMocks.decorators = [
        { type: Injectable },
    ];
    PotentialClientCallmeMocks.ctorParameters = function () { return []; };
    return PotentialClientCallmeMocks;
}());
export { PotentialClientCallmeMocks };
//# sourceMappingURL=mocks.js.map