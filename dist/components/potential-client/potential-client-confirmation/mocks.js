import { Injectable } from '@angular/core';
var PotentialClientConfirmationMocks = (function () {
    function PotentialClientConfirmationMocks() {
    }
    PotentialClientConfirmationMocks.getDefaultIcons = function () {
        return this.defaultIcons;
    };
    PotentialClientConfirmationMocks.defaultIcons = ["mapfre-check", "mapfre-download", "mapfre-download"];
    PotentialClientConfirmationMocks.decorators = [
        { type: Injectable },
    ];
    PotentialClientConfirmationMocks.ctorParameters = function () { return []; };
    return PotentialClientConfirmationMocks;
}());
export { PotentialClientConfirmationMocks };
//# sourceMappingURL=mocks.js.map