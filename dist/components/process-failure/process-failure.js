import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Events, ModalController } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
import { ApiProvider } from '../../providers/api/api';
var ProcessFailureComponent = (function () {
    function ProcessFailureComponent(translationProvider, modalController, events, formBuilder, apiProvider) {
        this.translationProvider = translationProvider;
        this.modalController = modalController;
        this.events = events;
        this.formBuilder = formBuilder;
        this.apiProvider = apiProvider;
        this.defaultIcons = ["mapfre-warning"];
        this.contactPhone = "";
        this.translationProvider.bind(this);
    }
    ProcessFailureComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.mocked) {
            this.icon = 'mapfre-cross-error';
            this.title = 'Proceso cancelado';
            this.description = 'No se ha podido llevar a cabo tu modificación ni se ha cobrado el importe en el método de pago seleccionado.';
        }
        if (this.formCallMe) {
            this.callMeForm = this.formBuilder.group({
                phoneNumber: [this.contactPhone, Validators.required]
            });
            this.callMeForm.valueChanges.subscribe(function (val) {
                _this.events.publish(ProcessFailureComponent.phoneChangeEvent, val.phoneNumber);
            });
            this.events.publish(ProcessFailureComponent.phoneChangeEvent, this.contactPhone);
        }
        this.componentReady = true;
    };
    ProcessFailureComponent.prototype.callMe = function () {
        this.events.publish(ProcessFailureComponent.callMeEvent);
    };
    ProcessFailureComponent.prototype.finish = function () {
        this.events.publish(ProcessFailureComponent.finishChangeEvent);
    };
    ProcessFailureComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    ProcessFailureComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    ProcessFailureComponent.prototype.getTitle = function () {
        var language = this.apiProvider.getLanguage();
        return this.title[language] ? this.title[language] : "";
    };
    ProcessFailureComponent.prototype.getDescription = function () {
        var language = this.apiProvider.getLanguage();
        return this.description[language] ? this.description[language] : "";
    };
    ProcessFailureComponent.componentName = 'process-failure';
    ProcessFailureComponent.phoneChangeEvent = ProcessFailureComponent.componentName + ":phoneChange";
    ProcessFailureComponent.callMeEvent = ProcessFailureComponent.componentName + ":callMe";
    ProcessFailureComponent.finishChangeEvent = ProcessFailureComponent.componentName + ":finish";
    ProcessFailureComponent.configInputs = ['mocked', 'icons', 'styles', 'title', 'description', 'showCallMe', 'formCallMe', 'showFinish'];
    ProcessFailureComponent.decorators = [
        { type: Component, args: [{
                    selector: 'process-failure',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div text-center class=\"error-red\" *ngIf=\"componentReady\" [ngStyle]=\"styles\">   <ion-icon *ngIf=\"icon\" [name]=\"getIcon(0)\" huge class=\"margin-bottom-l margin-top-m\"></ion-icon>    <h3 *ngIf=\"title\">     {{ title }}   </h3>    <ion-row *ngIf=\"description\" margin-bottom class=\"margin-top-xs\">     <ion-col [attr.col-12]=\"true\" [attr.col-sm-6]=\"!isModal ? '' : null\" [attr.push-sm-3]=\"!isModal ? '' : null\"              [attr.col-sm-10]=\"isModal ? '' : null\" [attr.push-sm-1]=\"isModal ? '' : null\">        <p text-center class=\"fmm-body1 fmm-regular\">         {{ description }}       </p>     </ion-col>   </ion-row>    <form *ngIf=\"formCallMe\" [formGroup]=\"callMeForm\">     <ion-row margin-bottom class=\"margin-top-m\">       <ion-col col-12 col-md-2 push-md-5>         <ion-item no-padding class=\"modal-form-content\">           <ion-label floating>             {{ translate('process-failure.call_me_innactivity_modal.mobile_phone') }}           </ion-label>            <ion-input formControlName=\"phoneNumber\" type=\"tel\" maxlength=\"20\" pattern=\"(\\\\+[0-9]*)?[0-9]*\"></ion-input>         </ion-item>       </ion-col>     </ion-row>   </form>    <hr *ngIf=\"showCallMe || showFinish\">    <ion-row *ngIf=\"showCallMe\" class=\"confButt row\">     <ion-col col-12 col-sm-4 offset-sm-4 text-center>       <button full ion-button=\"\" class=\"button button-md button-default button-default-md\" style=\"transition: none;\"               (keyup.enter)=\"callMe()\" (tap)=\"callMe()\"><span class=\"button-inner\">             {{ translate('process-failure.help_method.call_me') }}</span>         <div class=\"button-effect\"></div>       </button>     </ion-col>   </ion-row>   <ion-row *ngIf=\"showFinish\" class=\"confButt row\">     <ion-col col-12 col-sm-4 offset-sm-4 text-center>       <button full ion-button=\"\" class=\"button button-md button-default button-default-md\" style=\"transition: none;\"               (keyup.enter)=\"finish()\" (tap)=\"finish()\"><span class=\"button-inner\">             {{ translate('process-failure.generic.finish') }}</span>         <div class=\"button-effect\"></div>       </button>     </ion-col>   </ion-row> </div>"
                },] },
    ];
    ProcessFailureComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: ModalController, },
        { type: Events, },
        { type: FormBuilder, },
        { type: ApiProvider, },
    ]; };
    ProcessFailureComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'icon': [{ type: Input },],
        'title': [{ type: Input },],
        'description': [{ type: Input },],
        'showCallMe': [{ type: Input },],
        'formCallMe': [{ type: Input },],
        'isModal': [{ type: Input },],
        'showFinish': [{ type: Input },],
    };
    return ProcessFailureComponent;
}());
export { ProcessFailureComponent };
//# sourceMappingURL=process-failure.js.map