import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
export declare class ProfessionalAppointmentComponent implements OnInit {
    private translationProvider;
    private componentSettingsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    optionProfessional: number;
    meeting: number;
    claimId: any;
    defaultIcons: string[];
    private selectedProfessional;
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider);
    ngOnInit(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
