import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
export declare class RatingComponent {
    private translationProvider;
    private events;
    flowReference: any;
    ratingText: any;
    linkText: any;
    mocked?: boolean;
    styles?: any;
    alreadyRated: boolean;
    rating: number;
    static componentName: string;
    static linkClickedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events);
    sendRating(event: any): void;
    linkClicked(): void;
}
