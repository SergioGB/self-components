import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../providers/translation/translation';
var RatingComponent = (function () {
    function RatingComponent(translationProvider, events) {
        this.translationProvider = translationProvider;
        this.events = events;
        this.alreadyRated = false;
        this.rating = 0;
        this.translationProvider.bind(this);
    }
    RatingComponent.prototype.sendRating = function (event) {
        this.alreadyRated = true;
        this.rating = 5 - event + 1;
    };
    RatingComponent.prototype.linkClicked = function () {
        this.events.publish(RatingComponent.linkClickedEvent, true);
    };
    RatingComponent.componentName = 'rating-component';
    RatingComponent.linkClickedEvent = RatingComponent.componentName + ":linkClicked";
    RatingComponent.inputs = [
        'flowReference', 'ratingText', 'linkText'
    ];
    RatingComponent.configInputs = ['mocked', 'styles'];
    RatingComponent.decorators = [
        { type: Component, args: [{
                    selector: 'rating-component',
                    template: "<div *ngIf=\"flowReference\" [ngStyle]=\"styles\">   <h3 [hidden]=\"alreadyRated\" class=\"margin-bottom-xs\">     {{ translate('rating.rating.request') }}   </h3>    <h3 *ngIf=\"ratingText && ratingText !== true\" class=\"font-16-h3 margin-top-s margin-bottom-xs\" [hidden]=\"alreadyRated\">     {{ ratingText }}   </h3>   <h3 [hidden]=\"!alreadyRated\">     {{ translate('rating.rating.response') }}   </h3>   <rating [hidden]=\"alreadyRated\" [(ngModel)]=\"rating\" readOnly=\"false\" max=\"5\" emptyStarIconName=\"star-outline\"           halfStarIconName=\"star-half\" starIconName=\"star\" nullable=\"false\"           (ngModelChange)=\"sendRating($event)\"></rating>   <div class=\"margin-top-xs\">     <a class=\"font-m text-uppercase\" *ngIf=\"linkText\" (click)=\"linkClicked()\">{{ linkText }}</a>   </div> </div>"
                },] },
    ];
    RatingComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: Events, },
    ]; };
    RatingComponent.propDecorators = {
        'flowReference': [{ type: Input },],
        'ratingText': [{ type: Input },],
        'linkText': [{ type: Input },],
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return RatingComponent;
}());
export { RatingComponent };
//# sourceMappingURL=rating.js.map