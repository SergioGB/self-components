import { OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { InsuranceProvider } from '../../providers/insurance/insurance';
export declare class ReceiptSelectionComponent implements OnInit {
    private events;
    private insuranceProvider;
    static componentName: string;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    eventTarget?: string;
    selectedReceipt: any;
    receipts: any;
    defaultIcons: string[];
    static completedEvent: string;
    static getSelectionEventName(eventTarget?: string): string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(events: Events, insuranceProvider: InsuranceProvider);
    ngOnInit(): void;
    selectReceipt(receipt: any): void;
    private publishSelection;
    private updateCompleteness;
    private queryData;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
