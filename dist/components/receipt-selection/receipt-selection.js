import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';
import * as _ from 'lodash';
import { InsuranceProvider } from '../../providers/insurance/insurance';
import { Utils } from '../../providers/utils/utils';
var ReceiptSelectionComponent = (function () {
    function ReceiptSelectionComponent(events, insuranceProvider) {
        this.events = events;
        this.insuranceProvider = insuranceProvider;
        this.defaultIcons = ["mapfre-check", "mapfre-no-check"];
    }
    ReceiptSelectionComponent.getSelectionEventName = function (eventTarget) {
        return Utils.getPrefixedString(ReceiptSelectionComponent.componentName + ":selection", eventTarget);
    };
    ReceiptSelectionComponent.prototype.ngOnInit = function () {
        this.queryData();
        this.updateCompleteness();
    };
    ReceiptSelectionComponent.prototype.selectReceipt = function (receipt) {
        this.selectedReceipt = receipt;
        this.publishSelection();
        this.updateCompleteness();
    };
    ReceiptSelectionComponent.prototype.publishSelection = function () {
        this.events.publish(ReceiptSelectionComponent.getSelectionEventName(this.eventTarget), this.selectedReceipt);
    };
    ReceiptSelectionComponent.prototype.updateCompleteness = function () {
        this.events.publish(ReceiptSelectionComponent.completedEvent, !_.isNil(this.selectedReceipt));
    };
    ReceiptSelectionComponent.prototype.queryData = function () {
        var _this = this;
        this.receipts = [];
        this.insuranceProvider.getInsuranceSettings(this.mocked, this.insuranceProvider.getClientId()).subscribe(function (response) {
            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
            if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                var insurances = mapfreResponse.data;
                if (insurances && insurances.length) {
                    insurances.forEach(function (insurance) {
                        insurance.associated_policies.forEach(function (policy) {
                            _this.insuranceProvider.getPolicyReceipts(_this.mocked, policy.id, 'UNPAID').subscribe(function (response) {
                                mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                                if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                                    var receipts = mapfreResponse.data;
                                    if (receipts && receipts.length) {
                                        receipts.forEach(function (receipt) {
                                            _this.receipts.push({
                                                id: receipt.id,
                                                risk_id: insurance.id,
                                                risk_name: insurance.name,
                                                policy_type: policy.type,
                                                price: receipt.price,
                                                modality: receipt.modality,
                                                number: receipt.number
                                            });
                                        });
                                    }
                                }
                            });
                        });
                    });
                }
            }
        });
    };
    ReceiptSelectionComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    ReceiptSelectionComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    ReceiptSelectionComponent.componentName = "ReceiptSelectionComponent";
    ReceiptSelectionComponent.completedEvent = ReceiptSelectionComponent.componentName + ":completed";
    ReceiptSelectionComponent.inputs = ['eventTarget', 'selectedReceipt'];
    ReceiptSelectionComponent.configInputs = ['mocked', 'icons', 'styles'];
    ReceiptSelectionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'receipt-selection',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"!receipts?.length\" [ngStyle]=\"styles\">     <loading-spinner></loading-spinner>   </ion-card>      <div *ngIf=\"receipts?.length\">     <ion-card *ngFor=\"let receipt of receipts\" class=\"no-vertical-padding clickable\" (tap)=\"selectReceipt(receipt)\">       <ion-card-content class=\"vertical-padding-xs\">         <ion-item tabindex=\"0\" no-padding>           <ion-icon             hideWhen=\"mobile\" [name]=\"selectedReceipt && selectedReceipt.id === receipt.id ? getIcon(0) : getIcon(1)\" item-start extra-large>           </ion-icon>              <ion-icon             showWhen=\"mobile\" [name]=\"selectedReceipt && selectedReceipt.id === receipt.id ? getIcon(0) : getIcon(1)\" item-start>           </ion-icon>              <h3 class=\"primary-text\">             {{ receipt.price.amount | currency:receipt.price.iso_code:true:'1.2-2'}} - {{ receipt.modality }}           </h3>              <p class=\"secondary-text font-m\">             {{ receipt.risk_name }} - {{ receipt.policy_type }}.           </p>         </ion-item>       </ion-card-content>     </ion-card>   </div>      "
                },] },
    ];
    ReceiptSelectionComponent.ctorParameters = function () { return [
        { type: Events, },
        { type: InsuranceProvider, },
    ]; };
    ReceiptSelectionComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'eventTarget': [{ type: Input },],
        'selectedReceipt': [{ type: Input },],
    };
    return ReceiptSelectionComponent;
}());
export { ReceiptSelectionComponent };
//# sourceMappingURL=receipt-selection.js.map