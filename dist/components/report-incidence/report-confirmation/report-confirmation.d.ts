import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
export declare class ReportConfirmationComponent {
    private events;
    private documentsProvider;
    private translationProvider;
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    private emitEmail;
    private file;
    private componentReady;
    constructor(events: Events, documentsProvider: DocumentsProvider, translationProvider: TranslationProvider);
    download(): void;
    sendEmail(): void;
    goToGlobalPosition(): void;
}
