import { Events } from 'ionic-angular/util/events';
import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class AssistanceMotionSelectionComponent implements OnInit {
    private events;
    private translationProvider;
    static componentName: string;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    componentReady: boolean;
    assistanceSelected: boolean;
    selectedInMovement: any;
    breakdownAssistanceSelected: boolean;
    defaultIcons: string[];
    static selectedMotionEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(events: Events, translationProvider: TranslationProvider);
    ngOnInit(): void;
    ionViewDidEnter(): void;
    setInMovement(inMovement: boolean): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
