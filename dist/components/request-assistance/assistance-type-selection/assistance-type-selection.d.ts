import { Events } from 'ionic-angular/util/events';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class AssistanceTypeSelectionComponent {
    private translationProvider;
    private events;
    static componentName: string;
    componentReady: boolean;
    selectedFaultType: any;
    policyId: string;
    faultTypes: any[];
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    breakdownAssistanceSelected: boolean;
    defaultIcons: string[];
    static damageTypeSelected: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    ionViewDidEnter(): void;
    setDamageType(damageType: any): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
