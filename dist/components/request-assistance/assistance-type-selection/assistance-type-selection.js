import { ViewEncapsulation } from '@angular/core';
import { Events } from 'ionic-angular/util/events';
import { Component, Input } from '@angular/core';
import { Mocks } from '../../../providers/mocks/mocks';
import { TranslationProvider } from '../../../providers/translation/translation';
var AssistanceTypeSelectionComponent = (function () {
    function AssistanceTypeSelectionComponent(translationProvider, events) {
        this.translationProvider = translationProvider;
        this.events = events;
        this.defaultIcons = ["mapfre-warning", "mapfre-check", "mapfre-no-check", "mapfre-info-full"];
        this.translationProvider.bind(this);
    }
    AssistanceTypeSelectionComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.faultTypes = Mocks.getFaultTypes();
            this.breakdownAssistanceSelected = true;
        }
        this.componentReady = true;
    };
    AssistanceTypeSelectionComponent.prototype.ionViewDidEnter = function () {
        this.events.publish(AssistanceTypeSelectionComponent.completedEvent, !!this.selectedFaultType);
    };
    AssistanceTypeSelectionComponent.prototype.setDamageType = function (damageType) {
        this.events.publish(AssistanceTypeSelectionComponent.damageTypeSelected, damageType);
        this.selectedFaultType = damageType;
        this.events.publish(AssistanceTypeSelectionComponent.completedEvent, true);
    };
    AssistanceTypeSelectionComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    AssistanceTypeSelectionComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    AssistanceTypeSelectionComponent.componentName = "AssistanceTypeSelectionComponent";
    AssistanceTypeSelectionComponent.damageTypeSelected = AssistanceTypeSelectionComponent.componentName + ":damage-type-selected";
    AssistanceTypeSelectionComponent.completedEvent = AssistanceTypeSelectionComponent.componentName + ":completed";
    AssistanceTypeSelectionComponent.inputs = ['selectedFaultType', 'policyId', 'faultTypes', 'breakdownAssistanceSelected'];
    AssistanceTypeSelectionComponent.configInputs = ['mocked', 'icons', 'styles'];
    AssistanceTypeSelectionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'assistance-type-selection',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card *ngIf=\"breakdownAssistanceSelected\" [ngStyle]=\"styles\">   <loading-spinner *ngIf=\"!componentReady\"></loading-spinner>   <div *ngIf=\"componentReady\">     <ion-item class=\"alert warning\" *ngIf=\"displayValidation\">       <ion-icon [name]=\"getIcon(0)\" small item-start></ion-icon>       <div>         {{ translate('assistance-type-selection.request_assistance_page.request_type_circunstancies.please') }}       </div>     </ion-item>      <ion-card-header>       <ion-item>         <h3>           {{ translate('assistance-type-selection.request_assistance_page.request_type_circunstancies.type') }}         </h3>       </ion-item>     </ion-card-header>      <ion-card-content>       <ion-item         *ngFor=\"let faultType of faultTypes\"         tabindex=\"0\" (keyup.enter)=\"setDamageType(faultType)\"         (click)=\"setDamageType(faultType)\"         class=\"clickable\" no-padding>          <ion-icon           [name]=\"(selectedFaultType && selectedFaultType.id === faultType.id) ? getIcon(1) : getIcon(2)\" item-start>         </ion-icon>          <h4 class=\"tooltip-wrapper\">           {{ faultType.title }}           <ion-icon             *ngIf=\"faultType.tooltip\"             [name]=\"(selectedFaultType && selectedFaultType.id === faultType.id) ? getIcon(3) : getIcon(4)\"             medium-small class=\"margin-left-xs\">           </ion-icon>         </h4>       </ion-item>     </ion-card-content>   </div>  </ion-card>"
                },] },
    ];
    AssistanceTypeSelectionComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: Events, },
    ]; };
    AssistanceTypeSelectionComponent.propDecorators = {
        'selectedFaultType': [{ type: Input },],
        'policyId': [{ type: Input },],
        'faultTypes': [{ type: Input },],
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'breakdownAssistanceSelected': [{ type: Input },],
    };
    return AssistanceTypeSelectionComponent;
}());
export { AssistanceTypeSelectionComponent };
//# sourceMappingURL=assistance-type-selection.js.map