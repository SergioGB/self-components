import { ApiProvider } from '../../providers/api/api';
export declare class RichTextComponent {
    private apiProvider;
    mocked?: boolean;
    content?: any;
    styles?: string;
    static component_name: string;
    static configInputs: string[];
    constructor(apiProvider: ApiProvider);
    getContent(): string;
}
