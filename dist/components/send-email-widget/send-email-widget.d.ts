import { ElementRef } from '@angular/core';
import { ViewController, NavParams, ModalController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ApiProvider } from '../../providers/api/api';
import { ModalProvider } from '../../providers/modal/modal';
import { TranslationProvider } from '../../providers/translation/translation';
import { FormGroup, FormBuilder } from '@angular/forms';
export declare class SendEmailWidgetComponent {
    private userProvider;
    private viewCtrl;
    private apiProvider;
    private navParams;
    private formBuilder;
    private translationProvider;
    private elementRef;
    private modalCtrl;
    private modalProvider;
    mocked: boolean;
    styles?: any;
    data: boolean;
    saveForm: FormGroup;
    emailSave: string;
    isMobile: boolean;
    static configInputs: string[];
    constructor(userProvider: UserProvider, viewCtrl: ViewController, apiProvider: ApiProvider, navParams: NavParams, formBuilder: FormBuilder, translationProvider: TranslationProvider, elementRef: ElementRef, modalCtrl: ModalController, modalProvider: ModalProvider);
    isDisabled(): boolean;
    confirmAction(): void;
}
