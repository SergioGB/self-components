import { ViewEncapsulation } from '@angular/core';
import { Component, ElementRef, Input } from '@angular/core';
import { ViewController, NavParams, ModalController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ApiProvider } from '../../providers/api/api';
import { ModalProvider } from '../../providers/modal/modal';
import { TranslationProvider } from '../../providers/translation/translation';
import { FormBuilder, Validators } from '@angular/forms';
import { InformationModalComponent } from '../_modals/information-modal/information-modal';
var SendEmailWidgetComponent = (function () {
    function SendEmailWidgetComponent(userProvider, viewCtrl, apiProvider, navParams, formBuilder, translationProvider, elementRef, modalCtrl, modalProvider) {
        this.userProvider = userProvider;
        this.viewCtrl = viewCtrl;
        this.apiProvider = apiProvider;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.translationProvider = translationProvider;
        this.elementRef = elementRef;
        this.modalCtrl = modalCtrl;
        this.modalProvider = modalProvider;
        this.translationProvider.bind(this);
        this.emailSave = 'fernando123@gmail.com';
        this.saveForm = this.formBuilder.group({
            email: [this.emailSave, [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
        });
    }
    SendEmailWidgetComponent.prototype.isDisabled = function () {
        return this.saveForm.invalid;
    };
    SendEmailWidgetComponent.prototype.confirmAction = function () {
        this.modalCtrl.create(InformationModalComponent, {
            mocked: this.mocked,
            title: this.translationProvider.getValueForKey('modal.send-email-widget.information-modal.documents.adhoc.share.confirmation'),
            subtitle: null,
            icon: 'mapfre-check',
            type: 1,
            confirmContent: null,
            showRating: ' ',
            hideButtons: true,
            rightButton: null
        }).present();
    };
    SendEmailWidgetComponent.configInputs = ['mocked', 'styles'];
    SendEmailWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'send-email-widget',
                    encapsulation: ViewEncapsulation.None,
                    styles: ["send-email-widget .modal-icon {   color: #8db602; }  send-email-widget .modal-form-content {   text-align: left; }   send-email-widget .modal-form-content input {     margin-left: 0 !important; }  send-email-widget .send_button {   width: 201px;   height: 42px; }"],
                    template: "<ion-card no-margin [ngStyle]=\"styles\">   <ion-card-content class=\"no-margin-bottom\">      <div class=\"modal-content\" text-center>       <h3> {{translate('send-email-widget.documents.adhoc.share.title')}} </h3>       <ion-grid no-padding class=\"margin-top-m\">         <form [formGroup]=\"saveForm\">           <ion-row>             <ion-col col-12 col-md-8 push-md-2>               <ion-item class=\"modal-form-content\">                 <ion-input type=\"text\" formControlName=\"email\"></ion-input>               </ion-item>             </ion-col>           </ion-row>         </form>       </ion-grid>     </div>      <hr class=\"no-margin-top margin-bottom-l\">      <ion-grid class=\"generic-buttons no-padding actions-wrapper\">       <ion-row>         <ion-col col-12 text-center>           <button             ion-button class=\"send_button\" text-uppercase             [disabled]=\"isDisabled()\"             (click)=\"confirmAction()\">              {{ translate('send-email-widget.save_incidence_modal.send') }}           </button>         </ion-col>        </ion-row>     </ion-grid>   </ion-card-content>  </ion-card>"
                },] },
    ];
    SendEmailWidgetComponent.ctorParameters = function () { return [
        { type: UserProvider, },
        { type: ViewController, },
        { type: ApiProvider, },
        { type: NavParams, },
        { type: FormBuilder, },
        { type: TranslationProvider, },
        { type: ElementRef, },
        { type: ModalController, },
        { type: ModalProvider, },
    ]; };
    SendEmailWidgetComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return SendEmailWidgetComponent;
}());
export { SendEmailWidgetComponent };
//# sourceMappingURL=send-email-widget.js.map