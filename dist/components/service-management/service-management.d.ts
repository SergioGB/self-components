import { TranslationProvider } from '../../providers/translation/translation';
export declare class ServiceManagementComponent {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    service: any;
    componentReady: boolean;
    defaultIcons: string[];
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
    doCall(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
