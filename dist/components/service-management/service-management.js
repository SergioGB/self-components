import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { Mocks } from '../../providers/mocks/mocks';
import { TranslationProvider } from '../../providers/translation/translation';
var ServiceManagementComponent = (function () {
    function ServiceManagementComponent(translationProvider) {
        this.translationProvider = translationProvider;
        this.defaultIcons = ["car"];
        this.translationProvider.bind(this);
    }
    ServiceManagementComponent.prototype.ngOnInit = function () {
        if (this.mocked) {
            this.service = Mocks.getWorkshopCoverages()[0];
        }
        this.componentReady = true;
    };
    ServiceManagementComponent.prototype.doCall = function () {
    };
    ServiceManagementComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    ServiceManagementComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    ServiceManagementComponent.inputs = ['service'];
    ServiceManagementComponent.configInputs = ['mocked', 'icons', 'styles'];
    ServiceManagementComponent.decorators = [
        { type: Component, args: [{
                    selector: 'service-management',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<ion-card no-margin *ngIf=\"service\" [ngStyle]=\"styles\">   <ion-card-content>      <div text-center>       <ion-icon [name]=\"getIcon(0)\" extra-large class=\"modal-icon\"></ion-icon>        <h3>         {{ service.name }}       </h3>        <p class=\"fmm-body1 fmm-regular\">         {{ translate('service-management.service_management.subtitle') }}       </p>        <button ion-button text-uppercase class=\"margin-top-s\" (click)=\"doCall()\">         {{ translate('service-management.service_management.callme_button') }}       </button>     </div>    </ion-card-content> </ion-card>"
                },] },
    ];
    ServiceManagementComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
    ]; };
    ServiceManagementComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'service': [{ type: Input },],
    };
    return ServiceManagementComponent;
}());
export { ServiceManagementComponent };
//# sourceMappingURL=service-management.js.map