import { TranslationProvider } from '../../providers/translation/translation';
export declare class ServicesManagementComponent {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    services: any[];
    componentReady: boolean;
    defaultIcons: string[];
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
    doCall(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
