import { ApiProvider } from '../../providers/api/api';
import { TranslationProvider } from '../../providers/translation/translation';
import { InsuranceProvider } from '../../providers/insurance/insurance';
import { UserProvider } from '../../providers/user/user';
import { Events } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
export declare class SinisterNoClientComponent {
    private insuranceProvider;
    private apiProvider;
    private userProvider;
    private translationProvider;
    private componentSettingsProvider;
    private events;
    mocked?: boolean;
    styles?: string;
    icons: string[];
    static componentName: string;
    static configInputs: string[];
    client: string;
    ready: boolean;
    losses: any;
    insuranceType: any;
    constructor(insuranceProvider: InsuranceProvider, apiProvider: ApiProvider, userProvider: UserProvider, translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider, events: Events);
    ngOnInit(): void;
}
