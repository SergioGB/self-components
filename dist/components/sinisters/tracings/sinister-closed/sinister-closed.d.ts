import { OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { FormatProvider } from '../../../../providers/format/format';
import { InsuranceProvider } from '../../../../providers/insurance/insurance';
export declare class SinisterClosedComponent implements OnInit {
    private translationProvider;
    private insuranceProvider;
    private formatProvider;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    tracing: any;
    riskId: any;
    defaultIcons: string[];
    ready: boolean;
    insurance: any;
    static componentName: string;
    static finalizeEvent: string;
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, insuranceProvider: InsuranceProvider, formatProvider: FormatProvider, events: Events);
    ngOnInit(): void;
    formatDate(date: Date): string;
    finalize(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
