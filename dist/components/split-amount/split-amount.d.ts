import { Events } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { TranslationProvider } from '../../providers/translation/translation';
import { FormatProvider } from '../../providers/format/format';
export declare class SplitAmountComponent {
    private componentSettingsProvider;
    private translationProvider;
    private formatProvider;
    private events;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    receiptId: any;
    defaultIcons: string[];
    paymentDivisions: any[];
    formatDate: Function;
    paymentDivisionSelected: any;
    static componentName: string;
    static loadEvent: string;
    static splitEvent: string;
    static inputs: string[];
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, formatProvider: FormatProvider, events: Events);
    ngOnInit(): void;
    private loadAvailableDivisions;
    changeCheck(paymentDivision: any): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
