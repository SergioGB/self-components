import { OnInit } from '@angular/core';
import { Modal, ModalController, NavController, Events } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { NavigationProvider } from '../../providers/navigation/navigation';
import { Link } from '../../models/link';
export declare class SubheaderComponent implements OnInit {
    private navCtrl;
    private modalController;
    private componentSettingsProvider;
    private navigationProvider;
    private events;
    mocked?: boolean;
    styles?: any;
    hidden?: boolean;
    modals?: any[];
    ready: boolean;
    loggedIn: boolean;
    links: Link[];
    modal: Modal;
    static component_name: string;
    static configInputs: string[];
    constructor(navCtrl: NavController, modalController: ModalController, componentSettingsProvider: ComponentSettingsProvider, navigationProvider: NavigationProvider, events: Events);
    ngOnInit(): void;
    checkSubmitIncidence(entry: any): boolean;
    goTo(entry: any): void;
}
