import { Events } from 'ionic-angular/util/events';
import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class HomeIncidenceDamageDetailComponent implements OnInit {
    private translationProvider;
    private events;
    static componentName: string;
    damageDetail?: any;
    mocked?: boolean;
    styles?: any;
    componentReady: boolean;
    static updatedEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    publishUpdate(): void;
    private updateCompleteness;
}
