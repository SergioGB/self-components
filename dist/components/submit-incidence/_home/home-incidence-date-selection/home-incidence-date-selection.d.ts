import { OnDestroy, OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class HomeIncidenceDateSelectionComponent implements OnInit, OnDestroy {
    private translationProvider;
    private events;
    static componentName: string;
    selectedDate?: any;
    selectedMode?: string;
    exactDate?: Date;
    startDate?: Date;
    endDate?: Date;
    mocked?: boolean;
    styles?: any;
    componentReady: boolean;
    static selectionEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    ngOnDestroy(): void;
    private subscribeEvents;
    private unsubscribeEvents;
}
