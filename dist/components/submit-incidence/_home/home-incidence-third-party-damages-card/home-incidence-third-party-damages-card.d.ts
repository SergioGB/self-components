import { Events } from 'ionic-angular';
import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
export declare class HomeIncidenceThirdPartyDamagesCardComponent implements OnInit {
    private translationProvider;
    private events;
    static componentName: string;
    selectedThirdPartyDamages?: any;
    options: any[];
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    componentReady: boolean;
    defaultIcons: string[];
    static selectedEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    select(option: any): void;
    isSelected(option: any): boolean;
    private loadMocks;
    private publishSelection;
    private updateCompleteness;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
