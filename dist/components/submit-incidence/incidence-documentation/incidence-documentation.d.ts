import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
export declare class IncidenceDocumentationComponent implements OnInit {
    private translationProvider;
    private documentsProvider;
    mocked?: boolean;
    styles?: any;
    documents: any[];
    componentReady: boolean;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, documentsProvider: DocumentsProvider);
    ngOnInit(): void;
    download(file: any): void;
}
