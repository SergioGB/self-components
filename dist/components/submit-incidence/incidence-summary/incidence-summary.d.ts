import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { FormatProvider } from '../../../providers/format/format';
import { SubmitIncidenceProvider } from '../../../providers/submit-incidence/submit-incidence';
import { SummaryData } from '../../../models/summary';
export declare class IncidenceSummaryComponent implements OnInit {
    private submitIncidenceProvider;
    private translationProvider;
    private formatProvider;
    mocked?: boolean;
    styles?: any;
    private insurance;
    private sinister;
    private professionalData;
    private info;
    private damages;
    private damagesForm;
    summaryData: SummaryData;
    private withOtherVehiclesInvolved;
    static configInputs: string[];
    constructor(submitIncidenceProvider: SubmitIncidenceProvider, translationProvider: TranslationProvider, formatProvider: FormatProvider);
    ngOnInit(): void;
    getDamageValue(damage: any): boolean;
    hasDamageValue(damage: any): boolean;
    private getSummary;
    private getDamages;
    private getDamagedSubareas;
}
