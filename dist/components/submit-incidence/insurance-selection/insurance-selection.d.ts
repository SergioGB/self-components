import { OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InsuranceProvider } from '../../../providers/insurance/insurance';
export declare class InsuranceSelectionComponent implements OnInit, OnChanges {
    private events;
    private translationProvider;
    private insuranceProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    insurance: any;
    eventTarget?: string;
    defaultIcons: string[];
    selectedInsurance: any;
    insurances: any[];
    static componentName: string;
    static completedEvent: string;
    static selectionEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    static getSelectionEventName(eventTarget?: string): string;
    constructor(events: Events, translationProvider: TranslationProvider, insuranceProvider: InsuranceProvider);
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    selectInsurance(insurance: any, policySelected: number): void;
    isSelected(insurance: any, policy: any): boolean;
    private publishSelection;
    private updateCompleteness;
    private generateInsuranceData;
    private queryData;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
