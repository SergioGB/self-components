import { TranslationProvider } from '../../../providers/translation/translation';
export declare class NextStepsComponent {
    private translationProvider;
    detailHidden: boolean;
    collapsed: any;
    fullHeights: any;
    collapsedHeights: any;
    componentReady: boolean;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    toggleDetail(): void;
    slideToggle(toggledSelector: string, togglerSelector: string): void;
    storeHeights(toggledSelector: string, togglerSelector: string): void;
    getProperHeight(selector: string): string;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
