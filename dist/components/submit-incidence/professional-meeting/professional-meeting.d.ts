import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class ProfessionalMeetingComponent implements OnInit {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    professional: any;
    defaultIcons: string[];
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
