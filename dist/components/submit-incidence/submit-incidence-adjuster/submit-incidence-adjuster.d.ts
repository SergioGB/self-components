import { OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Incidence } from '../../../models/incidence/incidence';
export declare class SubmitIncidenceAdjusterComponent implements OnInit {
    private componentSettingsProvider;
    private translationProvider;
    private events;
    static componentName: string;
    sinister: Incidence;
    icons?: string[];
    mocked?: boolean;
    styles?: any;
    componentReady: boolean;
    defaultIcons: string[];
    static adjusterSetEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    setAdjuster(adjuster: boolean): void;
    private publishSelection;
    private updateCompleteness;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
