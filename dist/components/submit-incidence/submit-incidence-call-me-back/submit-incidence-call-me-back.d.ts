import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class SubmitIncidenceCallMeBackComponent {
    private events;
    private translationProvider;
    componentReady: boolean;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    constructor(events: Events, translationProvider: TranslationProvider);
    goToGlobalPosition(): any;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
