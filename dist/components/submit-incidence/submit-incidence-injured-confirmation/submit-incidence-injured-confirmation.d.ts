import { OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
export declare class SubmitIncidenceInjuredConfirmationComponent implements OnInit {
    private events;
    private componentSettingsProvider;
    private translationProvider;
    private formBuilder;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    private componentReady;
    private injuredConfirmationForm;
    private selectedTime;
    private dayTimeSlots;
    private popOverOptions;
    private userData;
    static configInputs: string[];
    constructor(events: Events, componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, formBuilder: FormBuilder);
    ngOnInit(): void;
    onTimeSelection(selectedTime: string): void;
    goToGlobalPosition(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
