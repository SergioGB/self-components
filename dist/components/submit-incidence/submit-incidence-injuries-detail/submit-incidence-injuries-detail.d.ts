import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Incidence } from '../../../models/incidence/incidence';
import { Events } from 'ionic-angular/util/events';
export declare class SubmitIncidenceInjuriesDetailComponent {
    private componentSettingsProvider;
    private translationProvider;
    private formBuilder;
    private events;
    static componentName: string;
    mocked?: boolean;
    styles?: any;
    sinister: Incidence;
    componentReady: boolean;
    injuriesForm: FormGroup;
    showComponent?: boolean;
    static changedEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, formBuilder: FormBuilder, events: Events);
    ngOnInit(): void;
    initializeInjuriesForm(): void;
    changeForm(): void;
    private publishFormData;
    private updateCompleteness;
}
