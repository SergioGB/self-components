import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { Incidence } from '../../../models/incidence/incidence';
export declare class SubmitIncidenceInjuriesComponent {
    private componentSettingsProvider;
    private translationProvider;
    private events;
    static componentName: string;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    sinister: Incidence;
    componentReady: boolean;
    defaultIcons: string[];
    static isInjuredEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    setInjured(injured: boolean): void;
    private publishSelection;
    private updateCompleteness;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
