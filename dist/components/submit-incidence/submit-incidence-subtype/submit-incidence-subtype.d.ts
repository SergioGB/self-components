import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import { TooltipProvider } from '../../../providers/tooltip/tooltip';
import { TooltipData } from '../../../models/tooltipData';
import { Incidence } from '../../../models/incidence/incidence';
import { UserProvider } from '../../../providers/user/user';
export declare class SubmitIncidenceSubtypeComponent {
    private componentSettingsProvider;
    private translationProvider;
    private tooltipProvider;
    private userProvider;
    private events;
    static componentName: string;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    sinister: Incidence;
    defaultIcons: string[];
    componentReady: boolean;
    isPotential: boolean;
    client: string;
    uninsured: any;
    private multipleVehiclesSubtypeId;
    static selectedEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, tooltipProvider: TooltipProvider, userProvider: UserProvider, events: Events);
    ngOnInit(): void;
    setSubtype(subtype: any): void;
    presentPopoverTooltip(ev: UIEvent, tooltipData: TooltipData): void;
    dismissPopoverTooltip(ev: UIEvent): void;
    showVehiclesNumberInput(subtype: any): boolean;
    setVehicleCount(vehicleCount: number): void;
    private publishSelection;
    private updateCompleteness;
    private isSubtypeSelected;
    private getIconForSubtype;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
