import { OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { TooltipProvider } from '../../../../providers/tooltip/tooltip';
import { DocumentsProvider } from '../../../../providers/documents/documents';
import { TooltipData } from '../../../../models/tooltipData';
import { Incidence } from '../../../../models/incidence/incidence';
import { Events } from 'ionic-angular';
export declare class SubmitIncidenceHowOtherVehiclesComponent implements OnInit {
    private translationProvider;
    private tooltipProvider;
    private documentsProvider;
    private componentSettingsProvider;
    private formBuilder;
    private events;
    static componentName: string;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    claimId: string;
    sinister: Incidence;
    withContraryDamages: boolean;
    reportsList: any;
    defaultIcons: string[];
    otherVehicleForms: FormGroup[];
    uploadedFilesPerForm: any[];
    tooltipData: TooltipData;
    visible: boolean;
    ready: boolean;
    static updatedEvent: string;
    static completedEvent: string;
    static reportUpdatedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, tooltipProvider: TooltipProvider, documentsProvider: DocumentsProvider, componentSettingsProvider: ComponentSettingsProvider, formBuilder: FormBuilder, events: Events);
    ngOnInit(): void;
    private getVisibility;
    private initializeForms;
    private initializeSingleForm;
    addContraryForm(formData?: any): void;
    showOnLastForm(index: number): boolean;
    areFormsValid(): boolean;
    private publishFormData;
    private updateCompleteness;
    presentPopoverTooltip(ev: UIEvent, tooltipData: TooltipData): void;
    dismissPopoverTooltip(ev: UIEvent): void;
    downloadFile(file: any): void;
    fileUploadEvent(fileEvent: any, i: number): void;
    removeFile(file: any, i: number): void;
    tapUpload(i: number): void;
    clickUpload(i: number): void;
    private publishFileUpdate;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
