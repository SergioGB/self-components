import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import * as _ from 'lodash';
import * as jQuery from 'jquery';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../../../providers/component-settings/component-settings';
import { TooltipProvider } from '../../../../providers/tooltip/tooltip';
import { DocumentsProvider } from '../../../../providers/documents/documents';
import { Events } from 'ionic-angular';
import { Mocks } from '../../../../providers/mocks/mocks';
import { Utils } from '../../../../providers/utils/utils';
var SubmitIncidenceHowOtherVehiclesComponent = (function () {
    function SubmitIncidenceHowOtherVehiclesComponent(translationProvider, tooltipProvider, documentsProvider, componentSettingsProvider, formBuilder, events) {
        this.translationProvider = translationProvider;
        this.tooltipProvider = tooltipProvider;
        this.documentsProvider = documentsProvider;
        this.componentSettingsProvider = componentSettingsProvider;
        this.formBuilder = formBuilder;
        this.events = events;
        this.defaultIcons = ["mapfre-info-full"];
        this.translationProvider.bind(this);
        this.tooltipData = {
            title: '¿Problemas para identificar la situación de tu vehículo?  ',
            description: "Consulta en el parte amistoso de tu siniestro las casillas marcadas en el apartado\n        '12.Circunstacias' en relaci\u00F3n a la situaci\u00F3n de tu veh\u00EDculo en el momento del siniestro."
        };
        this.otherVehicleForms = [];
        this.uploadedFilesPerForm = [];
    }
    SubmitIncidenceHowOtherVehiclesComponent.prototype.ngOnInit = function () {
        this.getVisibility();
        this.sinister = Mocks.getIncidenceOthersVehicle();
        this.uploadedFilesPerForm = this.reportsList || [];
        this.updateCompleteness();
        this.ready = true;
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.getVisibility = function () {
        var _this = this;
        this.componentSettingsProvider.getCheckDamagesVisibilitySettings(this.mocked, this.claimId).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.visible = _this.withContraryDamages !== false && mapfreResponse.data.show_others_damages;
                if (_this.visible) {
                    _this.initializeForms();
                }
            }
        });
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.initializeForms = function () {
        var _this = this;
        if (this.sinister && this.sinister.sinister_info
            && this.sinister.sinister_info.how_other_vehicles
            && this.sinister.sinister_info.how_other_vehicles.length > 0) {
            _.forEach(this.sinister.sinister_info.how_other_vehicles, function (form) {
                _this.addContraryForm(form);
            });
        }
        else {
            for (var i = 0; i < this.sinister.vehicleCount; i++) {
                this.addContraryForm();
            }
        }
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.initializeSingleForm = function (formData) {
        var _this = this;
        var otherVehiclesValidators = this.visible ? [Validators.required] : [];
        var otherVehicleForm;
        if (formData !== undefined) {
            otherVehicleForm = this.formBuilder.group({
                driver_name: [formData.driver_name ? formData.driver_name : '', otherVehiclesValidators],
                driver_surname1: [formData.driver_surname1 ? formData.driver_surname1 : '', otherVehiclesValidators],
                driver_surname2: [formData.driver_surname1 ? formData.driver_surname1 : '', otherVehiclesValidators],
                vehicle_status: [formData.vehicle_status ? formData.vehicle_status : ''],
                more_info: [formData.more_info ? formData.more_info : ''],
            });
        }
        else {
            otherVehicleForm = this.formBuilder.group({
                driver_name: ['', otherVehiclesValidators],
                driver_surname1: ['', otherVehiclesValidators],
                driver_surname2: ['', otherVehiclesValidators],
                vehicle_status: [''],
                more_info: [''],
            });
        }
        otherVehicleForm.valueChanges.subscribe(function () {
            _this.publishFormData();
        });
        return otherVehicleForm;
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.addContraryForm = function (formData) {
        this.otherVehicleForms.push(this.initializeSingleForm(formData));
        this.uploadedFilesPerForm.push([]);
        this.publishFormData();
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.showOnLastForm = function (index) {
        return this.otherVehicleForms.length === (index + 1);
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.areFormsValid = function () {
        var valid = true;
        this.otherVehicleForms.forEach(function (form) {
            valid = valid && form.valid;
        });
        return valid;
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.publishFormData = function () {
        var otherVehicles = [];
        this.otherVehicleForms.forEach(function (form) {
            otherVehicles.push(form.value);
        });
        this.events.publish(SubmitIncidenceHowOtherVehiclesComponent.updatedEvent, otherVehicles);
        this.updateCompleteness();
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.updateCompleteness = function () {
        this.events.publish(SubmitIncidenceHowOtherVehiclesComponent.completedEvent, this.areFormsValid());
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.presentPopoverTooltip = function (ev, tooltipData) {
        this.tooltipProvider.presentPopoverTooltip(ev, tooltipData);
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.dismissPopoverTooltip = function (ev) {
        this.tooltipProvider.dismissPopoverTooltip(ev);
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.downloadFile = function (file) {
        this.documentsProvider.downloadFile(file);
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.fileUploadEvent = function (fileEvent, i) {
        var fileInput = fileEvent.target;
        if (!_.isEmpty(fileInput.value)) {
            this.uploadedFilesPerForm[i] = [];
            this.documentsProvider.uploadFiles(fileEvent, this.uploadedFilesPerForm[i], true, true, false);
            this.publishFileUpdate();
            fileInput.value = '';
        }
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.removeFile = function (file, i) {
        var index = this.uploadedFilesPerForm[i].indexOf(file);
        if (index > -1) {
            this.uploadedFilesPerForm[i].splice(index, 1);
            this.publishFileUpdate();
        }
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.tapUpload = function (i) {
        if (!Utils.onIE()) {
            jQuery("#fileUpload" + i).click();
        }
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.clickUpload = function (i) {
        if (Utils.onIE()) {
            jQuery("#fileUpload" + i).click();
        }
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.publishFileUpdate = function () {
        this.events.publish(SubmitIncidenceHowOtherVehiclesComponent.reportUpdatedEvent, this.uploadedFilesPerForm);
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.getIcon = function (position) {
        return this.existIcon(position) ? this.icons[position] : this.defaultIcons[position];
    };
    SubmitIncidenceHowOtherVehiclesComponent.prototype.existIcon = function (position) {
        return this.icons && this.icons[position];
    };
    SubmitIncidenceHowOtherVehiclesComponent.componentName = "SubmitIncidenceHowOtherVehiclesComponent";
    SubmitIncidenceHowOtherVehiclesComponent.updatedEvent = SubmitIncidenceHowOtherVehiclesComponent.componentName + ":updated";
    SubmitIncidenceHowOtherVehiclesComponent.completedEvent = SubmitIncidenceHowOtherVehiclesComponent.componentName + ":completed";
    SubmitIncidenceHowOtherVehiclesComponent.reportUpdatedEvent = SubmitIncidenceHowOtherVehiclesComponent.componentName + ":report-updated";
    SubmitIncidenceHowOtherVehiclesComponent.inputs = ['sinister', 'claimId', 'withContraryDamages', 'reportsList'];
    SubmitIncidenceHowOtherVehiclesComponent.configInputs = ['mocked', 'icons', 'styles'];
    SubmitIncidenceHowOtherVehiclesComponent.decorators = [
        { type: Component, args: [{
                    selector: 'submit-incidence-how-other-vehicles',
                    encapsulation: ViewEncapsulation.None,
                    styles: [""],
                    template: "<div *ngIf=\"ready && visible\" [ngStyle]=\"styles\">   <div *ngFor=\"let otherVehicleForm of otherVehicleForms; let i = index\">     <ion-item class=\"sub-title\">       <h4>         {{ translate('submit-incidence-how-other-vehicles.submit_incidence.how_other_vehicle.title') }} {{ i + 1 }}       </h4>     </ion-item>      <form [formGroup]=\"otherVehicleForm\">       <ion-card-content class=\"padding-top-s\">         <ion-grid class=\"padded-form-group no-padding\">            <ion-row>             <ion-col col-12 col-sm-6>               <ion-item>                 <ion-label floating>                   {{ translate('submit-incidence-how-other-vehicles.submit_incidence.how_other_vehicle.driver_name') + ' *' }}                 </ion-label>                 <ion-input type=\"text\" formControlName=\"driver_name\">                 </ion-input>               </ion-item>             </ion-col>              <ion-col col-12 col-sm-6>               <ion-item>                 <ion-label floating>                   {{ translate('submit-incidence-how-other-vehicles.submit_incidence.how_other_vehicle.driver_surname1') + ' *' }}                 </ion-label>                 <ion-input type=\"text\" formControlName=\"driver_surname1\">                 </ion-input>               </ion-item>             </ion-col>           </ion-row>            <ion-row>             <ion-col col-12 col-sm-6>               <ion-item>                 <ion-label floating>                   {{ translate('submit-incidence-how-other-vehicles.submit_incidence.how_other_vehicle.driver_surname2') + ' *' }}                 </ion-label>                 <ion-input type=\"text\" formControlName=\"driver_surname2\">                 </ion-input>               </ion-item>             </ion-col>              <ion-col col-12 col-sm-6>               <div class=\"item-with-outer-icon\">                  <ion-item>                   <ion-label floating>                     {{ translate('submit-incidence-how-other-vehicles.submit_incidence.how_other_vehicle.vehicle_situtation') }}                   </ion-label>                   <ion-input type=\"text\" formControlName=\"vehicle_status\">                   </ion-input>                 </ion-item>                  <ion-icon [name]=\"getIcon(0)\" medium-small class=\"clickable\"                   (click)=\"presentPopoverTooltip($event, tooltipData)\" tabindex=\"0\"                   (keyup.enter)=\"presentPopoverTooltip($event, tooltipData)\"                   (mouseenter)=\"presentPopoverTooltip($event, tooltipData)\"                   (mouseleave)=\"dismissPopoverTooltip($event)\">                 </ion-icon>                </div>             </ion-col>            </ion-row>            <ion-row>             <ion-col col-12>               <ion-item class=\"text-area margin-bottom-xs margin-top-m\">                  <ion-textarea id=\"textArea\" rows=\"5\" maxLength=\"150\"                   placeholder=\"{{ translate('submit-incidence-how-other-vehicles.submit_incidence.how_other_vehicle.tell_us') }}\"                   formControlName=\"more_info\">                 </ion-textarea>                </ion-item>             </ion-col>           </ion-row>          </ion-grid>       </ion-card-content>         <ion-card-content class=\"no-padding-top\">         <div class=\"bottom-border border-only\"></div>          <ion-grid class=\"margin-top-m no-padding\">            <download-item *ngFor=\"let file of uploadedFilesPerForm[i]\" [file]=\"file\" (onDownload)=\"downloadFile($event)\"             (onRemove)=\"removeFile($event, i)\" [noDownload]=\"true\"></download-item>            <ion-row>             <ion-col col-12 col-sm-4 class=\"no-padding\">               <button class=\"secondary\" ion-button full text-uppercase tabindex=\"0\" (keyup.enter)=\"tapUpload(i)\"                 (tap)=\"tapUpload(i)\" (click)=\"clickUpload(i)\">                 {{ translate('submit-incidence-how-other-vehicles.submit_incidence.when_where_how.how.upload_button') }}               </button>               <input id=\"fileUpload{{i}}\" type=\"file\" (change)=\"fileUploadEvent($event, i)\" class=\"no-display-input\">             </ion-col>             <ion-col col-12 col-sm-4 class=\"no-padding margin-left-auto\" *ngIf=\"showOnLastForm(i)\">               <button class=\"\" ion-button full text-uppercase tabindex=\"0\" (tap)=\"addContraryForm()\">                 {{ translate('submit-incidence-how-other-vehicles.submit_incidence.when_where_how.how.add_contrary_button') }}               </button>             </ion-col>           </ion-row>          </ion-grid>       </ion-card-content>     </form>   </div> </div>"
                },] },
    ];
    SubmitIncidenceHowOtherVehiclesComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: TooltipProvider, },
        { type: DocumentsProvider, },
        { type: ComponentSettingsProvider, },
        { type: FormBuilder, },
        { type: Events, },
    ]; };
    SubmitIncidenceHowOtherVehiclesComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'icons': [{ type: Input },],
        'styles': [{ type: Input },],
        'claimId': [{ type: Input },],
        'sinister': [{ type: Input },],
        'withContraryDamages': [{ type: Input },],
        'reportsList': [{ type: Input },],
    };
    return SubmitIncidenceHowOtherVehiclesComponent;
}());
export { SubmitIncidenceHowOtherVehiclesComponent };
//# sourceMappingURL=submit-incidence-how-other-vehicles.js.map