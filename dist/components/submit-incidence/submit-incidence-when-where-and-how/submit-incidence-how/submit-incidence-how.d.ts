import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../../providers/translation/translation';
import { Incidence } from '../../../../models/incidence/incidence';
export declare class SubmitIncidenceHowComponent implements OnInit {
    private translationProvider;
    mocked?: boolean;
    styles?: any;
    claimId: string;
    sinister: Incidence;
    withContraryDamages: boolean;
    reportLists: any;
    componentReady: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider);
    ngOnInit(): void;
}
