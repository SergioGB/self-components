import { OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class SubmitIncidenceWhenComponent implements OnInit {
    private events;
    private translationProvider;
    static componentName: string;
    mocked?: boolean;
    styles?: any;
    _selectedDate: Date;
    _selectedTime: string;
    ready: boolean;
    datePattern: string;
    datePickerPattern: string;
    static selectedEvent: string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(events: Events, translationProvider: TranslationProvider);
    selectedDate: Date;
    selectedTime: string;
    ngOnInit(): void;
    private initializeSelection;
    private emitSelection;
    private publishSelection;
    private updateCompleteness;
}
