import { OnInit } from '@angular/core';
import { Modal, ModalController, Events } from 'ionic-angular';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { NavigationProvider } from '../../providers/navigation/navigation';
import { Link } from '../../models/link';
export declare class TabLinksComponent implements OnInit {
    private events;
    private modalController;
    private componentSettingsProvider;
    private navigationProvider;
    mocked?: boolean;
    styles?: any;
    modals?: any[];
    ready: boolean;
    loggedIn: boolean;
    links: Link[];
    modal: Modal;
    static configInputs: string[];
    constructor(events: Events, modalController: ModalController, componentSettingsProvider: ComponentSettingsProvider, navigationProvider: NavigationProvider);
    ngOnInit(): void;
    goTo(entry: any): void;
    checkSubmitIncidence(entry: any): boolean;
}
