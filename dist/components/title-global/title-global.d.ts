import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { User } from '../../models/user';
import { TranslationProvider } from '../../providers/translation/translation';
import { ApiProvider } from '../../providers/api/api';
export declare class TitleGlobalComponent {
    private translationProvider;
    private userProvider;
    private navCtrl;
    private apiProvider;
    user: User;
    mocked?: any;
    styles?: any;
    title?: any;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, userProvider: UserProvider, navCtrl: NavController, apiProvider: ApiProvider);
    ngOnInit(): void;
    getTitle(): any;
}
