import { ViewEncapsulation } from '@angular/core';
import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { TranslationProvider } from '../../providers/translation/translation';
import { Mocks } from '../../providers/mocks/mocks';
import { ApiProvider } from '../../providers/api/api';
var TitleGlobalComponent = (function () {
    function TitleGlobalComponent(translationProvider, userProvider, navCtrl, apiProvider) {
        this.translationProvider = translationProvider;
        this.userProvider = userProvider;
        this.navCtrl = navCtrl;
        this.apiProvider = apiProvider;
        this.translationProvider.bind(this);
    }
    TitleGlobalComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.mocked) {
            this.user = Mocks.getUserData();
        }
        else {
            this.userProvider.getUserData().subscribe(function (userData) {
                if (userData) {
                    _this.user = userData;
                }
            });
        }
    };
    TitleGlobalComponent.prototype.getTitle = function () {
        var language = this.apiProvider.getLanguage();
        return this.title[language] ? this.title[language] : "";
    };
    TitleGlobalComponent.configInputs = ['mocked', 'styles', 'title'];
    TitleGlobalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'title-global',
                    encapsulation: ViewEncapsulation.None,
                    styles: [".primary, .fmm-body1.primary, .fmm-body2.primary {   color: #D81E05 !important; }  .secondary, .fmm-body1.secondary, .fmm-body2.secondary {   color: #be0f0f !important; }  .tertiary, .fmm-body1.tertiary, .fmm-body2.tertiary {   color: #820404 !important; }  .primary-text, .fmm-body1.primary-text, .fmm-body2.primary-text {   color: #333333 !important; }  .secondary-text, .fmm-body1.secondary-text, .fmm-body2.secondary-text {   color: #666666 !important; }  .white1, .fmm-body1.white1, .fmm-body2.white1 {   color: #ffffff !important; }  .white2, .fmm-body1.white2, .fmm-body2.white2 {   color: #fff4ec !important; }  .grey1, .fmm-body1.grey1, .fmm-body2.grey1 {   color: #999999 !important; }  .grey2, .fmm-body1.grey2, .fmm-body2.grey2 {   color: #c9c9c9 !important; }  .grey3, .fmm-body1.grey3, .fmm-body2.grey3 {   color: #eae9e9 !important; }  .orange, .fmm-body1.orange, .fmm-body2.orange {   color: #ef9454 !important; }  .green, .fmm-body1.green, .fmm-body2.green {   color: #8db602 !important; }  .blue, .fmm-body1.blue, .fmm-body2.blue {   color: #00b4ff !important; }  .light-blue, .fmm-body1.light-blue, .fmm-body2.light-blue {   color: #effaff !important; }  .red-on-hover:hover, .red-on-hover:hover * {   color: #D81E05 !important; }  .red-on-hover, .red-on-hover * {   transition: .2s; }  title-global {   /* desktop */   /* tablet */   /* mobile */ }   title-global .header {     position: absolute;     height: 300px;     width: 100%;     background-image: url(\"../../assets/images/non-scalable/background/login/imagen_fondo_login.jpg\");     background-position: center; }   title-global .titleComp p {     color: #eae9e9; }   title-global .titleComp .title span {     color: white;     font-size: 28px; }   @media (min-width: 1200px) {     title-global .titleComp {       margin: 40px 200px 40px 60px;       position: relative; }       title-global .titleComp .title {         margin-bottom: 15px; } }   @media (min-width: 768px) and (max-width: 1199px) {     title-global .titleComp {       margin: 50px 100px 30px 24px;       position: relative; }       title-global .titleComp .title {         margin-bottom: 15px; } }   @media (max-width: 767px) {     title-global .titleComp {       padding-left: 18px;       padding-right: 18px;       margin-top: 30px;       position: relative; }       title-global .titleComp .title {         margin-bottom: 15px; } }"],
                    template: "<h2 *ngIf=\"user\" class=\"light upper-header\" [ngStyle]=\"styles\">     {{ getTitle() }}, <strong> {{ user.personal_data.name }} </strong> </h2>"
                },] },
    ];
    TitleGlobalComponent.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: UserProvider, },
        { type: NavController, },
        { type: ApiProvider, },
    ]; };
    TitleGlobalComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
        'title': [{ type: Input },],
    };
    return TitleGlobalComponent;
}());
export { TitleGlobalComponent };
//# sourceMappingURL=title-global.js.map