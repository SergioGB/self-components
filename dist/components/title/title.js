import { Component, Input } from '@angular/core';
var TitleComponent = (function () {
    function TitleComponent() {
    }
    TitleComponent.configInputs = ['mocked', 'styles'];
    TitleComponent.decorators = [
        { type: Component, args: [{
                    selector: 'title-page',
                    template: "<div class=\"header\"></div> <div class=\"titleComp\" [ngStyle]=\"styles\">   <div class=\"title\">     <span>{{ translate('title.title.title') }}</span>   </div>   <p>{{ translate('title.title.subtitle') }}</p> </div>"
                },] },
    ];
    TitleComponent.ctorParameters = function () { return []; };
    TitleComponent.propDecorators = {
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return TitleComponent;
}());
export { TitleComponent };
//# sourceMappingURL=title.js.map