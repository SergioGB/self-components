import { OnInit } from '@angular/core';
import { TooltipProvider } from '../../providers/tooltip/tooltip';
import { TooltipData } from '../../models/tooltipData';
export declare class TooltipIconComponent implements OnInit {
    private tooltipProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    tooltipData: TooltipData;
    defaultIcons: string[];
    ready: boolean;
    static inputs: string[];
    static configInputs: string[];
    constructor(tooltipProvider: TooltipProvider);
    ngOnInit(): void;
    presentPopoverTooltip(ev: UIEvent, tooltipData: TooltipData): void;
    dismissPopoverTooltip(ev: UIEvent): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
