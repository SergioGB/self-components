import { FormGroup } from "@angular/forms";
import { KeyValue } from "../../models/keyValue";
import { FormatProvider } from "../../providers/format/format";
export declare class TypedInputComponent {
    private formatProvider;
    form: FormGroup;
    fieldName: string;
    fieldType?: string;
    options?: KeyValue[];
    value: string;
    newValue: string;
    dateValue: Date;
    editing: boolean;
    isEditable: boolean;
    fieldLabel: string;
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    constructor(formatProvider: FormatProvider);
    ngOnChanges(): void;
    editMode(): boolean;
    setFormatedDate(value: any): string;
}
