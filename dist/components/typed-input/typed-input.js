import { Component, Input } from "@angular/core";
import { FormatProvider } from "../../providers/format/format";
import * as moment from "moment";
var TypedInputComponent = (function () {
    function TypedInputComponent(formatProvider) {
        this.formatProvider = formatProvider;
        this.formatProvider = formatProvider;
        this.fieldType = this.fieldType || 'text';
    }
    TypedInputComponent.prototype.ngOnChanges = function () {
        if (this.fieldType === 'date') {
            this.dateValue = moment(this.value).toDate();
        }
        else {
            this.newValue = this.value;
        }
    };
    TypedInputComponent.prototype.editMode = function () {
        if (this.isEditable === true) {
            return this.editing;
        }
        else {
            return false;
        }
    };
    TypedInputComponent.prototype.setFormatedDate = function (value) {
        return this.formatProvider.formatDate(value);
    };
    TypedInputComponent.configInputs = ['mocked', 'styles'];
    TypedInputComponent.decorators = [
        { type: Component, args: [{
                    selector: "typed-input",
                    template: "<div [formGroup]=\"form\" [ngStyle]=\"styles\">   <span class=\"fmm-body1 input-value secondary-text\" *ngIf=\"!editMode()\">     <span *ngIf=\"fieldType === 'text'\">{{value}}</span>     <span *ngIf=\"fieldType === 'email'\">{{value}}</span>     <span *ngIf=\"fieldType === 'tel'\">{{value}}</span>     <span *ngIf=\"fieldType === 'number'\">{{value}}</span>     <span *ngIf=\"fieldType === 'currency' && value.iso_code !== 'EUR'\">{{value.amount !== '' ? value.amount : '0' | currency:value.iso_code:true:'1.2-2'}}</span>     <span *ngIf=\"fieldType === 'currency' && value.iso_code === 'EUR'\">{{value.amount | number:'1.2-2'}} &euro;</span>     <span *ngIf=\"fieldType === 'date'\">{{setFormatedDate(value)}}</span>     <span *ngIf=\"fieldType === 'select'\">{{value}}</span>   </span>   <ion-item *ngIf=\"editMode()\" no-padding>     <ion-label *ngIf=\"fieldLabel && fieldType !== 'date'\" floating>       {{ fieldLabel }}     </ion-label>     <ion-input *ngIf=\"fieldType === 'text'\" [formControlName]=\"fieldName\" [(ngModel)]=\"newValue\" type=\"text\"></ion-input>     <ion-input *ngIf=\"fieldType === 'tel'\" [formControlName]=\"fieldName\" [(ngModel)]=\"newValue\" type=\"tel\"></ion-input>     <ion-input *ngIf=\"fieldType === 'email'\" [formControlName]=\"fieldName\" [(ngModel)]=\"newValue\" type=\"email\"></ion-input>     <ion-input *ngIf=\"fieldType === 'number'\" [formControlName]=\"fieldName\" [(ngModel)]=\"newValue\" type=\"number\" min=\"0\"></ion-input>     <ion-input *ngIf=\"fieldType === 'currency'\" [formControlName]=\"fieldName\" [(ngModel)]=\"newValue.amount\" type=\"number\" min=\"0\" step=\".01\"></ion-input>     <mapfre-datetime *ngIf=\"fieldType === 'date'\" [formControlName]=\"fieldName\" [placeHolder]=\"fieldLabel\" [ngModel]=\"dateValue\" item-content [(mModel)]=\"dateValue\" [calendar]=\"true\" [displayFormat]=\"'DD/MM/YYYY'\" [pickerFormat]=\"'DD MMMM YYYY'\" ngDefaultControl></mapfre-datetime>     <ion-select *ngIf=\"fieldType === 'select'\" [formControlName]=\"fieldName\">       <ion-option *ngFor=\"let option of options\" [value]=\"entry.value\">{{ entry.key }}</ion-option>     </ion-select>   </ion-item> </div>",
                },] },
    ];
    TypedInputComponent.ctorParameters = function () { return [
        { type: FormatProvider, },
    ]; };
    TypedInputComponent.propDecorators = {
        'form': [{ type: Input },],
        'fieldName': [{ type: Input },],
        'fieldType': [{ type: Input },],
        'options': [{ type: Input },],
        'value': [{ type: Input },],
        'editing': [{ type: Input },],
        'isEditable': [{ type: Input },],
        'fieldLabel': [{ type: Input },],
        'mocked': [{ type: Input },],
        'styles': [{ type: Input },],
    };
    return TypedInputComponent;
}());
export { TypedInputComponent };
//# sourceMappingURL=typed-input.js.map