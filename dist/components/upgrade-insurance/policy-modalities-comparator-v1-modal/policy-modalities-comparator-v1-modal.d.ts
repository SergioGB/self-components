import { ElementRef } from '@angular/core';
import { ViewController, Events, NavParams } from 'ionic-angular';
import { ModalProvider } from '../../../providers/modal/modal';
export declare class PolicyModalitiesComparatorV1Modal {
    private viewCtrl;
    private elementRef;
    private navParams;
    private modalProvider;
    private events;
    showCompare: boolean;
    budgetId?: any;
    constructor(viewCtrl: ViewController, elementRef: ElementRef, navParams: NavParams, modalProvider: ModalProvider, events: Events);
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    dismiss(): void;
}
