import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { DocumentsProvider } from '../../../providers/documents/documents';
import { InsuranceUpgradeProvider } from '../../../providers/insurance-upgrade/insurance-upgrade';
import { FormatProvider } from '../../../providers/format/format';
import { InsuranceType } from '../../../models/upgrade-insurance/insurance-type';
export declare class UpgradeInsuranceConfirmationComponent implements OnInit {
    private translationProvider;
    private documentsProvider;
    private insuranceUpgradeProvider;
    private formatProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    formatCost: Function;
    termsAccepted: boolean;
    componentReady: boolean;
    insuranceType: InsuranceType;
    newCoverages: any[];
    conditions: any;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, documentsProvider: DocumentsProvider, insuranceUpgradeProvider: InsuranceUpgradeProvider, formatProvider: FormatProvider);
    ngOnInit(): void;
    acceptTerms(): void;
    downloadConditions(): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
