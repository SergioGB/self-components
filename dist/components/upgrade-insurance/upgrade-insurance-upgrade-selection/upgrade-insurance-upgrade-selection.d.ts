import { OnDestroy, OnInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { TranslationProvider } from '../../../providers/translation/translation';
import { InsuranceType } from '../../../models/upgrade-insurance/insurance-type';
export declare class UpgradeInsuranceUpgradeSelectionComponent implements OnInit, OnDestroy {
    private translationProvider;
    private events;
    static componentName: string;
    mocked?: boolean;
    styles?: any;
    eventTarget?: string;
    selectedInsuranceType: InsuranceType;
    insuranceTypes: InsuranceType[];
    ready: boolean;
    static getSelectionEventName(eventTarget?: string): string;
    static getSubmissionEventName(eventTarget?: string): string;
    static completedEvent: string;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, events: Events);
    ngOnInit(): void;
    ngOnDestroy(): void;
    private mockData;
    private initialize;
    private publishSubmission;
    private updateCompleteness;
}
