import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import { Contact } from '../../models/contact';
export declare class UserContactsComponent implements OnInit {
    private translationProvider;
    private componentSettingsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    contacts: Contact[];
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, componentSettingsProvider: ComponentSettingsProvider);
    ngOnInit(): void;
    call(phoneNumber: string): void;
    sendEmail(email: string): void;
    chat(url: string): void;
    openAddress(url: string): void;
    private getContacts;
    private loadContacts;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
