import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../providers/translation/translation';
import { UserProvider } from '../../providers/user/user';
import { ApiProvider } from '../../providers/api/api';
export declare class UserWelcomeComponent implements OnInit {
    private translationProvider;
    private userProvider;
    private apiProvider;
    mocked?: boolean;
    styles?: any;
    title?: any;
    subtitle?: any;
    userData: any;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, userProvider: UserProvider, apiProvider: ApiProvider);
    ngOnInit(): void;
    getTitle(): any;
    getSubtitle(): any;
}
