import { Link } from "../../models/link";
import { TranslationProvider } from '../../providers/translation/translation';
export declare class UsualInquiriesComponent {
    private translationProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    inquiries: Link[];
    constructor(translationProvider: TranslationProvider);
    getIcon(position: number): string;
    existIcon(position: number): string;
}
