import { OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormatProvider } from '../../../providers/format/format';
import { TranslationProvider } from '../../../providers/translation/translation';
export declare class WarningAdvertisementComponent implements OnInit {
    private navCtrl;
    private translationProvider;
    private formatProvider;
    mocked?: boolean;
    styles?: any;
    static configInputs: string[];
    formatDate: Function;
    formatCost: Function;
    componentReady: boolean;
    constructor(navCtrl: NavController, translationProvider: TranslationProvider, formatProvider: FormatProvider);
    ngOnInit(): void;
}
