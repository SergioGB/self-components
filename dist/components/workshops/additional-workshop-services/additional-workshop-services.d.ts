import { OnInit } from '@angular/core';
import { TranslationProvider } from '../../../providers/translation/translation';
import { AnimationsProvider } from '../../../providers/animations/animations';
export declare class AdditionalWorkshopServicesComponent implements OnInit {
    private translationProvider;
    private animationsProvider;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    selectedWorkshop?: any;
    defaultIcons: string[];
    additionalServiceTypes: any[];
    ready: boolean;
    static inputs: Array<string>;
    static configInputs: string[];
    constructor(translationProvider: TranslationProvider, animationsProvider: AnimationsProvider);
    ngOnInit(): void;
    toggle(selector: string): void;
    isCollapsed(selector: string): boolean;
    private mockData;
    private getMockAdditionalServiceTypes;
    private getMockAdditionalServices;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
