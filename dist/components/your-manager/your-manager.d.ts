import { ComponentSettingsProvider } from "../../providers/component-settings/component-settings";
import { TranslationProvider } from "../../providers/translation/translation";
import { UserProvider } from "../../providers/user/user";
export declare class YourManagerComponent {
    private componentSettingsProvider;
    private translationProvider;
    private userProvider;
    componentReady: boolean;
    componentSettings: Object;
    mocked?: boolean;
    icons?: string[];
    styles?: any;
    defaultIcons: string[];
    static configInputs: string[];
    constructor(componentSettingsProvider: ComponentSettingsProvider, translationProvider: TranslationProvider, userProvider: UserProvider);
    call(phoneNumber: string): void;
    sendEmail(email: string): void;
    chat(url: string): void;
    getIcon(position: number): string;
    existIcon(position: number): string;
}
