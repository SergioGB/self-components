import { ElementRef, OnInit } from '@angular/core';
export declare class AutosizeDirective implements OnInit {
    element: ElementRef;
    onInput(): void;
    constructor(element: ElementRef);
    ngOnInit(): void;
    adjust(): void;
}
