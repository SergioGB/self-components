import { ElementRef, HostListener, Directive } from '@angular/core';
var AutosizeDirective = (function () {
    function AutosizeDirective(element) {
        this.element = element;
    }
    AutosizeDirective.prototype.onInput = function () {
        this.adjust();
    };
    AutosizeDirective.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.adjust(); }, 0);
    };
    AutosizeDirective.prototype.adjust = function () {
        var textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
        var px = "px";
        textArea.style.overflow = 'hidden';
        textArea.style.height = 'auto';
        textArea.style.height = textArea.scrollHeight + px;
    };
    AutosizeDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'ion-textarea[autosize]'
                },] },
    ];
    AutosizeDirective.ctorParameters = function () { return [
        { type: ElementRef, },
    ]; };
    AutosizeDirective.propDecorators = {
        'onInput': [{ type: HostListener, args: ['input', ['$event.target'],] },],
    };
    return AutosizeDirective;
}());
export { AutosizeDirective };
//# sourceMappingURL=autosize.js.map