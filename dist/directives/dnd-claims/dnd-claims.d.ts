import { FileClaimsProvider } from '../../providers/file-claims/file-claims';
export declare class DndClaimsDirective {
    private fileIncidences;
    constructor(fileIncidences: FileClaimsProvider);
    onDragOver(evt: any): void;
    onDragLeave(evt: any): void;
    onDrop(evt: any): void;
}
