import { Directive, HostListener } from '@angular/core';
import { FileClaimsProvider } from '../../providers/file-claims/file-claims';
var DndClaimsDirective = (function () {
    function DndClaimsDirective(fileIncidences) {
        this.fileIncidences = fileIncidences;
    }
    DndClaimsDirective.prototype.onDragOver = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    DndClaimsDirective.prototype.onDragLeave = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    DndClaimsDirective.prototype.onDrop = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var files = evt.dataTransfer.files;
        this.fileIncidences.setFiles(files[0]);
    };
    DndClaimsDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[dnd-claims]'
                },] },
    ];
    DndClaimsDirective.ctorParameters = function () { return [
        { type: FileClaimsProvider, },
    ]; };
    DndClaimsDirective.propDecorators = {
        'onDragOver': [{ type: HostListener, args: ['dragover', ['$event'],] },],
        'onDragLeave': [{ type: HostListener, args: ['dragleave', ['$event'],] },],
        'onDrop': [{ type: HostListener, args: ['drop', ['$event'],] },],
    };
    return DndClaimsDirective;
}());
export { DndClaimsDirective };
//# sourceMappingURL=dnd-claims.js.map