import { FileInsuranceProvider } from '../../providers/file-insurance/file-insurance';
export declare class DndInsuranceDirective {
    private fileInsurance;
    constructor(fileInsurance: FileInsuranceProvider);
    onDragOver(evt: any): void;
    onDragLeave(evt: any): void;
    onDrop(evt: any): void;
}
