import { Directive, HostListener } from '@angular/core';
import { FileInsuranceProvider } from '../../providers/file-insurance/file-insurance';
var DndInsuranceDirective = (function () {
    function DndInsuranceDirective(fileInsurance) {
        this.fileInsurance = fileInsurance;
    }
    DndInsuranceDirective.prototype.onDragOver = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    DndInsuranceDirective.prototype.onDragLeave = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    DndInsuranceDirective.prototype.onDrop = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var files = evt.dataTransfer.files;
        this.fileInsurance.setFiles(files[0]);
    };
    DndInsuranceDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[dnd-insurance]'
                },] },
    ];
    DndInsuranceDirective.ctorParameters = function () { return [
        { type: FileInsuranceProvider, },
    ]; };
    DndInsuranceDirective.propDecorators = {
        'onDragOver': [{ type: HostListener, args: ['dragover', ['$event'],] },],
        'onDragLeave': [{ type: HostListener, args: ['dragleave', ['$event'],] },],
        'onDrop': [{ type: HostListener, args: ['drop', ['$event'],] },],
    };
    return DndInsuranceDirective;
}());
export { DndInsuranceDirective };
//# sourceMappingURL=dnd-insurance.js.map