import { FileIncidencesProvider } from '../../providers/file-incidences/file-incidences';
export declare class DndDirective {
    private fileIncidences;
    constructor(fileIncidences: FileIncidencesProvider);
    onDragOver(evt: any): void;
    onDragLeave(evt: any): void;
    onDrop(evt: any): void;
}
