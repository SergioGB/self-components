import { Directive, HostListener } from '@angular/core';
import { FileIncidencesProvider } from '../../providers/file-incidences/file-incidences';
var DndDirective = (function () {
    function DndDirective(fileIncidences) {
        this.fileIncidences = fileIncidences;
    }
    DndDirective.prototype.onDragOver = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    DndDirective.prototype.onDragLeave = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
    };
    DndDirective.prototype.onDrop = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var files = evt.dataTransfer.files;
        this.fileIncidences.setFiles(files[0]);
    };
    DndDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[dnd]'
                },] },
    ];
    DndDirective.ctorParameters = function () { return [
        { type: FileIncidencesProvider, },
    ]; };
    DndDirective.propDecorators = {
        'onDragOver': [{ type: HostListener, args: ['dragover', ['$event'],] },],
        'onDragLeave': [{ type: HostListener, args: ['dragleave', ['$event'],] },],
        'onDrop': [{ type: HostListener, args: ['drop', ['$event'],] },],
    };
    return DndDirective;
}());
export { DndDirective };
//# sourceMappingURL=dnd.js.map