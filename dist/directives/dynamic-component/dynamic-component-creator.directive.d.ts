import { OnInit, ViewContainerRef, EventEmitter } from '@angular/core';
import { ComponentCreatorService } from '../../providers/component-creator/component-creator.service';
export declare class DynamicCreatorDirective implements OnInit {
    private _viewContainerRef;
    private _componentCreatorService;
    component: any;
    inputs: any;
    componentCreated: EventEmitter<any>;
    constructor(_viewContainerRef: ViewContainerRef, _componentCreatorService: ComponentCreatorService);
    ngOnInit(): void;
}
