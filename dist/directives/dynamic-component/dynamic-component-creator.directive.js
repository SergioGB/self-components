import { Directive, Input, ViewContainerRef, EventEmitter, Output } from '@angular/core';
import { ComponentCreatorService } from '../../providers/component-creator/component-creator.service';
var DynamicCreatorDirective = (function () {
    function DynamicCreatorDirective(_viewContainerRef, _componentCreatorService) {
        this._viewContainerRef = _viewContainerRef;
        this._componentCreatorService = _componentCreatorService;
        this.componentCreated = new EventEmitter();
    }
    DynamicCreatorDirective.prototype.ngOnInit = function () {
        var instance = this._componentCreatorService.createComponent(this._viewContainerRef, this.component, this.inputs);
        this.componentCreated.emit({ componentName: this.component.componentName, instance: instance });
    };
    DynamicCreatorDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[dynamicCreator]'
                },] },
    ];
    DynamicCreatorDirective.ctorParameters = function () { return [
        { type: ViewContainerRef, },
        { type: ComponentCreatorService, },
    ]; };
    DynamicCreatorDirective.propDecorators = {
        'component': [{ type: Input },],
        'inputs': [{ type: Input },],
        'componentCreated': [{ type: Output },],
    };
    return DynamicCreatorDirective;
}());
export { DynamicCreatorDirective };
//# sourceMappingURL=dynamic-component-creator.directive.js.map