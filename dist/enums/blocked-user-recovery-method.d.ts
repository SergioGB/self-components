export declare enum BlockedUserRecoveryMethod {
    none = 0,
    mail = 1,
    sms = 2,
    securityQuestions = 3
}
