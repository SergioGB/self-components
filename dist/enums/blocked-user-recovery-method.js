export var BlockedUserRecoveryMethod;
(function (BlockedUserRecoveryMethod) {
    BlockedUserRecoveryMethod[BlockedUserRecoveryMethod["none"] = 0] = "none";
    BlockedUserRecoveryMethod[BlockedUserRecoveryMethod["mail"] = 1] = "mail";
    BlockedUserRecoveryMethod[BlockedUserRecoveryMethod["sms"] = 2] = "sms";
    BlockedUserRecoveryMethod[BlockedUserRecoveryMethod["securityQuestions"] = 3] = "securityQuestions";
})(BlockedUserRecoveryMethod || (BlockedUserRecoveryMethod = {}));
//# sourceMappingURL=blocked-user-recovery-method.js.map