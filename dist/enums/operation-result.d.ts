export declare enum OperationResult {
    failure = 1,
    success = 2,
    partialSuccess = 3
}
