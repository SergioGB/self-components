export var OperationResult;
(function (OperationResult) {
    OperationResult[OperationResult["failure"] = 1] = "failure";
    OperationResult[OperationResult["success"] = 2] = "success";
    OperationResult[OperationResult["partialSuccess"] = 3] = "partialSuccess";
})(OperationResult || (OperationResult = {}));
//# sourceMappingURL=operation-result.js.map