export declare class Address {
    road_type_id?: any;
    road_type_name?: string;
    road_name?: string;
    road_number?: string;
    direction?: string;
    cp?: string;
    postal_code?: string;
    zip_code?: string;
    city_id?: any;
    city_name?: string;
    province_id?: any;
    province_name?: string;
    county_id?: any;
    county_name?: string;
    country_id?: any;
    country_name?: string;
}
