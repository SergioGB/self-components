export declare class AssistanceRequest {
    selectedAssistanceType?: any;
    selectedVehicle?: any;
    selectedPolicy?: any;
    selectedFaultType?: any;
    selectedInMovement?: any;
    selectedLocation?: any;
}
