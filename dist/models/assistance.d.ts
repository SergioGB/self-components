import { Icon } from "./icon";
export declare class Assistance {
    assistance_id?: number;
    assistance_icon: Icon;
    assistance_type?: string;
    assistance_description?: string;
    assistance_number?: number;
    assistance_date?: Date;
    assistance_location?: string;
    assistance_estimated_time?: string;
    assistance_state?: string;
    cancel_enabled?: boolean;
}
