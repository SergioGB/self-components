export declare class AssociatedReceipt {
    receipt_id?: string;
    policy_id?: string;
    constructor(receipt_id: string, policy_id: string);
}
