var AssociatedReceipt = (function () {
    function AssociatedReceipt(receipt_id, policy_id) {
        this.receipt_id = receipt_id;
        this.policy_id = policy_id;
    }
    return AssociatedReceipt;
}());
export { AssociatedReceipt };
//# sourceMappingURL=associatedReceipt.js.map