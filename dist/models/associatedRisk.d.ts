export declare class AssociatedRisk {
    risk_id?: string;
    policy_id?: string;
    risk_description?: string;
    policy_description?: string;
    constructor(risk_id: string, policy_id: string);
}
