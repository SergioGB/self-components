var AssociatedRisk = (function () {
    function AssociatedRisk(risk_id, policy_id) {
        this.risk_id = risk_id;
        this.policy_id = policy_id;
    }
    return AssociatedRisk;
}());
export { AssociatedRisk };
//# sourceMappingURL=associatedRisk.js.map