import { Cost } from "./cost";
import { CarBudgetDetail } from "./car-budget-detail";
import { HomeBudgetDetail } from "./home-budget-detail";
export declare class Budget {
    id?: string;
    associated_risk_type?: string;
    risk_name?: string;
    policy_modality?: string;
    number?: string;
    state?: string;
    opening_date?: Date;
    due_date?: Date;
    price?: Cost;
    car_budget_detail?: CarBudgetDetail;
    home_budget_detail?: HomeBudgetDetail;
    actions?: any;
    detail_link?: string;
}
