import { TooltipData } from "./tooltipData";
export declare class BusinessParameter {
    id?: string;
    title?: string;
    description?: string;
    tooltip?: TooltipData[];
    subparameters?: string[];
}
