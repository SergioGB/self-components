import { BusinessParameter } from "./business_parameter";
import { NavigationInfo } from "./navigation_info";
export declare class CancellationReason {
    business_parameter?: BusinessParameter;
    navigation_info?: NavigationInfo;
}
