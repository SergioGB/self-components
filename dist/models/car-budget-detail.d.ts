import { VehicleInfo } from "./vehicle-info";
import { TakerInfo } from "./taker-info";
export declare class CarBudgetDetail {
    vehicle_info: VehicleInfo;
    taker_info: TakerInfo;
}
