export declare class ClaimCarDetail {
    associated_risk?: any;
    claim_description?: any;
    claim_info?: any;
    damages?: any;
    professional?: string;
    associated_docs?: any;
    uninsured_vehicle_info: any;
}
