export declare class ClaimProceeding {
    proceeding_number: string;
    proceeding_detail: string;
    proceeding_documents?: any;
    proceeding_images?: any;
}
