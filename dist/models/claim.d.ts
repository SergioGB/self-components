import { ClaimCarDetail } from "./claim-car-detail";
import { ClaimHomeDetail } from "./claim-home-detail";
export declare class Claim {
    car_claim_request?: ClaimCarDetail;
    home_claim_request?: ClaimHomeDetail;
}
