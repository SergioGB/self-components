export declare class ClaimDescription {
    type?: string;
    adjuster?: boolean;
    involved?: string;
    injured?: boolean;
    injured_detail?: any;
    constructor(type: string, adjuster: boolean, involved: string, injured: boolean, injured_detail: any);
}
