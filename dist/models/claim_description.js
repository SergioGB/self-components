var ClaimDescription = (function () {
    function ClaimDescription(type, adjuster, involved, injured, injured_detail) {
        this.type = type;
        this.adjuster = adjuster;
        this.involved = involved;
        this.injured = injured;
        this.injured_detail = injured_detail;
    }
    return ClaimDescription;
}());
export { ClaimDescription };
//# sourceMappingURL=claim_description.js.map