export declare class ClaimInfo {
    when?: string;
    where?: any;
    how_own_vehicle?: any;
    how_other_vehicles?: any;
    constructor(when: string, where: any, how_own_vehicle: any, how_other_vehicles: any);
}
