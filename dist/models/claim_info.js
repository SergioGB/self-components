var ClaimInfo = (function () {
    function ClaimInfo(when, where, how_own_vehicle, how_other_vehicles) {
        this.when = when;
        this.where = where;
        this.how_own_vehicle = how_own_vehicle;
        this.how_other_vehicles = how_other_vehicles;
    }
    return ClaimInfo;
}());
export { ClaimInfo };
//# sourceMappingURL=claim_info.js.map