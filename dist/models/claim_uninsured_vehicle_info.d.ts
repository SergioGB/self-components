export declare class ClaimUninsuredVehicleInfo {
    personal_data?: ClaimUninsuredVehicleInfoPersonalData;
    vehicle_info?: ClaimUninsuredVehicleInfoVehicleInfo;
    constructor(personal_data: ClaimUninsuredVehicleInfoPersonalData, vehicle_info: ClaimUninsuredVehicleInfoVehicleInfo);
}
export declare class ClaimUninsuredVehicleInfoPersonalData {
    name?: string;
    surname1?: string;
    surname2?: string;
    identification_document_id?: string;
    identification_document_number?: string;
    phone?: string;
    address?: ClaimUninsuredVehicleInfoPersonalDataAddress;
    constructor(name: string, surname1: string, surname2: string, identification_document_id: string, identification_document_number: string, phone: string, address: ClaimUninsuredVehicleInfoPersonalDataAddress);
}
export declare class ClaimUninsuredVehicleInfoPersonalDataAddress {
    road_type_id?: string;
    road_name?: string;
    road_number?: string;
    zip_code?: string;
    city_id?: string;
    province_id?: string;
    country_id?: string;
    constructor(road_type_id: string, road_name: string, road_number: string, zip_code: string, city_id: string, province_id: string, country_id: string);
}
export declare class ClaimUninsuredVehicleInfoVehicleInfo {
    registration_date?: string;
    vehicle_brand_id?: string;
    vehicle_model_id?: string;
    doors_count?: string;
    vehicle_color_id?: string;
    registration?: string;
    constructor(registration_date: string, vehicle_brand_id: string, vehicle_model_id: string, doors_count: string, vehicle_color_id: string, registration: string);
}
