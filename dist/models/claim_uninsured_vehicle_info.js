var ClaimUninsuredVehicleInfo = (function () {
    function ClaimUninsuredVehicleInfo(personal_data, vehicle_info) {
        this.personal_data = personal_data;
        this.vehicle_info = vehicle_info;
    }
    return ClaimUninsuredVehicleInfo;
}());
export { ClaimUninsuredVehicleInfo };
var ClaimUninsuredVehicleInfoPersonalData = (function () {
    function ClaimUninsuredVehicleInfoPersonalData(name, surname1, surname2, identification_document_id, identification_document_number, phone, address) {
        this.name = name;
        this.surname1 = surname1;
        this.surname2 = surname2;
        this.identification_document_id = identification_document_id;
        this.identification_document_number = identification_document_number;
        this.phone = phone;
        this.address = address;
    }
    return ClaimUninsuredVehicleInfoPersonalData;
}());
export { ClaimUninsuredVehicleInfoPersonalData };
var ClaimUninsuredVehicleInfoPersonalDataAddress = (function () {
    function ClaimUninsuredVehicleInfoPersonalDataAddress(road_type_id, road_name, road_number, zip_code, city_id, province_id, country_id) {
        this.road_type_id = road_type_id;
        this.road_name = road_name;
        this.road_number = road_number;
        this.zip_code = zip_code;
        this.city_id = city_id;
        this.province_id = province_id;
        this.country_id = country_id;
    }
    return ClaimUninsuredVehicleInfoPersonalDataAddress;
}());
export { ClaimUninsuredVehicleInfoPersonalDataAddress };
var ClaimUninsuredVehicleInfoVehicleInfo = (function () {
    function ClaimUninsuredVehicleInfoVehicleInfo(registration_date, vehicle_brand_id, vehicle_model_id, doors_count, vehicle_color_id, registration) {
        this.registration_date = registration_date;
        this.vehicle_brand_id = vehicle_brand_id;
        this.vehicle_model_id = vehicle_model_id;
        this.doors_count = doors_count;
        this.vehicle_color_id = vehicle_color_id;
        this.registration = registration;
    }
    return ClaimUninsuredVehicleInfoVehicleInfo;
}());
export { ClaimUninsuredVehicleInfoVehicleInfo };
//# sourceMappingURL=claim_uninsured_vehicle_info.js.map