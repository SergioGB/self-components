export declare class Communication {
    communication_code: string;
    communication_date?: Date;
    communication_title: string;
    related_product: string;
}
