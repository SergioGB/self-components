export declare class Contact {
    address?: string;
    avatar?: string;
    chat_link?: string;
    email?: string;
    enable_chat?: boolean;
    name?: string;
    phone?: string;
    type?: string;
}
