export declare class ContentTab {
    key: any;
    label?: any;
    order?: number;
    active?: boolean;
    hide?: boolean;
}
