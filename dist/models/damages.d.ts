export declare class Damages {
    own_damages?: any;
    others_damages?: any;
    other_damages?: any;
    other_participants?: any;
    constructor(own_damages: any, others_damages: any, other_damages: any, other_participants: any);
}
