var Damages = (function () {
    function Damages(own_damages, others_damages, other_damages, other_participants) {
        this.own_damages = own_damages;
        this.others_damages = others_damages;
        this.other_damages = other_damages;
        this.other_participants = other_participants;
    }
    return Damages;
}());
export { Damages };
//# sourceMappingURL=damages.js.map