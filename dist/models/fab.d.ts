export declare class Fab {
    name: string;
    link: string;
    text: string;
    icon: string;
    page_ref?: any;
    link_data?: any;
}
