export interface FlowRequestInterface {
    step: string;
    variant_id: string;
    version_id: string;
    step_data: any;
}
export declare class FlowRequest implements FlowRequestInterface {
    step: string;
    variant_id: string;
    version_id: string;
    step_data: any;
}
