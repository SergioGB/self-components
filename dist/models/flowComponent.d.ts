export declare class FlowComponent {
    component: any;
    inputs?: Array<any>;
    flowInputs?: Array<string>;
    required?: boolean;
}
