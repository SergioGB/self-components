import { TakerInfo } from "./taker-info";
import { HomeInfo } from "./home-info";
export declare class HomeBudgetDetail {
    home_info: HomeInfo;
    taker_info: TakerInfo;
}
