export declare class HomeInfo {
    address?: string;
    housing_type?: {
        id?: string;
        title?: string;
    };
    size?: 0;
    construction_date?: Date;
    type?: string;
    use?: string;
    rooms_number?: string;
    bathrooms?: string;
    safety_guards?: any;
    floor?: string;
    regime?: {
        id?: string;
        title?: string;
    };
    construction_quality?: string;
}
