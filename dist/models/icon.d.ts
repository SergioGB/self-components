export declare class Icon {
    name: string;
    hex_color?: string;
}
