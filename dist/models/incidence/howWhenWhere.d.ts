import { Address } from '../address';
import { SinisterVehicleInfo } from './sinisterVehicleInfo';
export declare class HowWhenWhere {
    dateTime: Date;
    address: Address;
    howOwnVehicle: SinisterVehicleInfo;
    howOtherVehicles: SinisterVehicleInfo[];
}
