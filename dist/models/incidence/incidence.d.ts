import { SinisterInfo } from './sinisterInfo';
import { Address } from '../address';
import { Damages } from '../damages';
export declare class Incidence {
    type?: any;
    vehicleCount?: any;
    injuries?: any;
    sinister_type?: number;
    sinister_adjuster?: boolean;
    sinister_adjuster_detail?: any;
    subtype?: any;
    injured?: boolean;
    injured_detail?: any;
    sinister_info?: SinisterInfo;
    damages?: Damages;
    when?: any;
    where?: Address;
    constructor();
}
