import { SinisterInfo } from './sinisterInfo';
var Incidence = (function () {
    function Incidence() {
        this.injuries = {};
        this.injured_detail = {};
        this.sinister_info = new SinisterInfo();
        this.damages = {};
        this.when = {};
        this.where = {};
    }
    return Incidence;
}());
export { Incidence };
//# sourceMappingURL=incidence.js.map