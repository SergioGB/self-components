import { SinisterVehicleInfo } from "./sinisterVehicleInfo";
export declare class SinisterInfo {
    when: Date;
    where: string;
    how_own_vehicle: SinisterVehicleInfo;
    how_other_vehicles: SinisterVehicleInfo[];
    constructor();
}
