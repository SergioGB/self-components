export declare class SinisterVehicleInfo {
    driver_policy_role?: string;
    driver_circumstances?: string[];
    driver_name?: string;
    driver_surname1?: string;
    driver_surname2?: string;
    vehicle_status?: string;
    more_info?: string;
}
