export declare class SinisterWhere {
    road_type_id: number;
    road_name: string;
    road_number: number;
    cp: number;
    city_id: number;
    province_id: number;
    country_id: number;
}
