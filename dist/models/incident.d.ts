import { Policy } from "./policy";
import { KeyValue } from "./keyValue";
export declare class Incident {
    number?: string;
    additional_info?: KeyValue[];
    associated_policies: Policy[];
    risk_id: number;
    risk_name: string;
    risk_type: string;
    detail?: string;
    state?: string;
    opening_date?: Date;
}
