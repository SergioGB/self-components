import { Policy } from "./policy";
import { KeyValue } from "./keyValue";
export declare class Insurance {
    additional_info?: KeyValue[];
    associated_policies?: Policy[];
    annotations?: any;
    id?: string;
    name?: string;
    type?: string;
}
