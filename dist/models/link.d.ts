import { Modal } from "ionic-angular";
export declare class Link {
    link?: string;
    label?: string;
    title?: string;
    href?: string;
    order?: number;
    link_data?: any;
    modal_data?: Object;
    modal?: Modal;
}
export declare class LinkV1 {
    link?: string;
    client?: string;
    label?: string;
    title?: string;
    href?: string;
    order?: number;
    link_data?: any;
    modal_data?: Object;
    modal?: Modal;
}
