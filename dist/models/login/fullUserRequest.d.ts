export declare class FullUserRequest {
    avatar: string;
    personal_information: any;
    contact_information: any;
    security_information: any;
}
