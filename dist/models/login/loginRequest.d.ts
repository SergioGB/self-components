export declare class LoginRequest {
    user_pass_login: LoginRequestUserPassLogin;
    rememberMe?: boolean;
}
export declare class LoginRequestUserPassLogin {
    user: string;
    password: string;
}
