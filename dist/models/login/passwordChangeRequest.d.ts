export declare class PasswordChangeRequest {
    username: string;
    name: string;
    surname1: string;
    surname2: string;
    identification_number: string;
    mail: string;
    password: string;
    address: string;
    mobile_phone: string;
    security_questions: string;
    constructor(username: string, name: string, surname1: string, surname2: string, identification_number: string, mobile_phone: string, mail: string, address: string, password: string, security_questions: any);
}
