var PasswordChangeRequest = (function () {
    function PasswordChangeRequest(username, name, surname1, surname2, identification_number, mobile_phone, mail, address, password, security_questions) {
        this.username = username;
        this.name = name;
        this.surname1 = surname1;
        this.surname2 = surname2;
        this.identification_number = identification_number;
        this.mobile_phone = mobile_phone;
        this.mail = mail;
        this.address = address;
        this.password = password;
        this.security_questions = security_questions;
    }
    return PasswordChangeRequest;
}());
export { PasswordChangeRequest };
//# sourceMappingURL=passwordChangeRequest.js.map