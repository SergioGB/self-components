import { Icon } from '../icon';
export declare class QuickAction {
    description?: any;
    icon?: Icon;
    link?: any;
    title?: any;
    component_name?: string;
    static fromFormData(formData: any): QuickAction;
}
