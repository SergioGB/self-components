import { Utils } from '../../providers/utils/utils';
var QuickAction = (function () {
    function QuickAction() {
    }
    QuickAction.fromFormData = function (formData) {
        var quickAction = new QuickAction();
        quickAction.title = formData.title;
        quickAction.description = formData.description;
        quickAction.link = formData.link;
        quickAction.icon = formData.icon;
        quickAction.component_name = formData.component ? formData.component.component_name : undefined;
        Utils.removeNilKeys(quickAction);
        return quickAction;
    };
    return QuickAction;
}());
export { QuickAction };
//# sourceMappingURL=quickAction.js.map