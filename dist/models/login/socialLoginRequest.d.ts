export declare class SocialLoginRequest {
    facebookUserId?: string;
    facebookAuthToken?: string;
    twitterUserId?: string;
    twitterAuthToken?: string;
    googleplusUserId?: string;
    googleplusAuthToken?: string;
}
