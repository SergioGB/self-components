import { Address } from "../address";
export declare class UserRequest {
    name: string;
    surname1: string;
    surname2: string;
    identification_document: any;
    identification_number: string;
    mail: string;
    address: Address;
    mobile_phone: string;
    password?: string;
    validationPassword?: string;
    first_security_question?: any;
    first_security_question_answer?: string;
    second_security_question?: any;
    second_security_question_answer?: string;
}
