export declare class MapfreFile {
    id?: number;
    name?: string;
    title?: string;
    content?: string;
    mime_type?: string;
    size?: string;
}
