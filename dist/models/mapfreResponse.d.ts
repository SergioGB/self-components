import { MapfreResponseEntity } from "./mapfreResponseEntity";
import { KeyValue } from "./keyValue";
export declare class MapfreResponse {
    response: MapfreResponseEntity;
    data?: any;
    tags?: KeyValue[];
    total_items?: number;
}
export declare class MapfreResponseV1 {
    response: MapfreResponseEntity;
    data?: any;
    tags?: KeyValue[];
    total_items?: number;
    payload?: any;
    security_tokens?: any;
}
