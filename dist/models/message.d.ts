import { MessageEmailContent } from "./messageEmailContent";
import { SMSMessageContent } from "./smsMessageContent";
export declare class Message {
    type: any;
    email_content?: MessageEmailContent;
    sms_content?: SMSMessageContent;
    constructor(type: any, email_content?: MessageEmailContent, sms_content?: SMSMessageContent);
}
