var Message = (function () {
    function Message(type, email_content, sms_content) {
        this.type = type;
        this.email_content = email_content;
        this.sms_content = sms_content;
    }
    return Message;
}());
export { Message };
//# sourceMappingURL=message.js.map