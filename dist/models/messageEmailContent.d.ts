export declare class MessageEmailContent {
    sender: string;
    subject: string;
    template: string;
    recipients: string[];
    constructor(sender: string, subject: string, template: string, recipients: string[]);
}
