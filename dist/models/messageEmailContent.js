var MessageEmailContent = (function () {
    function MessageEmailContent(sender, subject, template, recipients) {
        this.sender = sender;
        this.subject = subject;
        this.template = template;
        this.recipients = recipients;
    }
    return MessageEmailContent;
}());
export { MessageEmailContent };
//# sourceMappingURL=messageEmailContent.js.map