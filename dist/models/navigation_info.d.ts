import { PageRef } from "./page_ref";
export declare class NavigationInfo {
    title?: string;
    description?: string;
    page_ref?: PageRef;
}
