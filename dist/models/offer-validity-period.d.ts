export declare class OfferValidityPeriod {
    period?: string;
    from?: string;
    to?: string;
}
