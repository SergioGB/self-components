import { Cost } from "./cost";
import { OfferValidityPeriod } from "./offer-validity-period";
export declare class Offer {
    id?: string;
    subtitle?: string;
    group?: string;
    insurance_line?: string;
    order?: number;
    image?: string;
    title?: string;
    description?: string;
    price?: Cost;
    validity_period?: OfferValidityPeriod;
    link?: string;
    keywords?: string[];
    benefits?: string[];
    terms?: string[];
}
