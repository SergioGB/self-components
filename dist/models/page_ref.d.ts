import { NavParamsModel } from "./nav_params_model";
export declare class PageRef {
    view_name?: string;
    nav_params?: NavParamsModel[];
}
