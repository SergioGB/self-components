export declare class PaymentMethod {
    id: string;
    type: number;
    credit_card_info?: any;
    bank_account_info?: any;
    default?: boolean;
}
