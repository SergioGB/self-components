export declare class PendingTopic {
    id?: string;
    icon?: any;
    expiration_info?: string;
    origin?: string;
    days_to_expiration: number;
    expiration_date: Date;
    number_comments: number;
    related_product: string;
    task_code: string;
    title: string;
}
