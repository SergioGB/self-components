export declare class Pill {
    mocked?: any;
    label: string;
    data?: any;
    active?: boolean;
    removable?: boolean;
}
