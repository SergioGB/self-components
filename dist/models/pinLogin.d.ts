export declare class PinLogin {
    identification_document_id?: any;
    identification_document_number?: any;
    birthdate?: any;
    access_pin?: any;
    constructor(identification_document_id?: any, identification_document_number?: any, birthdate?: any, access_pin?: any);
}
