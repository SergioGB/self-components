var PinLogin = (function () {
    function PinLogin(identification_document_id, identification_document_number, birthdate, access_pin) {
        this.identification_document_id = identification_document_id;
        this.identification_document_number = identification_document_number;
        this.birthdate = birthdate;
        this.access_pin = access_pin;
    }
    return PinLogin;
}());
export { PinLogin };
//# sourceMappingURL=pinLogin.js.map