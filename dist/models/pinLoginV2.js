var PinLoginV2 = (function () {
    function PinLoginV2(by_identification_number, by_mail, by_budget, access_pin) {
        this.by_identification_number = by_identification_number;
        this.by_mail = by_mail;
        this.by_budget = by_budget;
        this.access_pin = access_pin;
    }
    return PinLoginV2;
}());
export { PinLoginV2 };
//# sourceMappingURL=pinLoginV2.js.map