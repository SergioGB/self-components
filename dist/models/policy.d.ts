export declare class Policy {
    risk_id?: string;
    risk_name?: string;
    risk_type?: string;
    id?: string;
    name?: string;
    due_date?: Date;
    policy_annotations?: string;
    policy_number?: string;
    policy_state?: string;
    policy_type?: string;
    type?: string;
    price?: string;
    state?: string;
    car_policy_detail?: any;
    home_policy_detail?: any;
    additional_info?: any[];
    basic_info?: any;
    vehicle_info?: any;
    owner_info?: any;
    taker_info?: any;
    driver_info?: any;
    home_info?: any;
}
