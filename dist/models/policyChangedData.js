var PolicyChangedData = (function () {
    function PolicyChangedData(changedData, fieldName) {
        this.changedData = changedData;
        this.fieldName = fieldName;
    }
    return PolicyChangedData;
}());
export { PolicyChangedData };
//# sourceMappingURL=policyChangedData.js.map