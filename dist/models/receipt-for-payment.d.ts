export declare class ReceiptForPayment {
    id?: number;
    price: any;
    modality: string;
    risk_name: string;
    policy_type: string;
    number: string;
}
