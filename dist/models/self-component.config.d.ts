export interface SelfComponentsConfig {
    defaultLanguage?: string;
    forcedLanguageKey?: string;
    userDataCookieName?: string;
    userCookieName?: string;
    userDataType?: string;
    clientIdCookieName?: string;
    rememberedUserCookieName?: string;
    securityPinId?: string;
    fingerprintEnabledId?: string;
    apiKeyID?: string;
    rememberedUserCookieExpirationInDays?: number;
    numberPattern?: string;
    spacedNumberPattern?: string;
    phonePattern?: string;
    datePattern?: string;
    datePickerPattern?: string;
    emptyField?: string;
    colors?: any;
    clientPermissionsCookieName?: any;
}
export declare const defaultSelfComponentsConfig: SelfComponentsConfig;
export declare class ConfigProvider {
    static config: SelfComponentsConfig;
    static configure(selfComponentsConfig: SelfComponentsConfig): void;
}
