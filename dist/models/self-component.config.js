export var defaultSelfComponentsConfig = {
    defaultLanguage: 'es',
    forcedLanguageKey: 'forcedLanguage',
    userDataCookieName: 'userInfoMOD',
    userDataType: 'userDataTypeMOD',
    userCookieName: 'userNameMOD',
    clientIdCookieName: 'clientIdMOD',
    rememberedUserCookieName: 'rememberedUserMOD',
    securityPinId: 'securityPin',
    fingerprintEnabledId: 'fingerprintEnabled',
    apiKeyID: 'apiKey',
    rememberedUserCookieExpirationInDays: 365,
    numberPattern: '[0-9]*',
    spacedNumberPattern: '[0-9 ]*',
    phonePattern: '(\\+[0-9 ]*)?[0-9 ]*',
    datePattern: 'DD/MM/YYYY',
    datePickerPattern: 'DD MMMM YYYY',
    emptyField: '-',
    colors: {
        primary: '#D81E05'
    },
    clientPermissionsCookieName: 'clientPermissions'
};
var ConfigProvider = (function () {
    function ConfigProvider() {
    }
    ConfigProvider.configure = function (selfComponentsConfig) {
        this.config = selfComponentsConfig;
    };
    return ConfigProvider;
}());
export { ConfigProvider };
//# sourceMappingURL=self-component.config.js.map