export declare class SMSMessageContent {
    sender: string;
    template: string;
    recipients: string[];
    constructor(sender: string, template: string, recipients: string[]);
}
