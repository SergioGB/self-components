var SMSMessageContent = (function () {
    function SMSMessageContent(sender, template, recipients) {
        this.sender = sender;
        this.template = template;
        this.recipients = recipients;
    }
    return SMSMessageContent;
}());
export { SMSMessageContent };
//# sourceMappingURL=smsMessageContent.js.map