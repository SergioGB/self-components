import { Cost } from "./cost";
export declare class Step {
    step?: number;
    showStep?: number;
    pageName?: string;
    pageParams?: any;
    description?: string;
    title?: string;
    subtitle?: string | string[];
    status?: string;
    completed?: boolean;
    active?: boolean;
    visitable?: boolean;
    clickable?: boolean;
    hidden?: boolean;
    pageId?: string;
    queryUrl?: string;
}
export declare class StepPrice {
    step?: number;
    showStep?: number;
    pageName?: string;
    pageParams?: any;
    description?: string;
    title?: string;
    subtitle?: string | string[];
    status?: string;
    completed?: boolean;
    active?: boolean;
    visitable?: boolean;
    clickable?: boolean;
    hidden?: boolean;
    pageId?: string;
    queryUrl?: string;
    price?: Cost;
}
