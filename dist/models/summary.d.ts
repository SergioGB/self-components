import { MapfreFile } from './mapfreFile';
export declare class SummaryData {
    title: string;
    sections: SummarySection[];
}
export declare class SummarySection {
    title?: string;
    toggle?: boolean;
    type?: string;
    rows: SummaryRow[];
}
export declare class SummaryRow {
    key?: string;
    value?: string | string[];
    file?: MapfreFile;
    date?: any;
    listRow?: any[];
    accept_conditions?: any;
}
