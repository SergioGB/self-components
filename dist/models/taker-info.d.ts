export declare class TakerInfo {
    name?: string;
    gender?: {
        id?: string;
        title?: string;
    };
    bithdate?: string;
    zip_code?: string;
    identification_document_id?: string;
    identification_document_number?: string;
    license_date?: Date;
}
