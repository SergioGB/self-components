export declare class TooltipData {
    title?: string;
    description?: string;
}
