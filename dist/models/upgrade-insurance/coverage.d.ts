import { Cost } from "../cost";
export declare class Coverage {
    id?: string;
    title?: string;
    icon?: any;
    price?: Cost;
    paymentFrequency?: string;
    selected?: boolean;
    available?: boolean;
}
export declare class CoverageV2 {
    id?: string;
    title?: string;
    icon?: any;
    price?: Cost;
    paymentFrequency?: string;
    selected?: boolean;
    available?: boolean;
    prices_per_sum_insured?: any;
    selected_sum_insured?: any;
    per_sum_list?: any;
}
