import { Cost } from "../cost";
import { Coverage, CoverageV2 } from "./coverage";
export declare class InsuranceType {
    id: number;
    title: string;
    description: string;
    price?: Cost;
    totalPrice?: Cost;
    paymentFrequency?: string;
    optionalCoverages?: Coverage[];
    lostCoverages?: Coverage[];
    selected?: boolean;
    currentInsurance?: boolean;
}
export declare class InsuranceTypeV2 {
    id: number;
    title: string;
    description: string;
    price?: Cost;
    totalPrice?: Cost;
    general_considerations_document: any;
    paymentFrequency?: string;
    coverages: CoverageV2[];
    optionalCoverages?: Coverage[];
    lostCoverages?: Coverage[];
    selected?: boolean;
    additional_coverages?: CoverageV2[];
    currentInsurance?: boolean;
    payment_modality?: any;
}
