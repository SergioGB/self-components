import { Address } from "./address";
export declare class User {
    name: string;
    surname1: string;
    surname2: string;
    identification_document_id: string;
    identification_number: string;
    mail: string;
    address: Address;
    mobile_phone: string;
    preferred_language_id: string;
    permissions: any;
    id: string;
    avatar?: string;
    birthdate: Date;
    civil_state: string;
    gender: string;
    personal_data?: any;
    contact_data?: any;
    notification_preferences?: any;
    security_questions?: any;
}
