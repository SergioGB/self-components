export declare class Validation {
    recover_way: any;
    validation_code: any;
    security_questions?: any[];
    client_id: any;
    constructor(recover_way: any, validation_code?: any, security_questions?: any[], client_id?: any);
}
