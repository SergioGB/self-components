var Validation = (function () {
    function Validation(recover_way, validation_code, security_questions, client_id) {
        this.recover_way = recover_way;
        this.validation_code = validation_code;
        this.security_questions = security_questions;
        this.client_id = client_id;
    }
    return Validation;
}());
export { Validation };
//# sourceMappingURL=validation.js.map