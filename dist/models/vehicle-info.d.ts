export declare class VehicleInfo {
    brand?: string;
    model?: string;
    version?: string;
    registration?: string;
    doors_count?: string;
    release_date?: string;
    engine_power?: string;
    engine_capacity?: string;
    gearbox?: string;
    registration_date?: string;
    garage_type?: string;
}
