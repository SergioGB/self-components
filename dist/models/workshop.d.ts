import { Address } from './address';
import { Coverage } from './upgrade-insurance/coverage';
export declare class Workshop {
    id?: string;
    title?: string;
    phone?: string | number;
    address?: Address;
    coverages?: Coverage[];
    services?: Coverage[];
    favourite?: boolean;
    available?: boolean;
    appointment?: any;
}
