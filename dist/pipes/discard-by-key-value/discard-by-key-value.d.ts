import { PipeTransform } from '@angular/core';
export declare class DiscardByKeyValuePipe implements PipeTransform {
    transform(array: any[], key: string, value: any): any[];
}
