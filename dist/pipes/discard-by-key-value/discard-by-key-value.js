import { Pipe } from '@angular/core';
var DiscardByKeyValuePipe = (function () {
    function DiscardByKeyValuePipe() {
    }
    DiscardByKeyValuePipe.prototype.transform = function (array, key, value) {
        return array.filter(function (item) { return item[key] !== value; });
    };
    DiscardByKeyValuePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'discardByKeyValue'
                },] },
    ];
    DiscardByKeyValuePipe.ctorParameters = function () { return []; };
    return DiscardByKeyValuePipe;
}());
export { DiscardByKeyValuePipe };
//# sourceMappingURL=discard-by-key-value.js.map