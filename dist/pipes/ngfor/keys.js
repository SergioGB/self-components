import { Pipe } from '@angular/core';
var KeysPipe = (function () {
    function KeysPipe() {
    }
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var key in value) {
            keys.push({ key: key, value: value[key] });
        }
        return keys;
    };
    KeysPipe.decorators = [
        { type: Pipe, args: [{ name: 'keys' },] },
    ];
    KeysPipe.ctorParameters = function () { return []; };
    return KeysPipe;
}());
export { KeysPipe };
//# sourceMappingURL=keys.js.map