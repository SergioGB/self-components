import { PipeTransform } from '@angular/core';
export declare class SortPipe implements PipeTransform {
    transform(array: any[], field: string): any[];
}
