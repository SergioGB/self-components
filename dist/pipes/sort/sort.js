import { Pipe } from '@angular/core';
var SortPipe = (function () {
    function SortPipe() {
    }
    SortPipe.prototype.transform = function (array, field) {
        if (array && array.length) {
            array.sort(function (a, b) {
                if (a[field] < b[field]) {
                    return -1;
                }
                else if (a[field] > b[field]) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        return array;
    };
    SortPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'sort',
                },] },
    ];
    SortPipe.ctorParameters = function () { return []; };
    return SortPipe;
}());
export { SortPipe };
//# sourceMappingURL=sort.js.map