import { Utils } from '../utils/utils';
import { TranslationProvider } from '../translation/translation';
import { FormatProvider } from '../format/format';
import { ApiProvider } from '../api/api';
import { ProcessFailureModalComponent } from '../../components/_modals/process-failure-modal/process-failure-modal';
import { UserProvider } from '../user/user';
var AbstractFlowProvider = (function () {
    function AbstractFlowProvider(injector) {
        this.translationProvider = injector.get(TranslationProvider);
        this.formatProvider = injector.get(FormatProvider);
        this.apiProvider = injector.get(ApiProvider);
        this.userProvider = injector.get(UserProvider);
        this.currentStep = '1';
    }
    AbstractFlowProvider.prototype.getQueryUrls = function () {
        return this.queryUrls;
    };
    AbstractFlowProvider.prototype.setQueryUrls = function (queryUrls) {
        this.queryUrls = queryUrls;
    };
    AbstractFlowProvider.prototype.getOpenFlag = function () {
        return this.openFlag;
    };
    AbstractFlowProvider.prototype.setOpenFlag = function (openFlag) {
        this.openFlag = openFlag;
    };
    AbstractFlowProvider.prototype.getResult = function () {
        return this.result;
    };
    AbstractFlowProvider.prototype.setResult = function (result) {
        this.result = result;
    };
    AbstractFlowProvider.prototype.stepBack = function (navController, navParams) {
        var backStep = parseInt(this.currentStep) - 1;
        this.redirectToNextStep(navController, navParams, backStep.toString());
    };
    AbstractFlowProvider.prototype.redirectToNextStep = function (navController, navParams, nextStepId) {
        this.currentStep = nextStepId;
        navController.push(this.stepsPageName, this.getNewParams(navParams, {
            stepId: parseInt(nextStepId) ? parseInt(nextStepId) : parseInt(navParams.get('stepId')) + 1
        }), { animate: false });
    };
    AbstractFlowProvider.prototype.redirectToResult = function (navController, navParams, extraParams) {
        this.redirectTo(this.resultPageName, navController, navParams, extraParams);
    };
    AbstractFlowProvider.prototype.redirectTo = function (destination, navController, navParams, extraParams) {
        navController.push(destination, this.getNewParams(navParams, extraParams), { animate: false });
    };
    AbstractFlowProvider.prototype.redirectToGlobalPosition = function (navController) {
        navController.setRoot('GlobalPositionPage', {}, { animate: false });
    };
    AbstractFlowProvider.prototype.redirectToLogin = function (navController) {
        this.userProvider.deleteUserDataOpen();
        navController.setRoot('LoginPage', {}, { animate: false });
    };
    AbstractFlowProvider.prototype.redirectFromResponse = function (navController, navParams, mapfreResponse, mocked) {
        if (mocked) {
            this.redirectToNextStep(navController, navParams);
        }
        else if (mapfreResponse.data && mapfreResponse.data.nextStep) {
            this.redirectToNextStep(navController, navParams, mapfreResponse.data.nextStep);
        }
    };
    AbstractFlowProvider.prototype.cancelFlow = function (navController, modalController, modalSettings) {
        this.resetFlowData();
        modalController.create(ProcessFailureModalComponent, {
            settings: modalSettings
        }).present();
        if (this.isOpenFlow) {
            navController.setRoot('LoginPage', {}, { animate: true, direction: 'forward' });
        }
        else {
            navController.setRoot('GlobalPositionPage', {}, { animate: true, direction: 'forward' });
        }
    };
    AbstractFlowProvider.prototype.onRequestPost = function (response, callback) {
        var mapfreResponse = this.apiProvider.getResponseJSON(response);
        this.setRequestIdFromResponse(response);
        if (Utils.isFunction(callback)) {
            callback(mapfreResponse);
        }
    };
    AbstractFlowProvider.prototype.onRequestPut = function (response, callback) {
        var mapfreResponse = this.apiProvider.getResponseJSON(response);
        if (Utils.isFunction(callback)) {
            callback(mapfreResponse);
        }
    };
    AbstractFlowProvider.prototype.onRequestDelete = function (response, callback) {
        var mapfreResponse = this.apiProvider.getResponseJSON(response);
        if (Utils.isFunction(callback)) {
            callback(mapfreResponse);
        }
    };
    AbstractFlowProvider.prototype.setRequestIdFromResponse = function (response) {
        this.requestId = response.json().data && response.json().data.stepData ? response.json().data.stepData.id : undefined;
    };
    AbstractFlowProvider.prototype.getNewParams = function (navParams, extraParams) {
        var newParams = Utils.copy(navParams.data);
        if (Utils.isObject(extraParams)) {
            Object.keys(extraParams).forEach(function (paramKey) {
                newParams[paramKey] = extraParams[paramKey];
            });
        }
        return newParams;
    };
    return AbstractFlowProvider;
}());
export { AbstractFlowProvider };
//# sourceMappingURL=abstract-flow.js.map