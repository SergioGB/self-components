export declare class AnimationsProvider {
    static transitionDuration: number;
    static collapsedClass: string;
    constructor();
    verticalSlideToggle(containerSelector: string, transitionDuration?: number): void;
    verticalSlide(containerSelector: string, open?: boolean, transitionDuration?: number): void;
    slideUp(containerSelector: string, transitionDuration?: number): void;
    addClass(containerSelector: string, customClass: string): void;
    slideDown(containerSelector: string, transitionDuration?: number): void;
    isCollapsed(containerSelector: string): boolean;
    private getTransitionDuration;
}
