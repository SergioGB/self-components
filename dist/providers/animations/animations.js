import { Injectable } from '@angular/core';
import * as jQuery from "jquery";
var AnimationsProvider = (function () {
    function AnimationsProvider() {
    }
    AnimationsProvider.prototype.verticalSlideToggle = function (containerSelector, transitionDuration) {
        this.verticalSlide(containerSelector, this.isCollapsed(containerSelector), transitionDuration);
    };
    AnimationsProvider.prototype.verticalSlide = function (containerSelector, open, transitionDuration) {
        var container = jQuery(containerSelector);
        if (container && container.length) {
            if (open) {
                this.slideDown(containerSelector, transitionDuration);
            }
            else {
                this.slideUp(containerSelector, transitionDuration);
            }
        }
    };
    AnimationsProvider.prototype.slideUp = function (containerSelector, transitionDuration) {
        var container = jQuery(containerSelector);
        if (container && container.length) {
            container.slideUp(this.getTransitionDuration(transitionDuration));
            container.addClass(AnimationsProvider.collapsedClass);
        }
    };
    AnimationsProvider.prototype.addClass = function (containerSelector, customClass) {
        var container = jQuery(containerSelector);
        if (container && container.length) {
            container.addClass(customClass);
        }
    };
    AnimationsProvider.prototype.slideDown = function (containerSelector, transitionDuration) {
        var container = jQuery(containerSelector);
        if (container && container.length) {
            container.slideDown(this.getTransitionDuration(transitionDuration));
            container.removeClass(AnimationsProvider.collapsedClass);
        }
    };
    AnimationsProvider.prototype.isCollapsed = function (containerSelector) {
        var container = jQuery(containerSelector);
        return container && container.length && container.hasClass(AnimationsProvider.collapsedClass);
    };
    AnimationsProvider.prototype.getTransitionDuration = function (transitionDuration) {
        return transitionDuration >= 0 ? transitionDuration : AnimationsProvider.transitionDuration;
    };
    AnimationsProvider.transitionDuration = 250;
    AnimationsProvider.collapsedClass = 'collapsed';
    AnimationsProvider.decorators = [
        { type: Injectable },
    ];
    AnimationsProvider.ctorParameters = function () { return []; };
    return AnimationsProvider;
}());
export { AnimationsProvider };
//# sourceMappingURL=animations.js.map