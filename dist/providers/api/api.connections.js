export var connection = {
    mapfreApiUrl: 'http://autoserviciopre-apinegocio.eu-west-1.elasticbeanstalk.com/MapfreClients/api/1.0.0',
    autoservicioApiUrl: 'https://bmj9yotz9h.execute-api.eu-west-1.amazonaws.com/autoservicio/1.0.0',
    configurationApiUrl: 'https://1a3pl7v1je.execute-api.eu-west-1.amazonaws.com/selfconfigtool/api/1.0.0',
    mocksApiUrl: 'http://ApiEnlaceMocks.3hk8mkurcr.eu-west-1.elasticbeanstalk.com/MapfreClients/api/1.0.0'
};
export var localClientIds = {
    google: '741989573397-g1apio7fu5261rup7161jrigisjdnb5j.apps.googleusercontent.com',
    facebook: '158289531510253',
    linkedin: '86hm8arvicxrj4'
};
export var googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA4ggV0ms2F81qiWd9_fVB7Wy4R4P0SDc0';
export var firebaseConfig = {
    apiKey: "AIzaSyBmQmSLUebhIDJxhS1xVphONS5qlXEsm30",
    authDomain: "autoservicio-mapfre.firebaseapp.com",
    databaseURL: "https://autoservicio-mapfre.firebaseio.com",
    projectId: "autoservicio-mapfre",
    storageBucket: "autoservicio-mapfre.appspot.com",
    messagingSenderId: "741989573397"
};
//# sourceMappingURL=api.connections.js.map