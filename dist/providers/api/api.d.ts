import { Injector } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Events, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { CookieService } from 'ngx-cookie';
import { DateProvider } from '../date/date';
import { IdentificationProvider } from '../identification/identification';
import { MapfreResponse, MapfreResponseV1 } from '../../models/mapfreResponse';
import { MapfreResponseEntity } from '../../models/mapfreResponseEntity';
import { User } from '../../models/user';
export declare class ApiProvider {
    protected injector: Injector;
    connections: any;
    protected http: Http;
    protected events: Events;
    protected storage: Storage;
    protected cookieService: CookieService;
    protected dateProvider: DateProvider;
    protected identificationProvider: IdentificationProvider;
    private lastVisitedPage;
    static componentName: string;
    static errorEvent: string;
    constructor(injector: Injector);
    setClientId(clientId: string): void;
    getClientId(): string;
    deleteClientId(): void;
    setUserName(userName: string): void;
    getUserName(): string;
    setUserType(userData: User): void;
    getUserType(): Observable<string>;
    setUserData(userData: User): void;
    getUserData(): Observable<User>;
    redirectWhenNotLoggedIn(navController: NavController): void;
    getLoggedUserData(): User;
    getLoggedUserType(): string;
    getAppUserData(): Promise<User>;
    deleteUserData(): void;
    isLoggedIn(): boolean;
    isTemporarilyLoggedIn(): boolean;
    getQueryUrl(mocked: boolean, business_api: boolean, suffix: string, parameters?: any): string;
    getApiVersion(): string;
    getLanguage(requestedLanguage?: string): string;
    getObservableForSuffix(mocked: boolean, business_api: boolean, suffix: string, authenticated: boolean, parameters?: any): Observable<Response>;
    postObservableForSuffix(mocked: boolean, business_api: boolean, suffix: string, authenticated: boolean, parameters?: any, data?: any): Observable<Response>;
    putObservableForSuffix(mocked: boolean, business_api: boolean, suffix: string, authenticated: boolean, parameters?: any, data?: any): Observable<Response>;
    deleteObservableForSuffix(mocked: boolean, business_api: boolean, suffix: string, authenticated: boolean, parameters?: any, data?: any): Observable<Response>;
    getHeaders(authenticated: boolean, language?: string): Promise<Headers>;
    getResponseJSON(response: Response): MapfreResponse;
    getResponseJSONV1(response: Response): MapfreResponse;
    validateResponse(response: Response): void;
    isResponseValid(response: Response): boolean;
    isMapfreResponseValid(mapfreResponse: MapfreResponse): boolean;
    isMapfreResponseValidV1(mapfreResponse: MapfreResponseV1): boolean;
    areResponsesValid(responses: Response[]): boolean;
    areMapfreResponsesValid(mapfreResponses: MapfreResponse[]): boolean;
    isResponseEntityValid(response: Response): boolean;
    isMapfreResponseEntityValid(mapfreResponseEntity: MapfreResponseEntity): boolean;
    setLastVisitedPageWhenError(navController: NavController): void;
    getLastVisitedPageBeforeError(): any;
    bindToInputs(target: any, values: any): void;
}
