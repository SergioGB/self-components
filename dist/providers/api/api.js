import { Injectable, Injector } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Events } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { CookieService } from 'ngx-cookie';
import * as _ from 'lodash';
import { DateProvider } from '../date/date';
import { IdentificationProvider } from '../identification/identification';
import { connection } from './api.connections';
import { ConfigProvider } from '../../models/self-component.config';
import { Utils } from '../utils/utils';
var ApiProvider = (function () {
    function ApiProvider(injector) {
        this.injector = injector;
        this.http = injector.get(Http);
        this.events = injector.get(Events);
        this.storage = injector.get(Storage);
        this.cookieService = injector.get(CookieService);
        this.dateProvider = injector.get(DateProvider);
        this.identificationProvider = injector.get(IdentificationProvider);
        this.connections = connection;
    }
    ApiProvider.prototype.setClientId = function (clientId) {
        this.cookieService.put(ConfigProvider.config.clientIdCookieName, clientId);
    };
    ApiProvider.prototype.getClientId = function () {
        return this.cookieService.get(ConfigProvider.config.clientIdCookieName);
    };
    ApiProvider.prototype.deleteClientId = function () {
        this.cookieService.remove(ConfigProvider.config.clientIdCookieName);
    };
    ApiProvider.prototype.setUserName = function (userName) {
        this.cookieService.put(ConfigProvider.config.userCookieName, userName);
    };
    ApiProvider.prototype.getUserName = function () {
        return this.cookieService.get(ConfigProvider.config.userCookieName);
    };
    ApiProvider.prototype.setUserType = function (userData) {
        var userDataString = JSON.stringify(userData);
        this.cookieService.put(ConfigProvider.config.userDataType, userDataString);
        this.storage.set(ConfigProvider.config.userDataType, userDataString);
    };
    ApiProvider.prototype.getUserType = function () {
        return Observable.of(this.getLoggedUserType());
    };
    ApiProvider.prototype.setUserData = function (userData) {
        var userDataString = JSON.stringify(userData);
        this.cookieService.put(ConfigProvider.config.userDataCookieName, userDataString);
        this.storage.set(ConfigProvider.config.userDataCookieName, userDataString);
    };
    ApiProvider.prototype.getUserData = function () {
        return Observable.of(this.getLoggedUserData());
    };
    ApiProvider.prototype.redirectWhenNotLoggedIn = function (navController) {
        if (!this.isLoggedIn()) {
            navController.setRoot('LoginPage');
        }
    };
    ApiProvider.prototype.getLoggedUserData = function () {
        var userDataString = this.cookieService.get(ConfigProvider.config.userDataCookieName);
        return userDataString ? JSON.parse(userDataString) : undefined;
    };
    ApiProvider.prototype.getLoggedUserType = function () {
        var userTypeString = this.cookieService.get(ConfigProvider.config.userDataType);
        return userTypeString ? JSON.parse(userTypeString) : undefined;
    };
    ApiProvider.prototype.getAppUserData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get(ConfigProvider.config.userDataCookieName).then(function (userDataString) {
                resolve(userDataString ? JSON.parse(userDataString) : undefined);
            });
        });
    };
    ApiProvider.prototype.deleteUserData = function () {
        this.cookieService.remove(ConfigProvider.config.userDataCookieName);
    };
    ApiProvider.prototype.isLoggedIn = function () {
        return !_.isEmpty(this.cookieService.get(ConfigProvider.config.userDataCookieName));
    };
    ApiProvider.prototype.isTemporarilyLoggedIn = function () {
        return true;
    };
    ApiProvider.prototype.getQueryUrl = function (mocked, business_api, suffix, parameters) {
        var queryUrl = '';
        if (!business_api) {
            queryUrl += "" + this.connections.autoservicioApiUrl;
        }
        else {
            if (mocked) {
                queryUrl += "" + this.connections.mocksApiUrl;
            }
            else {
                queryUrl += "" + this.connections.mapfreApiUrl;
            }
        }
        queryUrl += "/" + suffix + "?";
        if (parameters && Object.keys(parameters).length) {
            Object.keys(parameters).forEach(function (key) {
                if (Array.isArray(parameters[key])) {
                    for (var _i = 0, _a = parameters[key]; _i < _a.length; _i++) {
                        var parameter = _a[_i];
                        queryUrl += key + "=" + parameter + "&";
                    }
                }
                else {
                    queryUrl += key + "=" + parameters[key] + "&";
                }
            });
        }
        return queryUrl;
    };
    ApiProvider.prototype.getApiVersion = function () {
        return '1.0';
    };
    ApiProvider.prototype.getLanguage = function (requestedLanguage) {
        var forcedLanguage = this.cookieService.get(ConfigProvider.config.forcedLanguageKey);
        var userData = this.getLoggedUserData();
        var userLanguage = userData ? userData.preferred_language_id : undefined;
        var browserLanguage = navigator.language;
        var defaultLanguage = ConfigProvider.config.defaultLanguage;
        var languageSelected = forcedLanguage || requestedLanguage || userLanguage || browserLanguage || defaultLanguage;
        var languageSplit = languageSelected.split('-');
        if (languageSplit && languageSplit.length > 1) {
            languageSelected = languageSplit[0];
        }
        return languageSelected;
    };
    ApiProvider.prototype.getObservableForSuffix = function (mocked, business_api, suffix, authenticated, parameters) {
        var _this = this;
        return Observable.fromPromise(this.getHeaders(authenticated)).mergeMap(function (headers) {
            if (parameters && parameters.logged_user_id) {
                headers.append('logged_user_id', _this.getClientId());
                delete parameters.logged_user_id;
            }
            return _this.http.get(_this.getQueryUrl(mocked, business_api, suffix, parameters), { headers: headers });
        });
    };
    ApiProvider.prototype.postObservableForSuffix = function (mocked, business_api, suffix, authenticated, parameters, data) {
        var _this = this;
        return Observable.fromPromise(this.getHeaders(authenticated)).mergeMap(function (headers) {
            return _this.http.post(_this.getQueryUrl(mocked, business_api, suffix, parameters), data, { headers: headers });
        });
    };
    ApiProvider.prototype.putObservableForSuffix = function (mocked, business_api, suffix, authenticated, parameters, data) {
        var _this = this;
        return Observable.fromPromise(this.getHeaders(authenticated)).mergeMap(function (headers) {
            return _this.http.put(_this.getQueryUrl(mocked, business_api, suffix, parameters), data, { headers: headers });
        });
    };
    ApiProvider.prototype.deleteObservableForSuffix = function (mocked, business_api, suffix, authenticated, parameters, data) {
        var _this = this;
        return Observable.fromPromise(this.getHeaders(authenticated)).mergeMap(function (headers) {
            return _this.http.delete(_this.getQueryUrl(mocked, business_api, suffix, parameters), { headers: headers, body: data });
        });
    };
    ApiProvider.prototype.getHeaders = function (authenticated, language) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new Headers();
            if (language) {
                headers.append('Accept-Language', _this.getLanguage(language));
            }
            else {
                headers.append('Accept-Language', _this.getLanguage(null));
            }
            if (authenticated) {
                _this.identificationProvider.getApiKey().then(function (apiKey) {
                    headers.append('Authorization', apiKey);
                    resolve(headers);
                });
            }
            else {
                resolve(headers);
            }
        });
    };
    ApiProvider.prototype.getResponseJSON = function (response) {
        this.validateResponse(response);
        return (response && response.text().length && response.json().response) ? response.json() : {};
    };
    ApiProvider.prototype.getResponseJSONV1 = function (response) {
        this.validateResponse(response);
        return (response && response.text().length && response.json().payload.response) ? response.json() : {};
    };
    ApiProvider.prototype.validateResponse = function (response) {
        if (response.status === 500) {
            this.events.publish(ApiProvider.errorEvent, response);
        }
    };
    ApiProvider.prototype.isResponseValid = function (response) {
        return this.isMapfreResponseValid(this.getResponseJSON(response));
    };
    ApiProvider.prototype.isMapfreResponseValid = function (mapfreResponse) {
        return mapfreResponse ? this.isMapfreResponseEntityValid(mapfreResponse.response) : false;
    };
    ApiProvider.prototype.isMapfreResponseValidV1 = function (mapfreResponse) {
        return mapfreResponse ? this.isMapfreResponseEntityValid(mapfreResponse.payload.response) : false;
    };
    ApiProvider.prototype.areResponsesValid = function (responses) {
        var _this = this;
        responses.forEach(function (response) {
            if (!_this.isResponseValid(response)) {
                return false;
            }
        });
        return true;
    };
    ApiProvider.prototype.areMapfreResponsesValid = function (mapfreResponses) {
        var _this = this;
        mapfreResponses.forEach(function (mapfreResponse) {
            if (!_this.isMapfreResponseValid(mapfreResponse)) {
                return false;
            }
        });
        return true;
    };
    ApiProvider.prototype.isResponseEntityValid = function (response) {
        return this.isMapfreResponseEntityValid(this.getResponseJSON(response).response);
    };
    ApiProvider.prototype.isMapfreResponseEntityValid = function (mapfreResponseEntity) {
        return mapfreResponseEntity && mapfreResponseEntity.code === 0;
    };
    ApiProvider.prototype.setLastVisitedPageWhenError = function (navController) {
        var name = Utils.getLastViewName(navController);
        var params = Utils.getLastViewParameters(navController);
        this.lastVisitedPage = { name: name, params: params };
    };
    ApiProvider.prototype.getLastVisitedPageBeforeError = function () {
        return !_.isEmpty(this.lastVisitedPage) ? this.lastVisitedPage : null;
    };
    ApiProvider.prototype.bindToInputs = function (target, values) {
        for (var prop in values) {
            if (values.hasOwnProperty(prop)) {
                target[prop] = values[prop];
            }
        }
    };
    ApiProvider.componentName = 'api-provider';
    ApiProvider.errorEvent = ApiProvider.componentName + ":error";
    ApiProvider.decorators = [
        { type: Injectable },
    ];
    ApiProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return ApiProvider;
}());
export { ApiProvider };
//# sourceMappingURL=api.js.map