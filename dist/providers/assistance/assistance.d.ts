import { Injector } from '@angular/core';
import { Response } from '@angular/http';
import { ApiProvider } from '../api/api';
import { Observable } from 'rxjs/Observable';
import { Assistance } from '../../models/assistance';
export declare class AssistanceProvider extends ApiProvider {
    protected injector: Injector;
    protected apiProvider: ApiProvider;
    private assistanceRequest;
    constructor(injector: Injector, apiProvider: ApiProvider);
    cancelAssistance(mocked: boolean, assistance: Assistance): Observable<Response>;
}
