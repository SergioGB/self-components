var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
var AssistanceProvider = (function (_super) {
    __extends(AssistanceProvider, _super);
    function AssistanceProvider(injector, apiProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.apiProvider = apiProvider;
        return _this;
    }
    AssistanceProvider.prototype.cancelAssistance = function (mocked, assistance) {
        return this.apiProvider.deleteObservableForSuffix(mocked, true, "assistance/" + assistance.assistance_id, true, true);
    };
    AssistanceProvider.decorators = [
        { type: Injectable },
    ];
    AssistanceProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ApiProvider, },
    ]; };
    return AssistanceProvider;
}(ApiProvider));
export { AssistanceProvider };
//# sourceMappingURL=assistance.js.map