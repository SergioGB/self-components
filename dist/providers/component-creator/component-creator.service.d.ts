import { ComponentFactory, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { Type } from '@angular/core/src/type';
export declare class ComponentCreatorService {
    private _componentFactoryResolver;
    constructor(_componentFactoryResolver: ComponentFactoryResolver);
    createComponent<T>(viewRef: ViewContainerRef, component: Type<T>, inputs?: Array<any>): T;
    createComponentWithFactory<T>(viewRef: ViewContainerRef, componentFactory: ComponentFactory<T>): T;
    bindToInputs(target: any, values: any): void;
}
