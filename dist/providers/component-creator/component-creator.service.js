import { ComponentFactoryResolver, Injectable } from '@angular/core';
var ComponentCreatorService = (function () {
    function ComponentCreatorService(_componentFactoryResolver) {
        this._componentFactoryResolver = _componentFactoryResolver;
    }
    ComponentCreatorService.prototype.createComponent = function (viewRef, component, inputs) {
        var componentInstance = null;
        if (viewRef && component) {
            componentInstance =
                viewRef.createComponent(this._componentFactoryResolver.resolveComponentFactory(component)).instance;
            this.bindToInputs(componentInstance, inputs);
        }
        return componentInstance;
    };
    ComponentCreatorService.prototype.createComponentWithFactory = function (viewRef, componentFactory) {
        return viewRef && componentFactory ? viewRef.createComponent(componentFactory).instance : null;
    };
    ComponentCreatorService.prototype.bindToInputs = function (target, values) {
        for (var prop in values) {
            if (values.hasOwnProperty(prop)) {
                target[prop] = values[prop];
            }
        }
    };
    ComponentCreatorService.decorators = [
        { type: Injectable },
    ];
    ComponentCreatorService.ctorParameters = function () { return [
        { type: ComponentFactoryResolver, },
    ]; };
    return ComponentCreatorService;
}());
export { ComponentCreatorService };
//# sourceMappingURL=component-creator.service.js.map