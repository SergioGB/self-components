var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
import 'rxjs/add/operator/map';
var ComponentSettingsProvider = (function (_super) {
    __extends(ComponentSettingsProvider, _super);
    function ComponentSettingsProvider(injector, apiProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.apiProvider = apiProvider;
        _this.connect = apiProvider.connections;
        return _this;
    }
    ComponentSettingsProvider.prototype.getLanguageSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'labels', false);
    };
    ComponentSettingsProvider.prototype.getHeaderSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/header", true);
    };
    ComponentSettingsProvider.prototype.getFooterSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'footer/', false);
    };
    ComponentSettingsProvider.prototype.getQuickManagementSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'quick_steps', false);
    };
    ComponentSettingsProvider.prototype.getQuickLinkSettings = function (mocked, clientType) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/quick_links/", true, { type: clientType });
    };
    ComponentSettingsProvider.prototype.getSuccessStoriesSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'opinions', false);
    };
    ComponentSettingsProvider.prototype.getBannerSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'banner/1', false);
    };
    ComponentSettingsProvider.prototype.getLoginVideoSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'video/1', false);
    };
    ComponentSettingsProvider.prototype.getAppDownloadSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'mobile_applications', false);
    };
    ComponentSettingsProvider.prototype.getSocialNetworkContactSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'social_networks', false);
    };
    ComponentSettingsProvider.prototype.getContactSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/contact/", true);
    };
    ComponentSettingsProvider.prototype.getContactPreferences = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'contact_preferences/', true);
    };
    ComponentSettingsProvider.prototype.getTimeRanges = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/daytime_slots/', true);
    };
    ComponentSettingsProvider.prototype.getSecurityQuestions = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/security_questions/', true);
    };
    ComponentSettingsProvider.prototype.getOffersSettings = function (mocked, parameters) {
        if (parameters) {
            return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/offers/", true, {
                page: parameters ? parameters.page : 0,
                page_size: parameters ? parameters.page_size : 3
            });
        }
        else {
            return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/offers/", true);
        }
    };
    ComponentSettingsProvider.prototype.getRelatedOffersSettings = function (mocked, relatedOfferId, parameters) {
        if (parameters) {
            return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/offers/", true, {
                related_to: relatedOfferId,
                page: parameters ? parameters.page : 0,
                page_size: parameters ? parameters.page_size : 3
            });
        }
        else {
            return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/offers/", true, {
                related_to: relatedOfferId
            });
        }
    };
    ComponentSettingsProvider.prototype.getProfessionalSchedules = function (mocked, professionalId) {
        return this.getObservableForSuffix(mocked, false, "professional/" + professionalId + "/appointment_schedule/", true);
    };
    ComponentSettingsProvider.prototype.getInsuranceSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/risks/", true);
    };
    ComponentSettingsProvider.prototype.getClaimsSettings = function (mocked, policy_id, riskId) {
        return this.getObservableForSuffix(mocked, false, "policy/" + policy_id + "/risk/" + riskId + "/claims", true);
    };
    ComponentSettingsProvider.prototype.getClaimDetailSettings = function (mocked, claim_id) {
        return this.getObservableForSuffix(mocked, false, "claim/" + claim_id, true);
    };
    ComponentSettingsProvider.prototype.getPendingTopicsSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/tasks/", true);
    };
    ComponentSettingsProvider.prototype.getBudgetSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/budgets", true);
    };
    ComponentSettingsProvider.prototype.getBudgetSettingsByState = function (mocked, state, page, page_size) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/budgets", true, { state: state, page: page, page_size: page_size });
    };
    ComponentSettingsProvider.prototype.getCommunicationsSettingsByState = function (mocked, state) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/communications/", true, { state: state });
    };
    ComponentSettingsProvider.prototype.getManagerSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/agent/", true);
    };
    ComponentSettingsProvider.prototype.getPaymentsSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/payment_methods/", true);
    };
    ComponentSettingsProvider.prototype.getPolicyTypes = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "policy_types/", true);
    };
    ComponentSettingsProvider.prototype.getBudgetPolicyTypes = function (mocked, budgetId) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/budget/" + budgetId + "/policy_types", true);
    };
    ComponentSettingsProvider.prototype.postPaymentMethod = function (mocked, paymentMethod) {
        return this.postObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/payment_methods/", true, null, paymentMethod);
    };
    ComponentSettingsProvider.prototype.putPaymentMethod = function (mocked, paymentMethod) {
        return this.putObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/payment_method/" + paymentMethod.id, true, null, paymentMethod);
    };
    ComponentSettingsProvider.prototype.getAssistancesSettings = function (mocked, claim_id, by_claim) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/assistances", true, { by_claim: by_claim, claim_id: claim_id });
    };
    ComponentSettingsProvider.prototype.getSinisterTracing = function (mocked, proceeding_id) {
        return this.getObservableForSuffix(mocked, true, "proceeding/" + proceeding_id + "/tasks", true);
    };
    ComponentSettingsProvider.prototype.getLabels = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/labels", true);
    };
    ComponentSettingsProvider.prototype.getCookiesMessage = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'cookies_message', false);
    };
    ComponentSettingsProvider.prototype.getIdentificationDocuments = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/identification_documents', true);
    };
    ComponentSettingsProvider.prototype.getIdentificationDocumentsById = function (mocked, document_id) {
        return this.getObservableForSuffix(mocked, true, "catalogs/identification_documents/" + document_id, true);
    };
    ComponentSettingsProvider.prototype.getClaimTypeSettings = function (mocked, risk_type) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/claim_types', true, { risk_type: risk_type });
    };
    ComponentSettingsProvider.prototype.getPreferredProfessionalSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'professionals', false);
    };
    ComponentSettingsProvider.prototype.getFavProfessionalSettings = function (mocked, client_id) {
        return this.getObservableForSuffix(mocked, false, "client/" + client_id + "/professionals", true);
    };
    ComponentSettingsProvider.prototype.getAssistanceTypeSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/assistance_types', true);
    };
    ComponentSettingsProvider.prototype.getSinisterTypeSettings = function (mocked, claim_id) {
        return this.getObservableForSuffix(mocked, true, "catalogs/claim/" + claim_id + "/claim_types", true);
    };
    ComponentSettingsProvider.prototype.getGlobalPositionDynamicConfig = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'configuration/global-position', true);
    };
    ComponentSettingsProvider.prototype.getClaimDocumentsSettings = function (mocked, claim_id, owner, sort_by, sort_order) {
        var queryParams = {};
        if (owner) {
            queryParams["owner"] = owner;
        }
        if (sort_by && sort_order) {
            queryParams["sort_by"] = sort_by;
            queryParams["sort_order"] = sort_order;
        }
        return this.getObservableForSuffix(mocked, false, "claim/" + claim_id + "/documents", true, queryParams);
    };
    ComponentSettingsProvider.prototype.getClaimImagesSettings = function (mocked, claim_id) {
        return this.getObservableForSuffix(mocked, true, "claim/" + claim_id + "/images", true);
    };
    ComponentSettingsProvider.prototype.getProceedingsSettings = function (mocked, claim_id) {
        return this.getObservableForSuffix(mocked, true, "claim/" + claim_id + "/proceedings", true);
    };
    ComponentSettingsProvider.prototype.getProceedingTasksSettings = function (mocked, proceeding_id) {
        return this.getObservableForSuffix(mocked, true, "proceeding/" + proceeding_id + "/tasks", true);
    };
    ComponentSettingsProvider.prototype.getProceedingTaskDocumentsSettings = function (mocked, proceeding_id, task_id, owner, sort_by, sort_order) {
        var queryParams = {};
        if (owner) {
            queryParams["owner"] = owner;
        }
        if (sort_by && sort_order) {
            queryParams["sort_by"] = sort_by;
            queryParams["sort_order"] = sort_order;
        }
        return this.getObservableForSuffix(mocked, false, "proceeding/" + proceeding_id + "/task/" + task_id + "/documents", true, queryParams);
    };
    ComponentSettingsProvider.prototype.getProceedingTaskImagesSettings = function (mocked, proceeding_id, task_id) {
        return this.getObservableForSuffix(mocked, true, "proceeding/" + proceeding_id + "/task/" + task_id + "/images", true);
    };
    ComponentSettingsProvider.prototype.getFaqsSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'faqs/', true);
    };
    ComponentSettingsProvider.prototype.getCheckDamagesVisibilitySettings = function (mocked, claim_id) {
        return this.getObservableForSuffix(mocked, true, "claim/" + claim_id + "/damages", true);
    };
    ComponentSettingsProvider.prototype.getCountriesSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/countries', true);
    };
    ComponentSettingsProvider.prototype.getStatesSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/states', true);
    };
    ComponentSettingsProvider.prototype.getProvincesSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/provinces', true);
    };
    ComponentSettingsProvider.prototype.getCitiesSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/cities', true);
    };
    ComponentSettingsProvider.prototype.getRoadTypesSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/road_types', true);
    };
    ComponentSettingsProvider.prototype.getHelpMethods = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/help_methods", true);
    };
    ComponentSettingsProvider.prototype.getPoliciesFromRiskSettings = function (mocked, risk_id) {
        return this.getObservableForSuffix(mocked, true, "policy/" + risk_id + "/receipts", true);
    };
    ComponentSettingsProvider.prototype.getSocialLoginsSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'social_logins', true);
    };
    ComponentSettingsProvider.prototype.getClientsSettings = function (mocked, username) {
        return this.getObservableForSuffix(mocked, true, 'clients', true, { username: username });
    };
    ComponentSettingsProvider.prototype.getClientReceipts = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/receipts", true);
    };
    ComponentSettingsProvider.prototype.getClientReceiptsByStateSettings = function (mocked, state) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/receipts", true, { state: state });
    };
    ComponentSettingsProvider.prototype.getUnpaidClientReceipts = function (mocked) {
        return this.getClientReceiptsByStateSettings(mocked, 'UNPAID');
    };
    ComponentSettingsProvider.prototype.getWelcomePack = function (mocked, platform) {
        return this.getObservableForSuffix(mocked, false, "welcome_pack", true, { platform: platform });
    };
    ComponentSettingsProvider.prototype.getContactChannels = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/contact_channels', true);
    };
    ComponentSettingsProvider.prototype.getVehicleTypes = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/vehicle_types', true);
    };
    ComponentSettingsProvider.prototype.getVehicleBrands = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/vehicle_brands', true);
    };
    ComponentSettingsProvider.prototype.getVehicleModels = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/vehicle_models', true);
    };
    ComponentSettingsProvider.prototype.getColors = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/colors', true);
    };
    ComponentSettingsProvider.prototype.getPaymentDivisions = function (mocked, receipt_id) {
        return this.getObservableForSuffix(mocked, true, "receipt/" + receipt_id + "/available_payment_divisions", true);
    };
    ComponentSettingsProvider.prototype.generateDocument = function (mocked, requestInfo) {
        return this.postObservableForSuffix(mocked, true, "documents/generate", true, null, requestInfo);
    };
    ComponentSettingsProvider.prototype.postCallMeRequest = function (mocked, data) {
        return this.postObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/call_requests", true, null, data);
    };
    ComponentSettingsProvider.prototype.postMailMeRequest = function (mocked, data) {
        return this.postObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/mail_requests", true, null, data);
    };
    ComponentSettingsProvider.prototype.postClaim = function (mocked, data) {
        return this.postObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/claims", true, null, data);
    };
    ComponentSettingsProvider.prototype.putClaim = function (mocked, data, claimId) {
        return this.putObservableForSuffix(mocked, false, "claim/" + claimId, true, null, data);
    };
    ComponentSettingsProvider.prototype.postOpenClaim = function (mocked, data) {
        return this.postObservableForSuffix(mocked, false, "open_operations/client/" + this.getClientId() + "/claims", true, null, data);
    };
    ComponentSettingsProvider.prototype.putOpenClaim = function (mocked, data, claimId) {
        return this.putObservableForSuffix(mocked, false, "open_operations/claim/" + claimId, true, null, data);
    };
    ComponentSettingsProvider.prototype.getClaimById = function (mocked, claimId) {
        return this.getObservableForSuffix(mocked, true, "claim/" + claimId, true, null);
    };
    ComponentSettingsProvider.prototype.submitAssistanceRequest = function (mocked, data, uriParams) {
        return this.postObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/assistances", true, null, data);
    };
    ComponentSettingsProvider.prototype.openSubmitAssistanceRequest = function (mocked, data, uriParams, userId) {
        return this.postObservableForSuffix(mocked, false, "open_operations/client/" + userId + "/assistances/", true, null, data);
    };
    ComponentSettingsProvider.prototype.submitPotentialClientRequest = function (mocked, data, budgetId) {
        return this.postObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/budget/" + budgetId + "/contract", true, null, data);
    };
    ComponentSettingsProvider.prototype.submitOpenPotentialClientRequest = function (mocked, data, budgetId) {
        return this.postObservableForSuffix(mocked, false, "open_operations/client/" + this.getClientId() + "/budget/" + budgetId + "/contract", true, null, data);
    };
    ComponentSettingsProvider.prototype.sendLoginRequest = function (mocked, data) {
        return this.postObservableForSuffix(mocked, true, 'authentications/login', true, null, data);
    };
    ComponentSettingsProvider.prototype.sendToolLoginRequest = function (data) {
        return this.http.post(this.connect.mapfreApiUrl + "/authentications/login", data);
    };
    ComponentSettingsProvider.prototype.getPolicyCancellationReasons = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "policy_cancellation_reasons/", true);
    };
    ComponentSettingsProvider.prototype.postAcceptPolicyRenewal = function (mocked, policyId) {
        return this.postObservableForSuffix(mocked, true, "policy/" + policyId + "/accept_renewal_proposal/", true, null, null);
    };
    ComponentSettingsProvider.prototype.postCancelReceipt = function (mocked, receiptId) {
        return this.postObservableForSuffix(mocked, true, "receipt/" + receiptId + "/cancel/", true, null, null);
    };
    ComponentSettingsProvider.prototype.postDelayReceipt = function (mocked, receiptId, data) {
        return this.postObservableForSuffix(mocked, true, "receipt/" + receiptId + "/delay/", true, null, data);
    };
    ComponentSettingsProvider.prototype.getClaimSpecificTypes = function (mocked, claimId) {
        return this.getObservableForSuffix(mocked, true, "catalogs/claim/" + claimId + "/claim_types/", true);
    };
    ComponentSettingsProvider.prototype.getClaimOrigins = function (mocked, claimTypeId) {
        return this.getObservableForSuffix(mocked, true, "catalogs/claim_type/" + claimTypeId + "/origins/", true);
    };
    ComponentSettingsProvider.prototype.getRepairmanTypes = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "catalogs/repairman_types/", true);
    };
    ComponentSettingsProvider.prototype.getDamagedElements = function (mocked, claimTypeId) {
        return this.getObservableForSuffix(mocked, true, "catalogs/claim_type/" + claimTypeId + "/damaged_elements/", true);
    };
    ComponentSettingsProvider.prototype.getDamageTypes = function (mocked, claimTypeId) {
        return this.getObservableForSuffix(mocked, true, "catalogs/claim_type/" + claimTypeId + "/damage_types/", true);
    };
    ComponentSettingsProvider.prototype.getDamageReasons = function (mocked, damageTypeId) {
        return this.getObservableForSuffix(mocked, true, "catalogs/damage_type/" + damageTypeId + "/damage_causes/", true);
    };
    ComponentSettingsProvider.prototype.getInformativeMessage = function (mocked, key) {
        return this.getObservableForSuffix(mocked, true, 'informative_messages', false, { key: key });
    };
    ComponentSettingsProvider.prototype.getAssistanceProfessionals = function (mocked, assistance_id) {
        return this.getObservableForSuffix(mocked, true, "assistance/" + assistance_id + "/professionals/", true);
    };
    ComponentSettingsProvider.prototype.getClientFavouritesProfessionals = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/professionals/", true);
    };
    ComponentSettingsProvider.prototype.getMapfreProfessionals = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "professionals/", false);
    };
    ComponentSettingsProvider.prototype.getProfessionalServices = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "catalogs/professional_services/", false);
    };
    ComponentSettingsProvider.prototype.getIconsNames = function () {
        return this.http.get('assets/fonts/mapfre-icons/src/mapfreIcons.json').map(function (response) {
            var iconNames = [];
            response.json().iconSets[0].selection.forEach(function (icon) {
                iconNames.push("mapfre-" + icon.name);
            });
            return iconNames;
        });
    };
    ComponentSettingsProvider.prototype.getBudgetById = function (mocked, budgetId) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/budget/" + budgetId, true);
    };
    ComponentSettingsProvider.prototype.getNearestOfficeSettings = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/contact/", true);
    };
    ComponentSettingsProvider.prototype.getTasksByBudgetSettings = function (mocked, budgetId) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/budget/" + budgetId + "/tasks", true);
    };
    ComponentSettingsProvider.prototype.getClaimsByid = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/claims/", true);
    };
    ComponentSettingsProvider.prototype.getRatingUrl = function (mocked, budgetId) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/budget/" + budgetId + "/rating", true);
    };
    ComponentSettingsProvider.decorators = [
        { type: Injectable },
    ];
    ComponentSettingsProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ApiProvider, },
    ]; };
    return ComponentSettingsProvider;
}(ApiProvider));
export { ComponentSettingsProvider };
//# sourceMappingURL=component-settings.js.map