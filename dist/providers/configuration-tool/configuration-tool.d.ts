import { Http } from '@angular/http';
import { PlatformProvider } from '../platform/platform';
import { ApiProvider } from '../api/api';
export declare class ConfigurationToolProvider {
    private http;
    private readonly platformProvider;
    private apiProvider;
    private baseUrl;
    constructor(http: Http, platformProvider: PlatformProvider, apiProvider: ApiProvider);
    getDynamicPageConfig(pageId: string): Promise<any>;
    getResponseData(response: any): boolean;
}
