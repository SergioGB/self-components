import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as _ from 'lodash';
import { PlatformProvider } from '../platform/platform';
import { ApiProvider } from '../api/api';
var ConfigurationToolProvider = (function () {
    function ConfigurationToolProvider(http, platformProvider, apiProvider) {
        this.http = http;
        this.platformProvider = platformProvider;
        this.apiProvider = apiProvider;
        this.baseUrl = apiProvider.connections.configurationApiUrl;
    }
    ConfigurationToolProvider.prototype.getDynamicPageConfig = function (pageId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.baseUrl + "/page/" + pageId + "?platform=" + _this.platformProvider.getPlatformForDisplay()).subscribe(function (response) {
                var pageData = _this.getResponseData(response);
                if (pageData) {
                    pageData.template = 'page';
                    resolve(pageData);
                }
                else {
                    reject();
                }
            });
        });
    };
    ConfigurationToolProvider.prototype.getResponseData = function (response) {
        return (response && typeof response.json === 'function' && !_.isNil(response.json())) ? response.json() : undefined;
    };
    ConfigurationToolProvider.decorators = [
        { type: Injectable },
    ];
    ConfigurationToolProvider.ctorParameters = function () { return [
        { type: Http, },
        { type: PlatformProvider, },
        { type: ApiProvider, },
    ]; };
    return ConfigurationToolProvider;
}());
export { ConfigurationToolProvider };
//# sourceMappingURL=configuration-tool.js.map