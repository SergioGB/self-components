import { Injectable } from '@angular/core';
var DateProvider = (function () {
    function DateProvider() {
    }
    DateProvider.prototype.getDatePlusDays = function (date, days) {
        return new Date(date.setDate(date.getDate() + days));
    };
    DateProvider.decorators = [
        { type: Injectable },
    ];
    DateProvider.ctorParameters = function () { return []; };
    return DateProvider;
}());
export { DateProvider };
//# sourceMappingURL=date.js.map