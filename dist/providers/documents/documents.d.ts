import { Injector } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { ApiProvider } from "../api/api";
import { TransferProvider } from "../transfer/transfer";
import { MapfreFile } from '../../models/mapfreFile';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
export declare class DocumentsProvider extends ApiProvider {
    protected injector: Injector;
    protected componentSettingsProvider: ComponentSettingsProvider;
    private transferProvider;
    constructor(injector: Injector, componentSettingsProvider: ComponentSettingsProvider, transferProvider: TransferProvider);
    uploadFiles(fileInput: any, outputArray: any[], allowPdf: boolean, allowImage: boolean, allowVideo: boolean, callback?: Function): void;
    uploadFile(file: File, outputArray: any[], allowPdf: boolean, allowImage: boolean, allowVideo: boolean, callback?: Function): void;
    downloadFile(mapfreFile: MapfreFile): void;
    downloadFileById(documentId: number, filename?: string): void;
    getFileById(documentId: number): Observable<Response>;
    postAdhoc(requestInfo: any): Observable<Response>;
    mapfreFileFromFile(file: File): MapfreFile;
    mapfreFilesFromFiles(files: File[]): MapfreFile[];
    bytesFromFile(file: File, callback: Function): void;
}
