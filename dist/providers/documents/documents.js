var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from "../api/api";
import { TransferProvider } from "../transfer/transfer";
import encoding from 'text-encoding';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
var DocumentsProvider = (function (_super) {
    __extends(DocumentsProvider, _super);
    function DocumentsProvider(injector, componentSettingsProvider, transferProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.componentSettingsProvider = componentSettingsProvider;
        _this.transferProvider = transferProvider;
        return _this;
    }
    DocumentsProvider.prototype.uploadFiles = function (fileInput, outputArray, allowPdf, allowImage, allowVideo, callback) {
        this.transferProvider.uploadFile(fileInput.target.files[0], outputArray, allowPdf, allowImage, allowVideo, callback);
    };
    DocumentsProvider.prototype.uploadFile = function (file, outputArray, allowPdf, allowImage, allowVideo, callback) {
        this.transferProvider.uploadFile(file, outputArray, allowPdf, allowImage, allowVideo, callback);
    };
    DocumentsProvider.prototype.downloadFile = function (mapfreFile) {
        if (mapfreFile.content) {
            this.transferProvider.presentDownloadingMessage();
            this.transferProvider.downloadFile(mapfreFile);
        }
        else {
            this.downloadFileById(mapfreFile.id);
        }
    };
    DocumentsProvider.prototype.downloadFileById = function (documentId, filename) {
        var _this = this;
        this.transferProvider.presentDownloadingMessage();
        this.getFileById(documentId).subscribe(function (response) {
            var mapfreResponse = _this.getResponseJSON(response);
            if (mapfreResponse) {
                _this.transferProvider.downloadFile(mapfreResponse.data);
            }
        });
    };
    DocumentsProvider.prototype.getFileById = function (documentId) {
        return this.getObservableForSuffix(false, true, "document/" + documentId, true);
    };
    DocumentsProvider.prototype.postAdhoc = function (requestInfo) {
        return this.componentSettingsProvider.generateDocument(false, requestInfo);
    };
    DocumentsProvider.prototype.mapfreFileFromFile = function (file) {
        var mapfreFile = {
            name: file.name
        };
        var reader = new FileReader();
        reader.readAsArrayBuffer(file);
        reader.onload = function () {
            mapfreFile.content = reader.result;
        };
        return mapfreFile;
    };
    DocumentsProvider.prototype.mapfreFilesFromFiles = function (files) {
        var _this = this;
        var mapfreFiles = [];
        files.forEach(function (file) {
            mapfreFiles.push(_this.mapfreFileFromFile(file));
        });
        return mapfreFiles;
    };
    DocumentsProvider.prototype.bytesFromFile = function (file, callback) {
        var reader = new FileReader();
        reader.onload = function (event) {
            var decoder = new encoding.TextDecoder("utf-8");
            var bytes = decoder.decode(new Uint8Array(this.result));
            if (callback && typeof callback === 'function') {
                callback(bytes);
            }
        };
        reader.readAsArrayBuffer(file);
    };
    DocumentsProvider.decorators = [
        { type: Injectable },
    ];
    DocumentsProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ComponentSettingsProvider, },
        { type: TransferProvider, },
    ]; };
    return DocumentsProvider;
}(ApiProvider));
export { DocumentsProvider };
//# sourceMappingURL=documents.js.map