import { EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
export declare class FileClaimsProvider {
    files: File[];
    fileUpdated: EventEmitter<any>;
    constructor();
    getFiles(): File[];
    setFiles(updateFile: File): void;
    deleteFiles(num: number): void;
}
