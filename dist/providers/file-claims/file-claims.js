import { Injectable, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
var FileClaimsProvider = (function () {
    function FileClaimsProvider() {
        this.fileUpdated = new EventEmitter();
        this.files = [];
    }
    FileClaimsProvider.prototype.getFiles = function () {
        return this.files;
    };
    FileClaimsProvider.prototype.setFiles = function (updateFile) {
        this.files.push(updateFile);
        this.fileUpdated.emit(this.files);
    };
    FileClaimsProvider.prototype.deleteFiles = function (num) {
        this.files.splice(num, 1);
        this.fileUpdated.emit(this.files);
    };
    FileClaimsProvider.decorators = [
        { type: Injectable },
    ];
    FileClaimsProvider.ctorParameters = function () { return []; };
    return FileClaimsProvider;
}());
export { FileClaimsProvider };
//# sourceMappingURL=file-claims.js.map