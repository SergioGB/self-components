import { EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
export declare class FileIncidencesProvider {
    files: File[];
    fileUpdated: EventEmitter<any>;
    constructor();
    getFiles(): File[];
    setFiles(updateFile: File): any;
    deleteFiles(num: number): any;
}
