import { Injectable, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
var FileIncidencesProvider = (function () {
    function FileIncidencesProvider() {
        this.fileUpdated = new EventEmitter();
        this.files = [];
    }
    FileIncidencesProvider.prototype.getFiles = function () {
        return this.files;
    };
    FileIncidencesProvider.prototype.setFiles = function (updateFile) {
        this.files.push(updateFile);
        this.fileUpdated.emit(this.files);
    };
    FileIncidencesProvider.prototype.deleteFiles = function (num) {
        this.files.splice(num, 1);
        this.fileUpdated.emit(this.files);
    };
    FileIncidencesProvider.decorators = [
        { type: Injectable },
    ];
    FileIncidencesProvider.ctorParameters = function () { return []; };
    return FileIncidencesProvider;
}());
export { FileIncidencesProvider };
//# sourceMappingURL=file-incidences.js.map