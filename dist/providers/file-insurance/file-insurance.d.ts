import { EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
import { TransferProvider } from "../transfer/transfer";
import { FileUtils } from "../utils/fileUtils";
export declare class FileInsuranceProvider {
    private transferProvider;
    private fileUtils;
    files: any[];
    fileUpdated: EventEmitter<any>;
    constructor(transferProvider: TransferProvider, fileUtils: FileUtils);
    getFiles(): File[];
    setFiles(updateFile: any): void;
    deleteFiles(num: number): void;
}
