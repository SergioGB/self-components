import { Injectable, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
import { TransferProvider } from "../transfer/transfer";
import { FileUtils } from "../utils/fileUtils";
var FileInsuranceProvider = (function () {
    function FileInsuranceProvider(transferProvider, fileUtils) {
        this.transferProvider = transferProvider;
        this.fileUtils = fileUtils;
        this.fileUpdated = new EventEmitter();
        this.files = [];
    }
    FileInsuranceProvider.prototype.getFiles = function () {
        return this.files;
    };
    FileInsuranceProvider.prototype.setFiles = function (updateFile) {
        var _this = this;
        if (updateFile !== undefined) {
            this.fileUtils.getBase64(updateFile).then(function (data) {
                _this.files.push({ title: updateFile.name, content: data.replace('data:application/pdf;base64,', '') });
                _this.fileUpdated.emit(_this.files);
            });
        }
    };
    FileInsuranceProvider.prototype.deleteFiles = function (num) {
        this.files.splice(num, 1);
        this.fileUpdated.emit(this.files);
    };
    FileInsuranceProvider.decorators = [
        { type: Injectable },
    ];
    FileInsuranceProvider.ctorParameters = function () { return [
        { type: TransferProvider, },
        { type: FileUtils, },
    ]; };
    return FileInsuranceProvider;
}());
export { FileInsuranceProvider };
//# sourceMappingURL=file-insurance.js.map