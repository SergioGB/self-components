import { Injectable } from '@angular/core';
import { TranslationProvider } from '../translation/translation';
import { StepsProvider } from '../steps/steps';
var FlowProvider = (function () {
    function FlowProvider(translationProvider, stepsProvider) {
        this.translationProvider = translationProvider;
        this.stepsProvider = stepsProvider;
        this.flowViewsNames = [];
        this.initializeData();
    }
    FlowProvider.prototype.setNavParams = function (navParams) {
        this.navParams = navParams;
        this.steps.forEach(function (step) {
            step.pageParams = navParams;
        });
    };
    FlowProvider.prototype.getSteps = function () {
        return this.steps;
    };
    FlowProvider.prototype.setSteps = function (steps) {
        this.steps = steps;
    };
    FlowProvider.prototype.getResult = function () {
        return this.result;
    };
    FlowProvider.prototype.setResult = function (result) {
        this.result = result;
    };
    FlowProvider.prototype.getFlowViewsNames = function () {
        return this.flowViewsNames;
    };
    FlowProvider.prototype.setFlowViewsNames = function (flowViewsNames) {
        this.flowViewsNames = flowViewsNames;
    };
    FlowProvider.prototype.getStepViewNames = function () {
        var viewNames = [];
        this.steps.forEach(function (step) {
            viewNames.push(step.pageName);
        });
        return viewNames;
    };
    FlowProvider.prototype.getCurrentStepIndex = function () {
        return this.currentStepIndex;
    };
    FlowProvider.prototype.setCurrentStepIndex = function (currentStepIndex) {
        this.currentStepIndex = currentStepIndex;
        this.stepsProvider.updateSteps(this.steps, currentStepIndex + 1);
    };
    FlowProvider.prototype.stepBack = function (navController) {
        navController.pop({ animate: false });
    };
    FlowProvider.prototype.stepForward = function (navController) {
        var newStep;
        for (var i = this.getCurrentStepIndex() + 1; i < this.steps.length; i++) {
            var step = this.steps[i];
            if (!step.hidden) {
                newStep = step;
                break;
            }
        }
        if (newStep) {
            navController.push(newStep.pageName, newStep.pageParams || {}, { animate: false });
        }
    };
    FlowProvider.prototype.getPreviousStep = function () {
        var previousStepIndex = this.currentStepIndex - 1;
        return previousStepIndex >= 0 ? this.steps[previousStepIndex] : undefined;
    };
    FlowProvider.prototype.getCurrentStep = function () {
        return this.steps[this.currentStepIndex];
    };
    FlowProvider.prototype.getNextStep = function () {
        var nextStepIndex = this.currentStepIndex + 1;
        return nextStepIndex < this.steps.length ? this.steps[nextStepIndex] : undefined;
    };
    FlowProvider.prototype.hasDataForThePreviousSteps = function () {
        return this.getCurrentStepIndexBasedOnTheCurrentData() >= this.currentStepIndex;
    };
    FlowProvider.prototype.getCurrentStepBasedOnTheCurrentData = function () {
        return this.steps[this.getCurrentStepIndexBasedOnTheCurrentData()];
    };
    FlowProvider.prototype.redirectToCurrentStepBasedOnTheCurrentData = function (navController) {
        var newStep = this.getCurrentStepBasedOnTheCurrentData();
        if (newStep) {
            navController.push(newStep.pageName, newStep.pageParams || {}, { animate: false });
        }
        else {
            navController.setRoot('GlobalPositionPage', {}, { animate: false });
        }
    };
    FlowProvider.prototype.updateFlowProgress = function (navController) {
        var previousView = navController.last().instance;
        if (previousView && previousView.name && this.flowViewsNames.indexOf(previousView.name) < 0) {
            this.initializeData();
        }
    };
    FlowProvider.prototype.initializeSteps = function () {
        if (this.steps) {
            this.steps.forEach(function (step) {
                step.completed = false;
                step.active = false;
                step.visitable = false;
                step.clickable = false;
                step.subtitle = undefined;
            });
        }
    };
    FlowProvider.prototype.initializeData = function () { };
    FlowProvider.prototype.initializeFormData = function () { };
    FlowProvider.prototype.getCurrentStepIndexBasedOnTheCurrentData = function () { return -1; };
    FlowProvider.decorators = [
        { type: Injectable },
    ];
    FlowProvider.ctorParameters = function () { return [
        { type: TranslationProvider, },
        { type: StepsProvider, },
    ]; };
    return FlowProvider;
}());
export { FlowProvider };
//# sourceMappingURL=flow.js.map