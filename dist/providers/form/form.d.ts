import { FormGroup } from '@angular/forms';
export declare class FormProvider {
    constructor();
    mapFormData(formData: any, formGroup: FormGroup): void;
    mapListItem(fieldsList: any[], fieldName: string, formGroup: FormGroup): void;
    getListEntryById(fieldsList: any[], id: any): any;
}
