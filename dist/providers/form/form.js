import { Injectable } from '@angular/core';
var FormProvider = (function () {
    function FormProvider() {
    }
    FormProvider.prototype.mapFormData = function (formData, formGroup) {
        if (formData) {
            Object.keys(formGroup.value).forEach(function (fieldName) {
                var fieldData = formData[fieldName];
                if (fieldData) {
                    formGroup.controls[fieldName].patchValue(fieldData);
                }
            });
        }
    };
    FormProvider.prototype.mapListItem = function (fieldsList, fieldName, formGroup) {
        var fieldData = formGroup.value[fieldName];
        if (fieldData) {
            formGroup.controls[fieldName].patchValue(this.getListEntryById(fieldsList, fieldData.id));
        }
    };
    FormProvider.prototype.getListEntryById = function (fieldsList, id) {
        var field;
        if (fieldsList && fieldsList.length && id) {
            var matches = fieldsList.filter(function (field) { return field.id === id; });
            if (matches) {
                field = matches[0];
            }
        }
        return field;
    };
    FormProvider.decorators = [
        { type: Injectable },
    ];
    FormProvider.ctorParameters = function () { return []; };
    return FormProvider;
}());
export { FormProvider };
//# sourceMappingURL=form.js.map