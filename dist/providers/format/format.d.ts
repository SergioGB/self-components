import { CurrencyPipe } from '@angular/common';
import { Cost } from '../../models/cost';
import { Address } from '../../models/address';
import { PaymentMethod } from '../../models/payment-method';
export declare class FormatProvider {
    private currencyPipe;
    constructor(currencyPipe: CurrencyPipe);
    formatDate(date: any): string;
    formatTime(date: any): string;
    formatDateByPattern(date: any, pattern: string): string;
    formatCost(cost: Cost, divisor?: number): string;
    formatCostInt(cost: Cost): number;
    formatCurrency(cost: Cost): string;
    formatAddress(originalAddress: Address): string;
    formatPaymentMethodNumber(paymentMethod: PaymentMethod): string;
    private hasLength;
}
