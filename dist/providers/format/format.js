import { Injectable } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import * as moment from 'moment';
import * as _ from 'lodash';
import { Utils } from '../utils/utils';
import { ConfigProvider } from '../../models/self-component.config';
var FormatProvider = (function () {
    function FormatProvider(currencyPipe) {
        this.currencyPipe = currencyPipe;
    }
    FormatProvider.prototype.formatDate = function (date) {
        return date ? moment(moment(date).utc()).local().format(ConfigProvider.config.datePattern) : undefined;
    };
    FormatProvider.prototype.formatTime = function (date) {
        return date ? moment(moment(date).utc()).local().format('HH:mm') : undefined;
    };
    FormatProvider.prototype.formatDateByPattern = function (date, pattern) {
        return date ? moment(moment(date).utc()).local().format(pattern) : undefined;
    };
    FormatProvider.prototype.formatCost = function (cost, divisor) {
        if (!divisor) {
            divisor = 1;
        }
        return (cost && cost.amount) ? (cost.amount / divisor).toFixed(2).replace(/\./, ',') + "\u20AC" : undefined;
    };
    FormatProvider.prototype.formatCostInt = function (cost) {
        return (cost && cost.amount) ? cost.amount : undefined;
    };
    FormatProvider.prototype.formatCurrency = function (cost) {
        return cost ? this.currencyPipe.transform(cost.amount, cost.iso_code, true, '1.2-2') : undefined;
    };
    FormatProvider.prototype.formatAddress = function (originalAddress) {
        var addressAsString = '';
        if (originalAddress) {
            var address = Utils.copy(originalAddress);
            address.province_name = address.province_name || address.county_name;
            address.cp = address.cp || address.postal_code || address.zip_code;
            addressAsString += "" + (this.hasLength(address.road_type_name) ? address.road_type_name : '') + (this.hasLength(address.road_type_name) && this.hasLength(address.road_name) ? ' ' : '') + (this.hasLength(address.road_name) ? address.road_name : '') + (this.hasLength(address.road_name) && this.hasLength(address.road_number) ? ' ' : '') + (this.hasLength(address.road_number) ? address.road_number : '');
            var listSeparatedFields = [];
            if (this.hasLength(address.direction)) {
                listSeparatedFields.push(address.direction);
            }
            if (this.hasLength(address.cp)) {
                listSeparatedFields.push(address.cp);
            }
            if (this.hasLength(address.city_name)) {
                listSeparatedFields.push(address.city_name);
            }
            if (this.hasLength(address.province_name)) {
                listSeparatedFields.push(address.province_name);
            }
            if (this.hasLength(address.country_name)) {
                listSeparatedFields.push(address.country_name);
            }
            if (listSeparatedFields.length) {
                if (this.hasLength(address.road_type_name) || this.hasLength(address.road_name)) {
                    addressAsString += ', ';
                }
                addressAsString += "" + listSeparatedFields.join(', ');
            }
        }
        return addressAsString;
    };
    FormatProvider.prototype.formatPaymentMethodNumber = function (paymentMethod) {
        if (paymentMethod.credit_card_info) {
            var paymentMethodNumber = paymentMethod.credit_card_info.card_number;
            return _.isEmpty(paymentMethodNumber) ? undefined : "**** **** **** " + paymentMethodNumber.substring(15, 19);
        }
        else {
            var paymentMethodNumber = paymentMethod.type === 1 ? (paymentMethod.credit_card_info && paymentMethod.credit_card_info.card_number ? paymentMethod.credit_card_info.card_number : undefined) :
                (paymentMethod.bank_account_info && paymentMethod.bank_account_info.account_number ? paymentMethod.bank_account_info.account_number : undefined);
            return _.isEmpty(paymentMethodNumber) ? undefined : "ES** **** **** **** **** " + paymentMethodNumber.substring(15, 19);
        }
    };
    FormatProvider.prototype.hasLength = function (string) {
        return !_.isNil(string) && string.length > 0;
    };
    FormatProvider.decorators = [
        { type: Injectable },
    ];
    FormatProvider.ctorParameters = function () { return [
        { type: CurrencyPipe, },
    ]; };
    return FormatProvider;
}());
export { FormatProvider };
//# sourceMappingURL=format.js.map