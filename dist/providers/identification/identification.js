var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from '@angular/core';
import { Events, Platform } from 'ionic-angular';
import { FingerprintAIO } from "@ionic-native/fingerprint-aio";
import { PlatformProvider } from '../platform/platform';
import { Storage } from '@ionic/storage';
import { TranslationProvider } from '../translation/translation';
import { ConfigProvider } from '../../models/self-component.config';
var IdentificationProvider = (function () {
    function IdentificationProvider(platform, platformProvider, fingerprintAIO, translationProvider, events, storage) {
        this.platform = platform;
        this.platformProvider = platformProvider;
        this.fingerprintAIO = fingerprintAIO;
        this.translationProvider = translationProvider;
        this.events = events;
        this.storage = storage;
        this.fingerprintOptions = {
            clientId: 'autoservicio',
            clientSecret: 'password',
            disableBackup: true
        };
        this.activateFingerprint = false;
    }
    IdentificationProvider.prototype.checkFingerprintAvailability = function (onSuccess, onError) {
        var _this = this;
        this.platform.ready().then(function () {
            _this.checkFingerprintAIOAvailability(onSuccess, onError);
        });
    };
    IdentificationProvider.prototype.checkFingerprintAIOAvailability = function (onSuccess, onError) {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.platformProvider.onCordova()) return [3, 5];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4, this.fingerprintAIO.isAvailable().then(function (response) {
                                _this.fingerprintAvailable = response.indexOf('finger') >= 0 || response.indexOf('face') >= 0;
                                _this.identificationType = response;
                                _this.fingerprintOptions.clientId = _this.identificationType === 'finger' ?
                                    _this.translationProvider.getValueForKey('identification.touch_id.message_access') :
                                    _this.translationProvider.getValueForKey('identification.touch_id.message_identification');
                                if (typeof onSuccess === 'function') {
                                    onSuccess(_this.fingerprintAvailable);
                                }
                            }).catch(function (error) {
                                _this.outputError(error);
                                if (typeof onError === 'function') {
                                    onError(error);
                                }
                            })];
                    case 2:
                        _a.sent();
                        return [3, 4];
                    case 3:
                        error_1 = _a.sent();
                        this.outputError(error_1);
                        if (typeof onError === 'function') {
                            onError(error_1);
                        }
                        return [3, 4];
                    case 4: return [3, 6];
                    case 5:
                        if (typeof onSuccess === 'function') {
                            onSuccess(false);
                        }
                        _a.label = 6;
                    case 6: return [2];
                }
            });
        });
    };
    IdentificationProvider.prototype.getIdentificationType = function () {
        return this.identificationType;
    };
    IdentificationProvider.prototype.setSecurityPin = function (pinValue) {
        return this.storage.set(ConfigProvider.config.securityPinId, pinValue);
    };
    IdentificationProvider.prototype.getSecurityPin = function () {
        return this.storage.get(ConfigProvider.config.securityPinId);
    };
    IdentificationProvider.prototype.setFingerprintEnabled = function (fingerprintValue) {
        return this.storage.set(ConfigProvider.config.fingerprintEnabledId, fingerprintValue);
    };
    IdentificationProvider.prototype.isFingerprintEnabled = function () {
        return this.storage.get(ConfigProvider.config.fingerprintEnabledId);
    };
    IdentificationProvider.prototype.showFingerprintDialog = function (onSuccess, onCancellation, onError) {
        var _this = this;
        this.displayFingerprintId(function () {
            if (typeof onSuccess === 'function') {
                onSuccess(_this.fingerprintAvailable);
            }
        }, function () {
            if (typeof onCancellation === 'function') {
                onCancellation();
            }
        }, function (error) {
            if (typeof onError === 'function') {
                onError(error);
            }
        });
    };
    IdentificationProvider.prototype.displayFingerprintId = function (onSuccess, onCancellation, onError) {
        var _this = this;
        if (this.platformProvider.onCordova() && this.fingerprintAvailable) {
            try {
                this.fingerprintAIO.show(this.fingerprintOptions).then(function (response) {
                    if (typeof onSuccess === 'function') {
                        onSuccess(_this.fingerprintAvailable);
                    }
                }).catch(function (error) {
                    var cancelledByUser = error.indexOf('Canceled by user') >= 0;
                    if (cancelledByUser) {
                        if (typeof onCancellation === 'function') {
                            onCancellation();
                        }
                    }
                    else {
                        _this.outputError(error);
                        if (typeof onError === 'function') {
                            onError(error);
                        }
                    }
                });
            }
            catch (error) {
                this.outputError(error);
                if (typeof onError === 'function') {
                    onError(error);
                }
            }
        }
    };
    IdentificationProvider.prototype.outputError = function (error) {
        var notEnabled = error.indexOf('No identities') >= 0 ||
            error.indexOf('Must have android.permission') >= 0 ||
            error.indexOf('not ready') >= 0;
        var noAttemptsLeft = error.indexOf('Application retry limit exceeded') >= 0;
        if (notEnabled) {
        }
        else if (noAttemptsLeft) {
            this.events.publish('FingerPrintRanOutOfAttempts');
        }
        else {
            window.console.error("Fingerprint ERROR: " + this.responseToString(error));
        }
    };
    IdentificationProvider.prototype.responseToString = function (response) {
        return JSON.stringify(response, null, 2);
    };
    IdentificationProvider.prototype.setApiKey = function (apiKey) {
        console.log("Saving API Key: " + apiKey);
        return this.storage.set(ConfigProvider.config.apiKeyID, apiKey);
    };
    IdentificationProvider.prototype.getApiKey = function () {
        return this.storage.get(ConfigProvider.config.apiKeyID);
    };
    IdentificationProvider.decorators = [
        { type: Injectable },
    ];
    IdentificationProvider.ctorParameters = function () { return [
        { type: Platform, },
        { type: PlatformProvider, },
        { type: FingerprintAIO, },
        { type: TranslationProvider, },
        { type: Events, },
        { type: Storage, },
    ]; };
    return IdentificationProvider;
}());
export { IdentificationProvider };
//# sourceMappingURL=identification.js.map