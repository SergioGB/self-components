import { Injectable } from '@angular/core';
import { SinisterInfo } from "../../models/incidence/sinisterInfo";
import { Utils } from '../utils/utils';
var IncidenceProvider = (function () {
    function IncidenceProvider() {
        this.initializeIncident();
    }
    IncidenceProvider.prototype.initializeIncident = function () {
        this.incidence = {
            type: {
                parameter_id: 1,
                id: "1",
                title: "Con otros vehículos involucrados",
                description: "Con otros vehículos involucrados",
                tooltip: null,
                subparameters: {
                    group_title: "Número de vehículos involucrados",
                    parameters: [
                        {
                            id: "11",
                            title: "Uno",
                            description: "Uno",
                            tooltip: null,
                            subparameters: null
                        },
                        {
                            id: "12",
                            title: "Más de uno",
                            description: "Más de uno",
                            tooltip: null,
                            subparameters: null
                        }
                    ]
                }
            },
            sinister_adjuster: true,
            vehicleCount: {
                parameter_id: 1
            },
            injured: true,
            injured_detail: {},
            sinister_info: new SinisterInfo(),
            damages: {}
        };
    };
    IncidenceProvider.prototype.getIncidence = function () {
        return this.incidence;
    };
    IncidenceProvider.prototype.getIncidenceAsString = function () {
        return JSON.stringify(this.incidence, null, 2);
    };
    IncidenceProvider.prototype.setIncidenceType = function (type) {
        this.incidence.type = type;
    };
    IncidenceProvider.prototype.setIncidenceSubType = function (subtype) {
        this.incidence.subtype = subtype;
    };
    IncidenceProvider.prototype.setIncidenceVehicleCount = function (vehicleCount) {
        this.incidence.vehicleCount = vehicleCount;
    };
    IncidenceProvider.prototype.setIncidenceInjuries = function (injured_detail) {
        this.incidence.injured_detail = injured_detail;
    };
    IncidenceProvider.prototype.setWhen = function (when) {
        this.incidence.when = when;
        if (when.date && when.time) {
            this.incidence.when.dateTime =
                new Date(new Date(when.date.setHours(Utils.getTimeStringHours(when.time))).setMinutes(Utils.getTimeStringMinutes(when.time)));
        }
    };
    IncidenceProvider.prototype.getWhen = function () {
        return this.incidence ? this.incidence.when : undefined;
    };
    IncidenceProvider.prototype.getDate = function () {
        return this.getWhen() ? this.getWhen().date : undefined;
    };
    IncidenceProvider.prototype.getTime = function () {
        return this.getWhen() ? this.getWhen().time : undefined;
    };
    IncidenceProvider.prototype.getDateTime = function () {
        return this.getWhen() ? this.getWhen().dateTime : undefined;
    };
    IncidenceProvider.prototype.setWhere = function (where) {
        this.incidence.where = where;
    };
    IncidenceProvider.prototype.getWhere = function () {
        return this.incidence.where;
    };
    IncidenceProvider.prototype.setHowOwnVehicle = function (sinisterVehicleInfo) {
        this.incidence.sinister_info = this.incidence.sinister_info || new SinisterInfo();
        this.incidence.sinister_info.how_own_vehicle = sinisterVehicleInfo;
    };
    IncidenceProvider.prototype.getHowOwnVehicle = function () {
        return this.incidence.sinister_info.how_own_vehicle;
    };
    IncidenceProvider.prototype.setHowOtherVehicles = function (sinisterVehicleInfoList) {
        this.incidence.sinister_info = this.incidence.sinister_info || new SinisterInfo();
        this.incidence.sinister_info.how_other_vehicles = sinisterVehicleInfoList;
    };
    IncidenceProvider.prototype.getHowOtherVehicles = function () {
        return this.incidence.sinister_info.how_other_vehicles;
    };
    IncidenceProvider.prototype.setOwnDamages = function (own_damages) {
        this.incidence.damages.own_damages = own_damages;
    };
    IncidenceProvider.prototype.getOwnDamages = function () {
        return this.incidence.damages.own_damages;
    };
    IncidenceProvider.prototype.setOthersDamages = function (others_damages) {
        this.incidence.damages.others_damages = others_damages;
    };
    IncidenceProvider.prototype.getOthersDamages = function () {
        return this.incidence.damages.others_damages;
    };
    IncidenceProvider.prototype.setOtherDamages = function (other_damages) {
        this.incidence.damages.other_damages = other_damages;
    };
    IncidenceProvider.prototype.getOtherDamages = function () {
        return this.incidence.damages.other_damages;
    };
    IncidenceProvider.prototype.setOtherParticipants = function (other_participants) {
        this.incidence.damages.other_participants = other_participants;
    };
    IncidenceProvider.prototype.getOtherParticipants = function () {
        return this.incidence.damages.other_participants;
    };
    IncidenceProvider.decorators = [
        { type: Injectable },
    ];
    IncidenceProvider.ctorParameters = function () { return []; };
    return IncidenceProvider;
}());
export { IncidenceProvider };
//# sourceMappingURL=incidence.js.map