import { Injector } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ApiProvider } from '../api/api';
export declare class IncidencesAndClaimsProvider extends ApiProvider {
    protected injector: Injector;
    constructor(injector: Injector);
    getFormTypeCards(mocked: boolean): Observable<Response>;
    getIncidenceReasons(mocked: boolean): Observable<Response>;
    getClaimReasons(mocked: boolean): Observable<Response>;
    getClaimDocuments(mocked: boolean): Observable<Response>;
    postDocuments(mocked: boolean, documents: any): Observable<Response>;
    postIncidenceForm(mocked: boolean, incidenceFormData: any): Observable<Response>;
    postClaimForm(mocked: boolean, claimFormData: any): Observable<Response>;
}
