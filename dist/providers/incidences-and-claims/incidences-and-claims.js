var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
var IncidencesAndClaimsProvider = (function (_super) {
    __extends(IncidencesAndClaimsProvider, _super);
    function IncidencesAndClaimsProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        return _this;
    }
    IncidencesAndClaimsProvider.prototype.getFormTypeCards = function (mocked) {
        return this.getObservableForSuffix(mocked, false, 'loss_types/', true);
    };
    IncidencesAndClaimsProvider.prototype.getIncidenceReasons = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/loss_reasons/', true);
    };
    IncidencesAndClaimsProvider.prototype.getClaimReasons = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'catalogs/complaint_reasons/', true);
    };
    IncidencesAndClaimsProvider.prototype.getClaimDocuments = function (mocked) {
        return this.getObservableForSuffix(mocked, true, 'complaint_documents/', true);
    };
    IncidencesAndClaimsProvider.prototype.postDocuments = function (mocked, documents) {
        return this.postObservableForSuffix(mocked, true, "documents/", true, null, documents);
    };
    IncidencesAndClaimsProvider.prototype.postIncidenceForm = function (mocked, incidenceFormData) {
        return this.postObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/losses/", true, null, incidenceFormData);
    };
    IncidencesAndClaimsProvider.prototype.postClaimForm = function (mocked, claimFormData) {
        return this.postObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/complaints/", true, null, claimFormData);
    };
    IncidencesAndClaimsProvider.decorators = [
        { type: Injectable },
    ];
    IncidencesAndClaimsProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return IncidencesAndClaimsProvider;
}(ApiProvider));
export { IncidencesAndClaimsProvider };
//# sourceMappingURL=incidences-and-claims.js.map