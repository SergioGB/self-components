import { ModalController } from 'ionic-angular';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
export declare class InformationProvider {
    private componentSettingsProvider;
    constructor(componentSettingsProvider: ComponentSettingsProvider);
    showPrivacyModal(modalController: ModalController, mocked?: boolean): void;
    showConditionsModal(modalController: ModalController, mocked?: boolean): void;
    presentPrivacyModal(modalController: ModalController, data: any): void;
    presentConditionsModal(modalController: ModalController, data: any): void;
}
