import { Injectable } from '@angular/core';
import { Mocks } from '../mocks/mocks';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
import { HtmlContentModalComponent } from '../../components/_modals/html-content-modal/html-content-modal';
var InformationProvider = (function () {
    function InformationProvider(componentSettingsProvider) {
        this.componentSettingsProvider = componentSettingsProvider;
    }
    InformationProvider.prototype.showPrivacyModal = function (modalController, mocked) {
        var _this = this;
        if (mocked) {
            this.presentPrivacyModal(modalController, Mocks.getInformativeMessage()[0]);
        }
        else {
            this.componentSettingsProvider.getInformativeMessage(mocked, 'PRIVACITY').subscribe(function (response) {
                var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                    var data = mapfreResponse.data[0];
                    _this.presentPrivacyModal(modalController, data);
                }
            });
        }
    };
    InformationProvider.prototype.showConditionsModal = function (modalController, mocked) {
        var _this = this;
        if (mocked) {
            this.presentConditionsModal(modalController, Mocks.getInformativeMessage()[1]);
        }
        else {
            this.componentSettingsProvider.getInformativeMessage(mocked, 'TERMS_OF_USE').subscribe(function (response) {
                var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
                if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                    var data = mapfreResponse.data[0];
                    _this.presentConditionsModal(modalController, data);
                }
            });
        }
    };
    InformationProvider.prototype.presentPrivacyModal = function (modalController, data) {
        modalController.create(HtmlContentModalComponent, { title: data.title, description: data.description }).present();
    };
    InformationProvider.prototype.presentConditionsModal = function (modalController, data) {
        modalController.create(HtmlContentModalComponent, { title: data.title, description: data.description }).present();
    };
    InformationProvider.decorators = [
        { type: Injectable },
    ];
    InformationProvider.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
    ]; };
    return InformationProvider;
}());
export { InformationProvider };
//# sourceMappingURL=information.js.map