var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import * as _ from 'lodash';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
import { ConfigProvider } from '../../models/self-component.config';
var InsuranceUpgradeProvider = (function (_super) {
    __extends(InsuranceUpgradeProvider, _super);
    function InsuranceUpgradeProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.mocked = false;
        _this.initializeFlowData();
        return _this;
    }
    InsuranceUpgradeProvider.prototype.getInsurance = function () {
        return this.insurance;
    };
    InsuranceUpgradeProvider.prototype.setInsurance = function (value) {
        this.insurance = value;
    };
    InsuranceUpgradeProvider.prototype.getInsuranceType = function () {
        return this.insuranceType;
    };
    InsuranceUpgradeProvider.prototype.setInsuranceType = function (value) {
        this.insuranceType = value;
    };
    InsuranceUpgradeProvider.prototype.getPaymentMethod = function () {
        return this.paymentMethod;
    };
    InsuranceUpgradeProvider.prototype.setPaymentMethod = function (value) {
        this.paymentMethod = value;
    };
    InsuranceUpgradeProvider.prototype.getNewCoverages = function () {
        return this.newCoverages;
    };
    InsuranceUpgradeProvider.prototype.setNewCoverages = function (newCoverages) {
        this.newCoverages = newCoverages;
    };
    InsuranceUpgradeProvider.prototype.setCurrentStepIndex = function (currentStepIndex) {
        this.currentStepIndex = currentStepIndex;
    };
    InsuranceUpgradeProvider.prototype.setResultTypeById = function (resultTypeId) {
        var resultTypeMatches = _.filter(this.resultTypes, function (resultType) {
            return resultType.id === resultTypeId;
        });
        this.result = resultTypeMatches.length ? resultTypeMatches[0] : undefined;
    };
    InsuranceUpgradeProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.insurance = undefined;
        this.insuranceType = undefined;
        this.paymentMethod = undefined;
        this.result = undefined;
    };
    InsuranceUpgradeProvider.prototype.getSummaryData = function () {
        var summary;
        if (this.insurance && this.insuranceType) {
            var optionalCoverageNames = [];
            if (this.insuranceType.optionalCoverages) {
                for (var _i = 0, _a = this.insuranceType.optionalCoverages; _i < _a.length; _i++) {
                    var optionalCoverage = _a[_i];
                    if (optionalCoverage.selected) {
                        optionalCoverageNames.push(optionalCoverage.title);
                    }
                }
            }
            var coverageNames = [];
            if (this.newCoverages) {
                for (var _b = 0, _c = this.newCoverages; _b < _c.length; _b++) {
                    var coverage = _c[_b];
                    coverageNames.push(coverage.title);
                }
            }
            summary = {
                title: 'upgrade_insurance.summary.title',
                sections: [{
                        title: 'upgrade_insurance.summary.risk_selection',
                        rows: [{
                                key: 'upgrade_insurance.summary.risk_selection.risk',
                                value: this.insurance.name
                            }, {
                                key: 'upgrade_insurance.summary.risk_selection.policy',
                                value: this.insurance.associated_policies[0].type
                            }]
                    }, {
                        title: 'upgrade_insurance.summary.upgrade_insurance',
                        rows: [{
                                key: 'upgrade_insurance.summary.upgrade_insurance.type',
                                value: this.insuranceType.title
                            }, {
                                key: 'upgrade_insurance.summary.upgrade_insurance.coverage',
                                value: optionalCoverageNames.length > 0 ? optionalCoverageNames : ConfigProvider.config.emptyField
                            }, {
                                key: 'upgrade_insurance.summary.upgrade_insurance.payment_modality',
                                value: this.insuranceType.paymentFrequency
                            }, {
                                key: 'upgrade_insurance.summary.upgrade_insurance.amount',
                                value: this.formatProvider.formatCost(this.insuranceType.totalPrice)
                            }]
                    }, {
                        title: 'upgrade_insurance.summary.coverages_list',
                        toggle: true,
                        rows: [{
                                value: coverageNames
                            }]
                    }]
            };
            if (this.paymentMethod && this.paymentMethod.credit_card_info) {
                summary.sections.push({
                    title: 'upgrade_insurance.summary.payment_method',
                    rows: [{
                            key: 'upgrade_insurance.summary.payment_method.card_number',
                            value: this.paymentMethod.credit_card_info.card_number
                        }, {
                            key: 'upgrade_insurance.summary.payment_method.expiration_date',
                            value: this.formatProvider.formatDate(this.paymentMethod.credit_card_info.expiration_date)
                        }]
                });
            }
            else if (this.paymentMethod && this.paymentMethod.bank_account_info) {
                summary.sections.push({
                    title: 'upgrade_insurance.summary.payment_method',
                    rows: [{
                            key: 'upgrade_insurance.summary.payment_method.account_number',
                            value: this.paymentMethod.bank_account_info.account_number
                        }, {
                            key: 'upgrade_insurance.summary.payment_method.bank_entity',
                            value: this.paymentMethod.bank_account_info.entity
                        }]
                });
            }
            summary.sections.push({
                rows: [{
                        accept_conditions: {
                            text1: 'upgrade_insurance.summary.privacy_check1',
                            link1: 'upgrade_insurance.summary.privacy_check2',
                            text2: 'upgrade_insurance.summary.privacy_check3',
                            insurance: this.insuranceType.title
                        }
                    }]
            });
        }
        return summary;
    };
    InsuranceUpgradeProvider.prototype.getRequestJson = function (formattedRequest) {
        formattedRequest.step_data = {
            policy_type_id: this.getInsurance() ? this.getInsurance().associated_policies[0].id : null,
            additional_coverages_ids: this.getNewCoverages() && this.getNewCoverages().length > 0 ? this.getNewCoverages() : null,
            payment_method_id: this.getPaymentMethod() ? this.getPaymentMethod().id : null
        };
        return formattedRequest;
    };
    InsuranceUpgradeProvider.prototype.updateRequest = function (formattedRequest, callback, mocked) {
        var _this = this;
        this.apiProvider.postObservableForSuffix(mocked, false, "policy/" + this.getInsurance().associated_policies[0].id + "/improve", true, {}, this.getRequestJson(formattedRequest)).subscribe(function (response) {
            _this.onRequestPut(response, callback);
        });
    };
    InsuranceUpgradeProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'UpgradeInsuranceStepsPage';
        this.resultPageName = 'UpgradeInsuranceResultPage';
        this.resetFlowData();
        this.resultTypes = [{
                id: 1,
                description: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.result.result_type1')
            }, {
                id: 2,
                description: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.result.result_type2')
            }, {
                id: 3,
                description: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.result.result_type1')
            }];
        if (this.mocked) {
            this.mockData();
        }
    };
    InsuranceUpgradeProvider.prototype.mockData = function () {
        this.newCoverages = [
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage1') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage2') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage3') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage4') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage5') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage6') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage7') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage8') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage9') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage10') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage11') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage12') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage13') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage14') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage15') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage16') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage17') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage18') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage19') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage20') },
            { title: this.translationProvider.getValueForKey('insurance-upgrade.upgrade_insurance.summary.coverage21') }
        ];
    };
    InsuranceUpgradeProvider.decorators = [
        { type: Injectable },
    ];
    InsuranceUpgradeProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return InsuranceUpgradeProvider;
}(AbstractFlowProvider));
export { InsuranceUpgradeProvider };
//# sourceMappingURL=insurance-upgrade.js.map