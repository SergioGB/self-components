var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
import 'rxjs/add/operator/toPromise';
var InsuranceProvider = (function (_super) {
    __extends(InsuranceProvider, _super);
    function InsuranceProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        return _this;
    }
    InsuranceProvider.prototype.getInsuranceDetailSettings = function (mocked, risk_id) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/risk/" + risk_id, true);
    };
    InsuranceProvider.prototype.getInsuranceSettings = function (mocked, client_id) {
        return this.getObservableForSuffix(mocked, false, "client/" + client_id + "/risks/", true);
    };
    InsuranceProvider.prototype.getPolicyDetailSettings = function (mocked, policy_id) {
        return this.getObservableForSuffix(mocked, false, "policy/" + policy_id, true);
    };
    InsuranceProvider.prototype.getPoliciesSettings = function (mocked, client_id) {
        return this.getObservableForSuffix(mocked, false, "client/" + client_id + "/policies", true);
    };
    InsuranceProvider.prototype.updatePolicyDetailSettings = function (mocked, policy_id, policyData) {
        return this.putObservableForSuffix(mocked, false, "policy/" + policy_id, false, {}, policyData);
    };
    InsuranceProvider.prototype.updatePolicyDataDetailSettings = function (mocked, policy_id, policyData) {
        return this.putObservableForSuffix(mocked, true, "policy/" + policy_id, false, {}, policyData);
    };
    InsuranceProvider.prototype.putFractionamentFlowData = function (mocked, policy_id, policyData) {
        return this.putObservableForSuffix(mocked, false, "policy/" + policy_id + "/fractionament", true, {}, policyData);
    };
    InsuranceProvider.prototype.getPolicyDocuments = function (mocked, policy_id, owner, sort_by, sort_order) {
        var queryParams = {};
        if (owner) {
            queryParams['owner'] = owner;
        }
        if (sort_by && sort_order) {
            queryParams['sort_by'] = sort_by;
            queryParams['sort_order'] = sort_order;
        }
        return this.getObservableForSuffix(mocked, false, "policy/" + policy_id + "/documents", true, queryParams);
    };
    InsuranceProvider.prototype.getCoverageSettings = function (mocked, policy_id, risk_id) {
        return this.getObservableForSuffix(mocked, false, "policy/" + policy_id + "/risk/" + risk_id + "/coverages", true);
    };
    InsuranceProvider.prototype.getClaimSettings = function (mocked, policy_id, risk_id) {
        return this.getObservableForSuffix(mocked, false, "policy/" + policy_id + "/risk/" + risk_id + "/claims/", true);
    };
    InsuranceProvider.prototype.getPolicyReceipts = function (mocked, policy_id, state) {
        return this.getObservableForSuffix(mocked, true, "policy/" + policy_id + "/receipts", true, { state: state });
    };
    InsuranceProvider.prototype.getPolicyReceiptsPaginated = function (mocked, policy_id, state, page, page_size) {
        return this.getObservableForSuffix(mocked, false, "policy/" + policy_id + "/receipts", true, {
            state: state, page: page, page_size: page_size, total_count_required: true
        });
    };
    InsuranceProvider.prototype.getPolicyPaymentMethod = function (mocked, policy_id) {
        return this.getObservableForSuffix(mocked, true, "policy/" + policy_id + "/payment_method", true);
    };
    InsuranceProvider.prototype.getPolicyFractionamentTypes = function (mocked, policy_id) {
        return this.getObservableForSuffix(mocked, true, "policy/" + policy_id + "/available_instalment_payments", true);
    };
    InsuranceProvider.prototype.cancelPolicy = function (mocked, policy_id, flowRequest) {
        return this.deleteObservableForSuffix(mocked, false, "policy/" + policy_id, true, true, flowRequest ? flowRequest : undefined);
    };
    InsuranceProvider.prototype.getClaimDetailSettings = function (mocked, claim_id) {
        return this.getObservableForSuffix(mocked, false, "claim/" + claim_id, true);
    };
    InsuranceProvider.prototype.getInsurances = function (mocked, riskType) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getObservableForSuffix(mocked, false, "client/" + _this.getClientId() + "/risks/", true, riskType ? { type: riskType } : null)
                .toPromise()
                .then(function (response) {
                var mapfreResponse = _this.getResponseJSON(response);
                if (_this.isMapfreResponseValid(mapfreResponse)) {
                    var insurances_1 = [];
                    mapfreResponse.data.forEach(function (insurance) {
                        insurances_1.push(insurance);
                    });
                    resolve(insurances_1);
                }
            });
        });
    };
    InsuranceProvider.prototype.getInsuranceById = function (mocked, riskId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getObservableForSuffix(mocked, false, "client/" + _this.getClientId() + "/risk/" + riskId, true)
                .toPromise()
                .then(function (response) {
                var mapfreResponse = _this.getResponseJSON(response);
                if (_this.isMapfreResponseValid(mapfreResponse)) {
                    var insurance = mapfreResponse.data;
                    resolve(insurance);
                }
            });
        });
    };
    InsuranceProvider.prototype.getInsurancesOfType = function (mocked, insuranceType, userRole) {
        var _this = this;
        if (userRole) {
            return new Promise(function (resolve, reject) {
                _this.getObservableForSuffix(mocked, false, "client/" + _this.getClientId() + "/risks/", true, { user_role: userRole })
                    .toPromise()
                    .then(function (response) {
                    var mapfreResponse = _this.getResponseJSON(response);
                    if (_this.isMapfreResponseValid(mapfreResponse) && mapfreResponse.data) {
                        var insurances_2 = [];
                        mapfreResponse.data.forEach(function (insurance) {
                            if (insurance.type === insuranceType) {
                                insurances_2.push(insurance);
                            }
                        });
                        resolve(insurances_2);
                    }
                });
            });
        }
        else {
            return new Promise(function (resolve, reject) {
                _this.getObservableForSuffix(mocked, false, "client/" + _this.getClientId() + "/risks/", true)
                    .toPromise()
                    .then(function (response) {
                    var mapfreResponse = _this.getResponseJSON(response);
                    if (_this.isMapfreResponseValid(mapfreResponse) && mapfreResponse.data) {
                        var insurances_3 = [];
                        mapfreResponse.data.forEach(function (insurance) {
                            if (insurance.type === insuranceType) {
                                insurances_3.push(insurance);
                            }
                        });
                        resolve(insurances_3);
                    }
                });
            });
        }
    };
    InsuranceProvider.prototype.getPoliciesOfType = function (mocked, insuranceType, userRole) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getInsurancesOfType(mocked, insuranceType, userRole)
                .then(function (insurances) {
                var policies = [];
                insurances.forEach(function (insurance) {
                    insurance.associated_policies.forEach(function (policy) {
                        policy.risk_id = insurance.id;
                        policy.risk_name = insurance.name;
                        policy.risk_type = insurance.type;
                        policies.push(policy);
                    });
                });
                resolve(policies);
            });
        });
    };
    InsuranceProvider.decorators = [
        { type: Injectable },
    ];
    InsuranceProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return InsuranceProvider;
}(ApiProvider));
export { InsuranceProvider };
//# sourceMappingURL=insurance.js.map