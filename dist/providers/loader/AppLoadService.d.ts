import 'rxjs/add/operator/toPromise';
import { ApiProvider } from '../api/api';
import { TranslationProvider } from '../translation/translation';
import { Http } from '@angular/http';
export declare class AppLoadService {
    private http;
    private api;
    private translationProvider;
    constructor(http: Http, api: ApiProvider, translationProvider: TranslationProvider);
    initializeAppLanguage(mocked: boolean): Promise<any>;
}
