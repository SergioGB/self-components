import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { ApiProvider } from '../api/api';
import { TranslationProvider } from '../translation/translation';
import { Http } from '@angular/http';
var AppLoadService = (function () {
    function AppLoadService(http, api, translationProvider) {
        this.http = http;
        this.api = api;
        this.translationProvider = translationProvider;
    }
    AppLoadService.prototype.initializeAppLanguage = function (mocked) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.api.getObservableForSuffix(mocked, false, 'labels', false)
                .toPromise()
                .then(function (settings) {
                var response = settings.json();
                _this.translationProvider.setTranslations(response.data);
                resolve();
            });
        });
    };
    AppLoadService.decorators = [
        { type: Injectable },
    ];
    AppLoadService.ctorParameters = function () { return [
        { type: Http, },
        { type: ApiProvider, },
        { type: TranslationProvider, },
    ]; };
    return AppLoadService;
}());
export { AppLoadService };
//# sourceMappingURL=AppLoadService.js.map