var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from "../api/api";
import { AlertController } from "ionic-angular";
import { PlatformProvider } from "../platform/platform";
import { Diagnostic } from "@ionic-native/diagnostic";
import * as _ from "lodash";
var LocationProvider = (function (_super) {
    __extends(LocationProvider, _super);
    function LocationProvider(injector, platformProvider, diagnostic, alertCtrl) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.platformProvider = platformProvider;
        _this.diagnostic = diagnostic;
        _this.alertCtrl = alertCtrl;
        _this.error = 'Error';
        _this.warning = 'Aviso';
        _this.permissionsError = 'Error con los permisos de localización.';
        _this.pleaseActivateLocation = 'Por favor, activa la ubicación para que podamos localizarte.';
        _this.givePermissionAndroid = "Has de dar permisos a la aplicaci\u00F3n para que podamos localizarte.\n \n              Ajustes -> Aplicaciones -> Autoservicio -> Permisos -> Ubicaci\u00F3n";
        _this.givePermissionIos = "Has de dar permisos a la aplicaci\u00F3n para que podamos localizarte.\n \n              Ajustes -> Privacidad -> Localizaci\u00F3n -> Autoservicio -> Siempre/Cuando se use la aplicaci\u00F3n";
        _this.protocolError = 'Sin HTTPS no se puede usar la ubicación.';
        return _this;
    }
    LocationProvider.prototype.getRoadTypes = function () {
        return this.getObservableForSuffix(false, true, 'catalogs/road_types', true);
    };
    LocationProvider.prototype.getCities = function () {
        return this.getObservableForSuffix(false, true, 'catalogs/cities', true);
    };
    LocationProvider.prototype.getProvinces = function () {
        return this.getObservableForSuffix(false, true, 'catalogs/provinces', true);
    };
    LocationProvider.prototype.getCountries = function () {
        return this.getObservableForSuffix(false, true, 'catalogs/countries', true);
    };
    LocationProvider.prototype.locate = function (successCallback, failureCallback) {
        var _this = this;
        if (this.platformProvider.onApp()) {
            if (this.platformProvider.onAndroid()) {
                this.diagnostic.getLocationAuthorizationStatus().then(function (state) {
                    if (_.isEqual(state, _this.diagnostic.permissionStatus.DENIED_ALWAYS)) {
                        _this.displayAndroidPermissionError(failureCallback);
                    }
                    else {
                        _this.diagnostic.requestLocationAuthorization().then(function (result) {
                            if (_.isEqual(result, _this.diagnostic.permissionStatus.GRANTED) || _.isEqual(result, _this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE)) {
                                _this.diagnostic.isLocationEnabled().then(function (status) {
                                    if (status) {
                                        successCallback();
                                    }
                                    else {
                                        _this.displayLocationActivationWarning(failureCallback);
                                    }
                                }).catch(function (error) {
                                    _this.displayPermissionError(failureCallback);
                                });
                            }
                            else {
                                _this.displayAndroidPermissionError(failureCallback);
                            }
                        }).catch(function (error) {
                            _this.displayPermissionError(failureCallback);
                        });
                    }
                }).catch(function (error) {
                    _this.displayPermissionError(failureCallback);
                });
            }
            else {
                this.diagnostic.getLocationAuthorizationStatus().then(function (state) {
                    if (_.isEqual(state, _this.diagnostic.permissionStatus.DENIED)) {
                        _this.displayIosPermissionError(failureCallback);
                    }
                    else if (_.isEqual(state, 'authorized') || _.isEqual(state, 'authorized_when_in_use')) {
                        _this.diagnostic.isLocationEnabled().then(function (status) {
                            if (status) {
                                successCallback();
                            }
                            else {
                                _this.displayLocationActivationWarning(failureCallback);
                            }
                        }).catch(function (error) {
                            _this.displayPermissionError(failureCallback);
                        });
                    }
                    else {
                        _this.diagnostic.requestLocationAuthorization().then(function (result) {
                            if (_.isEqual(result, 'authorized') || _.isEqual(result, 'authorized_when_in_use')) {
                                _this.diagnostic.isLocationEnabled().then(function (status) {
                                    if (status) {
                                        successCallback();
                                    }
                                    else {
                                        _this.displayLocationActivationWarning(failureCallback);
                                    }
                                }).catch(function (error) {
                                    _this.displayPermissionError(failureCallback);
                                });
                            }
                            else {
                                _this.displayIosPermissionError(failureCallback);
                            }
                        }).catch(function (error) {
                            _this.displayPermissionError(failureCallback);
                        });
                    }
                }).catch(function (error) {
                    _this.displayPermissionError(failureCallback);
                });
            }
        }
        else {
            navigator.geolocation.getCurrentPosition(function () {
                _this.displayLocationActivationWarning(failureCallback);
            }, function () {
                _this.displayPermissionError(failureCallback);
            }, null);
        }
    };
    LocationProvider.prototype.displayPermissionError = function (failureCallback) {
        this.displayAlert(this.error, this.permissionsError, failureCallback);
    };
    LocationProvider.prototype.displayAndroidPermissionError = function (failureCallback) {
        this.displayAlert(this.error, this.givePermissionAndroid, failureCallback);
    };
    LocationProvider.prototype.displayIosPermissionError = function (failureCallback) {
        this.displayAlert(this.error, this.givePermissionIos, failureCallback);
    };
    LocationProvider.prototype.displayProtocolError = function (failureCallback) {
        this.displayAlert(this.error, this.protocolError, failureCallback);
    };
    LocationProvider.prototype.displayLocationActivationWarning = function (failureCallback) {
        this.displayAlert(this.warning, this.pleaseActivateLocation, failureCallback);
    };
    LocationProvider.prototype.displayAlert = function (title, message, failureCallback) {
        var alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    role: 'cancel',
                    handler: function () {
                        failureCallback();
                    }
                }
            ]
        });
        alert.present();
    };
    LocationProvider.decorators = [
        { type: Injectable },
    ];
    LocationProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: PlatformProvider, },
        { type: Diagnostic, },
        { type: AlertController, },
    ]; };
    return LocationProvider;
}(ApiProvider));
export { LocationProvider };
//# sourceMappingURL=location.js.map