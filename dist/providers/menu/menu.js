import { EventEmitter, Injectable } from '@angular/core';
var MenuProvider = (function () {
    function MenuProvider() {
        this.menuReadyEmitter = new EventEmitter();
        this.menuOptionsEmitter = new EventEmitter();
    }
    MenuProvider.decorators = [
        { type: Injectable },
    ];
    MenuProvider.ctorParameters = function () { return []; };
    return MenuProvider;
}());
export { MenuProvider };
//# sourceMappingURL=menu.js.map