import { TranslationProvider } from '../../providers/translation/translation';
import { Injectable } from '@angular/core';
var Components = (function () {
    function Components(translationProvider) {
        this.translationProvider = translationProvider;
        this.components = {
            CM0001: {
                id: 'CM0001',
                label: 'CM0001 Autenticación de acceso',
                component_name: 'LoginFormComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-warning", "checkmark", "close-circle", "checkmark", "close-circle", "checkmark", "close-circle", "checkmark", "close-circle", "mapfre-arrow-right", "mapfre-fingerprint", "person", "mapfre-warning", "checkmark", "close-circle", "checkmark", "close-circle", "checkmark", "close-circle", "checkmark", "close-circle", "mapfre-arrow-right", "finger-print", "person"],
                    simplified: false,
                    modals: [
                        {
                            reference: "WelcomePackModalComponent",
                            type: "23",
                            reflabel: "Modal de Welcome Pack (Login con Pin)",
                            componentId: "pin-login",
                            modalId: "welcome-pack-modal",
                            labels: {
                                button_text: "modal.pin-login.welcome-pack-modal.welcome_pack.button"
                            }
                        },
                        {
                            reference: "WelcomePackModalComponent",
                            type: "23",
                            reflabel: "Modal de Welcome Pack (Login con credenciales)",
                            componentId: "login-form",
                            modalId: "welcome-pack-modal",
                            labels: {
                                button_text: "modal.login-form.welcome-pack-modal.welcome_pack.button"
                            }
                        }
                    ]
                }
            },
            CM0002: {
                id: 'CM0002',
                label: 'CM0002 Gestiones rápidas',
                component_name: 'LoginQuickManagementComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-right", "mapfre-arrow-right"],
                    quickActions: [
                        {
                            "description": {
                                "es": "Acción 1"
                            },
                            "title": {
                                "es": "Acción 1"
                            },
                            "icon_color": "#d81e05",
                            "icon": "settings",
                            "href": "http://www.mapfre.com",
                            "target": "_blank"
                        }
                    ],
                    modals: [
                        {
                            reference: "OpenPaymentModalComponent",
                            type: "24",
                            reflabel: "Modal de pago en abierto",
                            componentId: "login-quick-management",
                            modalId: "open-payment-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.login-quick-management.open-payment-modal.title',
                                subtitle: 'modal.login-quick-management.open-payment-modal.subtitle',
                                birthday: "modal.login-quick-management.open-payment-modal.open_payment.modal.birthday",
                                identification_number: "modal.login-quick-management.open-payment-modal.new_account.identification_number",
                                identification_document: "modal.login-quick-management.open-payment-modal.new_account.identification_document",
                                continue: "modal.login-quick-management.open-payment-modal.generic.continue",
                                cancel_button: "modal.login-quick-management.open-payment-modal.close_session_modal.cancel_button",
                                warning_text: "modal.login-quick-management.oopen-payment-modal.blocked_user.check_sms.warning_text"
                            }
                        },
                        {
                            reference: "OpenPaymentModalComponent",
                            type: "24",
                            reflabel: "Modal de asistencia en abierto",
                            componentId: "login-quick-management",
                            modalId: "open-request-assistance",
                            mocked: true,
                            labels: {
                                title: "modal.login-quick-management.open-request-assistance.title",
                                subtitle: "modal.login-quick-management.open-request-assistance.subtitle",
                                birthday: "modal.login-quick-management.open-request-assistance.open_payment.modal.birthday",
                                identification_number: "modal.login-quick-management.open-request-assistance.new_account.identification_number",
                                identification_document: "modal.login-quick-management.open_request_assistance.new_account.identification_document",
                                continue: "modal.login-quick-management.open-request-assistance.generic.continue",
                                cancel_button: "modal.login-quick-management.open-request-assistance.close_session_modal.cancel_button",
                                warning_text: "modal.login-quick-management.open-request-assistance.blocked_user.check_sms.warning_text"
                            }
                        }
                    ]
                }
            },
            CM0002_V2: {
                id: "CM0002_V2",
                label: "CM0002_V2 Gestiones rápidas",
                component_name: "LoginQuickManagementV2Component",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-right", "mapfre-arrow-right"],
                    quickActions: [
                        {
                            "description": {
                                "es": "Acción 1"
                            },
                            "title": {
                                "es": "Acción 1"
                            },
                            "icon_color": "#d81e05",
                            "icon": "settings",
                            "href": "http://www.mapfre.com",
                            "target": "_blank"
                        }
                    ],
                    modals: [
                        {
                            reference: "OpenPaymentModalComponent",
                            type: "24",
                            reflabel: "Modal de pago en abierto",
                            componentId: "login-quick-management-v2",
                            modalId: "open-payment-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.login-quick-management-v2.open-payment-modal.title',
                                subtitle: 'modal.login-quick-management-v2.open-payment-modal.subtitle',
                                birthday: "modal.login-quick-management-v2.open-payment-modal.birthday",
                                identification_number: "modal.login-quick-management-v2.open-payment-modal.identification_number",
                                identification_document: "modal.login-quick-management-v2.open-payment-modal.identification_document",
                                continue: "modal.login-quick-management-v2.open-payment-modal.generic.continue",
                                cancel_button: "modal.login-quick-management-v2.open-payment-modal.cancel",
                                warning_text: "modal.login-quick-management-v2.open-payment-modal.warning"
                            },
                            icon: "mapfre-credit-card"
                        },
                        {
                            reference: "OpenPaymentModalComponent",
                            type: "24",
                            reflabel: "Modal de asistencia en abierto",
                            componentId: "login-quick-management-v2",
                            modalId: "request-assistance-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.login-quick-management-v2.request-assistance-modal.title',
                                subtitle: 'modal.login-quick-management-v2.request-assistance-modal.subtitle',
                                birthday: "modal.login-quick-management-v2.request-assistance-modal.birthday",
                                identification_number: "modal.login-quick-management-v2.request-assistance-modal.identification_number",
                                identification_document: "modal.login-quick-management-v2.request-assistance-modal.identification_document",
                                continue: "modal.login-quick-management-v2.request-assistance-modal.continue",
                                cancel_button: "modal.login-quick-management-v2.request-assistance-modal.cancel",
                                warning_text: "modal.login-quick-management-v2.request-assistance-modal.warning"
                            },
                            icon: "mapfre-buoy"
                        },
                        {
                            reference: "OpenSubmitIncidenceModalComponent",
                            type: "24",
                            reflabel: "Modal de dar un parte en abierto",
                            componentId: "login-quick-management-v2",
                            modalId: "open-submit-incidence-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.login-quick-management-v2.open-submit-incidence-modal.title',
                                subtitle: 'modal.login-quick-management-v2.open-submit-incidence-modal.subtitle',
                                continue: "modal.login-quick-management-v2.open-submit-incidence-modal.continue",
                                cancel_button: "modal.login-quick-management-v2.open-submit-incidence-modal.cancel",
                                error_text: 'modal.login-quick-management-v2.open-submit-incidence-modal.error',
                                warning_text: "modal.login-quick-management-v2.open-submit-incidence-modal.warning",
                                identification_document: "modal.login-quick-management-v2.open-submit-incidence-modal.identification_document",
                                identification_number: "modal.login-quick-management-v2.open-submit-incidence-modal.identification_number",
                                isClient: "modal.login-quick-management-v2.open-submit-incidence-modal.is_client",
                                birthday: "modal.login-quick-management-v2.open-submit-incidence-modal.birthday"
                            },
                            icon: "mapfre-car"
                        },
                        {
                            reference: "OpenPaymentModalV2Component",
                            type: "24",
                            reflabel: "Modal de contratación (número de presupuesto)",
                            componentId: "login-quick-management-v2",
                            modalId: "contract-budget-number-modal",
                            mocked: false,
                            labels: {
                                title: 'modal.login-quick-management-v2.contract-budget-number-modal.title',
                                subtitle: 'modal.login-quick-management-v2.contract-budget-number-modal.subtitle',
                                budget_number: "modal.login-quick-management-v2.contract-budget-number-modal.budget_number",
                                email: "modal.login-quick-management-v2.contract-budget-number-modal.email",
                                error_email: "modal.login-quick-management-v2.contract-budget-number-modal.error_email",
                                continue: "modal.login-quick-management-v2.contract-budget-number-modal.continue",
                                cancel_button: "modal.login-quick-management-v2.contract-budget-number-modal.cancel",
                                error_text: 'modal.login-quick-management-v2.contract-budget-number-modal.error',
                                warning_text: "modal.login-quick-management-v2.contract-budget-number-modal.warning"
                            },
                            icon: "mapfre-folder"
                        },
                        {
                            reference: "OpenPaymentModalV2Component",
                            type: "24",
                            reflabel: "Modal de contratación (fecha de nacimiento)",
                            componentId: "login-quick-management-v2",
                            modalId: "contract-birthday-modal",
                            mocked: false,
                            labels: {
                                title: 'modal.login-quick-management-v2.contract-birthday-modal.title',
                                subtitle: 'modal.login-quick-management-v2.contract-birthday-modal.subtitle',
                                birthday: "modal.login-quick-management-v2.contract-birthday-modal.birthday",
                                email: "modal.login-quick-management-v2.contract-birthday-modal.email",
                                error_email: "modal.login-quick-management-v2.contract-birthday-modal.error_email",
                                continue: "modal.login-quick-management-v2.contract-birthday-modal.continue",
                                cancel_button: "modal.login-quick-management-v2.contract-birthday-modal.cancel",
                                error_text: 'modal.login-quick-management-v2.contract-birthday-modal.error',
                                warning_text: "modal.login-quick-management-v2.contract-birthday-modal.warning"
                            },
                            icon: "mapfre-file"
                        }
                    ]
                }
            },
            CM0003: {
                id: 'CM0003',
                label: 'CM0003 Opiniones',
                component_name: 'LoginSuccessStoriesComponent',
                params: {
                    mocked: false,
                    styles: {},
                    clear: true,
                    background_image: null
                }
            },
            CM0004: {
                id: 'CM0004',
                label: 'CM0004 Imagen y texto promocional',
                component_name: 'LoginBannerComponent',
                params: {
                    mocked: true,
                    styles: {},
                    clear: true,
                    background_image: null,
                    title: "Imagen promocional",
                    description: "Elemento de imagen promocional con título, descripción e imagen de fondo."
                }
            },
            CM0005: {
                id: 'CM0005',
                label: 'CM0005 Vídeo y texto promocional',
                component_name: 'LoginVideoComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ['mapfre-play'],
                    clear: true,
                    video_link: null,
                    title: 'Vídeo promocional',
                    description: 'Elemento de vídeo promocional con título, descripción y vídeo.',
                    modals: [
                        {
                            reference: "VideoModalComponent",
                            type: "25",
                            reflabel: "Modal de video",
                            componentId: "login-video",
                            modalId: "video-modal"
                        }
                    ]
                }
            },
            CM0006: {
                id: 'CM0006',
                label: 'CM0006  Enlaces de páginas de descarga de APPs',
                component_name: 'LoginAppDownloadComponent',
                params: {
                    mocked: false,
                    styles: {},
                    title: { "es": "Nueva App disponible" },
                    subtitle: { "es": "Descarga nuestra App y realiza tus gestiones desde tu móvil o tablet" },
                    background_image: null
                }
            },
            CM0007: {
                id: 'CM0007',
                label: 'CM0007 Acceder a página de MAPFRE desde lista de Redes Sociales',
                component_name: 'LoginContactComponent',
                params: {
                    mocked: false,
                    styles: {},
                    title: { "es": "¿Algo que contarnos?" },
                    subtitle: { "es": "¿Tienes alguna duda? ¿Has probado el área de Clientes? Comparte con nosotros tu experiencia" },
                    background_image: null
                }
            },
            CM0013: {
                label: 'CM0013 Título de página',
                component_name: 'UserWelcomeComponent',
                params: {
                    mocked: false,
                    styles: {},
                    title: {
                        es: "Título"
                    },
                    subtitle: {
                        es: "Subtítulo"
                    }
                }
            },
            CM0016: {
                id: 'CM0016',
                label: 'CM0016 Título de página con pestañas',
                component_name: 'FaqsHeaderComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0018: {
                id: 'CM0018',
                label: 'CM0018 Formulario de alta',
                component_name: 'UserDataFormComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["warning", "close-circle"]
                }
            },
            CM0021: {
                id: 'CM0021',
                label: 'CM0021 Volver a ver welcome pack',
                component_name: 'WelcomePackLinkCardComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-present", "mapfre-arrow-right"],
                    modals: [
                        {
                            type: 23,
                            modalId: "welcome-pack-modal",
                            componentId: "welcome-pack-link-card",
                            reference: 'WelcomePackModalComponent',
                            reflabel: 'Modal de Welcome Pack',
                            labels: {
                                button_text: "modal.welcome-pack-link-card.welcome-pack-modal.welcome_pack.button"
                            }
                        }
                    ]
                }
            },
            CM0022: {
                id: 'CM0022',
                label: 'CM0022 Ofertas para ti',
                component_name: 'YourOffersComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-right"]
                }
            },
            CM0022V1: {
                id: 'CM0022V1',
                label: 'CM0022V1 Ofertas para ti',
                component_name: 'YourOffersV1Component',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-right"]
                }
            },
            CM0023: {
                id: 'CM0023',
                label: 'CM0023 Contacto',
                component_name: 'UserContactsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-mobile", "mapfre-email", "mapfre-location", "mapfre-chat"]
                }
            },
            CM0024: {
                id: 'CM0024',
                label: 'CM0024 Temas pendientes',
                component_name: 'YourPendingTopicsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: [
                        'mapfre-warning',
                        'mapfre-warning',
                        'mapfre-warning'
                    ],
                    modals: [
                        {
                            type: 1,
                            modalId: "attendance-tracking-modal",
                            componentId: "your-pending-topics",
                            reference: 'AttendanceTrackingModalComponent',
                            reflabel: 'Modal de seguimiento de asistencia',
                            labels: {
                                title: "modal.your-pending-topics.attendance-tracking-modal.attendance_tracking.title",
                                subtitle: "modal.your-pending-topics.attendance-tracking-modal.attendance_tracking.subtitle",
                                generic_cancel: "modal.your-pending-topics-tracking-modal.generic.cancel",
                                generic_call: "modal.your-pending-topics.attendance-tracking-modal.generic.call",
                                cancellation_title: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.title",
                                cancelation_subtitle: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.subtitle",
                                no_button: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.no_button",
                                yes_button: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.yes_button",
                                confirmation_title: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.confirmation_title",
                                confirmation_subtitle: "modal.your-pending-topics.attendance-tracking-modal.request_assistance.assistance_cancelation.confirmation_description"
                            }
                        },
                        {
                            type: 2,
                            modalId: "assistance-cancellation-modal",
                            componentId: "attendance-tracking",
                            reference: 'AssistanceCancellationModalComponent',
                            reflabel: 'Modal de cancelación de asistencia',
                            labels: {
                                title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.title",
                                subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.subtitle",
                                no_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.no_button",
                                yes_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.yes_button",
                                confirmation_title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_title",
                                confirmation_subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_description"
                            }
                        }
                    ]
                }
            },
            CM0025: {
                id: 'CM0025',
                label: 'CM0025 Nuevas comunicaciones',
                component_name: 'GlobalPositionNewCommunicationsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-right"]
                }
            },
            CM0026: {
                id: 'CM0026',
                label: 'CM0026 Resumen por ramo/póliza',
                component_name: 'GlobalPositionInsuranceEntryComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-warning", "mapfre-arrow-right"]
                }
            },
            CM0027: {
                id: 'CM0027',
                label: 'CM0027 Acceso a incidencias, quejas y/o reclamaciones',
                component_name: 'IncidencesAndClaimsCardsComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0028: {
                id: 'CM0028',
                label: 'CM0028 FAQs',
                component_name: 'FaqsContentComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0029: {
                id: 'CM0029',
                label: 'CM0029 Formulario de incidencias',
                component_name: 'IncidenceFormSimpleComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0029V1: {
                id: 'CM0029V1',
                label: 'CM0029V1 Formulario de incidencias',
                component_name: 'IncidenceFormSimpleV1Component',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0030: {
                id: 'CM0030',
                label: 'CM0030 Comunicaciones',
                component_name: 'CommunicationsListComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up"],
                    modals: [
                        {
                            reference: "CommunicationsModalComponent",
                            type: "7",
                            reflabel: "Visualización detallada de comunicaciones",
                            componentId: "communications-list",
                            modalId: "communications-modal",
                            mocked: true
                        }
                    ]
                }
            },
            CM0035: {
                id: 'CM0035',
                label: 'CM0035 Datos de los perjudicados',
                component_name: 'HomeIncidenceInjuredDataComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-close"]
                }
            },
            CM0040: {
                id: 'CM0040',
                label: 'CM0040 Exteder a otras pólizas',
                component_name: 'PolicyExtensionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-checkbox-on", "mapfre-checkbox-off"]
                }
            },
            CM0042: {
                id: 'CM0042',
                label: 'CM0042 Documentación disponible',
                component_name: 'InsuranceDocumentationListComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-filter-arrows"],
                    modals: [
                        {
                            reference: "DocumentAdHocModalComponent",
                            type: "12",
                            reflabel: "Modal de documentación ad-hoc",
                            componentId: "documentation-list",
                            modalId: "modal.documentation-list.document-adhoc-modal",
                            mocked: true,
                            relatedEntity: null,
                            relatedEntityId: null,
                            template: 'pdf',
                            labels: {
                                title: "modal.documentation-list.document-adhoc-modal.documents.adhoc.title",
                                description: "modal.documentation-list.document-adhoc-modal.documents.adhoc.description",
                                required_fields: "modal.documentation-list.document-adhoc-modal.documents.adhoc.required_fields",
                                name: "modal.documentation-list.document-adhoc-modal.new_account.name",
                                surname1: "modal.documentation-list.document-adhoc-modal.new_account.surname1",
                                surname2: "modal.documentation-list.document-adhoc-modal.new_account.surname2",
                                identification_document: "modal.documentation-list.document-adhoc-modal.new_account.identification_document",
                                identification_number: "modal.documentation-list.document-adhoc-modal.new_account.identification_number",
                                cancel_button: "modal.documentation-list.document-adhoc-modal.close_session_modal.cancel_button",
                                generate: "modal.documentation-list.document-adhoc-modal.download_item.generate",
                                cancel: "modal.documentation-list.document-adhoc-modal.close_session_modal.cancel",
                                confirmation_title: "modal.documentation-list.document-adhoc-modal.documents.adhoc.confirmation.title",
                                confirmation_description: "modal.documentation-list.document-adhoc-modal.documents.adhoc.confirmation.description"
                            }
                        }
                    ]
                }
            },
            CM0043: {
                id: 'CM0043',
                label: 'CM0043 Nueva contratación',
                component_name: 'GlobalPositionHireInsuranceComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-right"]
                }
            },
            CM0044: {
                id: 'CM0044',
                label: 'CM0044 Recibo pendiente',
                component_name: 'ReceiptSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0045: {
                label: 'CM0045 Título de página',
                component_name: 'UserWelcomeComponent',
                params: {
                    mocked: false,
                    styles: {},
                    title: {
                        es: "Título"
                    },
                    subtitle: {
                        es: "Subtítulo"
                    }
                }
            },
            CM0046: {
                id: 'CM0046',
                label: 'CM0046 Método de pago',
                component_name: 'PaymentMethodSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-minus-circle", "mapfre-plus-circle", "mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check", "mapfre-minus-circle", "mapfre-plus-circle"],
                    modals: [
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept"
                            },
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.delay'),
                            hideButtons: true
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.call.title'),
                            subtitle: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.call.description'),
                            visualizeButtonAccept: true,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept"
                            },
                            hideButtons: true,
                            formCallMe: true
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.payment-method-creation.delay'
                            },
                            hideButtons: true
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            type: 0,
                            eventName: 'acceptDelay',
                            hideButtons: true,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.policy_extension.hours.title',
                                subtitle: 'modal.payment-method-selection.information-modal.policy_extension.hours.description'
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.generic.accept')
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            type: 0,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.policy_extension.call.title',
                                subtitle: 'modal.payment-method-selection.information-modal.policy_extension.call.description'
                            },
                            eventName: 'acceptDelay',
                            hideButtons: true,
                        },
                        {
                            reference: "ProcessFailureModalComponent",
                            type: "11",
                            reflabel: "Modal de fallo del proceso",
                            componentId: "payment-method-selection",
                            modalId: "process-failure-modal",
                            icon: 'mapfre-cross-error',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.process-failure-modal.payment-method-creation.cancel')
                        },
                        {
                            reference: "PolicyExtensionModalComponent",
                            type: "27",
                            reflabel: "Modal de extensión de pólizas",
                            componentId: "payment-method-selection",
                            modalId: "policy-extension-modal",
                            policyId: 1,
                            mocked: true,
                            method: 'credit-card',
                            labels: {
                                cancel: "modal.payment-method-selection.policy-extension-modal.generic.cancel",
                                apply: "modal.payment-method-selection.policy-extension-modal.generic.apply"
                            }
                        },
                        {
                            reference: "ProcessConfirmationModalComponent",
                            type: "28",
                            reflabel: "Modal de confirmación de proceso",
                            componentId: "payment-method-selection",
                            modalId: "process-confirmation-modal",
                            icon: 'mapfre-check',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.process-confirmation-modal.payment-method-creation.confirmation')
                        }
                    ]
                }
            },
            CM0046V1: {
                id: "CM0046V1",
                label: "CM0046V1  Método de pago",
                component_name: "PaymentMethodSelectionV1Component",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-minus-circle", "mapfre-plus-circle", "mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check", "mapfre-minus-circle", "mapfre-plus-circle"],
                    modals: [
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept"
                            },
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.delay'),
                            hideButtons: true
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.call.title'),
                            subtitle: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.call.description'),
                            visualizeButtonAccept: true,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept"
                            },
                            hideButtons: true,
                            formCallMe: true
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.payment-method-creation.delay'
                            },
                            hideButtons: true
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            type: 0,
                            eventName: 'acceptDelay',
                            hideButtons: true,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.policy_extension.hours.title',
                                subtitle: 'modal.payment-method-selection.information-modal.policy_extension.hours.description'
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.generic.accept')
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            type: 0,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.policy_extension.call.title',
                                subtitle: 'modal.payment-method-selection.information-modal.policy_extension.call.description'
                            },
                            eventName: 'acceptDelay',
                            hideButtons: true,
                        },
                        {
                            reference: "ProcessFailureModalComponent",
                            type: "11",
                            reflabel: "Modal de fallo del proceso",
                            componentId: "payment-method-selection",
                            modalId: "process-failure-modal",
                            icon: 'mapfre-cross-error',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.process-failure-modal.payment-method-creation.cancel')
                        },
                        {
                            reference: "PolicyExtensionModalComponent",
                            type: "27",
                            reflabel: "Modal de extensión de pólizas",
                            componentId: "payment-method-selection",
                            modalId: "policy-extension-modal",
                            policyId: 1,
                            mocked: true,
                            method: 'credit-card',
                            labels: {
                                cancel: "modal.payment-method-selection.policy-extension-modal.generic.cancel",
                                apply: "modal.payment-method-selection.policy-extension-modal.generic.apply"
                            }
                        },
                        {
                            reference: "ProcessConfirmationModalComponent",
                            type: "28",
                            reflabel: "Modal de confirmación de proceso",
                            componentId: "payment-method-selection",
                            modalId: "process-confirmation-modal",
                            icon: 'mapfre-check',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.process-confirmation-modal.payment-method-creation.confirmation')
                        }
                    ]
                }
            },
            CM0047: {
                id: 'CM0047',
                label: 'CM0047 Resumen de la operación',
                component_name: 'SummaryComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up", "mapfre-file"],
                    models: [
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.payment-method-creation.delay'
                            },
                            hideButtons: true
                        }
                    ]
                }
            },
            CM0047V1: {
                id: 'CM0047V1',
                label: 'CM0047V1 Resumen de la operación',
                component_name: 'SummaryV2Component',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up", "mapfre-file"],
                    disclaimerMessage: {
                        es: "Los datos facilitados son determinantes del riesgo asegurado y su valoración. La inexactitud o falsedad de los mismos podría afectar al pago de cualquier siniestro y dar lugar a la rescisión de la póliza o su modificación para adaptarla al riesgo real."
                    },
                    modals: [
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.payment-method-creation.delay'
                            },
                            hideButtons: true
                        }
                    ]
                }
            },
            CM0048: {
                id: 'CM0048',
                label: 'CM0048 Confirmación del proceso',
                component_name: 'UpgradeInsuranceResultComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-outgoing-call", "mapfre-incoming-call", "mapfre-download", "mapfre-share", "mapfre-envelope", "mapfre-download"],
                    modals: [
                        {
                            type: "34",
                            modalId: "call-me-back",
                            componentId: "upgrade-insurance-result",
                            reference: 'AppointmentModalComponent',
                            reflabel: 'Modal de solicitud de llamada',
                            labels: {
                                hours_48: "modal.upgrade-insurance-result.appointment-modal.description.48_hours",
                                appointment_time: "modal.upgrade-insurance-result.appointment-modal.description.appointment_time",
                                less_than: "modal.upgrade-insurance-result.appointment-modal.description.less_than",
                                will_call: "modal.upgrade-insurance-result.appointment-modal.description.will_call",
                                title: "modal.upgrade-insurance-result.appointment-modal.title",
                                home_button: "modal.upgrade-insurance-result.appointment-modal.home_button"
                            }
                        },
                        {
                            reference: "CallMeModalComponent",
                            type: "8",
                            reflabel: "Modal Llámame",
                            componentId: "help",
                            modalId: "call-me-modal",
                            mocked: true,
                            labels: {
                                title: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.save_button"
                            }
                        },
                        {
                            reference: "MailMeModalComponent",
                            type: "34",
                            componentId: "sinister-documentation",
                            reflabel: "Confirmación por e-mail",
                            modalId: "mail-me-modal",
                            labels: {
                                save: "modal.upgrade-insurance-result.mail-me-modal.generic.save",
                                cancel: "modal.upgrade-insurance-result.mail-me-modal.generic.cancel",
                                mail: "modal.upgrade-insurance-result.mail-me-modal.new_account.mail",
                                privacy_check1: "modal.upgrade-insurance-result.mail-me-modal.new_account.privacy_check1",
                                privacy_check2: "modal.upgrade-insurance-result.mail-me-modal.new_account.privacy_check2",
                                privacy_check3: "modal.upgrade-insurance-result.mail-me-modal.new_account.privacy_check3",
                                privacy_check4: "modal.upgrade-insurance-result.mail-me-modal.new_account.privacy_check4",
                                privacy_check5: "modal.upgrade-insurance-result.mail-me-modal.new_account.privacy_check5",
                                confirmation: "modal.upgrade-insurance-result.mail-me-modal.mail_me.confirmation",
                                description: "modal.upgrade-insurance-result.mail-me-modal.mail_me.description",
                                mail_me_button: "modal.upgrade-insurance-result.mail-me-modal.mail_me.mail_me_button",
                                title: "modal.upgrade-insurance-result.mail-me-modal.mail_me.title",
                                want_update_your_data_description: "modal.upgrade-insurance-result.mail-me-modal.mail_me.mail",
                                want_update_your_data_title: "modal.upgrade-insurance-result.mail-me-modal.mail_me.mail"
                            }
                        }
                    ]
                }
            },
            CM0049: {
                id: 'CM0049',
                label: 'CM0049 Siniestro',
                component_name: 'LossListComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-warning", "mapfre-arrow-up"],
                    modals: [
                        {
                            reference: "ConditionsAcceptModal",
                            type: "16",
                            reflabel: "Modal de aceptación de condiciones",
                            componentId: "loss",
                            modalId: "conditions-accept-modal",
                            mocked: true,
                            labels: {
                                accept: "modal.loss.conditions-accept-modal.generic.accept",
                                cancel: "modal.loss.conditions-accept-modal.generic.cancel",
                                indemnify_proposal: "modal.loss.conditions-accept-modal.new_account.indemnify_proposal",
                                privacy_check1: "modal.loss.conditions-accept-modal.new_account.privacy_check1",
                                privacy_check2: "modal.loss.conditions-accept-modal.new_account.privacy_check2",
                                privacy_check3: "modal.loss.conditions-accept-modal.new_account.privacy_check3",
                                privacy_check4: "modal.loss.conditions-accept-modal.new_account.privacy_check4",
                                privacy_check5: "modal.loss.conditions-accept-modal.new_account.privacy_check5"
                            }
                        },
                        {
                            reference: "CancelSinisterModalComponent",
                            type: "17",
                            reflabel: "Modal de cancelación de siniestro",
                            componentId: "loss",
                            modalId: "cancel-sinister-modal",
                            mocked: true,
                            icon: 'mapfre-information',
                            labels: {
                                title: this.translationProvider.getValueForKey("modal.loss.cancel-sinister-modal.title"),
                                subtitle: this.translationProvider.getValueForKey("modal.loss.cancel-sinister-modal.subtitle"),
                                textCenterButton: this.translationProvider.getValueForKey("modal.loss.cancel-sinister-modal.generic.call")
                            }
                        },
                        {
                            reference: "IndemnifyDetailModalComponent",
                            type: "18",
                            reflabel: "Modal de detalle de indemnización",
                            componentId: "loss",
                            modalId: "indemnify-detail-modal",
                            mocked: true,
                            labels: {
                                accept: "modal.loss.indemnify-detail-modal.generic.accept",
                                cancel: "modal.loss.indemnify-detail-modal.generic.cancel",
                                account_number: "modal.loss.indemnify-detail-modal.indemnify_proposal.account_number",
                                advertiser: "modal.loss.indemnify-detail-modal.indemnify_proposal.advertiser",
                                amount: "modal.loss.indemnify-detail-modal.indemnify_proposal.amount",
                                insured: "modal.loss.indemnify-detail-modal.indemnify_proposal.insured",
                                payment_method: "modal.loss.indemnify-detail-modal.indemnify_proposal.payment_method",
                                policy_number: "modal.loss.indemnify-detail-modal.indemnify_proposal.policy_number",
                                risk: "modal.loss.indemnify-detail-modal.indemnify_proposal.risk",
                                sinister_date: "modal.loss.indemnify-detail-modal.indemnify_proposal.sinister_dater",
                                sinister_number: "modal.loss.indemnify-detail-modal.indemnify_proposal.sinister_number",
                                title: "modal.loss.indemnify-detail-modal.indemnify_proposal.title"
                            }
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "1",
                            reflabel: "Modal de Información",
                            componentId: "popover-receipt-payment",
                            modalId: "information-modal",
                            mocked: true,
                            labels: {
                                cancel: "modal.popover-receipt-payment.information-modal.generic.cancel",
                                finish: "modal.popover-receipt-payment.information-modal.generic.finish",
                                accept: "modal.popover-receipt-payment.information-modal.generic.accept",
                                title: 'modal.popover-receipt-payment.information-modal.my_receipts.action.delay.with_economic_impact',
                            },
                            icon: 'mapfre-warning',
                            confirmContent: {
                                flowReference: true,
                                icon: 'mapfre-check',
                                title: this.translationProvider.getValueForKey('popover-receipt-payment.my_receipts.action.delay.confirmation.title'),
                                description: this.translationProvider.getValueForKey('popover-receipt-payment.my_receipts.action.delay.confirmation.description')
                            },
                            eventName: 'acceptDelay',
                            hideFinalizeButton: true,
                            rightButton: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.generic.accept'),
                            eventConfirm: 'popover-receipt-payment:delay',
                            newLineSubtitle: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.delay.question')
                        },
                        {
                            type: "2",
                            modalId: "assistance-cancellation-modal",
                            componentId: "attendance-tracking",
                            reference: 'AssistanceCancellationModalComponent',
                            reflabel: 'Modal de cancelación de asistencia',
                            labels: {
                                title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.title",
                                subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.subtitle",
                                no_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.no_button",
                                yes_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.yes_button",
                                confirmation_title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_title",
                                confirmation_subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_description"
                            }
                        }
                    ]
                }
            },
            CM0050: {
                id: 'CM0050',
                label: 'CM0050 Sin siniestros',
                component_name: 'EmptySubmitIncidenceComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-right", "mapfre-folder"],
                    modals: [
                        {
                            reference: "UrgentAssistanceModalComponent",
                            type: "9",
                            reflabel: "Modal de asistencia urgente",
                            componentId: "empty-submit-incidence",
                            modalId: "urgent-assistance-modal",
                            mocked: true,
                            labels: {
                                success_title: "urgent-assistance-modal.urgent_assistance_modal.success.title",
                                success_subtitle: "urgent-assistance-modal.urgent_assistance_modal.success.subtitle",
                                success_date: "urgent-assistance-modal.urgent_assistance_modal.success.date",
                                title: "urgent-assistance-modal.urgent_assistance_modal.title",
                                subtitle: "urgent-assistance-modal.urgent_assistance_modal.subtitle",
                                skip_button: "urgent-assistance-modal.urgent_assistance_modal.success.skip_button",
                                continue_button: "urgent-assistance-modal.urgent_assistance_modal.success.continue_button",
                                accept_button: "urgent-assistance-modal.urgent_assistance_modal.success.accept_button",
                                cancel_button: "urgent-assistance-modal.urgent_assistance_modal.success.cancel_button"
                            }
                        }
                    ]
                }
            },
            CM0051: {
                id: 'CM0051',
                label: 'CM0051 Expediente',
                component_name: 'SinisterTracingsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-check"]
                }
            },
            CM0052: {
                id: 'CM0052',
                label: 'CM0052 Resumen de siniestro',
                component_name: 'LossDetailComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0056: {
                id: 'CM0056',
                label: 'CM0056 Selección de riesgo',
                component_name: 'InsuranceSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0056_2: {
                id: 'CM0056_2',
                label: 'CM0056 Selección de riesgo - Asistencia',
                component_name: 'AssistanceInsuranceSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0057: {
                id: 'CM0057',
                label: 'CM0057 Tipo de siniestro',
                component_name: 'SubmitIncidenceTypeComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check", "mapfre-info-full"]
                }
            },
            CM0059: {
                id: 'CM0059',
                label: 'CM0059 Vehículos involucrados',
                component_name: 'SubmitIncidenceSubtypeComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check", "mapfre-info-full"],
                    isCallMeBack: true,
                }
            },
            CM0061: {
                id: 'CM0061',
                label: 'CM0061 Lesionados',
                component_name: 'SubmitIncidenceInjuriesComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0062: {
                id: 'CM0062',
                label: 'CM0062 Detalle de lesionados',
                component_name: 'SubmitIncidenceInjuriesDetailComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0063: {
                id: 'CM0063',
                label: 'CM0063 Figura del ajustador',
                component_name: 'SubmitIncidenceAdjusterComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0064: {
                id: 'CM0064',
                label: 'CM0064 Cita con el profesional',
                component_name: 'ProfessionalAppointmentComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-tools"],
                    modals: [
                        {
                            type: 3,
                            modalId: "calendar-meeting-modal",
                            componentId: "professional-meeting-modal",
                            reference: 'CalendarMeetingModalComponent',
                            reflabel: 'Modal de calendario para solicitud de cita',
                            labels: {
                                title: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.title",
                                date: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.date",
                                hour: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.hour",
                                busy: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.busy",
                                low: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.low",
                                medium: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.medium",
                                high: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.high",
                                services: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.services",
                                decoy: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.decoy",
                                cancel: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.cancel",
                                confirm: "modal.professional-meeting-modal.calendar-meeting-modal.calendar_selection.confirm"
                            }
                        }
                    ]
                }
            },
            CM0066: {
                id: 'CM0066',
                label: 'CM0066 Cuándo ha sucedido',
                component_name: 'SubmitIncidenceWhenComponent',
                params: {
                    mocked: false,
                    styles: {},
                }
            },
            CM0067: {
                id: 'CM0067',
                label: 'CM0067 Mi ubicación',
                component_name: 'MyLocationComponent',
                params: {
                    styles: {},
                    icons: ["mapfre-geolocation", "mapfre-info-full"],
                    checkMap: true,
                    modals: [
                        {
                            type: 3,
                            modalId: "calendar-meeting-modal",
                            componentId: "my-location",
                            reference: 'CalendarMeetingModalComponent',
                            reflabel: 'Modal de calendario para solicitud de cita',
                            labels: {
                                title: "modal.my-location.calendar-meeting-modal.calendar_selection.title",
                                date: "modal.my-location.calendar-meeting-modal.calendar_selection.date",
                                hour: "modal.my-location.calendar-meeting-modal.calendar_selection.hour",
                                busy: "modal.my-location.calendar-meeting-modal.calendar_selection.busy",
                                low: "modal.my-location.calendar-meeting-modal.calendar_selection.low",
                                medium: "modal.my-location.calendar-meeting-modal.calendar_selection.medium",
                                high: "modal.my-location.calendar-meeting-modal.calendar_selection.high",
                                services: "modal.my-location.calendar-meeting-modal.calendar_selection.services",
                                decoy: "modal.my-location.calendar-meeting-modal.calendar_selection.decoy",
                                cancel: "modal.my-location.calendar-meeting-modal.calendar_selection.cancel",
                                confirm: "modal.my-location.calendar-meeting-modal.calendar_selection.confirm"
                            }
                        }
                    ]
                }
            },
            CM0068: {
                id: 'CM0068',
                label: 'CM0068 Cómo se ha producido',
                component_name: 'SubmitIncidenceHowComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0069: {
                id: 'CM0069',
                label: 'CM0069 Daños propios',
                component_name: 'OwnDamagesFormComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0070: {
                id: 'CM0070',
                label: 'CM0070 Daños contrarios',
                component_name: 'ContraryDamagesFormComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0071: {
                id: 'CM0071',
                label: 'CM0071 Otros daños',
                component_name: 'OtherDamagesFormComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0072: {
                id: 'CM0072',
                label: 'CM0072 Otros intervinientes',
                component_name: 'OtherParticipantsFormComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0073: {
                id: 'CM0073',
                label: 'CM0073 Elige un taller',
                component_name: 'WorkshopSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-geolocation", "mapfre-arrow-up", "mapfre-arrow-up", "mapfre-arrow-up", "checkmark", "mapfre-information", "mapfre-plus-circle", "mapfre-minus-circle"],
                    modals: [
                        {
                            reference: "ProfessionalMeetingModalComponent",
                            type: "38",
                            componentId: "workshop-display",
                            modalId: "professional-meeting-modal",
                            reflabel: "Modal de cita con profesional",
                            labels: {
                                continue: "modal.workshop-display.professional-meeting-modal.generic.continue",
                                cancel_button: "modal.workshop-display.professional-meeting-modal.submit_incidence.professional_meeting.cancel_button",
                                modify_button: "modal.workshop-display.professional-meeting-modal.submit_incidence.professional_meeting.modify_button",
                                subtitle: "modal.workshop-display.professional-meeting-modal.submit_incidence.professional_meeting.subtitle",
                                title: "modal.workshop-display.professional-meeting-modal.submit_incidence.professional_meeting.title"
                            }
                        },
                        {
                            reference: "NoticeNonCoverageModalComponent",
                            type: "37",
                            componentId: "workshop-display",
                            reflabel: "Modal de aviso de cobertura no incluida",
                            modalId: "notice-non-coverage-modal",
                            mocked: true,
                            labels: {
                                continue: "modal.workshop-display.notice-non-coverage-modal.generic.continue",
                                cancel: "modal.workshop-display.notice-non-coverage-modal.generic.cancel",
                                title: "modal.workshop-display.notice-non-coverage-modal.title",
                                subtitle: "modal.workshop-display.notice-non-coverage-modal.subtitle"
                            }
                        }
                    ]
                }
            },
            CM0074: {
                id: 'CM0074',
                label: 'CM0074 Servicios adicionales',
                component_name: 'AdditionalWorkshopServicesComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up"]
                }
            },
            CM0075: {
                id: 'CM0075',
                label: 'CM0075 Gestion de cita',
                component_name: 'ServicesManagementComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["car"]
                }
            },
            CM0078: {
                id: 'CM0078',
                label: 'CM0078 Error en proceso',
                component_name: 'ProcessFailureComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-warning"],
                    showCallMe: false,
                    formCallMe: false,
                    showFinish: false,
                    title: 'Error en el proceso',
                    description: 'Ha ocurrido un error en el proceso. Por favor, inténtelo de nuevo en unos minutos.'
                }
            },
            CM0080: {
                id: 'CM0080',
                label: 'CM0080 Coberturas no incluidas',
                component_name: 'CoverageNotIncludedComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-earphones"]
                }
            },
            CM0081: {
                id: 'CM0081',
                label: 'CM0081 Gestión de servicios',
                component_name: 'ServiceManagementComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["car"]
                }
            },
            CM0082: {
                id: 'CM0082',
                label: 'CM0082 Listado Ofertas y Servicios',
                component_name: 'OffersListComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-without-results", "mapfre-plus-circle"]
                }
            },
            CM0083: {
                id: 'CM0083',
                label: 'CM0083 Detalle Ofertas y Servicios',
                component_name: 'OffersDetailDescriptionComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0086: {
                id: 'CM0086',
                label: 'CM0086 Tipo de asistencia - Auto',
                component_name: 'AssistanceRiskSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0087: {
                id: 'CM0087',
                label: 'CM0087 Tipo de avería',
                component_name: 'AssistanceTypeSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-warning", "mapfre-check", "mapfre-no-check", "mapfre-info-full"]
                }
            },
            CM0088: {
                id: 'CM0088',
                label: 'CM0088 Circunstancia de avería',
                component_name: 'AssistanceMotionSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-warning", "mapfre-check", "mapfre-no-check"]
                }
            },
            CM0089: {
                id: 'CM0089',
                label: 'CM0089 Seguimiento del profesional',
                component_name: 'AttendanceTrackingComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-crane"],
                    checkMap: true,
                    modals: [
                        {
                            reference: "AssistanceCancellationModalComponent",
                            type: "2",
                            reflabel: "Modal de cancelación de asistencia",
                            componentId: "attendance-tracking",
                            modalId: "assistance-cancellation-modal",
                            mocked: true,
                            assistance: {
                                assistance_id: "1"
                            },
                            labels: {
                                title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.title",
                                subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.subtitle",
                                no_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.no_button",
                                yes_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.yes_button",
                                confirmation_title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_title",
                                confirmation_subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_description"
                            }
                        }
                    ]
                }
            },
            CM0091: {
                id: 'CM0091',
                label: 'CM0091 Tipo de asistencia - Hogar',
                component_name: 'AssistanceTypeHomeComponent',
                params: {
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0092: {
                id: 'CM0092',
                label: 'CM0092 ¿Cuándo ha sucedido? - Hogar',
                component_name: 'HomeIncidenceDateSelectionComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0093: {
                id: 'CM0093',
                label: 'CM0093 ¿Qué te ha ocurrido?',
                component_name: 'HomeIncidenceTypeSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0094: {
                id: 'CM0094',
                label: 'CM0094 ¿Dónde se ha originado el daño?',
                component_name: 'HomeIncidenceLocationCardComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0095: {
                id: 'CM0095',
                label: 'CM0095 ¿Hay elementos dañados?',
                component_name: 'HomeIncidenceDamagedItemsCardComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"],
                    modals: [{
                            type: "2",
                            modalId: "assistance-cancellation-modal",
                            componentId: "attendance-tracking",
                            reference: 'AssistanceCancellationModalComponent',
                            reflabel: 'Modal de cancelación de asistencia',
                            labels: {
                                title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.title",
                                subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.subtitle",
                                no_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.no_button",
                                yes_button: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.yes_button",
                                confirmation_title: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_title",
                                confirmation_subtitle: "modal.attendance-tracking.assistance-cancellation-modal.request_assistance.assistance_cancelation.confirmation_description"
                            }
                        }]
                }
            },
            CM0096: {
                id: 'CM0096',
                label: 'CM0096 ¿Hay daños a terceros?',
                component_name: 'HomeIncidenceThirdPartyDamagesCardComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0097: {
                id: 'CM0097',
                label: 'CM0097 Tipo de daño',
                component_name: 'HomeIncidenceDamageTypeSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0098: {
                id: 'CM0098',
                label: 'CM0098 Causa del daño',
                component_name: 'HomeIncidenceDamageReasonSelectionComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0099: {
                id: 'CM0099',
                label: 'CM0099 ¿Está averiado?',
                component_name: 'HomeIncidenceDamageDetailComponent',
                params: {
                    styles: {}
                }
            },
            CM0104: {
                id: 'CM0104',
                label: 'CM0104 Ayuda',
                component_name: 'HelpComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-incoming-call"],
                    modals: [{
                            reference: "CallMeModalComponent",
                            type: "8",
                            reflabel: "Modal Llámame",
                            componentId: "help",
                            modalId: "call-me-modal",
                            mocked: true,
                            labels: {
                                title: "modal.help.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.help.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.help.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.help.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.help.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.help.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.help.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.help.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.help.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.help.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.help.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.help.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.help.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.help.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.help.call-me-modal.call_me_innactivity_modal.save_button",
                            }
                        }
                    ]
                }
            },
            CM0105: {
                id: 'CM0105',
                label: 'CM0105 Presupuestos pendientes',
                component_name: 'BudgetsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-car", "mapfre-home"]
                }
            },
            CM0105V1: {
                id: 'CM0105-V1',
                label: 'CM0105V1 Presupuestos pendientes',
                component_name: 'BudgetsV1Component',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-car", "mapfre-home"]
                }
            },
            CM0106: {
                id: 'CM0106',
                label: 'CM0106 Mi foto de perfil',
                component_name: 'UserPictureDataFormComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["md-create"],
                    modals: [
                        {
                            type: 5,
                            modalId: "profile-image-modal",
                            componentId: "user-picture-data-form",
                            reference: 'ProfileImageModalComponent',
                            reflabel: 'Modal de foto de perfil',
                            labels: {
                                upload_title: "modal.user-picture-data-form.profile-image-modal.personal_data_page.upload_title",
                                upload_image: "modal.user-picture-data-form.profile-image-modal.personal_data_page.upload_image",
                                upload_facebook: "modal.user-picture-data-form.profile-image-modal.personal_data_page.upload_facebook",
                                upload_twitter: "modal.user-picture-data-form.profile-image-modal.personal_data_page.upload_twitter",
                                upload_google: "modal.user-picture-data-form.profile-image-modal.personal_data_page.upload_google",
                                cancel_button: "modal.user-picture-data-form.profile-image-modal.close_session_modal.cancel_button",
                                accept_button: "modal.user-picture-data-form.profile-image-modal.close_session_modal.accept_button"
                            }
                        }
                    ]
                }
            },
            CM0107: {
                id: 'CM0107',
                label: 'CM0107 Canales de contacto',
                component_name: 'UserContactDataFormComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0108: {
                id: 'CM0108',
                label: 'CM0108 Mis claves de acceso',
                component_name: 'UserSecurityDataFormComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0114: {
                id: 'CM0114',
                label: 'CM0114 Datos personales',
                component_name: 'PersonalDataComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up"],
                    modals: [
                        {
                            reference: "RenovationModalComponent",
                            type: "19",
                            reflabel: "Modal de renovación de póliza",
                            componentId: "personal-data",
                            modalId: "renovation-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.personal-data.renovation-modal.accept_renovation.done.title',
                                description: 'modal.personal-data.renovation-modal.accept_renovation.done.subtitle'
                            }
                        }
                    ]
                }
            },
            CM0115: {
                id: 'CM0115',
                label: 'CM0115 Relación con Mapfre',
                component_name: 'UserRelationshipsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up"]
                }
            },
            CM0116: {
                id: 'CM0116',
                label: 'CM0116 Datos de contacto',
                component_name: 'ContactDataComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up", "mapfre-star", "mapfre-star", "mapfre-star"],
                    modals: [
                        {
                            reference: "RenovationModalComponent",
                            type: "19",
                            reflabel: "Modal de renovación de póliza",
                            componentId: "contact-data",
                            modalId: "renovation-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.contact-data.renovation-modal.accept_renovation.done.title',
                                description: 'modal.contact-data.renovation-modal.accept_renovation.done.subtitle'
                            }
                        }
                    ]
                }
            },
            CM0117: {
                id: 'CM0117',
                label: 'CM0117 Medios de pago',
                component_name: 'PaymentMethodsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up", "trash", "trash", "mapfre-information", "mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check"]
                }
            },
            CM0117V1: {
                id: 'CM0117V1',
                label: 'CM0117V1 Medios de pago',
                component_name: 'PaymentMethodsV1Component',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up", "trash", "trash", "mapfre-information", "mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check"]
                }
            },
            CM0057V1: {
                id: "CM0057",
                label: "CM0057 Tipo de siniestro",
                component_name: "SubmitIncidenceTypeV1Component",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check", "mapfre-info-full"]
                }
            },
            CM0118: {
                id: 'CM0118',
                label: 'CM0118 Mecanismos de firma',
                component_name: 'SignMethodsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up"]
                }
            },
            CM0119: {
                id: 'CM0119',
                label: 'CM0119 Permisos otorgados',
                component_name: 'GrantedPermissionsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up"],
                    modals: [
                        {
                            type: 4,
                            modalId: "communications-modal",
                            componentId: "granted-permissions",
                            reference: 'CommunicationsModalComponent',
                            reflabel: 'Modal de comunicaciones'
                        }
                    ]
                }
            },
            CM0120: {
                id: 'CM0120',
                label: 'CM0120 Mis preferencias',
                component_name: 'AccountDataComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up", "mapfre-arrow-up", "mapfre-arrow-up", "mapfre-pencil", "mapfre-pencil", "mapfre-pencil", "mapfre-arrow-up", "mapfre-facebook", "mapfre-twitter", "mapfre-google-plus", "mapfre-facebook", "mapfre-twitter", "mapfre-google-plus", "mapfre-arrow-up", "mapfre-arrow-up", "mapfre-check", "mapfre-no-check"],
                    modals: [
                        {
                            reference: "ChangedSecurityQuestionModalComponent",
                            type: "32",
                            componentId: "account-data",
                            reflabel: "Modal de cambio de preguntas de seguridad",
                            modalId: "changed-security-question-modal",
                            labels: {
                                first_answer: "modal.account-data.changed-security-question-modal.change_security_questions.first_answer",
                                first_question: "modal.account-data.changed-security-question-modal.change_security_questions.first_question",
                                second_answer: "modal.account-data.changed-security-question-modal.change_security_questions.second_answer",
                                second_question: "modal.account-data.changed-security-question-modal.change_security_questions.second_question",
                                subtitle: "modal.account-data.changed-security-question-modal.change_security_questions.subtitle",
                                title: "modal.account-data.changed-security-question-modal.change_security_questions.title",
                                accept: "modal.account-data.changed-security-question-modal.generic.accept",
                                cancel: "modal.account-data.changed-security-question-modal.generic.cancel"
                            }
                        },
                        {
                            reference: "ChangedPinModalComponent",
                            type: "31",
                            componentId: "account-data",
                            modalId: "changed-pin-modal",
                            reflabel: "Modal de cambio de PIN",
                            labels: {
                                new_pin: "modal.account-data.changed-pin-modal.change_pin.new_pin",
                                pins_mismatch: "modal.account-data.changed-pin-modal.change_pin.pins_mismatch",
                                repeat_pin: "modal.account-data.changed-pin-modal.change_pin.repeat_pin",
                                title: "modal.account-data.changed-pin-modal.change_pin.title",
                                confirm: "modal.account-data.changed-pin-modal.generic.confirm",
                                cancel: "modal.account-data.changed-pin-modal.generic.cancel"
                            }
                        },
                        {
                            reference: "LanguageChangedSuccessModalComponent",
                            type: "29",
                            componentId: "account-data",
                            modalId: "language-changed-success-modal",
                            reflabel: "Modal de confirmación de cambio de idioma",
                            labels: {
                                title: "modal.account-data.language-changed-success-modal.personal_data_page.account_info.languages.modal.title",
                                subtitle: "modal.account-data.language-changed-success-modal.personal_data_page.account_info.languages.modal.subtitle"
                            }
                        },
                        {
                            reference: "ChangedPasswordModalComponent",
                            type: "30",
                            componentId: "account-data",
                            modalId: "changed-password-modal",
                            reflabel: "Modal de cambio de contraseña",
                            labels: {
                                current_password: "modal.account-data.changed-password-modal.change_password.current_password",
                                new_password: "modal.account-data.changed-password-modal.change_password.new_password",
                                old_wrong: "modal.account-data.changed-password-modal.change_password.old_wrong",
                                passwords_mismatch: "modal.account-data.changed-password-modal.change_password.passwords_mismatch",
                                repeat_password: "modal.account-data.changed-password-modal.change_password.repeat_password",
                                subtitle: "modal.account-data.changed-password-modal.change_password.subtitle",
                                title: "modal.account-data.changed-password-modal.change_password.title",
                                accept: "modal.account-data.changed-password-modal.generic.accept",
                                cancel: "modal.account-data.changed-password-modal.generic.cancel",
                                forgot_password: "modal.account-data.changed-password-modal.login.forgot_password"
                            }
                        }
                    ]
                }
            },
            CM0121: {
                id: 'CM0121',
                label: 'CM0121 Mi perfil',
                component_name: 'ProfileImageComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["md-create"],
                    modals: [
                        {
                            type: 5,
                            modalId: "profile-image-modal",
                            componentId: "profile-image",
                            reference: 'ProfileImageModalComponent',
                            reflabel: 'Modal de foto de perfil',
                            labels: {
                                upload_title: "profile-image-modal.personal_data_page.upload_title",
                                upload_image: "profile-image-modal.personal_data_page.upload_image",
                                upload_facebook: "profile-image-modal.personal_data_page.upload_facebook",
                                upload_twitter: "profile-image-modal.personal_data_page.upload_twitter",
                                upload_google: "profile-image-modal.personal_data_page.upload_google",
                                cancel_button: "profile-image-modal.close_session_modal.cancel_button",
                                accept_button: "profile-image-modal.close_session_modal.accept_button"
                            }
                        }
                    ]
                }
            },
            CM0125: {
                label: 'CM0125 Tipo de fraccionamiento',
                component_name: 'FractionamentTypeSelectionComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0126: {
                label: 'CM0126 Plan de pagos',
                component_name: 'PaymentPlanComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0130: {
                id: 'CM0130',
                label: 'CM0130 Fecha de cancelación',
                component_name: 'CancellationDateComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CM0131: {
                id: 'CM0131',
                label: 'CM0131 Comparador de modalidades',
                component_name: 'PolicyModalitiesComparatorComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["checkmark", "mapfre-solid-radio-on", "mapfre-no-check", "mapfre-check", "mapfre-no-check", "checkmark"]
                }
            },
            CM0133: {
                id: 'CM0133',
                label: 'CM0133 Subir documentación',
                component_name: 'FileUploaderCardComponent',
                params: {
                    mocked: false,
                    styles: {},
                    title: {
                        es: 'Sube tus documentos'
                    },
                    description: {
                        es: 'Necesitamos que nos adjuntes los siguientes documentos:'
                    },
                    requiredFiles: {
                        es: 'DNI, Libro de familia'
                    }
                }
            },
            CM0135: {
                id: 'CM0135',
                label: 'CM0135 Seleccionar mejoras',
                component_name: 'UpgradeInsuranceUpgradeSelectionComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0135V1: {
                id: "CM0135V1",
                label: "CM0135V1 Seleccionar mejoras",
                component_name: "UpgradeInsuranceUpgradeSelectionV1Component",
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0136: {
                id: 'CM0136',
                label: 'CM0136 Motivos y comentarios',
                component_name: 'ReasonsCommentsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"],
                    title: 'Razones y comentarios',
                    subtitle: 'Por favor, indique sus razones o comentarios',
                    showInputReasons: true,
                }
            },
            CM0137: {
                id: 'CM0137',
                label: 'CM0137 Documentos del siniestro',
                component_name: 'SinisterDocumentationComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-upload-doc", "mapfre-upload-doc", "search", "add", "search", "add"],
                    modals: [
                        {
                            reference: "ImageSliderModalComponent",
                            type: "35",
                            reflabel: "Carroussel de imágenes",
                            componentId: "sinister-documentation",
                            modalId: "image-slider-modal"
                        },
                        {
                            reference: "MailMeModalComponent",
                            type: "34",
                            componentId: "sinister-documentation",
                            reflabel: "Confirmación por e-mail",
                            modalId: "mail-me-modal",
                            labels: {
                                save: "modal.process-confirmation.mail-me-modal.generic.save",
                                cancel: "modal.process-confirmation.mail-me-modal.generic.cancel",
                                mail: "modal.process-confirmation.mail-me-modal.new_account.mail",
                                privacy_check1: "modal.process-confirmation.mail-me-modal.new_account.privacy_check1",
                                privacy_check2: "modal.process-confirmation.mail-me-modal.new_account.privacy_check2",
                                privacy_check3: "modal.process-confirmation.mail-me-modal.new_account.privacy_check3",
                                privacy_check4: "modal.process-confirmation.mail-me-modal.new_account.privacy_check4",
                                privacy_check5: "modal.process-confirmation.mail-me-modal.new_account.privacy_check5",
                                confirmation: "modal.process-confirmation.mail-me-modal.mail_me.confirmation",
                                description: "modal.process-confirmation.mail-me-modal.mail_me.description",
                                mail_me_button: "modal.process-confirmation.mail-me-modal.mail_me.mail_me_button",
                                title: "modal.process-confirmation.mail-me-modal.mail_me.title",
                                want_update_your_data_description: "modal.process-confirmation.mail-me-modal.mail_me.mail",
                                want_update_your_data_title: "modal.process-confirmation.mail-me-modal.mail_me.mail"
                            }
                        }
                    ]
                }
            },
            CM0138: {
                id: 'CM0138',
                label: 'CM0138 Formulario de quejas y/o reclamaciones',
                component_name: 'ClaimFormSimpleComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0139: {
                id: 'CM0139',
                label: 'CM0139 Detalle del fraccionamiento del pago',
                component_name: 'FractionamentPaymentResultComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-money-bag"]
                }
            },
            CM0141: {
                id: 'CM0141',
                label: 'CM0141 Te llamamos',
                component_name: 'SubmitIncidenceCallMeBackComponent',
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-rocket"]
                }
            },
            CM0142: {
                id: 'CM0142',
                label: 'CM0142 Datos de asistencia',
                component_name: 'AssistancesComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0143: {
                id: 'CM0143',
                label: 'CM0143 Footer acceso redes sociales',
                component_name: 'MapfreNetworksFooterComponent',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0144: {
                id: 'CM0144',
                label: 'CM0144 Componente HTML',
                component_name: 'HtmlComponent',
                params: {
                    mocked: false,
                    styles: {},
                    content: {
                        es: ""
                    }
                }
            },
            CM0145: {
                id: 'CM0145',
                label: 'CM0145 Componente texto enriquecido',
                component_name: 'RichTextComponent',
                params: {
                    mocked: false,
                    styles: {},
                    content: {
                        es: ""
                    }
                }
            },
            CM0056V2: {
                id: "CM0056V2",
                label: "CM0056V2 Selección de riesgo",
                component_name: "InsuranceSelectionV2Component",
            },
            CPOT0001: {
                id: "CPOT0001",
                label: "CPOT0001 Beneficios cliente MAPFRE",
                component_name: "BenefitsBannerComponent",
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CPOT0002: {
                id: "CPOT0002",
                label: "CPOT0002 Selección de presupuesto",
                component_name: "BudgetSelectionComponent",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-no-check"]
                }
            },
            CPOT0003: {
                id: "CPOT0003",
                label: "CPOT0003 Datos de tu presupuesto",
                component_name: "PotentialBudgetDetailsComponent",
                params: {
                    mocked: false,
                    styles: {},
                    modals: [
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de confirmación",
                            componentId: "potential-budget-details",
                            modalId: "information-modal",
                            icon: 'mapfre-warning',
                            mocked: true,
                            labels: {
                                cancel: "modal.potential-budget-details.information-modal.generic.cancel",
                                accept: "modal.potential-budget-details.information-modal.generic.continue",
                                title: 'modal.potential-budget-details.information-modal.title',
                                subtitle: 'modal.potential-budget-details.information-modal.description'
                            },
                            hideButtons: false
                        }
                    ]
                }
            },
            CPOT0008: {
                id: "CPOT0008",
                label: "CPOT0008 Tu oficina más cercana",
                component_name: "NearestOfficeComponent",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-location", "mapfre-incoming-call", "mapfre-incoming-call"]
                }
            },
            CPOT0009: {
                id: "CPOT0009",
                label: "CPOT0009 Información importante de tu presupuesto",
                component_name: "ImportantBudgetInformationComponent",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-information"],
                    modals: [
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal llámame",
                            componentId: "important-budget-information",
                            modalId: "information-modal",
                            mocked: true,
                            type: 4,
                            icon: 'mapfre-screwdriver-and-wrench',
                            labels: {
                                title: "modal.important-budget-information.information-modal.title",
                                subtitle: "modal.important-budget-information.information-modal.description",
                                accept: "modal.important-budget-information.information-modal.call_me_button",
                                cancel: "modal.important-budget-information.information-modal.cancel_button"
                            }
                        }
                    ]
                }
            },
            CPOT0007: {
                id: "CPOT0007",
                label: "CPOT0007 Confirmación de aceptación de tu presupuesto",
                component_name: "PotentialClientConfirmationComponent",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-check", "mapfre-download", "mapfre-download"]
                }
            },
            CPOT0006: {
                id: "CPOT0006",
                label: "CPOT0006 Call me – Aceptación de tu presupuesto",
                component_name: "PotentialClientCallmeComponent",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ['mapfre-rocket']
                }
            },
            CPOT0004: {
                id: "CPOT0004",
                label: "CPOT0004 Datos adicionales",
                component_name: "PotentialClientAdditionalDataComponent",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-info-full", "mapfre-check", "mapfre-no-check"]
                }
            },
            CPOT0005: {
                id: "CPOT0005",
                label: "CPOT0005 Datos para el área de clientes",
                component_name: "PotentialClientPrivateAreaDataComponent",
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CPOT0010: {
                id: "CPOT0010",
                label: "CPOT0010 Siniestro no cliente",
                component_name: "SinisterNoClientComponent",
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0131V1: {
                id: "CM0131V1",
                label: "CM0131V1 Comparador de modalidades",
                component_name: "PolicyModalitiesComparatorV1Component",
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0070V1: {
                id: "CM0070V1",
                label: "CM0070V1 Daños contrarios",
                component_name: "ContraryDamagesFormV1Component",
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            CM0115V1: {
                id: "CM0115V1",
                label: "CM0115V1 Relación con Mapfre",
                component_name: "UserRelationshipsV1Component",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-arrow-up"]
                }
            },
            SubcabeceraV1: {
                id: 'subheaderWrapperV1',
                component_name: 'SubheaderV1Component',
                label: 'Subcabecera',
                customizable: true,
                params: {
                    styles: {},
                    modals: [
                        {
                            reference: "UrgentAssistanceModalComponent",
                            type: "9",
                            reflabel: "Modal de asistencia urgente",
                            componentId: "subheader",
                            modalId: "urgent-assistance-modal",
                            mocked: true,
                            labels: {
                                success_title: "urgent-assistance-modal.urgent_assistance_modal.success.title",
                                success_subtitle: "urgent-assistance-modal.urgent_assistance_modal.success.subtitle",
                                success_date: "urgent-assistance-modal.urgent_assistance_modal.success.date",
                                title: "urgent-assistance-modal.urgent_assistance_modal.title",
                                subtitle: "urgent-assistance-modal.urgent_assistance_modal.subtitle",
                                skip_button: "urgent-assistance-modal.urgent_assistance_modal.skip_button",
                                continue_button: "urgent-assistance-modal.urgent_assistance_modal.continue_button",
                                accept_button: "urgent-assistance-modal.urgent_assistance_modal.accept_button",
                                cancel_button: "urgent-assistance-modal.urgent_assistance_modal.cancel_button"
                            }
                        }
                    ]
                }
            },
            CM0046V2: {
                id: "CM0046V2",
                label: "CM0046V2  Método de pago",
                component_name: "PaymentMethodSelectionV2Component",
                params: {
                    mocked: false,
                    styles: {},
                    icons: ["mapfre-minus-circle", "mapfre-plus-circle", "mapfre-check", "mapfre-no-check", "mapfre-check", "mapfre-no-check", "mapfre-minus-circle", "mapfre-plus-circle"],
                    modals: [
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept"
                            },
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.delay'),
                            hideButtons: true
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.call.title'),
                            subtitle: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.payment-method-creation.call.description'),
                            visualizeButtonAccept: true,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept"
                            },
                            hideButtons: true,
                            formCallMe: true
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.payment-method-creation.delay'
                            },
                            hideButtons: true
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            type: 0,
                            eventName: 'acceptDelay',
                            hideButtons: true,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.policy_extension.hours.title',
                                subtitle: 'modal.payment-method-selection.information-modal.policy_extension.hours.description'
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.payment-method-selection.information-modal.generic.accept')
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "payment-method-selection",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            type: 0,
                            labels: {
                                cancel: "modal.payment-method-selection.information-modal.generic.cancel",
                                finish: "modal.payment-method-selection.information-modal.generic.finish",
                                accept: "modal.payment-method-selection.information-modal.generic.accept",
                                title: 'modal.payment-method-selection.information-modal.policy_extension.call.title',
                                subtitle: 'modal.payment-method-selection.information-modal.policy_extension.call.description'
                            },
                            eventName: 'acceptDelay',
                            hideButtons: true,
                        },
                        {
                            reference: "ProcessFailureModalComponent",
                            type: "11",
                            reflabel: "Modal de fallo del proceso",
                            componentId: "payment-method-selection",
                            modalId: "process-failure-modal",
                            icon: 'mapfre-cross-error',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.process-failure-modal.payment-method-creation.cancel')
                        },
                        {
                            reference: "PolicyExtensionModalComponent",
                            type: "27",
                            reflabel: "Modal de extensión de pólizas",
                            componentId: "payment-method-selection",
                            modalId: "policy-extension-modal",
                            policyId: 1,
                            mocked: true,
                            method: 'credit-card',
                            labels: {
                                cancel: "modal.payment-method-selection.policy-extension-modal.generic.cancel",
                                apply: "modal.payment-method-selection.policy-extension-modal.generic.apply"
                            }
                        },
                        {
                            reference: "ProcessConfirmationModalComponent",
                            type: "28",
                            reflabel: "Modal de confirmación de proceso",
                            componentId: "payment-method-selection",
                            modalId: "process-confirmation-modal",
                            icon: 'mapfre-check',
                            title: this.translationProvider.getValueForKey('modal.payment-method-selection.process-confirmation-modal.payment-method-creation.confirmation')
                        }
                    ]
                }
            },
            FabV1: {
                id: 'fabGroupV1Wrapper',
                component_name: 'FabGroupV1Component',
                label: 'Componente FAB (sólo visible en móvil y Apps)',
                customizable: true,
                params: {
                    modals: [
                        {
                            reference: "UrgentAssistanceModalComponent",
                            type: "9",
                            reflabel: "Modal de asistencia urgente",
                            componentId: "fab-group",
                            modalId: "urgent-assistance-modal",
                            mocked: true,
                            labels: {
                                success_title: "urgent-assistance-modal.urgent_assistance_modal.success.title",
                                success_subtitle: "urgent-assistance-modal.urgent_assistance_modal.success.subtitle",
                                success_date: "urgent-assistance-modal.urgent_assistance_modal.success.date",
                                title: "urgent-assistance-modal.urgent_assistance_modal.title",
                                subtitle: "urgent-assistance-modal.urgent_assistance_modal.subtitle",
                                skip_button: "urgent-assistance-modal.urgent_assistance_modal.skip_button",
                                continue_button: "urgent-assistance-modal.urgent_assistance_modal.continue_button",
                                accept_button: "urgent-assistance-modal.urgent_assistance_modal.accept_button",
                                cancel_button: "urgent-assistance-modal.urgent_assistance_modal.cancel_button"
                            }
                        }
                    ]
                }
            },
            CM0138V1: {
                id: 'CM0138V1',
                label: 'CM0138V1 Formulario de quejas y/o reclamaciones',
                component_name: 'ClaimFormSimpleV1Component',
                params: {
                    mocked: false,
                    styles: {}
                }
            },
            BloqueListadoPoliza: {
                id: 'Bloque listado de pólizas',
                label: 'Bloque listado de pólizas (CM0036, CM0037, CM0132)',
                component_name: 'PolicyListComponent',
                params: {
                    mocked: false,
                    styles: {},
                    modals: [
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información",
                            componentId: "policy-data",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            mocked: true,
                            labels: {
                                cancel: "modal.policy-data.information-modal.generic.cancel",
                                finish: "modal.policy-data.information-modal.information-modal.generic.finish",
                                accept: "modal.policy-data.information-modal.information-modal.generic.accept",
                                title: "modal.policy-data.information-modal.policy-data.policy_update_feedback.changes_on_the_way.title",
                                subtitle: "modal.policy-data.information-modal.policy-data.policy_update_feedback.changes_on_the_way.subtitle"
                            },
                            hideButtons: true
                        },
                        {
                            reference: "PolicyUpdateSuccessWithPaymentModalComponent",
                            type: "22",
                            reflabel: "Modal de aviso de impacto en prima",
                            componentId: "policy-data",
                            modalId: "policy-update-success-with-payment-modal",
                            mocked: true,
                            labels: {
                                title: "modal.policy-data.policy-update-success-with-payment-modal.policy_update_feedback.success.with_payment.title",
                                subtitle: "modal.policy-data.policy-update-success-with-payment-modal.policy_update_feedback.success.with_payment.subtitle",
                                cancel_button: "modal.policy-data.policy-update-success-with-payment-modal.policy_update_feedback.success.with_payment.cancel_button",
                                continue_button: "modal.policy-data.policy-update-success-with-payment-modal.policy_update_feedback.success.with_payment.continue_button"
                            }
                        },
                        {
                            reference: "RenovationModalComponent",
                            type: "19",
                            reflabel: "Modal de renovación de póliza (pendiente)",
                            componentId: "car-policy",
                            modalId: "renovation-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.car-policy.renovation-modal.accept_renovation.pending.title',
                                description: 'modal.car-policy.renovation-modal.accept_renovation.pending.subtitle'
                            }
                        },
                        {
                            reference: "RenovationModalComponent",
                            type: "19",
                            reflabel: "Modal de renovación de póliza (hecho)",
                            componentId: "car-policy",
                            modalId: "renovation-modal",
                            mocked: true,
                            labels: {
                                title: 'modal.car-policy.renovation-modal.accept_renovation.done.title',
                                description: 'modal.car-policy.renovation-modal.accept_renovation.done.subtitle'
                            }
                        },
                        {
                            reference: "PolicyCancelModalComponent",
                            type: "20",
                            reflabel: "Modal de cancelar póliza (auto)",
                            componentId: "car-policy",
                            modalId: "policy-cancel-modal",
                            riskId: 1,
                            policyId: 1,
                            mocked: true,
                            labels: {
                                title: "modal.car-policy.policy-cancel-modal.cancel_policy.confirm_modal.title",
                                policy: "modal.car-policy.policy-cancel-modal.cancel_policy.confirm_modal.policy",
                                reason: "modal.car-policy.policy-cancel-modal.cancel_policy.confirm_modal.reason",
                                request_button: "modal.car-policy.policy-cancel-modal.cancel_policy.confirm_modal.request_button",
                                procedure_title: "modal.car-policy.policy-cancel-modal.cancel_policy.confirm_modal.procedure.title",
                                procedure_subtitle: "modal.car-policy.policy-cancel-modal.cancel_policy.confirm_modal.procedure.subtitle"
                            }
                        },
                        {
                            reference: "CallMeModalComponent",
                            type: "8",
                            reflabel: "Modal de Llámame (auto)",
                            componentId: "car-policy",
                            modalId: "call-me-modal",
                            mocked: true,
                            labels: {
                                title: "modal.car-policy.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.car-policy.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.car-policy.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.car-policy.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.car-policy.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.car-policy.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.car-policy.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.car-policy.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.car-policy.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.car-policy.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.car-policy.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.car-policy.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.car-policy.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.car-policy.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.car-policy.call-me-modal.call_me_innactivity_modal.save_button",
                            }
                        },
                        {
                            reference: "RefuseRenewalModalComponent",
                            type: "21",
                            reflabel: "Modal de anulación de renovación  (auto)",
                            componentId: "car-policy",
                            modalId: "refuse-renewal-modal",
                            mocked: true,
                            labels: {
                                callme_button: "modal.car-policy.refuse_renewal_modal.refuse-renewal.callme_button",
                                cancel_button: "modal.car-policy.refuse_renewal_modal.refuse-renewal.cancel_button",
                                continue_button: "modal.car-policy.refuse_renewal_modal.refuse-renewal.continue_button",
                                lastview_subtitle: "modal.car-policy.refuse_renewal_modal.refuse-renewal.lastview.subtitle",
                                lastview_title: "modal.car-policy.refuse_renewal_modal.refuse-renewal.lastview.title",
                                subtitle: "modal.car-policy.refuse_renewal_modal.refuse-renewal.subtitle",
                                thanks_subtitle: "modal.car-policy.refuse_renewal_modal.refuse-renewal.thanks.subtitle",
                                thanks_title: "modal.car-policy.refuse_renewal_modal.refuse-renewal.thanks.title",
                                title: "modal.car-policy.refuse_renewal_modal.refuse-renewal.title"
                            }
                        },
                        {
                            reference: "PolicyCancelModalComponent",
                            type: "20",
                            reflabel: "Modal de cancelar póliza  (hogar)",
                            componentId: "home-policy",
                            modalId: "policy-cancel-modal",
                            riskId: 1,
                            policyId: 1,
                            mocked: true,
                            labels: {
                                title: "modal.home-policy.policy-cancel-modal.cancel_policy.confirm_modal.title",
                                policy: "modal.home-policy.policy-cancel-modal.cancel_policy.confirm_modal.policy",
                                reason: "modal.home-policy.policy-cancel-modal.cancel_policy.confirm_modal.reason",
                                request_button: "modal.home-policy.policy-cancel-modal.cancel_policy.confirm_modal.request_button",
                                procedure_title: "modal.home-policy.policy-cancel-modal.cancel_policy.confirm_modal.procedure.title",
                                procedure_subtitle: "modal.home-policy.policy-cancel-modal.cancel_policy.confirm_modal.procedure.subtitle"
                            }
                        },
                        {
                            reference: "CallMeModalComponent",
                            type: "8",
                            reflabel: "Modal de Llámame (hogar)",
                            componentId: "home-policy",
                            modalId: "call-me-modal",
                            mocked: true,
                            labels: {
                                title: "modal.home-policy.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.home-policy.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.home-policy.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.home-policy.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.home-policy.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.home-policy.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.home-policy.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.home-policy.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.home-policy.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.home-policy.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.home-policy.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.home-policy.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.home-policy.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.home-policy.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.home-policy.call-me-modal.call_me_innactivity_modal.save_button",
                            }
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal de Información (hogar)",
                            componentId: "home-policy",
                            modalId: "information-modal",
                            icon: 'mapfre-information',
                            mocked: true,
                            labels: {
                                cancel: "modal.home-policy.information-modal.generic.cancel",
                                finish: "modal.home-policy.information-modal.generic.finish",
                                accept: "modal.home-policy.information-modal.generic.accept",
                                title: 'modal.home-policy.information-modal.policy.home-policy.fractionament.alert.title',
                                subtitle: 'modal.home-policy.information-modal.policy.home-policy.fractionament.alert.subtitle'
                            },
                        },
                        {
                            reference: "RefuseRenewalModalComponent",
                            type: "21",
                            reflabel: "Modal de anulación de renovación (hogar)",
                            componentId: "home-policy",
                            modalId: "refuse-renewal-modal",
                            mocked: true,
                            labels: {
                                callme_button: "modal.home-policy.refuse_renewal_modal.refuse-renewal.callme_button",
                                cancel_button: "modal.home-policy.refuse_renewal_modal.refuse-renewal.cancel_button",
                                continue_button: "modal.home-policy.refuse_renewal_modal.refuse-renewal.continue_button",
                                lastview_subtitle: "modal.home-policy.refuse_renewal_modal.refuse-renewal.lastview.subtitle",
                                lastview_title: "modal.home-policy.refuse_renewal_modal.refuse-renewal.lastview.title",
                                subtitle: "modal.home-policy.refuse_renewal_modal.refuse-renewal.subtitle",
                                thanks_subtitle: "modal.home-policy.refuse_renewal_modal.refuse-renewal.thanks.subtitle",
                                thanks_title: "modal.home-policy.refuse_renewal_modal.refuse-renewal.thanks.title",
                                title: "modal.home-policy.refuse_renewal_modal.refuse-renewal.title"
                            }
                        },
                        {
                            reference: "ProcessFailureModalComponent",
                            type: "11",
                            reflabel: "Modal de fallo de cancelación",
                            componentId: "cancel-policy-question",
                            modalId: "process-failure-modal",
                            mocked: true,
                            labels: {
                                title: "modal.cancel-policy-question.process-failure-modal.cancel_policy.result.failure.title",
                                description: "modal.cancel-policy-question.process-failure-modal.cancel_policy.result.failure.description"
                            }
                        },
                        {
                            reference: "InformationModalComponent",
                            type: "7",
                            reflabel: "Modal del proceso de cancelación",
                            componentId: "cancel-policy-question",
                            modalId: "information-modal",
                            icon: 'mapfre-rocket',
                            mocked: true,
                            labels: {
                                cancel: "modal.cancel-policy-question.information-modal.generic.cancel",
                                finish: "modal.cancel-policy-question.information-modal.generic.finish",
                                accept: "modal.cancel-policy-question.information-modal.generic.accept",
                                title: 'modal.cancel-policy-question.information-modal.cancelation_process.modal_cancelation.title',
                                subtitle: 'modal.cancel-policy-question.information-modal.cancelation_process.modal_cancelation.description'
                            },
                            hideButtons: true
                        }
                    ]
                }
            },
            BloqueListadoRecibos: {
                id: 'Bloque Listado de recibos',
                label: 'Bloque Listado de recibos (CM0038, CM0039, CM0124)',
                component_name: 'PaymentReceiptsComponent',
                params: {
                    mocked: false,
                    styles: {},
                    modals: [
                        {
                            reference: "CallMeModalComponent",
                            type: "8",
                            reflabel: "Modal Llámame",
                            componentId: "popover-receipt-payment",
                            modalId: "call-me-modal",
                            mocked: true,
                            labels: {
                                title: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.save_button",
                            }
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "popover-receipt-payment",
                            modalId: "information-modal",
                            mocked: true,
                            title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.title'),
                            subtitle: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.description'),
                            icon: 'mapfre-warning',
                            type: 3,
                            confirmContent: {
                                flowReference: true,
                                icon: 'mapfre-check',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.confirmation.title')
                            },
                            cancelContent: {
                                icon: 'mapfre-cross-error',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.cancel')
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.generic.continue')
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "popover-receipt-payment",
                            modalId: "information-modal",
                            mocked: true,
                            title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.title'),
                            subtitle: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.description'),
                            icon: 'mapfre-warning',
                            type: 3,
                            confirmContent: {
                                flowReference: true,
                                icon: 'mapfre-check',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.confirmation.title'),
                                description: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.confirmation.description')
                            },
                            showRating: ' ',
                            cancelContent: {
                                icon: 'mapfre-cross-error',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.cancel')
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.generic.confirm')
                        },
                        {
                            reference: "PaymentGroupingComponent",
                            type: "13",
                            reflabel: "Modal de unificar pagos",
                            componentId: "popover-receipt-payment",
                            modalId: "payment-grouping",
                            mocked: true,
                            labels: {
                                title: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.title",
                                description_line1: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.description.line1",
                                description_line2: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.description.line2",
                                day_selection_placeholder: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.day_selection.placeholder",
                                cancel: "modal.popover-receipt-payment.payment-grouping.generic.cancel",
                                confirm: "modal.popover-receipt-payment.payment-grouping.generic.confirm",
                                confirmation: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.confirmation",
                                action_selectday_cancel: "modal.popover-receipt-payment.payment-grouping.my_receipts.action.selectday.cancel"
                            }
                        },
                        {
                            reference: "AmountDivisionsModalComponent",
                            type: "14",
                            reflabel: "Modal de dividir importe",
                            componentId: "popover-receipt-payment",
                            modalId: "amount-divisions-modal",
                            mocked: true,
                            labels: {
                                title: "modal.popover-receipt-payment.amount-divisions-modal.confirmation.title",
                                confirm: "modal.popover-receipt-payment.amount_divisions_modal.generic.confirm",
                                cancel: "modal.popover-receipt-payment.amount_divisions_modal.generic.cancel",
                                finalize: "modal.popover-receipt-payment.amount_divisions_modal.generic.finalize"
                            }
                        },
                        {
                            reference: "AddReceiptsToPayModalComponent",
                            type: "15",
                            reflabel: "Modal de añadir recibos",
                            componentId: "popover-receipt-payment",
                            modalId: "add-receipts-to-pay-modal",
                            mocked: true,
                            labels: {
                                add: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.add",
                                cancel: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.cancel",
                                title: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.title",
                                warning: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.warning"
                            }
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "receipt-fractionate-question",
                            modalId: "information-modal",
                            cancelContent: {
                                icon: 'mapfre-cross-error',
                                title: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.receipt_fractionate.process_cancel')
                            },
                            icon: 'mapfre-information',
                            labels: {
                                cancel: "modal.receipt-fractionate-question.information-modal.generic.cancel",
                                finish: "modal.receipt-fractionate-question.information-modal.generic.finish",
                                accept: "modal.receipt-fractionate-question.information-modal.generic.accept"
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.generic.continue'),
                            subtitle: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.fractionament.alert.subtitle'),
                            title: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.fractionament.alert.title'),
                            type: 2,
                            varSubtitle: '17/07/2018'
                        }
                    ]
                }
            },
            BloqueListadoRecibosV1: {
                id: 'Bloque Listado de recibos',
                label: 'Bloque Listado de recibos (CM0038, CM0039V1, CM0124)',
                component_name: 'PaymentReceiptsV1Component',
                params: {
                    mocked: false,
                    styles: {},
                    modals: [
                        {
                            reference: "CallMeModalComponent",
                            type: "8",
                            reflabel: "Modal Llámame",
                            componentId: "popover-receipt-payment",
                            modalId: "call-me-modal",
                            mocked: true,
                            labels: {
                                title: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.popover-receipt-payment.call-me-modal.call_me_innactivity_modal.save_button",
                            }
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "popover-receipt-payment",
                            modalId: "information-modal",
                            mocked: true,
                            title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.title'),
                            subtitle: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.description'),
                            icon: 'mapfre-warning',
                            type: 3,
                            confirmContent: {
                                flowReference: true,
                                icon: 'mapfre-check',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.confirmation.title')
                            },
                            cancelContent: {
                                icon: 'mapfre-cross-error',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.cancel')
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.generic.continue')
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "popover-receipt-payment",
                            modalId: "information-modal",
                            mocked: true,
                            title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.title'),
                            subtitle: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.description'),
                            icon: 'mapfre-warning',
                            type: 3,
                            confirmContent: {
                                flowReference: true,
                                icon: 'mapfre-check',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.confirmation.title'),
                                description: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.give_back_amount_modal.confirmation.description')
                            },
                            showRating: ' ',
                            cancelContent: {
                                icon: 'mapfre-cross-error',
                                title: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.my_receipts.action.cancel.cancel')
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.popover-receipt-payment.information-modal.generic.confirm')
                        },
                        {
                            reference: "PaymentGroupingComponent",
                            type: "13",
                            reflabel: "Modal de unificar pagos",
                            componentId: "popover-receipt-payment",
                            modalId: "payment-grouping",
                            mocked: true,
                            labels: {
                                title: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.title",
                                description_line1: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.description.line1",
                                description_line2: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.description.line2",
                                day_selection_placeholder: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.day_selection.placeholder",
                                cancel: "modal.popover-receipt-payment.payment-grouping.generic.cancel",
                                confirm: "modal.popover-receipt-payment.payment-grouping.generic.confirm",
                                confirmation: "modal.popover-receipt-payment.payment-grouping.receipt_grouping.confirmation",
                                action_selectday_cancel: "modal.popover-receipt-payment.payment-grouping.my_receipts.action.selectday.cancel"
                            }
                        },
                        {
                            reference: "AmountDivisionsModalComponent",
                            type: "14",
                            reflabel: "Modal de dividir importe",
                            componentId: "popover-receipt-payment",
                            modalId: "amount-divisions-modal",
                            mocked: true,
                            labels: {
                                title: "modal.popover-receipt-payment.amount-divisions-modal.confirmation.title",
                                confirm: "modal.popover-receipt-payment.amount_divisions_modal.generic.confirm",
                                cancel: "modal.popover-receipt-payment.amount_divisions_modal.generic.cancel",
                                finalize: "modal.popover-receipt-payment.amount_divisions_modal.generic.finalize"
                            }
                        },
                        {
                            reference: "AddReceiptsToPayModalComponent",
                            type: "15",
                            reflabel: "Modal de añadir recibos",
                            componentId: "popover-receipt-payment",
                            modalId: "add-receipts-to-pay-modal",
                            mocked: true,
                            labels: {
                                add: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.add",
                                cancel: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.cancel",
                                title: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.title",
                                warning: "modal.popover-receipt-payment.add-receipts-to-pay-modal.add_receipt.warning"
                            }
                        },
                        {
                            reference: "InformationModalComponent",
                            reflabel: "Modal de Información",
                            componentId: "receipt-fractionate-question",
                            modalId: "information-modal",
                            cancelContent: {
                                icon: 'mapfre-cross-error',
                                title: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.receipt_fractionate.process_cancel')
                            },
                            icon: 'mapfre-information',
                            labels: {
                                cancel: "modal.receipt-fractionate-question.information-modal.generic.cancel",
                                finish: "modal.receipt-fractionate-question.information-modal.generic.finish",
                                accept: "modal.receipt-fractionate-question.information-modal.generic.accept"
                            },
                            rightButton: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.generic.continue'),
                            subtitle: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.fractionament.alert.subtitle'),
                            title: this.translationProvider.getValueForKey('modal.receipt-fractionate-question.information-modal.fractionament.alert.title'),
                            type: 2,
                            varSubtitle: '17/07/2018'
                        }
                    ]
                }
            },
            Cabecera: {
                id: 'headerWrapper',
                component_name: 'HeaderGroupComponent',
                label: 'Cabecera',
                customizable: true,
                params: {
                    styles: {},
                    modals: [
                        {
                            reference: "UrgentAssistanceModalComponent",
                            type: "9",
                            reflabel: "Modal de asistencia urgente",
                            componentId: "header-group",
                            modalId: "urgent-assistance-modal",
                            mocked: true,
                            labels: {
                                success_title: "urgent-assistance-modal.urgent_assistance_modal.success.title",
                                success_subtitle: "urgent-assistance-modal.urgent_assistance_modal.success.subtitle",
                                success_date: "urgent-assistance-modal.urgent_assistance_modal.success.date",
                                title: "urgent-assistance-modal.urgent_assistance_modal.title",
                                subtitle: "urgent-assistance-modal.urgent_assistance_modal.subtitle",
                                skip_button: "urgent-assistance-modal.urgent_assistance_modal.skip_button",
                                continue_button: "urgent-assistance-modal.urgent_assistance_modal.continue_button",
                                accept_button: "urgent-assistance-modal.urgent_assistance_modal.accept_button",
                                cancel_button: "urgent-assistance-modal.urgent_assistance_modal.cancel_button"
                            }
                        },
                        {
                            reference: "CallMeModalComponent",
                            type: "8",
                            reflabel: "Modal Llámame",
                            componentId: "help",
                            modalId: "call-me-modal",
                            mocked: true,
                            labels: {
                                title: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.title",
                                description: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.description",
                                mobile_phone: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.mobile_phone",
                                day_time: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.day_time",
                                privacy_check1: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check1",
                                privacy_check2: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check2",
                                privacy_check3: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check3",
                                privacy_check4: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check4",
                                privacy_check5: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.privacy_check5",
                                want_update_your_data_title: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.want_update_your_data_title",
                                want_update_your_data_description: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.want_update_your_data_description",
                                call_me_button: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.call_me_button",
                                confirmation: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.confirmation",
                                cancel_button: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.cancel_button",
                                save_button: "modal.upgrade-insurance-result.call-me-modal.call_me_innactivity_modal.save_button"
                            }
                        },
                        {
                            reference: "NoticeNonCoverageModalComponent",
                            type: "9",
                            reflabel: "Modal de aviso de no cobertura",
                            componentId: "header-group",
                            modalId: "notice-non-coverage-modal",
                            mocked: true,
                            labels: {
                                title: 'notice-non-coverage-modal.title',
                                subtitle: 'notice-non-coverage-modal.subtitle',
                                cancelButton: 'notice-non-coverage-modal.cancel_button',
                                acceptButton: 'notice-non-coverage-modal.accept_button'
                            }
                        },
                        {
                            reference: "CloseSessionModalComponent",
                            type: "10",
                            reflabel: "Salir de Autoservicio",
                            componentId: "header-group",
                            modalId: "close-session-modal",
                            labels: {
                                title: 'close_session_modal.title',
                                subtitle: 'close_session_modal.subtitle',
                                cancelButton: 'close_session_modal.cancel_button',
                                acceptButton: 'close_session_modal.accept_button'
                            }
                        }
                    ]
                }
            },
            Subcabecera: {
                id: 'subheaderWrapper',
                component_name: 'SubheaderComponent',
                label: 'Subcabecera',
                customizable: true,
                params: {
                    styles: {},
                    modals: [
                        {
                            reference: "UrgentAssistanceModalComponent",
                            type: "9",
                            reflabel: "Modal de asistencia urgente",
                            componentId: "subheader",
                            modalId: "urgent-assistance-modal",
                            mocked: true,
                            labels: {
                                success_title: "urgent-assistance-modal.urgent_assistance_modal.success.title",
                                success_subtitle: "urgent-assistance-modal.urgent_assistance_modal.success.subtitle",
                                success_date: "urgent-assistance-modal.urgent_assistance_modal.success.date",
                                title: "urgent-assistance-modal.urgent_assistance_modal.title",
                                subtitle: "urgent-assistance-modal.urgent_assistance_modal.subtitle",
                                skip_button: "urgent-assistance-modal.urgent_assistance_modal.skip_button",
                                continue_button: "urgent-assistance-modal.urgent_assistance_modal.continue_button",
                                accept_button: "urgent-assistance-modal.urgent_assistance_modal.accept_button",
                                cancel_button: "urgent-assistance-modal.urgent_assistance_modal.cancel_button"
                            }
                        }
                    ]
                }
            },
            Fondo: {
                id: 'pageBackgroundWrapper',
                component_name: 'PageBackgroundComponent',
                label: 'Fondo de página',
                params: {}
            },
            Pie: {
                id: 'mapfreFooterWrapper',
                component_name: 'MapfreFooterComponent',
                label: 'Pie de página',
                customizable: true,
                params: {
                    styles: {},
                    modals: [
                        {
                            reference: "FooterModalComponent",
                            type: "9",
                            reflabel: "Modal del pié de pagina",
                            componentId: "mapfre-footer",
                            modalId: "footer-modal",
                            mocked: true,
                            labels: {}
                        }
                    ]
                }
            },
            Fab: {
                id: 'fabGroupWrapper',
                component_name: 'FabGroupComponent',
                label: 'Componente FAB (sólo visible en móvil y Apps)',
                customizable: true,
                params: {
                    modals: [
                        {
                            reference: "UrgentAssistanceModalComponent",
                            type: "9",
                            reflabel: "Modal de asistencia urgente",
                            componentId: "fab-group",
                            modalId: "urgent-assistance-modal",
                            mocked: true,
                            labels: {
                                success_title: "urgent-assistance-modal.urgent_assistance_modal.success.title",
                                success_subtitle: "urgent-assistance-modal.urgent_assistance_modal.success.subtitle",
                                success_date: "urgent-assistance-modal.urgent_assistance_modal.success.date",
                                title: "urgent-assistance-modal.urgent_assistance_modal.title",
                                subtitle: "urgent-assistance-modal.urgent_assistance_modal.subtitle",
                                skip_button: "urgent-assistance-modal.urgent_assistance_modal.skip_button",
                                continue_button: "urgent-assistance-modal.urgent_assistance_modal.continue_button",
                                accept_button: "urgent-assistance-modal.urgent_assistance_modal.accept_button",
                                cancel_button: "urgent-assistance-modal.urgent_assistance_modal.cancel_button"
                            }
                        }
                    ]
                }
            },
            Tablinks: {
                id: 'tabLinksWrapper',
                component_name: 'TabLinksComponent',
                label: 'Componente barra de enlaces (sólo visible en móvil y Apps)',
                customizable: true,
                params: {
                    styles: {},
                    modals: [
                        {
                            reference: "UrgentAssistanceModalComponent",
                            type: "9",
                            reflabel: "Modal de asistencia urgente",
                            componentId: "tabLinksWrapper",
                            modalId: "urgent-assistance-modal",
                            mocked: true,
                            labels: {
                                ssuccess_title: "urgent-assistance-modal.urgent_assistance_modal.success.title",
                                success_subtitle: "urgent-assistance-modal.urgent_assistance_modal.success.subtitle",
                                success_date: "urgent-assistance-modal.urgent_assistance_modal.success.date",
                                title: "urgent-assistance-modal.urgent_assistance_modal.title",
                                subtitle: "urgent-assistance-modal.urgent_assistance_modal.subtitle",
                                skip_button: "urgent-assistance-modal.urgent_assistance_modal.skip_button",
                                continue_button: "urgent-assistance-modal.urgent_assistance_modal.continue_button",
                                accept_button: "urgent-assistance-modal.urgent_assistance_modal.accept_button",
                                cancel_button: "urgent-assistance-modal.urgent_assistance_modal.cancel_button"
                            }
                        }
                    ]
                }
            }
        };
        this.translationProvider.bind(this);
    }
    Components.decorators = [
        { type: Injectable },
    ];
    Components.ctorParameters = function () { return [
        { type: TranslationProvider, },
    ]; };
    return Components;
}());
export { Components };
//# sourceMappingURL=components.js.map