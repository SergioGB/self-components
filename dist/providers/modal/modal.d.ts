import { ElementRef } from '@angular/core';
export declare class ModalProvider {
    private helpModalActive;
    constructor();
    getHelpModalActive(): boolean;
    setHelpModalProvider(helpmodalActive: boolean): void;
    setupCentering(elementRef: ElementRef): void;
    center(elementRef: ElementRef): void;
    isModal(elementRef: ElementRef): boolean;
}
