import { Injectable } from '@angular/core';
import * as jQuery from "jquery";
var ModalProvider = (function () {
    function ModalProvider() {
        this.helpModalActive = false;
    }
    ModalProvider.prototype.getHelpModalActive = function () {
        return this.helpModalActive;
    };
    ModalProvider.prototype.setHelpModalProvider = function (helpmodalActive) {
        this.helpModalActive = helpmodalActive;
    };
    ModalProvider.prototype.setupCentering = function (elementRef) {
        var _this = this;
        window.addEventListener('resize', function () {
            _this.center(elementRef);
        });
        this.center(elementRef);
    };
    ModalProvider.prototype.center = function (elementRef) {
        setTimeout(function () {
            var modalElement = jQuery(elementRef.nativeElement);
            var modalWrapper = modalElement ? modalElement.parents('.modal-wrapper') : undefined;
            if (modalWrapper && modalWrapper.length) {
                var modalHeight = modalWrapper.outerHeight();
                var modalWidth = modalWrapper.outerWidth();
                var viewportHeight = jQuery(window).height();
                var viewportWidth = jQuery(window).width();
                if (modalHeight < viewportHeight) {
                    modalWrapper.css({
                        'top': "calc(50vh - " + modalHeight / 2 + "px)"
                    });
                }
                else {
                    modalWrapper.css({
                        'top': "0"
                    });
                }
                if (modalWidth < viewportWidth) {
                    modalWrapper.css({
                        'left': "calc(50vw - " + modalWidth / 2 + "px)"
                    });
                }
                else {
                    modalWrapper.css({
                        'left': "0"
                    });
                }
            }
        });
    };
    ModalProvider.prototype.isModal = function (elementRef) {
        var modalElement = jQuery(elementRef.nativeElement);
        var modalWrapper = modalElement ? modalElement.parents('.modal-wrapper') : undefined;
        return modalWrapper && modalWrapper.length;
    };
    ModalProvider.decorators = [
        { type: Injectable },
    ];
    ModalProvider.ctorParameters = function () { return []; };
    return ModalProvider;
}());
export { ModalProvider };
//# sourceMappingURL=modal.js.map