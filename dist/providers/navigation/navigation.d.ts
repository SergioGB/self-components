import { NavController, Events } from 'ionic-angular';
export declare class NavigationProvider {
    private readonly events;
    constructor(events: Events);
    navigateTo(navCtrl: NavController, page_ref: any, link_data: any, isFlow?: boolean): void;
}
