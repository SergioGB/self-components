import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
var NavigationProvider = (function () {
    function NavigationProvider(events) {
        this.events = events;
    }
    NavigationProvider.prototype.navigateTo = function (navCtrl, page_ref, link_data, isFlow) {
        var params = {
            navCtrl: navCtrl,
            page_ref: page_ref,
            link_data: link_data,
            isFlow: isFlow
        };
        this.events.publish('navigateTo', params);
    };
    NavigationProvider.decorators = [
        { type: Injectable },
    ];
    NavigationProvider.ctorParameters = function () { return [
        { type: Events, },
    ]; };
    return NavigationProvider;
}());
export { NavigationProvider };
//# sourceMappingURL=navigation.js.map