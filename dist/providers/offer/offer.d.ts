import { Response } from '@angular/http';
import { Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
import { Observable } from 'rxjs/Observable';
export declare class OfferProvider extends ApiProvider {
    protected injector: Injector;
    constructor(injector: Injector);
    getOfferGroups(mocked: boolean): Observable<Response>;
    getOffers(mocked: boolean, parameters: any): Observable<Response>;
    getFilteredOffers(mocked: boolean, parameters: any): Observable<Response>;
    getOfferDetail(mocked: boolean, offerId: String): Observable<Response>;
}
