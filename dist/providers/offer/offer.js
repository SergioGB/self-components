var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
var OfferProvider = (function (_super) {
    __extends(OfferProvider, _super);
    function OfferProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        return _this;
    }
    OfferProvider.prototype.getOfferGroups = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "catalogs/offer_groups", true);
    };
    OfferProvider.prototype.getOffers = function (mocked, parameters) {
        return this.getObservableForSuffix(mocked, false, "client/" + this.getClientId() + "/offers", true, parameters);
    };
    OfferProvider.prototype.getFilteredOffers = function (mocked, parameters) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/offers", true, parameters);
    };
    OfferProvider.prototype.getOfferDetail = function (mocked, offerId) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/offer/" + offerId, true);
    };
    OfferProvider.decorators = [
        { type: Injectable },
    ];
    OfferProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return OfferProvider;
}(ApiProvider));
export { OfferProvider };
//# sourceMappingURL=offer.js.map