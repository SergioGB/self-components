import { Injector } from '@angular/core';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
import { SummaryData } from '../../models/summary';
import { FlowRequest } from '../../models/flow-request';
export declare class OpenReceiptPaymentProvider extends AbstractFlowProvider {
    private injector;
    private receiptPayment;
    constructor(injector: Injector);
    cleanProvider(): void;
    getReceiptSelectedData(): any;
    setReceiptSelectedData(receiptSelectedData: any): void;
    getPaymentMethodSelectedData(): any;
    setPaymentMethodSelectedData(paymentMethodSelectedData: any): void;
    resetFlowData(): void;
    getRequestJson(formattedRequest: FlowRequest): FlowRequest;
    getSummaryData(): SummaryData;
    updateRequest(flowRequest: FlowRequest, callback: Function, mocked?: boolean): any;
    protected initializeFlowData(): void;
}
