var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
var OpenReceiptPaymentProvider = (function (_super) {
    __extends(OpenReceiptPaymentProvider, _super);
    function OpenReceiptPaymentProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.initializeFlowData();
        return _this;
    }
    OpenReceiptPaymentProvider.prototype.cleanProvider = function () {
        this.initializeFlowData();
    };
    OpenReceiptPaymentProvider.prototype.getReceiptSelectedData = function () {
        return this.receiptPayment.receiptSelectedData;
    };
    OpenReceiptPaymentProvider.prototype.setReceiptSelectedData = function (receiptSelectedData) {
        this.receiptPayment.receiptSelectedData = receiptSelectedData;
    };
    OpenReceiptPaymentProvider.prototype.getPaymentMethodSelectedData = function () {
        return this.receiptPayment.paymentMethodSelectedData;
    };
    OpenReceiptPaymentProvider.prototype.setPaymentMethodSelectedData = function (paymentMethodSelectedData) {
        this.receiptPayment.paymentMethodSelectedData = paymentMethodSelectedData;
    };
    OpenReceiptPaymentProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.receiptPayment = {};
    };
    OpenReceiptPaymentProvider.prototype.getRequestJson = function (formattedRequest) {
        return null;
    };
    OpenReceiptPaymentProvider.prototype.getSummaryData = function () {
        var sections = [];
        if (this.receiptPayment.receiptSelectedData) {
            sections.push({
                title: 'request_assistance_page.summary.receipt_title',
                rows: [
                    {
                        key: 'receipt_payment_page.payment_summary.receipt',
                        value: this.translationProvider.getValueForKey('open-receipt-payment.my_receipts.receipt_number') + " " + this.receiptPayment.receiptSelectedData.number
                    }, {
                        key: 'receipt_payment_page.payment_summary.amount',
                        value: this.formatProvider.formatCurrency(this.receiptPayment.receiptSelectedData.price)
                    }, {
                        key: 'receipt_payment_page.payment_summary.payment_type',
                        value: this.receiptPayment.receiptSelectedData.modality
                    }, {
                        key: 'receipt_payment_page.payment_summary.risk',
                        value: this.receiptPayment.receiptSelectedData.risk_name
                    }, {
                        key: 'receipt_payment_page.payment_summary.policy_type',
                        value: this.receiptPayment.receiptSelectedData.policy_type
                    }
                ]
            });
        }
        if (this.receiptPayment.paymentMethodSelectedData) {
            sections.push({
                title: 'request_assistance_page.summary.payment_title',
                rows: [
                    {
                        key: 'receipt_payment_page.payment_summary.payment_method',
                        value: this.translationProvider.getValueForKey(this.receiptPayment.paymentMethodSelectedData.type === 1
                            ? 'receipt_payment_page.method_selection.credit_card'
                            : 'receipt_payment_page.method_selection.bank_dom')
                    }, {
                        key: this.receiptPayment.paymentMethodSelectedData.id === 1
                            ? 'receipt_payment_page.payment_summary.credit_card'
                            : 'receipt_payment_page.payment_summary.account',
                        value: this.receiptPayment.paymentMethodSelectedData.type === 1
                            ? this.receiptPayment.paymentMethodSelectedData.credit_card_info.card_number
                            : this.receiptPayment.paymentMethodSelectedData.bank_account_info.account_number
                    }, {
                        key: 'receipt_payment_page.payment_summary.payment_date',
                        value: this.formatProvider.formatDate(new Date())
                    }
                ]
            });
        }
        return {
            title: 'receipt_payment_page.payment_summary.your_data',
            sections: sections
        };
    };
    OpenReceiptPaymentProvider.prototype.updateRequest = function (flowRequest, callback, mocked) {
        if (mocked) {
            callback({
                response: {}
            });
        }
        else {
        }
    };
    OpenReceiptPaymentProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'OpenReceiptPaymentStepsPage';
        this.resultPageName = 'OpenReceiptPaymentResultPage';
        this.resetFlowData();
    };
    OpenReceiptPaymentProvider.decorators = [
        { type: Injectable },
    ];
    OpenReceiptPaymentProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return OpenReceiptPaymentProvider;
}(AbstractFlowProvider));
export { OpenReceiptPaymentProvider };
//# sourceMappingURL=open-receipt-payment.js.map