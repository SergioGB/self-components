var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { ComponentSettingsProvider } from '../../providers/component-settings/component-settings';
import * as _ from 'lodash';
import { Mocks } from '../mocks/mocks';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
var OpenRequestAssistanceProvider = (function (_super) {
    __extends(OpenRequestAssistanceProvider, _super);
    function OpenRequestAssistanceProvider(injector, componentSettingsProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.componentSettingsProvider = componentSettingsProvider;
        _this.claimId = 1;
        _this.componentName = 'request-assistance';
        _this.initializeFlowData();
        return _this;
    }
    OpenRequestAssistanceProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'OpenRequestAssistanceStepsPage';
        this.resultPageName = 'OpenRequestAssistanceResultPage';
        this.resetFlowData();
    };
    OpenRequestAssistanceProvider.prototype.isLastPageIsInFlow = function (navCtrl) {
        return navCtrl.last().pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE');
    };
    OpenRequestAssistanceProvider.prototype.isFirstPageResetDataApp = function (navCtrl) {
        var views = navCtrl.getViews();
        var viewsLength = views.length;
        if (viewsLength > 1) {
            return !views[viewsLength - 1].pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE') ||
                !views[viewsLength - 2].pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE');
        }
        else {
            return false;
        }
    };
    OpenRequestAssistanceProvider.prototype.cleanProvider = function () {
        this.initializeFlowData();
    };
    OpenRequestAssistanceProvider.prototype.getAssistanceTypes = function (mocked) {
        if (this.assistanceRequest.selectedPolicy) {
            return mocked ? Observable.of({ data: Mocks.getCarAssistanceTypes() }) :
                this.apiProvider.getObservableForSuffix(mocked, true, 'catalogs/assistance_types/', true, { risk_type: this.assistanceRequest.selectedPolicy.type });
        }
        else {
            return Observable.of(true);
        }
    };
    OpenRequestAssistanceProvider.prototype.getInsurances = function () {
        return this.apiProvider.getObservableForSuffix(false, false, "client/" + this.componentSettingsProvider.getClientId() + "/risks/", true);
    };
    OpenRequestAssistanceProvider.prototype.getFaultTypes = function (mocked) {
        if (this.assistanceRequest.selectedAssistanceType) {
            return mocked ? Observable.of({ data: Mocks.getFaultTypes() }) :
                this.apiProvider.getObservableForSuffix(mocked, true, "catalogs/assistance_type/" + this.getSelectedAssistanceType().id + "/fault_types", true);
        }
        else {
            return Observable.of(true);
        }
    };
    OpenRequestAssistanceProvider.prototype.getRequestJson = function (formattedRequest) {
        if (this.isCarPolicy()) {
            formattedRequest.step_data = {
                associated_risk: {
                    risk_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.risk_id : null,
                    policy_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.id : null
                },
                claim_id: this.claimId,
                car_detail: {
                    assistance_type_id: this.assistanceRequest.selectedAssistanceType ? this.assistanceRequest.selectedAssistanceType.id : null,
                    fault_type_id: this.assistanceRequest.selectedFaultType ? this.assistanceRequest.selectedFaultType.id : undefined,
                    vehicle_motion: !_.isNil(this.assistanceRequest.selectedInMovement) ? this.assistanceRequest.selectedInMovement : undefined,
                    location: {
                        road_type_id: null,
                        road_name: null,
                        road_number: null,
                        zip_code: null,
                        city_id: null,
                        province_id: null,
                        country_id: null,
                        details: null
                    }
                },
                home_detail: null
            };
        }
        else {
            formattedRequest.step_data = {
                associated_risk: {
                    risk_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.risk_id : null,
                    policy_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.id : null
                },
                claim_id: this.claimId,
                car_detail: null,
                home_detail: {
                    assistance_type: {
                        assistance_type_id: this.assistanceRequest.selectedAssistanceType ? this.assistanceRequest.selectedAssistanceType.id : null,
                        details: this.assistanceRequest.selectedFailureDetail ? this.assistanceRequest.selectedFailureDetail : null
                    },
                    location: {
                        road_type_id: null,
                        road_name: null,
                        road_number: null,
                        zip_code: null,
                        city_id: null,
                        province_id: null,
                        country_id: null,
                        details: null
                    }
                }
            };
        }
        return formattedRequest;
    };
    OpenRequestAssistanceProvider.prototype.updateRequest = function (flowRequestData, callback, mocked) {
        var _this = this;
        if (this.requestId) {
            this.apiProvider.putObservableForSuffix(false, false, "open_operations/client/" + this.componentSettingsProvider.getClientId() + "/assistance/" + this.claimId, true, {}, this.getRequestJson(flowRequestData)).subscribe(function (response) {
                _this.onRequestPut(response, callback);
            });
        }
        else {
            this.componentSettingsProvider.submitAssistanceRequest(mocked, this.getRequestJson(flowRequestData), this.claimId).subscribe(function (response) {
                _this.onRequestPost(response, callback);
            });
        }
    };
    OpenRequestAssistanceProvider.prototype.getAssistanceRequest = function () {
        return this.assistanceRequest;
    };
    OpenRequestAssistanceProvider.prototype.setSelectedAssistanceType = function (selectedAssistanceType) {
        this.assistanceRequest.selectedAssistanceType = selectedAssistanceType;
    };
    OpenRequestAssistanceProvider.prototype.getSelectedAssistanceType = function () {
        return this.assistanceRequest.selectedAssistanceType;
    };
    OpenRequestAssistanceProvider.prototype.setSelectedFailureDetail = function (selectedFailureDetail) {
        this.assistanceRequest.selectedFailureDetail = selectedFailureDetail;
    };
    OpenRequestAssistanceProvider.prototype.getSelectedFailureDetail = function () {
        return this.assistanceRequest.selectedFailureDetail;
    };
    OpenRequestAssistanceProvider.prototype.setSelectedFaultType = function (selectedFaultType) {
        this.assistanceRequest.selectedFaultType = selectedFaultType;
    };
    OpenRequestAssistanceProvider.prototype.getSelectedFaultType = function () {
        return this.assistanceRequest.selectedFaultType;
    };
    OpenRequestAssistanceProvider.prototype.setSelectedInMovement = function (selectedInMovement) {
        this.assistanceRequest.selectedInMovement = selectedInMovement;
    };
    OpenRequestAssistanceProvider.prototype.getSelectedInMovement = function () {
        return this.assistanceRequest.selectedInMovement;
    };
    OpenRequestAssistanceProvider.prototype.setSelectedPolicy = function (selectedPolicy) {
        this.assistanceRequest.selectedPolicy = selectedPolicy;
    };
    OpenRequestAssistanceProvider.prototype.getSelectedPolicy = function () {
        return this.assistanceRequest.selectedPolicy;
    };
    OpenRequestAssistanceProvider.prototype.isCarPolicy = function () {
        return this.assistanceRequest.selectedPolicy && this.assistanceRequest.selectedPolicy.type === 'C';
    };
    OpenRequestAssistanceProvider.prototype.setSelectedLocation = function (selectedLocation) {
        this.assistanceRequest.selectedLocation = selectedLocation;
    };
    OpenRequestAssistanceProvider.prototype.getSelectedLocation = function () {
        return this.assistanceRequest.selectedLocation;
    };
    OpenRequestAssistanceProvider.prototype.getSummaryData = function () {
        var summaryData;
        summaryData = {
            title: 'request_assistance_page.summary.title',
            sections: [
                {
                    title: 'request_assistance_page.summary.risk_selection',
                    rows: [{
                            key: 'request_assistance_page.summary.risk',
                            value: this.assistanceRequest && this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.name : ''
                        }, {
                            key: 'request_assistance_page.summary.policy',
                            value: this.assistanceRequest && this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.associated_policies[0].type : ''
                        }]
                }
            ]
        };
        if (this.getSelectedFaultType() && (this.getSelectedInMovement() !== null)) {
            summaryData.sections.push({
                title: 'request_assistance_page.summary.assistance_type',
                rows: [{
                        key: 'request_assistance_page.summary.assistance_type',
                        value: this.assistanceRequest && this.assistanceRequest.selectedAssistanceType ? this.assistanceRequest.selectedAssistanceType.title : ''
                    }, {
                        key: 'request_assistance_page.summary.incident_type',
                        value: this.getSelectedFaultType() ? this.getSelectedFaultType().title : undefined
                    }, {
                        key: 'request_assistance_page.summary.vehicle_status_movement',
                        value: (this.getSelectedInMovement()) ? this.translationProvider.getValueForKey('request-assistance.generic.yes') :
                            this.translationProvider.getValueForKey('request-assistance.generic.no')
                    }]
            });
        }
        else {
            summaryData.sections.push({
                title: 'request_assistance_page.summary.assistance_type',
                rows: [{
                        key: 'request_assistance_page.summary.assistance_type',
                        value: this.getSelectedAssistanceType() ? this.getSelectedAssistanceType().title : ''
                    }, {
                        key: 'request_assistance_page.summary.failure_detail',
                        value: this.getSelectedFailureDetail() !== null && this.getSelectedFailureDetail() !== '' ?
                            this.getSelectedFailureDetail() : '-'
                    }]
            });
        }
        if (this.getSelectedLocation()) {
            summaryData.sections.push({
                title: 'request_assistance_page.summary.where_are_you',
                rows: [{
                        key: 'request_assistance_page.summary.location',
                        value: this.getSelectedLocation() ? this.formatProvider.formatAddress(this.getSelectedLocation()) : undefined
                    }]
            });
        }
        return summaryData;
    };
    OpenRequestAssistanceProvider.prototype.goBackIfPreviousStepIsIncomplete = function (navController, currentStepNumber) {
        if (currentStepNumber > 1) {
            var previousStepNumber = currentStepNumber - 1;
            navController.push('OpenRequestAssistanceStepsPage', { stepId: previousStepNumber }, { animate: false });
        }
    };
    OpenRequestAssistanceProvider.prototype.checkLastPageIsInFlow = function (navCtrl) {
        return navCtrl.last().pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE');
    };
    OpenRequestAssistanceProvider.prototype.isProviderReset = function () {
        return !this.assistanceRequest.selectedAssistanceType;
    };
    OpenRequestAssistanceProvider.prototype.goBackToStep = function (navController) {
        navController.pop({ animate: false });
    };
    OpenRequestAssistanceProvider.prototype.advanceToStep = function (navController, stepId) {
        navController.push('OpenRequestAssistanceStepsPage', { stepId: stepId + 1 }, { animate: false });
    };
    OpenRequestAssistanceProvider.prototype.advanceToResultPage = function (navController, index) {
        navController.push('OpenRequestAssistanceResultPage', { animate: false });
    };
    OpenRequestAssistanceProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.assistanceRequest = {
            selectedAssistanceType: null,
            selectedFailureDetail: null,
            selectedVehicle: null,
            selectedPolicy: null,
            selectedFaultType: null,
            selectedInMovement: null,
            selectedLocation: null
        };
    };
    OpenRequestAssistanceProvider.decorators = [
        { type: Injectable },
    ];
    OpenRequestAssistanceProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ComponentSettingsProvider, },
    ]; };
    return OpenRequestAssistanceProvider;
}(AbstractFlowProvider));
export { OpenRequestAssistanceProvider };
//# sourceMappingURL=open-request-assistance.js.map