import { Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
export declare class PageSettingsProvider extends ApiProvider {
    protected injector: Injector;
    constructor(injector: Injector);
}
