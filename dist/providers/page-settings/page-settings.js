var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
var PageSettingsProvider = (function (_super) {
    __extends(PageSettingsProvider, _super);
    function PageSettingsProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        return _this;
    }
    PageSettingsProvider.decorators = [
        { type: Injectable },
    ];
    PageSettingsProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return PageSettingsProvider;
}(ApiProvider));
export { PageSettingsProvider };
//# sourceMappingURL=page-settings.js.map