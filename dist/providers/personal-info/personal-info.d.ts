import { Injector } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ApiProvider } from '../api/api';
import { MenuProvider } from '../menu/menu';
export declare class PersonalInfoProvider extends ApiProvider {
    private menuProvider;
    protected injector: Injector;
    private userData;
    private userPictureData;
    private userContactData;
    private userSecurityData;
    constructor(menuProvider: MenuProvider, injector: Injector);
    getContactsByClientId(mocked: boolean): Observable<Response>;
    getContactsByPolicyId(mocked: boolean, policyId: string): Observable<Response>;
    getClientData(mocked: boolean): Observable<Response>;
    setClientData(mocked: boolean): any;
    getUserData(): any;
    setUserData(userData: any): void;
    getUserPictureData(): any;
    setUserPictureData(userPictureData: any): void;
    getUserContactData(): any;
    setUserContactData(userContactData: any): void;
    getUserSecurityData(): any;
    setUserSecurityData(userSecurityData: any): void;
    getAvailableLanguages(mocked: boolean): Observable<Response>;
}
