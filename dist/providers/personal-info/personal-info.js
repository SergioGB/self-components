var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { ApiProvider } from '../api/api';
import { MenuProvider } from '../menu/menu';
var PersonalInfoProvider = (function (_super) {
    __extends(PersonalInfoProvider, _super);
    function PersonalInfoProvider(menuProvider, injector) {
        var _this = _super.call(this, injector) || this;
        _this.menuProvider = menuProvider;
        _this.injector = injector;
        return _this;
    }
    PersonalInfoProvider.prototype.getContactsByClientId = function (mocked) {
        var _this = this;
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId() + "/contact", true).map(function (response) {
            var success = _this.isMapfreResponseValid(_this.getResponseJSON(response));
            if (success) {
                _this.menuProvider.menuOptionsEmitter.emit([]);
            }
            return response;
        });
    };
    PersonalInfoProvider.prototype.getContactsByPolicyId = function (mocked, policyId) {
        var _this = this;
        return this.getObservableForSuffix(mocked, true, "client/" + policyId + "/contact", true).map(function (response) {
            var success = _this.isMapfreResponseValid(_this.getResponseJSON(response));
            if (success) {
                _this.menuProvider.menuOptionsEmitter.emit([]);
            }
            return response;
        });
    };
    PersonalInfoProvider.prototype.getClientData = function (mocked) {
        return this.getObservableForSuffix(mocked, true, "client/" + this.getClientId(), true);
    };
    PersonalInfoProvider.prototype.setClientData = function (mocked) {
        return this.getQueryUrl(mocked, true, "client/" + this.getClientId());
    };
    PersonalInfoProvider.prototype.getUserData = function () {
        return this.userData;
    };
    PersonalInfoProvider.prototype.setUserData = function (userData) {
        this.userData = userData;
    };
    PersonalInfoProvider.prototype.getUserPictureData = function () {
        return this.userPictureData;
    };
    PersonalInfoProvider.prototype.setUserPictureData = function (userPictureData) {
        this.userPictureData = userPictureData;
    };
    PersonalInfoProvider.prototype.getUserContactData = function () {
        return this.userContactData;
    };
    PersonalInfoProvider.prototype.setUserContactData = function (userContactData) {
        this.userContactData = userContactData;
    };
    PersonalInfoProvider.prototype.getUserSecurityData = function () {
        return this.userSecurityData;
    };
    PersonalInfoProvider.prototype.setUserSecurityData = function (userSecurityData) {
        this.userSecurityData = userSecurityData;
    };
    PersonalInfoProvider.prototype.getAvailableLanguages = function (mocked) {
        return this.getObservableForSuffix(mocked, false, "available_languages", true);
    };
    PersonalInfoProvider.decorators = [
        { type: Injectable },
    ];
    PersonalInfoProvider.ctorParameters = function () { return [
        { type: MenuProvider, },
        { type: Injector, },
    ]; };
    return PersonalInfoProvider;
}(ApiProvider));
export { PersonalInfoProvider };
//# sourceMappingURL=personal-info.js.map