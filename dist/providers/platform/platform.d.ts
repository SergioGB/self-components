import { Platform } from 'ionic-angular';
export declare class PlatformProvider {
    private platform;
    platforms: any;
    constructor(platform: Platform);
    getPlatformForDisplay(): 'desktop' | 'app' | 'tablet' | 'mobile';
    onDesktop(): boolean;
    onTablet(): boolean;
    onMobile(): boolean;
    onApp(): boolean;
    onCordova(): boolean;
    getPlatform(): string;
    onIos(): boolean;
    onAndroid(): boolean;
    onIE(): boolean;
    bindTo(target: any): void;
}
