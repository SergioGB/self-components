import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
var PlatformProvider = (function () {
    function PlatformProvider(platform) {
        this.platform = platform;
        this.platforms = {
            desktop: 'core',
            tablet: 'tablet',
            mobile: 'mobile'
        };
    }
    PlatformProvider.prototype.getPlatformForDisplay = function () {
        return this.onApp() ? 'app' : this.onTablet() ? 'tablet' : this.onMobile() ? 'mobile' : 'desktop';
    };
    PlatformProvider.prototype.onDesktop = function () {
        return this.platform.is(this.platforms.desktop);
    };
    PlatformProvider.prototype.onTablet = function () {
        return this.platform.is(this.platforms.tablet) && !this.platform.is(this.platforms.desktop);
    };
    PlatformProvider.prototype.onMobile = function () {
        return this.platform.is(this.platforms.mobile) && !this.platform.is(this.platforms.tablet);
    };
    PlatformProvider.prototype.onApp = function () {
        return !(this.platform.is('core') || this.platform.is('mobileweb'));
    };
    PlatformProvider.prototype.onCordova = function () {
        return this.platform.is('cordova') && this.onApp();
    };
    PlatformProvider.prototype.getPlatform = function () {
        return this.onMobile() ? this.platforms.mobile : this.onTablet() ? this.platforms.tablet : this.platforms.desktop;
    };
    PlatformProvider.prototype.onIos = function () {
        return this.platform.is('ios');
    };
    PlatformProvider.prototype.onAndroid = function () {
        return this.platform.is('android');
    };
    PlatformProvider.prototype.onIE = function () {
        return navigator.userAgent.indexOf('Trident') >= 0;
    };
    PlatformProvider.prototype.bindTo = function (target) {
        target.onDesktop = this.onDesktop();
        target.onTablet = this.onTablet();
        target.onMobile = this.onMobile();
        target.onApp = this.onApp();
        target.platform = this.getPlatform();
    };
    PlatformProvider.decorators = [
        { type: Injectable },
    ];
    PlatformProvider.ctorParameters = function () { return [
        { type: Platform, },
    ]; };
    return PlatformProvider;
}());
export { PlatformProvider };
//# sourceMappingURL=platform.js.map