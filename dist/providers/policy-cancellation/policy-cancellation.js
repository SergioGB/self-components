var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { InformationModalComponent } from '../../components/_modals/information-modal/information-modal';
import { BackWarningModalComponent } from '../../components/_modals/back-warning-modal/back-warning-modal';
import { InsuranceProvider } from '../insurance/insurance';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
var PolicyCancellationProvider = (function (_super) {
    __extends(PolicyCancellationProvider, _super);
    function PolicyCancellationProvider(injector, insuranceProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.insuranceProvider = insuranceProvider;
        _this.initializeFlowData();
        return _this;
    }
    PolicyCancellationProvider.prototype.setNavParams = function (navParams) {
        var _this = this;
        Observable.forkJoin([
            this.insuranceProvider.getInsuranceDetailSettings(false, navParams.get('riskId')),
            this.insuranceProvider.getPolicyDetailSettings(false, navParams.get('policyId'))
        ]).subscribe(function (responses) {
            if (_this.insuranceProvider.areResponsesValid(responses)) {
                _this.insurance = _this.insuranceProvider.getResponseJSON(responses[0]).data;
                _this.policy = _this.insuranceProvider.getResponseJSON(responses[1]).data;
            }
        });
    };
    PolicyCancellationProvider.prototype.getReasonsCommentsSelection = function () {
        return this.reasonsCommentsSelection;
    };
    PolicyCancellationProvider.prototype.setReasonsCommentsSelection = function (reasonsCommentsSelection) {
        this.reasonsCommentsSelection = reasonsCommentsSelection;
    };
    PolicyCancellationProvider.prototype.getCancellationDateSelection = function () {
        return this.policyCancellationDateSelection;
    };
    PolicyCancellationProvider.prototype.setCancellationDateSelection = function (policyCancellationDateSelection) {
        this.policyCancellationDateSelection = policyCancellationDateSelection;
    };
    PolicyCancellationProvider.prototype.getUploadedDocuments = function () {
        return this.uploadedDocuments;
    };
    PolicyCancellationProvider.prototype.setUploadedDocuments = function (uploadedDocuments) {
        this.uploadedDocuments = uploadedDocuments;
    };
    PolicyCancellationProvider.prototype.getResult = function () {
        return this.result;
    };
    PolicyCancellationProvider.prototype.setResult = function (result) {
        this.result = result;
    };
    PolicyCancellationProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.reasonsCommentsSelection = undefined;
        this.policyCancellationDateSelection = undefined;
        this.uploadedDocuments = [];
    };
    PolicyCancellationProvider.prototype.showSummaryModal = function (modalController) {
        var _this = this;
        if (this.policy) {
            this.insuranceProvider.getPolicyPaymentMethod(false, this.policy.id).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    var paymentData = mapfreResponse.data;
                    var refundType = mapfreResponse.response.code;
                    var policyData = _this.policy.car_policy_detail ? _this.policy.car_policy_detail.basic_info : _this.policy.home_policy_detail.basic_info;
                    var dueDate = new Date(policyData.due_date.field_content);
                    var cancelDate = _this.policyCancellationDateSelection;
                    if (dueDate && cancelDate) {
                        var modal = void 0;
                        var amount = policyData.price.field_content;
                        if (dueDate > cancelDate.date && refundType === 1) {
                            modal = modalController.create(InformationModalComponent, {
                                title: _this.translationProvider.getValueForKey('modal.policy-cancellation.information-modal.reserve_bonus_modal.title'),
                                subtitle: _this.translationProvider.getValueForKey('modal.policy-cancellation.information-modal.reserve_bonus_modal.desc1'),
                                subtitle2: _this.translationProvider.getValueForKey('modal.policy-cancellation.information-modal.reserve_bonus_modal.desc2'),
                                amount: amount,
                                labels: {
                                    cancel: "modal.policy-cancellation.information-modal.generic.cancel",
                                    finish: "modal.policy-cancellation.information-modal.generic.finish",
                                    accept: "modal.policy-cancellation.information-modal.generic.accept"
                                },
                                icon: 'mapfre-warning',
                                type: 0,
                                rightButton: _this.translationProvider.getValueForKey('modal.policy-cancellation.information-modal.reserve_bonus_modal.continue')
                            });
                            modal.present();
                        }
                        if (dueDate > cancelDate.date && refundType === 2) {
                            if (paymentData.bank_account_info) {
                                modal = modalController.create(BackWarningModalComponent, {
                                    method: '1',
                                    dataA: paymentData.bank_account_info.account_number,
                                    dataB: amount
                                });
                            }
                            else if (paymentData.credit_card_info) {
                                modal = modalController.create(BackWarningModalComponent, {
                                    method: '2',
                                    dataA: paymentData.bank_account_info.cardholder_name,
                                    dataB: amount
                                });
                            }
                            modal.present();
                        }
                    }
                }
            });
        }
    };
    PolicyCancellationProvider.prototype.getSummaryData = function () {
        var summarySections = [];
        if (this.insurance && this.policy) {
            var policyData = this.policy ?
                (this.policy.car_policy_detail ? this.policy.car_policy_detail.basic_info : this.policy.home_policy_detail.basic_info) : undefined;
            summarySections.push({
                rows: [{
                        key: 'cancel_policy.summary.risk',
                        value: this.insurance.name
                    }, {
                        key: 'cancel_policy.summary.policy_type',
                        value: policyData.description.field_content
                    }, {
                        key: 'cancel_policy.summary.policy_number',
                        value: policyData.number.field_content
                    }, {
                        key: 'cancel_policy.summary.due_date',
                        value: this.formatProvider.formatDate(policyData.due_date.field_content)
                    }, {
                        key: 'cancel_policy.summary.amount',
                        value: this.formatProvider.formatCost(policyData.price.field_content)
                    }, {
                        key: 'cancel_policy.summary.payment_periodicity',
                        value: policyData.payment_modality.field_content
                    }]
            });
        }
        var reasonsComments = this.getReasonsCommentsSelection();
        if (reasonsComments) {
            var reasonRows = [];
            if (reasonsComments.reason) {
                reasonRows.push({
                    key: 'cancel_policy.summary.reason',
                    value: reasonsComments.reason.title
                });
            }
            if (reasonsComments.comment && reasonsComments.comment.length) {
                reasonRows.push({
                    key: 'cancel_policy.summary.comments',
                    value: reasonsComments.comment
                });
            }
            summarySections.push({
                title: 'cancel_policy.summary.reason_comments_title',
                rows: reasonRows
            });
        }
        var cancellationDate = this.getCancellationDateSelection();
        if (cancellationDate) {
            summarySections.push({
                title: 'cancel_policy.summary.date_title',
                rows: [{
                        key: 'cancel_policy.summary.date_selection',
                        value: cancellationDate.dateOption.title + (cancellationDate.date ? " (" + this.formatProvider.formatDate(cancellationDate.date) + ")" : '')
                    }]
            });
        }
        return {
            title: 'cancel_policy.summary.title',
            sections: summarySections
        };
    };
    PolicyCancellationProvider.prototype.getRequestJson = function (formattedRequest) {
        return null;
    };
    PolicyCancellationProvider.prototype.updateRequest = function (flowRequest, callback, mocked) {
        var _this = this;
        this.insuranceProvider.cancelPolicy(mocked, this.policy.id, flowRequest).subscribe(function (response) {
            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.onRequestDelete(response, callback);
            }
        });
    };
    PolicyCancellationProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'PolicyCancellationStepsPage';
        this.resultPageName = 'PolicyCancellationResultPage';
        this.resetFlowData();
    };
    PolicyCancellationProvider.decorators = [
        { type: Injectable },
    ];
    PolicyCancellationProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: InsuranceProvider, },
    ]; };
    return PolicyCancellationProvider;
}(AbstractFlowProvider));
export { PolicyCancellationProvider };
//# sourceMappingURL=policy-cancellation.js.map