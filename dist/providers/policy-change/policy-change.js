var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { InsuranceProvider } from '../insurance/insurance';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
var PolicyChangeProvider = (function (_super) {
    __extends(PolicyChangeProvider, _super);
    function PolicyChangeProvider(injector, insuranceProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.insuranceProvider = insuranceProvider;
        _this.initializeFlowData();
        return _this;
    }
    PolicyChangeProvider.prototype.setNavParams = function (navParams) {
        var _this = this;
        this.navParams = navParams;
        this.riskId = navParams.get('riskId');
        this.policyId = navParams.get('policyId');
        Observable.forkJoin([
            this.insuranceProvider.getInsuranceDetailSettings(false, this.riskId),
            this.insuranceProvider.getPolicyDetailSettings(false, this.policyId)
        ]).subscribe(function (responses) {
            if (_this.insuranceProvider.areResponsesValid(responses)) {
                _this.insurance = _this.insuranceProvider.getResponseJSON(responses[0]).data;
                _this.policy = _this.insuranceProvider.getResponseJSON(responses[1]).data;
            }
        });
    };
    PolicyChangeProvider.prototype.getPoliciesExtension = function () {
        return this.policiesExtension;
    };
    PolicyChangeProvider.prototype.setPoliciesExtension = function (policiesExtension) {
        this.policiesExtension = policiesExtension;
    };
    PolicyChangeProvider.prototype.getAdditionalDocumentation = function () {
        return this.additionalDocumentation;
    };
    PolicyChangeProvider.prototype.setAdditionalDocumentation = function (additionalDocumentation) {
        this.additionalDocumentation = additionalDocumentation;
    };
    PolicyChangeProvider.prototype.getPaymentMethod = function () {
        return this.paymentMethod;
    };
    PolicyChangeProvider.prototype.setPaymentMethod = function (paymentMethod) {
        this.paymentMethod = paymentMethod;
    };
    PolicyChangeProvider.prototype.getResult = function () {
        return this.result;
    };
    PolicyChangeProvider.prototype.setResult = function (result) {
        this.result = result;
    };
    PolicyChangeProvider.prototype.getDataChanged = function () {
        return this.dataChanged;
    };
    PolicyChangeProvider.prototype.setDataChanged = function (dataChanged) {
        this.dataChanged = dataChanged;
    };
    PolicyChangeProvider.prototype.getFieldsChanged = function () {
        return this.fieldsChanged;
    };
    PolicyChangeProvider.prototype.setFieldsChanged = function (fieldsChanged) {
        this.fieldsChanged = fieldsChanged;
    };
    PolicyChangeProvider.prototype.getRiskId = function () {
        return this.riskId;
    };
    PolicyChangeProvider.prototype.setPolicyResult = function (policyResult) {
        this.policyResult = policyResult;
    };
    PolicyChangeProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.policiesExtension = [];
        this.additionalDocumentation = [];
        this.paymentMethod = undefined;
    };
    PolicyChangeProvider.prototype.getSummaryData = function () {
        var policyExtension = this.policiesExtension;
        var additionalDocumentation = this.additionalDocumentation;
        var paymentMethod = this.paymentMethod;
        var summarySections = [];
        if (this.dataChanged && this.dataChanged !== undefined) {
            if (typeof this.dataChanged.changedData !== 'object') {
                summarySections.push({
                    title: 'policy_change.summary.new_data',
                    rows: [{
                            key: this.dataChanged.fieldName,
                            value: this.dataChanged.changedData
                        }]
                });
            }
            else {
                summarySections.push({
                    title: 'policy_change.summary.new_data',
                    rows: [{
                            key: this.dataChanged.fieldName,
                            date: this.dataChanged.changedData
                        }]
                });
            }
        }
        if (policyExtension) {
            var extensionRow = [];
            for (var _i = 0, policyExtension_1 = policyExtension; _i < policyExtension_1.length; _i++) {
                var entry = policyExtension_1[_i];
                extensionRow.push({
                    key: 'policy_change.summary.risk_description',
                    value: entry.risk_description
                });
                extensionRow.push({
                    key: 'policy_change.summary.policy_description',
                    value: entry.policy_description
                });
            }
            if (extensionRow.length > 0) {
                summarySections.push({
                    title: 'policy_change.summary.policy_extension',
                    rows: extensionRow
                });
            }
        }
        if (additionalDocumentation) {
            var documentationRow = [];
            for (var _a = 0, additionalDocumentation_1 = additionalDocumentation; _a < additionalDocumentation_1.length; _a++) {
                var entry = additionalDocumentation_1[_a];
                documentationRow.push({
                    file: entry
                });
            }
            if (documentationRow.length > 0) {
                summarySections.push({
                    title: 'policy_change.summary.aditional_documentation',
                    rows: documentationRow
                });
            }
        }
        if (paymentMethod) {
            if (paymentMethod.credit_card_info) {
                summarySections.push({
                    title: 'policy_change.summary.payment_method',
                    rows: [{
                            key: 'policy_change.summary.payment_amount',
                            value: '200,00 €'
                        }, {
                            key: 'policy_change.summary.payment_type',
                            value: this.translationProvider.getValueForKey('policy-change.policy_change.summary.payment_type_card')
                        }, {
                            key: 'policy_change.summary.card_number',
                            value: paymentMethod.credit_card_info.card_number
                        }]
                });
            }
            else {
                summarySections.push({
                    title: 'policy_change.summary.payment_method',
                    rows: [{
                            key: 'policy_change.summary.payment_amount',
                            value: '200,00 €'
                        }, {
                            key: 'policy_change.summary.payment_type',
                            value: this.translationProvider.getValueForKey('policy-change.policy_change.summary.payment_type_bank')
                        }, {
                            key: 'policy_change.summary.bank_number',
                            value: paymentMethod.bank_account_info.account_number
                        }]
                });
            }
        }
        return {
            title: 'process_steps.payment.summary',
            sections: summarySections
        };
    };
    PolicyChangeProvider.prototype.getRequestJson = function (formattedRequest) {
        formattedRequest.step_data = this.policyResult;
        return formattedRequest;
    };
    PolicyChangeProvider.prototype.getPolicyUpdateRequest = function (policy) {
        var car_policy_detail;
        if (policy.car_policy_detail !== null) {
            car_policy_detail = {
                basic_info: {
                    description: policy.car_policy_detail.basic_info.description.field_content,
                    number: policy.car_policy_detail.basic_info.number.field_content,
                    due_date: policy.car_policy_detail.basic_info.due_date.field_content,
                    price: {
                        amount: policy.car_policy_detail.basic_info.price.field_content.amount,
                        iso_code: policy.car_policy_detail.basic_info.price.field_content.iso_code
                    },
                    payment_modality_id: policy.car_policy_detail.basic_info.payment_modality.field_content
                },
                vehicle_info: {
                    type: policy.car_policy_detail.vehicle_info.type.field_content,
                    registration: policy.car_policy_detail.vehicle_info.registration.field_content
                },
                taker_info: {
                    name: policy.car_policy_detail.taker_info.name.field_content,
                    birthdate: policy.car_policy_detail.taker_info.birthdate.field_content,
                    identity_document: policy.car_policy_detail.taker_info.identity_document.field_content,
                    gender: policy.car_policy_detail.taker_info.gender.field_content,
                    civil_state: policy.car_policy_detail.taker_info.civil_state.field_content,
                    email: policy.car_policy_detail.taker_info.email.field_content,
                    license_date: policy.car_policy_detail.taker_info.license_date.field_content,
                    license_type: policy.car_policy_detail.taker_info.license_type.field_content
                },
                driver_info: {
                    name: policy.car_policy_detail.driver_info.name.field_content,
                    birthdate: policy.car_policy_detail.driver_info.birthdate.field_content,
                    identity_document: policy.car_policy_detail.driver_info.identity_document.field_content,
                    gender: policy.car_policy_detail.driver_info.gender.field_content,
                    civil_state: policy.car_policy_detail.driver_info.civil_state.field_content,
                    phone: policy.car_policy_detail.driver_info.phone.field_content,
                    email: policy.car_policy_detail.driver_info.email.field_content,
                    license_date: policy.car_policy_detail.driver_info.license_date.field_content,
                    license_type: policy.car_policy_detail.driver_info.license_type.field_content
                },
                owner_info: {
                    name: policy.car_policy_detail.owner_info.name.field_content,
                    birthdate: policy.car_policy_detail.owner_info.birthdate.field_content,
                    identity_document: policy.car_policy_detail.owner_info.identity_document.field_content,
                    gender: policy.car_policy_detail.owner_info.gender.field_content,
                    civil_state: policy.car_policy_detail.owner_info.civil_state.field_content,
                    phone: policy.car_policy_detail.owner_info.phone.field_content,
                    email: policy.car_policy_detail.owner_info.email.field_content,
                    license_date: policy.car_policy_detail.owner_info.license_date.field_content,
                    license_type: policy.car_policy_detail.owner_info.license_type.field_content
                }
            };
        }
        else {
            car_policy_detail = null;
        }
        var home_policy_detail;
        if (policy.home_policy_detail !== null) {
            home_policy_detail = {
                basic_info: {
                    description: policy.home_policy_detail.basic_info.description.field_content,
                    number: policy.home_policy_detail.basic_info.number.field_content,
                    init_date: policy.home_policy_detail.basic_info.init_date.field_content,
                    due_date: policy.home_policy_detail.basic_info.due_date.field_content,
                    duration: policy.home_policy_detail.basic_info.duration.field_content,
                    price: {
                        amount: policy.home_policy_detail.basic_info.price.field_content.amount,
                        iso_code: policy.home_policy_detail.basic_info.price.field_content.iso_code
                    },
                    payment_modality_id: policy.home_policy_detail.basic_info.payment_modality.field_content
                },
                home_info: {
                    type: policy.home_policy_detail.home_info.type.field_content,
                    address: policy.home_policy_detail.home_info.address.field_content,
                    construction_date: policy.home_policy_detail.home_info.construction_date.field_content,
                    regime: policy.home_policy_detail.home_info.regime.field_content,
                    size: policy.home_policy_detail.home_info.size.field_content,
                    use: policy.home_policy_detail.home_info.use.field_content
                },
                taker_info: {
                    address: policy.home_policy_detail.taker_info.address.field_content,
                    identity_document: policy.home_policy_detail.taker_info.identity_document.field_content,
                    name: policy.home_policy_detail.taker_info.name.field_content,
                    phone: policy.home_policy_detail.taker_info.phone.field_content
                }
            };
        }
        else {
            home_policy_detail = null;
        }
        return {
            associated_risk_id: this.riskId,
            confirm: true,
            car_policy_detail: car_policy_detail,
            home_policy_detail: home_policy_detail
        };
    };
    PolicyChangeProvider.prototype.updateRequest = function (flowRequest, callback, mocked) {
        var _this = this;
        var fieldsChanged = this.fieldsChanged;
        if (fieldsChanged && fieldsChanged.length > 0) {
            if (this.policiesExtension) {
                for (var _i = 0, _a = this.policiesExtension; _i < _a.length; _i++) {
                    var entry = _a[_i];
                    this.insuranceProvider.getPolicyDetailSettings(false, entry.policy_id).subscribe(function (response) {
                        var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                        if (_this.insuranceProvider.isMapfreResponseValid(mapfreResponse)) {
                            var policyDetail = mapfreResponse.data;
                            policyDetail[fieldsChanged[2]][fieldsChanged[0]][fieldsChanged[1]].field_content = _this.dataChanged.changedData;
                            flowRequest.step_data = _this.getPolicyUpdateRequest(policyDetail);
                            _this.insuranceProvider.updatePolicyDetailSettings(mocked, policyDetail.id, flowRequest).subscribe(function (response) {
                                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                                if (mapfreResponse) {
                                    _this.onRequestPut(response, callback);
                                }
                            });
                        }
                    });
                }
            }
            var updatedPolicy = this.getRequestJson(flowRequest);
            if (updatedPolicy) {
                updatedPolicy.confirm = true;
            }
            this.insuranceProvider.updatePolicyDetailSettings(mocked, this.policy.id, this.getRequestJson(flowRequest)).subscribe(function (response) {
                var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
                if (mapfreResponse) {
                    _this.onRequestPut(response, callback);
                }
            });
        }
    };
    PolicyChangeProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'PolicyChangeStepsPage';
        this.resultPageName = 'PolicyChangeResultPage';
        this.resetFlowData();
    };
    PolicyChangeProvider.decorators = [
        { type: Injectable },
    ];
    PolicyChangeProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: InsuranceProvider, },
    ]; };
    return PolicyChangeProvider;
}(AbstractFlowProvider));
export { PolicyChangeProvider };
//# sourceMappingURL=policy-change.js.map