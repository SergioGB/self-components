import { Injector } from '@angular/core';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
import { SummaryData } from '../../models/summary';
import { FlowRequest } from '../../models/flow-request';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
import { Budget } from '../../models/budget';
import { NavController } from 'ionic-angular';
import { MapfreFile } from '../../models/mapfreFile';
export declare class PotentialClientProvider extends AbstractFlowProvider {
    private injector;
    private componentSettingsProvider;
    private potentialClient;
    private additionalReceipts;
    private identificationDocuments;
    static flowReference: string;
    private formatCost;
    private isFlowInit;
    constructor(injector: Injector, componentSettingsProvider: ComponentSettingsProvider);
    getBudgetSelectedData(): Budget;
    setBudgetSelectedData(budgetSelectedData: Budget): void;
    getPaymentMethodSelectedData(): any;
    setPaymentMethodSelectedData(PaymentMethodSelectedData: any): void;
    getUpgradeSelectedData(): any;
    setUpgradeSelectedData(UpgradeSelectedData: any): void;
    resetFlowData(): void;
    setIsOpenFlow(isOpenFlow: boolean): void;
    getIsOpenFlow(): boolean;
    getRequestJson(formattedRequest: FlowRequest): FlowRequest;
    getSummaryData(): SummaryData;
    formatDate(date: Date): string;
    getConfirmation(): boolean;
    setConfirmation(confirmation: boolean): void;
    getContractBudgetData(): any;
    setContractBudgetData(contractBudgetData: any): void;
    getPassword(): string;
    setPassword(password: string): void;
    getFiles(): MapfreFile[];
    setFiles(files: MapfreFile[]): void;
    updateRequest(flowRequest: FlowRequest, callback: Function, mocked?: boolean): any;
    checkLastPageIsInFlow(navCtrl: NavController): boolean;
    cleanProvider(isOpenFlow?: boolean): void;
    setIsFlowInit(value: boolean): void;
    getIsFlowInit(): boolean;
    protected initializeFlowData(): void;
    protected initializeOpenFlowData(): void;
}
