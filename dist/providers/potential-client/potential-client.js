var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
var PotentialClientProvider = (function (_super) {
    __extends(PotentialClientProvider, _super);
    function PotentialClientProvider(injector, componentSettingsProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.componentSettingsProvider = componentSettingsProvider;
        _this.formatCost = _this.formatProvider.formatCost;
        _this.componentSettingsProvider.getIdentificationDocuments(false).subscribe(function (response) {
            var mapfreResponse = _this.componentSettingsProvider.getResponseJSON(response);
            if (_this.componentSettingsProvider.isMapfreResponseValid(mapfreResponse)) {
                _this.identificationDocuments = mapfreResponse.data;
            }
        });
        _this.initializeFlowData();
        return _this;
    }
    PotentialClientProvider.prototype.getBudgetSelectedData = function () {
        return this.potentialClient.budgetSelectedData;
    };
    PotentialClientProvider.prototype.setBudgetSelectedData = function (budgetSelectedData) {
        this.potentialClient.budgetSelectedData = budgetSelectedData;
    };
    PotentialClientProvider.prototype.getPaymentMethodSelectedData = function () {
        return this.potentialClient.PaymentMethodSelectedData;
    };
    PotentialClientProvider.prototype.setPaymentMethodSelectedData = function (PaymentMethodSelectedData) {
        this.potentialClient.PaymentMethodSelectedData = PaymentMethodSelectedData;
    };
    PotentialClientProvider.prototype.getUpgradeSelectedData = function () {
        return this.potentialClient.UpgradeSelectedData;
    };
    PotentialClientProvider.prototype.setUpgradeSelectedData = function (UpgradeSelectedData) {
        this.potentialClient.UpgradeSelectedData = UpgradeSelectedData;
    };
    PotentialClientProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.potentialClient = {};
        this.isOpenFlow = false;
        this.isFlowInit = false;
    };
    PotentialClientProvider.prototype.setIsOpenFlow = function (isOpenFlow) {
        this.isOpenFlow = isOpenFlow;
    };
    PotentialClientProvider.prototype.getIsOpenFlow = function () {
        return this.isOpenFlow;
    };
    PotentialClientProvider.prototype.getRequestJson = function (formattedRequest) {
        formattedRequest.step_data = {
            confirmation: this.getConfirmation(),
            budget_id: this.getBudgetSelectedData() ? this.getBudgetSelectedData().id : null,
            policy_type: {
                "id": "string",
                "payment_modality_id": "string",
                "additional_coverages": [
                    {
                        "id": "string",
                        "sum_insured": {
                            "amount": 0,
                            "iso_code": "AED"
                        }
                    }
                ]
            },
            additional_information: {
                init_date: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.init_date : null,
                taker_info: {
                    phone_number: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.phone_number : null,
                    address: {
                        road_type_id: this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.road_type ?
                            this.getContractBudgetData().form.road_type.id : null,
                        road_name: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.road_name : null,
                        road_number: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.road_number : null,
                        zip_code: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.zip_code : null,
                        city_id: this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.city ?
                            this.getContractBudgetData().form.city.id : null,
                        province_id: this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.province ?
                            this.getContractBudgetData().form.province.id : null,
                        country_id: this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.country ?
                            this.getContractBudgetData().form.country.id : null,
                    }
                },
                owner_info: {
                    name: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.name : null,
                    surname1: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.surname1 : null,
                    surname2: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.surname2 : null,
                    identification_document_id: this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.identification_document ?
                        this.getContractBudgetData().form.identification_document.id : null,
                    identification_document_number: this.getContractBudgetData() && this.getContractBudgetData().form ?
                        this.getContractBudgetData().form.identification_document_number : null,
                    birthdate: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.birthdate : null
                },
                self_password: this.getPassword() ? this.getPassword() : null,
            },
            documentation: [
                {
                    "title": "string",
                    "mime_type": "audio/aac",
                    "content": "string",
                    "owner": "MAPFRE"
                }
            ],
            payment_method: {
                id: this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().selectedPaymentMethod && this.getPaymentMethodSelectedData().selectedPaymentMethod.id ?
                    this.getPaymentMethodSelectedData().selectedPaymentMethod.id : null,
                type: this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().selectedPaymentMethod && this.getPaymentMethodSelectedData().selectedPaymentMethod.type ?
                    this.getPaymentMethodSelectedData().selectedPaymentMethod.type : null,
                bank_account_info: this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().selectedPaymentMethod &&
                    this.getPaymentMethodSelectedData().selectedPaymentMethod.id ? null : {
                    account_owner: {
                        name: this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().bankFormInfo && this.getPaymentMethodSelectedData().bankFormInfo.name ?
                            this.getPaymentMethodSelectedData().bankFormInfo.name : undefined,
                        surname1: this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().bankFormInfo && this.getPaymentMethodSelectedData().bankFormInfo.lastname1 ?
                            this.getPaymentMethodSelectedData().bankFormInfo.lastname1 : undefined,
                        surname2: this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().bankFormInfo && this.getPaymentMethodSelectedData().bankFormInfo.lastname2 ?
                            this.getPaymentMethodSelectedData().bankFormInfo.lastname2 : undefined
                    },
                    account_iban: this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().bankFormInfo && this.getPaymentMethodSelectedData().bankFormInfo.iban ?
                        this.getPaymentMethodSelectedData().bankFormInfo.iban : undefined
                }
            }
        };
        return formattedRequest;
    };
    PotentialClientProvider.prototype.getSummaryData = function () {
        var _this = this;
        var summaryData;
        summaryData = {
            title: 'potential-client.summary-v2.title',
            sections: [
                {
                    title: 'potential-client.summary-v2.budget_selection',
                    rows: [{
                            key: 'potential-client.summary-v2.budget_selection.name',
                            value: this.getBudgetSelectedData() ? this.getBudgetSelectedData().risk_name : ''
                        }, {
                            key: 'potential-client.summary-v2.budget_selection.number',
                            value: this.getBudgetSelectedData() ? this.getBudgetSelectedData().number : ''
                        }]
                }
            ]
        };
        var identificationDocumentSelected;
        if (this.getBudgetSelectedData() && this.getBudgetSelectedData().car_budget_detail) {
            identificationDocumentSelected = this.identificationDocuments.find(function (document) {
                return document.id === _this.getBudgetSelectedData().car_budget_detail.taker_info.identification_document_id;
            });
            summaryData.sections.push({
                title: 'potential-client.summary-v2.budget_data',
                rows: [{
                        key: 'potential-client.summary-v2.budget_data.car_budget_detail',
                        value: this.getBudgetSelectedData().car_budget_detail.vehicle_info ? [
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.brand,
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.model,
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.version,
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.registration,
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.doors_count + ' ' + this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.car_budget_detail.doors'),
                            this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.car_budget_detail.date') + ' ' + this.getBudgetSelectedData().car_budget_detail.vehicle_info.release_date,
                            this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.car_budget_detail.model') + " \n                  " + this.getBudgetSelectedData().car_budget_detail.vehicle_info.model,
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.engine_power,
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.engine_capacity,
                            this.getBudgetSelectedData().car_budget_detail.vehicle_info.gearbox,
                            this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.car_budget_detail.registration_date') + " \n                  " + this.getBudgetSelectedData().car_budget_detail.vehicle_info.registration_date,
                            this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.car_budget_detail.garage_type') + " \n                  " + this.getBudgetSelectedData().car_budget_detail.vehicle_info.garage_type
                        ] : ''
                    }, {
                        key: 'potential-client.summary-v2.budget_data.car_taker_info',
                        value: this.getBudgetSelectedData().car_budget_detail.taker_info ? [
                            this.getBudgetSelectedData().car_budget_detail.taker_info.name,
                            this.getBudgetSelectedData().car_budget_detail.taker_info.gender ? this.getBudgetSelectedData().car_budget_detail.taker_info.gender.title : '',
                            this.getBudgetSelectedData().car_budget_detail.taker_info.bithdate,
                            this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.car_taker_info.license_date') + " \n                  " + this.getBudgetSelectedData().car_budget_detail.taker_info.license_date,
                            this.getBudgetSelectedData().car_budget_detail.taker_info.zip_code,
                            (identificationDocumentSelected ? identificationDocumentSelected.title : '') + " \n                  " + this.getBudgetSelectedData().car_budget_detail.taker_info.identification_document_number
                        ] : ''
                    }]
            });
        }
        else if (this.getBudgetSelectedData() && this.getBudgetSelectedData().home_budget_detail) {
            identificationDocumentSelected = this.identificationDocuments.find(function (document) {
                return document.id === _this.getBudgetSelectedData().home_budget_detail.taker_info.identification_document_id;
            });
            summaryData.sections.push({
                title: 'potential-client.summary-v2.budget_data',
                rows: [{
                        key: 'potential-client.summary-v2.budget_data.home_budget_detail',
                        value: this.getBudgetSelectedData().home_budget_detail.home_info ? [
                            this.getBudgetSelectedData().home_budget_detail.home_info.address,
                            this.getBudgetSelectedData().home_budget_detail.home_info.housing_type ? this.getBudgetSelectedData().home_budget_detail.home_info.housing_type.title : '',
                            this.getBudgetSelectedData().home_budget_detail.home_info.size.toString() + ' ' + this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.home_budget_detail.size'),
                            this.formatDate(this.getBudgetSelectedData().home_budget_detail.home_info.construction_date),
                            this.getBudgetSelectedData().home_budget_detail.home_info.type,
                            this.getBudgetSelectedData().home_budget_detail.home_info.use,
                            this.getBudgetSelectedData().home_budget_detail.home_info.rooms_number + ' ' + this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.home_budget_detail.rooms'),
                            this.getBudgetSelectedData().home_budget_detail.home_info.bathrooms + ' ' + this.translationProvider.getValueForKey('potential-client.summary-v2.budget_data.home_budget_detail.bathrooms'),
                            this.getBudgetSelectedData().home_budget_detail.home_info.safety_guards,
                            'Planta ' + this.getBudgetSelectedData().home_budget_detail.home_info.floor,
                            this.getBudgetSelectedData().home_budget_detail.home_info.regime ? this.getBudgetSelectedData().home_budget_detail.home_info.regime.title : '',
                            this.getBudgetSelectedData().home_budget_detail.home_info.construction_quality,
                        ] : ''
                    }, {
                        key: 'potential-client.summary-v2.budget_data.home_taker_info',
                        value: this.getBudgetSelectedData().home_budget_detail.taker_info ? [
                            this.getBudgetSelectedData().home_budget_detail.taker_info.name,
                            this.getBudgetSelectedData().home_budget_detail.taker_info.gender ? this.getBudgetSelectedData().home_budget_detail.taker_info.gender.title : '',
                            this.getBudgetSelectedData().home_budget_detail.taker_info.bithdate,
                            (identificationDocumentSelected ? identificationDocumentSelected.title : '') + " \n                  " + this.getBudgetSelectedData().home_budget_detail.taker_info.identification_document_number
                        ] : ''
                    }]
            });
        }
        var titlesCoverage = [];
        if (this.getUpgradeSelectedData() && this.getUpgradeSelectedData().insuranceType && this.getUpgradeSelectedData().insuranceType.additional_coverages) {
            for (var _i = 0, _a = this.getUpgradeSelectedData().insuranceType.additional_coverages; _i < _a.length; _i++) {
                var coverage = _a[_i];
                titlesCoverage.push(coverage.title);
            }
        }
        summaryData.sections.push({
            title: 'potential-client.summary-v2.policy_modality.title',
            toggle: true,
            rows: [{
                    key: 'potential-client.summary-v2.policy_modality.type',
                    value: this.getUpgradeSelectedData() && this.getUpgradeSelectedData().insuranceType ?
                        this.getUpgradeSelectedData().insuranceType.title : ''
                }, {
                    key: 'potential-client.summary-v2.policy_modality.payment_modality',
                    value: this.getUpgradeSelectedData() ? this.getUpgradeSelectedData().payment_modality.title : ''
                }, {
                    key: 'potential-client.summary-v2.policy_modality.additional_coverages',
                    value: this.getUpgradeSelectedData() ? titlesCoverage : ''
                }, {
                    key: 'potential-client.summary-v2.policy_modality.total_price',
                    value: this.getUpgradeSelectedData() ? this.formatCost(this.getUpgradeSelectedData().totalCost) : ''
                }]
        });
        var coverageNames = [];
        var coveragePrices = [];
        var row = [];
        if (this.getUpgradeSelectedData() && this.getUpgradeSelectedData().insuranceType && this.getUpgradeSelectedData().insuranceType.coverages) {
            for (var _b = 0, _c = this.getUpgradeSelectedData().insuranceType.coverages; _b < _c.length; _b++) {
                var coverage = _c[_b];
                coverageNames.push(coverage.title);
                coveragePrices.push(coverage.sum_insured ? coverage.sum_insured.amount : 'Incluida');
                if (coverage.sum_insured) {
                    row.push({
                        key: coverage.title,
                        value: coverage.sum_insured.amount + "€"
                    });
                }
                else {
                    row.push({
                        key: coverage.title,
                        value: 'Incluida'
                    });
                }
            }
            summaryData.sections.push({
                title: 'upgrade_insurance.summary.coverages_list',
                toggle: true,
                type: 'coverages',
                rows: row
            });
        }
        var files = [];
        if (this.getFiles() && this.getFiles().length > 0) {
            for (var _d = 0, _e = this.getFiles(); _d < _e.length; _d++) {
                var file = _e[_d];
                files.push(file.title);
            }
        }
        summaryData.sections.push({
            title: 'potential-client.summary-v2.contract_budget_data',
            toggle: true,
            rows: [{
                    key: 'potential-client.summary-v2.contract_budget_data.documents',
                    value: files
                }, {
                    key: 'potential-client.summary-v2.contract_budget_data.init_date',
                    date: this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.init_date : ''
                }, {
                    key: 'potential-client.summary-v2.contract_budget_data.taker_info',
                    value: [
                        this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.road_type ?
                            this.getContractBudgetData().form.road_type.title : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.road_name : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.road_number : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.zip_code : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.city ?
                            this.getContractBudgetData().form.city.title : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.province ?
                            this.getContractBudgetData().form.province.title : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form && this.getContractBudgetData().form.country ?
                            this.getContractBudgetData().form.country.title : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.phone_number : ''
                    ]
                }, {
                    key: 'potential-client.summary-v2.contract_budget_data.owner_info',
                    value: [
                        this.getContractBudgetData() && this.getContractBudgetData().form ?
                            this.getContractBudgetData().form.name + " " + this.getContractBudgetData().form.surname1 + " " + this.getContractBudgetData().form.surname2 : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.identification_document_number : '',
                        this.getContractBudgetData() && this.getContractBudgetData().form ? this.getContractBudgetData().form.birthdate : ''
                    ]
                }, {
                    key: this.userProvider.isPotentialClient() && this.isOpenFlow ? 'potential-client.summary-v2.contract_budget_data.clients_area' : '',
                    value: [
                        this.userProvider.isPotentialClient() && this.isOpenFlow ? '*******' : '',
                    ]
                }]
        });
        if (this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().selectedPaymentMethod && this.getPaymentMethodSelectedData().selectedPaymentMethod.type === 1) {
            if (this.getPaymentMethodSelectedData().selectedPaymentMethod.credit_card_info) {
                summaryData.sections.push({
                    title: 'potential-client.summary-v2.contract_budget_payment_method',
                    toggle: true,
                    rows: [{
                            key: 'potential-client.summary-v2.contract_budget_payment_method.card_number',
                            value: this.getPaymentMethodSelectedData().selectedPaymentMethod.credit_card_info && this.getPaymentMethodSelectedData().selectedPaymentMethod.credit_card_info.card_number ?
                                this.formatProvider.formatPaymentMethodNumber(this.getPaymentMethodSelectedData().selectedPaymentMethod) : ''
                        }]
                });
            }
            else {
                summaryData.sections.push({
                    title: 'potential-client.summary-v2.contract_budget_payment_method',
                    toggle: true,
                    rows: [{
                            key: 'potential-client.summary-v2.contract_budget_payment_method.payment_method',
                            value: this.translationProvider.getValueForKey('potential-client.summary-v2.contract_budget_payment_method.payment_method_card')
                        }]
                });
            }
        }
        else if (this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().selectedPaymentMethod && this.getPaymentMethodSelectedData().selectedPaymentMethod.type === 2) {
            if (!this.getPaymentMethodSelectedData().bankFormInfo) {
                summaryData.sections.push({
                    title: 'potential-client.summary-v2.contract_budget_payment_method',
                    toggle: true,
                    rows: [{
                            key: 'potential-client.summary-v2.contract_budget_payment_method.bank_account',
                            value: this.getPaymentMethodSelectedData().selectedPaymentMethod.bank_account_info.account_number ? this.formatProvider.formatPaymentMethodNumber(this.getPaymentMethodSelectedData().selectedPaymentMethod) : ''
                        }]
                });
            }
            else {
                summaryData.sections.push({
                    title: 'potential-client.summary-v2.contract_budget_payment_method',
                    toggle: true,
                    rows: [{
                            key: 'potential-client.summary-v2.contract_budget_payment_method.bank_account',
                            value: this.getPaymentMethodSelectedData().bankFormInfo.iban ? this.getPaymentMethodSelectedData().bankFormInfo.iban : this.getPaymentMethodSelectedData().bankFormInfo.account_number
                        }]
                });
            }
        }
        else if (this.getPaymentMethodSelectedData() && this.getPaymentMethodSelectedData().selectedPaymentMethod && this.getPaymentMethodSelectedData().selectedPaymentMethod.type === 3) {
            summaryData.sections.push({
                title: 'potential-client.summary-v2.contract_budget_payment_method',
                toggle: true,
                rows: [{
                        key: 'potential-client.summary-v2.contract_budget_payment_method.payment_method',
                        value: this.translationProvider.getValueForKey('potential-client.summary-v2.contract_budget_payment_method.payment_method_barcode')
                    }]
            });
        }
        summaryData.sections.push({
            rows: [{
                    accept_conditions: {
                        text1: 'potential-client.summary-v2.privacy_check1',
                        link1: 'potential-client.summary-v2.privacy_check2',
                        text2: 'potential-client.summary-v2.privacy_check3',
                        link2: 'potential-client.summary-v2.privacy_check4',
                        text3: 'potential-client.summary-v2.privacy_check5',
                        link3: 'potential-client.summary-v2.privacy_check6'
                    }
                }]
        });
        return summaryData;
    };
    PotentialClientProvider.prototype.formatDate = function (date) {
        return this.formatProvider.formatDate(date);
    };
    PotentialClientProvider.prototype.getConfirmation = function () {
        return this.potentialClient.confirmation;
    };
    PotentialClientProvider.prototype.setConfirmation = function (confirmation) {
        this.potentialClient.confirmation = confirmation;
    };
    PotentialClientProvider.prototype.getContractBudgetData = function () {
        return this.potentialClient.contractBudgetData;
    };
    PotentialClientProvider.prototype.setContractBudgetData = function (contractBudgetData) {
        this.potentialClient.contractBudgetData = contractBudgetData;
    };
    PotentialClientProvider.prototype.getPassword = function () {
        return this.potentialClient.password;
    };
    PotentialClientProvider.prototype.setPassword = function (password) {
        this.potentialClient.password = password;
    };
    PotentialClientProvider.prototype.getFiles = function () {
        return this.potentialClient.files;
    };
    PotentialClientProvider.prototype.setFiles = function (files) {
        this.potentialClient.files = files;
    };
    PotentialClientProvider.prototype.updateRequest = function (flowRequest, callback, mocked) {
        var _this = this;
        if (this.getBudgetSelectedData()) {
            if (this.isOpenFlow) {
                this.componentSettingsProvider.submitOpenPotentialClientRequest(false, this.getRequestJson(flowRequest), this.getBudgetSelectedData().id).subscribe(function (response) {
                    var mapfreResponse = _this.apiProvider.getResponseJSON(response);
                    if (mapfreResponse.data.stepData.payment_method.type === 1) {
                        _this.resultPageName = 'OpenPotentialClientResultPage';
                    }
                    else if (mapfreResponse.data.stepData.payment_method.type === 2) {
                        _this.resultPageName = 'OpenPotentialClientResultPage';
                    }
                    else {
                        _this.resultPageName = 'OpenPotentialClientResultBarcodePage';
                    }
                    _this.onRequestPut(response, callback);
                });
            }
            else {
                this.componentSettingsProvider.submitPotentialClientRequest(mocked, this.getRequestJson(flowRequest), this.getBudgetSelectedData().id).subscribe(function (response) {
                    var mapfreResponse = _this.apiProvider.getResponseJSON(response);
                    if (mapfreResponse.data.stepData.payment_method.type === 1) {
                        _this.resultPageName = 'PotentialClientResultPage';
                    }
                    else if (mapfreResponse.data.stepData.payment_method.type === 2) {
                        _this.resultPageName = 'PotentialClientResultPage';
                    }
                    else {
                        _this.resultPageName = 'PotentialClientResultBarcodePage';
                    }
                    _this.onRequestPut(response, callback);
                });
            }
        }
    };
    PotentialClientProvider.prototype.checkLastPageIsInFlow = function (navCtrl) {
        return navCtrl.last().pageRef().nativeElement.tagName.includes('POTENTIAL-CLIENT');
    };
    PotentialClientProvider.prototype.cleanProvider = function (isOpenFlow) {
        if (isOpenFlow) {
            this.initializeOpenFlowData();
        }
        else {
            this.initializeFlowData();
        }
    };
    PotentialClientProvider.prototype.setIsFlowInit = function (value) {
        this.isFlowInit = value;
    };
    PotentialClientProvider.prototype.getIsFlowInit = function () {
        return this.isFlowInit;
    };
    PotentialClientProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'PotentialClientStepsPage';
        this.resultPageName = 'PotentialClientResultPage';
        this.resetFlowData();
    };
    PotentialClientProvider.prototype.initializeOpenFlowData = function () {
        this.stepsPageName = 'OpenPotentialClientStepsPage';
        this.resultPageName = 'OpenPotentialClientResultPage';
        this.resetFlowData();
    };
    PotentialClientProvider.flowReference = 'potential-client';
    PotentialClientProvider.decorators = [
        { type: Injectable },
    ];
    PotentialClientProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ComponentSettingsProvider, },
    ]; };
    return PotentialClientProvider;
}(AbstractFlowProvider));
export { PotentialClientProvider };
//# sourceMappingURL=potential-client.js.map