var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { InsuranceProvider } from '../insurance/insurance';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
var ReceiptFractionamentProvider = (function (_super) {
    __extends(ReceiptFractionamentProvider, _super);
    function ReceiptFractionamentProvider(injector, insuranceProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.insuranceProvider = insuranceProvider;
        _this.initializeFlowData();
        return _this;
    }
    ReceiptFractionamentProvider.prototype.setRiskPolicyData = function (navParams) {
        var _this = this;
        this.riskId = navParams.get('riskId');
        Observable.forkJoin([
            this.insuranceProvider.getInsuranceDetailSettings(false, navParams.get('riskId')),
            this.insuranceProvider.getPolicyDetailSettings(false, navParams.get('policyId')),
            this.insuranceProvider.getPolicyReceipts(false, navParams.get('policyId'), 'UNPAID'),
            this.insuranceProvider.getPolicyPaymentMethod(false, navParams.get('policyId'))
        ]).subscribe(function (responses) {
            if (_this.insuranceProvider.areResponsesValid(responses)) {
                _this.insurance = _this.insuranceProvider.getResponseJSON(responses[0]).data;
                _this.policy = _this.insuranceProvider.getResponseJSON(responses[1]).data;
                _this.paymentMethod = _this.insuranceProvider.getResponseJSON(responses[3]).data;
            }
        });
    };
    ReceiptFractionamentProvider.prototype.getRiskId = function () {
        return this.riskId;
    };
    ReceiptFractionamentProvider.prototype.getPolicyData = function () {
        return this.policy ? (this.policy.car_policy_detail || this.policy.home_policy_detail || {}).basic_info : undefined;
    };
    ReceiptFractionamentProvider.prototype.getPolicyPrice = function () {
        var policyData = this.getPolicyData();
        return policyData ? policyData.price.field_content : undefined;
    };
    ReceiptFractionamentProvider.prototype.getFractionamentType = function () {
        return this.fractionamentType;
    };
    ReceiptFractionamentProvider.prototype.setFractionamentType = function (fractionamentType) {
        this.fractionamentType = fractionamentType;
    };
    ReceiptFractionamentProvider.prototype.getPaymentMethod = function () {
        return this.paymentMethod;
    };
    ReceiptFractionamentProvider.prototype.setPaymentMethod = function (method) {
        this.paymentMethod = method;
    };
    ReceiptFractionamentProvider.prototype.getPaymentPlanList = function () {
        return this.paymentPlanList;
    };
    ReceiptFractionamentProvider.prototype.setPaymentPlanList = function (value) {
        this.paymentPlanList = value;
    };
    ReceiptFractionamentProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.fractionamentType = undefined;
        this.result = undefined;
    };
    ReceiptFractionamentProvider.prototype.getRequestJson = function (formattedRequest) {
        var car_policy_detail;
        if (this.policy.car_policy_detail !== null) {
            car_policy_detail = {
                basic_info: {
                    description: this.policy.car_policy_detail.basic_info.description.field_content,
                    number: this.policy.car_policy_detail.basic_info.number.field_content,
                    due_date: this.policy.car_policy_detail.basic_info.due_date.field_content,
                    price: {
                        amount: this.policy.car_policy_detail.basic_info.price.field_content.amount,
                        iso_code: this.policy.car_policy_detail.basic_info.price.field_content.iso_code
                    },
                    payment_modality_id: this.policy.car_policy_detail.basic_info.payment_modality.field_content
                },
                vehicle_info: {
                    type: this.policy.car_policy_detail.vehicle_info.type.field_content,
                    registration: this.policy.car_policy_detail.vehicle_info.registration.field_content
                },
                taker_info: {
                    name: this.policy.car_policy_detail.taker_info.name.field_content,
                    birthdate: this.policy.car_policy_detail.taker_info.birthdate.field_content,
                    identity_document: this.policy.car_policy_detail.taker_info.identity_document.field_content,
                    gender: this.policy.car_policy_detail.taker_info.gender.field_content,
                    civil_state: this.policy.car_policy_detail.taker_info.civil_state.field_content,
                    phone: this.policy.car_policy_detail.taker_info.phone.field_content,
                    email: this.policy.car_policy_detail.taker_info.email.field_content,
                    license_date: this.policy.car_policy_detail.taker_info.license_date.field_content,
                    license_type: this.policy.car_policy_detail.taker_info.license_type.field_content
                },
                driver_info: {
                    name: this.policy.car_policy_detail.driver_info.name.field_content,
                    birthdate: this.policy.car_policy_detail.driver_info.birthdate.field_content,
                    identity_document: this.policy.car_policy_detail.driver_info.identity_document.field_content,
                    gender: this.policy.car_policy_detail.driver_info.gender.field_content,
                    civil_state: this.policy.car_policy_detail.driver_info.civil_state.field_content,
                    phone: this.policy.car_policy_detail.driver_info.phone.field_content,
                    email: this.policy.car_policy_detail.driver_info.email.field_content,
                    license_date: this.policy.car_policy_detail.driver_info.license_date.field_content,
                    license_type: this.policy.car_policy_detail.driver_info.license_type.field_content
                },
                owner_info: {
                    name: this.policy.car_policy_detail.owner_info.name.field_content,
                    birthdate: this.policy.car_policy_detail.owner_info.birthdate.field_content,
                    identity_document: this.policy.car_policy_detail.owner_info.identity_document.field_content,
                    gender: this.policy.car_policy_detail.owner_info.gender.field_content,
                    civil_state: this.policy.car_policy_detail.owner_info.civil_state.field_content,
                    phone: this.policy.car_policy_detail.owner_info.phone.field_content,
                    email: this.policy.car_policy_detail.owner_info.email.field_content,
                    license_date: this.policy.car_policy_detail.owner_info.license_date.field_content,
                    license_type: this.policy.car_policy_detail.owner_info.license_type.field_content
                }
            };
        }
        else {
            car_policy_detail = null;
        }
        var home_policy_detail;
        if (this.policy.home_policy_detail !== null) {
            home_policy_detail = {
                basic_info: {
                    description: this.policy.home_policy_detail.basic_info.description.field_content,
                    number: this.policy.home_policy_detail.basic_info.number.field_content,
                    init_date: this.policy.home_policy_detail.basic_info.init_date.field_content,
                    due_date: this.policy.home_policy_detail.basic_info.due_date.field_content,
                    duration: this.policy.home_policy_detail.basic_info.duration.field_content,
                    price: {
                        amount: this.policy.home_policy_detail.basic_info.price.field_content.amount,
                        iso_code: this.policy.home_policy_detail.basic_info.price.field_content.iso_code
                    },
                    payment_modality_id: this.policy.home_policy_detail.basic_info.payment_modality.field_content
                },
                home_info: {
                    type: this.policy.home_policy_detail.home_info.type.field_content,
                    address: this.policy.home_policy_detail.home_info.address.field_content,
                    construction_date: this.policy.home_policy_detail.home_info.construction_date.field_content,
                    regime: this.policy.home_policy_detail.home_info.regime.field_content,
                    size: this.policy.home_policy_detail.home_info.size.field_content,
                    use: this.policy.home_policy_detail.home_info.use.field_content
                },
                taker_info: {
                    address: this.policy.home_policy_detail.taker_info.address.field_content,
                    identity_document: this.policy.home_policy_detail.taker_info.identity_document.field_content,
                    name: this.policy.home_policy_detail.taker_info.name.field_content,
                    phone: this.policy.home_policy_detail.taker_info.phone.field_content
                }
            };
        }
        else {
            home_policy_detail = null;
        }
        formattedRequest.step_data = {
            associated_risk_id: this.getRiskId(),
            confirm: true,
            car_policy_detail: car_policy_detail,
            home_policy_detail: home_policy_detail
        };
        return formattedRequest;
    };
    ReceiptFractionamentProvider.prototype.updateRequest = function (flowRequest, callback, mocked) {
        var _this = this;
        var policyData = this.getPolicyData();
        policyData.payment_modality.field_content = this.fractionamentType.description;
        this.insuranceProvider.putFractionamentFlowData(mocked, this.policy.id, this.getRequestJson(flowRequest)).subscribe(function (response) {
            var mapfreResponse = _this.insuranceProvider.getResponseJSON(response);
            if (mapfreResponse) {
                _this.onRequestPut(response, callback);
            }
        });
    };
    ReceiptFractionamentProvider.prototype.getSummaryData = function () {
        var rows = [];
        if (this.insurance) {
            rows.push({
                key: 'receipt_payment_page.payment_summary.risk',
                value: this.insurance.name
            });
        }
        var policyData = this.getPolicyData();
        if (policyData) {
            rows.push({
                key: 'receipt_payment_page.payment_summary.policy_type',
                value: this.getPolicyData().description.field_content
            });
        }
        if (this.fractionamentType) {
            rows.push({
                key: 'receipt_payment_page.payment_summary.payment_type',
                value: this.fractionamentType ? this.fractionamentType.description : ''
            });
            rows.push({
                key: 'receipt_payment_page.payment_summary.amount',
                value: this.fractionamentType ? this.formatProvider.formatCost(this.fractionamentType.price_per_period) : ''
            });
        }
        if (this.paymentMethod) {
            rows.push({
                key: 'receipt_payment_page.payment_summary.payment_method',
                value: this.translationProvider.getValueForKey(this.paymentMethod.type === 1 ?
                    'receipt_payment_page.method_selection.credit_card' : 'receipt_payment_page.method_selection.bank_dom')
            });
            rows.push({
                key: 'receipt_payment_page.payment_summary.account',
                value: this.paymentMethod.type === 1 ?
                    this.paymentMethod.credit_card_info.card_number : this.paymentMethod.bank_account_info.account_number
            });
        }
        return {
            title: 'receipt_fractionament.fractionament_data',
            sections: [{
                    rows: rows
                }]
        };
    };
    ReceiptFractionamentProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'PaymentFractionamentStepsPage';
        this.resultPageName = 'FractionamentResultPage';
        this.resetFlowData();
    };
    ReceiptFractionamentProvider.decorators = [
        { type: Injectable },
    ];
    ReceiptFractionamentProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: InsuranceProvider, },
    ]; };
    return ReceiptFractionamentProvider;
}(AbstractFlowProvider));
export { ReceiptFractionamentProvider };
//# sourceMappingURL=receipt-fractionament.js.map