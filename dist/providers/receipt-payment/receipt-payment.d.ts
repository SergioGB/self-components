import { Injector } from '@angular/core';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
import { SummaryData } from '../../models/summary';
import { FlowRequest } from '../../models/flow-request';
export declare class ReceiptPaymentProvider extends AbstractFlowProvider {
    private injector;
    private receiptPayment;
    private additionalReceipts;
    constructor(injector: Injector);
    getReceiptSelectedData(): any;
    setReceiptSelectedData(receiptSelectedData: any): void;
    getPaymentMethodSelectedData(): any;
    setPaymentMethodSelectedData(paymentMethodSelectedData: any): void;
    setExtraReceipts(extraReceipt: any[]): void;
    resetFlowData(): void;
    getRequestJson(formattedRequest: FlowRequest): FlowRequest;
    getSummaryData(): SummaryData;
    updateRequest(flowRequest: FlowRequest, callback: Function, mocked?: boolean): any;
    protected initializeFlowData(): void;
}
