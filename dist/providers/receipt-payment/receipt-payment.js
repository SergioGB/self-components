var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
var ReceiptPaymentProvider = (function (_super) {
    __extends(ReceiptPaymentProvider, _super);
    function ReceiptPaymentProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.initializeFlowData();
        return _this;
    }
    ReceiptPaymentProvider.prototype.getReceiptSelectedData = function () {
        return this.receiptPayment.receiptSelectedData;
    };
    ReceiptPaymentProvider.prototype.setReceiptSelectedData = function (receiptSelectedData) {
        this.receiptPayment.receiptSelectedData = receiptSelectedData;
    };
    ReceiptPaymentProvider.prototype.getPaymentMethodSelectedData = function () {
        return this.receiptPayment.paymentMethodSelectedData;
    };
    ReceiptPaymentProvider.prototype.setPaymentMethodSelectedData = function (paymentMethodSelectedData) {
        this.receiptPayment.paymentMethodSelectedData = paymentMethodSelectedData;
    };
    ReceiptPaymentProvider.prototype.setExtraReceipts = function (extraReceipt) {
        this.additionalReceipts = extraReceipt;
    };
    ReceiptPaymentProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.receiptPayment = {};
    };
    ReceiptPaymentProvider.prototype.getRequestJson = function (formattedRequest) {
        return null;
    };
    ReceiptPaymentProvider.prototype.getSummaryData = function () {
        var sections = [];
        if (this.additionalReceipts && this.additionalReceipts.length > 0) {
            var listReceipt = [
                {
                    title: this.translationProvider.getValueForKey('receipt-payment.my_receipts.receipt_number') + " " + this.receiptPayment.receiptSelectedData.number,
                    rows: [
                        {
                            key: 'receipt_payment_page.payment_summary.amount',
                            value: this.formatProvider.formatCurrency(this.receiptPayment.receiptSelectedData.price)
                        }, {
                            key: 'receipt_payment_page.payment_summary.payment_type',
                            value: this.receiptPayment.receiptSelectedData.modality
                        }, {
                            key: 'receipt_payment_page.payment_summary.risk',
                            value: this.receiptPayment.receiptSelectedData.risk_name
                        }, {
                            key: 'receipt_payment_page.payment_summary.policy_type',
                            value: this.receiptPayment.receiptSelectedData.policy_type
                        }
                    ]
                }
            ];
            var number = void 0;
            var price = void 0;
            var risk = void 0;
            var policy = void 0;
            var cont = 2;
            for (var _i = 0, _a = this.additionalReceipts; _i < _a.length; _i++) {
                var receipt = _a[_i];
                number = this.translationProvider.getValueForKey('receipt-payment.my_receipts.receipt_number') + " " + receipt.number;
                price = this.formatProvider.formatCurrency(receipt.price);
                risk = receipt.associated_risk;
                policy = receipt.policy_type;
                listReceipt.push({
                    title: number,
                    rows: [
                        {
                            key: 'receipt_payment_page.payment_summary.amount',
                            value: price
                        }, {
                            key: 'receipt_payment_page.payment_summary.payment_type',
                            value: receipt.modality
                        }, {
                            key: 'receipt_payment_page.payment_summary.risk',
                            value: this.receiptPayment.receiptSelectedData.risk_name
                        }, {
                            key: 'receipt_payment_page.payment_summary.policy_type',
                            value: this.receiptPayment.receiptSelectedData.policy_type
                        }
                    ]
                });
                cont += 1;
            }
            sections.push({
                title: 'receipt_payment_page.payment_summary.receipts_title',
                rows: [
                    {
                        listRow: listReceipt
                    }
                ]
            });
        }
        else if (this.receiptPayment.receiptSelectedData) {
            sections.push({
                title: 'request_assistance_page.summary.receipt_title',
                rows: [
                    {
                        key: 'receipt_payment_page.payment_summary.receipt',
                        value: this.translationProvider.getValueForKey('receipt-payment.my_receipts.receipt_number') + " " + this.receiptPayment.receiptSelectedData.number
                    }, {
                        key: 'receipt_payment_page.payment_summary.amount',
                        value: this.formatProvider.formatCurrency(this.receiptPayment.receiptSelectedData.price)
                    }, {
                        key: 'receipt_payment_page.payment_summary.payment_type',
                        value: this.receiptPayment.receiptSelectedData.modality
                    }, {
                        key: 'receipt_payment_page.payment_summary.risk',
                        value: this.receiptPayment.receiptSelectedData.risk_name
                    }, {
                        key: 'receipt_payment_page.payment_summary.policy_type',
                        value: this.receiptPayment.receiptSelectedData.policy_type
                    }
                ]
            });
        }
        if (this.receiptPayment.paymentMethodSelectedData) {
            sections.push({
                title: 'request_assistance_page.summary.payment_title',
                rows: [
                    {
                        key: 'receipt_payment_page.payment_summary.payment_method',
                        value: this.translationProvider.getValueForKey(this.receiptPayment.paymentMethodSelectedData.type === 1
                            ? 'receipt_payment_page.method_selection.credit_card'
                            : 'receipt_payment_page.method_selection.bank_dom')
                    }, {
                        key: this.receiptPayment.paymentMethodSelectedData.id === 1
                            ? 'receipt_payment_page.payment_summary.credit_card'
                            : 'receipt_payment_page.payment_summary.account',
                        value: this.receiptPayment.paymentMethodSelectedData.type === 1
                            ? this.receiptPayment.paymentMethodSelectedData.credit_card_info.card_number
                            : this.receiptPayment.paymentMethodSelectedData.bank_account_info.account_number
                    }, {
                        key: 'receipt_payment_page.payment_summary.payment_date',
                        value: this.formatProvider.formatDate(new Date())
                    }
                ]
            });
        }
        return {
            title: 'receipt_payment_page.payment_summary.your_data',
            sections: sections
        };
    };
    ReceiptPaymentProvider.prototype.updateRequest = function (flowRequest, callback, mocked) {
        if (mocked) {
            callback({
                response: {}
            });
        }
        else {
        }
    };
    ReceiptPaymentProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'ReceiptPaymentStepsPage';
        this.resultPageName = 'ReceiptPaymentResultPage';
        this.resetFlowData();
    };
    ReceiptPaymentProvider.decorators = [
        { type: Injectable },
    ];
    ReceiptPaymentProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return ReceiptPaymentProvider;
}(AbstractFlowProvider));
export { ReceiptPaymentProvider };
//# sourceMappingURL=receipt-payment.js.map