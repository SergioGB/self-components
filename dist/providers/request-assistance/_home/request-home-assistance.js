var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { ComponentSettingsProvider } from '../../../providers/component-settings/component-settings';
import * as _ from 'lodash';
import { Mocks } from '../../mocks/mocks';
import { AbstractFlowProvider } from '../../_abstract/abstract-flow';
var RequestAssistanceHomeProvider = (function (_super) {
    __extends(RequestAssistanceHomeProvider, _super);
    function RequestAssistanceHomeProvider(injector, componentSettingsProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.componentSettingsProvider = componentSettingsProvider;
        _this.claimId = 1;
        _this.componentName = 'request-home-assistance';
        _this.initializeFlowData();
        return _this;
    }
    RequestAssistanceHomeProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'RequestAssistanceHomeStepsPage';
        this.resultPageName = 'RequestAssistanceResultPage';
        this.resetFlowData();
    };
    RequestAssistanceHomeProvider.prototype.isLastPageIsInFlow = function (navCtrl) {
        return navCtrl.last().pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE');
    };
    RequestAssistanceHomeProvider.prototype.isFirstPageResetDataApp = function (navCtrl) {
        var views = navCtrl.getViews();
        var viewsLength = views.length;
        if (viewsLength > 1) {
            return !views[viewsLength - 1].pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE') ||
                !views[viewsLength - 2].pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE');
        }
        else {
            return false;
        }
    };
    RequestAssistanceHomeProvider.prototype.cleanProvider = function () {
        this.initializeFlowData();
    };
    RequestAssistanceHomeProvider.prototype.getAssistanceTypes = function (mocked) {
        if (this.assistanceRequest.selectedPolicy) {
            return mocked ? Observable.of({ data: Mocks.getCarAssistanceTypes() }) :
                this.apiProvider.getObservableForSuffix(mocked, true, 'catalogs/assistance_types/', true, { risk_type: this.assistanceRequest.selectedPolicy.type });
        }
        else {
            return Observable.of(true);
        }
    };
    RequestAssistanceHomeProvider.prototype.getInsurances = function (mocked) {
        return this.apiProvider.getObservableForSuffix(mocked, false, "client/" + this.apiProvider.getClientId() + "/risks/", true);
    };
    RequestAssistanceHomeProvider.prototype.getFaultTypes = function (mocked) {
        if (this.assistanceRequest.selectedAssistanceType) {
            return mocked ? Observable.of({ data: Mocks.getFaultTypes() }) :
                this.apiProvider.getObservableForSuffix(mocked, true, "catalogs/assistance_type/" + this.getSelectedAssistanceType().id + "/fault_types", true);
        }
        else {
            return Observable.of(true);
        }
    };
    RequestAssistanceHomeProvider.prototype.getRequestJson = function (formattedRequest) {
        if (this.isCarPolicy()) {
            formattedRequest.step_data = {
                associated_risk: {
                    risk_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.risk_id : null,
                    policy_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.id : null
                },
                claim_id: this.claimId,
                car_detail: {
                    assistance_type_id: this.assistanceRequest.selectedAssistanceType ? this.assistanceRequest.selectedAssistanceType.id : null,
                    fault_type_id: this.assistanceRequest.selectedFaultType ? this.assistanceRequest.selectedFaultType.id : undefined,
                    vehicle_motion: !_.isNil(this.assistanceRequest.selectedInMovement) ? this.assistanceRequest.selectedInMovement : undefined,
                    location: {
                        road_type_id: null,
                        road_name: null,
                        road_number: null,
                        zip_code: null,
                        city_id: null,
                        province_id: null,
                        country_id: null,
                        details: null
                    }
                },
                home_detail: null
            };
        }
        else {
            formattedRequest.step_data = {
                associated_risk: {
                    risk_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.risk_id : null,
                    policy_id: this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.id : null
                },
                claim_id: this.claimId,
                car_detail: null,
                home_detail: {
                    assistance_type: {
                        assistance_type_id: this.assistanceRequest.selectedAssistanceType ? this.assistanceRequest.selectedAssistanceType.id : null,
                        details: this.assistanceRequest.selectedFailureDetail ? this.assistanceRequest.selectedFailureDetail : null
                    },
                    location: {
                        road_type_id: null,
                        road_name: null,
                        road_number: null,
                        zip_code: null,
                        city_id: null,
                        province_id: null,
                        country_id: null,
                        details: null
                    }
                }
            };
        }
        return formattedRequest;
    };
    RequestAssistanceHomeProvider.prototype.updateRequest = function (flowRequestData, callback, mocked) {
        var _this = this;
        if (mocked) {
            callback(undefined);
        }
        else {
            if (this.requestId) {
                this.apiProvider.putObservableForSuffix(mocked, false, "client/" + this.componentSettingsProvider.getClientId() + "/assistance/" + this.claimId, false, {}, this.getRequestJson(flowRequestData)).subscribe(function (response) {
                    _this.onRequestPut(response, callback);
                });
            }
            else {
                this.componentSettingsProvider.submitAssistanceRequest(mocked, this.getRequestJson(flowRequestData), this.claimId).subscribe(function (response) {
                    _this.onRequestPost(response, callback);
                });
            }
        }
    };
    RequestAssistanceHomeProvider.prototype.getAssistanceRequest = function () {
        return this.assistanceRequest;
    };
    RequestAssistanceHomeProvider.prototype.setSelectedAssistanceType = function (selectedAssistanceType) {
        this.assistanceRequest.selectedAssistanceType = selectedAssistanceType;
    };
    RequestAssistanceHomeProvider.prototype.getSelectedAssistanceType = function () {
        return this.assistanceRequest.selectedAssistanceType;
    };
    RequestAssistanceHomeProvider.prototype.setSelectedFailureDetail = function (selectedFailureDetail) {
        this.assistanceRequest.selectedFailureDetail = selectedFailureDetail;
    };
    RequestAssistanceHomeProvider.prototype.getSelectedFailureDetail = function () {
        return this.assistanceRequest.selectedFailureDetail;
    };
    RequestAssistanceHomeProvider.prototype.setSelectedFaultType = function (selectedFaultType) {
        this.assistanceRequest.selectedFaultType = selectedFaultType;
    };
    RequestAssistanceHomeProvider.prototype.getSelectedFaultType = function () {
        return this.assistanceRequest.selectedFaultType;
    };
    RequestAssistanceHomeProvider.prototype.setSelectedInMovement = function (selectedInMovement) {
        this.assistanceRequest.selectedInMovement = selectedInMovement;
    };
    RequestAssistanceHomeProvider.prototype.getSelectedInMovement = function () {
        return this.assistanceRequest.selectedInMovement;
    };
    RequestAssistanceHomeProvider.prototype.setSelectedPolicy = function (selectedPolicy) {
        this.assistanceRequest.selectedPolicy = selectedPolicy;
    };
    RequestAssistanceHomeProvider.prototype.getSelectedPolicy = function () {
        return this.assistanceRequest.selectedPolicy;
    };
    RequestAssistanceHomeProvider.prototype.isCarPolicy = function () {
        return this.assistanceRequest.selectedPolicy && this.assistanceRequest.selectedPolicy.type === 'C';
    };
    RequestAssistanceHomeProvider.prototype.setSelectedLocation = function (selectedLocation) {
        this.assistanceRequest.selectedLocation = selectedLocation;
    };
    RequestAssistanceHomeProvider.prototype.getSelectedLocation = function () {
        return this.assistanceRequest.selectedLocation;
    };
    RequestAssistanceHomeProvider.prototype.getSummaryData = function () {
        var summaryData;
        summaryData = {
            title: 'request_assistance_page.summary.title',
            sections: [
                {
                    title: 'request_assistance_page.summary.risk_selection',
                    rows: [{
                            key: 'request_assistance_page.summary.risk',
                            value: this.assistanceRequest && this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.name : ''
                        }, {
                            key: 'request_assistance_page.summary.policy',
                            value: this.assistanceRequest && this.assistanceRequest.selectedPolicy ? this.assistanceRequest.selectedPolicy.associated_policies[0].type : ''
                        }]
                }
            ]
        };
        if (this.getSelectedFaultType() && (this.getSelectedInMovement() !== null)) {
            summaryData.sections.push({
                title: 'request_assistance_page.summary.assistance_type',
                rows: [{
                        key: 'request_assistance_page.summary.assistance_type',
                        value: this.assistanceRequest && this.assistanceRequest.selectedAssistanceType ? this.assistanceRequest.selectedAssistanceType.title : ''
                    }, {
                        key: 'request_assistance_page.summary.incident_type',
                        value: this.getSelectedFaultType() ? this.getSelectedFaultType().title : undefined
                    }, {
                        key: 'request_assistance_page.summary.vehicle_status_movement',
                        value: (this.getSelectedInMovement()) ? this.translationProvider.getValueForKey('request-assistance.generic.yes') :
                            this.translationProvider.getValueForKey('request-assistance.generic.no')
                    }]
            });
        }
        else {
            summaryData.sections.push({
                title: 'request_assistance_page.summary.assistance_type',
                rows: [{
                        key: 'request_assistance_page.summary.assistance_type',
                        value: this.getSelectedAssistanceType() ? this.getSelectedAssistanceType().title : ''
                    }, {
                        key: 'request_assistance_page.summary.failure_detail',
                        value: this.getSelectedFailureDetail() !== null && this.getSelectedFailureDetail() !== '' ?
                            this.getSelectedFailureDetail() : '-'
                    }]
            });
        }
        if (this.getSelectedLocation()) {
            summaryData.sections.push({
                title: 'request_assistance_page.summary.where_are_you',
                rows: [{
                        key: 'request_assistance_page.summary.location',
                        value: this.getSelectedLocation() ? this.formatProvider.formatAddress(this.getSelectedLocation()) : undefined
                    }]
            });
        }
        return summaryData;
    };
    RequestAssistanceHomeProvider.prototype.goBackIfPreviousStepIsIncomplete = function (navController, currentStepNumber) {
        if (currentStepNumber > 1) {
            var previousStepNumber = currentStepNumber - 1;
            navController.push('RequestAssistanceStepsPage', { stepId: currentStepNumber }, { animate: false });
        }
    };
    RequestAssistanceHomeProvider.prototype.checkLastPageIsInFlow = function (navCtrl) {
        return navCtrl.last().pageRef().nativeElement.tagName.includes('REQUEST-ASSISTANCE');
    };
    RequestAssistanceHomeProvider.prototype.isProviderReset = function () {
        return !this.assistanceRequest.selectedAssistanceType;
    };
    RequestAssistanceHomeProvider.prototype.goBackToStep = function (navController) {
        navController.pop({ animate: false });
    };
    RequestAssistanceHomeProvider.prototype.advanceToStep = function (navController, stepId, localization) {
        navController.push('RequestAssistanceHomeStepsPage', { stepId: stepId + 1, localization: localization }, { animate: false });
    };
    RequestAssistanceHomeProvider.prototype.advanceToResultPage = function (navController, localization) {
        navController.push('RequestAssistanceResultPage', { localization: localization }, { animate: false });
    };
    RequestAssistanceHomeProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.assistanceRequest = {
            selectedAssistanceType: null,
            selectedFailureDetail: null,
            selectedVehicle: null,
            selectedPolicy: null,
            selectedFaultType: null,
            selectedInMovement: null,
            selectedLocation: null
        };
    };
    RequestAssistanceHomeProvider.decorators = [
        { type: Injectable },
    ];
    RequestAssistanceHomeProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ComponentSettingsProvider, },
    ]; };
    return RequestAssistanceHomeProvider;
}(AbstractFlowProvider));
export { RequestAssistanceHomeProvider };
//# sourceMappingURL=request-home-assistance.js.map