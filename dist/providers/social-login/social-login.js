import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Facebook } from '@ionic-native/facebook';
import { TwitterConnect } from '@ionic-native/twitter-connect';
import { GooglePlus } from '@ionic-native/google-plus';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { PlatformProvider } from '../platform/platform';
var SocialLoginProvider = (function () {
    function SocialLoginProvider(http, authService, platformProvider, angularFireAuth, facebook, twitterConnect, googlePlus) {
        this.http = http;
        this.authService = authService;
        this.platformProvider = platformProvider;
        this.angularFireAuth = angularFireAuth;
        this.facebook = facebook;
        this.twitterConnect = twitterConnect;
        this.googlePlus = googlePlus;
    }
    SocialLoginProvider.prototype.getAvailableNetworks = function () {
        return {
            facebook: true,
            twitter: true,
            googleplus: true
        };
    };
    SocialLoginProvider.prototype.getSocialUser = function () {
        return this._socialUser;
    };
    SocialLoginProvider.prototype.getFacebookUser = function () {
        return this._facebookUser;
    };
    SocialLoginProvider.prototype.getTwitterUser = function () {
        return this._twitterUser;
    };
    SocialLoginProvider.prototype.getGoogleUser = function () {
        return this._googleUser;
    };
    SocialLoginProvider.prototype.getSocialUserObservable = function () {
        return this.authService.authState;
    };
    SocialLoginProvider.prototype.signInWithGoogle = function (logoutOnResolution) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            (_this.onCordova() ? _this.signInWithGoogleWithCordova() : _this.signInWithGoogleWithoutCordova()).then(function (googleUser) {
                _this._googleUser = googleUser;
                resolve(googleUser);
                if (logoutOnResolution) {
                    _this.signOutWithGoogle();
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    SocialLoginProvider.prototype.signOutWithGoogle = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            (_this.onCordova() ? _this.signOutWithGoogleWithCordova() : _this.signOut()).then(function () {
                _this._googleUser = undefined;
                resolve();
            }, function (error) {
                reject(error);
            });
        });
    };
    SocialLoginProvider.prototype.signInWithTwitter = function (logoutOnResolution) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            (_this.onCordova() ? _this.signInWithTwitterWithCordova() : _this.signInWithTwitterWithoutCordova()).then(function (twitterUser) {
                _this._twitterUser = twitterUser;
                resolve(twitterUser);
                if (logoutOnResolution) {
                    _this.signOutWithTwitter();
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    SocialLoginProvider.prototype.signOutWithTwitter = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            (_this.onCordova() ? _this.signOutWithTwitterWithCordova() : _this.signOut()).then(function () {
                _this._twitterUser = undefined;
                resolve();
            }, function (error) {
                reject(error);
            });
        });
    };
    SocialLoginProvider.prototype.signInWithFacebook = function (logoutOnResolution) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            (_this.onCordova() ? _this.signInWithFacebookWithCordova() : _this.signInWithFacebookWithoutCordova()).then(function (facebookUser) {
                _this._facebookUser = facebookUser;
                resolve(facebookUser);
                if (logoutOnResolution) {
                    _this.signOutWithFacebook();
                }
            }, function (error) {
                reject(error);
            });
        });
    };
    SocialLoginProvider.prototype.signOutWithFacebook = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            (_this.onCordova() ? _this.signOutWithFacebookWithCordova() : _this.signOut()).then(function () {
                _this._facebookUser = undefined;
                resolve();
            }, function (error) {
                reject(error);
            });
        });
    };
    SocialLoginProvider.prototype.signInWithGoogleWithoutCordova = function () {
        return this.getAndSetUser(this.authService.signIn(GoogleLoginProvider.PROVIDER_ID));
    };
    SocialLoginProvider.prototype.signInWithGoogleWithCordova = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.googlePlus.login({
                'webClientId': '887354720056-rnp82hfnhtlr6na7ev41ciev852nel94.apps.googleusercontent.com',
                'offline': true
            }).then(function (response) {
                _this._socialUser = new SocialUser();
                _this._socialUser.authToken = response.accessToken;
                _this._socialUser.id = response.userId;
                _this._socialUser.email = response.email;
                _this._socialUser.firstName = response.givenName;
                _this._socialUser.lastName = response.familyName;
                _this._socialUser.photoUrl = response.imageUrl;
                resolve(_this._socialUser);
            }).catch(function (error) { return reject(error); });
        });
    };
    SocialLoginProvider.prototype.signOutWithGoogleWithCordova = function () {
        return this.googlePlus.logout();
    };
    SocialLoginProvider.prototype.signInWithFacebookWithoutCordova = function () {
        return this.getAndSetUser(this.authService.signIn(FacebookLoginProvider.PROVIDER_ID));
    };
    SocialLoginProvider.prototype.signInWithFacebookWithCordova = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.facebook.login(['public_profile'])
                .then(function (facebookLoginResponse) {
                _this._socialUser = new SocialUser();
                _this._socialUser.authToken = facebookLoginResponse.authResponse.accessToken;
                _this._socialUser.id = facebookLoginResponse.authResponse.userID;
                _this.getFacebookPicture().then(function (pictureUrl) {
                    _this._socialUser.photoUrl = pictureUrl;
                    resolve(_this._socialUser);
                }).catch(function (error) { return reject(error); });
            })
                .catch(function (error) { return reject(error); });
        });
    };
    SocialLoginProvider.prototype.getFacebookPicture = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.facebook.api(_this._socialUser.id + "/picture?redirect=false", ['public_profile']).then(function (pictureResponse) {
                resolve(pictureResponse.data.url);
            }).catch(function (error) { return reject(error); });
        });
    };
    SocialLoginProvider.prototype.signOutWithFacebookWithCordova = function () {
        return this.facebook.logout();
    };
    SocialLoginProvider.prototype.signInWithTwitterWithoutCordova = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.angularFireAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider()).then(function (response) {
                var userData = response.additionalUserInfo.profile;
                _this._socialUser = new SocialUser();
                _this._socialUser.id = userData.id;
                _this._socialUser.name = userData.name;
                _this._socialUser.photoUrl = userData.profile_image_url_https;
                resolve(_this._socialUser);
            }).catch(function (error) { return reject(error); });
        });
    };
    SocialLoginProvider.prototype.signInWithTwitterWithCordova = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.twitterConnect.login()
                .then(function (twitterConnectResponse) {
                _this._socialUser = new SocialUser();
                _this._socialUser.authToken = twitterConnectResponse.token;
                _this._socialUser.id = twitterConnectResponse.userId;
                _this.getTwitterPicture().then(function (pictureUrl) {
                    _this._socialUser.photoUrl = pictureUrl;
                    resolve(_this._socialUser);
                }).catch(function (error) { return reject(error); });
            })
                .catch(function (error) { return reject(error); });
        });
    };
    SocialLoginProvider.prototype.getTwitterPicture = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.twitterConnect.showUser().then(function (response) {
                resolve(response.profile_image_url);
            }).catch(function (response) {
                if (response.profile_image_url) {
                    resolve(response.profile_image_url);
                }
                else {
                    reject(response);
                }
            });
        });
    };
    SocialLoginProvider.prototype.signOutWithTwitterWithCordova = function () {
        return this.twitterConnect.logout();
    };
    SocialLoginProvider.prototype.onCordova = function () {
        return this.platformProvider.onCordova();
    };
    SocialLoginProvider.prototype.signOut = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this._socialUser) {
                _this.authService.signOut().then(function () {
                    _this._socialUser = undefined;
                    resolve();
                });
            }
            else {
                resolve();
            }
        });
    };
    SocialLoginProvider.prototype.getAndSetUser = function (socialUserPromise) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            socialUserPromise.then(function (socialUser) {
                _this._socialUser = socialUser;
                resolve(_this._socialUser);
            }, function () {
                reject();
            });
        });
    };
    SocialLoginProvider.decorators = [
        { type: Injectable },
    ];
    SocialLoginProvider.ctorParameters = function () { return [
        { type: Http, },
        { type: AuthService, },
        { type: PlatformProvider, },
        { type: AngularFireAuth, },
        { type: Facebook, },
        { type: TwitterConnect, },
        { type: GooglePlus, },
    ]; };
    return SocialLoginProvider;
}());
export { SocialLoginProvider };
//# sourceMappingURL=social-login.js.map