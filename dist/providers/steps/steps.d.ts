import { MapfreSteps } from 'mapfre-framework/dist';
import { Step } from '../../models/step';
import { PlatformProvider } from '../platform/platform';
export declare class StepsProvider {
    private platformProvider;
    constructor(platformProvider: PlatformProvider);
    updateSteps(steps: Step[], selectedStepId: any): void;
    getCurrentStep(steps: Step[]): Step;
    getCurrentStepIndex(steps: Step[]): number;
    resetStep(step: Step): void;
    resetSteps(steps: Step[]): void;
    navigateMapfreSteps(stepsCtrl: MapfreSteps, steps: Step[], forward?: boolean): void;
}
