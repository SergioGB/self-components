import { Injectable } from '@angular/core';
import { PlatformProvider } from '../platform/platform';
var StepsProvider = (function () {
    function StepsProvider(platformProvider) {
        this.platformProvider = platformProvider;
    }
    StepsProvider.prototype.updateSteps = function (steps, selectedStepId) {
        if (steps && steps.length) {
            steps.forEach(function (step) {
                step.completed = step.completed || (step.queryUrl && step.queryUrl.length > 0) || step.step < selectedStepId;
                step.active = step.step === selectedStepId;
                step.visitable = step.visitable || step.completed;
                step.clickable = step.visitable && !step.active;
                step.status = step.active ? 'progress' : step.completed ? 'completed' : 'disabled';
            });
        }
    };
    StepsProvider.prototype.getCurrentStep = function (steps) {
        var matches = steps.filter(function (steps) { return steps.active; });
        if (matches.length) {
            return matches[0];
        }
        else {
            return steps[0];
        }
    };
    StepsProvider.prototype.getCurrentStepIndex = function (steps) {
        return steps.indexOf(this.getCurrentStep(steps));
    };
    StepsProvider.prototype.resetStep = function (step) {
        if (step) {
            step.completed = false;
            step.active = false;
            step.visitable = false;
            step.clickable = false;
            step.subtitle = undefined;
        }
    };
    StepsProvider.prototype.resetSteps = function (steps) {
        var _this = this;
        if (steps) {
            steps.forEach(function (step) { return _this.resetStep(step); });
        }
    };
    StepsProvider.prototype.navigateMapfreSteps = function (stepsCtrl, steps, forward) {
        if (forward === void 0) { forward = true; }
        if (!this.platformProvider.onDesktop() && stepsCtrl) {
            for (var times = this.getCurrentStepIndex(steps) - 1; times > 0; times--) {
                if (forward) {
                    stepsCtrl.nextStep();
                }
                else {
                    stepsCtrl.previousStep();
                }
            }
        }
    };
    StepsProvider.decorators = [
        { type: Injectable },
    ];
    StepsProvider.ctorParameters = function () { return [
        { type: PlatformProvider, },
    ]; };
    return StepsProvider;
}());
export { StepsProvider };
//# sourceMappingURL=steps.js.map