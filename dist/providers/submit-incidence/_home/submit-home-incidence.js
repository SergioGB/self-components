var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { AbstractFlowProvider } from '../../_abstract/abstract-flow';
import { ComponentSettingsProvider } from '../../component-settings/component-settings';
var SubmitHomeIncidenceProvider = (function (_super) {
    __extends(SubmitHomeIncidenceProvider, _super);
    function SubmitHomeIncidenceProvider(injector, componentSettingsProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.componentSettingsProvider = componentSettingsProvider;
        _this.componentName = 'submit-home-incidence';
        _this.type_selected_event = 'home-claim:type-selected';
        _this.ownHouseId = '1';
        _this.initializeFlowData();
        return _this;
    }
    SubmitHomeIncidenceProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'SubmitHomeIncidenceStepsPage';
        this.resultPageName = 'SubmitHomeIncidenceResultPage';
        this.resetFlowData();
    };
    SubmitHomeIncidenceProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.insurance = undefined;
        this.incidenceDate = undefined;
        this.incidenceType = undefined;
        this.incidenceLocation = undefined;
        this.incidenceDamagedItems = undefined;
        this.incidenceThirdPartyDamages = undefined;
        this.incidenceDamageType = undefined;
        this.incidenceDamageReason = undefined;
        this.incidenceDamageDetail = undefined;
        this.incidenceInjuredData = undefined;
    };
    SubmitHomeIncidenceProvider.prototype.getRequestJson = function (formattedRequest) {
        formattedRequest.step_data = {
            car_claim_request: null,
            home_claim_request: {
                associated_risk: {
                    risk_id: this.insurance ? this.insurance.id : null,
                    policy_id: this.insurance ? this.insurance.associated_policies[0].id : null
                },
                claim_info: {
                    when: {
                        exact_day: "2019-03-16T12:59:02.755Z",
                        days_range: {
                            from: "2019-03-16T12:59:02.755Z",
                            to: "2019-03-16T12:59:02.755Z"
                        }
                    },
                    claim_type_id: "string",
                    origin: {
                        origin_id: "string",
                        details: {
                            repaired: "YES",
                            repair_amount: {
                                amount: 0,
                                iso_code: "AED"
                            },
                            bills: [],
                            repairman_type_id: "string"
                        }
                    }
                },
                damages: {
                    damaged_elements: {
                        place_id: "string",
                        damaged_elements_ids: [
                            "string"
                        ]
                    },
                    third_parties_damaged: true,
                    third_parties_info: [
                        {
                            personal_info: {
                                name: "string",
                                surname1: "string",
                                surname2: "string",
                                identification_document_id: "string",
                                identification_document_number: "string",
                                address: {
                                    road_type_id: "string",
                                    road_name: "string",
                                    road_number: "string",
                                    zip_code: "string",
                                    city_id: "string",
                                    province_id: "string",
                                    country_id: "string"
                                },
                                birthdate: "2019-03-16T12:59:02.755Z",
                                birthplace: "string",
                                gender: "string",
                                civil_state: "string",
                                nationality: "string",
                                profession: "string"
                            },
                            contact_info: {
                                phone: "string",
                                phone_daytime_slot_id: "string",
                                mail: "string",
                                mail_daytime_slot_id: "string",
                                preference_id: "string"
                            },
                            details: "string"
                        }
                    ],
                    damage_type_id: "string",
                    damage_cause_id: "string",
                    details: "string"
                },
                professional: {
                    id: "string",
                    client_name: "string",
                    client_surnames: "string",
                    phone: "string",
                    mail: "string",
                    date: "2019-03-16T12:59:02.755Z",
                    time: "string"
                }
            }
        };
        return formattedRequest;
    };
    SubmitHomeIncidenceProvider.prototype.updateRequest = function (formattedRequest, callback, mocked) {
        var _this = this;
        if (mocked) {
            callback(undefined);
        }
        else {
            if (this.requestId) {
                this.putFullSubmitIncidenceRequest(this.requestId, this.getRequestJson(formattedRequest)).subscribe(function (response) {
                    _this.onRequestPut(response, callback);
                });
            }
            else {
                this.postFullSubmitIncidenceRequest(this.getRequestJson(formattedRequest)).subscribe(function (response) {
                    _this.onRequestPost(response, callback);
                });
            }
        }
    };
    SubmitHomeIncidenceProvider.prototype.postFullSubmitIncidenceRequest = function (claimRequest) {
        return this.componentSettingsProvider.postClaim(false, claimRequest);
    };
    SubmitHomeIncidenceProvider.prototype.putFullSubmitIncidenceRequest = function (requestId, claimRequest) {
        return this.componentSettingsProvider.putClaim(false, claimRequest, requestId);
    };
    SubmitHomeIncidenceProvider.prototype.advanceToStep = function (navController, stepId) {
        navController.push('SubmitHomeIncidencePage', { stepId: stepId + 1 }, { animate: false });
    };
    SubmitHomeIncidenceProvider.prototype.advanceToResultPage = function (navController) {
        navController.push('SubmitHomeIncidenceResultPage', { animate: false });
    };
    SubmitHomeIncidenceProvider.prototype.areThirdPartyDamagesAvailable = function () {
        return this.incidenceLocation && this.incidenceLocation.id === this.ownHouseId;
    };
    SubmitHomeIncidenceProvider.prototype.getInsurance = function () {
        return this.insurance;
    };
    SubmitHomeIncidenceProvider.prototype.setInsurance = function (insurance) {
        this.insurance = insurance;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceDate = function () {
        return this.incidenceDate;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceDate = function (incidenceDate) {
        this.incidenceDate = incidenceDate;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceType = function () {
        return this.incidenceType;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceType = function (incidenceDamageReason) {
        this.incidenceType = incidenceDamageReason;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceLocation = function () {
        return this.incidenceLocation;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceLocation = function (incidenceLocation) {
        this.incidenceLocation = incidenceLocation;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceDamagedItems = function () {
        return this.incidenceDamagedItems;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceDamagedItems = function (incidenceDamagedItems) {
        this.incidenceDamagedItems = incidenceDamagedItems;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceThirdPartyDamages = function () {
        return this.incidenceThirdPartyDamages;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceThirdPartyDamages = function (incidenceThirdPartyDamages) {
        this.incidenceThirdPartyDamages = incidenceThirdPartyDamages;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceDamageType = function () {
        return this.incidenceDamageType;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceDamageType = function (incidenceDamageType) {
        this.incidenceDamageType = incidenceDamageType;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceDamageReason = function () {
        return this.incidenceDamageReason;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceDamageReason = function (incidenceDamageDetail) {
        this.incidenceDamageReason = incidenceDamageDetail;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceDamageDetail = function () {
        return this.incidenceDamageDetail;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceDamageDetail = function (incidenceDamageDetail) {
        this.incidenceDamageDetail = incidenceDamageDetail;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceInjuredData = function () {
        return this.incidenceInjuredData;
    };
    SubmitHomeIncidenceProvider.prototype.setIncidenceInjuredData = function (incidenceInjuredData) {
        this.incidenceInjuredData = incidenceInjuredData;
    };
    SubmitHomeIncidenceProvider.prototype.getAppointmentData = function () {
        return this.appointmentData;
    };
    SubmitHomeIncidenceProvider.prototype.setAppointmentData = function (appointmentData) {
        this.appointmentData = appointmentData;
    };
    SubmitHomeIncidenceProvider.prototype.isResetData = function () {
        return !this.insurance;
    };
    SubmitHomeIncidenceProvider.prototype.getIncidenceDateString = function (formatProvider) {
        var dateString = '';
        if (this.incidenceDate && this.incidenceDate.selectedMode === 'exact') {
            dateString += this.translationProvider.getValueForKey('submit-home-incidence.fuzzy_date_selector.the') + " ";
            dateString += formatProvider.formatDate(this.incidenceDate.exactDate);
        }
        if (this.incidenceDate && this.incidenceDate.selectedMode === 'fuzzy') {
            dateString += this.translationProvider.getValueForKey('submit-home-incidence.fuzzy_date_selector.start_date') + " ";
            dateString += formatProvider.formatDate(this.incidenceDate.startDate);
            dateString += " " + this.translationProvider.getValueForKey('submit-home-incidence.fuzzy_date_selector.end_date') + " ";
            dateString += formatProvider.formatDate(this.incidenceDate.endDate);
        }
        return dateString;
    };
    SubmitHomeIncidenceProvider.prototype.getLocationTypeSelected = function () {
        return this.locationTypeSelected;
    };
    SubmitHomeIncidenceProvider.prototype.setLocationTypeSelected = function (loc) {
        this.locationTypeSelected = loc;
    };
    SubmitHomeIncidenceProvider.prototype.getSummaryData = function () {
        var _this = this;
        var insurance = this.getInsurance();
        var incidenceDate = this.getIncidenceDate();
        var incidenceType = this.getIncidenceType();
        var incidenceLocation = this.getIncidenceLocation();
        var incidenceDamagedItems = this.getIncidenceDamagedItems();
        var incidenceThirdPartyDamages = this.getIncidenceThirdPartyDamages();
        var incidenceDamageType = this.getIncidenceDamageType();
        var incidenceDamageReason = this.getIncidenceDamageReason();
        var incidenceDamageDetail = this.getIncidenceDamageDetail();
        var incidenceInjuredData = this.getIncidenceInjuredData();
        var appointmentData = this.getAppointmentData();
        var summarySections = [];
        if (insurance) {
            summarySections.push({
                title: this.translationProvider.getValueForKey('submit-home-incidence.process_steps.indicences.risk'),
                rows: [{
                        key: 'submit_incidence.summary.risk_selection.risk',
                        value: insurance.name
                    }, {
                        key: 'submit_incidence.summary.risk_selection.policy',
                        value: insurance.associated_policies ? insurance.associated_policies[0].type : ''
                    }]
            });
        }
        var sources = [];
        if (incidenceLocation) {
            sources.push(incidenceLocation.title);
        }
        if (incidenceDamageType) {
            sources.push(incidenceDamageType.title);
        }
        if (incidenceLocation && incidenceLocation.data && incidenceLocation.data.professional) {
            sources.push(incidenceLocation.data.professional.title);
        }
        var damagedItems = [];
        if (incidenceDamagedItems) {
            damagedItems.push(incidenceDamagedItems.title);
            if (incidenceDamagedItems.items && incidenceDamagedItems.items.length > 0) {
                incidenceDamagedItems.items.forEach(function (item) {
                    if (item.selected) {
                        damagedItems.push("\u2022 " + item.title);
                    }
                });
            }
        }
        var whenWhereHowRows = [{
                key: 'submit_incidence.summary.when_where_how.when',
                value: this.getIncidenceDateString(this.formatProvider)
            }, {
                key: 'submit_incidence.summary.what_happened',
                value: incidenceType ? incidenceType.title : ''
            }, {
                key: 'submit_incidence.summary.source',
                value: sources
            }, {
                key: 'submit_incidence.summary.damaged_items',
                value: damagedItems
            }];
        if (incidenceThirdPartyDamages) {
            whenWhereHowRows.push({
                key: 'submit_incidence.summary.third_party_damages',
                value: incidenceThirdPartyDamages.title
            });
        }
        summarySections.push({
            rows: whenWhereHowRows
        });
        if (incidenceDamageType) {
            summarySections.push({
                rows: [{
                        key: 'submit_incidence.summary.damages_detail.type',
                        value: incidenceDamageType.title
                    }, {
                        key: 'submit_incidence.summary.damages_detail.source',
                        value: incidenceDamageReason ? incidenceDamageReason.title : '-'
                    }, {
                        key: 'submit_incidence.summary.damages_detail.description',
                        value: incidenceDamageDetail || '-'
                    }]
            });
        }
        if (incidenceInjuredData && incidenceInjuredData.length > 0) {
            var injuredRows_1 = [];
            incidenceInjuredData.forEach(function (singleInjuredData, i) {
                injuredRows_1.push({
                    key: _this.translationProvider.getValueForKey('submit-home-incidence.submit_incidence.summary.injured_third_party.title') + " " + (i + 1),
                    value: [
                        singleInjuredData.name + " " + singleInjuredData.surname1 + " " + singleInjuredData.surname2,
                        singleInjuredData.phone,
                        _this.formatProvider.formatAddress(_this.getLocation(singleInjuredData))
                    ]
                });
            });
            summarySections.push({
                title: this.translationProvider.getValueForKey('submit-home-incidence.submit_home_incidence.third_party_damages.step.title'),
                rows: injuredRows_1
            });
        }
        if (appointmentData) {
            var professionalValues = [];
            if (appointmentData.workshop) {
                if (appointmentData.workshop.title) {
                    professionalValues.push(appointmentData.workshop.title);
                }
                if (appointmentData.workshop.address) {
                    professionalValues.push(appointmentData.workshop.address);
                }
            }
            if (appointmentData.appointment) {
                if (appointmentData.appointment.date && appointmentData.appointment.time) {
                    professionalValues.push(this.translationProvider.getValueForKey('submit-home-incidence.workshop_display.appointment.arranged_on_date') + "\n             " + this.formatProvider.formatDate(appointmentData.appointment.date) + "\n             " + this.translationProvider.getValueForKey('submit-home-incidence.workshop_display.appointment.arranged_at_time') + "\n             " + appointmentData.appointment.time);
                }
            }
            summarySections.push({
                title: 'submit_incidence.summary.choose_professional.title',
                rows: [{
                        key: 'submit_incidence.summary.choose_professional.recommended_professionals',
                        value: professionalValues
                    }]
            });
        }
        return {
            title: 'submit_incidence_page.summary.title',
            sections: summarySections
        };
    };
    SubmitHomeIncidenceProvider.prototype.getLocation = function (address) {
        return {
            road_type_id: address.road_type ? address.road_type.id : undefined,
            road_type_name: address.road_type ? address.road_type.title : '',
            road_name: address.road_name,
            road_number: address.road_number,
            direction: address.direction,
            cp: address.postal_code,
            city_id: address.city ? address.city.id : undefined,
            city_name: address.city ? address.city.title : '',
            province_id: address.province ? address.province.id : undefined,
            province_name: address.province ? address.province.title : '',
            country_id: address.country ? address.country.id : undefined,
            country_name: address.country ? address.country.title : ''
        };
    };
    SubmitHomeIncidenceProvider.decorators = [
        { type: Injectable },
    ];
    SubmitHomeIncidenceProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ComponentSettingsProvider, },
    ]; };
    return SubmitHomeIncidenceProvider;
}(AbstractFlowProvider));
export { SubmitHomeIncidenceProvider };
//# sourceMappingURL=submit-home-incidence.js.map