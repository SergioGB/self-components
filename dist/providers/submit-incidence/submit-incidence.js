var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import * as _ from 'lodash';
import { Utils } from '../utils/utils';
import { ClaimCarDetail } from '../../models/claim-car-detail';
import { ClaimHomeDetail } from '../../models/claim-home-detail';
import { Claim } from '../../models/claim';
import { AssociatedDocuments } from '../../models/associated-documents';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
import { Observable } from 'rxjs/Observable';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
var SubmitIncidenceProvider = (function (_super) {
    __extends(SubmitIncidenceProvider, _super);
    function SubmitIncidenceProvider(injector, componentSettingsProvider) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.componentSettingsProvider = componentSettingsProvider;
        _this.withOtherVehiclesInvolvedTypeId = '1';
        _this.componentName = 'submit-incidence';
        _this.initializeFlowData();
        return _this;
    }
    SubmitIncidenceProvider.prototype.checkLastPageIsInFlow = function (navCtrl) {
        return this.isPageIsInFlow(navCtrl.last().pageRef());
    };
    SubmitIncidenceProvider.prototype.checkFirstPageResetDataApp = function (navCtrl) {
        var views = navCtrl.getViews();
        var viewsLength = views.length;
        if (viewsLength > 1) {
            return !this.isPageIsInFlow(views[viewsLength - 1].pageRef()) || !this.isPageIsInFlow(views[viewsLength - 2].pageRef());
        }
        else {
            return false;
        }
    };
    SubmitIncidenceProvider.prototype.isPageIsInFlow = function (elementRef) {
        return elementRef.nativeElement.tagName.includes('SUBMIT-INCIDENCE') || elementRef.nativeElement.tagName.includes('SUBMIT-HOME-INCIDENCE');
    };
    SubmitIncidenceProvider.prototype.isCallMeBack = function () {
        return this.insuranceSelectedData
            && this.insuranceSelectedData.associated_policies
            && this.insuranceSelectedData.associated_policies.length > 0
            && this.insuranceSelectedData.associated_policies[0].id === '1';
    };
    SubmitIncidenceProvider.prototype.setInsurance = function (insurance) {
        this.insuranceSelectedData = insurance;
    };
    SubmitIncidenceProvider.prototype.getInsurance = function () {
        return this.insuranceSelectedData;
    };
    SubmitIncidenceProvider.prototype.setSinister = function (sinister) {
        this.sinister = sinister;
    };
    SubmitIncidenceProvider.prototype.getSinister = function () {
        return this.sinister;
    };
    SubmitIncidenceProvider.prototype.setOtherParticipants = function (otherParticipants) {
        this.otherParticipantsData = otherParticipants;
    };
    SubmitIncidenceProvider.prototype.getOtherParticipants = function () {
        return this.otherParticipantsData;
    };
    SubmitIncidenceProvider.prototype.setHowWhenWhere = function (howWhenWhere) {
        this.howWhenWhereSelectedData = howWhenWhere;
    };
    SubmitIncidenceProvider.prototype.getHowWhenWhere = function () {
        return this.howWhenWhereSelectedData;
    };
    SubmitIncidenceProvider.prototype.setOwnDamagesSelected = function (ownDamagesSelected) {
        this.ownDamagesSelected = ownDamagesSelected;
        if (!this.ownDamagesSelected) {
            this.professionalSelectedData = undefined;
            this.professionalMeetingSelectedData = undefined;
            this.professionalMeetingShown = false;
            this.professionalMeetingConfirmationShown = false;
        }
    };
    SubmitIncidenceProvider.prototype.getOwnDamagesSelected = function () {
        return this.ownDamagesSelected;
    };
    SubmitIncidenceProvider.prototype.setProfessionalSelected = function (professional) {
        this.professionalSelectedData = professional;
    };
    SubmitIncidenceProvider.prototype.getProfessionalSelected = function () {
        return this.professionalSelectedData;
    };
    SubmitIncidenceProvider.prototype.setOwnProfessionalSelected = function (professional) {
        this.professionalOwnSelectedData = professional;
    };
    SubmitIncidenceProvider.prototype.getOwnProfessionalSelected = function () {
        return this.professionalOwnSelectedData;
    };
    SubmitIncidenceProvider.prototype.setProfessionalMeetingTimeSelected = function (meeting) {
        this.professionalMeetingSelectedData = meeting;
    };
    SubmitIncidenceProvider.prototype.getProfessionalMeetingTimeSelected = function () {
        return this.professionalMeetingSelectedData;
    };
    SubmitIncidenceProvider.prototype.setClaimId = function (claim_id) {
        this.claim_id = claim_id;
    };
    SubmitIncidenceProvider.prototype.getClaimId = function () {
        return this.claim_id;
    };
    SubmitIncidenceProvider.prototype.setAssociatedRisk = function (associated_risk, insurance_type) {
        if (insurance_type === 'C') {
            this.claim.car_claim_request.associated_risk = associated_risk;
        }
        else if (insurance_type === 'H') {
            this.claim.home_claim_request.associated_risk = associated_risk;
        }
    };
    SubmitIncidenceProvider.prototype.getAssociatedRisk = function () {
        if (this.claim.car_claim_request) {
            return this.claim.car_claim_request.associated_risk;
        }
        else if (this.claim.home_claim_request) {
            return this.claim.home_claim_request.associated_risk;
        }
    };
    SubmitIncidenceProvider.prototype.setClaimDescription = function (claim_description) {
        this.claim.car_claim_request.claim_description = claim_description;
    };
    SubmitIncidenceProvider.prototype.getClaimDescription = function () {
        return this.claim.car_claim_request.claim_description;
    };
    SubmitIncidenceProvider.prototype.setClaimInfo = function (claim_info) {
        this.claim.car_claim_request.claim_info = claim_info;
    };
    SubmitIncidenceProvider.prototype.getOtherVehiclesInvolved = function () {
        if (this.claim.car_claim_request.claim_info && this.claim.car_claim_request.claim_info.how_other_vehicles) {
            return this.claim.car_claim_request.claim_info.how_other_vehicles;
        }
        else {
            return [];
        }
    };
    SubmitIncidenceProvider.prototype.getClaimInfo = function () {
        return this.claim.car_claim_request.claim_info;
    };
    SubmitIncidenceProvider.prototype.getClaimUninsuredVehicleInfo = function () {
        return this.claim.car_claim_request.uninsured_vehicle_info;
    };
    SubmitIncidenceProvider.prototype.setClaimUninsuredVehicleInfo = function (claimUninsuredVehicleInfo) {
        this.claim.car_claim_request.uninsured_vehicle_info = claimUninsuredVehicleInfo;
    };
    SubmitIncidenceProvider.prototype.setDamages = function (damages) {
        this.claim.car_claim_request.damages = damages;
    };
    SubmitIncidenceProvider.prototype.getDamages = function () {
        return this.claim.car_claim_request.damages;
    };
    SubmitIncidenceProvider.prototype.getClaim = function () {
        return this.claim;
    };
    SubmitIncidenceProvider.prototype.getOnlineMeeting = function () {
        return this.onlineMeeting;
    };
    SubmitIncidenceProvider.prototype.setOnlineMeeting = function (onlineMeeting) {
        this.onlineMeeting = onlineMeeting;
    };
    SubmitIncidenceProvider.prototype.updateRequest = function (formattedRequest, callback, mocked) {
        var _this = this;
        if (mocked) {
            callback(undefined);
        }
        else {
            if (this.requestId) {
                this.putFullSubmitIncidenceRequest(this.requestId, this.getRequestJson(formattedRequest)).subscribe(function (response) {
                    _this.onRequestPut(response, callback);
                });
            }
            else {
                this.postFullSubmitIncidenceRequest(this.getRequestJson(formattedRequest)).subscribe(function (response) {
                    _this.setClaimId(response.json().data && response.json().data.stepData ? response.json().data.stepData.id : undefined);
                    _this.onRequestPost(response, callback);
                });
            }
        }
    };
    SubmitIncidenceProvider.prototype.postFullSubmitIncidenceRequest = function (claimRequest) {
        return this.componentSettingsProvider.postClaim(false, claimRequest);
    };
    SubmitIncidenceProvider.prototype.putFullSubmitIncidenceRequest = function (requestId, claimRequest) {
        return this.componentSettingsProvider.putClaim(false, claimRequest, requestId);
    };
    SubmitIncidenceProvider.prototype.getRequestJson = function (formattedRequest) {
        var request = Utils.copy((this.claim));
        if (request && request.damages && request.damages.others_damages) {
            request && request.damages && request.damages.others_damages.forEach(function (other_participant) {
                other_participant.contact_channel = other_participant.contact_channel ? other_participant.contact_channel.id : undefined;
                other_participant.vehicle_type = other_participant.vehicle_type ? other_participant.vehicle_type.id : undefined;
                other_participant.vehicle_color = other_participant.vehicle_color ? other_participant.vehicle_color.id : undefined;
                other_participant.vehicle_brand = other_participant.vehicle_brand ? other_participant.vehicle_brand.id : undefined;
                other_participant.vehicle_model = other_participant.vehicle_model ? other_participant.vehicle_model.id : undefined;
            });
        }
        formattedRequest.step_data = request;
        return formattedRequest;
    };
    SubmitIncidenceProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.insuranceSelectedData = undefined;
        this.sinister = undefined;
        this.howWhenWhereSelectedData = undefined;
        this.professionalSelectedData = undefined;
        this.professionalMeetingSelectedData = undefined;
        this.coverageNotIncluded = undefined;
        this.incidenceSubmitted = false;
        this.callMeBack = false;
        this.ownDamagesSelected = false;
        this.ownDocuments = [];
        this.othersDocuments = [];
        this.othersVehicleAreas = [];
        this.claim = new Claim();
        this.claim.car_claim_request = new ClaimCarDetail();
        this.claim.home_claim_request = new ClaimHomeDetail();
        this.claim.car_claim_request.associated_docs = new AssociatedDocuments();
    };
    SubmitIncidenceProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'SubmitIncidenceInsuranceSelectionStepPage';
        this.resultPageName = 'SubmitIncidenceConfirmationStepPage';
        this.resetFlowData();
        this.getFormData();
    };
    SubmitIncidenceProvider.prototype.setOwnDocuments = function (file) {
        this.ownDocuments = file;
    };
    SubmitIncidenceProvider.prototype.getSummaryData = function () {
        var _this = this;
        var damagesData = this.getDamages();
        var damages_ref_own = '';
        if (damagesData && damagesData.own_damages && damagesData.own_damages.damages_ref) {
            switch (damagesData.own_damages.damages_ref) {
                case '1':
                    damages_ref_own = this.translationProvider.getValueForKey('submit-incidence.submit_incidence.own_damages_form.vehicle_damage.partial_damages');
                    break;
                case '2':
                    damages_ref_own = this.translationProvider.getValueForKey('submit-incidence.submit_incidence.own_damages_form.vehicle_damage.total_damage');
                    break;
                case '3':
                    damages_ref_own = this.translationProvider.getValueForKey('submit-incidence.submit_incidence.own_damages_form.vehicle_damage.burned');
                    break;
                case '4':
                    damages_ref_own = this.translationProvider.getValueForKey('submit-incidence.submit_incidence.own_damages_form.vehicle_damage.disappeared');
                    break;
            }
        }
        var damages_ref_others = [];
        if (damagesData && damagesData.others_damages && damagesData.others_damages.length > 0) {
            damagesData.others_damages.forEach(function (item, index) {
                if (item.damages_ref) {
                    switch (damagesData.others_damages.damages_ref) {
                        case '1':
                            damages_ref_others[index] = _this.translationProvider.getValueForKey('submit-incidence.summary-v2.own_damages_form.vehicle_damage.partial_damages');
                            break;
                        case '2':
                            damages_ref_others[index] = _this.translationProvider.getValueForKey('submit-incidence.summary-v2.own_damages_form.vehicle_damage.total_damage');
                            break;
                        case '3':
                            damages_ref_others[index] = _this.translationProvider.getValueForKey('submit-incidence.summary-v2.own_damages_form.vehicle_damage.burned');
                            break;
                        case '4':
                            damages_ref_others[index] = _this.translationProvider.getValueForKey('submit-incidence.summary-v2.own_damages_form.vehicle_damage.disappeared');
                            break;
                    }
                }
            });
        }
        var damages = {};
        if (damagesData && damagesData.own_damages && damagesData.other_damages && damagesData.other_participants) {
            damages = {
                own: [
                    damagesData.own_damages.isDamaged ?
                        this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.damages.own_damages.my_vehicle_damages') : null,
                    damagesData.own_damages.circulate ?
                        this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.damages.own_damages.my_vehicle_circulate') : null,
                    damages_ref_own
                ],
                otherDamages: [
                    damagesData.other_damages.furniture ?
                        this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.damages.other_damages.furniture_damages') : null,
                    damagesData.other_damages.animals ?
                        this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.damages.other_damages.animals_damages') : null,
                    damagesData.other_damages.more_info
                ],
                otherParticipants: [
                    damagesData.other_participants.healthcare ?
                        this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.damages.other_participants.sanitary_assistance') : null,
                    damagesData.other_participants.tow_truck ?
                        this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.damages.other_participants.tow_truck') : null,
                    damagesData.other_participants.authorities ?
                        this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.damages.other_participants.authorities') : null
                ]
            };
        }
        var summaryData = {
            title: 'submit_incidence.summary.title',
            sections: []
        };
        if (this.getInsurance()) {
            summaryData.sections.push({
                title: 'submit_incidence.summary.risk_selection.title',
                rows: [{
                        key: 'submit_incidence.summary.risk_selection.risk',
                        value: this.getInsurance().name ? this.getInsurance().name : '–'
                    }, {
                        key: 'submit_incidence.summary.risk_selection.policy',
                        value: this.getInsurance().associated_policies[0].type ? this.getInsurance().associated_policies[0].type : '–'
                    }]
            });
        }
        else if (this.getClaimUninsuredVehicleInfo()) {
            summaryData.sections.push({
                title: 'submit_incidence.summary.risk_selection.title',
                rows: [{
                        key: 'submit_incidence.summary.risk_selection.name',
                        value: this.getClaimUninsuredVehicleInfo().personal_data.name + ' ' + this.getClaimUninsuredVehicleInfo().personal_data.surname1 + ' ' + this.getClaimUninsuredVehicleInfo().personal_data.surname2
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.document_number',
                        value: this.getClaimUninsuredVehicleInfo().personal_data.identification_document_number ? this.getClaimUninsuredVehicleInfo().personal_data.identification_document_number : "–"
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.phone',
                        value: this.getClaimUninsuredVehicleInfo().personal_data.phone ? this.getClaimUninsuredVehicleInfo().personal_data.phone : "–"
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.direction',
                        value: this.getClaimUninsuredVehicleInfo().personal_data.address.road_name ? this.getClaimUninsuredVehicleInfo().personal_data.address.road_name : "–"
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.risk',
                        value: this.getVehicleBrand(this.getClaimUninsuredVehicleInfo().vehicle_info.vehicle_brand_id) + ' ' + this.getVehicleModel(this.getClaimUninsuredVehicleInfo().vehicle_info.vehicle_model_id)
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.registration_date',
                        value: this.getClaimUninsuredVehicleInfo().vehicle_info.registration_date ? this.getClaimUninsuredVehicleInfo().vehicle_info.registration_date : "–"
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.doors',
                        value: this.getClaimUninsuredVehicleInfo().vehicle_info.doors_count ? this.getClaimUninsuredVehicleInfo().vehicle_info.doors_count : "–"
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.registration',
                        value: this.getClaimUninsuredVehicleInfo().vehicle_info.registration ? this.getClaimUninsuredVehicleInfo().vehicle_info.registration : "–"
                    },
                    {
                        key: 'submit_incidence.summary.risk_selection.color',
                        value: this.getVehicleColor(this.getClaimUninsuredVehicleInfo().vehicle_info.vehicle_color_id) ? this.getVehicleColor(this.getClaimUninsuredVehicleInfo().vehicle_info.vehicle_color_id) : "–"
                    }]
            });
        }
        if (this.sinister) {
            if (this.sinister.type) {
                var sinisterTypeRows = [{
                        key: 'submit_incidence.summary.risk_type.sinister_type',
                        value: this.sinister.type.title ? this.sinister.type.title : "–"
                    }];
                if (this.sinister.sinister_adjuster) {
                    sinisterTypeRows.push({
                        key: 'submit_incidence.summary.risk_type.adjuster',
                        value: this.sinister.sinister_adjuster_detail ? this.sinister.sinister_adjuster_detail : "–"
                    });
                }
                sinisterTypeRows.push({
                    key: 'submit_incidence.summary.risk_type.injured',
                    value: this.translationProvider.getValueForKey(this.sinister.injured ?
                        'submit_incidence.summary.risk_type.injured_yes' : 'submit_incidence.summary.risk_type.injured_no') ? this.translationProvider.getValueForKey(this.sinister.injured ?
                        'submit_incidence.summary.risk_type.injured_yes' : 'submit_incidence.summary.risk_type.injured_no') : "–"
                });
                if (this.sinister.injured && this.sinister.injured_detail) {
                    sinisterTypeRows.push({
                        key: 'submit_incidence.summary.risk_type.injured_number',
                        value: [
                            (this.sinister.injured_detail.injured_number_own_vehicle || 0) + "\n                " + this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.risk_type.injured_vehicle'),
                            (this.sinister.injured_detail.injured_number_others_vehicle || 0) + "\n                " + this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.risk_type.injured_other_vehicle'),
                            (this.sinister.injured_detail.injured_cyclist || 0) + "\n                " + this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.risk_type.injured_cyclist'),
                            (this.sinister.injured_detail.injured_pedestrian || 0) + "\n                " + this.translationProvider.getValueForKey('submit-incidence.submit_incidence.summary.risk_type.injured_pedestrian')
                        ]
                    });
                }
                summaryData.sections.push({
                    title: 'submit_incidence.summary.risk_type.title',
                    rows: sinisterTypeRows
                });
            }
        }
        if (this.getHowWhenWhere()) {
            var howWhenWhere = this.getHowWhenWhere();
            var infoRows = [{
                    key: 'submit_incidence.summary.when_where_how.when',
                    value: this.formatProvider.formatDate(howWhenWhere.dateTime) + " " + this.formatProvider.formatTime(howWhenWhere.dateTime),
                }, {
                    key: 'submit_incidence.summary.when_where_how.where',
                    value: howWhenWhere.address ? howWhenWhere.address : "–"
                }, {
                    key: 'submit_incidence.summary.when_where_how.how_own_vehicle',
                    value: this.getHowOwnVehicle(howWhenWhere.howOwnVehicle) ? this.getHowOwnVehicle(howWhenWhere.howOwnVehicle) : '–'
                }];
            if (this.wereOtherVehiclesInvolved()) {
                infoRows.push({
                    key: 'submit_incidence.summary.when_where_how.how_contrary_vehicle',
                    value: this.getHowOtherVehicles(howWhenWhere.howOtherVehicles) ? this.getHowOtherVehicles(howWhenWhere.howOtherVehicles) : '–'
                });
            }
            summaryData.sections.push({
                title: 'submit_incidence.summary.when_where_how.title',
                rows: infoRows
            });
        }
        if (damagesData) {
            var damagesRows_1 = [];
            damagesRows_1.push({
                key: 'submit_incidence.summary.damages.own_damages',
                value: this.getDamagesData(damages.own) ? this.getDamagesData(damages.own) : '-'
            });
            if (damagesData.own_damages && damagesData.own_damages.areas) {
                damagesData.own_damages.areas.forEach(function (area) {
                    damagesRows_1.push({
                        key: area.area,
                        value: _this.getDamagedSubareas(area) ? _this.getDamagedSubareas(area) : "–"
                    });
                });
            }
            if (this.wereOtherVehiclesInvolved()) {
                if (damagesData.others_damages && damagesData.others_damages.length > 0) {
                    damagesData.others_damages.forEach(function (item, index) {
                        var othersDamages = [
                            damagesData.others_damages[index].isDamaged ?
                                _this.translationProvider.getValueForKey('submit-incidence.summary-v2.damages.own_damages.my_vehicle_damages') : null,
                            damagesData.others_damages[index].circulate ?
                                _this.translationProvider.getValueForKey('submit-incidence.summary-v2.damages.own_damages.my_vehicle_circulate') : null,
                            damages_ref_others[index]
                        ];
                        damagesRows_1.push({
                            key: 'submit-incidence.summary-v2.damages.contrary_damages',
                            value: othersDamages ? othersDamages : '-'
                        });
                        if (item.areas) {
                            item.areas.forEach(function (area) {
                                damagesRows_1.push({
                                    key: area.area,
                                    value: _this.getDamagedSubareas(area) ? _this.getDamagedSubareas(area) : "–"
                                });
                            });
                        }
                    });
                }
            }
            if (damages.otherDamages) {
                damagesRows_1.push({
                    key: 'submit_incidence.summary.damages.other_damages',
                    value: this.getDamagesData(damages.otherDamages) ? this.getDamagesData(damages.otherDamages) : "–"
                });
            }
            if (damages.otherParticipants) {
                damagesRows_1.push({
                    key: 'submit_incidence.summary.damages.other_participants',
                    value: damages.otherParticipants ? damages.otherParticipants : "–"
                });
            }
            summaryData.sections.push({
                title: 'submit_incidence.summary.damages.title',
                rows: damagesRows_1
            });
        }
        if (this.getProfessionalSelected()) {
            var professional = this.getProfessionalSelected();
            var professionalValues = [];
            if (professional.workshop) {
                if (professional.workshop.title) {
                    professionalValues.push(professional.workshop.title);
                }
                if (professional.workshop.address) {
                    if (typeof (professional.workshop.address === "string")) {
                        professionalValues.push(professional.workshop.address);
                    }
                    else {
                        professionalValues.push(this.formatProvider.formatAddress(professional.workshop.address));
                    }
                }
            }
            if (professional.appointment) {
                if (professional.appointment.date && professional.appointment.time) {
                    professionalValues.push(this.translationProvider.getValueForKey('submit-incidence.workshop_display.appointment.arranged_on_date') + "\n              " + this.formatProvider.formatDate(professional.appointment.date) + "\n              " + this.translationProvider.getValueForKey('submit-incidence.workshop_display.appointment.arranged_at_time') + "\n              " + professional.appointment.time);
                }
            }
            summaryData.sections.push({
                title: 'submit_incidence.summary.choose_professional.title',
                rows: [{
                        key: 'submit_incidence.summary.choose_professional.recommended_professionals',
                        value: professionalValues
                    }]
            });
        }
        return summaryData;
    };
    SubmitIncidenceProvider.prototype.getFormData = function () {
        var _this = this;
        Observable.forkJoin([
            this.componentSettingsProvider.getProvincesSettings(false),
            this.componentSettingsProvider.getCitiesSettings(false),
            this.componentSettingsProvider.getRoadTypesSettings(false),
            this.componentSettingsProvider.getIdentificationDocuments(false),
            this.componentSettingsProvider.getVehicleBrands(false),
            this.componentSettingsProvider.getVehicleModels(false),
            this.componentSettingsProvider.getCountriesSettings(false),
            this.componentSettingsProvider.getColors(false)
        ]).subscribe(function (responses) {
            if (_this.componentSettingsProvider.areResponsesValid(responses)) {
                _this.provinces = _this.componentSettingsProvider.getResponseJSON(responses[0]).data;
                _this.cities = _this.componentSettingsProvider.getResponseJSON(responses[1]).data;
                _this.road_types = _this.componentSettingsProvider.getResponseJSON(responses[2]).data;
                _this.document_types = _this.componentSettingsProvider.getResponseJSON(responses[3]).data;
                _this.brands = _this.componentSettingsProvider.getResponseJSON(responses[4]).data;
                _this.models = _this.componentSettingsProvider.getResponseJSON(responses[5]).data;
                _this.countries = _this.componentSettingsProvider.getResponseJSON(responses[6]).data;
                _this.colors = _this.componentSettingsProvider.getResponseJSON(responses[7]).data;
            }
        });
    };
    SubmitIncidenceProvider.prototype.getVehicleBrand = function (id) {
        for (var i = 0; i < this.brands.length; i++) {
            if (this.brands[i].id == id) {
                return this.brands[i].title;
            }
        }
    };
    SubmitIncidenceProvider.prototype.getVehicleModel = function (id) {
        for (var i = 0; i < this.models.length; i++) {
            if (this.models[i].id == id) {
                return this.models[i].title;
            }
        }
    };
    SubmitIncidenceProvider.prototype.getVehicleColor = function (id) {
        for (var i = 0; i < this.colors.length; i++) {
            if (this.colors[i].id == id) {
                return this.colors[i].title;
            }
        }
    };
    SubmitIncidenceProvider.prototype.getDamagesData = function (source) {
        var _this = this;
        var damages = [];
        if (source && source.length) {
            source.forEach(function (damage) {
                if (_this.hasDamageValue(damage)) {
                    damages.push("" + _this.getDamageValue(damage));
                }
            });
        }
        return damages;
    };
    SubmitIncidenceProvider.prototype.getDamageValue = function (damage) {
        return damage ? (typeof damage === 'object' ? damage.title : damage) : undefined;
    };
    SubmitIncidenceProvider.prototype.hasDamageValue = function (damage) {
        var damageValue = this.getDamageValue(damage);
        return damageValue && damageValue.length > 0;
    };
    SubmitIncidenceProvider.prototype.getHowOwnVehicle = function (source) {
        var damages = [];
        if (source) {
            damages = Object.keys(source).map(function (key) { return source[key]; });
        }
        return damages;
    };
    SubmitIncidenceProvider.prototype.getHowOtherVehicles = function (source) {
        var damages = [];
        if (source && source.length) {
            source.forEach(function (damage) {
                var values = Object.keys(damage).map(function (key) { return damage[key]; });
                values.forEach(function (value) {
                    damages.push(value);
                });
            });
        }
        return damages;
    };
    SubmitIncidenceProvider.prototype.getDamagedSubareas = function (area) {
        var damagedSubareas = [];
        if (area && area.subareas && area.subareas.length > 0) {
            area.subareas.forEach(function (subarea) {
                damagedSubareas.push(subarea);
            });
        }
        return damagedSubareas;
    };
    SubmitIncidenceProvider.prototype.getOwnDocuments = function () {
        return this.ownDocuments;
    };
    SubmitIncidenceProvider.prototype.setOthersDocuments = function (file) {
        this.othersDocuments = file;
    };
    SubmitIncidenceProvider.prototype.getOthersDocuments = function () {
        return this.othersDocuments;
    };
    SubmitIncidenceProvider.prototype.setAttestationDocuments = function (file) {
        this.attestationDocument = file;
    };
    SubmitIncidenceProvider.prototype.getAttestationDocuments = function () {
        return this.attestationDocument;
    };
    SubmitIncidenceProvider.prototype.setAssociatedDocuments = function (associated_documents) {
        this.claim.car_claim_request.associated_docs = associated_documents;
    };
    SubmitIncidenceProvider.prototype.setReportLists = function (reportLists) {
        this.claim.car_claim_request.associated_docs.reportLists = reportLists;
    };
    SubmitIncidenceProvider.prototype.getReportLists = function () {
        return this.claim.car_claim_request.associated_docs.reportLists;
    };
    SubmitIncidenceProvider.prototype.getAssociatedDocuments = function () {
        return this.claim.car_claim_request.associated_docs;
    };
    SubmitIncidenceProvider.prototype.wereOtherVehiclesInvolved = function () {
        return this.withOtherVehiclesInvolved;
    };
    SubmitIncidenceProvider.prototype.setWithOtherVehiclesInvolved = function (withOtherVehiclesInvolved) {
        this.withOtherVehiclesInvolved = withOtherVehiclesInvolved;
        if (!withOtherVehiclesInvolved && this.claim && this.claim.car_claim_request.damages) {
            this.claim.car_claim_request.damages.others_damages = undefined;
        }
    };
    SubmitIncidenceProvider.prototype.setOwnAreas = function (areas) {
        this.ownVehicleAreas = areas;
    };
    SubmitIncidenceProvider.prototype.getOwnAreas = function () {
        return this.ownVehicleAreas;
    };
    SubmitIncidenceProvider.prototype.setOthersAreas = function (areas) {
        this.othersVehicleAreas = areas;
    };
    SubmitIncidenceProvider.prototype.getOthersAreas = function () {
        return this.othersVehicleAreas;
    };
    SubmitIncidenceProvider.prototype.advanceToStep = function (navController, stepId) {
        navController.push('SubmitIncidenceStepsPage', { stepId: stepId + 1 }, { animate: false });
    };
    SubmitIncidenceProvider.prototype.advanceToResultPage = function (navController) {
        navController.push('SubmitIncidenceConfirmationStepPage', { forceFinish: false }, { animate: false });
    };
    SubmitIncidenceProvider.prototype.onStepSelected = function (step, stepsCtrl) {
        var nextStep = 1;
        if (step > 1) {
            if (_.isEqual(step, 2) && this.insuranceSelectedData) {
                nextStep = step;
            }
            else if (_.isEqual(step, 3) && this.insuranceSelectedData && this.sinister) {
                nextStep = step;
            }
            else if (_.isEqual(step, 4) && this.insuranceSelectedData && this.sinister && this.howWhenWhereSelectedData) {
                nextStep = step;
            }
            else if (_.isEqual(step, 5) && this.insuranceSelectedData && this.sinister && this.howWhenWhereSelectedData) {
                if (this.ownDamagesSelected) {
                    nextStep = step;
                }
                else {
                    this.professionalSelectedData = undefined;
                    nextStep = step + 1;
                }
            }
            else if (_.isEqual(step, 6) && this.insuranceSelectedData && this.sinister && this.howWhenWhereSelectedData) {
                if (this.ownDamagesSelected) {
                    if (this.professionalSelectedData) {
                        nextStep = step;
                    }
                    else {
                        nextStep = step - 1;
                    }
                }
                else {
                    this.professionalSelectedData = null;
                    nextStep = step;
                }
            }
            else if (_.isEqual(step, 7)) {
                nextStep = step;
                this.incidenceSubmitted = true;
            }
        }
        return this.incidenceSubmitted ? 'SubmitIncidenceConfirmationStepPage' : 'SubmitIncidenceStepsPage';
    };
    SubmitIncidenceProvider.prototype.getLastAllowedPage = function () {
        if (!this.insuranceSelectedData) {
            return 1;
        }
        else {
            if (!this.sinister) {
                return 2;
            }
            else {
                if (!this.howWhenWhereSelectedData) {
                    return 3;
                }
                else {
                    if (this.ownDamagesSelected) {
                        if (!this.professionalSelectedData && !this.professionalOwnSelectedData) {
                            return 5;
                        }
                        else {
                            return 7;
                        }
                    }
                    else {
                        return 7;
                    }
                }
            }
        }
    };
    SubmitIncidenceProvider.prototype.isProviderReset = function () {
        return !this.insuranceSelectedData;
    };
    SubmitIncidenceProvider.prototype.setCoverageNotIncluded = function (input) {
        this.coverageNotIncluded = input;
    };
    SubmitIncidenceProvider.prototype.getCoveragesNotIncluded = function () {
        return this.coverageNotIncluded;
    };
    SubmitIncidenceProvider.decorators = [
        { type: Injectable },
    ];
    SubmitIncidenceProvider.ctorParameters = function () { return [
        { type: Injector, },
        { type: ComponentSettingsProvider, },
    ]; };
    return SubmitIncidenceProvider;
}(AbstractFlowProvider));
export { SubmitIncidenceProvider };
//# sourceMappingURL=submit-incidence.js.map