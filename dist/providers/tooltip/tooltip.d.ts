import { PopoverController } from 'ionic-angular';
import { PlatformProvider } from '../platform/platform';
import { TooltipData } from '../../models/tooltipData';
export declare class TooltipProvider {
    private popoverCtrl;
    private platformProvider;
    private popoverTooltip;
    private blockerSelector;
    constructor(popoverCtrl: PopoverController, platformProvider: PlatformProvider);
    presentPopoverTooltip(event: UIEvent, tooltipData: TooltipData, optionsAdded?: string): void;
    dismissPopoverTooltip(event: UIEvent): void;
}
