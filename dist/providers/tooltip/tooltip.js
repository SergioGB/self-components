import { Injectable } from '@angular/core';
import { PopoverController } from 'ionic-angular';
import * as jQuery from 'jquery';
import { PlatformProvider } from '../platform/platform';
import { PopoverTooltipComponent } from '../../components/popover-tooltip/popover-tooltip';
var TooltipProvider = (function () {
    function TooltipProvider(popoverCtrl, platformProvider) {
        var _this = this;
        this.popoverCtrl = popoverCtrl;
        this.platformProvider = platformProvider;
        this.blockerSelector = '.click-block-active';
        window.addEventListener('orientationchange', function (event) {
            if (_this.popoverTooltip) {
                _this.popoverTooltip.dismiss();
            }
        });
    }
    TooltipProvider.prototype.presentPopoverTooltip = function (event, tooltipData, optionsAdded) {
        var _this = this;
        event.stopPropagation();
        if (this.popoverTooltip === undefined) {
            var options = {
                cssClass: 'popover-tooltip',
                showBackdrop: !this.platformProvider.onDesktop()
            };
            if (this.platformProvider.onDesktop()) {
                options.showBackdrop = false;
                options.cssClass += ' desktop';
            }
            if (optionsAdded) {
                options.cssClass += optionsAdded;
            }
            this.popoverTooltip = this.popoverCtrl.create(PopoverTooltipComponent, { tooltipData: tooltipData }, options);
            this.popoverTooltip.onDidDismiss(function () {
                _this.popoverTooltip = undefined;
            });
            this.popoverTooltip.present({ ev: event });
            if (this.platformProvider.onDesktop()) {
                jQuery(this.blockerSelector).removeClass('click-block-active');
            }
        }
    };
    TooltipProvider.prototype.dismissPopoverTooltip = function (event) {
        event.stopPropagation();
        if (this.platformProvider.onDesktop()) {
            if (this.popoverTooltip !== undefined && !jQuery(this.blockerSelector).length) {
                this.popoverTooltip.dismiss();
            }
        }
    };
    TooltipProvider.decorators = [
        { type: Injectable },
    ];
    TooltipProvider.ctorParameters = function () { return [
        { type: PopoverController, },
        { type: PlatformProvider, },
    ]; };
    return TooltipProvider;
}());
export { TooltipProvider };
//# sourceMappingURL=tooltip.js.map