import { ToastController, LoadingController } from "ionic-angular";
import { File } from '@ionic-native/file';
import { FileOpener } from "@ionic-native/file-opener";
import { PlatformProvider } from '../platform/platform';
import { MapfreFile } from '../../models/mapfreFile';
import { TranslationProvider } from '../translation/translation';
import { FileUtils } from '../utils/fileUtils';
export declare class TransferProvider {
    private fileUtils;
    private platform;
    private fileManager;
    private translationProvider;
    private fileOpener;
    private toastCtrl;
    private loadingCtrl;
    private toast;
    private loading;
    private reader;
    constructor(fileUtils: FileUtils, platform: PlatformProvider, fileManager: File, translationProvider: TranslationProvider, fileOpener: FileOpener, toastCtrl: ToastController, loadingCtrl: LoadingController);
    uploadFile(file: any, outputArray: any[], allowPdf: boolean, allowImages: boolean, allowVideos: boolean, callback?: Function): void;
    downloadFile(mapfreFile: MapfreFile): void;
    presentUploadingMessage(): void;
    presentDownloadingMessage(): void;
    dismissLoadMessage(): void;
}
