import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from "ionic-angular";
import { File } from '@ionic-native/file';
import { FileOpener } from "@ionic-native/file-opener";
import { saveAs as importedSaveAs } from "file-saver";
import { PlatformProvider } from '../platform/platform';
import { TranslationProvider } from '../translation/translation';
import { FileUtils } from '../utils/fileUtils';
var TransferProvider = (function () {
    function TransferProvider(fileUtils, platform, fileManager, translationProvider, fileOpener, toastCtrl, loadingCtrl) {
        this.fileUtils = fileUtils;
        this.platform = platform;
        this.fileManager = fileManager;
        this.translationProvider = translationProvider;
        this.fileOpener = fileOpener;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.reader = new FileReader();
    }
    TransferProvider.prototype.uploadFile = function (file, outputArray, allowPdf, allowImages, allowVideos, callback) {
        var _this = this;
        if (file) {
            this.presentUploadingMessage();
            this.fileUtils.getBase64(file).then(function (data) {
                var type;
                var content;
                if (allowPdf && data.includes('data:application/pdf')) {
                    type = 'pdf';
                }
                else if (allowImages && data.includes('data:image')) {
                    type = 'image';
                }
                else if (allowVideos && data.includes('data:video')) {
                    type = 'video';
                }
                else {
                    var error = _this.translationProvider.getValueForKey('transfer.documentation.message1');
                    if (allowPdf) {
                        error = error + " PDF";
                        if (!allowImages && !allowVideos) {
                            error = error + ".";
                        }
                    }
                    if (allowImages) {
                        if (allowPdf) {
                            if (allowVideos) {
                                error = error + ", " + _this.translationProvider.getValueForKey('transfer.documentation.message2');
                            }
                            else {
                                error = error + " " + _this.translationProvider.getValueForKey('transfer.documentation.message3');
                            }
                        }
                        else {
                            error = error + " " + _this.translationProvider.getValueForKey('transfer.documentation.message2');
                            if (!allowVideos) {
                                error = error + ".";
                            }
                        }
                    }
                    if (allowVideos) {
                        if (allowPdf || allowImages) {
                            error = error + " " + _this.translationProvider.getValueForKey('transfer.documentation.message4');
                        }
                        error = error + " " + _this.translationProvider.getValueForKey('transfer.documentation.message5');
                    }
                    throw error;
                }
                content = data.substring(data.lastIndexOf('base64,') + 7, data.length);
                if (allowImages) {
                    _this.reader.onload = function (e) {
                        var outputFile = {
                            title: file.name,
                            content: content,
                            type: type,
                            href: e.target.result,
                            mime_type: file.type,
                            size: FileUtils.formatBytes(file.size)
                        };
                        if (outputArray) {
                            outputArray.push(outputFile);
                        }
                        if (callback && typeof callback === 'function') {
                            callback(outputFile);
                        }
                        _this.dismissLoadMessage();
                    };
                    _this.reader.readAsDataURL(file);
                }
                else {
                    if (outputArray) {
                        outputArray.push({
                            title: file.name,
                            content: content,
                            type: type,
                            size: FileUtils.formatBytes(file.size)
                        });
                    }
                    _this.dismissLoadMessage();
                }
            }).catch(function (error) {
                _this.toast = _this.toastCtrl.create({
                    message: error.includes(_this.translationProvider.getValueForKey('transfer.documentation.only')) ? error :
                        _this.translationProvider.getValueForKey('transfer.documentation.message6'),
                    duration: 1500,
                    position: 'bottom'
                });
                _this.toast.present();
                _this.dismissLoadMessage();
            });
        }
    };
    TransferProvider.prototype.downloadFile = function (mapfreFile) {
        var _this = this;
        try {
            var blob = this.fileUtils.b64toBlob(mapfreFile.content);
            this.toast = this.toastCtrl.create({
                message: '',
                duration: 1500,
                position: 'bottom'
            });
            var filename_1 = mapfreFile.title || mapfreFile.name;
            if (this.platform.onApp()) {
                var path = void 0;
                if (this.platform.onAndroid()) {
                    path = this.fileManager.externalApplicationStorageDirectory;
                }
                else if (this.platform.onIos()) {
                    path = this.fileManager.documentsDirectory;
                }
                this.fileManager.writeFile(path, filename_1, blob, { replace: true })
                    .then(function (data) {
                    _this.fileOpener.open(data.nativeURL.substr(0, data.nativeURL.lastIndexOf('/')) + "/" + filename_1, 'application/pdf')
                        .then(function (data) {
                        _this.dismissLoadMessage();
                    }).catch(function (err) {
                        _this.dismissLoadMessage();
                        _this.toast.setMessage(_this.translationProvider.getValueForKey('transfer.documentation.message7'));
                        _this.toast.present();
                    });
                }).catch(function (err) {
                    _this.dismissLoadMessage();
                    _this.toast.setMessage(_this.translationProvider.getValueForKey('transfer.documentation.message8'));
                    _this.toast.present();
                });
            }
            else {
                if (this.platform.onIos()) {
                    window.location.href = "data:application/pdf;base64," + mapfreFile.content;
                    this.dismissLoadMessage();
                }
                else {
                    importedSaveAs(blob, filename_1);
                    this.dismissLoadMessage();
                }
            }
        }
        catch (err) {
            this.toast = this.toastCtrl.create({
                message: this.translationProvider.getValueForKey('transfer.documentation.message9'),
                duration: 1500,
                position: 'bottom'
            });
            this.toast.present();
        }
    };
    TransferProvider.prototype.presentUploadingMessage = function () {
        this.dismissLoadMessage();
        this.loading = this.loadingCtrl.create({
            content: this.translationProvider.getValueForKey('transfer.documentation.message10')
        });
        this.loading.present();
    };
    TransferProvider.prototype.presentDownloadingMessage = function () {
        this.dismissLoadMessage();
        this.loading = this.loadingCtrl.create({
            content: this.translationProvider.getValueForKey('transfer.documentation.message11')
        });
        this.loading.present();
    };
    TransferProvider.prototype.dismissLoadMessage = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = undefined;
        }
    };
    TransferProvider.decorators = [
        { type: Injectable },
    ];
    TransferProvider.ctorParameters = function () { return [
        { type: FileUtils, },
        { type: PlatformProvider, },
        { type: File, },
        { type: TranslationProvider, },
        { type: FileOpener, },
        { type: ToastController, },
        { type: LoadingController, },
    ]; };
    return TransferProvider;
}());
export { TransferProvider };
//# sourceMappingURL=transfer.js.map