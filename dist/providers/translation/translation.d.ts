export declare class TranslationProvider {
    translations: any;
    constructor();
    setTranslations(translations: any): void;
    bind(target: any): void;
    get(translations: any, key: string): string;
    getValueForKey(key: string): string;
    getMonthTagByDate(date: Date): string;
}
