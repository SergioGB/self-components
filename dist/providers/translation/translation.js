import { Injectable } from '@angular/core';
var TranslationProvider = (function () {
    function TranslationProvider() {
        this.translations = {};
    }
    TranslationProvider.prototype.setTranslations = function (translations) {
        this.translations = translations;
    };
    TranslationProvider.prototype.bind = function (target) {
        target.translations = this.translations;
        var newTranslationProvider = this;
        target.translate = function (key) {
            return newTranslationProvider.get(target.translations, key);
        };
        target.translationsReady = true;
    };
    TranslationProvider.prototype.get = function (translations, key) {
        return translations ? (translations[key] || key) : key;
    };
    TranslationProvider.prototype.getValueForKey = function (key) {
        return this.get(this.translations, key);
    };
    TranslationProvider.prototype.getMonthTagByDate = function (date) {
        return this.getValueForKey("generic.dateMonth." + date.getMonth());
    };
    TranslationProvider.decorators = [
        { type: Injectable },
    ];
    TranslationProvider.ctorParameters = function () { return []; };
    return TranslationProvider;
}());
export { TranslationProvider };
//# sourceMappingURL=translation.js.map