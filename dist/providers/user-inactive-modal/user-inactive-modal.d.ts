export declare class UserInactiveModalProvider {
    private active;
    constructor();
    isActive(): boolean;
    setActive(active: boolean): void;
}
