import { Injectable } from '@angular/core';
var UserInactiveModalProvider = (function () {
    function UserInactiveModalProvider() {
        this.active = false;
    }
    UserInactiveModalProvider.prototype.isActive = function () {
        return this.active;
    };
    UserInactiveModalProvider.prototype.setActive = function (active) {
        this.active = active;
    };
    UserInactiveModalProvider.decorators = [
        { type: Injectable },
    ];
    UserInactiveModalProvider.ctorParameters = function () { return []; };
    return UserInactiveModalProvider;
}());
export { UserInactiveModalProvider };
//# sourceMappingURL=user-inactive-modal.js.map