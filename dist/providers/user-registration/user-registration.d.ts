import { FlowRequest } from './../../models/flow-request';
import { Injector } from '@angular/core';
import { SummaryData } from '../../models/summary';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
export declare class UserRegistrationProvider extends AbstractFlowProvider {
    private injector;
    private userData;
    private userPictureData;
    private userContactData;
    private userSecurityData;
    constructor(injector: Injector);
    resetFlowData(): void;
    protected initializeFlowData(): void;
    getUserData(): any;
    setUserData(userData: any): void;
    getUserPictureData(): any;
    setUserPictureData(userPictureData: any): void;
    getUserContactData(): any;
    setUserContactData(userContactData: any): void;
    getUserSecurityData(): any;
    setUserSecurityData(userSecurityData: any): void;
    getSummaryData(): SummaryData;
    getRequestJson(formattedRequest: FlowRequest): FlowRequest;
    updateRequest(flowRequest: FlowRequest, callback: Function, mocked?: boolean): void;
}
