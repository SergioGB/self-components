var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { FullUserRequest } from '../../models/login/fullUserRequest';
import { SummaryData } from '../../models/summary';
import { AbstractFlowProvider } from '../_abstract/abstract-flow';
var UserRegistrationProvider = (function (_super) {
    __extends(UserRegistrationProvider, _super);
    function UserRegistrationProvider(injector) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.initializeFlowData();
        return _this;
    }
    UserRegistrationProvider.prototype.resetFlowData = function () {
        this.queryUrls = [];
        this.userData = undefined;
        this.userPictureData = undefined;
        this.userContactData = undefined;
        this.userSecurityData = undefined;
    };
    UserRegistrationProvider.prototype.initializeFlowData = function () {
        this.stepsPageName = 'CompleteUserRegistrationStepsPage';
        this.resultPageName = 'LoginPage';
        this.resetFlowData();
    };
    UserRegistrationProvider.prototype.getUserData = function () {
        return this.userData;
    };
    UserRegistrationProvider.prototype.setUserData = function (userData) {
        this.userData = userData;
    };
    UserRegistrationProvider.prototype.getUserPictureData = function () {
        return this.userPictureData;
    };
    UserRegistrationProvider.prototype.setUserPictureData = function (userPictureData) {
        this.userPictureData = userPictureData;
    };
    UserRegistrationProvider.prototype.getUserContactData = function () {
        return this.userContactData;
    };
    UserRegistrationProvider.prototype.setUserContactData = function (userContactData) {
        this.userContactData = userContactData;
    };
    UserRegistrationProvider.prototype.getUserSecurityData = function () {
        return this.userSecurityData;
    };
    UserRegistrationProvider.prototype.setUserSecurityData = function (userSecurityData) {
        this.userSecurityData = userSecurityData;
    };
    UserRegistrationProvider.prototype.getSummaryData = function () {
        return new SummaryData();
    };
    UserRegistrationProvider.prototype.getRequestJson = function (formattedRequest) {
        var fullRequest = new FullUserRequest();
        if (this.userPictureData) {
            if (this.userPictureData.picture) {
                fullRequest.avatar = this.userPictureData.picture.replace(/^data:image\/(png|jpg|jpeg);base64,/, '');
            }
        }
        if (this.userData) {
            fullRequest.personal_information = {
                name: this.userData.name,
                surname1: this.userData.surname1,
                surname2: this.userData.surname2,
                identification_document_id: this.userData.identification_document ? this.userData.identification_document.id : undefined,
                identification_document_number: this.userData.identification_number,
                address: {
                    road_type_id: this.userData.road_type ? this.userData.road_type.id : undefined,
                    road_name: this.userData.road_name,
                    road_number: this.userData.road_number,
                    zip_code: this.userData.postal_code,
                    city_id: this.userData.city ? this.userData.city.id : undefined,
                    province_id: this.userData.province ? this.userData.province.id : undefined,
                    country_id: this.userData.country ? this.userData.country.id : undefined
                }
            };
        }
        if (this.userContactData) {
            fullRequest.contact_information = {
                phone: this.userContactData.phone_number,
                phone_daytime_slot_id: this.userContactData.time_range ? this.userContactData.time_range.id : undefined,
                mail: this.userContactData.email,
                mail_daytime_slot_id: this.userContactData.time_range ? this.userContactData.time_range.id : undefined,
                preference_id: this.userContactData.contact_platform ? this.userContactData.contact_platform.id : undefined
            };
        }
        if (this.userSecurityData) {
            fullRequest.security_information = {
                password: this.userSecurityData.password,
                security_questions: [{
                        question_id: this.userSecurityData.first_security_question ? this.userSecurityData.first_security_question.id : undefined,
                        answer: this.userSecurityData.first_security_question_answer
                    }, {
                        question_id: this.userSecurityData.second_security_question ? this.userSecurityData.second_security_question.id : undefined,
                        answer: this.userSecurityData.second_security_question_answer
                    }]
            };
        }
        formattedRequest.step_data = fullRequest;
        return formattedRequest;
    };
    UserRegistrationProvider.prototype.updateRequest = function (flowRequest, callback, mocked) {
        var _this = this;
        if (mocked) {
            callback({
                response: {}
            });
        }
        else {
            if (this.requestId) {
                this.userProvider.putFullRegistrationRequest(this.requestId, this.getRequestJson(flowRequest)).subscribe(function (response) {
                    _this.onRequestPut(response, callback);
                });
            }
            else {
                this.userProvider.postFullRegistrationRequest(this.getRequestJson(flowRequest)).subscribe(function (response) {
                    _this.onRequestPost(response, callback);
                });
            }
        }
    };
    UserRegistrationProvider.decorators = [
        { type: Injectable },
    ];
    UserRegistrationProvider.ctorParameters = function () { return [
        { type: Injector, },
    ]; };
    return UserRegistrationProvider;
}(AbstractFlowProvider));
export { UserRegistrationProvider };
//# sourceMappingURL=user-registration.js.map