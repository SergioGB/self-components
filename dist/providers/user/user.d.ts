import { Injector } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ApiProvider } from '../api/api';
import { DateProvider } from '../date/date';
import { MenuProvider } from '../menu/menu';
import { MapfreResponse, MapfreResponseV1 } from '../../models/mapfreResponse';
import { User } from '../../models/user';
import { UserRequest } from '../../models/login/userRequest';
import { FullUserRequest } from '../../models/login/fullUserRequest';
import { LoginRequest } from '../../models/login/loginRequest';
import { SocialLoginRequest } from '../../models/login/socialLoginRequest';
import { Validation } from '../../models/validation';
import { Message } from '../../models/message';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
export declare class UserProvider extends ApiProvider {
    private componentSettingsProvider;
    protected dateProvider: DateProvider;
    private menuProvider;
    protected injector: Injector;
    static PotentailTypeID: string;
    static ClientTypeID: string;
    username: string;
    userDataOpen: User;
    userTypeOpen: string;
    openUserFlow: boolean;
    constructor(componentSettingsProvider: ComponentSettingsProvider, dateProvider: DateProvider, menuProvider: MenuProvider, injector: Injector);
    setPermissions(permissions: any): void;
    getPermissions(): any;
    sendLoginRequest(mocked: boolean, loginRequest: LoginRequest): Observable<MapfreResponseV1>;
    sendToolLoginRequest(mocked: boolean, loginRequest: LoginRequest): Observable<MapfreResponse>;
    sendOpenLoginRequest(mocked: boolean, loginRequest: any): Observable<MapfreResponseV1>;
    setUserDataOpen(userData: User): void;
    getUserDataOpen(): User;
    setOpenUserFlow(value: boolean): void;
    getOpenUserFlow(): boolean;
    isTemporarilyLoggedIn(): boolean;
    deleteUserDataOpen(): void;
    sendSocialLoginRequest(socialLoginRequest: SocialLoginRequest): Promise<User>;
    queryUserData(username: string): Observable<Response>;
    getNewUserToken(mocked?: boolean): Observable<Response | any>;
    getLogout(): Observable<Response>;
    getAccountMethod(): Observable<Response>;
    sendRegistrationRequest(userRequest: UserRequest): Observable<Response>;
    postFullRegistrationRequest(fullUserRequest: any): Observable<Response>;
    putFullRegistrationRequest(requestId: any, fullUserRequest: any): Observable<Response>;
    sendUpdateRequest(userRequest: UserRequest, clientId: string): Observable<Response>;
    sendUpdateUser(updateData: any, clientId: string): Observable<Response>;
    getUserCompanyRelationships(): Observable<Response>;
    getUserGrantedPermissions(): Observable<Response>;
    sendUpdateGrantedPermissions(updateData: any, clientId: string): Observable<Response>;
    getUserRequest(user: User): UserRequest;
    getFullUserRequest(userRequest: UserRequest): FullUserRequest;
    sendValidationRequest(validationRequest: Validation): Observable<Response>;
    sendMailSMSRequest(messageRequest: Message): Observable<Response>;
    sendPasswordChange(userData: any, client_id: string): Observable<Response>;
    sendPinChange(userData: any, client_id: string): Observable<Response>;
    sendOpenLogin(userData: any): Observable<Response>;
    setRememberedUser(userToRemember: string): void;
    setUsername(username: string): void;
    getUsername(): string;
    getRememberedUser(): string;
    clearRememberedUser(): void;
    getMapfreRelationship(): Observable<Response>;
    isPotentialClient(): boolean;
}
