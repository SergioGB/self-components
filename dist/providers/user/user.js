var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ApiProvider } from '../api/api';
import { DateProvider } from '../date/date';
import { MenuProvider } from '../menu/menu';
import { ConfigProvider } from '../../models/self-component.config';
import { ComponentSettingsProvider } from '../component-settings/component-settings';
var UserProvider = (function (_super) {
    __extends(UserProvider, _super);
    function UserProvider(componentSettingsProvider, dateProvider, menuProvider, injector) {
        var _this = _super.call(this, injector) || this;
        _this.componentSettingsProvider = componentSettingsProvider;
        _this.dateProvider = dateProvider;
        _this.menuProvider = menuProvider;
        _this.injector = injector;
        return _this;
    }
    UserProvider.prototype.setPermissions = function (permissions) {
        var permissionsString = JSON.stringify(permissions);
        this.cookieService.put(ConfigProvider.config.clientPermissionsCookieName, permissionsString);
    };
    UserProvider.prototype.getPermissions = function () {
        return JSON.parse(this.cookieService.get(ConfigProvider.config.clientPermissionsCookieName));
    };
    UserProvider.prototype.sendLoginRequest = function (mocked, loginRequest) {
        var _this = this;
        return this.componentSettingsProvider.sendLoginRequest(mocked, loginRequest).map(function (response) {
            var success = _this.isMapfreResponseValidV1(_this.getResponseJSONV1(response));
            if (success) {
                var userData = response.json().payload.data.client_info;
                _this.setClientId(userData.id);
                _this.setUserData(userData);
                _this.setUserType(response.json().payload.data.client_type);
                if (loginRequest.rememberMe) {
                    _this.setRememberedUser(loginRequest.user_pass_login.user);
                }
                else {
                    _this.clearRememberedUser();
                }
            }
            return response.json();
        });
    };
    UserProvider.prototype.sendToolLoginRequest = function (mocked, loginRequest) {
        var _this = this;
        return this.componentSettingsProvider.sendLoginRequest(mocked, loginRequest).map(function (response) {
            var success = _this.isMapfreResponseValid(_this.getResponseJSON(response));
            if (success) {
                var userData = response.json().data.client_info;
                _this.setClientId(userData.id);
                _this.setUserData(userData);
                if (loginRequest.rememberMe) {
                    _this.setRememberedUser(loginRequest.user_pass_login.user);
                }
                else {
                    _this.clearRememberedUser();
                }
            }
            return response.json();
        });
    };
    UserProvider.prototype.sendOpenLoginRequest = function (mocked, loginRequest) {
        var _this = this;
        return this.componentSettingsProvider.sendLoginRequest(mocked, loginRequest).map(function (response) {
            var success = _this.isMapfreResponseValidV1(_this.getResponseJSONV1(response));
            if (success) {
                var userData = response.json().payload.data.client_info;
                _this.setUserDataOpen(userData);
                _this.setUserType(response.json().payload.data.client_type);
                _this.setOpenUserFlow(true);
                _this.identificationProvider.setApiKey(response.json().security_tokens.token).then(function () {
                });
            }
            return response.json();
        });
    };
    UserProvider.prototype.setUserDataOpen = function (userData) {
        this.userDataOpen = userData;
    };
    UserProvider.prototype.getUserDataOpen = function () {
        return this.userDataOpen;
    };
    UserProvider.prototype.setOpenUserFlow = function (value) {
        this.openUserFlow = value;
    };
    UserProvider.prototype.getOpenUserFlow = function () {
        return this.openUserFlow;
    };
    UserProvider.prototype.isTemporarilyLoggedIn = function () {
        return (this.userDataOpen !== null && this.userDataOpen !== undefined);
    };
    UserProvider.prototype.deleteUserDataOpen = function () {
        this.userDataOpen = null;
    };
    UserProvider.prototype.sendSocialLoginRequest = function (socialLoginRequest) {
        var _this = this;
        var mockUserMapping = {
            facebookUserId: '107404280101167',
            twitterUserId: '981866215164600300',
            googleplusUserId: '109281823313075103864'
        };
        return new Promise(function (resolve, reject) {
            if (socialLoginRequest.facebookUserId && socialLoginRequest.facebookUserId === mockUserMapping.facebookUserId ||
                socialLoginRequest.twitterUserId && socialLoginRequest.twitterUserId === mockUserMapping.twitterUserId ||
                socialLoginRequest.googleplusUserId && socialLoginRequest.googleplusUserId === mockUserMapping.googleplusUserId) {
                _this.queryUserData('user20171031').subscribe(function (response) {
                    var mapfreResponse = _this.getResponseJSON(response);
                    if (_this.isMapfreResponseValid(mapfreResponse)) {
                        var user = mapfreResponse.data[0];
                        _this.setUserData(user);
                        resolve(user);
                    }
                    else {
                        reject(mapfreResponse.response);
                    }
                });
            }
            else {
                reject({
                    message: 'No existe ningún usuario enlazado a esa cuenta.',
                    code: 1
                });
            }
        });
    };
    UserProvider.prototype.queryUserData = function (username) {
        return this.getObservableForSuffix(false, true, 'clients', true, { username: username });
    };
    UserProvider.prototype.getNewUserToken = function (mocked) {
        var token = this.identificationProvider.getApiKey();
        if (mocked) {
            return Observable.of({ token: token });
        }
        return this.getObservableForSuffix(false, true, 'authentications/newtoken', true, { token: token });
    };
    UserProvider.prototype.getLogout = function () {
        var _this = this;
        return this.postObservableForSuffix(false, true, 'authentications/logout', true).map(function (response) {
            _this.deleteClientId();
            _this.deleteUserData();
            _this.setUsername(null);
            _this.menuProvider.menuOptionsEmitter.emit([]);
            _this.storage.set(ConfigProvider.config.securityPinId, null);
            _this.storage.set(ConfigProvider.config.fingerprintEnabledId, null);
            _this.storage.set(ConfigProvider.config.apiKeyID, null);
            return response;
        });
    };
    UserProvider.prototype.getAccountMethod = function () {
        return this.getObservableForSuffix(false, false, 'create_account_mode', false);
    };
    UserProvider.prototype.sendRegistrationRequest = function (userRequest) {
        return this.postObservableForSuffix(false, true, 'clients', false, null, this.getFullUserRequest(userRequest));
    };
    UserProvider.prototype.postFullRegistrationRequest = function (fullUserRequest) {
        return this.postObservableForSuffix(false, false, 'clients/', true, null, fullUserRequest);
    };
    UserProvider.prototype.putFullRegistrationRequest = function (requestId, fullUserRequest) {
        return this.putObservableForSuffix(false, false, "client/" + requestId + "/registration", true, null, fullUserRequest);
    };
    UserProvider.prototype.sendUpdateRequest = function (userRequest, clientId) {
        return this.putObservableForSuffix(false, false, "client/" + clientId + "/registration", false, {}, this.getFullUserRequest(userRequest));
    };
    UserProvider.prototype.sendUpdateUser = function (updateData, clientId) {
        return this.putObservableForSuffix(false, true, "client/" + clientId, false, {}, updateData);
    };
    UserProvider.prototype.getUserCompanyRelationships = function () {
        return this.getObservableForSuffix(false, true, "client/" + this.getClientId() + "/company_relationship", true);
    };
    UserProvider.prototype.getUserGrantedPermissions = function () {
        return this.getObservableForSuffix(false, true, "client/" + this.getClientId() + "/granted_permissions", true);
    };
    UserProvider.prototype.sendUpdateGrantedPermissions = function (updateData, clientId) {
        return this.putObservableForSuffix(false, true, "client/" + clientId + "/granted_permissions", false, {}, updateData);
    };
    UserProvider.prototype.getUserRequest = function (user) {
        return {
            name: user.name,
            surname1: user.surname1,
            surname2: user.surname2,
            identification_document: { id: user.identification_document_id },
            identification_number: user.identification_number,
            mobile_phone: user.mobile_phone,
            mail: user.mail,
            address: user.address
        };
    };
    UserProvider.prototype.getFullUserRequest = function (userRequest) {
        var fullRequest = {};
        if (userRequest) {
            fullRequest.personal_information = {
                name: userRequest.name,
                surname1: userRequest.surname1,
                surname2: userRequest.surname2,
                identification_document_id: userRequest.identification_document ? userRequest.identification_document.id : undefined,
                identification_document_number: userRequest.identification_number
            };
            fullRequest.contact_information = {
                phone: userRequest.mobile_phone,
                mail: userRequest.mail
            };
            fullRequest.security_information = {
                password: userRequest.password,
                security_questions: [{
                        question_id: userRequest.first_security_question ? userRequest.first_security_question.id : undefined,
                        answer: userRequest.first_security_question_answer
                    }, {
                        question_id: userRequest.second_security_question ? userRequest.second_security_question.id : undefined,
                        answer: userRequest.second_security_question_answer
                    }]
            };
        }
        return fullRequest;
    };
    UserProvider.prototype.sendValidationRequest = function (validationRequest) {
        validationRequest.client_id = this.getClientId();
        return this.postObservableForSuffix(false, true, "authentications/recovery", true, {}, validationRequest);
    };
    UserProvider.prototype.sendMailSMSRequest = function (messageRequest) {
        return this.postObservableForSuffix(false, true, "client/" + this.getClientId() + "/messages", true, {}, messageRequest);
    };
    UserProvider.prototype.sendPasswordChange = function (userData, client_id) {
        return this.putObservableForSuffix(false, true, "client/" + client_id, true, {}, userData);
    };
    UserProvider.prototype.sendPinChange = function (userData, client_id) {
        return this.putObservableForSuffix(false, true, "client/" + client_id, true, {}, userData);
    };
    UserProvider.prototype.sendOpenLogin = function (userData) {
        return this.postObservableForSuffix(false, true, "authentications/request_access_pin", true, {}, userData);
    };
    UserProvider.prototype.setRememberedUser = function (userToRemember) {
        this.cookieService.put(ConfigProvider.config.rememberedUserCookieName, "" + userToRemember, {
            expires: this.dateProvider.getDatePlusDays(new Date(), ConfigProvider.config.rememberedUserCookieExpirationInDays)
        });
    };
    UserProvider.prototype.setUsername = function (username) {
        this.username = username;
    };
    UserProvider.prototype.getUsername = function () {
        return this.username;
    };
    UserProvider.prototype.getRememberedUser = function () {
        return this.cookieService.get(ConfigProvider.config.rememberedUserCookieName);
    };
    UserProvider.prototype.clearRememberedUser = function () {
        this.cookieService.remove(ConfigProvider.config.rememberedUserCookieName);
    };
    UserProvider.prototype.getMapfreRelationship = function () {
        return this.getObservableForSuffix(false, true, "client/" + this.getClientId() + "/company_relationship", true);
    };
    UserProvider.prototype.isPotentialClient = function () {
        return this.getLoggedUserType() === UserProvider.PotentailTypeID;
    };
    UserProvider.PotentailTypeID = "potential";
    UserProvider.ClientTypeID = "client";
    UserProvider.decorators = [
        { type: Injectable },
    ];
    UserProvider.ctorParameters = function () { return [
        { type: ComponentSettingsProvider, },
        { type: DateProvider, },
        { type: MenuProvider, },
        { type: Injector, },
    ]; };
    return UserProvider;
}(ApiProvider));
export { UserProvider };
//# sourceMappingURL=user.js.map