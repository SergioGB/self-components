import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { CookieService } from 'ngx-cookie';
import { ComponentSettingsProvider } from "../component-settings/component-settings";
import { TranslationProvider } from '../translation/translation';
import { PlatformProvider } from '../platform/platform';
import { ScrollEvent, ModalController } from 'ionic-angular';
export declare class AppGlobalService {
    private http;
    private componentSettingsProvider;
    private storage;
    private cookieService;
    private translationProvider;
    private modalController;
    private platformProvider;
    lastFooterPosition: number;
    constructor(http: Http, componentSettingsProvider: ComponentSettingsProvider, storage: Storage, cookieService: CookieService, translationProvider: TranslationProvider, modalController: ModalController, platformProvider: PlatformProvider);
    setLanguage(language?: string): Promise<any>;
    scrollHandler(event: ScrollEvent): void;
    assistanceResolve(): void;
    assistanceCancelled(): void;
}
