import { Http } from '@angular/http';
import { Injectable } from "@angular/core";
import { Storage } from '@ionic/storage';
import { CookieService } from 'ngx-cookie';
import { ComponentSettingsProvider } from "../component-settings/component-settings";
import { TranslationProvider } from '../translation/translation';
import { PlatformProvider } from '../platform/platform';
import { ModalController } from 'ionic-angular';
import { ProcessConfirmationModalComponent } from '../../components/_modals/process-confirmation-modal/process-confirmation-modal';
import { InformationModalComponent } from '../../components/_modals/information-modal/information-modal';
import { ConfigProvider } from '../../models/self-component.config';
var AppGlobalService = (function () {
    function AppGlobalService(http, componentSettingsProvider, storage, cookieService, translationProvider, modalController, platformProvider) {
        this.http = http;
        this.componentSettingsProvider = componentSettingsProvider;
        this.storage = storage;
        this.cookieService = cookieService;
        this.translationProvider = translationProvider;
        this.modalController = modalController;
        this.platformProvider = platformProvider;
        this.lastFooterPosition = 10000000;
    }
    AppGlobalService.prototype.setLanguage = function (language) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = {
                'Accept-Language': language ? language : 'es'
            };
            _this.http
                .get(_this.componentSettingsProvider.getQueryUrl(false, false, 'labels'), { headers: headers })
                .toPromise()
                .then(function (res) {
                var response = res.json();
                _this.storage.set('language', response);
                _this.cookieService.put(ConfigProvider.config.forcedLanguageKey, language);
                _this.translationProvider.setTranslations(response.data);
                _this.translationProvider.bind(_this);
                resolve();
            }).catch(function (err) { return window.console.log('Error: %s', err); });
        });
    };
    AppGlobalService.prototype.scrollHandler = function (event) {
        if (event && event.contentElement) {
            var parentElement = event.contentElement.parentNode;
            if (parentElement.querySelector("ion-fab") && parentElement.querySelector("mapfre-footer") && parentElement.querySelector(".links-tab")) {
                if (!this.platformProvider.onApp() && (this.platformProvider.onTablet() || this.platformProvider.onMobile())) {
                    var mapfreFooter = parentElement.querySelector("mapfre-footer").getBoundingClientRect();
                    var fabGroup = parentElement.querySelector("ion-fab").getBoundingClientRect();
                    var linksTab = parentElement.querySelector(".links-tab").getBoundingClientRect();
                    var difference = mapfreFooter.y - fabGroup.y;
                    var scrollingToTop = this.lastFooterPosition - mapfreFooter.y < 0 ? true : false;
                    this.lastFooterPosition = mapfreFooter.y;
                    if (scrollingToTop) {
                        if (difference > 60) {
                            if (mapfreFooter.y > linksTab.y && linksTab.y - fabGroup.y < 60) {
                                parentElement.querySelector('ion-fab').style.cssText = 'top: ""';
                            }
                            else {
                                var topValueNumber = mapfreFooter.y - 60;
                                var topValue = topValueNumber.toString().concat("px !important");
                                parentElement.querySelector('ion-fab').style.cssText = "top: " + topValue;
                            }
                        }
                        else {
                            parentElement.querySelector('ion-fab').style.cssText = 'top: ""';
                        }
                    }
                    else {
                        if (difference < 60) {
                            var topValueNumber = mapfreFooter.y - 60;
                            var topValue = topValueNumber.toString().concat("px !important");
                            parentElement.querySelector('ion-fab').style.cssText = "top: " + topValue;
                        }
                        else {
                            parentElement.querySelector('ion-fab').style.cssText = 'top: ""';
                        }
                    }
                }
            }
        }
    };
    AppGlobalService.prototype.assistanceResolve = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.componentSettingsProvider.isLoggedIn()) {
                _this.modalController.create(ProcessConfirmationModalComponent, {
                    icon: 'mapfre-check',
                    title: _this.translationProvider.getValueForKey('app-global.pending_task.assistance.confirm.title'),
                    description: _this.translationProvider.getValueForKey('app-global.pending_task.assistance.confirm.subtitle'),
                    flowReference: 'AssistanceCancellation'
                }).present();
            }
        }, 20000);
    };
    AppGlobalService.prototype.assistanceCancelled = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.componentSettingsProvider.isLoggedIn()) {
                _this.modalController.create(InformationModalComponent, {
                    icon: 'mapfre-warning',
                    labels: {
                        title: 'app-global.pending_task.assistance.cancel.title',
                        subtitle: 'app-global.pending_task.assistance.cancel.subtitle'
                    },
                    hideButtons: true
                }).present();
            }
        }, 20000);
    };
    AppGlobalService.decorators = [
        { type: Injectable },
    ];
    AppGlobalService.ctorParameters = function () { return [
        { type: Http, },
        { type: ComponentSettingsProvider, },
        { type: Storage, },
        { type: CookieService, },
        { type: TranslationProvider, },
        { type: ModalController, },
        { type: PlatformProvider, },
    ]; };
    return AppGlobalService;
}());
export { AppGlobalService };
//# sourceMappingURL=app-global.service.js.map