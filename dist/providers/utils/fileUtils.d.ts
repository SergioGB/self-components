export declare class FileUtils {
    constructor();
    b64toBlob(b64Data: any): Blob;
    getBase64(file: File): Promise<String>;
    static formatBytes(bytes: number): string;
}
