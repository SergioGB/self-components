import { Injectable } from '@angular/core';
var FileUtils = (function () {
    function FileUtils() {
    }
    FileUtils.prototype.b64toBlob = function (b64Data) {
        var sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        return new Blob(byteArrays, { type: "application/octet-stream" });
    };
    FileUtils.prototype.getBase64 = function (file) {
        return new Promise(function (resolve, reject) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () { return resolve(reader.result); };
            reader.onerror = function (error) { return reject(error); };
        });
    };
    FileUtils.formatBytes = function (bytes) {
        var i = bytes === 0 ? 0 : Math.floor(Math.log(bytes) / Math.log(1024));
        return Math.ceil(bytes / Math.pow(1024, i)) + " " + ['B', 'KB', 'MB', 'GB', 'TB'][i];
    };
    FileUtils.decorators = [
        { type: Injectable },
    ];
    FileUtils.ctorParameters = function () { return []; };
    return FileUtils;
}());
export { FileUtils };
//# sourceMappingURL=fileUtils.js.map