import { FormGroup } from '@angular/forms';
export declare class FormUtil {
    constructor();
    static mapFormData(formValues: any, formGroup: FormGroup): void;
    static mapListItem(fieldsList: any[], fieldName: string, formGroup: FormGroup): void;
    static getListEntryById(fieldsList: any[], id: any): any;
    static getFieldValue(formValues: any, fieldName: string): any;
}
