import { Injectable } from '@angular/core';
var FormUtil = (function () {
    function FormUtil() {
    }
    FormUtil.mapFormData = function (formValues, formGroup) {
        if (formValues) {
            Object.keys(formGroup.value).forEach(function (fieldName) {
                var fieldData = formValues[fieldName];
                if (fieldData) {
                    formGroup.controls[fieldName].patchValue(fieldData);
                }
            });
        }
    };
    FormUtil.mapListItem = function (fieldsList, fieldName, formGroup) {
        var fieldData = formGroup.value[fieldName];
        if (fieldData) {
            formGroup.controls[fieldName].patchValue(FormUtil.getListEntryById(fieldsList, fieldData.id));
        }
    };
    FormUtil.getListEntryById = function (fieldsList, id) {
        var field;
        if (fieldsList && fieldsList.length && id) {
            var matches = fieldsList.filter(function (field) { return field.id === id; });
            if (matches) {
                field = matches[0];
            }
        }
        return field;
    };
    FormUtil.getFieldValue = function (formValues, fieldName) {
        return formValues ? formValues[fieldName] || undefined : undefined;
    };
    FormUtil.decorators = [
        { type: Injectable },
    ];
    FormUtil.ctorParameters = function () { return []; };
    return FormUtil;
}());
export { FormUtil };
//# sourceMappingURL=form.js.map