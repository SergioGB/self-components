import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import * as jQuery from 'jquery';
var Utils = (function () {
    function Utils() {
    }
    Utils.isFunction = function (object) {
        return typeof object === 'function';
    };
    Utils.hasLength = function (string) {
        return !_.isNil(string) && string.length > 0;
    };
    Utils.getPrefixedString = function (baseString, prefixString) {
        var prefix = _.size(prefixString) ? prefixString + "-" : "";
        return "" + prefix + baseString;
    };
    Utils.getTimeStringHours = function (timeString) {
        var timeStringTokens = timeString.split(':');
        return timeStringTokens.length >= 1 ? parseInt(timeStringTokens[0]) : undefined;
    };
    Utils.getTimeStringMinutes = function (timeString) {
        var timeStringTokens = timeString.split(':');
        return timeStringTokens.length >= 2 ? parseInt(timeStringTokens[1]) : undefined;
    };
    Utils.getTimeStringSeconds = function (timeString) {
        var timeStringTokens = timeString.split(':');
        return timeStringTokens.length >= 3 ? parseInt(timeStringTokens[2]) : undefined;
    };
    Utils.equalsIgnoreCase = function (foo, bar) {
        return foo.toLowerCase() === bar.toLowerCase();
    };
    Utils.generateUniqueId = function () {
        return Math.random().toString().replace(/\D/g, '');
    };
    Utils.isObject = function (object) {
        return typeof object === 'object' && object.length === undefined;
    };
    Utils.copy = function (object) {
        return JSON.parse(JSON.stringify(object));
    };
    Utils.equals = function (foo, bar) {
        return JSON.stringify(foo) === JSON.stringify(bar);
    };
    Utils.removeNilKeys = function (object) {
        Object.keys(object).forEach(function (key) { return !object[key] && object[key] !== 0 ? delete object[key] : ''; });
    };
    Utils.isArray = function (object) {
        return typeof object === 'object' && object.length !== undefined;
    };
    Utils.contains = function (iterable, value) {
        var _this = this;
        var contained = false;
        iterable.forEach(function (entry) {
            if (_this.equals(entry, value)) {
                contained = true;
            }
        });
        return contained;
    };
    Utils.containsByField = function (iterable, value, fieldName) {
        return this.getByField(iterable, value, fieldName) !== undefined;
    };
    Utils.getByField = function (iterable, value, fieldName) {
        var _this = this;
        var foundEntry = undefined;
        iterable.forEach(function (entry) {
            if (_this.equals(entry[fieldName], value[fieldName])) {
                foundEntry = entry;
            }
        });
        return foundEntry;
    };
    Utils.isLast = function (index, array) {
        return index === array.length - 1;
    };
    Utils.moveArrayEntry = function (array, currentIndex, newIndex) {
        array.splice(newIndex, 0, array.splice(currentIndex, 1)[0]);
    };
    Utils.getRandomInteger = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    Utils.getRandomBoolean = function () {
        return Math.random() >= .5;
    };
    Utils.isControlChecked = function (control) {
        return control.value !== true ? { 'notChecked': true } : undefined;
    };
    Utils.scrollToContent = function (content) {
        if (!_.isNil(content)) {
            content.scrollToTop();
        }
    };
    Utils.scrollToId = function (id) {
        if (!_.isNil(id)) {
            var element = document.getElementById(id);
            if (!_.isNil(element)) {
                element.scrollIntoView({ block: 'start', behavior: 'smooth' });
            }
        }
    };
    Utils.focusForm = function (formSelector) {
        setTimeout(function () {
            var form = jQuery(formSelector);
            if (form.length) {
                var inputs = form.find('input');
                if (inputs.length) {
                    var firstInput = inputs.eq(0);
                    var inputValue = firstInput.val();
                    firstInput.focus().val('').val(inputValue);
                }
            }
        }, 150);
    };
    Utils.onIE = function () {
        var matches = navigator.userAgent.match(/Trident/);
        return matches && matches.length > 0;
    };
    Utils.onEdge = function () {
        var matches = navigator.userAgent.match(/Edge/);
        return matches && matches.length > 0;
    };
    Utils.onChrome = function () {
        var matches = navigator.userAgent.match(/Chrome/);
        return matches && matches.length > 0;
    };
    Utils.getLastViewName = function (navController) {
        return this.getViewControllerName(navController.last());
    };
    Utils.getLastViewParameters = function (navController) {
        return navController.last() ? navController.last().getNavParams() : null;
    };
    Utils.getPreviousViewName = function (navController) {
        return this.getViewControllerName(navController.getPrevious());
    };
    Utils.getViewControllerName = function (viewController) {
        return viewController.name
            || (viewController.instance ? viewController.instance.name : '')
            || (viewController.component ? viewController.component.name : '');
    };
    Utils.output = function (data) {
        window.console.log(JSON.stringify(data, undefined, 2));
    };
    Utils.decorators = [
        { type: Injectable },
    ];
    Utils.ctorParameters = function () { return []; };
    return Utils;
}());
export { Utils };
//# sourceMappingURL=utils.js.map