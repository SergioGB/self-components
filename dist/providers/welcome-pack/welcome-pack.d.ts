export declare class WelcomePackProvider {
    private welcomePackAvailable;
    constructor();
    getWelcomePackAvailable(): boolean;
    setWelcomePackAvailable(welcomePackAvailable: boolean): void;
}
