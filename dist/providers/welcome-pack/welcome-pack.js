import { Injectable } from '@angular/core';
var WelcomePackProvider = (function () {
    function WelcomePackProvider() {
        this.welcomePackAvailable = true;
    }
    WelcomePackProvider.prototype.getWelcomePackAvailable = function () {
        return this.welcomePackAvailable;
    };
    WelcomePackProvider.prototype.setWelcomePackAvailable = function (welcomePackAvailable) {
        this.welcomePackAvailable = welcomePackAvailable;
    };
    WelcomePackProvider.decorators = [
        { type: Injectable },
    ];
    WelcomePackProvider.ctorParameters = function () { return []; };
    return WelcomePackProvider;
}());
export { WelcomePackProvider };
//# sourceMappingURL=welcome-pack.js.map