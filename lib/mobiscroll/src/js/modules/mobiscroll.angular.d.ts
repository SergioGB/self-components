import { ModuleWithProviders, mobiscroll } from '../frameworks/angular';
import { MbscInputModule } from '../input.angular';
import { MbscCalendarModule, MbscCalendar, MbscCalendarOptions, MbscCalendarComponent } from '../calendar.angular';
import { MbscFormsModule, MbscForm, MbscRating, MbscDropdown, MbscTextarea, MbscButton, MbscCheckbox, MbscSwitch, MbscStepper, MbscProgress, MbscSlider, MbscRadio, MbscRadioGroup, MbscSegmentedGroup, MbscSegmented, MbscFormOptions, MbscFormGroup, MbscFormGroupTitle, MbscFormGroupContent, MbscAccordion } from '../forms.angular';
import { MbscPageModule, MbscPage, MbscPageOptions, MbscNote, MbscAvatar } from '../page.angular';
declare class MbscModule {
    static forRoot(config: {
        angularRouter: any;
    }): ModuleWithProviders;
}
export { mobiscroll, MbscCalendar, MbscCalendarComponent, MbscForm, MbscRating, MbscPage, MbscNote, MbscAvatar, MbscDropdown, MbscTextarea, MbscButton, MbscCheckbox, MbscSwitch, MbscStepper, MbscProgress, MbscSlider, MbscRadio, MbscRadioGroup, MbscSegmentedGroup, MbscSegmented, MbscFormGroup, MbscFormGroupTitle, MbscFormGroupContent, MbscAccordion, MbscCalendarOptions, MbscFormOptions, MbscPageOptions, MbscModule, MbscInputModule, MbscCalendarModule, MbscFormsModule, MbscPageModule };
